package br.com.BiTemplate.enums;

public enum MetaLinha {
	ANCORA("Ancora"),
	PLACASEPARAFUSOS("Placas e Parafusos"),
	QUADRIL("Quadril"),
	CAIXAS("Caixas"),
	COLUNA("Coluna"),
	SIGNUS("Signus"),
	JOELHO("Joelho"),
	PLACATUBO("Placa Tubo"),
	ORTOLOCK("Ortolock"),
	MINIEMICRO("Mini e Micro"),
	BUCOMAXILO("Bucomaxilo"),
	FIXADORES("Fixadores"),
	MEDICINAESPORTIVA("Medicina Esportiva"),
	MANUFATURAADITIVA("Manufatura Aditiva"),
	DIVERSOS("Diversos");

	private final String nome;
	
	MetaLinha(String nome) {
		this.nome = nome;
	}
	
	public String toString() {
		return this.nome;
	}
	
}
