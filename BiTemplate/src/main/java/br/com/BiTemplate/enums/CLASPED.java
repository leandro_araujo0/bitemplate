package br.com.BiTemplate.enums;

public enum CLASPED {
	CAIXAS("2"), REPOSICAO("1"), EXPORTACAO("4"), EQUIPAMENTOS("3"), MERCADOINTERNO("99"), GLOBAL("00"), EXPORTACAOORTOPEDIA("00"), EXPORTACAOEQUIPAMENTOS("00");
	
	private String number;

	CLASPED(String number) {
		this.number = number;
	}
	
	public String getNumber() {
		return this.number;
	}
	
}
