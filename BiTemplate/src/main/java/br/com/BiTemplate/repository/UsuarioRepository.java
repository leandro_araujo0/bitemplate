package br.com.BiTemplate.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.BiTemplate.model.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, String> {

}
