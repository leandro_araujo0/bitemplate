package br.com.BiTemplate.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.BiTemplate.model.FollowUpProduto;

public interface FollowUpProdutoRepository extends CrudRepository<FollowUpProduto, String>{

}
