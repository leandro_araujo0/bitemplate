package br.com.BiTemplate.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.BiTemplate.model.Relatorio;

public interface RelatorioRepository extends CrudRepository<Relatorio, String>{

}
