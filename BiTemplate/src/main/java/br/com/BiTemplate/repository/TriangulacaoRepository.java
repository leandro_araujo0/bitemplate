package br.com.BiTemplate.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.BiTemplate.model.Triangulacao;

public interface TriangulacaoRepository extends CrudRepository<Triangulacao, Integer>{

}
