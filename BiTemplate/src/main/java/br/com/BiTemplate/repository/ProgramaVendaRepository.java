package br.com.BiTemplate.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.BiTemplate.model.ProgramaVendas;

public interface ProgramaVendaRepository extends CrudRepository<ProgramaVendas, Integer> {

}
