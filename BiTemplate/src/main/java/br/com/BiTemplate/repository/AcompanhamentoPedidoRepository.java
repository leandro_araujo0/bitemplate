package br.com.BiTemplate.repository;

import java.util.LinkedList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.BiTemplate.model.AcompanhamentoPedido;

public interface AcompanhamentoPedidoRepository extends CrudRepository<AcompanhamentoPedido, Integer> {

	public AcompanhamentoPedido findByNumPedidoAndItemPed(String numeroPedido, String numItemPedido);

	public Iterable<AcompanhamentoPedido> findByMesProducao(String mesProducao);

	public Iterable<AcompanhamentoPedido> findByNumPedidoIn(LinkedList<String> numeroPedidos);

	public Iterable<AcompanhamentoPedido> findByNumPedido(String pv);

	@Query("select ap from AcompanhamentoPedido ap where concat(ops, chassi, caldInt, sistElev, porta, hidraulica, painelEletrico, eletrica, revestimento, embalagem, montagem, opRevestimento, opEmbalagem, opAjusteRevestimento, opPneumatica, camara, estrutura, gerador) like concat('%', ?1, '%')")
	public Iterable<AcompanhamentoPedido> findByAnyOp(String op);

}
