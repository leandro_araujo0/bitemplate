package br.com.BiTemplate.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import br.com.BiTemplate.model.PedidoFaturamento;

public interface PedidoFaturamentoRepository extends CrudRepository<PedidoFaturamento, Integer> {

	public Optional<PedidoFaturamento> findByNumeroPedidoAndItemPedido(String numeroPedido, String itemPedido);

	public Iterable<PedidoFaturamento> findByAnoAndMes(String ano, String mes);

	public Iterable<PedidoFaturamento> findByNumeroPedido(String pedido);


}
