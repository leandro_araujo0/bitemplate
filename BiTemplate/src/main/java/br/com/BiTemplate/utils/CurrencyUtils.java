package br.com.BiTemplate.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class CurrencyUtils {

	public static String format(BigDecimal valor) {
		if (valor == null)
			return null;
		DecimalFormat decimalFormat = new DecimalFormat("R$ ##,###,###,##0.00",
				new DecimalFormatSymbols(new Locale("pt", "BR")));
		decimalFormat.setMinimumFractionDigits(2);
		decimalFormat.setParseBigDecimal(true);
		return decimalFormat.format(valor);
	}
}
