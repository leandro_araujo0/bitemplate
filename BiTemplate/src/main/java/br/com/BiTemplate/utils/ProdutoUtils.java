package br.com.BiTemplate.utils;

import java.util.LinkedList;

import br.com.BiTemplate.model.VariacaoPrecoProduto;

public class ProdutoUtils {

	public static void removerProdutosSemVariacao(LinkedList<VariacaoPrecoProduto> precosProdutos) {
		LinkedList<VariacaoPrecoProduto> produtosRemovidos = new LinkedList<>();
		for (VariacaoPrecoProduto variacaoPrecoProduto : precosProdutos) {
			if (variacaoPrecoProduto.getVariacaoPorcento() == null) {
				produtosRemovidos.add(variacaoPrecoProduto);
			}
		}
		precosProdutos.removeAll(produtosRemovidos);
	}

}
