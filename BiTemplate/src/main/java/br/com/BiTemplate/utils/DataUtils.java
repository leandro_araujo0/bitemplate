package br.com.BiTemplate.utils;

import java.util.Calendar;
import java.util.Collection;
import java.util.LinkedList;

public class DataUtils {

	public static String formatData(String data) {
		return (data != null) ? data.substring(6, 8) + "/" + data.substring(4, 6) + "/" + data.substring(0, 4) : null;
	}

	public static String formatMes(String data) {
		return data.substring(4, 6) + "/" + data.substring(0, 4);
	}

	public static String[] getLabelMeses() {
		String[] labels = { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro",
				"Outubro", "Novembro", "Dezembro" };
		return labels;
	}

	public static Calendar getLastYear() {
		Calendar date = Calendar.getInstance();
		date.add(Calendar.YEAR, -1);
		return date;
	}

	public static Collection<String> formatMes(Collection<String> chartLabels) {
		Collection<String> labels = new LinkedList<>();
		for (String string : chartLabels) {
			switch (string.substring(4, 6)) {
			case "01":
				labels.add("janeiro/" + string.substring(0, 4));
				break;
			case "02":
				labels.add("fevereiro/" + string.substring(0, 4));
				break;
			case "03":
				labels.add("março/" + string.substring(0, 4));
				break;
			case "04":
				labels.add("abril/" + string.substring(0, 4));
				break;
			case "05":
				labels.add("maio/" + string.substring(0, 4));
				break;
			case "06":
				labels.add("junho/" + string.substring(0, 4));
				break;
			case "07":
				labels.add("julho/" + string.substring(0, 4));
				break;
			case "08":
				labels.add("agosto/" + string.substring(0, 4));
				break;
			case "09":
				labels.add("setembro/" + string.substring(0, 4));
				break;
			case "10":
				labels.add("outubro/" + string.substring(0, 4));
				break;
			case "11":
				labels.add("novembro/" + string.substring(0, 4));
				break;
			case "12":
				labels.add("dezembro/" + string.substring(0, 4));
				break;
			}
		}
		return labels;
	}

	public static String getMes(String mes) {
		if (mes == null) return null;
		switch (mes.toUpperCase()) {
			case "JANEIRO":
				return "1";
			case "FEVEREIRO":
				return "2";
			case "MARÇO":
				return "3";
			case "MARCO":
				return "3";
			case "ABRIL":
				return "4";
			case "MAIO":
				return "5";
			case "JUNHO":
				return "6";
			case "JULHO":
				return "7";
			case "AGOSTO":
				return "8";
			case "SETEMBRO":
				return "9";
			case "OUTUBRO":
				return "10";
			case "NOVEMBRO":
				return "11";
			case "DEZEMBRO":
				return "12";
		}
		return mes;
	}

	public static int getMesAtual() {
		return Calendar.getInstance().get(Calendar.MONTH);
	}

	public static int getMesPassado() {
		Calendar date = Calendar.getInstance();
		date.add(Calendar.MONTH, -1);
		return date.get(Calendar.MONTH);
	}

	public static Calendar getLastYear(int ref) {
		Calendar date = Calendar.getInstance();
		date.set(Calendar.YEAR, ref - 1);
		return date;
	}

	public static Calendar getInstance(int ano) {
		Calendar date = Calendar.getInstance();
		date.set(Calendar.YEAR, ano);
		return date;
	}

	public static String formatData(Calendar date) {
		int year = date.get(Calendar.YEAR);
		int month = date.get(Calendar.MONTH);
		int day = date.get(Calendar.DAY_OF_MONTH);
		
		String inversedDate = "" + year + month + day;
		
		return formatData(inversedDate);
	}

	public static String mmddyyToYymmdd(String data) {
		String dataFinal = data.replace("/", "");
		dataFinal = dataFinal.substring(4, 8) + dataFinal.substring(2, 4) + dataFinal.substring(0, 2);
		return dataFinal;
	}

}
