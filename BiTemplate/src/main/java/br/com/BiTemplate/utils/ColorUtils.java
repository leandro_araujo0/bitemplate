package br.com.BiTemplate.utils;


import java.util.Random;

import be.ceau.chart.color.Color;

public class ColorUtils extends Color {
	
	private static final Random RANDOMIZER = new Random(System.nanoTime());

	public ColorUtils(Color color, double alpha) {
		super(color, alpha);
	}
	
	public static Color random() {
		int r = RANDOMIZER.nextInt(256);
		int g = RANDOMIZER.nextInt(256);
		int b = RANDOMIZER.nextInt(256);
		double a = 1;
		return new Color(r, g, b, a);
	}

	public static Color fromHex(String hexColor) {
		int r =  Integer.valueOf(hexColor.substring(0, 2), 16);
		int g =  Integer.valueOf(hexColor.substring(2, 4), 16);
		int b =  Integer.valueOf(hexColor.substring(4, 6), 16);
		int a;
		if (hexColor.length() == 8) {
			a =  Integer.valueOf(hexColor.substring(0, 2), 16);
		} else {
			a = 1;
		}
		
		return new Color(r, g, b, a);
	}

}
