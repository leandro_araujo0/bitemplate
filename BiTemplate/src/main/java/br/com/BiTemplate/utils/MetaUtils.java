package br.com.BiTemplate.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

import br.com.BiTemplate.model.ChartDataAndLabels;

public class MetaUtils {

	public static BigDecimal recalcula(ChartDataAndLabels vendasRealizadas, ChartDataAndLabels metas, int numRealiz, double metaObjetiva) {
		//SOMAR AS METAS ATÉ O ULTIMO MES PASSADO
		//SOMAR AS VENDAS ATÉ O ULTIMO MES PASSADO
		//OBTER A DIFERENÇA COMO META NÃO ALCANÇADA / ULTRAPASSADA
		try {
		int mesPassado = DataUtils.getMesPassado();
		BigDecimal saldo = BigDecimal.ZERO;
		if (mesPassado < (numRealiz - 1)) {//SE O MES PASSADO < MES DA META, ADICIONAR SALDO TOTAL
			for(int i = 0; i <= mesPassado; i++) {
				saldo = saldo.add((BigDecimal) metas.getChartData().toArray()[i]).subtract((BigDecimal) vendasRealizadas.getChartData().toArray()[i]);
			}
			saldo = saldo.divide(BigDecimal.valueOf(11 - mesPassado), 2, RoundingMode.HALF_EVEN);
			BigDecimal metaRecalculada = BigDecimal.valueOf(metaObjetiva).add(saldo);
			return metaRecalculada;
		} else {//SE O MES PASSADO > MES DA META, CALCULA RETROATIVO
			for(int i = 0; i < (numRealiz - 1); i++) {
				saldo = saldo.add((BigDecimal) metas.getChartData().toArray()[i]).subtract((BigDecimal) vendasRealizadas.getChartData().toArray()[i]);
			}
			saldo = saldo.divide(BigDecimal.valueOf(13 - numRealiz), 2, RoundingMode.HALF_EVEN);
			BigDecimal metaRecalculada = BigDecimal.valueOf(metaObjetiva).add(saldo);
			return metaRecalculada;
		}
		}catch (Exception e) {
			return BigDecimal.valueOf(metaObjetiva);
		}
	}

}
