package br.com.BiTemplate.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;

public class CalcUtils {

	public static BigDecimal getPorcentagem(BigDecimal valor, Collection<BigDecimal> valores) {
		BigDecimal valorTotal = BigDecimal.ZERO;
		for (BigDecimal bigDecimal : valores) {
			valorTotal = valorTotal.add(bigDecimal);
		}
		return valor.divide(valorTotal, 2, RoundingMode.HALF_EVEN).multiply(BigDecimal.valueOf(100));
	}

}
