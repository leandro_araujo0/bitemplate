package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.ProdutoDAO;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/custos")
public class CustosController {

	@GetMapping("")
	public String custos() {
		return "custos";
	}

	@ResponseBody
	@PostMapping("")
	public DataTable getCustos() {
		return ProdutoDAO.getCustos();
	}

}
