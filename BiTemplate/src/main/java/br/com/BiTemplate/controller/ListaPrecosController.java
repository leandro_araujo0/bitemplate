package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.VendasDAO;
import br.com.BiTemplate.enums.UNIDADEVENDAS;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/lista-de-precos")
public class ListaPrecosController {
	
	@GetMapping("")
	public String listaPrecos() {
		return "listaPrecos/listaPrecos";
	}
	
	@ResponseBody
	@PostMapping("/getTabela")
	public DataTable getTabela(UNIDADEVENDAS unidadeVendas, String codigoCliente) {
		if (codigoCliente != null && !codigoCliente.equals("")) {
			return VendasDAO.getTabelaPrecosCliente(codigoCliente);
		} else {
			return VendasDAO.getTabelaPrecos(unidadeVendas);
		}
	}

}
