package br.com.BiTemplate.controller;

import java.util.Collection;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.MetasDAO;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/metas")
public class MetasController {

	@GetMapping("")
	public String metas() {
		return "metas/metas";
	}
	
	@ResponseBody
	@GetMapping("/getCabecalhoTabela")
	public Collection<String> getCabecalhoTabela() {
		return MetasDAO.getCabecalhoTabelaVendas();
	}
	
	@ResponseBody
	@GetMapping("/getTabelaMetas")
	public DataTable getTabelaMetas() {
		return MetasDAO.getTabelaMetaVendas();
	}

}
