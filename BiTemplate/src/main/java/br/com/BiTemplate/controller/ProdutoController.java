package br.com.BiTemplate.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import be.ceau.chart.BarChart;
import be.ceau.chart.LineChart;
import be.ceau.chart.color.Color;
import be.ceau.chart.data.BarData;
import be.ceau.chart.data.LineData;
import be.ceau.chart.dataset.BarDataset;
import be.ceau.chart.dataset.LineDataset;
import be.ceau.chart.options.LineOptions;
import be.ceau.chart.options.scales.LinearScale;
import be.ceau.chart.options.ticks.LinearTicks;
import br.com.BiTemplate.DAO.EstruturaDAO;
import br.com.BiTemplate.DAO.ProdutoDAO;
import br.com.BiTemplate.DAO.VendasDAO;
import br.com.BiTemplate.model.ChartDataAndLabels;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.model.DetalhesProduto;
import br.com.BiTemplate.model.Produto;
import br.com.BiTemplate.model.VariacaoPrecoProduto;
import br.com.BiTemplate.utils.ColorUtils;
import br.com.BiTemplate.utils.ProdutoUtils;

@Controller
@RequestMapping("/produtos")
public class ProdutoController {

	@GetMapping("")
	public String produtos(Model model) {
		return "produtos/produtos";
	}
	
	@ResponseBody
	@GetMapping("/lista")
	public DataTable listaProdutos(String tipo, String produto, String descricao, String bloqueio) {
		return ProdutoDAO.findDetalhesProdutosTabela(tipo, produto, descricao, bloqueio);
	}
	
	@ResponseBody
	@GetMapping("/codigo")
	public List<Produto> findProduto(String codigo) {
		List<Produto> produtos = ProdutoDAO.findByCodigo(codigo);
		return produtos;
	}

	@GetMapping("/variacao-precos")
	public String variacaoPrecos(String dataInicial, String dataFinal, String fornecedor, Model model) {
		LinkedList<VariacaoPrecoProduto> precosProdutos = ProdutoDAO.getVariacaoPrecoProdutos(dataInicial, dataFinal,
				fornecedor);

		ProdutoUtils.removerProdutosSemVariacao(precosProdutos);

		precosProdutos.sort(new Comparator<VariacaoPrecoProduto>() {
			@Override
			public int compare(VariacaoPrecoProduto o1, VariacaoPrecoProduto o2) {
				return o2.getVariacao() - o1.getVariacao();
			}
		});

		model.addAttribute("dataInicial", dataInicial);
		model.addAttribute("dataFinal", dataFinal);
		model.addAttribute("fornecedor", fornecedor);
		model.addAttribute("precosProdutos", precosProdutos);

		return "produtos/variacaoPrecos";
	}

	@GetMapping("/variacao-precos/grafico")
	public String graficoVariacaoPrecos(String codigoProduto, String dataInicial, String dataFinal,
			String fornecedor, Model model) {

		ChartDataAndLabels dl = ProdutoDAO.getBarData(codigoProduto, dataInicial, dataFinal, fornecedor);

		LineDataset dataset = new LineDataset().setLabel("Média Valor de Compras").setData(dl.getChartData()).setBorderWidth(2).setLineTension(0f).setBorderColor(new Color(50, 200, 255));
		LineData data = new LineData().setLabels(dl.getChartLabelsToArray()).addDataset(dataset);
		LineChart chart = new LineChart(data);
		LineOptions lo = new LineOptions();
		lo.setScales(LineOptions.scales());
		lo.getScales().getyAxes().add(new LinearScale().setTicks(new LinearTicks().setBeginAtZero(true)));
		chart.setOptions(lo);
		model.addAttribute("chart", chart);
		
		Produto produto = ProdutoDAO.findOne(codigoProduto);
		model.addAttribute("produto", produto);

		return "produtos/graficoVariacaoPrecos";
	}

	@ResponseBody
	@GetMapping("/estrutura")
	public List<Produto> findProdutoEstrutura(String codigo) {
		List<Produto> produtos = EstruturaDAO.findByCodigo(codigo);
		return produtos;
	}

	@GetMapping("/detalhes")
	public String detalhes(String codigoProduto, Model model) {
		DetalhesProduto detalhesProduto = ProdutoDAO.findDetalhesProduto(codigoProduto);
		model.addAttribute("detalhesProduto", detalhesProduto);
		return "produtos/detalhesProduto";
	}
	
	@ResponseBody
	@GetMapping("/chartData")
	public LineChart produtoChartData(String codigoProduto, Model model) {
		ChartDataAndLabels cdl = ProdutoDAO.getDataCMM(codigoProduto);
		LineDataset dataset = new LineDataset().setData(cdl.getChartData()).setBorderWidth(2).setBackgroundColor(null).setBorderColor(Color.random());
		LineData lineData = new LineData().addDataset(dataset).setLabels(cdl.getChartLabels());
		return new LineChart(lineData);
	}
	
	@ResponseBody
	@GetMapping("/graficoCompradoresProduto")
	public BarChart getGraficoCompradoresProduto(String codigoProduto) {
		ChartDataAndLabels cdl = VendasDAO.getCompradoresProduto(codigoProduto);
		BarDataset dataset = new BarDataset().setData(cdl.getChartData());
		for (int i = 0; i < cdl.getChartData().size(); i++) {
			dataset.addBackgroundColor(ColorUtils.random());
		}
		BarData data = new BarData().setLabels(cdl.getChartLabels()).addDataset(dataset);
		return new BarChart(data).setHorizontal();
	}
	
	
	@ResponseBody
	@GetMapping("/produtos-excel")
	public Object produtosExcel(Model model, HttpServletResponse response) throws FileNotFoundException {
		File file = ProdutoDAO.findAllExcel();
		
		response.setContentType("application/xls");
		response.setHeader("Content-Disposition", "attachment; filename=produtos.xls");
		InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
		
		return resource;
	}
	
	@ResponseBody
	@PostMapping("/getGrafico")
	public LineChart getGrafico(@RequestParam("codigoProdutos[]") String[] codigoProdutos) {
		ChartDataAndLabels cdl = VendasDAO.getHistoricoVendasProdutos(codigoProdutos);
		LineData data = new LineData().setLabels(cdl.getChartLabels());
		LineDataset lineDataset = new LineDataset().setData(cdl.getChartData()).setBorderColor(ColorUtils.random());
		data.addDataset(lineDataset);
		return new LineChart().setData(data);
	}
	
	@ResponseBody
	@GetMapping("/valida")
	public boolean validaProduto(String codigoProduto) {
		return ProdutoDAO.validaProduto(codigoProduto);
	}
	
}
