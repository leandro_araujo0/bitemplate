package br.com.BiTemplate.controller;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.OrdemProducaoDAO;
import br.com.BiTemplate.DAO.VendasDAO;
import br.com.BiTemplate.model.AcompanhamentoPedido;
import br.com.BiTemplate.model.ClipBoard;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.model.ItemPedidoVenda;
import br.com.BiTemplate.model.OrdemProducao;
import br.com.BiTemplate.model.PedidoManual;
import br.com.BiTemplate.repository.AcompanhamentoPedidoRepository;
import br.com.BiTemplate.utils.HtmlUtils;

@Controller
@RequestMapping("/carteira-pcp")
public class CarteiraPCPController {

	@Autowired
	private AcompanhamentoPedidoRepository repository;
	@Autowired
	private PedidosManuaisRepository pmRepository;
	
	@ResponseBody
	@PostMapping("/clipBoard")
	public ClipBoard getClipBoard(HttpSession session) {
		ClipBoard clipBoard = (ClipBoard) session.getAttribute("clipBoard");
		
		if (clipBoard == null) {
			clipBoard = new ClipBoard();
			session.setAttribute("clipBoard", clipBoard);
		}
		
		return clipBoard;
	}
	
	@ResponseBody
	@PostMapping("/addClipBoardItem")
	public String addClipBoardItem(String item, HttpSession session) {
		getClipBoard(session).addItem(item);
		return "success";
	}
	
	@ResponseBody
	@PostMapping("/limparClipBoard")
	public String limparClipBoard(HttpSession session) {
		ClipBoard clipBoard = getClipBoard(session);
		clipBoard.limpaItens();
		return "success";
	}

	@GetMapping("")
	public String carteiraPcp() {
		return "carteiraPCP";
	}

	@ResponseBody
	@PostMapping
	public DataTable getCarteiraPcp(String filtro) {
		Iterable<PedidoManual> pedidosManuais;
		if (filtro.equals("finalizados")) {
			pedidosManuais = new LinkedList<PedidoManual>();
			Iterator<AcompanhamentoPedido> iterAps = repository.findAll().iterator();
			while (iterAps.hasNext()) {
				AcompanhamentoPedido ap = iterAps.next();
				((LinkedList<PedidoManual>) pedidosManuais).add(new PedidoManual().setNumero(ap.getNumPedido()).setItem(ap.getItemPed()));
			}
		} else {
			pedidosManuais = pmRepository.findAll();
		}
		DataTable dataTable = VendasDAO.getCarteiraPcp(filtro, pedidosManuais);
		for (Collection<Object> collection : dataTable.getData()) {
			LinkedList<Object> tempCollection = (LinkedList<Object>) collection;
			AcompanhamentoPedido ap = repository.findByNumPedidoAndItemPed(HtmlUtils.removeLink((String) tempCollection.get(1)), (String) tempCollection.get(2));
			if (ap == null) {
				continue;
			}
			((LinkedList<Object>) collection).set(0, ap.getPrioridade());
			int i = 7;
			((LinkedList<Object>) collection).set(i++, ap.getMesProducao());
			((LinkedList<Object>) collection).set(i++, ap.getOps());
			((LinkedList<Object>) collection).set(i++, ap.getProcesso());
			((LinkedList<Object>) collection).set(i++, ap.getObs());
			((LinkedList<Object>) collection).set(i++, ap.getMemorando());
			i = i + 4;
			((LinkedList<Object>) collection).set(i++, ap.getChassi());
			((LinkedList<Object>) collection).set(i++, ap.getCamara());
			((LinkedList<Object>) collection).set(i++, ap.getEstrutura());
			((LinkedList<Object>) collection).set(i++, ap.getGerador());
			((LinkedList<Object>) collection).set(i++, ap.getCaldInt());
			((LinkedList<Object>) collection).set(i++, ap.getSistElev());
			((LinkedList<Object>) collection).set(i++, ap.getPorta());
			((LinkedList<Object>) collection).set(i++, ap.getHidraulica());
			((LinkedList<Object>) collection).set(i++, ap.getOpPneumatica());
			((LinkedList<Object>) collection).set(i++, ap.getEletrica());
			((LinkedList<Object>) collection).set(i++, ap.getPainelEletrico());
			((LinkedList<Object>) collection).set(i++, ap.getOpRevestimento());
			((LinkedList<Object>) collection).set(i++, ap.getOpEmbalagem());
			((LinkedList<Object>) collection).set(i++, ap.getOpAjusteRevestimento());
			((LinkedList<Object>) collection).set(i++, ap.getRevestimento());
			((LinkedList<Object>) collection).set(i++, ap.getEmbalagem());
		}
		return dataTable;
	}

	@ResponseBody
	@GetMapping("get-itens-pedido")
	public Iterable<ItemPedidoVenda> getItensPedido(String numeroPedido) {
		return VendasDAO.getItensPedidoVenda(numeroPedido);
	}

	@GetMapping("/{numeroPedido}/{numItemPedido}/acompanhamento")
	public String acompanhamentoPedido(@PathVariable String numeroPedido, @PathVariable String numItemPedido,
			Model model) {
		ItemPedidoVenda itemPedido = VendasDAO.getItemPedido(numeroPedido, numItemPedido);
		model.addAttribute("itemPedido", itemPedido);

		AcompanhamentoPedido ap = repository.findByNumPedidoAndItemPed(numeroPedido, numItemPedido);
		if (ap == null) {
			ap = new AcompanhamentoPedido();
			ap.setItemPed(numItemPedido);
			ap.setNumPedido(numeroPedido);
			repository.save(ap);
		}

		PedidoManual pm = pmRepository.findByNumeroAndItem(numeroPedido, numItemPedido);
		
		model.addAttribute("pm", pm);

		model.addAttribute("ap", ap);

		return "carteiraPCPDetalhesPedido";
	}

	@ResponseBody
	@PostMapping("/acompanhamentoPedido")
	public void acompanhamentoPedido(AcompanhamentoPedido ap) {
		AcompanhamentoPedido tempAp = repository.findByNumPedidoAndItemPed(ap.getNumPedido(), ap.getItemPed());
		if (tempAp != null) {
			ap.setId(tempAp.getId());
		}
		repository.save(ap);
	}

	@ResponseBody
	@PostMapping("/getAcompanhamentoPedido")
	public AcompanhamentoPedido getAcompanhamentoPedido(String numPedido, String itemPedido) {
		return repository.findByNumPedidoAndItemPed(numPedido, itemPedido);
	}

	@GetMapping("/adicionar-manual")
	public String adicionarManual(Model model, PedidoManual pedidoManual) {
		return "adicionarPedidoCarteiraPCP";
	}

	@PostMapping("/adicionar-manual")
	public String adicionarManual(PedidoManual pedidoManual) {
		PedidoManual tempPedidoManual = pmRepository.findByNumeroAndItem(pedidoManual.getNumero(), pedidoManual.getItem());
		if (tempPedidoManual == null) {
			pmRepository.save(pedidoManual);
		}
		return "redirect:/carteira-pcp/" + pedidoManual.getNumero() + "/" + pedidoManual.getItem() + "/acompanhamento";
	}
	
	@GetMapping("/adicionar-manual/{numPedido}/{itemPedido}")
	public String adicionarManual(PedidoManual pedidoManual, @PathVariable String numPedido, @PathVariable String itemPedido) {
		PedidoManual tempPedidoManual = pmRepository.findByNumeroAndItem(numPedido, itemPedido);
		if (tempPedidoManual == null) {
			PedidoManual pm = new PedidoManual();
			pm.setNumero(numPedido);
			pm.setItem(itemPedido);
			pmRepository.save(pm);
		}
		return "redirect:/carteira-pcp/" + numPedido + "/" + itemPedido + "/acompanhamento";
	}

	@GetMapping("/remover-pedido")
	public String removerPedido(String numero, String item, HttpServletRequest request, HttpServletResponse response) {
		PedidoManual pedido = pmRepository.findByNumeroAndItem(numero, item);
		pmRepository.delete(pedido);
		return "redirect:/carteira-pcp/" + numero + "/" + item + "/acompanhamento";
	}

	@ResponseBody
	@PostMapping("/{numeroPedido}/{numItemPedido}/acompanhamento")
	public DataTable getTabelaFaltas(@PathVariable String numeroPedido, @PathVariable String numItemPedido) {
		AcompanhamentoPedido ap = repository.findByNumPedidoAndItemPed(numeroPedido, numItemPedido);
		return OrdemProducaoDAO.getGetEmpenhosPendentes(ap.getTodasOps());
	}

	@ResponseBody
	@PostMapping("/transferir-pedido")
	public boolean transferirPedido(String numPedOri, String numItemPedOri, String numPedDest, String numItemPedDest) {
		AcompanhamentoPedido apo = repository.findByNumPedidoAndItemPed(numPedOri, numItemPedOri);
		AcompanhamentoPedido apd = repository.findByNumPedidoAndItemPed(numPedDest, numItemPedDest);
		apo.setNumPedido(numPedDest);
		apo.setItemPed(numItemPedDest);
		String tempMemorando = apo.getMemorando();
		apo.setMemorando(apd.getMemorando());
		repository.save(apo);
		apd.setNumPedido(numPedOri);
		apd.setItemPed(numItemPedOri);
		apd.setMemorando(tempMemorando);
		repository.save(apd);
		return true;
	}
	
	@GetMapping("/{numeroPedido}/{numItemPedido}/acompanhamento/folha-de-rosto")
	public String getFolhaRosto(@PathVariable String numeroPedido, @PathVariable String numItemPedido, Model model) {
			ItemPedidoVenda itemPedido = VendasDAO.getItemPedido(numeroPedido, numItemPedido);
			model.addAttribute("itemPedido", itemPedido);
			AcompanhamentoPedido ap = repository.findByNumPedidoAndItemPed(numeroPedido, numItemPedido);
			model.addAttribute("ap", ap);
			OrdemProducao op = OrdemProducaoDAO.findOne(ap.getOps());
			model.addAttribute("op", op);
			return "carteiraPcpFolhaRosto";
	}
	
	@ResponseBody
	@PostMapping("/alteraTipoProduto")
	public void alteraTipoProduto(AcompanhamentoPedido tempAp) {
		AcompanhamentoPedido ap = repository.findById(tempAp.getId()).orElse(null);
		ap.setTipoProduto(tempAp.getTipoProduto());
		repository.save(ap);
	}
}