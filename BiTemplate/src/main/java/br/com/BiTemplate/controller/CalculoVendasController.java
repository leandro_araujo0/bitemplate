package br.com.BiTemplate.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.EstoqueDAO;
import br.com.BiTemplate.DAO.ProdutoDAO;
import br.com.BiTemplate.enums.UNIDADEVENDAS;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/utilitarios/calculo-vendas")
public class CalculoVendasController {
	
	@GetMapping("")
	public String calculoVendas() {
		return "utilitarios/calculoVendas";
	}
	
	@ModelAttribute("unidades")
	public List<String> getUnidades() {
		return ProdutoDAO.getUnidades();
	}
	
	@ModelAttribute("segmentos")
	public List<String> getSegmentos() {
		return ProdutoDAO.getSegmentos();
	}
	
	@ModelAttribute("classes")
	public List<String> getClasses() {
		return ProdutoDAO.getClasses();
	}
	
	@ModelAttribute("sistemas")
	public List<String> getSistemas() {
		return ProdutoDAO.getSistemas();
	}
	
	@ModelAttribute("caracs1")
	public List<String> getCaracs1() {
		return ProdutoDAO.getCaracs1();
	}
	
	@ModelAttribute("matsPrimas")
	public List<String> getMatsPrimas() {
		return ProdutoDAO.getMatsPrimas();
	}
	
	@ModelAttribute("caracs2")
	public List<String> getCaracs2() {
		return ProdutoDAO.getCaracs2();
	}
	
	@ModelAttribute("unidadesVendas")
	public UNIDADEVENDAS[] getMercados() {
		return UNIDADEVENDAS.values();
	}
	
	@ModelAttribute("grupos")
	public Iterable<String> getGrupo() {
		return ProdutoDAO.getGrupos();
	}
	
	@ResponseBody
	@PostMapping("/getCalculo")
	public DataTable getCalculoVendas(UNIDADEVENDAS unidadeVendas, String segmento, String classe, String sistema, String carac1, String matPrima, String carac2, 
			Long estoqueMinimo, String codigoProduto, String descricaoProduto, Long numClientesProduto, String grupo) {
		return EstoqueDAO.getEstoqueCalculoVendas(unidadeVendas, segmento, classe, sistema, carac1, matPrima, carac2, estoqueMinimo, codigoProduto, descricaoProduto, numClientesProduto, grupo);
	}

}
