package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/acompanhamento-pedido-producao")
public class AcompPedProdController {

	@RequestMapping("")
	public String acompanhamentoPedidoProducao() {
		return "acompPedProd";
	}
}
