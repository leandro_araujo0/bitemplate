package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.InventarioDAO;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/inventario")
public class InventarioController {
	
	@GetMapping("")
	public String inventario(Model model, String target) {
		model.addAttribute("target", target);
		return "inventario/monitor";
	}
	
	@ResponseBody
	@PostMapping("")
	public DataTable inventario(String armazem, String data, String target) {
		if (target != null && target.equals("blank")) {
			return new DataTable();
		} else {
			return InventarioDAO.getInventario(data, armazem);
		}
	}
	
	@GetMapping("/mestres-pendentes")
	public String mestresPendentes(Model model) {
		return "inventario/mestresPendentes";
	}
	
	@ResponseBody
	@PostMapping("/mestres-pendentes")
	public DataTable mestresPendentes(String data, String armazem) {
		return InventarioDAO.getMestresPendentes(data, armazem);
	}

}
