package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/fluxo-processo")
public class FluxoProcessoController {
	
	@GetMapping("")
	public String fluxoProcesso() {
		return "fluxoProcesso";
	}

}
