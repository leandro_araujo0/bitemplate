package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.BiTemplate.DAO.FaturamentoDAO;
import br.com.BiTemplate.enums.CLASPED;
import br.com.BiTemplate.enums.UNIDADE;

@Controller
@RequestMapping("/faturamento/pedidos")
public class PedidosFaturadosController {
	
	@GetMapping("")
	public String pedidosFaturados(Model model, CLASPED clasped, UNIDADE unidade, String dia, String mes, String ano, String dataDe, String dataAte, String codigoCliente) {
		model.addAttribute("itensFaturamento", FaturamentoDAO.getPedidosFaturados(clasped, unidade, dia, mes, ano, dataDe, dataAte, codigoCliente, null, null, null, null, null, null, null, null, null));
		return "faturamento/pedidosFaturados";
	}

}
