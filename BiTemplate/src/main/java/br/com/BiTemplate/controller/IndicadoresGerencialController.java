package br.com.BiTemplate.controller;

import java.math.BigDecimal;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import be.ceau.chart.BarChart;
import be.ceau.chart.color.Color;
import be.ceau.chart.data.BarData;
import be.ceau.chart.dataset.BarDataset;
import br.com.BiTemplate.DAO.FaturamentoDAO;
import br.com.BiTemplate.DAO.FinanceiroDAO;
import br.com.BiTemplate.DAO.ProducaoDAO;
import br.com.BiTemplate.DAO.VendasDAO;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.ChartDataAndLabels;
import br.com.BiTemplate.utils.DataUtils;

@Controller
@RequestMapping("/indicadores/gerencial")
public class IndicadoresGerencialController {
	
	private int ano;


	@GetMapping("")
	public String indicadores(Integer ano) {
		this.ano = (ano == null) ? 2019 : ano;
		return "indicadores/indicadoresGerencial";
	}
	
	@ResponseBody
	@GetMapping("/graficoGerencialMensal")
	public BarChart getGraficoGerencialOrtopedia(UNIDADE unidade) {
		BarData data = new BarData();
		BarDataset dataset = new BarDataset();
		ChartDataAndLabels cdl;
		
		cdl = ProducaoDAO.getProducaoAnual(ano, unidade);
		dataset = new BarDataset().setLabel("Produção").setBackgroundColor(Color.RED).setData(cdl.getChartData());
		data.addDataset(dataset);//PRODUCAO
		
		cdl = VendasDAO.getVendasAno(ano, unidade);
		dataset = new BarDataset().setLabel("Vendas").setBackgroundColor(Color.BLUE).setData(cdl.getChartData());
		data.addDataset(dataset);//VENDAS
		
		cdl = FaturamentoDAO.getFaturamentoAno(ano, unidade);
		dataset = new BarDataset().setLabel("Faturamento").setBackgroundColor(Color.GREEN).setData(cdl.getChartData());
		data.addDataset(dataset);//FATURAMENTO
		
		cdl = FinanceiroDAO.getBaixas(ano, unidade);
		dataset = new BarDataset().setLabel("Gastos").setBackgroundColor(Color.PURPLE).setData(cdl.getChartData());
		data.addDataset(dataset);//GASTOS
		
		data.setLabels(DataUtils.getLabelMeses());
		
//		data.addDataset(dataset);//RECEBIMENTO
		
		return new BarChart(data);
	}
	
	
	@ResponseBody
	@GetMapping("/graficoGerencialAnual")
	public BarChart graficoGerencialOrtopediaAnual(UNIDADE unidade) {
		BarData data = new BarData();
		BarDataset dataset = new BarDataset();
		ChartDataAndLabels cdl;
		
		//producao
		cdl = ProducaoDAO.getProducaoAnualHorizontal(ano, unidade);
		dataset = new BarDataset().addData((cdl.getChartData().size() > 0) ? (BigDecimal) cdl.getChartData().toArray()[0] : BigDecimal.ZERO).addBackgroundColor(Color.RED);
		data.addLabel("PRODUCAO");
		
		//vendas
		cdl = VendasDAO.getVendasAnoHorizontal(ano, unidade);
		dataset.addData((BigDecimal) cdl.getChartData().toArray()[0]).addBackgroundColor(Color.BLUE);
		data.addLabel("VENDAS");
		
		//faturamento
		cdl = FaturamentoDAO.getFaturamentoHorizontal(ano, unidade);
		dataset.addData((BigDecimal) cdl.getChartData().toArray()[0]).addBackgroundColor(Color.GREEN);
		data.addLabel("FATURAMENTO");
		
		//gastos
		cdl = FinanceiroDAO.getBaixasHorizontal(ano, unidade);
		dataset.addData((BigDecimal) cdl.getChartData().toArray()[0]).addBackgroundColor(Color.PURPLE);
		data.addLabel("GASTOS");
		
		//recebimento
		
		data.addDataset(dataset);
		return new BarChart(data).setHorizontal();
	}
}
