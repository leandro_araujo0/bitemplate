package br.com.BiTemplate.controller;

import java.math.BigDecimal;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.EstruturaDAO;

@Controller
@RequestMapping("/utilitarios/processamento")
public class ProcessamentoController {

	@ResponseBody
	@PostMapping("/getMRP")
	public Object getMRP(@RequestParam("codigoProdutos[]") String[] codigoProdutos,
			@RequestParam("qtdsBase[]") BigDecimal[] qtdsBase, Integer nivelMaximo, String dataOps, String dataPcs, boolean processarEstoque) {

		return EstruturaDAO.processa(codigoProdutos, qtdsBase, (nivelMaximo != null) ? nivelMaximo - 1 : null, null,
				dataOps, dataPcs, processarEstoque);
	}
}
