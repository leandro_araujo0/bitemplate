package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.VendasDAO;
import br.com.BiTemplate.enums.CLASPED;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.model.ParametroVenda;
import br.com.BiTemplate.utils.DataUtils;

@Controller
@RequestMapping("/pedidos-venda/relacao-vendas")
public class RelacaoVendasController {
	
	@GetMapping("")
	public String getRelacaoVendas(String clasped, String dia, String mes, String ano, String codigoProduto, String codigoCliente, Model model) {
		return "pedidosVenda/relacaoVendas";
	}
	
	@ResponseBody
	@PostMapping("")
	public DataTable postRelacaoVendas(String clasped, String dia, String mes, String ano, String codigoCliente, Model model, String dataDe, String dataAte, String cliente, String codigoGrupoProduto, UNIDADE unidade, String estado) {
		return VendasDAO.getVendas(ParametroVenda.PORPRODUTO, (clasped != null) ? CLASPED.valueOf(clasped) : null, dia, DataUtils.getMes(mes), ano, codigoCliente, unidade, dataDe, dataAte, cliente, null, codigoGrupoProduto, null, estado);
	}

}
