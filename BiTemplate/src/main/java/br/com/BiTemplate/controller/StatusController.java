package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.OrdemProducaoDAO;
import br.com.BiTemplate.DAO.SolicitacaoComprasDAO;
import br.com.BiTemplate.DAO.VendasDAO;

@Controller
@RequestMapping("/status")
public class StatusController {
	
	@ResponseBody
	@PostMapping("/op")
	public String statusOp(String op) {
		return OrdemProducaoDAO.getStatus(op);
	}
	
	@ResponseBody
	@PostMapping("/sc")
	public String statusSc(String sc) {
		return SolicitacaoComprasDAO.getStatus(sc);
	}
	
	@ResponseBody
	@GetMapping("/tem-novo-pedido")
	public boolean temNovoPedidos() {
		return VendasDAO.temNovoPedido();
	}

}
