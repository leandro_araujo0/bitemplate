package br.com.BiTemplate.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.FinanceiroDAO;
import br.com.BiTemplate.enums.TipoRelatorioRecebimento;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.model.Usuario;
import br.com.BiTemplate.repository.UsuarioRepository;

@Controller
@RequestMapping("/indicadores/recebimento/analitico")
public class IndicadoresRecebimentoAnaliticoController {
	
	@Autowired
	private UsuarioRepository ur;

	@GetMapping("/valor-a-receber")
	public String valorAReceber(UNIDADE unidade, Model model, Principal principal) {
		String name = principal.getName();
		Usuario usuario = ur.findById(name).orElse(null);
		
		for (int i = 0; i < usuario.getAuthorities().size(); i++) {
			String string = usuario.getAuthorities().get(i);
			if (string.equals("ADMIN") || string.contains(unidade.toString())) {
				break;
			} else if (i >= (usuario.getAuthorities().size() - 1)) {
				return "redirect:/";
			}
		}
		return "indicadores/indicadoresRecebimentoAnalitico";
	}
	
	@ResponseBody
	@PostMapping("/valor-a-receber")
	public DataTable valorAReceber(UNIDADE unidade, TipoRelatorioRecebimento tipo, Principal principal) {
		return FinanceiroDAO.getValorAReceberTotalAnalitico(unidade, tipo);
	}
	
	
	@GetMapping("/recebimentos")
	public String recebimento() {
		return "indicadores/indicadoresRecebimentoAnalitico";
	}
	
	@ResponseBody
	@PostMapping("/recebimentos")
	public DataTable recebimento(UNIDADE unidade, Model model, String periodo, String dataDe, String dataAte, String numTitulo, String codCliente, String pedVenda) {
		return FinanceiroDAO.getRecebimentoPorPeriodo(unidade, periodo, dataDe, dataAte, numTitulo, codCliente, pedVenda);
	}
	
}
