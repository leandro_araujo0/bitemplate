package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.OrdemProducaoDAO;
import br.com.BiTemplate.DAO.ProdutoDAO;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.model.DetalhesOP;
import br.com.BiTemplate.model.DetalhesProduto;
import br.com.BiTemplate.model.OrdemProducao;
import br.com.BiTemplate.model.Produto;

@Controller
@RequestMapping("/ordens-producao")
public class OrdemProducaoController {
	
	@GetMapping("/produto")
	public String opsProduto(String codigoProduto, Model model) {
		Produto produto = ProdutoDAO.findOne(codigoProduto);
		model.addAttribute("produto", produto);

		Iterable<OrdemProducao> ops = OrdemProducaoDAO.findByProduto(produto);
		model.addAttribute("ops", ops);
		
		return "ordensProducao/ops";
	}
	
	@GetMapping("")
	public String ops(Model model) {
		Iterable<OrdemProducao> ops = OrdemProducaoDAO.findAll();
		model.addAttribute("ops", ops);
		return "ordensProducao/ops";
	}
	
	@GetMapping("/{numOp}/detalhes")
	public String detalhes(@PathVariable String numOp, Model model) {
		OrdemProducao op = OrdemProducaoDAO.findOne(numOp);
		DetalhesOP detalhesOp = OrdemProducaoDAO.findDetalhes(op);
		
		model.addAttribute("detalhesOp", detalhesOp);
		DetalhesProduto detalhesProduto = ProdutoDAO.findDetalhesProduto(op.getProduto().getCodigo());
		model.addAttribute("detalhesProduto", detalhesProduto);
		return "ordensProducao/detalhesOp";
	}
	
	@ResponseBody
	@PostMapping("/{numOp}/detalhes")
	public DataTable detalhes(@PathVariable String numOp) {
		return OrdemProducaoDAO.getEmpenhosOP(null, numOp, null, null, null, null, null, true);
	}

}
