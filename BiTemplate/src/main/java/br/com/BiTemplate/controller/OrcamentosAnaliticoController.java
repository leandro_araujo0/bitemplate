package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.VendasDAO;
import br.com.BiTemplate.enums.StatusOrcamento;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/orcamentos/analitico")
public class OrcamentosAnaliticoController {

	@GetMapping("")
	public String orcamentosAnalitico() {
		return "orcamentosAnalitico";
	}
	
	@ResponseBody
	@PostMapping("")
	public DataTable orcamentosAnalitico(String codCliente, StatusOrcamento statusOrcamento) {
		return VendasDAO.getOrcamentosAnalitico(codCliente, statusOrcamento);
	}
	
}
