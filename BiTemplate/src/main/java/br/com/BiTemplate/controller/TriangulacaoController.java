package br.com.BiTemplate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.BiTemplate.model.Triangulacao;
import br.com.BiTemplate.repository.TriangulacaoRepository;

@Controller
@RequestMapping("/triangulacao")
public class TriangulacaoController {
	
	@Autowired
	private TriangulacaoRepository repository;
	
	@ModelAttribute(name = "triangulacoes")
	public Iterable<Triangulacao> triangulacoes() {
		return repository.findAll();
	}
	
	@GetMapping("")
	public String triangulacao() {
		return "triangulacao";
	}
	
	@GetMapping(value= { "/form" } )
	public String form(Triangulacao triangulacao, Model model) {
		return "triangulacaoForm";
	}
	
	@GetMapping(value= { "/{id}/form" })
	public String form(@PathVariable Integer id, Model model) {
		Triangulacao triangulacao = repository.findById(id).orElse(null);
		model.addAttribute("triangulacao", triangulacao);
		return "triangulacaoForm";
	}
	
	@PostMapping("")
	public String save(Triangulacao triangulacao) {
		repository.save(triangulacao);
		return "redirect:/triangulacao";
	}
	
	@GetMapping("/{id}/remover")
	public String remover(@PathVariable Integer id) {
		repository.deleteById(id);
		return "redirect:/triangulacao";
	}

}
