package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.EstoqueDAO;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/ponto-pedido")
public class PontoPedidoController {

	@GetMapping("")
	public String pontoPedido() {
		return "pontoPedido";
	}
	
	@ResponseBody
	@PostMapping("")
	public DataTable pontoPedido(UNIDADE unidade) {
		return EstoqueDAO.getItensPontoPedido(unidade);
	}
	
}
