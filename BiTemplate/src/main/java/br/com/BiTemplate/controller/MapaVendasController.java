package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.VendasDAO;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/indicadores/mapa-vendas")
public class MapaVendasController {
	
	@GetMapping("")
	public String mapaVendas() {
		return "indicadores/mapaVendas";
	}
	
	@ResponseBody
	@GetMapping("/getMapa")
	public DataTable getMapa(UNIDADE unidade) {
		return VendasDAO.getMapaVendas(unidade);
	}

}
