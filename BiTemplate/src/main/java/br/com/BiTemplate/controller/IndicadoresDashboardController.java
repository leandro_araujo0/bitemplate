package br.com.BiTemplate.controller;

import java.util.Calendar;
import java.util.Iterator;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import be.ceau.chart.BarChart;
import be.ceau.chart.data.BarData;
import be.ceau.chart.dataset.BarDataset;
import br.com.BiTemplate.DAO.DatabaseMetas;
import br.com.BiTemplate.DAO.VendasDAO;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.ChartDataAndLabels;
import br.com.BiTemplate.utils.ColorUtils;

@Controller
@RequestMapping("/indicadores/dashboard")
public class IndicadoresDashboardController {
	
	@GetMapping("")
	public String dashboard() {
		 return "indicadores/dashboard";
	}
	
	@ResponseBody
	@PostMapping("/grafico-dashboard-vendas")
	public BarChart graficoDashboardVendas() {
		BarData data = new BarData();
		ChartDataAndLabels cdl = VendasDAO.getDashBoardMes(Calendar.getInstance().get(Calendar.MONTH) + 1, Calendar.getInstance().get(Calendar.YEAR));
		BarDataset dataset = new BarDataset().setBorderWidth(2);
		cdl.getChartData().add(DatabaseMetas.getMetaRecalc(UNIDADE.ORTOPEDIA, Calendar.getInstance().get(Calendar.MONTH)));
		cdl.getChartLabels().add("META");
		
		dataset.setData(cdl.getChartData());

		for (Iterator<String> iterator = cdl.getChartLabels().iterator(); iterator.hasNext();) {
			dataset.addBackgroundColor(ColorUtils.random());
		}
		
		data.setLabels(cdl.getChartLabels());
		data.addDataset(dataset);
		
		return new BarChart(data);
	}
}
