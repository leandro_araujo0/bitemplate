package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.ComprasDAO;
import br.com.BiTemplate.DAO.EstoqueDAO;
import br.com.BiTemplate.DAO.EstruturaDAO;
import br.com.BiTemplate.DAO.OrdemProducaoDAO;
import br.com.BiTemplate.DAO.ProdutoDAO;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/historico-produto")
public class HistoricoProdutoController {
	
	@GetMapping("")
	public String historicoProduto(String target, Model model, String codigoProduto) {
		model.addAttribute("codigoProduto", codigoProduto);
		model.addAttribute("detalhesProduto", ProdutoDAO.findDetalhesProduto(codigoProduto));
		model.addAttribute("target", target);
		DataTable estoques = EstoqueDAO.getSaldoPorArmazem(codigoProduto);
		model.addAttribute("estoques", estoques);
		return "historicoProduto";
	}
	
	@ResponseBody
	@PostMapping("/ultimas-ops")
	public DataTable ultimasOps(String codigoProduto) {
		return OrdemProducaoDAO.getUltimasOps(codigoProduto, 150);
	}
	
	@ResponseBody
	@PostMapping("/ultimos-pcs")
	public DataTable ultimosPcs(String codigoProduto) {
		return ComprasDAO.getUltimosPcs(codigoProduto, 150);
	}
	
	
	@ResponseBody
	@PostMapping("/onde-e-usado")
	public DataTable ondeEUsado(String codigoProduto) {
		return EstruturaDAO.getOndeEUsado(codigoProduto);
	}
}
