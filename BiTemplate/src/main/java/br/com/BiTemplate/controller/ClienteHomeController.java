package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/area-cliente")
public class ClienteHomeController {
	
	@RequestMapping("")
	public String home() {
		return "areaCliente/clienteHome";
	}

}
