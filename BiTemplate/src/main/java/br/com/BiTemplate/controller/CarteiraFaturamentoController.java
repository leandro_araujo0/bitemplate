package br.com.BiTemplate.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.OrdemProducaoDAO;
import br.com.BiTemplate.DAO.VendasDAO;
import br.com.BiTemplate.model.AcompanhamentoPedido;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.model.ItemPedidoVenda;
import br.com.BiTemplate.model.PedidoFaturamento;
import br.com.BiTemplate.model.Usuario;
import br.com.BiTemplate.repository.AcompanhamentoPedidoRepository;
import br.com.BiTemplate.repository.PedidoFaturamentoRepository;
import br.com.BiTemplate.repository.UsuarioRepository;

@Controller
@RequestMapping("/carteira-faturamento")
public class CarteiraFaturamentoController {
	
	@Autowired
	private PedidoFaturamentoRepository pfr;
	
	@Autowired
	private UsuarioRepository ur;
	
	@Autowired
	private AcompanhamentoPedidoRepository apr;

	@ModelAttribute(name = "meses")
	public String[] meses() {
		String[] meses = { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" };
		return meses;
	}
	
	@ResponseBody
	@PostMapping("/adicionar")
	public boolean adicionarPedido(@RequestParam("pedidos[]")String[] pedidos) {
		for (int i = 0; i < pedidos.length; i++) {
			String numeroPedido = pedidos[i].split(";")[0];
			String itemPedido = pedidos[i].split(";")[1];
			String mes = pedidos[i].split(";")[2];
			String ano = "2021";
			PedidoFaturamento tempPf = pfr.findByNumeroPedidoAndItemPedido(numeroPedido, itemPedido).orElse(null);
			
			if (tempPf != null) {
				tempPf.setMes(mes);
				tempPf.setAno(ano);
				pfr.save(tempPf);
				continue;
			}
			
			PedidoFaturamento pf = new PedidoFaturamento();
			pf.setNumeroPedido(numeroPedido);
			pf.setItemPedido(itemPedido);
			pf.setMes(mes);
			pf.setAno(ano);
			pfr.save(pf);
		}
		return true;
	}

	@GetMapping("")
	public String carteiraFaturamento() {
		return "carteiraFaturamento";
	}

	@ResponseBody
	@PostMapping("")
	public DataTable getTabelaPedidos(String mes, String ano) {
		Iterable<PedidoFaturamento> tempPfs = pfr.findByAnoAndMes(ano, mes);
		LinkedList<PedidoFaturamento> pfs = new LinkedList<PedidoFaturamento>();
		Iterator<PedidoFaturamento> iter = tempPfs.iterator();
		while (iter.hasNext()) {
			PedidoFaturamento pf = iter.next();
			pfs.add(pf);
		}
		
		
		DataTable dataTable = VendasDAO.getPedidosFaturamento(pfs);
		LinkedList<Collection<Object>> data = (LinkedList<Collection<Object>>) dataTable.getData();
		
		for (Collection<Object> collection : data) {
			LinkedList<Object> tempCollection = (LinkedList<Object>) collection;
			PedidoFaturamento tempPf = new PedidoFaturamento();
			tempPf.setNumeroPedido((String) tempCollection.get(0));
			tempPf.setItemPedido((String) tempCollection.get(1));
			
			PedidoFaturamento pf = pfs.get(pfs.indexOf(tempPf));
			((LinkedList<Object>) collection).set(8, pf.getStatus());
			AcompanhamentoPedido ap = apr.findByNumPedidoAndItemPed(pf.getNumeroPedido(), pf.getItemPedido());
			((LinkedList<Object>) collection).set(6, pf.getUltAtualiz());
			((LinkedList<Object>) collection).set(12, pf.getDtExp());
			if (ap == null) {
				continue;
			}
			((LinkedList<Object>) collection).set(7, ap.getProcesso());
		}
		return dataTable;
	}
	
	@ResponseBody
	@PostMapping("/getItensPedido")
	public List<ItemPedidoVenda> getItensPedido(String numPedido) {
		List<ItemPedidoVenda> ipv = VendasDAO.getItensPedidoVenda(numPedido);
		return ipv;
	}
	
	@GetMapping("/remover")
	public String remover(String pedido, String item, Principal principal) {
		Usuario usuario = ur.findById(principal.getName()).orElse(null);
		if (!usuario.hasAuthority("ADMIN")) {
			return "redirect:/carteira-faturamento";
		}
		
		
		if (item != null && !item.equals("")) {
			PedidoFaturamento pf = pfr.findByNumeroPedidoAndItemPedido(pedido, item).orElse(null);
			pfr.delete(pf);
		} else {
			Iterable<PedidoFaturamento> pfs = pfr.findByNumeroPedido(pedido);
			pfr.deleteAll(pfs);
		}
		return "redirect:/carteira-faturamento";
	}
	
	@ResponseBody
	@PostMapping("/salvar")
	public void salvar(@RequestBody PedidoFaturamento tempPf) {
		PedidoFaturamento pf = pfr.findByNumeroPedidoAndItemPedido(tempPf.getNumeroPedido(), tempPf.getItemPedido()).orElse(null);
		pf.setStatus(tempPf.getStatus());
		pf.setUltAtualiz(tempPf.getUltAtualiz());
		pf.setLocalizacao(tempPf.getLocalizacao());
		pf.setDtExp(tempPf.getDtExp());
		pfr.save(pf);
	}
	
	@ResponseBody
	@GetMapping("/getPf/{numPedido}/{itemPedido}")
	public PedidoFaturamento getPf(@PathVariable String numPedido, @PathVariable String itemPedido) {
		PedidoFaturamento pf = pfr.findByNumeroPedidoAndItemPedido(numPedido, itemPedido).orElse(null);
		return pf;
	}
	
	@ResponseBody
	@PostMapping("/getEmpenhosPedidos")
	public DataTable getEmpenhosPedidos(String mes, String ano) {
		LinkedList<String> numeroPedidos = new LinkedList<>();
		ArrayList<PedidoFaturamento> pfs = (ArrayList<PedidoFaturamento>) pfr.findByAnoAndMes(ano, mes);
		for (PedidoFaturamento pedidoFaturamento : pfs) {
			numeroPedidos.add(pedidoFaturamento.getNumeroPedido());
		}
		
		LinkedList<String> ops = new LinkedList<>();
		Iterable<AcompanhamentoPedido> aps = apr.findByNumPedidoIn(numeroPedidos);
		
		Iterator<AcompanhamentoPedido> iter = aps.iterator();
		while(iter.hasNext()) {
			AcompanhamentoPedido ap = iter.next();
			ops.addAll(ap.getTodasOps());
		}
		
		String todasOps = "";
		for (String op : ops) {
			todasOps += op + ";";
		}
		
		DataTable empenhosOP = OrdemProducaoDAO.getEmpenhosOP(null, todasOps, null, null, null, null, null, false);
		
		for (Collection<Object> collection : empenhosOP.getData()) {
			LinkedList<Object> tempCollection = (LinkedList<Object>) collection;
			try {
				Iterator<AcompanhamentoPedido> iter2 = apr.findByAnyOp((String) tempCollection.get(0).toString().substring(0, 6)).iterator();
				String pedido = "";
				while (iter2.hasNext()) {
					AcompanhamentoPedido ap = iter2.next();
					pedido += ap.getNumPedido();
					if (iter2.hasNext()) {
						pedido += "/";
					}
				}
				((LinkedList<Object>) collection).set(22, pedido);
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
		}
		return empenhosOP;
	}
}
