package br.com.BiTemplate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.BiTemplate.DAO.ClienteDAO;
import br.com.BiTemplate.DAO.VendasDAO;
import br.com.BiTemplate.model.Cliente;
import br.com.BiTemplate.model.ProgramaVendas;
import br.com.BiTemplate.repository.ProgramaVendaRepository;

@Controller
@RequestMapping("/programa-vendas")
public class ProgramaVendasDetalhesController {
	
	@Autowired
	private ProgramaVendaRepository repository;

	@GetMapping("/{id}/detalhes")
	public String detalhes(@PathVariable Integer id, Model model) {
		List<Cliente> clientes = ClienteDAO.getClientes();
		model.addAttribute("clientes", clientes);
		ProgramaVendas programaVendas = repository.findById(id).orElse(null);

		VendasDAO.setConclusaoPrograma(programaVendas);
		
		model.addAttribute("programaVendas", programaVendas);
		return "programaVendasDetalhes";
	}

}
