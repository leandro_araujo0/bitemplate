package br.com.BiTemplate.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import be.ceau.chart.BarChart;
import be.ceau.chart.LineChart;
import be.ceau.chart.color.Color;
import be.ceau.chart.data.BarData;
import be.ceau.chart.dataset.BarDataset;
import br.com.BiTemplate.DAO.FinanceiroDAO;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.ChartDataAndLabels;

@Controller
@RequestMapping("/indicadores/recebimento")
public class IndicadoresRecebimentoController {
	
	@GetMapping("")
	public String indicadoresRecebimento() {
		return "indicadores/indicadoresRecebimento";
	}
	
	@ResponseBody
	@GetMapping("/recebimento-mensal")
	public BarChart getGraficoRecebimentoMensal(UNIDADE unidade) {
		ChartDataAndLabels cdl = FinanceiroDAO.getRecebimentosMensal(unidade);
		BarData data = new BarData();
		BarDataset dataset = new BarDataset().setBorderWidth(2).setBackgroundColor(new Color(0, 112, 192));
		dataset.setData(cdl.getChartData());
		data.setLabels(cdl.getChartLabels());
		data.addDataset(dataset);
		return new BarChart(data);
	}
	
	@ResponseBody
	@GetMapping("/valorAReceberTotal")
	public String getValorAReceberTotal(UNIDADE unidade) {
		return FinanceiroDAO.getValorAReceberTotal(unidade);
	}
	
	@ResponseBody
	@GetMapping("/valorAReceberAtrasado")
	public String getValorAReceberAtrasado(UNIDADE unidade) {
		return FinanceiroDAO.getValorAReceberAtrasado(unidade);
	}
	
	@ResponseBody
	@GetMapping("/valorRecebimentoMesAtual")
	public List<String> getValorRecebimentoMesAtual(UNIDADE unidade) {
		return FinanceiroDAO.getValorRecebimentoMesAtual(unidade);
	}
	
	@ResponseBody
	@GetMapping("/graficoInadimplenciaMensal")
	public LineChart getGraficoInadimplenciaMensal(UNIDADE unidade) {
		return FinanceiroDAO.getGraficoInadimplenciaMensal(unidade);
	}

}
