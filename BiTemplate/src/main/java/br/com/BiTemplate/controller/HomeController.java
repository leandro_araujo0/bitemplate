package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

	@GetMapping("/")
	public String home() {
		return "home";
	}
	
	@GetMapping("/acompanhamento")
	public String cadastros() {
		return "acompanhamento";
	}
//	
//	@GetMapping("/novo-ip")
//	public String novoIp() {
//		return "novoip";
//	}
	
	
}