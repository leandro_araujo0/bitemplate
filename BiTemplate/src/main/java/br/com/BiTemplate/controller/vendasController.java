package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.VendasDAO;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.model.ParametroVenda;

@Controller
@RequestMapping("/vendas")
public class vendasController {
	
	@GetMapping("")
	public String faturamento(String target, Model model) {
		model.addAttribute("target", target);
		return "vendas";
	}
	
	@ResponseBody
	@PostMapping("")
	public DataTable vendas(
			String target, 
			String pedidoDe, 
			String pedidoAte, 
			String produtoDe, 
			String produtoAte,
			String clienteDe,
			String clienteAte,
			String dataDe,
			String dataAte,
			String localDe,
			String localAte,
			String linhaProdutos, String estado) {
		if (target != null && target.equals("blank"))
			return new DataTable();
		else
//			return VendasDAO.getRelacaoVendas(pedidoDe, pedidoAte, produtoDe, produtoAte, clienteDe, clienteAte, dataDe, dataAte, localDe, localAte, linhaProdutos);
			return VendasDAO.getVendas(ParametroVenda.PORPRODUTO, null, null, null, null, null, null, dataDe, dataAte, null, null, null, linhaProdutos, estado);
	}

}
