package br.com.BiTemplate.controller;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import be.ceau.chart.BarChart;
import be.ceau.chart.DoughnutChart;
import be.ceau.chart.LineChart;
import be.ceau.chart.color.Color;
import be.ceau.chart.data.BarData;
import be.ceau.chart.data.DoughnutData;
import be.ceau.chart.data.LineData;
import be.ceau.chart.dataset.BarDataset;
import be.ceau.chart.dataset.DoughnutDataset;
import be.ceau.chart.dataset.LineDataset;
import be.ceau.chart.options.DoughnutOptions;
import be.ceau.chart.options.Title;
import br.com.BiTemplate.DAO.ClienteDAO;
import br.com.BiTemplate.DAO.VendasDAO;
import br.com.BiTemplate.model.CardVenda;
import br.com.BiTemplate.model.ChartDataAndLabels;
import br.com.BiTemplate.model.Cliente;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.utils.CalcUtils;
import br.com.BiTemplate.utils.ColorUtils;

@Controller
@RequestMapping("/clientes")
public class DetalhesClienteController {
	
	@GetMapping(value={"/{codCliente}/detalhes/{nomeCliente}", "/{codCliente}/detalhes"})
	public String detalhes(@PathVariable String codCliente, Model model) {
		Cliente cliente = ClienteDAO.findOne(codCliente);
		model.addAttribute("cliente", cliente);
		
		List<CardVenda> cardsVendas = VendasDAO.getVendasPorLinhaPorCliente(codCliente);
		model.addAttribute("cardsVendas", cardsVendas);
		
		return "clientes/detalhesCliente";
	}
	
	@GetMapping("/graficoVendas")
	@ResponseBody
	public LineChart graficoVendas(String codCliente) {
		Cliente cliente = ClienteDAO.findOne(codCliente);
		
		LineData data = new LineData();
		LineDataset dataset;
		ChartDataAndLabels cdl = VendasDAO.getHistoricoVendas(cliente);
		dataset = new LineDataset().setLineTension(0f).setBorderColor(ColorUtils.random()).setData(cdl.getChartData()).setLabel("VENDAS");
		data.addDataset(dataset).setLabels(cdl.getChartLabels());
		
		return new LineChart(data);
	}
	
	@GetMapping("/graficoEntregasCliente")
	@ResponseBody
	public DoughnutChart getGraficoEntregasCliente(Model model, String codCliente, Integer ano) {
		Cliente cliente = ClienteDAO.findOne(codCliente);
		Calendar date = Calendar.getInstance();
		date.add(Calendar.YEAR, -ano);
		
		DoughnutData data = new DoughnutData();
		DoughnutDataset dataset = new DoughnutDataset();
		
		ChartDataAndLabels cdl = VendasDAO.getIndicadoresEntregas(cliente, date.get(Calendar.YEAR));
		
		for (int i = 0; i < cdl.getChartData().size(); i++) {
			dataset.addData(CalcUtils.getPorcentagem((BigDecimal) cdl.getChartData().toArray()[i], cdl.getChartData()));
			
			if (cdl.getChartLabels().toArray()[i].equals("<30")) {
				dataset.addBackgroundColor(Color.BLUE);
			} else if (cdl.getChartLabels().toArray()[i].equals("<60")){
				dataset.addBackgroundColor(Color.GREEN);
			} else if (cdl.getChartLabels().toArray()[i].equals("<90")){
				dataset.addBackgroundColor(Color.YELLOW);
			} else {
				dataset.addBackgroundColor(Color.RED);
			}
		}
		
		data.addDataset(dataset).setLabels(cdl.getChartLabels());
		return new DoughnutChart(data).setOptions(new DoughnutOptions().setTitle(new Title().setText("Entregas " + date.get(Calendar.YEAR)).setFontSize(15)));
	}
	
	@GetMapping("")
	public String clientes(Model model) {
		List<Cliente> clientes = ClienteDAO.findAll();
		model.addAttribute("clientes", clientes);
		return "clientes/clientes";
	}
	
	@ResponseBody
	@GetMapping("/graficoProdutosMaisComprados")
	public BarChart getGraficoProdutosMaisComprados(String codCliente) {
		ChartDataAndLabels cdl = VendasDAO.getProdutosMaisComprados(codCliente);
		BarDataset dataset = new BarDataset().setData(cdl.getChartData());
		for (int i = 0; i < cdl.getChartLabelsToArray().length; i++) {
			dataset.addBackgroundColor(ColorUtils.random());
		}
		BarData data = new BarData().addDataset(dataset).setLabels(cdl.getChartLabels());
		BarChart chart = new BarChart(data).setHorizontal();
		return chart;
	}
	
	@ResponseBody
	@PostMapping("getMapaVendas")
	public DataTable mapaVendas(String codigoCliente) {
		return ClienteDAO.getMapaVendas(codigoCliente);
	}
	
	@ResponseBody
	@PostMapping("getVendasLinha")
	public List<DataTable> getVendasLinha(String codigoCliente) {
		return VendasDAO.getVendasLinha(codigoCliente);
	}

}
