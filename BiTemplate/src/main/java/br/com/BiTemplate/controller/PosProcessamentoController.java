package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.EstruturaDAO;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/utilitarios/pos-processamento")
public class PosProcessamentoController {

	@GetMapping("")
	public String posProcessamento() {
		return "utilitarios/posProcessamento";
	}
	
	@ResponseBody
	@PostMapping("/processar")
	public DataTable processa(@RequestParam("codigoProdutos[]") String[] codigoProdutos) {
		return EstruturaDAO.posProcessamento(codigoProdutos);
	}

}
