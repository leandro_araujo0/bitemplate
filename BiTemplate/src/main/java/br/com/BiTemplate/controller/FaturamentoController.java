package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.FaturamentoDAO;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/faturamento")
public class FaturamentoController {
	
	@GetMapping("")
	public String faturamento(String target, Model model) {
		model.addAttribute("target", target);
		return "faturamento";
	}
	
	@ResponseBody
	@PostMapping("")
	public DataTable faturamento(String target, String produto, String descricaoProduto, String codigoCliente, String nomeCliente, String dataDe, String dataAte, String lote,
			String pedidoDe, String pedidoAte, String nfInicial, String nfFinal, String naturezaCliente, UNIDADE unidade) {
		if (target != null && target.equals("blank"))
			return new DataTable();
		else
//			return FaturamentoDAO.getRelacaoFaturamento(produto, descricao, codigoCliente, nomeCliente, dataDe, dataAte, lote, pedidoDe, pedidoAte, nfInicial, nfFinal, naturezaCliente);
			return FaturamentoDAO.getPedidosFaturados(null, unidade, null, null, null, dataDe, dataAte, codigoCliente, descricaoProduto, lote, nomeCliente, naturezaCliente, pedidoDe, pedidoAte, nfInicial, nfFinal, produto);
	}

}
