package br.com.BiTemplate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.OrdemProducaoDAO;
import br.com.BiTemplate.model.AcompanhamentoPedido;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.repository.AcompanhamentoPedidoRepository;

@Controller
@RequestMapping("/relacao-empenhos")
public class RelacaoEmpenhosController {
	
	@Autowired
	private AcompanhamentoPedidoRepository apr;
	
	@GetMapping("")
	public String relacaoEmpenhos(Boolean detalhes, Model model, String target) {
		model.addAttribute("target", target);
		return "relacaoEmpenhos";
	}
	
	@ResponseBody
	@PostMapping("")
	public DataTable relacaoEmpenhos(String codigoProduto, String op, String lote, String armazem, String emissao, String usuario, String target, String pvs) {
		if (pvs != null && !pvs.equals("")) {
			for (String pv : pvs.replace(" ", ";").split(";")) {
				Iterable<AcompanhamentoPedido> aps = apr.findByNumPedido(pv);
				for (AcompanhamentoPedido ap : aps) {
					List<String> ops = ap.getTodasOps();
					for (String tempOp : ops) {
						if (op.length() > 0) {
							op += ";" + tempOp;
						} else {
							op += tempOp;
						}
					}
				}
			}
		}
		
		
		if (target != null && target.equals("blank")) {
			return new DataTable();
		} else {
			return OrdemProducaoDAO.getEmpenhosOP(codigoProduto, op, lote, armazem, emissao, usuario, pvs, false);
		}
	}

}
