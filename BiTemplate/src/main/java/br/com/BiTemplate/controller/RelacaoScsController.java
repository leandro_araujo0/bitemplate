package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.SolicitacaoComprasDAO;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/relacao-scs")
public class RelacaoScsController {
	
	@GetMapping("")
	public String relacaoScs(String target, Model model) {
		model.addAttribute("target", target);
		return "relacaoScs";
	}
	
	@ResponseBody
	@PostMapping("")
	public DataTable relacaoScs(String solicitante, String codigoProduto, String emissao, String fornecedor, String codigoFornecedor, String target, String numero, boolean abertos) {
		if (target == null || !target.equals("blank")) {
			return SolicitacaoComprasDAO.getRelacaoScs(codigoProduto, emissao, fornecedor, codigoFornecedor, numero, solicitante, abertos);
		} else {
			return new DataTable();
		}
	}

}
