package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.OrdemProducaoDAO;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/relacao-ops")
public class RelacaoOpsController {
	
	@GetMapping("")
	public String relacaoOps(Model model, String target) {
		model.addAttribute("target", target);
		model.addAttribute("opsFilhas", OrdemProducaoDAO.temOpsFilhas());
		return "relacaoOps";
	}
	
	@ResponseBody
	@PostMapping("")
	public DataTable relacaoOps(String usuario, String emissaoDe, String emissaoAte, String sequencia, String armazem, String numero, String codigoProduto, String target, String descricaoProduto, String status, boolean opsFilhas, String lote, String situacao) {
		if (opsFilhas) {
			return OrdemProducaoDAO.getOpsFilhas();
		}
		if (target != null && target.equals("blank")) {
			return new DataTable();
		} else {
			return OrdemProducaoDAO.getRelacaoOps(usuario, emissaoDe, emissaoAte, sequencia, armazem, numero, codigoProduto, descricaoProduto, status, lote, situacao);
		}
	}
}
