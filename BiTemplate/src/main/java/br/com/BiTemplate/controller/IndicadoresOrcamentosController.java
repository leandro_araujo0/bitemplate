package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import be.ceau.chart.BarChart;
import be.ceau.chart.data.BarData;
import br.com.BiTemplate.DAO.VendasDAO;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.ChartDataAndLabels;

@Controller
@RequestMapping("/indicadores/orcamentos")
public class IndicadoresOrcamentosController {
	
	@GetMapping("")
	public String indicadoresOrcamentos() {
		return "indicadores/indicadoresOrcamentos";
	}
	
	@ResponseBody
	@PostMapping("/dashboard")
	public BarChart getDashBoard(UNIDADE unidade) {
		ChartDataAndLabels cdl = VendasDAO.getDashBoardOrcamentos(unidade);
		BarData data = new BarData();
		data.setLabels(cdl.getChartLabels());
		data.setDatasets(cdl.getChartDatasets());
		return new BarChart(data);
	}

}
