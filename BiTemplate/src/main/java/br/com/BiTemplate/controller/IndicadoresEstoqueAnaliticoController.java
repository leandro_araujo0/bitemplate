package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.EstoqueDAO;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/indicadores/estoques/analitico")
public class IndicadoresEstoqueAnaliticoController {
	
	@GetMapping("")
	public String indicadoresEstoqueAnalitico(UNIDADE unidade) {
		return "indicadoresEstoqueAnalitico";
	}
	
	@ResponseBody
	@PostMapping("")
	public DataTable indicadoresEstoquesAnaliticoTabela(UNIDADE unidade, String local) {
		return EstoqueDAO.getItensEstoque(unidade, local);
	}

}
