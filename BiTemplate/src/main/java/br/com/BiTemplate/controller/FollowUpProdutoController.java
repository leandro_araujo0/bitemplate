package br.com.BiTemplate.controller;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.ProdutoDAO;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.model.DetalhesProduto;
import br.com.BiTemplate.model.FollowUpProduto;
import br.com.BiTemplate.repository.FollowUpProdutoRepository;

@Controller
@RequestMapping("/follow-up-produto")
public class FollowUpProdutoController {
	
	@Autowired
	private FollowUpProdutoRepository fupr;
	
	@GetMapping("")
	public String followUpProduto(FollowUpProduto fup) {
		return "followUpProduto";
	}
	
	@ResponseBody
	@PostMapping("")
	public DataTable tabelaProdutos() {
		Iterable<FollowUpProduto> tempFup = fupr.findAll();
		DataTable table = ProdutoDAO.getAcompanhamentoProduto(tempFup);
		
		LinkedList<FollowUpProduto> fups = new LinkedList<FollowUpProduto>();
		Iterator<FollowUpProduto> iter = tempFup.iterator();
		while (iter.hasNext()) {
			fups.add(iter.next());
		}
		
		LinkedList<Collection<Object>> data = (LinkedList<Collection<Object>>) table.getData();
		for (Collection<Object> collection : data) {
			String codigo = ((String) ((LinkedList<Object>) collection).get(0)).replaceAll("\\<.*?\\>", "");
			
			FollowUpProduto fup = fups.get(fups.indexOf(new FollowUpProduto().setCodigoProduto(codigo)));
			
			((LinkedList<Object>) collection).add(fup.getMemorando());
			}
		
		return table;
	}
	
	@PostMapping("/adicionar")
	public String adicionarProduto(FollowUpProduto fup) {
		fup.setCodigoProduto(fup.getCodigoProduto().trim());
		fupr.save(fup);
		return "redirect:/follow-up-produto";
	}
	
	@GetMapping("/acompanhamento")
	public String acompanhamento(String produto, Model model) {
		DetalhesProduto dp = ProdutoDAO.findDetalhesProduto(produto);
		FollowUpProduto fup = fupr.findById(produto).orElse(null);
		
		model.addAttribute("dp", dp);
		model.addAttribute("fup", fup);
		
		return "followUpProdutoAcompanhamento";
	}
	
	@ResponseBody
	@PostMapping("/salvar")
	public boolean salvar(FollowUpProduto fup) {
		fupr.save(fup);
		return true;
	}
	
	@GetMapping("/remover")
	public String remover(String produto) {
		fupr.deleteById(produto);
		return "redirect:/follow-up-produto";
	}

}
