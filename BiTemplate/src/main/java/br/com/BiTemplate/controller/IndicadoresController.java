package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/indicadores")
public class IndicadoresController {

	@GetMapping("")
	public String indicadores() {
		return "indicadores/indicadores";
	}

}
