package br.com.BiTemplate.controller;

import java.math.BigDecimal;
import java.util.Calendar;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import be.ceau.chart.BarChart;
import be.ceau.chart.color.Color;
import be.ceau.chart.data.BarData;
import be.ceau.chart.dataset.BarDataset;
import br.com.BiTemplate.DAO.FaturamentoDAO;
import br.com.BiTemplate.enums.CLASPED;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.ChartDataAndLabels;
import br.com.BiTemplate.utils.ColorUtils;
import br.com.BiTemplate.utils.DataUtils;

@Controller
@RequestMapping("/indicadores/faturamento")
public class IndicadoresFaturamentoController {
	
	private int ano;


	@ResponseBody
	@GetMapping("/graficoFaturamentoOrtopedia")
	public BarChart getGraficoFaturamentoOrtopedia() {
		ChartDataAndLabels cdl = FaturamentoDAO.getFaturamentoAno(DataUtils.getLastYear(ano).get(Calendar.YEAR), UNIDADE.ORTOPEDIA);
		BarDataset dataset = new BarDataset().setBorderWidth(2).setData(cdl.getChartData()).setLabel("Faturamento " + DataUtils.getLastYear(ano).get(Calendar.YEAR)).setBackgroundColor(Color.DARK_RED);
		BarData data = new BarData().setLabels(DataUtils.getLabelMeses()).addDataset(dataset);
		cdl = FaturamentoDAO.getFaturamentoAno(DataUtils.getInstance(ano).get(Calendar.YEAR), UNIDADE.ORTOPEDIA);
		dataset = new BarDataset().setBorderWidth(2).setData(cdl.getChartData()).setLabel("Faturamento " + DataUtils.getInstance(ano).get(Calendar.YEAR)).setBackgroundColor(Color.DARK_SALMON);
		data.addDataset(dataset);
		BarChart graficoFaturamentoOrtopedia = new BarChart(data);
		return graficoFaturamentoOrtopedia;
	}
	
	@ResponseBody
	@GetMapping("/graficoFaturamentoEquipamentos")
	public BarChart getGraficoFaturamentoEquipamentos() {
		ChartDataAndLabels cdl = FaturamentoDAO.getFaturamentoAno(DataUtils.getLastYear(ano).get(Calendar.YEAR), UNIDADE.EQUIPAMENTOS);
		BarDataset dataset = new BarDataset().setBorderWidth(2).setData(cdl.getChartData()).setLabel("Faturamento " + DataUtils.getLastYear(ano).get(Calendar.YEAR)).setBackgroundColor(Color.DARK_CYAN);
		BarData data = new BarData().setLabels(DataUtils.getLabelMeses()).addDataset(dataset);
		cdl = FaturamentoDAO.getFaturamentoAno(DataUtils.getInstance(ano).get(Calendar.YEAR), UNIDADE.EQUIPAMENTOS);
		dataset = new BarDataset().setBorderWidth(2).setData(cdl.getChartData()).setLabel("Faturamento " + DataUtils.getInstance(ano).get(Calendar.YEAR)).setBackgroundColor(Color.LIGHT_SKY_BLUE);
		data.addDataset(dataset);
		BarChart graficoFaturamentoEquipamentos = new BarChart(data);
		return graficoFaturamentoEquipamentos;
	}
	
	@ResponseBody
	@GetMapping("/graficoFaturamentoMensal")
	public BarChart getGraficoFaturamentoMensal() {
		ChartDataAndLabels cdl = FaturamentoDAO.getFaturamentoMensal();
		BarDataset dataset = new BarDataset().setBorderWidth(2);
		for (int i = 0; i < cdl.getChartData().toArray().length; i++) {
			BigDecimal chartData = (BigDecimal) cdl.getChartData().toArray()[i];
			String chartLabel = (String) cdl.getChartLabels().toArray()[i];
			
			dataset.addData(chartData);
			
			if (chartLabel != null) {
				if (chartLabel.equals("REPOSICAO")) {
					dataset.addBackgroundColor(new Color(0, 112, 192));
				}
				if (chartLabel.equals("CAIXAS")) {
					dataset.addBackgroundColor(new Color(255, 255, 0));
				}
				if (chartLabel.equals("EXPORTACAO")) {
					dataset.addBackgroundColor(new Color(255, 0, 0));
				}
				if (chartLabel.equals("EQUIPAMENTOS")) {
					dataset.addBackgroundColor(new Color(0, 176, 80));
				}
			}
		}
		BarData data = new BarData().addDataset(dataset).setLabels(cdl.getChartLabels());
		BarChart graficoFaturamentoMensal = new BarChart(data).setHorizontal();
		return graficoFaturamentoMensal;
	}
	
	@ResponseBody
	@GetMapping("/graficoFaturamentoDetalhadoCaixas")
	public BarChart getGraficoFaturamentoDetalhadoCaixas() {
		ChartDataAndLabels cdl = FaturamentoDAO.getFaturamentoDiario(UNIDADE.ORTOPEDIA, CLASPED.CAIXAS);
		BarDataset dataset = new BarDataset().setBorderWidth(2).setData(cdl.getChartData()).addBackgroundColor(new Color(255, 255, 0));
		BarData data = new BarData().setLabels(cdl.getChartLabels()).addDataset(dataset);
		BarChart graficoFaturamentoDetalhadoCaixas = new BarChart(data);
		return graficoFaturamentoDetalhadoCaixas;
	}
	
	@ResponseBody
	@GetMapping("/graficoFaturamentoDetalhadoReposicao")
	public BarChart getGraficoFaturamentoDetalhadoReposicao() {
		ChartDataAndLabels cdl = FaturamentoDAO.getFaturamentoDiario(UNIDADE.ORTOPEDIA, CLASPED.REPOSICAO);
		BarDataset dataset = new BarDataset().setBorderWidth(2).setData(cdl.getChartData()).addBackgroundColor(new Color(0, 112, 192));
		BarData data = new BarData().setLabels(cdl.getChartLabels()).addDataset(dataset);
		BarChart graficoFaturamentoDetalhadoReposicao = new BarChart(data);
		return graficoFaturamentoDetalhadoReposicao;
	}
	
	@ResponseBody
	@GetMapping("/graficoFaturamentoDetalhadoExportacao")
	public BarChart getGraficoFaturamentoDetalhadoExportacao() {
		ChartDataAndLabels cdl = FaturamentoDAO.getFaturamentoDiario(UNIDADE.ORTOPEDIA, CLASPED.EXPORTACAO);
		BarDataset dataset = new BarDataset().setBorderWidth(2).setData(cdl.getChartData()).addBackgroundColor(new Color(255, 0, 0));
		BarData data = new BarData().setLabels(cdl.getChartLabels()).addDataset(dataset);
		BarChart graficoFaturamentoDetalhadoExportacao = new BarChart(data);
		return graficoFaturamentoDetalhadoExportacao;
	}
	
	@ResponseBody
	@GetMapping("/graficoFaturamentoDetalhadoEquipamentos")
	public BarChart getGraficoFaturamentoDetalhadoEquipamentos() {
		ChartDataAndLabels cdl = FaturamentoDAO.getFaturamentoDiario(UNIDADE.ORTOPEDIA, CLASPED.EQUIPAMENTOS);
		BarDataset dataset = new BarDataset().setBorderWidth(2).setData(cdl.getChartData()).addBackgroundColor(new Color(0, 176, 80));
		BarData data = new BarData().setLabels(cdl.getChartLabels()).addDataset(dataset);
		BarChart graficoFaturamentoDetalhadoEquipamentos = new BarChart(data);
		return graficoFaturamentoDetalhadoEquipamentos;
	}
	
	@ResponseBody
	@GetMapping("/graficoFaturamentoHorizontal")
	public BarChart getGraficoFaturamentoHorizontal() {
		ChartDataAndLabels cdl = FaturamentoDAO.getFaturamentoHorizontal(DataUtils.getInstance(ano).get(Calendar.YEAR));
		BarDataset dataset = new BarDataset().setBorderWidth(2).setData(cdl.getChartData()).setLabel(String.valueOf(ano));
		for (int i = 0; i < cdl.getChartData().size(); i++) {
			dataset.addBackgroundColor(ColorUtils.random());
		}
		BarData data = new BarData().setLabels(cdl.getChartLabels()).addDataset(dataset);
		BarChart graficoFaturamentoHorizontal = new BarChart(data).setHorizontal();
		return graficoFaturamentoHorizontal;
	}
	
	
	@GetMapping("")
	public String faturamento(Model model, Integer ano) {
		this.ano = (ano == null) ? Calendar.getInstance().get(Calendar.YEAR) : ano;
		model.addAttribute("ano", this.ano);
		return "indicadores/indicadoresFaturamento";
	}

}
