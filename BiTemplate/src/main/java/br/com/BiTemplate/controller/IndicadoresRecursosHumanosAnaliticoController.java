package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.RecursosHumanosDAO;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/indicadores/recursos-humanos/analitico")
public class IndicadoresRecursosHumanosAnaliticoController {

	@GetMapping("")
	public String indicadoresRecursosHumanosAnalitico() {
		return "indicadoresRecursosHumanosAnalitico";
	}
	
	
	@ResponseBody
	@PostMapping("")
	public DataTable indicadoresRecursosHumanosAnalitico(UNIDADE unidade, String tipo, String periodo, String dataDe, String dataAte) {
		return RecursosHumanosDAO.getFuncionarios(unidade, tipo, periodo, dataDe, dataAte);
	}
	
}
