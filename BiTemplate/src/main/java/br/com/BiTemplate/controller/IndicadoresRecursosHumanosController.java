package br.com.BiTemplate.controller;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import be.ceau.chart.BarChart;
import be.ceau.chart.data.BarData;
import be.ceau.chart.dataset.BarDataset;
import br.com.BiTemplate.DAO.RecursosHumanosDAO;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.ChartDataAndLabels;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.utils.ColorUtils;

@Controller
@RequestMapping("/indicadores/recursos-humanos")
public class IndicadoresRecursosHumanosController {

	@GetMapping("")
	public String indicadoresRecursosHumanos() {
		return "indicadores/indicadoresRecursosHumanos";
	}
	
	@ResponseBody
	@GetMapping("/graficoEscolaridade")
	public BarChart graficoEscolaridade(UNIDADE unidade) {
		ChartDataAndLabels cdl = RecursosHumanosDAO.getGraficoEscolaridade(unidade);
		
		BarDataset dataset = new BarDataset().setData(cdl.getChartData()).setBorderWidth(2);
		if (unidade.equals(UNIDADE.ORTOPEDIA)) {
			dataset.setBackgroundColor(ColorUtils.fromHex("0496ff"));
		} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
			dataset.setBackgroundColor(ColorUtils.fromHex("d81159"));
		}
		BarData data = new BarData().addDataset(dataset).setLabels(cdl.getChartLabelsToArray());
		
		BarChart graficoRecursosHumanos = new BarChart(data);
		graficoRecursosHumanos.setHorizontal();
		return graficoRecursosHumanos;
	}
	
	@ResponseBody
	@GetMapping("/graficoEscolaridadeMensal")
	public BarChart getGraficoEscolaridadeMensal(UNIDADE unidade) {
		DataTable table = RecursosHumanosDAO.getGraficoEscolaridadeMensal(unidade);
		
		String escolaridade = null;
		
		BarData data = new BarData();
		BarDataset dataset = new BarDataset();
		
		int i = 0;
		for (Iterator<Collection<Object>> iterator = table.getData().iterator(); iterator.hasNext();) {
			LinkedList<Object> linha = (LinkedList<Object>) iterator.next();
			
			if (escolaridade != null && (!escolaridade.equals(linha.get(1)) || !iterator.hasNext())) {
				if (!iterator.hasNext()) {
					dataset.addData(BigDecimal.valueOf(Double.parseDouble((String) linha.get(2))));
				}
				
				switch (i) {
				case 0:
					dataset.setBackgroundColor(ColorUtils.fromHex("43bccd"));
					break;
				case 1:
					dataset.setBackgroundColor(ColorUtils.fromHex("662e9b"));
					break;
				case 2:
					dataset.setBackgroundColor(ColorUtils.fromHex("ea3546"));
					break;
				case 3:
					dataset.setBackgroundColor(ColorUtils.fromHex("f86624"));
					break;
				case 4:
					dataset.setBackgroundColor(ColorUtils.fromHex("f9c80e"));
					break;
				case 5:
					dataset.setBackgroundColor(ColorUtils.fromHex("0ead69"));
					break;
				case 6:
					dataset.setBackgroundColor(ColorUtils.fromHex("3bceac"));
					break;
				case 7:
					dataset.setBackgroundColor(ColorUtils.fromHex("1d3461"));
					break;
				}
				
				i++;
				dataset.setLabel(escolaridade);
				data.addDataset(dataset);
				dataset = new BarDataset();
			}
			
			dataset.addData(BigDecimal.valueOf(Double.parseDouble((String) linha.get(2))));
			
			escolaridade = (String) linha.get(1);
		}
		
		LinkedList<String> labels = new LinkedList<String>();
		for (Iterator<Collection<Object>> iterator = table.getData().iterator(); iterator.hasNext();) {
			LinkedList<Object> linha = (LinkedList<Object>) iterator.next();
			if (!labels.contains(linha.get(0))) {
				labels.add((String) linha.get(0));
			}
		}
		
		data.setLabels(labels);
		return new BarChart(data);
	}
	
	@ResponseBody
	@PostMapping("/tabelaRotatividade")
	public DataTable tabelaRotatividade(UNIDADE unidade) {
		return RecursosHumanosDAO.getTabelaRotatividade(unidade);
	}
}
