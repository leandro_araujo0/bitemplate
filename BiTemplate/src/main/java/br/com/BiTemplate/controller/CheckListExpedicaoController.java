package br.com.BiTemplate.controller;

import java.util.LinkedList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.FaturamentoDAO;
import br.com.BiTemplate.model.CheckListExpedicao;

@Controller
@RequestMapping("/check-list-expedicao")
public class CheckListExpedicaoController {
	
	@GetMapping("")
	public String checkList(Model model, String target, String documento, String numPedido) {
		if (target != null && target.equals("blank")) {
			model.addAttribute("target", target);
		} else {
			CheckListExpedicao checkList = FaturamentoDAO.getCheckList(documento, numPedido);
			model.addAttribute("checkList", checkList);
		}
		return "checkListExpedicao";
	}
	
	@ResponseBody
	@GetMapping("/{numPedido}")
	public LinkedList<String> nfsPedido(@PathVariable String numPedido) {
		LinkedList<String> nfsPedido = FaturamentoDAO.getNfsPedido(numPedido);
		return nfsPedido;
	}

}
