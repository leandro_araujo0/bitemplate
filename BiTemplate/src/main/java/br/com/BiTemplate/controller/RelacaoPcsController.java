package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.ComprasDAO;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.utils.DataUtils;

@RequestMapping("/relacao-pcs")
@Controller
public class RelacaoPcsController {
	
	@GetMapping("")
	public String relacaoCompras(Model model, String target) {
		model.addAttribute("target", target);
		return "pedidosCompra/relacaoPcs";
	}
	
	@ResponseBody
	@PostMapping("")
	public DataTable tabelaCompras(UNIDADE unidade, String mes, String ano, String codigoProduto, String op, String target, String numero, String emissaoDe, String emissaoAte, String nomeFornecedor, String codigoFornecedor, String sc, String abertos, String comprador) {
		if (target != null && target.equals("blank")) {
			return new DataTable();
		} else {
			return ComprasDAO.getRelacaoCompras(unidade, DataUtils.getMes(mes), ano, codigoProduto, op, numero, emissaoDe, emissaoAte, nomeFornecedor, codigoFornecedor, sc, abertos, comprador);
		}
	}

}
