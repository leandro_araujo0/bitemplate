package br.com.BiTemplate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.BiTemplate.repository.UsuarioRepository;

@Controller
@RequestMapping("/admin/usuarios")
public class AdminUsuariosController {
	
	@Autowired
	private UsuarioRepository repository;
	
	@RequestMapping("")
	public String usuarios(Model model) {
		model.addAttribute("usuarios", repository.findAll());
		return "adminUsuarios";
	}

}
