package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/indicadores/resultados")
public class IndicadoresResultadosController {
	
	@GetMapping("")
	public String indicadoresResultados() {
		return "indicadores/indicadoresResultados";
	}

}
