package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import be.ceau.chart.BarChart;
import be.ceau.chart.data.BarData;
import be.ceau.chart.dataset.BarDataset;
import br.com.BiTemplate.DAO.EntradasDAO;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.ChartDataAndLabels;
import br.com.BiTemplate.utils.ColorUtils;

@Controller
@RequestMapping("/indicadores/nfs-entrada")
public class IndicadoresNfsEntradaController {
	
	@GetMapping("")
	public String nfsEntrada() {
		return "indicadoresNfsEntrada";
	}

	@ResponseBody
	@GetMapping("/nfsEntradaMensal")
	public BarChart nfsEntradaMensal(UNIDADE unidade) {
		ChartDataAndLabels cdl = EntradasDAO.getEntradasMensal(unidade);
		BarData data = new BarData();
		data.setLabels(cdl.getChartLabels());
		BarDataset dataset = new BarDataset();
		dataset.setData(cdl.getChartData());
		dataset.setBackgroundColor(ColorUtils.random());
		data.addDataset(dataset);
		return new BarChart(data);
	}
	
}
