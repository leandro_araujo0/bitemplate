package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/xpto")
public class FollowUpOpController {
	
	@GetMapping("")
	 public String followUpOps(String target, Model model) {
		model.addAttribute("target", target);
		return "followUpOp";
	 }
	
}

