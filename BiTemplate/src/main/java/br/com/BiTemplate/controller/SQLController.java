package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/sql")
public class SQLController {
	
	@GetMapping("")
	public String sql() {
		return "sql";
	}
	
	@ResponseBody
	@PostMapping("/execute")
	public String execute(String sql) {
		return sql;
	}
}
