package br.com.BiTemplate.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.Principal;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import be.ceau.chart.BarChart;
import be.ceau.chart.color.Color;
import be.ceau.chart.data.BarData;
import be.ceau.chart.dataset.BarDataset;
import br.com.BiTemplate.DAO.DatabaseMetas;
import br.com.BiTemplate.DAO.VendasDAO;
import br.com.BiTemplate.configuration.ConfigClass;
import br.com.BiTemplate.enums.CLASPED;
import br.com.BiTemplate.enums.StatusOrcamento;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.ChartDataAndLabels;
import br.com.BiTemplate.model.PedidoVenda;
import br.com.BiTemplate.model.Usuario;
import br.com.BiTemplate.repository.UsuarioRepository;
import br.com.BiTemplate.utils.ColorUtils;
import br.com.BiTemplate.utils.DataUtils;

@Controller
@RequestMapping("/indicadores/vendas")
public class IndicadoresVendasController {
	
	private Integer ano;
	@Autowired
	private UsuarioRepository ur;
	
	@ModelAttribute("taxaDolar")
	private BigDecimal getTaxaDolar() {
		return BigDecimal.valueOf(ConfigClass.taxaDolar).setScale(2, RoundingMode.HALF_EVEN);
	}

	@ResponseBody
	@GetMapping("/tabelaVendasDiaria")
	public List<PedidoVenda> getTabelaVendasDiaria(Principal principal) {
		Usuario usuario = ur.findById(principal.getName()).orElse(null);
		List<String> authorities = usuario.getAuthorities();
		List<PedidoVenda> pedidosVenda = VendasDAO.getItensPedidosVendaDia(authorities);
		return pedidosVenda;
	}

	@ResponseBody
	@GetMapping("/graficoVendasOrtopedia")
	public BarChart getGraficoVendasOrtopedia(boolean metas) {
		ChartDataAndLabels cdl = VendasDAO.getVendasAno(String.valueOf(DataUtils.getLastYear(ano).get(Calendar.YEAR)), CLASPED.MERCADOINTERNO);
		BarDataset dataset = new BarDataset().setData(cdl.getChartData()).setLabel("Vendas " + DataUtils.getLastYear(ano).get(Calendar.YEAR)).setBorderWidth(2).setBorderColor(Color.DARK_CYAN).setBackgroundColor(Color.DARK_CYAN);
		BarData data = new BarData().addDataset(dataset).setLabels(DataUtils.getLabelMeses());
		cdl = VendasDAO.getVendasAno(String.valueOf(DataUtils.getInstance(ano).get(Calendar.YEAR)), CLASPED.MERCADOINTERNO);
		dataset = new BarDataset().setData(cdl.getChartData()).setLabel("Vendas " + DataUtils.getInstance(ano).get(Calendar.YEAR)).setBorderWidth(2).setBorderColor(Color.LIGHT_SKY_BLUE).setBackgroundColor(Color.LIGHT_SKY_BLUE);
		data.addDataset(dataset);
		
		if (metas) {
			if (this.ano == 2019) {
				ChartDataAndLabels cdlMetasRecalc = DatabaseMetas.getMetasRecalc(UNIDADE.ORTOPEDIA);
				dataset = new BarDataset().setData(cdlMetasRecalc.getChartData()).setLabel("Meta 2019").setBackgroundColor(Color.TAN);
				data.addDataset(dataset);
			}
		}
		
		BarChart graficoVendasOrtopedia = new BarChart(data);
		return graficoVendasOrtopedia;
	}
	
	@ResponseBody
	@GetMapping("/graficoVendasEquipamentos")
	public BarChart getGraficoVendasEquipamentos(boolean metas) {
		ChartDataAndLabels cdl = VendasDAO.getVendasAno(String.valueOf(DataUtils.getLastYear(ano).get(Calendar.YEAR)), CLASPED.EQUIPAMENTOS);
		BarDataset dataset = new BarDataset().setData(cdl.getChartData()).setLabel("Vendas " + DataUtils.getLastYear(ano).get(Calendar.YEAR)).setBorderWidth(2).setBorderColor(Color.DARK_CYAN).setBackgroundColor(Color.DARK_CYAN);
		BarData data = new BarData().addDataset(dataset).setLabels(DataUtils.getLabelMeses());
		cdl = VendasDAO.getVendasAno(String.valueOf(DataUtils.getInstance(ano).get(Calendar.YEAR)), CLASPED.EQUIPAMENTOS);
		dataset = new BarDataset().setData(cdl.getChartData()).setLabel("Vendas " + DataUtils.getInstance(ano).get(Calendar.YEAR)).setBorderWidth(2).setBorderColor(Color.LIGHT_SKY_BLUE).setBackgroundColor(Color.LIGHT_SKY_BLUE);
		data.addDataset(dataset);
		
		if (metas) {
			if (ano == 2019) {
				ChartDataAndLabels cdlMetasRecalc = DatabaseMetas.getMetasRecalc(UNIDADE.EQUIPAMENTOS);
				dataset = new BarDataset().setData(cdlMetasRecalc.getChartData()).setLabel("Meta 2019").setBackgroundColor(Color.TAN);
				data.addDataset(dataset);
			}
		}
		
		BarChart graficoVendasOrtopedia = new BarChart(data);
		return graficoVendasOrtopedia;
	}
	
	@ResponseBody
	@GetMapping("/graficoVendasExportacaoOrtopedia")
	public BarChart getGraficoVendasExportacaoOrtopedia(boolean metas) {
		ChartDataAndLabels cdl = VendasDAO.getVendasAno(String.valueOf(DataUtils.getLastYear(ano).get(Calendar.YEAR)), CLASPED.EXPORTACAOORTOPEDIA);
		BarDataset dataset = new BarDataset().setData(cdl.getChartData()).setLabel("Vendas " + DataUtils.getLastYear(ano).get(Calendar.YEAR)).setBorderWidth(2).setBorderColor(Color.DARK_CYAN).setBackgroundColor(Color.DARK_CYAN);
		BarData data = new BarData().addDataset(dataset).setLabels(DataUtils.getLabelMeses());
		cdl = VendasDAO.getVendasAno(String.valueOf(DataUtils.getInstance(ano).get(Calendar.YEAR)), CLASPED.EXPORTACAOORTOPEDIA);
		dataset = new BarDataset().setData(cdl.getChartData()).setLabel("Vendas " + DataUtils.getInstance(ano).get(Calendar.YEAR)).setBorderWidth(2).setBorderColor(Color.LIGHT_SKY_BLUE).setBackgroundColor(Color.LIGHT_SKY_BLUE);
		data.addDataset(dataset);
		
		if (metas) {
			if (ano == 2019) {
				ChartDataAndLabels cdlMetasRecalc = DatabaseMetas.getMetasRecalc(UNIDADE.EXPORTACAO);
				dataset = new BarDataset().setData(cdlMetasRecalc.getChartData()).setLabel("Meta 2019").setBackgroundColor(Color.TAN);
				data.addDataset(dataset);
			}
		}
		
		BarChart graficoVendasOrtopedia = new BarChart(data);
		return graficoVendasOrtopedia;
	}
	
	@ResponseBody
	@GetMapping("/graficoVendasExportacaoEquipamentos")
	public BarChart getGraficoVendasExportacaoEquipamentos(boolean metas) {
		ChartDataAndLabels cdl = VendasDAO.getVendasAno(String.valueOf(DataUtils.getLastYear(ano).get(Calendar.YEAR)), CLASPED.EXPORTACAOEQUIPAMENTOS);
		BarDataset dataset = new BarDataset().setData(cdl.getChartData()).setLabel("Vendas " + DataUtils.getLastYear(ano).get(Calendar.YEAR)).setBorderWidth(2).setBorderColor(Color.DARK_CYAN).setBackgroundColor(Color.DARK_CYAN);
		BarData data = new BarData().addDataset(dataset).setLabels(DataUtils.getLabelMeses());
		cdl = VendasDAO.getVendasAno(String.valueOf(DataUtils.getInstance(ano).get(Calendar.YEAR)), CLASPED.EXPORTACAOEQUIPAMENTOS);
		dataset = new BarDataset().setData(cdl.getChartData()).setLabel("Vendas " + DataUtils.getInstance(ano).get(Calendar.YEAR)).setBorderWidth(2).setBorderColor(Color.LIGHT_SKY_BLUE).setBackgroundColor(Color.LIGHT_SKY_BLUE);
		data.addDataset(dataset);
		
		if (metas) {
			if (ano == 2019) {
				ChartDataAndLabels cdlMetasRecalc = DatabaseMetas.getMetasRecalc(UNIDADE.EXPORTACAO);
				dataset = new BarDataset().setData(cdlMetasRecalc.getChartData()).setLabel("Meta 2019").setBackgroundColor(Color.TAN);
				data.addDataset(dataset);
			}
		}
		
		BarChart graficoVendasOrtopedia = new BarChart(data);
		return graficoVendasOrtopedia;
	}
	
	
	@ResponseBody
	@GetMapping("/graficoVendasHorizontal")
	public BarChart getGraficoVendasHorizontal() {
		ChartDataAndLabels cdl = VendasDAO.getVendasHorizontal(DataUtils.getInstance(ano).get(Calendar.YEAR));
		cdl.getChartLabels().add("MI");
		BarDataset dataset = new BarDataset().setBorderWidth(2);
		
		BigDecimal miData = BigDecimal.ZERO;
		
		LinkedList<String> renamedChartLabels = new LinkedList<String>();
		
		for (int i = 0; i < cdl.getChartLabels().toArray().length; i++) {
			String chartLabel = (String) cdl.getChartLabels().toArray()[i];
			
			BigDecimal chartData = BigDecimal.ZERO;
			
			if (!chartLabel.equals("MI")) {
				chartData = (i < 4) ? (BigDecimal) cdl.getChartData().toArray()[i] : BigDecimal.ZERO;
			}
			
			if (chartLabel.equals("4 - REPOSICAO")) {
				dataset.addBackgroundColor(new Color(0, 112, 192));
				miData = miData.add(chartData);
				chartLabel = chartLabel.replace("4 - ", "");
			}
			if (chartLabel.equals("3 - CAIXAS")) {
				dataset.addBackgroundColor(new Color(255, 255, 0));
				miData = miData.add(chartData);
				chartLabel = chartLabel.replace("3 - ", "");
			}
			if (chartLabel.equals("MI")) {
				dataset.addBackgroundColor(new Color(0, 123, 255));
				chartData = miData;
			}
			if (chartLabel.equals("1 - EXPORTACAO")) {
				dataset.addBackgroundColor(new Color(255, 0, 0));
				chartLabel = chartLabel.replace("1 - ", "");
			}
			if (chartLabel.equals("2 - EQUIPAMENTOS")) {
				dataset.addBackgroundColor(new Color(0, 176, 80));
				chartLabel = chartLabel.replace("2 - ", "");
			}
			
			renamedChartLabels.add(chartLabel);
			dataset.addData(chartData);
		}
		
		BarData data = new BarData().setLabels(renamedChartLabels).addDataset(dataset);
		BarChart graficoFaturamentoHorizontal = new BarChart(data).setHorizontal();
		return graficoFaturamentoHorizontal;
	}
	
	@ResponseBody
	@GetMapping("/graficoVendasMensal")
	public BarChart getGraficoVendasMensal() {
		BarData data = new BarData();
		ChartDataAndLabels cdl = VendasDAO.getVendasMensalPorClasPed();
		BarDataset dataset = new BarDataset().setBorderWidth(2);
		for (int i = 0; i < cdl.getChartData().toArray().length; i++) {
			BigDecimal chartData = (BigDecimal) cdl.getChartData().toArray()[i];
			String chartLabel = (String) cdl.getChartLabels().toArray()[i];

			dataset.addData(chartData);

			if (chartLabel.equals("REPOSICAO")) {
				dataset.addBackgroundColor(new Color(0, 112, 192));
			}
			if (chartLabel.equals("CAIXAS")) {
				dataset.addBackgroundColor(new Color(255, 255, 0));
			}
			if (chartLabel.equals("EXPORTACAO")) {
				dataset.addBackgroundColor(new Color(255, 0, 0));
			}
			if (chartLabel.equals("EQUIPAMENTOS")) {
				dataset.addBackgroundColor(new Color(0, 176, 80));
			}
		}
		data.addDataset(dataset);
		data.setLabels(cdl.getChartLabels());
		BarChart graficoVendasMensal = new BarChart(data);
		graficoVendasMensal.setHorizontal();
		return graficoVendasMensal;
	}
	
	@ResponseBody
	@GetMapping("/graficoVendasDetalhadoCaixas")
	public BarChart getGraficoVendasDetalhadoCaixas() {
		BarData data = new BarData();
		ChartDataAndLabels cdl = VendasDAO.getVendasPorClaspedPorDia(CLASPED.CAIXAS);
		BarDataset dataset = new BarDataset().setBorderWidth(2).setBackgroundColor(new Color(255, 255, 0));
		dataset.setData(cdl.getChartData());
		data.setLabels(cdl.getChartLabels());
		data.addDataset(dataset);
		BarChart graficoVendasDetalhadoCaixas = new BarChart(data);
		return graficoVendasDetalhadoCaixas;
	}
	
	@ResponseBody
	@GetMapping("/graficoVendasDetalhadoReposicao")
	public BarChart getGraficoVendasDetalhadoReposicao() {
		BarData data = new BarData();
		ChartDataAndLabels cdl = VendasDAO.getVendasPorClaspedPorDia(CLASPED.REPOSICAO);
		BarDataset dataset = new BarDataset().setBorderWidth(2).setBackgroundColor(new Color(0, 112, 192));
		dataset.setData(cdl.getChartData());
		data.setLabels(cdl.getChartLabels());
		data.addDataset(dataset);
		BarChart graficoVendasDetalhadoReposicao = new BarChart(data);
		return graficoVendasDetalhadoReposicao;
	}
	
	@ResponseBody
	@GetMapping("/graficoVendasDetalhadoExportacao")
	public BarChart getGraficoVendasDetalhadoExportacao() {
		BarData data = new BarData();
		ChartDataAndLabels cdl = VendasDAO.getVendasPorClaspedPorDia(CLASPED.EXPORTACAO);
		BarDataset dataset = new BarDataset().setBorderWidth(2).setBackgroundColor(new Color(255, 0, 0));
		dataset.setData(cdl.getChartData());
		data.setLabels(cdl.getChartLabels());
		data.addDataset(dataset);
		BarChart graficoVendasDetalhadoExportacao = new BarChart(data);
		return graficoVendasDetalhadoExportacao;
	}
	
	
	@ResponseBody
	@GetMapping("/graficoVendasDetalhadoEquipamentos")
	public BarChart getGraficoVendasDetalhadoEquipamentos() {
		BarData data = new BarData();
		ChartDataAndLabels cdl = VendasDAO.getVendasPorClaspedPorDia(CLASPED.EQUIPAMENTOS);
		BarDataset dataset = new BarDataset().setBorderWidth(2).setBackgroundColor(new Color(0, 176, 80));
		dataset.setData(cdl.getChartData());
		data.setLabels(cdl.getChartLabels());
		data.addDataset(dataset);
		BarChart graficoVendasDetalhadoEquipamentos = new BarChart(data);
		return graficoVendasDetalhadoEquipamentos;
	}
	
	
	@ResponseBody
	@GetMapping("/graficoCarteiraPendente")
	public BarChart getGraficoCarteiraPendente() {
		BarData data = new BarData();
		ChartDataAndLabels cdl = VendasDAO.getCarteiraPendente();
		BarDataset dataset = new BarDataset().setBorderWidth(2);
		for (int j = 0; j < cdl.getChartData().toArray().length; j++) {
			BigDecimal chartData = (BigDecimal) cdl.getChartData().toArray()[j];
			String chartLabel = cdl.getChartLabelsToArray()[j];

			dataset.addData(chartData);

			if (chartLabel.equals("GLOBAL")) {
				dataset.addBackgroundColor(new Color(75, 170, 150));
			}
			if (chartLabel.equals("REPOSICAO")) {
				dataset.addBackgroundColor(new Color(0, 112, 192));
			}
			if (chartLabel.equals("CAIXAS")) {
				dataset.addBackgroundColor(new Color(255, 255, 0));
			}
			if (chartLabel.equals("EXPORTACAO")) {
				dataset.addBackgroundColor(new Color(255, 0, 0));
			}
			if (chartLabel.equals("EQUIPAMENTOS")) {
				dataset.addBackgroundColor(new Color(0, 176, 80));
			}

		}
		data.addDataset(dataset);
		data.setLabels(cdl.getChartLabels());
		BarChart graficoCarteiraPendente = new BarChart(data).setHorizontal();
		return graficoCarteiraPendente;
	}
	
	
	@ResponseBody
	@GetMapping("/graficoTopClientesOrtopedia")
	public BarChart getGraficoTopClientesOrtopedia() {
		BarData data = new BarData();
		ChartDataAndLabels cdl = VendasDAO.getTopClientes(15, UNIDADE.ORTOPEDIA, ano);
		BarDataset dataset = new BarDataset().setBorderWidth(2);
		dataset.setData(cdl.getChartData());
		for (int k = 0; k < cdl.getChartLabelsToArray().length; k++) {
			dataset.addBackgroundColor(ColorUtils.random());
		}
		data.addDataset(dataset);

		data.setLabels(cdl.getChartLabelsToArray());
		BarChart graficoTopClientesOrtopedia = new BarChart(data);
		graficoTopClientesOrtopedia.setHorizontal();
		return graficoTopClientesOrtopedia;
	}
	
	
	@ResponseBody
	@GetMapping("/graficoTopClientesEquipamentos")
	public BarChart getGraficoTopClientesEquipamentos() {
		BarData data = new BarData();
		ChartDataAndLabels cdl = VendasDAO.getTopClientes(15, UNIDADE.EQUIPAMENTOS, ano);
		BarDataset dataset = new BarDataset().setBorderWidth(2);
		dataset.setData(cdl.getChartData());
		for (int k = 0; k < cdl.getChartLabelsToArray().length; k++) {
			dataset.addBackgroundColor(ColorUtils.random());
		}
		data.addDataset(dataset);
		data.setLabels(cdl.getChartLabelsToArray());
		BarChart graficoTopClientesEquipamentos = new BarChart(data);
		graficoTopClientesEquipamentos.setHorizontal();
		return graficoTopClientesEquipamentos;
	}
	
	
	@ResponseBody
	@GetMapping("/graficoTopProdutosOrtopedia")
	public BarChart getGraficoTopProdutosOrtopedia() {
		BarData data = new BarData();
		ChartDataAndLabels cdl = VendasDAO.getTopProdutos(15, UNIDADE.ORTOPEDIA, ano);
		BarDataset dataset = new BarDataset().setBorderWidth(2);
		dataset.setData(cdl.getChartData());
		for (int k = 0; k < cdl.getChartLabelsToArray().length; k++) {
			dataset.addBackgroundColor(ColorUtils.random());
		}
		data.addDataset(dataset);
		data.setLabels(cdl.getChartLabelsToArray());
		BarChart graficoTopProdutosOrtopedia = new BarChart(data).setHorizontal();
		return graficoTopProdutosOrtopedia;
	}
	
	
	@ResponseBody
	@GetMapping("/graficoTopProdutosEquipamentos")
	public BarChart getGraficoTopProdutosEquipamentos() {
		BarData data = new BarData();
		ChartDataAndLabels cdl = VendasDAO.getTopProdutos(15, UNIDADE.EQUIPAMENTOS, ano);
		BarDataset dataset = new BarDataset().setBorderWidth(2);
		dataset.setData(cdl.getChartData());
		for (int k = 0; k < cdl.getChartLabelsToArray().length; k++) {
			dataset.addBackgroundColor(ColorUtils.random());
		}
		data.addDataset(dataset);
		data.setLabels(cdl.getChartLabelsToArray());
		BarChart graficoTopProdutosEquipamentos = new BarChart(data);
		graficoTopProdutosEquipamentos.setHorizontal();
		return graficoTopProdutosEquipamentos;
	}
	
	@ResponseBody
	@GetMapping("/qtdOrcamentos")
	public BarChart getGraficoQtdOrcamentos(UNIDADE unidade, StatusOrcamento statusOrcamento) {
		BarData data = new BarData();
		ChartDataAndLabels cdl = VendasDAO.getQtdOrcamentos(unidade, statusOrcamento);
		BarDataset dataset = new BarDataset().setBorderWidth(2);
		dataset.setData(cdl.getChartData()).setBackgroundColor(ColorUtils.MEDIUM_TURQUOISE);
		data.addDataset(dataset);
		data.setLabels(cdl.getChartLabels());
		return new BarChart(data);
	}
	
	@ResponseBody
	@GetMapping("/valorOrcamentos")
	public BarChart getGraficoValorOrcamentos(UNIDADE unidade, StatusOrcamento statusOrcamento) {
		BarData data = new BarData();
		ChartDataAndLabels cdl = VendasDAO.getValorOrcamentos(unidade, statusOrcamento);
		BarDataset dataset = new BarDataset().setBorderWidth(2);
		dataset.setData(cdl.getChartData()).setBackgroundColor(ColorUtils.MEDIUM_TURQUOISE);
		data.addDataset(dataset);
		data.setLabels(cdl.getChartLabels());
		return new BarChart(data);
	}
	

	@GetMapping("")
	public String vendas(Model model, Integer ano, boolean metas) {
		this.ano = (ano == null) ? Calendar.getInstance().get(Calendar.YEAR) : ano;
		model.addAttribute("metas", metas);
		return "indicadores/indicadoresVendas";
	}

}
