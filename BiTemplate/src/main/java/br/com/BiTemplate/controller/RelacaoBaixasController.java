package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.FinanceiroDAO;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/relacao-baixas")
public class RelacaoBaixasController {
	
	@GetMapping("")
	public String relacaoBaixas(String target, Model model) {
		model.addAttribute("target", target);
		return "relacaoBaixas";
	}
	
	@ResponseBody
	@PostMapping("")
	public DataTable relacaoBaixas(String target, String dataDe, String dataAte, String dataDigitacaoDe, String dataDigitacaoAte, String nomeFornecedor) {
		if (target != null && target.equals("blank")) {
			return null;
		}
		return FinanceiroDAO.getRelacaoBaixas(dataDe, dataAte, dataDigitacaoDe, dataDigitacaoAte, nomeFornecedor);
	}

}
