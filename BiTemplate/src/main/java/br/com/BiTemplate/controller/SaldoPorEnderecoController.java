package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.EstoqueDAO;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/saldo-por-endereco")
public class SaldoPorEnderecoController {

	@GetMapping("")
	public String saldoPorEndereco(Model model, String target) {
		model.addAttribute("target", target);
		return "saldoPorEndereco";
	}

	@ResponseBody
	@PostMapping("")
	public DataTable saldoPorEndereco(String target, String produto, String lote, String armazem, String descricao) {
		if (target != null && target.equals("blank")) {
			return new DataTable();
		} else {
			return EstoqueDAO.getSaldoPorEndereco(produto, lote, armazem, descricao);
		}
	}

}
