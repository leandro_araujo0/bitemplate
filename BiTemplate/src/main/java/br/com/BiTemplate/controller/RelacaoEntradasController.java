package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.EntradasDAO;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/relacao-entradas")
public class RelacaoEntradasController {
	
	@GetMapping("")
	public String relacaoEntradas(Model model, String target) {
		model.addAttribute("target", target);
		return "relacaoEntradas";
	}
	
	@ResponseBody
	@PostMapping("")
	public DataTable tabelaEntradas(
			String target, 
			String produtoDe,
			String produtoAte,
			String numPedido, 
			String itemPedido, 
			String nf,
			String codigoFornecedor, 
			String nomeFornecedor,
			String entradaDe,
			String entradaAte,
			String loteDe,
			String loteAte,
			String armazem,
			String descricao) {
		if (target != null && target.equals("blank")) {
			return new DataTable();
		}
		return EntradasDAO.getRelacaoEntradas(produtoDe, produtoAte, numPedido, itemPedido, nf, codigoFornecedor, nomeFornecedor, entradaDe, entradaAte, loteDe, loteAte, armazem, descricao);
	}

}
