package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.VendasDAO;
import br.com.BiTemplate.model.PedidoVenda;

@Controller
@RequestMapping("/pedidos-venda")
public class PedidoVendaController {
	
	@GetMapping("/{numero}/detalhes")
	public String detalhes(@PathVariable String numero, Model model) {
		model.addAttribute("numeroPedido", numero);
		PedidoVenda pedido = VendasDAO.findOne(numero);
		model.addAttribute("pedido", pedido);
		return "pedidosVenda/pedidoVenda";
	}
	
	@ResponseBody
	@GetMapping("/ultimo-pedido")
	public PedidoVenda getUltimoPedido() {
		return VendasDAO.getUltimoPedido();
	}
}
