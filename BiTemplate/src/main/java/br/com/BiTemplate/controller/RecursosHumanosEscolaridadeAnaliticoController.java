package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.RecursosHumanosDAO;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/recursos-humanos/escolaridade/analitico")
public class RecursosHumanosEscolaridadeAnaliticoController {

	@GetMapping("")
	public String escolaridadeAnalitico() {
		return "recursosHumanosEscolaridadeAnalitico";
	}
	
	@ResponseBody
	@PostMapping("")
	public DataTable escolaridadeAnalitico(UNIDADE unidade, String grauEscolaridade, String customWhere) {
		return RecursosHumanosDAO.getEscolaridadeAnalitico(unidade, grauEscolaridade, customWhere);
	}
	
}
