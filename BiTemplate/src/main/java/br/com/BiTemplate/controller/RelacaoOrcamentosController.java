package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.VendasDAO;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/relacao-orcamentos")
public class RelacaoOrcamentosController {
	
	@GetMapping("")
	public String relacaoOrcamentos(Model model, String target) {
		model.addAttribute("target", target);
		return "relacaoOrcamentos";
	}
	
	@ResponseBody
	@PostMapping("")
	public DataTable relacaoOrcamentos(String target, String dia, String mes, String ano, UNIDADE unidade) {
		if (target != null && target.equals("blank")) {
			return new DataTable();
		} else {
			return VendasDAO.getRelacaoOrcamentos(dia, mes, ano, unidade);
		}
	}
}
