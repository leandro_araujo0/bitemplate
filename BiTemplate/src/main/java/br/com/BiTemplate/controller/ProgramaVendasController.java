package br.com.BiTemplate.controller;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.BiTemplate.DAO.ClienteDAO;
import br.com.BiTemplate.DAO.VendasDAO;
import br.com.BiTemplate.model.Cliente;
import br.com.BiTemplate.model.ProgramaVendas;
import br.com.BiTemplate.repository.ProgramaVendaRepository;

@Controller
@RequestMapping("/programa-vendas")
public class ProgramaVendasController {
	
	@Autowired
	private ProgramaVendaRepository repository;

	@GetMapping("")
	public String programaVendas(Model model) {
		Iterator<ProgramaVendas> iter = repository.findAll().iterator();
		
		LinkedList<Object> programasVendas = new LinkedList<>();
		
		while(iter.hasNext()) {
			ProgramaVendas programaVendas = iter.next();
			VendasDAO.setConclusaoPrograma(programaVendas);
			
			Cliente cliente = ClienteDAO.findOne(programaVendas.getCliente());
			programaVendas.setNomeCliente(cliente.getNome());
			
			programasVendas.add(programaVendas);
		}
		
		model.addAttribute("programasVendas", programasVendas);
		return "programaVendas";
	}
	
	@GetMapping("/novo")
	public String novoPrograma(ProgramaVendas programaVendas, Model model) {
		List<Cliente> clientes = ClienteDAO.getClientes();
		model.addAttribute("clientes", clientes);
		model.addAttribute("programaVendas", programaVendas);
		return "programaVendasForm";
	}
	
	
	@PostMapping("/novo")
	public String novoPrograma(ProgramaVendas programaVendas) {
		Cliente cliente = ClienteDAO.findOne(programaVendas.getCliente());
		programaVendas.setNomeCliente(cliente.getNome());
		repository.save(programaVendas);
		return "redirect:/programa-vendas";
	}

}
