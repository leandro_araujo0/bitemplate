package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.MovimentoDAO;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/kardex-produto")
public class KardexProdutoController {

	@GetMapping("")
	public String kardexProduto(String target, Model model) {
		model.addAttribute("target", target);
		return "kardexProduto";
	}

	@ResponseBody
	@PostMapping("")
	public DataTable kardexProduto(String target, Model model, String produtoDe, String produtoAte, String loteDe,
			String loteAte, String armazemDe, String armazemAte, String dataInicial, String dataFinal) {

		if (target != null && target.equals("blank")) {
			return new DataTable();
		} else {
			return MovimentoDAO.getMovimentos(produtoDe, produtoAte, loteDe, loteAte, armazemDe, armazemAte, dataInicial, dataFinal);
		}
	}
}
