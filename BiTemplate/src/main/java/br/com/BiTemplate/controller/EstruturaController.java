package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/estruturas")
public class EstruturaController {
	
	@GetMapping("")
	public String estruturas() {
		return "estruturas/estruturas";
	}

}
