package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.VendasDAO;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/orcamentos")
public class OrcamentoDetalhes {
	
	@GetMapping("/{numOrcamento}/detalhes")
	public String orcamentoDetalhes() {
		return "orcamentoDetalhes";
	}
	
	@ResponseBody
	@PostMapping("/{numOrcamento}/detalhes")
	public DataTable orcamentoDetalhes(@PathVariable String numOrcamento) {
		return VendasDAO.getOrcamentoDetalhes(numOrcamento);
	}

}
