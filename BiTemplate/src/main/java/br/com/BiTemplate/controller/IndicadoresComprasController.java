package br.com.BiTemplate.controller;

import java.util.Calendar;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import be.ceau.chart.BarChart;
import be.ceau.chart.color.Color;
import be.ceau.chart.data.BarData;
import be.ceau.chart.dataset.BarDataset;
import br.com.BiTemplate.DAO.ComprasDAO;
import br.com.BiTemplate.enums.PERIODO;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.ChartDataAndLabels;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.utils.ColorUtils;
import br.com.BiTemplate.utils.DataUtils;

@Controller
@RequestMapping("/indicadores/compras")
public class IndicadoresComprasController {
	
	
	@ResponseBody
	@GetMapping("/graficoVariacaoPrecosOrtopedia")
	public BarChart graficoVariacaoPrecosOrtopedia() {
		ChartDataAndLabels cdl = ComprasDAO.getVariacaoPrecos("2018", "2019", UNIDADE.ORTOPEDIA);
		BarDataset dataset = new BarDataset().setBorderWidth(2).setData(cdl.getChartData()).setBackgroundColor(new Color(146, 205, 119)).setLabel("Compras " + Calendar.getInstance().get(Calendar.YEAR));
		BarData data = new BarData().setLabels(cdl.getChartLabels());
		data.addDataset(dataset);
		BarChart graficoVariacaoPrecosOrtopedia = new BarChart(data).setHorizontal();
		return graficoVariacaoPrecosOrtopedia;
	}
	
	@ResponseBody
	@GetMapping("/graficoVariacaoPrecosEquipamentos")
	public BarChart graficoVariacaoPrecosEquipamentos() {
		ChartDataAndLabels cdl = ComprasDAO.getVariacaoPrecos("2018", "2019", UNIDADE.EQUIPAMENTOS);
		BarDataset dataset = new BarDataset().setBorderWidth(2).setData(cdl.getChartData()).setBackgroundColor(new Color(146, 205, 119)).setLabel("Compras " + Calendar.getInstance().get(Calendar.YEAR));
		BarData data = new BarData().setLabels(cdl.getChartLabels());
		data.addDataset(dataset);
		BarChart graficoVariacaoPrecosEquipamentos = new BarChart(data).setHorizontal();
		return graficoVariacaoPrecosEquipamentos;
	}
	
	@ResponseBody
	@GetMapping("/graficoComprasOrtopedia")
	public BarChart graficoComprasOrtopedia() {
		Calendar date = Calendar.getInstance();
		date.add(Calendar.YEAR, -1);
		ChartDataAndLabels cdl = ComprasDAO.getCompras(date.get(Calendar.YEAR), UNIDADE.ORTOPEDIA);
		BarDataset dataset = new BarDataset().setBorderWidth(2).setData(cdl.getChartData()).setBackgroundColor(new Color(174, 144, 201)).setLabel("Compras " + date.get(Calendar.YEAR));
		BarData data = new BarData().addDataset(dataset).setLabels(DataUtils.getLabelMeses());
	
		cdl = ComprasDAO.getCompras(Calendar.getInstance().get(Calendar.YEAR), UNIDADE.ORTOPEDIA);
		dataset = new BarDataset().setBorderWidth(2).setData(cdl.getChartData()).setBackgroundColor(new Color(146, 205, 119)).setLabel("Compras " + Calendar.getInstance().get(Calendar.YEAR));
		data.addDataset(dataset);
		BarChart graficoComprasOrtopedia = new BarChart(data); 
		return graficoComprasOrtopedia;
	}
	
	@ResponseBody
	@GetMapping("/graficoComprasEquipamentos")
	public BarChart graficoComprasEquipamentos() {
		Calendar date = Calendar.getInstance();
		date.add(Calendar.YEAR, -1);
		
		ChartDataAndLabels cdl = ComprasDAO.getCompras(date.get(Calendar.YEAR), UNIDADE.EQUIPAMENTOS);
		BarDataset dataset = new BarDataset().setBorderWidth(2).setData(cdl.getChartData()).setBackgroundColor(new Color(174, 144, 201)).setLabel("Compras " + date.get(Calendar.YEAR));
		BarData data = new BarData().addDataset(dataset).setLabels(DataUtils.getLabelMeses());
	
		cdl = ComprasDAO.getCompras(Calendar.getInstance().get(Calendar.YEAR), UNIDADE.EQUIPAMENTOS);
		dataset = new BarDataset().setBorderWidth(2).setData(cdl.getChartData()).setBackgroundColor(new Color(146, 205, 119)).setLabel("Compras " + Calendar.getInstance().get(Calendar.YEAR));
		data.addDataset(dataset);
		BarChart graficoComprasEquipamentos = new BarChart(data);
		return graficoComprasEquipamentos;
	}
	
	@ResponseBody
	@GetMapping("pedidos")
	public DataTable getPedidos(PERIODO periodo, UNIDADE unidade) {
		return ComprasDAO.getPedidos(periodo, unidade);
	}
	
	@ResponseBody
	@GetMapping("/comprasSegmento")
	public BarChart getComprasSegmento(UNIDADE unidade, PERIODO periodo) {
		ChartDataAndLabels cdl = ComprasDAO.getComprasSegmento(periodo, unidade);
		BarDataset dataset = new BarDataset().setData(cdl.getChartData()).setBackgroundColor(ColorUtils.random());
		BarData data = new BarData().addDataset(dataset).setLabels(cdl.getChartLabels());
		return new BarChart(data).setHorizontal();
	}
	
	
	@GetMapping("")
	public String compras(Model model) {
		return "indicadores/indicadoresCompras";
	}
	
	@ResponseBody
	@PostMapping("/analiseCompras")
	public DataTable analiseCompras(String analiseComprasDe, String analiseComprasAte, UNIDADE unidade) {
		//TODO Alterar código para calcular análise de compras
		DataTable table = ComprasDAO.getAnaliseCompras(analiseComprasDe, analiseComprasAte, unidade);
		return table;
	}

}
