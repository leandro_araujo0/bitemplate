package br.com.BiTemplate.controller;

import java.util.Calendar;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import be.ceau.chart.BarChart;
import be.ceau.chart.data.BarData;
import be.ceau.chart.dataset.BarDataset;
import br.com.BiTemplate.DAO.ProducaoDAO;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.ChartDataAndLabels;
import br.com.BiTemplate.utils.ColorUtils;
import br.com.BiTemplate.utils.DataUtils;

@Controller
@RequestMapping("/indicadores/producao")
public class IndicadoresProducaoController {
	
	@GetMapping("")
	public String producao() {
		return "indicadores/indicadoresProducao";
	}
	
	@ResponseBody
	@GetMapping("/graficoProducaoAnualOrtopedia")
	public BarChart getGraficoProducaoAnualOrtopedia() {
		BarDataset dataset;
		BarData data = new BarData().setLabels(DataUtils.getLabelMeses());
		ChartDataAndLabels cdl;
		Calendar date = Calendar.getInstance();
		date.add(Calendar.YEAR, -1);
		cdl = ProducaoDAO.getProducaoAnual(date.get(Calendar.YEAR), UNIDADE.ORTOPEDIA);
		dataset = new BarDataset().setData(cdl.getChartData()).setBackgroundColor(ColorUtils.random()).setLabel("Produção - " + date.get(Calendar.YEAR));
		data.addDataset(dataset);
		cdl = ProducaoDAO.getProducaoAnual(Calendar.getInstance().get(Calendar.YEAR), UNIDADE.ORTOPEDIA);
		dataset = new BarDataset().setData(cdl.getChartData()).setBackgroundColor(ColorUtils.random()).setLabel("Produção - " + Calendar.getInstance().get(Calendar.YEAR));
		data.addDataset(dataset);
		return new BarChart(data);
	}
	
	
	@ResponseBody
	@GetMapping("/graficoProducaoAnualEquipamentos")
	public BarChart getGraficoProducaoAnualEquipamentos() {
		BarDataset dataset;
		BarData data = new BarData().setLabels(DataUtils.getLabelMeses());
		ChartDataAndLabels cdl;
		Calendar date = Calendar.getInstance();
		date.add(Calendar.YEAR, -1);
		cdl = ProducaoDAO.getProducaoAnual(date.get(Calendar.YEAR), UNIDADE.EQUIPAMENTOS);
		dataset = new BarDataset().setData(cdl.getChartData()).setBackgroundColor(ColorUtils.random()).setLabel("Produção - " + date.get(Calendar.YEAR));
		data.addDataset(dataset);
		cdl = ProducaoDAO.getProducaoAnual(Calendar.getInstance().get(Calendar.YEAR), UNIDADE.EQUIPAMENTOS);
		dataset = new BarDataset().setData(cdl.getChartData()).setBackgroundColor(ColorUtils.random()).setLabel("Produção - " + Calendar.getInstance().get(Calendar.YEAR));
		data.addDataset(dataset);
		return new BarChart(data);
	}
	
	
	@ResponseBody
	@GetMapping("/graficoCargaMaquina")
	public BarChart getGraficoCargaMaquina() {
		ChartDataAndLabels cdl = ProducaoDAO.getCargaMaquina();
		BarDataset dataset = new BarDataset().setData(cdl.getChartData()).setBackgroundColor(ColorUtils.random());
		BarData data = new BarData().addDataset(dataset).setLabels(cdl.getChartLabels());
		return new BarChart(data).setHorizontal();
	}

}
