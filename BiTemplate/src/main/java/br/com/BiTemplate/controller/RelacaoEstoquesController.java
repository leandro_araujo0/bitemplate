package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.EstoqueDAO;
import br.com.BiTemplate.model.DataTable;

@RequestMapping("/relacao-estoques")
@Controller
public class RelacaoEstoquesController {
	
	@GetMapping("")
	public String relacaoEstoques(Model model, String target) {
		model.addAttribute("target", target);
		return "relacaoEstoques";
	}
	
	@ResponseBody
	@PostMapping("")
	public DataTable relacaoEstoques(String target, String codigoProduto, String local, String lote, String dataDe, String dataAte, String estoqueMeses) {
		if (target != null && target.equals("blank") ) {
			return new DataTable();
		} else {
			return EstoqueDAO.getRelacaoEstoque(null, codigoProduto, local, lote, dataDe, dataAte, estoqueMeses);
		}
	}

}
