package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.BiTemplate.DAO.EstoqueDAO;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.ItemEstoque;

@Controller
@RequestMapping("/estoques")
public class EstoqueController {

	@GetMapping("/itens-estoque")
	public String itensEstoque(Model model) {
		Iterable<ItemEstoque> itens = EstoqueDAO.findAll(UNIDADE.ORTOPEDIA);
		model.addAttribute("itens", itens);
		return "estoque/itensEstoque";
	}
	
//	@ResponseBody
//	@GetMapping("/itens-estoque/excel")
//	public InputStreamResource downloadItensEstoque(Model model, String unidade, HttpServletResponse response) throws FileNotFoundException {
//		File file = EstoqueDAO.getRelacaoEstoque(unidade);
//		
//		response.setContentType("application/xls");
//		response.setHeader("Content-Disposition", "attachment; filename=saldos-estoque.xls");
//		
//		return new InputStreamResource(new FileInputStream(file));
//	}
	
}
