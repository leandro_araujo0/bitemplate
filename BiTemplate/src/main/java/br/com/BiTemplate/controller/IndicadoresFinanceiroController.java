package br.com.BiTemplate.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import be.ceau.chart.BarChart;
import be.ceau.chart.data.BarData;
import be.ceau.chart.dataset.BarDataset;
import br.com.BiTemplate.DAO.FinanceiroDAO;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.ChartDataAndLabels;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.utils.ColorUtils;
import br.com.BiTemplate.utils.DataUtils;

@Controller
@RequestMapping("/indicadores/financeiro")
public class IndicadoresFinanceiroController {
	
	@ResponseBody
	@GetMapping("/graficoBaixasAnualOrtopedia")
	public BarChart getGraficoBaixasAnualOrtopedia() {
		ChartDataAndLabels cdl;
		BarDataset dataset;
		BarData data;
		List<BarDataset> datasets = new LinkedList<>();
		
		cdl = FinanceiroDAO.getBaixas(DataUtils.getLastYear().get(Calendar.YEAR), UNIDADE.ORTOPEDIA);
		dataset = new BarDataset().setData(cdl.getChartData()).setBackgroundColor(ColorUtils.random()).setLabel("Baixas " + Calendar.getInstance().get(Calendar.YEAR));
		datasets.add(dataset);
		
		data = new BarData().setDatasets(datasets).setLabels(DataUtils.formatMes(cdl.getChartLabels()));
		return new BarChart(data);
	}
	
	
	@ResponseBody
	@GetMapping("/graficoBaixasAnualEquipamentos")
	public BarChart graficoBaixasAnualEquipamentos() {
		ChartDataAndLabels cdl;
		BarDataset dataset;
		BarData data;
		List<BarDataset> datasets = new LinkedList<>();
		
		cdl = FinanceiroDAO.getBaixas(DataUtils.getLastYear().get(Calendar.YEAR), UNIDADE.EQUIPAMENTOS);
		dataset = new BarDataset().setData(cdl.getChartData()).setBackgroundColor(ColorUtils.random()).setLabel("Baixas " + Calendar.getInstance().get(Calendar.YEAR));
		datasets.add(dataset);
		
		data = new BarData().setDatasets(datasets).setLabels(DataUtils.formatMes(cdl.getChartLabels()));
		return new BarChart(data);
	}
	
	
	@ResponseBody
	@GetMapping("/graficoBaixasAnualSemClas")
	public BarChart graficoBaixasAnualSemClas() {
		ChartDataAndLabels cdl;
		BarDataset dataset;
		BarData data;
		List<BarDataset> datasets = new LinkedList<>();
		
		cdl = FinanceiroDAO.getBaixas(DataUtils.getLastYear().get(Calendar.YEAR), null);
		dataset = new BarDataset().setData(cdl.getChartData()).setBackgroundColor(ColorUtils.random()).setLabel("Baixas " + Calendar.getInstance().get(Calendar.YEAR));
		datasets.add(dataset);
		
		data = new BarData().setDatasets(datasets).setLabels(DataUtils.formatMes(cdl.getChartLabels()));
		return new BarChart(data);
	}
	
	
	@ResponseBody
	@GetMapping("/download-baixas")
	public InputStreamResource relacaoBaixas(HttpServletResponse response) throws IOException {
		File file = FinanceiroDAO.getRelacaoBaixas(DataUtils.getLastYear().get(Calendar.YEAR));
		
		response.setContentType("application/xls");
		response.setHeader("Content-Disposition", "attachment; filename=relacao-baixas.xls");
		InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
		
		return resource;
	}
	
	@ResponseBody
	@GetMapping("/download-baixas2")
	public InputStreamResource relacaoBaixas2(HttpServletResponse response) throws IOException {
		File file = FinanceiroDAO.getRelacaoBaixas2(DataUtils.getLastYear().get(Calendar.YEAR));
		
		response.setContentType("application/xls");
		response.setHeader("Content-Disposition", "attachment; filename=relacao-baixas.xls");
		InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
		
		return resource;
	}
	
	@ResponseBody
	@PostMapping("/getTabelaBaixas")
	public DataTable getTabelaBaixas(String unidade) {
		return FinanceiroDAO.getTabelaBaixas(unidade);
	}
	

	@GetMapping("")
	public String indicadores() {
		return "indicadores/indicadoresFinanceiro";
	}

}
