package br.com.BiTemplate.controller;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.VendasDAO;
import br.com.BiTemplate.enums.TipoVendasPorLinha;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.model.Meta;
import br.com.BiTemplate.model.Metas;

@Controller
@RequestMapping("/indicadores/vendas-por-linha")
public class VendasPorLinhaController<V> {
	
	@ModelAttribute(name = "metas")
	public Iterable<Meta> metas() {
		return new Metas().getMetas(metasOrtopedia(UNIDADE.ORTOPEDIA));
	}
	
	@GetMapping("")
	public String vendasPorLinha() {
		return "indicadores/vendasPorLinha";
	}
	
	@ResponseBody
	@PostMapping("/vendas/{unidade}")
	public DataTable vendasUnidade(@PathVariable UNIDADE unidade) {
		return VendasDAO.getVendasPorLinha(unidade, null, null, null);
	}
	
	@ResponseBody
	@PostMapping("/vendas/{unidade}/por-cliente")
	public DataTable vendasUnidadePorCliente(@PathVariable UNIDADE unidade) {
		return VendasDAO.getVendasPorLinha(unidade, TipoVendasPorLinha.PORCLIENTE, null, null);
	}
	
	@ResponseBody
	@PostMapping("/vendas/por-cliente")
	public DataTable vendasPorCliente(String codigoCliente) {
		return VendasDAO.getVendasPorLinha(null, TipoVendasPorLinha.PORCLIENTE, codigoCliente, null);
	}
	
	@ResponseBody
	@PostMapping("/metas/ortopedia")
	public DataTable metasOrtopedia(UNIDADE unidade) {
		return VendasDAO.getVendasPorLinhaMeta(unidade);
	}
	
	@ResponseBody
	@PostMapping("/cards/{unidade}")
	public LinkedHashMap<String, Object> getCards(@PathVariable UNIDADE unidade) {
		DataTable vendasCards = VendasDAO.getVendasCards(unidade);
		
		int ac96 = 0;
		int ac127 = 0;
		int ac254 = 0;
		int ac365 = 0;
		int ac523 = 0;
//		int ac600 = 0;
		int ac697 = 0;
		int alr = 0;
		int ac100gp = 0;
		int ac254gp = 0;
		int ac365gp = 0;
		int ac523gp = 0;
		int ac600gp = 0;
		int ac697gp = 0;
		
		
		
		
		
		
		int td290 = 0;
		int gs = 0;
		int fl = 0;
		int mc = 0;
		
		
		for (Collection<Object> collection : vendasCards.getData()) {
			LinkedList<Object> tempCollection = (LinkedList<Object>) collection;
			String indice = (String) tempCollection.get(0);
			
			if (!tempCollection.get(2).equals("2020")) {
				continue;
			}
			
			
			if (indice.startsWith("AC96")) {
				ac96 += (int) tempCollection.get(3);
			} else if (indice.startsWith("AC100G")) {
				ac100gp += (int) tempCollection.get(3);
			} else if (indice.startsWith("AC127")) {
				ac127 += (int) tempCollection.get(3);
			} else if (indice.startsWith("AC254G")) {
				ac254gp += (int) tempCollection.get(3);
			} else if (indice.startsWith("AC254")) {
				ac254 += (int) tempCollection.get(3);
			} else if (indice.startsWith("AC365G")) {
				ac365gp += (int) tempCollection.get(3);
			} else if (indice.startsWith("AC365")) {
				ac365 += (int) tempCollection.get(3);
			} else if (indice.startsWith("AC523G")) {
				ac523gp += (int) tempCollection.get(3);
			} else if (indice.startsWith("AC523")) {
				ac523 += (int) tempCollection.get(3);
			} else if (indice.startsWith("AC697G")) {
				ac697gp += (int) tempCollection.get(3);
			} else if (indice.startsWith("AC697")) {
				ac697 += (int) tempCollection.get(3);
			} else if (indice.startsWith("ALR")) {
				alr += (int) tempCollection.get(3);
			} else if (indice.startsWith("TD290")) {
				td290 += (int) tempCollection.get(3);
			} else if (indice.startsWith("GS")) {
				gs += (int) tempCollection.get(3);
			} else if (indice.startsWith("FL")) {
				fl += (int) tempCollection.get(3);
			} else if (indice.startsWith("MC")) {
				mc += (int) tempCollection.get(3);
			}
			
			
			
		}
			
			
		
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
		map.put("AC96", ac96);
		map.put("AC127", ac127);
		map.put("AC254", ac254);
		map.put("AC365", ac365);
		map.put("AC523", ac523);
		map.put("AC697", ac697);
		map.put("ALR", alr);
		map.put("TD290", td290);
		map.put("GS", gs);
		map.put("FL", fl);
		map.put("AC600", mc);
		map.put("AC100GP", ac100gp);
		map.put("AC254GP", ac254gp);
		map.put("AC365GP", ac365gp);
		map.put("AC523GP", ac523gp);
		map.put("AC600GP", ac600gp);
		map.put("AC697GP", ac697gp);
		
		return map;
	}

}
