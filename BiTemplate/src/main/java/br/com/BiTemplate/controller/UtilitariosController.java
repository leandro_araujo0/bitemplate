package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.OrdemProducaoDAO;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/utilitarios")
public class UtilitariosController {
	
	@GetMapping("")
	public String utilitarios() {
		return "utilitarios/utilitarios";
	}
	
	@GetMapping("/estoque-empenho-ops")
	public String getEstoqueEmpenhos() {
		return "utilitarios/estoqueEmpenhos";
	}
	
	@ResponseBody
	@GetMapping("/tabela-empenhos")
	public DataTable tabelaEmpenhos() {
		DataTable dt = new DataTable();
		dt.setData(OrdemProducaoDAO.getEmpenhosEstoque());
		return dt;
	}
	
	@GetMapping("/processamento")
	public String processamento(Model model, String codigoProduto) {
		model.addAttribute("codigoProduto", codigoProduto);
		return "utilitarios/processamento";
	}

}
