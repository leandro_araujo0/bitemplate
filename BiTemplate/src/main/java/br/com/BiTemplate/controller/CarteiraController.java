package br.com.BiTemplate.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.LinkedList;

import javax.servlet.http.HttpServletResponse;

import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.ClienteDAO;
import br.com.BiTemplate.DAO.ProdutoDAO;
import br.com.BiTemplate.DAO.VendasDAO;
import br.com.BiTemplate.enums.CLASPED;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.model.ItemPedidoVenda;
import br.com.BiTemplate.model.Produto;

@Controller
@RequestMapping("/carteira")
public class CarteiraController {
	
	@GetMapping("/cliente/{codCliente}")
	public String getCarteiraCliente(@PathVariable String codCliente, Model model) {
		LinkedList<ItemPedidoVenda> itensPedidoVenda = VendasDAO.getCarteiraPendente(codCliente);
		model.addAttribute("itensPedidoVenda", itensPedidoVenda);
		model.addAttribute("cliente", ClienteDAO.findOne(codCliente));
		return "carteira/carteiraCliente";
	}
	
	@ResponseBody
	@PostMapping("/getProdutosCarteira")
	public Iterable<ItemPedidoVenda> getProdutosCarteira(UNIDADE unidade) {
		Iterable<ItemPedidoVenda> produtosCarteira = VendasDAO.getProdutosCarteira(unidade);
		return produtosCarteira;
	}
	
	@GetMapping("")
	public String getCarteira(Model model, CLASPED clasped) {
		model.addAttribute("clasped", clasped);
		return "carteira/carteira";
	}
	
	@ResponseBody
	@GetMapping("/carteiraTabela")
	public DataTable getCarteiraTabela(CLASPED clasped) {
		return VendasDAO.getCarteiraPendenteTabela(clasped);
	}
	
	@GetMapping("/produto")
	public String getCarteiraProduto(String codigoProduto, Model model) {
		Produto produto = ProdutoDAO.findOne(codigoProduto);
		model.addAttribute("produto", produto);
		
		Iterable<ItemPedidoVenda> itensPedidoVenda = VendasDAO.getCarteiraPendente(produto);
		model.addAttribute("itensPedidoVenda", itensPedidoVenda);
		
		return "carteira/carteiraProduto";
	}
	
	@ResponseBody
	@GetMapping("/excel")
	public InputStreamResource getCarteiraExcel(HttpServletResponse response) throws FileNotFoundException {
		File file = VendasDAO.getCarteiraPendenteExcel();
		
		response.setContentType("application/xls");
		response.setHeader("Content-Disposition", "attachment; filename=Carteira-Pedidos.xls"); 
		InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
		
		return resource;
	}

}
