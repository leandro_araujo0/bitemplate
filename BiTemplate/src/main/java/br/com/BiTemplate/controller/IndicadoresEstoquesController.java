package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import be.ceau.chart.BarChart;
import be.ceau.chart.data.BarData;
import be.ceau.chart.dataset.BarDataset;
import br.com.BiTemplate.DAO.EstoqueDAO;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.ChartDataAndLabels;
import br.com.BiTemplate.utils.ColorUtils;

@Controller
@RequestMapping("/indicadores/estoques")
public class IndicadoresEstoquesController {
	
	@ResponseBody
	@GetMapping("/graficoAlmoxarifadoOrtopedia")
	public BarChart getGraficoAlmoxarifadoOrtopedia() {
		BarDataset dataset;
		BarData data;
		ChartDataAndLabels cdl;

		cdl = EstoqueDAO.getValorEstoque(UNIDADE.ORTOPEDIA);
		dataset = new BarDataset().setBorderWidth(2);
		for (int i = 0; i < cdl.getChartLabelsToArray().length; i++) {
			dataset.addBackgroundColor(ColorUtils.random());
		}
		dataset.setData(cdl.getChartData());
		data = new BarData().setLabels(cdl.getChartLabels()).addDataset(dataset);
		return new BarChart(data);
	}
	
	@ResponseBody
	@GetMapping("/graficoAlmoxarifadoEquipamentos")
	public BarChart getGraficoAlmoxarifadoEquipamentos() {
		BarDataset dataset;
		BarData data;
		ChartDataAndLabels cdl;

		cdl = EstoqueDAO.getValorEstoque(UNIDADE.EQUIPAMENTOS);
		dataset = new BarDataset().setBorderWidth(2).addBackgroundColor(ColorUtils.random());
		for (int i = 0; i < cdl.getChartLabelsToArray().length; i++) {
			dataset.addBackgroundColor(ColorUtils.random());
		}
		dataset.setData(cdl.getChartData());
		data = new BarData().setLabels(cdl.getChartLabels()).addDataset(dataset);
		return new BarChart(data);
		
	}
	
	@ResponseBody
	@GetMapping("/graficoEstoque")
	public BarChart getGraficoEstoqueOrtopedia(UNIDADE unidade) {
		BarDataset dataset;
		BarData data;
		ChartDataAndLabels cdl;
		
		cdl = EstoqueDAO.getValorEstoqueAcabado(unidade);
		dataset = new BarDataset().setBorderWidth(2).addBackgroundColor(ColorUtils.random());
		for (int i = 0; i < cdl.getChartLabelsToArray().length; i++) {
			dataset.addBackgroundColor(ColorUtils.random());
		}
		dataset.setData(cdl.getChartData());
		data = new BarData().setLabels(cdl.getChartLabels()).addDataset(dataset);
		return new BarChart(data).setHorizontal();
	}
	
	@ResponseBody
	@GetMapping("/graficoProdutosAntigosOrtopedia")
	public BarChart getGraficoProdutosAntigosOrtopedia() {
		BarDataset dataset;
		BarData data;
		ChartDataAndLabels cdl;


		cdl = EstoqueDAO.getProdutosAntigos(15, UNIDADE.ORTOPEDIA);
		dataset = new BarDataset().setBorderWidth(2);
		for (int i = 0; i < cdl.getChartLabelsToArray().length; i++) {
			dataset.addBackgroundColor(ColorUtils.random());
		}
		dataset.setData(cdl.getChartData());
		data = new BarData().setLabels(cdl.getChartLabels()).addDataset(dataset);
		return new BarChart(data).setHorizontal();
	}

	@GetMapping("")
	public String estoques(Model model) {
		model.addAttribute("valorEstoque", EstoqueDAO.getValorAcimaXAnos(0, UNIDADE.ORTOPEDIA));
		model.addAttribute("valorAcima2Anos", EstoqueDAO.getValorAcimaXAnos(2, UNIDADE.ORTOPEDIA));

		return "indicadores/indicadoresEstoques";
	}

}
