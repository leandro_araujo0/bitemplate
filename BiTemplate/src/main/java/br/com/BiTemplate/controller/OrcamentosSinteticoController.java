package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.VendasDAO;
import br.com.BiTemplate.enums.TipoOrcamento;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/orcamentos/sintetico")
public class OrcamentosSinteticoController {
	
	@RequestMapping("")
	public String orcamentosSintetico() {
		return "orcamentosSintetico";
	}
	
	@ResponseBody
	@PostMapping("")
	public DataTable orcamentosSintetico(String mes, String ano, UNIDADE unidade) {
		return VendasDAO.getOrcamentosSintetico(null, null, TipoOrcamento.SINTETICO, unidade);
	}

}
