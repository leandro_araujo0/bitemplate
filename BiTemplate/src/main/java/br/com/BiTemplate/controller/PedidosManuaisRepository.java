package br.com.BiTemplate.controller;

import org.springframework.data.repository.CrudRepository;

import br.com.BiTemplate.model.PedidoManual;

public interface PedidosManuaisRepository extends CrudRepository<PedidoManual, Integer>{

	public PedidoManual findByNumeroAndItem(String numero, String item);

}
