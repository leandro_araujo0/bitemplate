package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.FinanceiroDAO;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/acompanhamento-recebimento")
public class AcompanhamentoRecebimentoController {
	
	//CRIAR AQUI UMA PÁGINA COM UMA TABELA CLIENTE X ANOS E TÍTULOS EM ABERTO
	
	@GetMapping("")
	public String acompanhamentoRecebimento() {
		return "acompanhamentoRecebimento";
	}
	
	@ResponseBody
	@PostMapping("/titulos-em-aberto")
	public DataTable getTitulosEmAberto() {
		return FinanceiroDAO.getValorAReceberPorCliente();
	}

}
