package br.com.BiTemplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.BiTemplate.DAO.ComprasDAO;
import br.com.BiTemplate.model.DataTable;

@Controller
@RequestMapping("/pedidos-compra")
public class PedidoCompraController {
	
	@GetMapping("")
	public String pedidos() {
		return "pedidosDeCompras/pedidos";
	}
	
	@GetMapping("/{numeroPedido}/detalhes")
	public String detalhesPedido(@PathVariable String numeroPedido) {
		return "pedidosCompra/detalhesPedidoCompra";
	}
	
	@ResponseBody
	@PostMapping("/{numeroPedido}/detalhes")
	public DataTable tabelaPedido(@PathVariable String numeroPedido) {
		return ComprasDAO.getPedido(numeroPedido);
	}

}
