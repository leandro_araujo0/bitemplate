package br.com.BiTemplate.DAO;

import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import br.com.BiTemplate.configuration.ConfigClass;
import br.com.BiTemplate.model.Cliente;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.utils.CurrencyUtils;

public class ClienteDAO {

	public static Cliente findOne(String codCliente) {
		String sql = "SELECT\r\n" + 
				"A1_COD,\r\n" + 
				"A1_NOME,\r\n" + 
				"A1_NREDUZ,\r\n" + 
				"A1_EST, A1_EMAIL, A1_CGC,\r\n" + 
				"'(' + RTRIM(A1_DDD) + ') ' + A1_TEL AS TELEFONE,\r\n" + 
				"ISNULL((SELECT CASE A1_EST WHEN 'EX' THEN SUM(C6_VALOR) * " + ConfigClass.taxaDolar + " ELSE SUM(C6_VALOR) END / 12 FROM SC6010 WITH(NOLOCK) INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM AND C5_FILIAL = '01' AND SC5010.D_E_L_E_T_ = '' AND C5_TIPO = 'N' AND C5_CLIENTE = A1_COD AND C5_EMISSAO > DATEADD(MONTH,-12,GETDATE()) WHERE SC6010.D_E_L_E_T_ = '' AND C6_FILIAL = '01' AND C6_BLQ <> 'R' AND C5_XREFAT IN ('', '2') AND C5_CLASPED IN ('1', '2', '3', '4') AND ((C6_TES LIKE '5%') OR (C6_TES IN ('701', '702', '703')))),0) AS MEDIA_COMPRAS,\r\n" + 
				"ISNULL((SELECT SUM(D2_TOTAL) / 12 FROM SD2010 WITH(NOLOCK) INNER JOIN SC6010 WITH(NOLOCK) ON D2_PEDIDO = C6_NUM AND D2_ITEMPV = C6_ITEM AND SC6010.D_E_L_E_T_ = '' INNER JOIN SC5010 WITH(NOLOCK) ON D2_PEDIDO = C5_NUM AND SC5010.D_E_L_E_T_ = '' AND C5_FILIAL = '01' WHERE SD2010.D_E_L_E_T_ = '' AND D2_EMISSAO > DATEADD(MONTH, -12, GETDATE()) AND D2_CLIENTE = A1_COD AND C5_TIPO = 'N' AND C5_XREFAT IN ('', '2') AND C5_CLASPED IN ('1', '2', '3', '4') AND ((C6_TES LIKE '5%') OR (C6_TES IN ('701', '702', '703')))),0) AS MEDIA_FATURAMENTO,\r\n" + 
				"ISNULL((SELECT CASE A1_EST WHEN 'EX' THEN SUM(C6_VALOR) * " + ConfigClass.taxaDolar + " ELSE SUM(C6_VALOR) END FROM SC6010 WITH(NOLOCK) INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM AND C5_FILIAL = '01' AND SC5010.D_E_L_E_T_ = '' AND C5_TIPO = 'N' AND C5_CLIENTE = A1_COD AND YEAR(C5_EMISSAO) = YEAR(GETDATE()) AND MONTH(C5_EMISSAO) = MONTH(GETDATE()) WHERE SC6010.D_E_L_E_T_ = '' AND C6_FILIAL = '01' AND C6_BLQ <> 'R' AND C5_XREFAT IN ('', '2') AND C5_CLASPED IN ('1', '2', '3', '4') AND ((C6_TES LIKE '5%') OR (C6_TES IN ('701', '702', '703')))),0) AS COMPRAS_MES,\r\n" +
				"(SELECT ISNULL(SUM(C6_PRCVEN * (C6_QTDVEN - C6_QTDENT)), 0) AS VALOR FROM (\r\n" + 
				"	SELECT\r\n" + 
				"		C6_QTDVEN,\r\n" + 
				"		C6_QTDENT,\r\n" + 
				"		C6_VALOR,\r\n" + 
				"		CASE A1_EST WHEN 'EX' THEN C6_PRCVEN * " + ConfigClass.taxaDolar + " ELSE C6_PRCVEN END AS C6_PRCVEN,\r\n" + 
				"		ISNULL((SELECT SUM(C9_QTDLIB) FROM SC9010 WITH(NOLOCK) WHERE C9_PEDIDO = C5_NUM AND C6_ITEM = C9_ITEM AND SC9010.D_E_L_E_T_ = '' AND C9_LOTECTL <> '' AND C9_NFISCAL = '' AND C9_FILIAL = '01'), 0) AS QTD_EMP\r\n" + 
				"	FROM SC6010 WITH(NOLOCK)\r\n" + 
				"	INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM\r\n" + 
				"		AND C5_FILIAL = '01'\r\n" + 
				"		AND SC5010.D_E_L_E_T_ = ''\r\n" + 
				"		AND C5_TIPO = 'N'\r\n" + 
				"		AND C5_XREFAT IN ('', '2')\r\n" + 
				"		AND C5_CLIENTE <> '000422'\r\n" + 
				"	INNER JOIN SA1010 A1 WITH(NOLOCK) ON C5_CLIENT = A1_COD\r\n" + 
				"		AND C5_LOJACLI = A1_LOJA\r\n" + 
				"		AND A1_FILIAL = ''\r\n" + 
				"		AND A1.D_E_L_E_T_ = ''\r\n" + 
				"	WHERE\r\n" + 
				"		SC6010.D_E_L_E_T_ = ''\r\n" + 
				"	   	AND C6_BLQ Not Like '%R%'\r\n" + 
				"		AND C5_XREFAT IN ('', '2')\r\n" + 
				"		AND C5_LIBEROK = 'S'\r\n" + 
				"		AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" + 
				"	   	AND C5_EMISSAO >= '20160801'\r\n" + 
				"		AND C6_QTDVEN > C6_QTDENT\r\n" + 
				"		AND C5_CLIENTE = SA1010.A1_COD\r\n" + 
				") AS CARTEIRA_PENDENTE) AS CARTEIRA_PENDENTE\r\n" +
				"FROM SA1010 WHERE (A1_COD = ?)";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setString(1, codCliente);
			ResultSet rs = stmt.executeQuery();
			Cliente cliente = new Cliente();
			if (rs.next()) {
				cliente.setCodigo(rs.getString("A1_COD"));
				cliente.setNome(rs.getString("A1_NOME"));
				cliente.setnReduz(rs.getString("A1_NREDUZ"));
				cliente.setEstado(rs.getString("A1_EST"));
				cliente.setTelefone(rs.getString("TELEFONE"));
				cliente.setMediaFaturamento(CurrencyUtils.format(rs.getBigDecimal("MEDIA_FATURAMENTO").setScale(2, RoundingMode.HALF_EVEN)));
				cliente.setMediaCompras(rs.getBigDecimal("MEDIA_COMPRAS").setScale(2, RoundingMode.HALF_EVEN));
				cliente.setComprasMes(rs.getBigDecimal("COMPRAS_MES").setScale(2, RoundingMode.HALF_EVEN));
				cliente.setCarteiraTotal(rs.getBigDecimal("CARTEIRA_PENDENTE").setScale(2, RoundingMode.HALF_EVEN));
				cliente.setCnpj(rs.getString("A1_CGC"));
				cliente.setEmail(rs.getString("A1_EMAIL"));
			}
			return cliente;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return findOne(codCliente);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static List<Cliente> findAll() {
		String sql = "SELECT A1_CGC, A1_EMAIL, A1_MSBLQL, ED_DESCRIC,A1_COD,A1_NOME,A1_NREDUZ,A1_EST,TELEFONE,MEDIA_COMPRAS,COMPRAS_MES FROM (\r\n" + 
				"SELECT\r\n" + 
				"CASE A1_MSBLQL WHEN '1' THEN 'BLOQUEADO' ELSE 'LIBERADO' END AS A1_MSBLQL, A1_CGC, A1_EMAIL, ED_DESCRIC,A1_COD,A1_NOME,A1_NREDUZ,A1_EST,'(' + RTRIM(A1_DDD) + ') ' + A1_TEL AS TELEFONE,\r\n" + 
				"ISNULL((SELECT CASE A1_EST WHEN 'EX' THEN SUM(C6_VALOR) * " + ConfigClass.taxaDolar + " ELSE SUM(C6_VALOR) END / 12 FROM SC6010 WITH(NOLOCK) INNER JOIN SC5010 ON C6_NUM = C5_NUM AND C5_FILIAL = '01' AND SC5010.D_E_L_E_T_ = '' AND C5_TIPO = 'N' AND C5_CLIENTE = A1_COD AND C5_EMISSAO > DATEADD(MONTH, -12, GETDATE()) WHERE SC6010.D_E_L_E_T_ = '' AND C6_FILIAL = '01' AND C6_BLQ <> 'R' AND C5_XREFAT IN ('', '2') AND C5_CLASPED IN ('1', '2', '3', '4') AND ((C6_TES LIKE '5%') OR (C6_TES IN ('701', '702', '703')))), 0) AS MEDIA_COMPRAS,\r\n" + 
				"ISNULL((SELECT CASE A1_EST WHEN 'EX' THEN SUM(C6_VALOR) * " + ConfigClass.taxaDolar + " ELSE SUM(C6_VALOR) END FROM SC6010 WITH(NOLOCK) INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM AND C5_FILIAL = '01' AND SC5010.D_E_L_E_T_ = '' AND C5_TIPO = 'N' AND C5_CLIENTE = A1_COD AND YEAR(C5_EMISSAO) = YEAR(GETDATE()) AND MONTH(C5_EMISSAO) = MONTH(GETDATE()) WHERE SC6010.D_E_L_E_T_ = '' AND C6_FILIAL = '01' AND C6_BLQ <> 'R' AND C5_XREFAT IN ('', '2') AND C5_CLASPED IN ('1', '2', '3', '4') AND ((C6_TES LIKE '5%') OR (C6_TES IN ('701', '702', '703')))),0) AS COMPRAS_MES\r\n" + 
				"FROM SA1010 WITH(NOLOCK)\r\n" +
				"LEFT JOIN SED010 WITH(NOLOCK) ON ED_CODIGO = A1_NATUREZ\r\n" +
				"WHERE SA1010.D_E_L_E_T_ = ''\r\n" + 
				"AND A1_FILIAL = ''\r\n" +
				"AND A1_COD <> '000422'" +
				") AS RESULTSET\r\n" +
				"ORDER BY MEDIA_COMPRAS DESC";
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			List<Cliente> clientes = new LinkedList<>();
			while (rs.next()) {
				Cliente cliente = new Cliente();
				cliente.setCodigo(rs.getString("A1_COD"));
				cliente.setNome(rs.getString("A1_NOME"));
				cliente.setnReduz(rs.getString("A1_NREDUZ"));
				cliente.setSituacao(rs.getString("A1_MSBLQL"));
				cliente.setEstado(rs.getString("A1_EST"));
				cliente.setTelefone(rs.getString("TELEFONE"));
				cliente.setNatureza(rs.getString("ED_DESCRIC"));
				cliente.setMediaCompras(rs.getBigDecimal("MEDIA_COMPRAS").setScale(2, RoundingMode.HALF_EVEN));
				cliente.setComprasMes(rs.getBigDecimal("COMPRAS_MES").setScale(2, RoundingMode.HALF_EVEN));
				cliente.setCnpj(rs.getString("A1_CGC"));
				cliente.setEmail(rs.getString("A1_EMAIL"));
				clientes.add(cliente);
			}
			return clientes;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static DataTable getMapaVendas(String codigoCliente) {
		String sql = "DECLARE @COD_CLIENTE AS VARCHAR(255)\r\n" + 
				"SET @COD_CLIENTE = '" + codigoCliente + "'\r\n" + 
				"SELECT YEAR,\r\n" + 
				"SUM(CASE WHEN YEAR + MONTH = YEAR + '01' THEN COMPRAS ELSE 0 END) AS 'JANEIRO',\r\n" + 
				"SUM(CASE WHEN YEAR + MONTH = YEAR + '02' THEN COMPRAS ELSE 0 END) AS 'FEVEREIRO',\r\n" + 
				"SUM(CASE WHEN YEAR + MONTH = YEAR + '03' THEN COMPRAS ELSE 0 END) AS 'MARCO',\r\n" + 
				"SUM(CASE WHEN YEAR + MONTH = YEAR + '04' THEN COMPRAS ELSE 0 END) AS 'ABRIL',\r\n" + 
				"SUM(CASE WHEN YEAR + MONTH = YEAR + '05' THEN COMPRAS ELSE 0 END) AS 'MAIO',\r\n" + 
				"SUM(CASE WHEN YEAR + MONTH = YEAR + '06' THEN COMPRAS ELSE 0 END) AS 'JUNHO',\r\n" + 
				"SUM(CASE WHEN YEAR + MONTH = YEAR + '07' THEN COMPRAS ELSE 0 END) AS 'JULHO',\r\n" + 
				"SUM(CASE WHEN YEAR + MONTH = YEAR + '08' THEN COMPRAS ELSE 0 END) AS 'AGOSTO',\r\n" + 
				"SUM(CASE WHEN YEAR + MONTH = YEAR + '09' THEN COMPRAS ELSE 0 END) AS 'SETEMBRO',\r\n" + 
				"SUM(CASE WHEN YEAR + MONTH = YEAR + '10' THEN COMPRAS ELSE 0 END) AS 'OUTUBRO',\r\n" + 
				"SUM(CASE WHEN YEAR + MONTH = YEAR + '11' THEN COMPRAS ELSE 0 END) AS 'NOVEMBRO',\r\n" + 
				"SUM(CASE WHEN YEAR + MONTH = YEAR + '12' THEN COMPRAS ELSE 0 END) AS 'DEZEMBRO',\r\n" + 
				"SUM(COMPRAS) AS 'TOTAL'\r\n" + 
				"FROM (\r\n" + 
				"	SELECT\r\n" + 
				"	YEAR(C5_EMISSAO) AS YEAR,MONTH(C5_EMISSAO) AS MONTH,SUM(CASE A1_EST WHEN 'EX' THEN C6_VALOR * 4.0 ELSE C6_VALOR END) AS COMPRAS\r\n" + 
				"	FROM SC6010 WITH(NOLOCK)\r\n" + 
				"	INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM	AND SC5010.D_E_L_E_T_= ''\r\n" + 
				"	INNER JOIN SA1010 ON C5_CLIENT = A1_COD AND C5_LOJACLI = A1_LOJA AND SA1010.D_E_L_E_T_ = '' AND A1_FILIAL <> 'ZZ'\r\n" + 
				"	WHERE\r\n" + 
				"	SC6010.D_E_L_E_T_ = ''\r\n" + 
				"	AND ((C6_TES LIKE '5%') OR (C6_TES IN ('644', '649', '701', '702', '703')))\r\n" + 
				"	AND YEAR(C5_EMISSAO) >= YEAR(DATEADD(YEAR, -3, GETDATE()))\r\n" + 
				"	AND C5_CLIENTE <> '000422'\r\n" + 
				"	AND C5_TIPO = 'N'\r\n" + 
				"	AND C5_FILIAL = '01'\r\n" + 
				"	AND C6_BLQ <> 'R'\r\n" + 
				"	AND C6_FILIAL <> 'ZZ'\r\n" + 
				"	AND C5_CLIENTE = @COD_CLIENTE\r\n" + 
				"	GROUP BY YEAR(C5_EMISSAO), MONTH(C5_EMISSAO)\r\n" + 
				"	UNION ALL\r\n" + 
				"	SELECT\r\n" + 
				"		YEAR(D1_EMISSAO) AS YEAR, MONTH(D1_EMISSAO) AS MONTH, -SUM(D1_TOTAL) AS COMPRAS\r\n" + 
				"	FROM SD1010 WITH (NOLOCK)\r\n" + 
				"	INNER JOIN SD2010 WITH (NOLOCK) ON D1_NFORI = D2_DOC AND SD2010.D_E_L_E_T_ = '' AND D1_ITEMORI = D2_ITEM AND D2_TIPO = 'N'\r\n" + 
				"	INNER JOIN SC6010 WITH (NOLOCK) ON SC6010.D_E_L_E_T_ = '' AND C6_FILIAL <> 'ZZ' AND D2_PEDIDO = C6_NUM AND D2_ITEMPV = C6_ITEM\r\n" + 
				"	INNER JOIN SC5010 WITH (NOLOCK) ON SC5010.D_E_L_E_T_ = '' AND C5_FILIAL <> 'ZZ' AND C5_NUM = C6_NUM\r\n" + 
				"	INNER JOIN SB1010 WITH(NOLOCK) ON D1_COD = B1_COD AND SB1010.D_E_L_E_T_ = '' AND B1_FILIAL <> 'ZZ'\r\n" + 
				"	INNER JOIN SA1010 WITH(NOLOCK) ON SA1010.D_E_L_E_T_ = '' AND A1_FILIAL <> 'ZZ' AND A1_COD = D2_CLIENTE AND A1_LOJA = D2_LOJA\r\n" + 
				"	INNER JOIN SBM010 WITH (NOLOCK) ON SBM010.D_E_L_E_T_ = '' AND BM_FILIAL <> 'ZZ' AND B1_GRUPO = BM_GRUPO\r\n" + 
				"	WHERE\r\n" + 
				"	SD1010.D_E_L_E_T_ = ''\r\n" + 
				"	AND YEAR(D1_EMISSAO) >= YEAR(DATEADD(YEAR, -3, GETDATE()))\r\n" + 
				"	AND C5_CLIENTE = @COD_CLIENTE\r\n" + 
				"	AND C5_TIPO = 'N'\r\n" + 
				"	AND D1_FILIAL <> 'ZZ'\r\n" + 
				"	AND D1_TIPO = 'D'\r\n" + 
				"	GROUP BY YEAR(D1_EMISSAO), MONTH(D1_EMISSAO)\r\n" + 
				") AS REULTSET\r\n" + 
				"GROUP BY YEAR\r\n" + 
				"ORDER BY 1\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"SELECT\r\n" + 
				"	YEAR(C5_EMISSAO) AS YEAR,MONTH(C5_EMISSAO) AS MONTH,SUM(CASE A1_EST WHEN 'EX' THEN C6_VALOR * 4.0 ELSE C6_VALOR END) AS COMPRAS\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM\r\n" + 
				"AND SC5010.D_E_L_E_T_= ''\r\n" + 
				"INNER JOIN SA1010 ON C5_CLIENT = A1_COD AND C5_LOJACLI = A1_LOJA AND SA1010.D_E_L_E_T_ = '' AND A1_FILIAL <> 'ZZ'\r\n" + 
				"WHERE\r\n" + 
				"SC6010.D_E_L_E_T_ = ''\r\n" + 
				"AND ((C6_TES LIKE '5%') OR (C6_TES IN ('644', '649', '701', '702', '703')))\r\n" + 
				"AND YEAR(C5_EMISSAO) >= YEAR(DATEADD(YEAR, -3, GETDATE()))\r\n" + 
				"AND C5_CLIENTE <> '000422'\r\n" + 
				"AND C5_TIPO = 'N'\r\n" + 
				"AND C5_FILIAL = '01'\r\n" + 
				"AND C6_BLQ <> 'R'\r\n" + 
				"AND C6_FILIAL = '01'\r\n" + 
				"AND C5_CLIENTE = '000217'\r\n" + 
				"GROUP BY YEAR(C5_EMISSAO), MONTH(C5_EMISSAO)\r\n" + 
				"UNION ALL\r\n" + 
				"SELECT\r\n" + 
				"	YEAR(D1_EMISSAO) AS YEAR, MONTH(D1_EMISSAO) AS MONTH, -SUM(D1_TOTAL) AS COMPRAS\r\n" + 
				"FROM SD1010 WITH (NOLOCK)\r\n" + 
				"INNER JOIN SD2010 WITH (NOLOCK) ON D1_NFORI = D2_DOC AND SD2010.D_E_L_E_T_ = '' AND D1_ITEMORI = D2_ITEM AND D2_TIPO = 'N'\r\n" + 
				"INNER JOIN SC6010 WITH (NOLOCK) ON SC6010.D_E_L_E_T_ = '' AND C6_FILIAL <> 'ZZ' AND D2_PEDIDO = C6_NUM AND D2_ITEMPV = C6_ITEM\r\n" + 
				"INNER JOIN SC5010 WITH (NOLOCK) ON SC5010.D_E_L_E_T_ = '' AND C5_FILIAL <> 'ZZ' AND C5_NUM = C6_NUM\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON D1_COD = B1_COD AND SB1010.D_E_L_E_T_ = '' AND B1_FILIAL <> 'ZZ'\r\n" + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON SA1010.D_E_L_E_T_ = '' AND A1_FILIAL <> 'ZZ' AND A1_COD = D2_CLIENTE AND A1_LOJA = D2_LOJA\r\n" + 
				"INNER JOIN SBM010 WITH (NOLOCK) ON SBM010.D_E_L_E_T_ = '' AND BM_FILIAL <> 'ZZ' AND B1_GRUPO = BM_GRUPO\r\n" + 
				"WHERE\r\n" + 
				"SD1010.D_E_L_E_T_ = ''\r\n" + 
				"AND C5_CLASPED IN ('1', '2')\r\n" + 
				"AND YEAR(D1_EMISSAO) = 2019\r\n" + 
				"AND D1_FILIAL <> 'ZZ'\r\n" + 
				"AND D1_TIPO = 'D'\r\n" + 
				"GROUP BY YEAR(D1_EMISSAO), MONTH(D1_EMISSAO)";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					linha.add(rs.getString(i));
				}
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<Cliente> getClientes() {
		String sql = "SELECT\r\n" + 
				"A1_COD,A1_NOME,A1_NREDUZ\r\n" + 
				"FROM SA1010\r\n" + 
				"WHERE SA1010.D_E_L_E_T_ = ''\r\n" +
				"ORDER BY 2";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<Cliente> clientes = new LinkedList<>();
			while (rs.next()) {
				Cliente cliente = new Cliente();
				cliente.setCodigo(rs.getString("A1_COD"));
				cliente.setNome(rs.getString("A1_NOME"));
				cliente.setnReduz(rs.getString("A1_NREDUZ"));
				clientes.add(cliente);
			}
			return clientes;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}