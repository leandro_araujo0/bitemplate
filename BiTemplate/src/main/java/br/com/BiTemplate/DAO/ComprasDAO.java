package br.com.BiTemplate.DAO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import br.com.BiTemplate.enums.PERIODO;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.ChartDataAndLabels;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.utils.HtmlUtils;

public class ComprasDAO {

	public static ChartDataAndLabels getCompras(int ano, UNIDADE unidade) {
		String sql = "SELECT\r\n" + 
				"SUBSTRING(C7_EMISSAO,1,6) AS EMISSAO,SUM(C7_TOTAL) AS VALOR_TOTAL\r\n" + 
				"FROM SC7010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON C7_PRODUTO = B1_COD\r\n" +
				"AND SB1010.D_E_L_E_T_ = ''\r\n" +
				"WHERE SC7010.D_E_L_E_T_ = ''\r\n" + 
				"AND C7_FILIAL = '01'\r\n" + 
				"AND YEAR(C7_EMISSAO) = '" + ano + "'\r\n";
				if (unidade.equals(UNIDADE.ORTOPEDIA)) {
					sql += "AND ((B1_LOCPAD IN ('01', '10', '97', '03', '08', '04', '50', '09', '99', '05')) OR (C7_CC IN ('125130')))\r\n";	
				} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
					sql += "AND B1_LOCPAD IN ('11', '96', '12', '26', '15', '31', '14', '17', '07')\r\n";
				}
				sql += "GROUP BY SUBSTRING(C7_EMISSAO,1,6)\r\n" + 
				"ORDER BY SUBSTRING(C7_EMISSAO,1,6)";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("EMISSAO"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR_TOTAL").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getCompras(ano, unidade);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ChartDataAndLabels getVariacaoPrecos(String AnoInicial, String AnoFinal, UNIDADE unidade) {
		String sql = "SELECT TOP 15 B1_COD, ULTIMO_PRECO - PRIMEIRO_PRECO AS VARIACAO, (ULTIMO_PRECO / PRIMEIRO_PRECO) - 1 AS 'REPRES %'FROM (\r\n" + 
				"SELECT RTRIM(B1_COD) AS B1_COD, B1_DESC,\r\n" + 
				"(SELECT TOP 1 D1_VUNIT FROM SD1010 WITH(NOLOCK) WHERE D1_COD = B1_COD AND D1_EMISSAO >= '" + AnoInicial + "' AND SD1010.D_E_L_E_T_ = '' AND D1_FILIAL = '01' AND D1_PEDIDO <> ''\r\n" + 
				"ORDER BY D1_EMISSAO) AS PRIMEIRO_PRECO,\r\n" + 
				"(SELECT TOP 1 A2_NOME FROM SD1010 WITH(NOLOCK) INNER JOIN SA2010 ON D1_FORNECE = A2_COD AND D1_LOJA = A2_LOJA AND A2_FILIAL = '' WHERE D1_COD = B1_COD AND D1_EMISSAO >= '" + AnoInicial + "' AND SD1010.D_E_L_E_T_ = '' AND D1_FILIAL = '01' AND D1_PEDIDO <> ''\r\n" + 
				"ORDER BY D1_EMISSAO) AS PRIMEIRO_FORNECEDOR,\r\n" + 
				"(SELECT TOP 1 D1_VUNIT FROM SD1010 WITH(NOLOCK) WHERE D1_COD = B1_COD AND D1_EMISSAO <= '" + AnoFinal +"' AND SD1010.D_E_L_E_T_ = '' AND D1_FILIAL = '01' AND D1_PEDIDO <> ''\r\n" + 
				"ORDER BY D1_EMISSAO DESC) AS ULTIMO_PRECO,\r\n" + 
				"(SELECT TOP 1 A2_NOME FROM SD1010 WITH(NOLOCK) INNER JOIN SA2010 ON D1_FORNECE = A2_COD AND D1_LOJA = A2_LOJA AND A2_FILIAL = '' WHERE D1_COD = B1_COD AND D1_EMISSAO <= '" + AnoFinal + "' AND SD1010.D_E_L_E_T_ = '' AND D1_FILIAL = '01' AND D1_PEDIDO <> ''\r\n" + 
				"ORDER BY D1_EMISSAO DESC) AS ULTIMO_FORNECEDOR\r\n" + 
				"FROM SB1010 SB1010 WITH(NOLOCK) WHERE B1_FILIAL = '' ";
				if (unidade.equals(UNIDADE.ORTOPEDIA)) {
					sql += "AND B1_LOCPAD = '10' AND SB1010.D_E_L_E_T_ = ''\r\n";
				} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
					sql += "AND B1_LOCPAD = '11' AND SB1010.D_E_L_E_T_ = ''\r\n";
				}
				
				sql += ") AS RESULTSET\r\n" + 
				"WHERE (ULTIMO_PRECO IS NOT NULL AND PRIMEIRO_PRECO IS NOT NULL)\r\n" + 
				"ORDER BY VARIACAO DESC";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("B1_COD") + " (" + rs.getBigDecimal("REPRES %").multiply(BigDecimal.valueOf(100)).setScale(2, RoundingMode.HALF_EVEN) + "%)");
				cdl.getChartData().add(rs.getBigDecimal("VARIACAO").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getVariacaoPrecos(AnoInicial, AnoFinal, unidade);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static DataTable getPedidos(PERIODO periodo, UNIDADE unidade) {
		DataTable table = new DataTable();
		Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
		
		String sql = "SELECT C7_NUM, C7_PRODUTO, B1_DESC, C7_QUANT, C7_PRECO, ULTIMO_PRECO, C7_PRECO - ULTIMO_PRECO AS SAVING, MEDIA_PRECO, C7_PRECO - MEDIA_PRECO AS SAVING_MEDIA FROM (\r\n" + 
				"SELECT\r\n" + 
				"C7_NUM,C7_PRODUTO,B1_DESC,C7_QUANT,C7_PRECO,\r\n" + 
				"(SELECT TOP 1 C7.C7_PRECO FROM SC7010 C7 WITH(NOLOCK) WHERE C7.C7_PRODUTO = SC7010.C7_PRODUTO AND C7.D_E_L_E_T_ = '' AND C7.C7_LOCAL = SC7010.C7_LOCAL ORDER BY C7.C7_EMISSAO DESC) AS ULTIMO_PRECO,\r\n" + 
				"(SELECT TOP 1 AVG(C7.C7_PRECO) FROM SC7010 C7 WITH(NOLOCK) WHERE C7.C7_PRODUTO = SC7010.C7_PRODUTO AND C7.D_E_L_E_T_ = '' AND C7_EMISSAO >= DATEADD(YEAR, -1, GETDATE()) AND C7.C7_LOCAL = SC7010.C7_LOCAL) AS MEDIA_PRECO\r\n" + 
				"FROM SC7010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON B1_COD = C7_PRODUTO\r\n" + 
				"AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE SC7010.D_E_L_E_T_ = ''\r\n" + 
				"AND YEAR(C7_EMISSAO) = YEAR(GETDATE())\r\n"; 
				if (periodo.equals(PERIODO.MESATUAL) || periodo.equals(PERIODO.DIAATUAL)) {
				sql += "AND MONTH(C7_EMISSAO) = MONTH(GETDATE())\r\n";
				}
				if (periodo.equals(PERIODO.DIAATUAL)) {
					sql += "AND DAY(C7_EMISSAO) = DAY(GETDATE())\r\n"; 
				}
				sql += (unidade.equals(UNIDADE.ORTOPEDIA)) ? "AND C7_LOCAL IN ('01','02','03','04','05','10','50')\r\n" : (unidade.equals(UNIDADE.EQUIPAMENTOS)) ? "AND C7_LOCAL IN ('11')\r\n" : "\r\n"; 
				sql += ") AS RESULTSET\r\n" + 
				"ORDER BY SAVING DESC";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				LinkedList<Object> row = new LinkedList<Object>();
				row.add(rs.getString("C7_NUM"));
				row.add(rs.getString("C7_PRODUTO"));
				row.add(rs.getString("B1_DESC"));
				row.add(rs.getString("C7_QUANT"));
				row.add(rs.getBigDecimal("C7_PRECO").setScale(2, RoundingMode.HALF_EVEN));
				row.add(rs.getBigDecimal("ULTIMO_PRECO").setScale(2, RoundingMode.HALF_EVEN));
				row.add(rs.getBigDecimal("SAVING").setScale(2, RoundingMode.HALF_EVEN));
				row.add(rs.getBigDecimal("MEDIA_PRECO").setScale(2, RoundingMode.HALF_EVEN));
				row.add(rs.getBigDecimal("SAVING_MEDIA").setScale(2, RoundingMode.HALF_EVEN));
				data.add(row);
			}
			table.setData(data);
			return table;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getPedidos(periodo, unidade);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ChartDataAndLabels getComprasSegmento(PERIODO periodo, UNIDADE unidade) {
		String sql = "SELECT CASE WHEN (GROUPING(SEGMENTO) = 1) THEN 'TOTAL' ELSE SEGMENTO END AS SEGMENTO, SUM(C7_TOTAL) AS VALOR_TOTAL FROM (\r\n" + 
				"SELECT\r\n" + 
				"CASE SUBSTRING(C7_PRODUTO, 1, 2) WHEN 'MP' THEN 'MP' WHEN 'BN' THEN 'BN' WHEN 'IF' THEN 'IF' WHEN 'CC' THEN 'CC' WHEN 'MA' THEN 'MA' ELSE 'N_CLASS' END AS SEGMENTO,C7_TOTAL\r\n" + 
				"FROM SC7010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON B1_COD = C7_PRODUTO\r\n" + 
				"AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE SC7010.D_E_L_E_T_ = ''\r\n" + 
				"AND YEAR(C7_EMISSAO) = YEAR(GETDATE())\r\n"; 
				if (periodo.equals(PERIODO.MESATUAL) || periodo.equals(PERIODO.DIAATUAL)) {
				sql += "AND MONTH(C7_EMISSAO) = MONTH(GETDATE())\r\n";
				}
				if (periodo.equals(PERIODO.DIAATUAL)) {
					sql += "AND DAY(C7_EMISSAO) = DAY(GETDATE())\r\n"; 
				}
				sql += (unidade.equals(UNIDADE.ORTOPEDIA)) ? "AND C7_LOCAL IN ('01','02','03','04','05','10','50', '97')\r\n" : (unidade.equals(UNIDADE.EQUIPAMENTOS)) ? "AND C7_LOCAL IN ('11', '12', '96')\r\n" : "\r\n"; 
				sql += ") AS RESULTSET\r\n" + 
				"GROUP BY SEGMENTO WITH ROLLUP\r\n" +
				"ORDER BY VALOR_TOTAL";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("SEGMENTO"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR_TOTAL"));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getComprasSegmento(periodo, unidade);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static DataTable getRelacaoCompras(UNIDADE unidade, String mes, String ano, String codigoProduto, String op, String numero, String emissaoDe, String emissaoAte, String nomeFornecedor, String codigoFornecedor, String sc, String abertos, String comprador) {
		String sql = "SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED\r\n" +
				"SELECT\r\n" + 
				"C7_XCOMPR, C7_OBS, C7_LOCAL, A2_NOME, C1_SOLICIT, YA_DESCR, C7_UM, C7_NUMSC, C1_EMISSAO, C7_DATPRF, C7_NUM, RTRIM(C7_OP) AS C7_OP, C7_EMISSAO, C7_ITEM, RTRIM(C7_PRODUTO) AS C7_PRODUTO, C7_DESCRI, C7_QUANT, C7_QUJE, C7_QUANT - C7_QUJE AS QTDPEN, C7_PRECO, C7_TOTAL\r\n" + 
				"FROM SC7010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON C7_PRODUTO = B1_COD\r\n" +
				"AND SB1010.D_E_L_E_T_ = ''\r\n" +
				"INNER JOIN SA2010 WITH(NOLOCK) ON C7_FORNECE = A2_COD AND C7_LOJA = A2_LOJA AND SA2010.D_E_L_E_T_ = ''\r\n" +
				"LEFT JOIN SYA010 WITH(NOLOCK) ON YA_FILIAL <> 'ZZ' AND YA_CODGI = A2_PAIS AND SYA010.R_E_C_D_E_L_ = ''\r\n" +
				"LEFT JOIN SC1010 WITH(NOLOCK) ON C7_NUMSC = C1_NUM AND C7_ITEMSC = C1_ITEM AND C7_PRODUTO = C1_PRODUTO AND SC1010.D_E_L_E_T_ = '' AND C1_FILIAL <> 'ZZ'\r\n" +
				"WHERE SC7010.D_E_L_E_T_ = ''\r\n" + 
				"AND C7_FILIAL <> 'ZZ'\r\n";
				if (ano != null && ano != "") {
					sql += "AND YEAR(C7_EMISSAO) = " + ano + "\r\n";
				}
				if (mes != null && mes != "") {
					sql += "AND MONTH(C7_EMISSAO) = " + mes + "\r\n";
				}
				if (unidade != null && unidade.equals(UNIDADE.ORTOPEDIA)) {
					sql += "AND ((B1_LOCPAD IN ('10','97')) OR (C7_CC IN ('125130')))\r\n"; 
				} else if (unidade != null && unidade.equals(UNIDADE.EQUIPAMENTOS)) {
					sql += "AND ((B1_LOCPAD IN ('11','96')) OR (C7_CC IN ('')))\r\n"; 
				}
				if (codigoProduto != null && !codigoProduto.equals("")) {
					sql += "AND C7_PRODUTO = '" + codigoProduto + "'\r\n";
				}
				if (op != null && !op.equals("")) {
					sql += "AND C7_OP LIKE '" + op + "%'\r\n";
				}
//				COMEÇO DO DESENVOLVIMENTO
				if (numero != null && !numero.equals("")) {
					if (numero.replace(" ", ";").contains(";")) {
						String[] numeros = numero.replace(" ", ";").split(";");
						for (int i = 0; i < numeros.length; i++) {
							if (i == 0) {
									sql += "AND (";
							} else {
								sql += " OR ";
							}
							sql += "C7_NUM = '" + numeros[i] + "'";
						}
						sql += ")\r\n";
					} else if (numero.contains("~")) {
						String[] numeros = numero.split("~");
						sql += "AND C7_NUM >= '" + numeros[0] + "' AND C7_NUM <= '" + numeros[1] + "'\r\n";
					} else {
						sql += "AND C7_NUM = '" + numero + "'\r\n"; 
					}
					sql += "AND (C7_RESIDUO <> 'S' OR C7_QUJE <> 0)\r\n";
				} else {
					if (abertos != null && abertos.equals("")) {
						sql += "AND (C7_RESIDUO <> 'S' AND C7_QUJE <> 0)\r\n";
					} else {
						sql += "AND (C7_RESIDUO <> 'S')\r\n";
						sql += "AND C7_QUANT > C7_QUJE\r\n";
					}
				}
//				FINAL DO DESENVOLVIMENTO				
//				if (numero != null && !numero.equals("")) {
//					sql += "AND C7_NUM = '" + numero + "'\r\n"; 
				if (emissaoDe != null && !emissaoDe.equals("")) {
					sql += "AND C7_EMISSAO >= '" + emissaoDe + "'\r\n";
				}
				if (emissaoAte != null && !emissaoAte.equals("")) {
					sql += "AND C7_EMISSAO <= '" + emissaoAte + "'\r\n";
				}
				if (nomeFornecedor != null && !nomeFornecedor.equals("")) {
					sql += "AND A2_NOME LIKE '" + nomeFornecedor + "%'\r\n";
				}
				if (codigoFornecedor != null && !codigoFornecedor.equals("")) {
					sql += "AND A2_COD = '" + codigoFornecedor + "'\r\n";
				}
				if (sc != null && !sc.equals("")) {
					sql += "AND C7_NUMSC = '" + sc + "'\r\n";
				}
				if (comprador != null && !comprador.equals("")) {
					sql += "AND C7_XCOMPR = '" + comprador + "'\r\n";
				}
				sql += "ORDER BY SUBSTRING(C7_EMISSAO,1,6)";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				String numPedido = rs.getString("C7_NUM");
				linha.add(HtmlUtils.createLink("/relacao-pcs?numero=" + numPedido, numPedido));
				linha.add(rs.getString("A2_NOME"));
				linha.add(rs.getString("C7_EMISSAO"));
				linha.add(rs.getString("C7_XCOMPR"));
				
				String numSc = rs.getString("C7_NUMSC");
				linha.add(HtmlUtils.createLink("/relacao-scs?numero=" + numSc + "&abertos=false", numSc));
				linha.add(rs.getString("C1_EMISSAO"));
				linha.add(rs.getString("C1_SOLICIT"));
				linha.add(HtmlUtils.createLink("/ordens-producao/" + rs.getString("C7_OP") + "/detalhes", rs.getString("C7_OP")));
				
				String tempCodigoProduto = rs.getString("C7_PRODUTO");
				
				if (tempCodigoProduto.contains("BN") || tempCodigoProduto.contains("PI")) {
					linha.add("BN");
				} else {
					linha.add("PC");
				}
				
				String itemPedido = rs.getString("C7_ITEM");
				linha.add(itemPedido);
				linha.add(tempCodigoProduto);
				linha.add(rs.getString("C7_DESCRI"));
				linha.add(rs.getString("C7_UM"));
				linha.add(rs.getString("C7_LOCAL"));
				linha.add(rs.getBigDecimal("C7_QUANT").setScale(4, RoundingMode.HALF_EVEN));
				
				BigDecimal qtdEnt = rs.getBigDecimal("C7_QUJE").setScale(4, RoundingMode.HALF_EVEN);
				if (qtdEnt.compareTo(BigDecimal.ZERO) > 0) {
					linha.add(HtmlUtils.createLink("/relacao-entradas?numPedido=" + numPedido + "&itemPedido=" + itemPedido, qtdEnt));
				} else {
					linha.add(qtdEnt);
				}
				
				linha.add(rs.getBigDecimal("QTDPEN").setScale(4, RoundingMode.HALF_EVEN));
				linha.add(rs.getString("C7_PRECO"));
				linha.add(rs.getString("C7_TOTAL"));
				linha.add(rs.getString("C7_OBS"));
				linha.add(HtmlUtils.createLink("/relacao-entradas?codigoProduto=" + rs.getString("C7_PRODUTO"), "Hist Compras"));
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static DataTable getPedido(String numeroPedido) {
		String sql = "SELECT\r\n" + 
				"C7_XLOTEOF, RTRIM(C7_OP) AS C7_OP, C1_SOLICIT, C7_NUMSC, C7_SOLICIT, C7_NUM, A2_NOME, C7_EMISSAO, C7_DATPRF, C7_ITEM, C7_PRODUTO, C7_DESCRI, C7_QUANT, C7_QUJE, C7_QUANT - C7_QUJE AS QTDPEN\r\n" + 
				"FROM SC7010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SA2010 WITH(NOLOCK) ON C7_FORNECE = A2_COD\r\n" +
				"LEFT JOIN SC1010 WITH(NOLOCK) ON C7_NUMSC = C1_NUM AND C7_ITEMSC = C1_ITEM AND SC1010.D_E_L_E_T_ = ''\r\n" + 
				"AND SA2010.D_E_L_E_T_ = ''\r\n" + 
				"AND A2_FILIAL = ''\r\n" + 
				"WHERE SC7010.D_E_L_E_T_ = ''\r\n" + 
				"AND C7_NUM = '" + numeroPedido + "'\r\n" + 
				"AND C7_FILIAL = '01'";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				String numPedido = rs.getString("C7_NUM");
				linha.add(numPedido);
				linha.add(HtmlUtils.createLink("/relacao-scs?numero=" + rs.getString("C7_NUMSC"), rs.getString("C7_NUMSC")));
				linha.add(rs.getString("C1_SOLICIT"));
				linha.add(rs.getString("A2_NOME"));
				String itemPedido = rs.getString("C7_ITEM");
				linha.add(itemPedido);
				linha.add(rs.getString("C7_PRODUTO"));
				linha.add(rs.getString("C7_DESCRI"));
				linha.add(HtmlUtils.createLink("/ordens-producao/" + rs.getString("C7_OP") + "/detalhes", rs.getString("C7_OP")));
				linha.add(rs.getString("C7_XLOTEOF"));
				linha.add(rs.getString("C7_EMISSAO"));
				linha.add(rs.getString("C7_DATPRF"));
				linha.add(rs.getString("C7_QUANT"));
				BigDecimal qtdEnt = rs.getBigDecimal("C7_QUJE").setScale(2, RoundingMode.HALF_EVEN);
				if (qtdEnt.compareTo(BigDecimal.ZERO) > 0) {
					linha.add(HtmlUtils.createLink("/relacao-entradas?numPedido=" + numPedido + "&itemPedido=" + itemPedido, qtdEnt));
				} else {
					linha.add(qtdEnt);
				}
				linha.add(rs.getBigDecimal("QTDPEN").setScale(2, RoundingMode.HALF_EVEN));
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static DataTable getUltimosPcs(String codigoProduto, int i) {
		String sql = "SELECT TOP " + i + "\r\n" + 
				"C7_NUM, C7_EMISSAO, A2_NOME, C7_PRECO, ISNULL(D1_QUANT, C7_QUANT) AS D1_QUANT, ISNULL(DATEDIFF(DAY, C7_EMISSAO, D1_DTDIGIT), 999) AS LEAD_TIME\r\n" + 
				"FROM SC7010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SA2010 WITH(NOLOCK) ON A2_FILIAL <> 'ZZ' AND A2_COD = C7_FORNECE AND A2_LOJA = C7_LOJA AND SA2010.R_E_C_N_O_ <> 0 AND SA2010.D_E_L_E_T_ = ''\r\n" + 
				"LEFT JOIN SD1010 WITH(NOLOCK) ON D1_FILIAL <> 'ZZ' AND D1_PEDIDO = C7_NUM AND D1_ITEMPC = C7_ITEM AND SD1010.R_E_C_N_O_ <> 0 AND SD1010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE\r\n" + 
				"C7_PRODUTO = '" + codigoProduto + "' AND SC7010.D_E_L_E_T_ = '' AND C7_EMISSAO <> 'ZZ' AND C7_RESIDUO <> 'S'\r\n" + 
				"ORDER BY C7_EMISSAO DESC";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				linha.add(rs.getString("C7_NUM"));
				linha.add(rs.getString("C7_EMISSAO"));
				linha.add(rs.getString("A2_NOME"));
				linha.add(rs.getString("C7_PRECO"));
				linha.add(rs.getString("D1_QUANT"));
				linha.add(rs.getString("LEAD_TIME"));
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public static DataTable getAnaliseCompras(String analiseComprasDe, String analiseComprasAte, UNIDADE unidade) {
		// TODO Auto-generated method stub
		String sql = "SELECT \r\n" + 
				"	COUNT(C7_NUM) AS QTD_PEDIDOS, SUM(C7_TOTAL) AS NUM_VALOR_TOTAL,\r\n" + 
				"	SUM(CASE WHEN ULT_PRECO = 0 THEN 0 WHEN C7_PRECO > ULT_PRECO THEN 1 ELSE 0 END) AS QTD_MAIOR, SUM(CASE WHEN ULT_PRECO = 0 THEN 0 WHEN C7_PRECO > ULT_PRECO THEN C7_TOTAL ELSE 0 END) AS NUM_VALOR_MAIOR, SUM(CASE WHEN ULT_PRECO = 0 THEN 0 WHEN C7_PRECO > ULT_PRECO THEN C7_QUANT * (C7_PRECO - ULT_PRECO) ELSE 0 END) AS NUM_DIF_MAIOR,\r\n" + 
				"	SUM(CASE WHEN ULT_PRECO = 0 THEN 0 WHEN C7_PRECO < ULT_PRECO THEN 1 ELSE 0 END) AS QTD_MENOR, SUM(CASE WHEN ULT_PRECO = 0 THEN 0 WHEN C7_PRECO < ULT_PRECO THEN C7_TOTAL ELSE 0 END) AS NUM_VALOR_MENOR, SUM(CASE WHEN ULT_PRECO = 0 THEN 0 WHEN C7_PRECO < ULT_PRECO THEN C7_QUANT * (ULT_PRECO - C7_PRECO) ELSE 0 END) AS NUM_DIF_MENOR,\r\n" + 
				"	SUM(CASE WHEN ULT_PRECO = 0 THEN 1 WHEN C7_PRECO = ULT_PRECO THEN 1 ELSE 0 END) AS QTD_IGUAL, SUM(CASE WHEN ULT_PRECO = 0 THEN C7_TOTAL WHEN C7_PRECO = ULT_PRECO THEN C7_TOTAL ELSE 0 END) AS NUM_VALOR_IGUAL\r\n" + 
				"FROM (\r\n" + 
				"	SELECT\r\n" + 
				"		C7_NUM, C7_PRODUTO, C7_QUANT, C7_PRECO ,C7_TOTAL,\r\n" + 
				"		ISNULL((SELECT TOP 1 D1_VUNIT FROM SD1010 WITH(NOLOCK) WHERE SD1010.D_E_L_E_T_ = '' AND D1_FILIAL <> 'ZZ' AND D1_COD = C7_PRODUTO AND D1_EMISSAO < '"+ analiseComprasDe + "' ORDER BY D1_EMISSAO DESC), 0) AS ULT_PRECO\r\n" + 
				"	FROM SC7010 WITH(NOLOCK)\r\n" + 
				"	WHERE SC7010.D_E_L_E_T_ = ''\r\n" + 
				"		AND C7_FILIAL <> 'ZZ'\r\n" + 
				"		AND C7_EMISSAO >= '" + analiseComprasDe + "'\r\n" + 
				"		AND C7_EMISSAO <= '" + analiseComprasAte + "'\r\n";
				if (unidade.equals(UNIDADE.ORTOPEDIA)) {
					sql += "		AND C7_LOCAL = '10'\r\n"; 
				} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
					sql += "		AND C7_LOCAL = '11'\r\n"; 
				}
				sql += "		AND C7_PRODUTO NOT LIKE 'BN%'\r\n" + 
				") AS  RESULTSET";
				
		System.out.println(sql);
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while(rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					if (rs.getMetaData().getColumnName(i).startsWith("NUM_")) {
						linha.add(rs.getBigDecimal(i).setScale(2, RoundingMode.HALF_EVEN));
					} else {
						linha.add(rs.getObject(i));
					}
				}
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
