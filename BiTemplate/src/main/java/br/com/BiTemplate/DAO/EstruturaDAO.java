package br.com.BiTemplate.DAO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.model.ItemEstoque;
import br.com.BiTemplate.model.ItemProcessamento;
import br.com.BiTemplate.model.Produto;

public class EstruturaDAO {
	
	public static List<Produto> findByCodigo(String codigo) {
		List<Produto> produtos = new LinkedList<>();
		
		Connection con = Database.getConnection();
		String sql = "SELECT DISTINCT G1_COD FROM SG1010 WITH(NOLOCK) WHERE G1_COD like ? + '%' AND SG1010.D_E_L_E_T_ = ''";		
		try (PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setString(1, codigo);
			ResultSet resultSet = stmt.executeQuery();
			while (resultSet.next()) {
				Produto produto = new Produto();
				produto.setCodigo(resultSet.getString("G1_COD"));
				produtos.add(produto);
			}
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return findByCodigo(codigo);
			} else {
				e.printStackTrace();
			}
		}
		return produtos;
	}

	public static <E> DataTable processa(String[] codigoProdutos, BigDecimal[] qtdsBase, Integer nivelMaximo, String parametro, String dataOps, String dataPcs, boolean processarEstoque) {

		LinkedList<Collection<Object>> data = new LinkedList<Collection<Object>>();
		for (int i = 0; i < codigoProdutos.length; i++) {
			System.out.println("Analisando estrutura do item " + codigoProdutos[i]);
			
			String sql = "WITH TREE (ROW, COD_PAI, COD, COMP, QTD, OPC, GROPC, QTD_BASE, NIVEL) AS\r\n" + 
					"(\r\n" + 
					"	SELECT\r\n" + 
					"		CAST(ROW_NUMBER() OVER(ORDER BY G1_COMP ASC) AS VARCHAR(255)) AS ROW, G1_COD, G1_COD, G1_COMP, G1_QUANT * " + qtdsBase[i] + " AS QTD, G1_OPC, G1_GROPC, 1 AS QTD_BASE, 1 AS NIVEL\r\n" + 
					"	FROM SG1010 WITH (NOLOCK)\r\n" +
					"	WHERE \r\n" + 
					"		G1_COD = '" + codigoProdutos[i] + "'\r\n" + 
					"		AND SG1010.D_E_L_E_T_ = ''\r\n" +
					"		AND G1_INI <= GETDATE()\r\n" +
					"		AND G1_FIM >= GETDATE()\r\n" +
					"	UNION ALL\r\n" + 
					"	SELECT\r\n" + 
					"		CAST(CONCAT(TREE.ROW, '/', ROW_NUMBER() OVER (ORDER BY G1_COMP ASC)) AS VARCHAR(255)) AS ROW, TREE.COD_PAI, G1_COD, G1_COMP, G1_QUANT * TREE.QTD AS QTD, CASE WHEN G1_OPC = '' THEN OPC ELSE G1_OPC END AS OPC, CASE WHEN G1_GROPC = '' THEN GROPC ELSE G1_GROPC END AS OPC, TREE.QTD_BASE, CASE WHEN G1_COD LIKE '%PI%' THEN TREE.NIVEL ELSE TREE.NIVEL + 1 END AS NIVEL\r\n" + 
					"	FROM SG1010 WITH (NOLOCK)\r\n" +
					"	INNER JOIN TREE ON TREE.COMP = G1_COD\r\n" +
					"	INNER JOIN SB1010 WITH(NOLOCK) ON B1_FILIAL <> 'ZZ' AND SB1010.D_E_L_E_T_ = '' AND B1_COD = G1_COD\r\n" +
					"	WHERE\r\n" + 
					"		SG1010.D_E_L_E_T_ = ''\r\n" +
					"		AND G1_INI <= GETDATE()\r\n" +
					"		AND G1_FIM >= GETDATE()\r\n" +
					")\r\n"; 
					sql += "	SELECT ROW, COD_PAI, DESC_PAI, QTD_BASE, NIVEL, GA_DESCOPC, COD_FILHO, DESC_FILHO, UM, SUM(QTD) AS QTD_NEC, '' AS QTD_RECALC, QTD_ESTOQUE, EMP_OPS, QTD_ENDERECAR, QTD_INSPECAO, QTD_EMPENHO, QTD_OP, QTD_SC, QTD_PD_COMPRA, B1_FANTASM FROM (\r\n";
					sql += "		SELECT ROW, COD_PAI AS COD_PAI, B1_PAI.B1_DESC AS DESC_PAI, QTD_BASE AS QTD_BASE, NIVEL, OPC, COMP AS COD_FILHO, B1_FILHO.B1_DESC AS DESC_FILHO, B1_FILHO.B1_UM AS UM, QTD, ISNULL(GA_DESCOPC, '') AS GA_DESCOPC, B1_FILHO.B1_FANTASM,\r\n" + 
					"				ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010  WITH (NOLOCK) WHERE BF_PRODUTO = COMP AND BF_LOCAL = B1_FILHO.B1_LOCPAD AND D_E_L_E_T_ = ''), 0) AS QTD_ESTOQUE,\r\n" + 
					"				ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH (NOLOCK) WHERE BF_PRODUTO = COMP AND BF_LOCAL = '98' AND D_E_L_E_T_ = ''), 0) AS QTD_INSPECAO,\r\n" + 
					"				ISNULL((SELECT SUM(DA_SALDO) FROM SDA010 WITH (NOLOCK) WHERE DA_PRODUTO = COMP AND DA_SALDO <> 0 AND D_E_L_E_T_ = '' AND DA_LOCAL = B1_FILHO.B1_LOCPAD), 0) AS QTD_ENDERECAR,\r\n" + 
					"				ISNULL((SELECT SUM(D4_QUANT) FROM SD4010 D4 WITH (NOLOCK) INNER JOIN SC2010 C2 ON D4.D4_OP = C2.C2_NUM + C2.C2_ITEM + C2.C2_SEQUEN AND C2.D_E_L_E_T_ = '' AND C2.C2_DATRF = '' WHERE D4.D4_COD = COMP AND D4.D_E_L_E_T_ = '' AND D4.D4_LOTECTL = ''), 0) AS QTD_EMPENHO,\r\n" + 
					"				ISNULL((SELECT SUM(C1_QUANT - C1_QUJE) FROM SC1010 C1 WITH (NOLOCK) WHERE C1.D_E_L_E_T_='' AND C1_QUJE < C1_QUANT AND C1_RESIDUO <> 'S' AND C1_PRODUTO = COMP AND C1_LOCAL = B1_FILHO.B1_LOCPAD), 0) AS QTD_SC,\r\n" + 
					"				(SELECT ISNULL(SUM(D4_QUANT), 0) FROM SD4010 WITH(NOLOCK) WHERE D4_COD = COMP AND D4_LOTECTL <> '' AND SD4010.D_E_L_E_T_ = '' AND D4_LOCAL = '96') AS EMP_OPS,\r\n" + 
					"				ISNULL((SELECT SUM(C7_QUANT - C7_QUJE) FROM SC7010 WITH (NOLOCK) WHERE D_E_L_E_T_='' AND C7_PRODUTO = COMP AND (C7_QUANT - C7_QUJE > 0) AND C7_RESIDUO <> 'S' AND C7_EMISSAO >= '" + dataPcs + "' AND C7_LOCAL = B1_FILHO.B1_LOCPAD), 0) AS QTD_PD_COMPRA,\r\n" + 
					"				ISNULL((SELECT SUM(C2_QUANT - C2_QUJE - C2_PERDA) FROM SC2010 WITH (NOLOCK) WHERE C2_PRODUTO = COMP AND C2_DATRF='' AND C2_LOCAL = B1_FILHO.B1_LOCPAD AND SC2010.D_E_L_E_T_='' AND C2_STATUS IN ('N', '') AND C2_EMISSAO >= '" + dataOps + "'), 0) AS QTD_OP\r\n" + 
					"		FROM TREE\r\n" + 
					"		INNER JOIN SB1010 B1_PAI WITH (NOLOCK) ON B1_PAI.B1_COD = COD_PAI AND B1_PAI.D_E_L_E_T_ = ''\r\n" + 
					"		INNER JOIN SB1010 B1_FILHO WITH (NOLOCK) ON B1_FILHO.B1_COD = COMP AND B1_FILHO.D_E_L_E_T_ = ''\r\n" +
					"		LEFT JOIN SGA010 WITH (NOLOCK) ON GROPC = GA_GROPC AND OPC = GA_OPC AND SGA010.D_E_L_E_T_ = ''\r\n" +
					"	WHERE\r\n" +
					"		B1_FILHO.B1_FANTASM <> 'ZZ'\r\n" +
					") AS RESULTSET\r\n" +
					"WHERE\r\n" +
					"COD_FILHO NOT LIKE 'ZZ'\r\n" + //NOT LIKE PI
					"AND COD_FILHO NOT LIKE '%BN%'\r\n" +
					"GROUP BY ROW, COD_PAI, DESC_PAI, QTD_BASE, NIVEL, OPC, COD_FILHO, DESC_FILHO, UM, QTD_ESTOQUE, QTD_INSPECAO, QTD_ENDERECAR, QTD_EMPENHO, QTD_SC, QTD_PD_COMPRA, QTD_OP, GA_DESCOPC, EMP_OPS, B1_FANTASM\r\n" + 
					"ORDER BY ROW";
					
			try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
				LinkedList<Object> headers = new LinkedList<>();
				
				ResultSet rs = stmt.executeQuery();
				
				for (int j = 1; j <= rs.getMetaData().getColumnCount(); j++) {
					headers.add(rs.getMetaData().getColumnName(j));
				}
				
				while (rs.next()) {
					LinkedList<Object> linha = new LinkedList<Object>();
					
					for (int j = 1; j <= rs.getMetaData().getColumnCount(); j++) {
						Object object = rs.getObject(j);
						try {
							if (j > 1) {
								linha.add(BigDecimal.valueOf(Double.parseDouble(object.toString())).setScale(4, RoundingMode.HALF_EVEN));
							} else {
								linha.add(object);
							}
						} catch (NumberFormatException e) {
							linha.add(object);
						}
					}
					data.add(linha);
				}
				
				
			} catch (SQLException e) {
				if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
					return processa(codigoProdutos, qtdsBase, nivelMaximo, parametro, dataOps, dataPcs, processarEstoque);
				} else {
					e.printStackTrace();
				}
			}
		}
		
		
		if (processarEstoque) {
			
			BigDecimal fator = BigDecimal.ONE;
			BigDecimal necess = BigDecimal.ZERO;
			BigDecimal necessRecalc = BigDecimal.ZERO;
			BigDecimal qtdEstoque = BigDecimal.ZERO;
			LinkedList<BigDecimal> fatores = new LinkedList<>();
			//Armazenar os niveis para saber quando está entrando em um filho
			String nivel = "";
			String nivelAnterior = "";
			
			//Armazenar os itens que tiveram estoque debitado 
			LinkedList<ItemEstoque> itensEstoque = new LinkedList<>();
			LinkedList<LinkedList<Object>> produtos = new LinkedList<LinkedList<Object>>();
			
			for (int k = 0; k < data.size(); k++) {
				LinkedList<Object> tempLinha = (LinkedList<Object>) data.get(k);
				nivel = (String) tempLinha.get(0);
				
				if (!tempLinha.get(5).equals("")) {
					//Item anterior...
					if (!produtos.getLast().get(5).equals("")) {
						tempLinha.set(5, produtos.getLast().get(5) + "; " + tempLinha.get(5));
					}
				}
				
				//Sempre que entrar em um filho, o fator deve ser armazenado para recuperação, quando não entrar em um filho, o fator deve se manter o mesmo, basicamente utilizar sempre o ultimo fator da lista
				//Salvar também o item anterior para gerenciamento dos opcionais
				if ((nivel.split("/").length > nivelAnterior.split("/").length) || nivelAnterior.equals("")) {
					fatores.add(fator);
					produtos.add(tempLinha);
				}
				
				//Sempre que sair de um filho para o pai, deve remover o ultimo fator, pois deve voltar ao fator anterior
				else {
					for (int l = 0; l < (nivelAnterior.split("/").length - nivel.split("/").length); l++) {
						fatores.removeLast();
					}
				}
				
				ItemEstoque tempItemEstoque = new ItemEstoque().setCodigo(tempLinha.get(6).toString());
				
				//Verificar se o item já teve o estoque modificado, caso positivo, alterar saldo atual de estoque
				boolean contemEstoque = itensEstoque.contains(tempItemEstoque);
				
				
				if (contemEstoque) {
					qtdEstoque = itensEstoque.get(itensEstoque.indexOf(tempItemEstoque)).getQuant();
				} else {
					qtdEstoque = ((BigDecimal) tempLinha.get(11));
				}
				
				necess = ((BigDecimal) tempLinha.get(9)).multiply(fatores.getLast());
				
				if (tempLinha.get(6).toString().contains("A3650508")) {
					System.out.println("");
				}
				
				if (necess.compareTo(BigDecimal.ZERO) == 0) {
					fator = BigDecimal.ZERO;
					nivelAnterior = nivel;
					tempLinha.set(10, BigDecimal.ZERO);
				} else {
					if (qtdEstoque.compareTo(necess) >= 0) {
						BigDecimal diferenca = qtdEstoque.subtract(necess);
						if (contemEstoque) {
							itensEstoque.get(itensEstoque.indexOf(tempItemEstoque)).setQuant(diferenca);
						} else {
							tempItemEstoque.addQuant(diferenca);
							itensEstoque.add(tempItemEstoque);
						}
						necessRecalc = BigDecimal.ZERO;
					} else if (qtdEstoque.compareTo(BigDecimal.ZERO) > 0) {
						BigDecimal diferenca = necess.subtract(qtdEstoque);
						if (contemEstoque) {
							itensEstoque.get(itensEstoque.indexOf(tempItemEstoque)).setQuant(BigDecimal.ZERO);
						} else {
							tempItemEstoque.addQuant(qtdEstoque.subtract(necess));
							itensEstoque.add(tempItemEstoque);
						}
						necessRecalc = diferenca;
					} else {
						necessRecalc = necess;
					}
					fator = necessRecalc.divide((BigDecimal) tempLinha.get(9), 4, RoundingMode.HALF_EVEN);
					
					if (necessRecalc.compareTo(BigDecimal.ZERO) <= 0) {
						tempLinha.set(10, BigDecimal.ZERO);
					} else {
						tempLinha.set(10, necessRecalc);
					}
				}
				if (tempLinha.get(6).toString().contains("PI") || tempLinha.get(6).toString().startsWith("BN") || tempLinha.get(19).toString().startsWith("S")) {
					data.remove(tempLinha);
					k--;
				}

				
				nivelAnterior = nivel;
			}
		}
		
		return new DataTable(data);
	}


	public static ItemProcessamento getOpcionais(String codigoProduto) {
		String sql = "WITH TREE (PRODUTO, PRODUTO2, DESC_PRODUTO, COMPONENTE, GRUPO_OPCIONAL, ITEM_OPCIONAL, DESC_COMPONENTE, QUANTIDADE, ARMAZEM, NIVEL) AS \r\n" + 
				"(\r\n" + 
				"	SELECT\r\n" + 
				"		SG1010.G1_COD\r\n" + 
				"		, SG1010.G1_COD\r\n" + 
				"		, SB1010.B1_DESC\r\n" + 
				"		, SG1010.G1_COMP\r\n" + 
				"		, SG1010.G1_GROPC\r\n" + 
				"		, SG1010.G1_OPC\r\n" + 
				"		, B1.B1_DESC\r\n" + 
				"		, SG1010.G1_QUANT\r\n" + 
				"		, B1.B1_LOCPAD\r\n" + 
				"		, 0 AS NIVEL\r\n" + 
				"	FROM SG1010 WITH(NOLOCK)\r\n" + 
				"		INNER JOIN SB1010 WITH(NOLOCK) ON SG1010.G1_COD = SB1010.B1_COD AND SB1010.B1_FILIAL = '' AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"		INNER JOIN SB1010 B1 WITH(NOLOCK) ON B1.B1_COD = SG1010.G1_COMP AND B1.B1_FILIAL = '' AND B1.D_E_L_E_T_ = ''\r\n" + 
				"	WHERE\r\n" + 
				"		SG1010.D_E_L_E_T_ = ''\r\n" + 
				"		AND SG1010.G1_FILIAL = ''\r\n" + 
				"		AND SB1010.B1_LOCPAD IN ('01', '10', '11', '12')\r\n" + 
				"		AND G1_COD = ?\r\n" + 
				"	UNION ALL\r\n" + 
				"	SELECT\r\n" + 
				"		TREE.PRODUTO\r\n" + 
				"		, SG1010.G1_COD\r\n" + 
				"		, TREE.DESC_PRODUTO\r\n" + 
				"		, SG1010.G1_COMP\r\n" + 
				"		, SG1010.G1_GROPC\r\n" + 
				"		, SG1010.G1_OPC\r\n" + 
				"		, B1.B1_DESC\r\n" + 
				"		, SG1010.G1_QUANT\r\n" + 
				"		, B1.B1_LOCPAD\r\n" + 
				"		, TREE.NIVEL + 1\r\n" + 
				"	FROM SG1010\r\n" + 
				"		INNER JOIN SB1010 WITH(NOLOCK) ON SG1010.G1_COD = SB1010.B1_COD AND SB1010.B1_FILIAL = '' AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"		INNER JOIN TREE ON TREE.COMPONENTE = G1_COD\r\n" + 
				"		INNER JOIN SB1010 B1 WITH(NOLOCK) ON B1.B1_COD = SG1010.G1_COMP AND B1.B1_FILIAL = '' AND B1.D_E_L_E_T_ = ''\r\n" + 
				"	WHERE\r\n" + 
				"		SG1010.G1_COMP NOT LIKE 'BN%'\r\n" + 
				"		AND SG1010.D_E_L_E_T_ = ''\r\n" + 
				"		AND SG1010.G1_FILIAL = ''\r\n" + 
				")\r\n" + 
				"SELECT DISTINCT GRUPO_OPCIONAL, ITEM_OPCIONAL, GA_DESCOPC\r\n" + 
				"FROM TREE\r\n" + 
				"INNER JOIN SGA010 WITH(NOLOCK) ON GRUPO_OPCIONAL = GA_GROPC \r\n" + 
				"AND ITEM_OPCIONAL = GA_OPC\r\n" + 
				"WHERE\r\n" + 
				"GRUPO_OPCIONAL <> ''\r\n" + 
				"ORDER BY GRUPO_OPCIONAL";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setString(1, codigoProduto);
			ResultSet rs = stmt.executeQuery();
			ItemProcessamento itemProcessamento = new ItemProcessamento();
			itemProcessamento.setCodigo(codigoProduto);
			String opcionais = "";
			while (rs.next()) {
				String grupoOpcional = rs.getString("GRUPO_OPCIONAL");
				String itemOpcional = rs.getString("ITEM_OPCIONAL");
				String descOpcional = rs.getString("GA_DESCOPC");
				opcionais += grupoOpcional + "/" + itemOpcional + "/" + descOpcional + ";";
			}
			itemProcessamento.setOpcionais(opcionais);
			return itemProcessamento;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				getOpcionais(codigoProduto);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static DataTable getOndeEUsado(String codigoProduto) {
		String sql = "WITH TREE(COD, COMP, QUANT, GROPC, OPC) AS (\r\n" + 
				"	SELECT\r\n" + 
				"	G1_COD, G1_COMP, G1_QUANT, G1_GROPC, G1_OPC\r\n" + 
				"	FROM SG1010 WITH(NOLOCK)\r\n" + 
				"	WHERE\r\n" + 
				"	G1_COMP = '" + codigoProduto + "' \r\n" + 
				"	AND SG1010.D_E_L_E_T_ = ''\r\n" + 
				"	UNION ALL\r\n" + 
				"	SELECT\r\n" + 
				"	G1_COD, G1_COMP, G1_QUANT * TREE.QUANT, CASE WHEN G1_GROPC = '' THEN GROPC ELSE G1_GROPC END AS GROPC, CASE WHEN G1_OPC = '' THEN OPC ELSE G1_OPC END AS OPC\r\n" + 
				"	FROM SG1010 WITH(NOLOCK)\r\n" + 
				"	INNER JOIN TREE ON G1_COMP = TREE.COD\r\n" + 
				"	WHERE \r\n" + 
				"	G1_COD <> 'ZZ' \r\n" + 
				"	AND SG1010.D_E_L_E_T_ = ''\r\n" + 
				")\r\n" + 
				"SELECT COD, B1_DESC, SUM(QUANT) AS QUANT, ISNULL(GA_DESCOPC, '') AS DESCOPC, B1_TIPO,\r\n" + 
				"(SELECT ISNULL(SUM(C6_QTDVEN - C6_QTDENT), 0) FROM SC6010 WITH(NOLOCK) INNER JOIN SF4010 WITH(NOLOCK) ON SF4010.F4_CODIGO = C6_TES AND SF4010.D_E_L_E_T_ = '' INNER JOIN SC5010 WITH(NOLOCK) ON C5_FILIAL <> 'ZZ' AND C6_NUM = C5_NUM AND SC5010.D_E_L_E_T_ = '' AND C5_TIPO = 'N' WHERE C6_FILIAL <> 'ZZ' AND C6_PRODUTO = COD AND C6_NUM <> 'ZZ' AND C6_ITEM <> 'ZZ' AND SC6010.R_E_C_N_O_ <> 0 AND SC6010.D_E_L_E_T_ = '' AND C6_BLQ <> 'R' AND C5_LIBEROK = 'S' AND (F4_DUPLIC = 'S' OR C6_CF IN ('5914', '6116', '5116'))) AS QTD_PV\r\n" +
				"FROM TREE\r\n" +
				"INNER JOIN SB1010 WITH(NOLOCK) ON B1_FILIAL <> 'ZZ' AND SB1010.D_E_L_E_T_ = '' AND B1_COD = COD AND B1_MSBLQL IN ('', '2') AND B1_COD NOT LIKE '%PI%' AND B1_COD NOT LIKE 'BN%'\r\n" +
				"LEFT JOIN SGA010 WITH(NOLOCK) ON GA_GROPC = GROPC AND GA_OPC = OPC AND SGA010.D_E_L_E_T_ = ''\r\n" +
				"GROUP BY COD, B1_DESC, ISNULL(GA_DESCOPC, ''), B1_TIPO";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				linha.add(rs.getString("COD"));
				linha.add(rs.getString("B1_DESC"));
				linha.add(rs.getString("B1_TIPO"));
				linha.add(rs.getBigDecimal("QUANT").setScale(4, RoundingMode.HALF_EVEN));
				linha.add(rs.getString("DESCOPC"));
				linha.add(rs.getBigDecimal("QTD_PV").setScale(4, RoundingMode.HALF_EVEN));
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		return null;
	}

	public static DataTable posProcessamento(String[] codigoProdutos) {
		String sql = "SELECT\r\n" + 
				"	B1_COD,\r\n" + 
				"	B1_DESC,\r\n" + 
				"	(SELECT ISNULL(SUM(BF_QUANT), 0) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = B1_LOCPAD AND D_E_L_E_T_ = '') AS QTD_ESTOQUE,\r\n" + 
				"	(SELECT ISNULL(SUM(D4_QUANT), 0) FROM SD4010 WITH(NOLOCK) WHERE D4_COD = B1_COD AND D_E_L_E_T_ = '' AND D4_LOCAL = B1_LOCPAD AND D4_LOTECTL = '') AS QTD_EMPENHADA,\r\n" + 
				"	(SELECT ISNULL(SUM(C1_QUANT - C1_QUJE), 0) FROM SC1010 C1 WITH(NOLOCK) WHERE C1.D_E_L_E_T_='' AND C1_QUJE < C1_QUANT AND C1_RESIDUO <> 'S' AND C1_PRODUTO = B1_COD) AS QTD_SC,\r\n" + 
				"	(SELECT ISNULL(SUM(C7_QUANT - C7_QUJE), 0) FROM SC7010 WITH(NOLOCK) WHERE D_E_L_E_T_='' AND C7_PRODUTO = B1_COD AND (C7_QUANT - C7_QUJE > 0) AND C7_RESIDUO <> 'S') AS QTD_PD_COMPRA,\r\n" + 
				"	(SELECT ISNULL(SUM(C2_QUANT - C2_QUJE - C2_PERDA), 0) FROM SC2010 WITH(NOLOCK) WHERE C2_PRODUTO = B1_COD AND C2_DATRF='' AND C2_LOCAL='11' AND D_E_L_E_T_='' AND C2_STATUS IN ('N', '')) AS QTD_OP,\r\n" + 
				"	ISNULL(C1_NUM, 0) AS NUM_SC,\r\n" + 
				"	ISNULL(C1_EMISSAO, 0) AS DT_SC,\r\n" + 
				"	ISNULL(C1_QUANT, 0) AS QTD_SC,\r\n" + 
				"	ISNULL(C1_QUANT - C1_QUJE, 0) AS SALDO,\r\n" + 
				"	'' AS NUM_PC,\r\n" + 
				"	'' AS DT_PC,\r\n" + 
				"	'' AS QTD_PC,\r\n" + 
				"	'' AS SALDO,\r\n" + 
				"	'' AS FORNECEDOR,\r\n" + 
				"	'' AS NUM_OP,\r\n" + 
				"	'' AS DT_OP,\r\n" + 
				"	'' AS QTD_OP,\r\n" + 
				"	'' AS SALDO\r\n" + 
				"FROM SC1010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON C1_PRODUTO = B1_COD AND SC1010.D_E_L_E_T_ = ''\r\n";
				sql += "WHERE C1_PRODUTO IN (";
					for (int i = 0; i < codigoProdutos.length; i++) {
						if (i != 0) {
							sql += ",";
						}
						sql += "'" + codigoProdutos[i] + "'";
					}
				sql += ")\r\n"; 
				sql += "	AND SC1010.D_E_L_E_T_ = '' \r\n" + 
				"	AND C1_QUJE < C1_QUANT\r\n" + 
				"	AND C1_RESIDUO <> 'S'\r\n" + 
				"UNION ALL\r\n" + 
				"SELECT\r\n" + 
				"	B1_COD,\r\n" + 
				"	B1_DESC,\r\n" + 
				"	(SELECT ISNULL(SUM(BF_QUANT), 0) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = B1_LOCPAD AND D_E_L_E_T_ = '') AS QTD_ESTOQUE,\r\n" + 
				"	(SELECT ISNULL(SUM(D4_QUANT), 0) FROM SD4010 WITH(NOLOCK) WHERE D4_COD = B1_COD AND D_E_L_E_T_ = '' AND D4_LOCAL = B1_LOCPAD AND D4_LOTECTL = '') AS QTD_EMPENHADA,\r\n" + 
				"	(SELECT ISNULL(SUM(C1_QUANT - C1_QUJE), 0) FROM SC1010 C1 WITH(NOLOCK) WHERE C1.D_E_L_E_T_='' AND C1_QUJE < C1_QUANT AND C1_RESIDUO <> 'S' AND C1_PRODUTO = B1_COD) AS QTD_SC,\r\n" + 
				"	(SELECT ISNULL(SUM(C7_QUANT - C7_QUJE), 0) FROM SC7010 WITH(NOLOCK) WHERE D_E_L_E_T_='' AND C7_PRODUTO = B1_COD AND (C7_QUANT - C7_QUJE > 0) AND C7_RESIDUO <> 'S') AS QTD_PD_COMPRA,\r\n" + 
				"	(SELECT ISNULL(SUM(C2_QUANT - C2_QUJE - C2_PERDA), 0) FROM SC2010 WITH(NOLOCK) WHERE C2_PRODUTO = B1_COD AND C2_DATRF='' AND C2_LOCAL='11' AND D_E_L_E_T_='' AND C2_STATUS IN ('N', '')) AS QTD_OP,\r\n" + 
				"	'' AS NUM_SC,\r\n" + 
				"	'' AS DT_SC,\r\n" + 
				"	'' AS QTD_SC,\r\n" + 
				"	'' AS SALDO,\r\n" + 
				"	ISNULL(C7_NUM, 0) AS NUM_PC,\r\n" + 
				"	ISNULL(C7_EMISSAO, 0) AS DT_PC,\r\n" + 
				"	ISNULL(C7_QUANT, 0) AS QTD_PC,\r\n" + 
				"	ISNULL(C7_QUANT - C7_QUJE, 0) AS SALDO,\r\n" + 
				"	A2_NOME AS FORNECEDOR,\r\n" + 
				"	'' AS NUM_OP,\r\n" + 
				"	'' AS DT_OP,\r\n" + 
				"	'' AS QTD_OP,\r\n" + 
				"	'' AS SALDO\r\n" + 
				"FROM SC7010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON C7_PRODUTO = B1_COD AND SC7010.D_E_L_E_T_ = ''\r\n" +
				"LEFT JOIN SA2010 WITH(NOLOCK) ON C7_FORNECE = A2_COD AND C7_LOJA = A2_LOJA AND SA2010.D_E_L_E_T_ = ''\r\n";
				sql += "WHERE C7_PRODUTO IN (";
					for (int i = 0; i < codigoProdutos.length; i++) {
						if (i != 0) {
							sql += ",";
						}
						sql += "'" + codigoProdutos[i] + "'";
					}
				sql += ")\r\n"; 
				sql += "	AND SC7010.D_E_L_E_T_ = ''\r\n" + 
				"	AND (C7_QUANT - C7_QUJE > 0)\r\n" + 
				"	AND C7_RESIDUO <> 'S'\r\n" + 
				"UNION ALL\r\n" + 
				"SELECT\r\n" + 
				"	B1_COD,\r\n" + 
				"	B1_DESC,\r\n" + 
				"	(SELECT ISNULL(SUM(BF_QUANT), 0) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = B1_LOCPAD AND D_E_L_E_T_ = '') AS QTD_ESTOQUE,\r\n" + 
				"	(SELECT ISNULL(SUM(D4_QUANT), 0) FROM SD4010 WITH(NOLOCK) WHERE D4_COD = B1_COD AND D_E_L_E_T_ = '' AND D4_LOCAL = B1_LOCPAD AND D4_LOTECTL = '') AS QTD_EMPENHADA,\r\n" + 
				"	(SELECT ISNULL(SUM(C1_QUANT - C1_QUJE), 0) FROM SC1010 C1 WITH(NOLOCK) WHERE C1.D_E_L_E_T_='' AND C1_QUJE < C1_QUANT AND C1_RESIDUO <> 'S' AND C1_PRODUTO = B1_COD) AS QTD_SC,\r\n" + 
				"	(SELECT ISNULL(SUM(C7_QUANT - C7_QUJE), 0) FROM SC7010 WITH(NOLOCK) WHERE D_E_L_E_T_='' AND C7_PRODUTO = B1_COD AND (C7_QUANT - C7_QUJE > 0) AND C7_RESIDUO <> 'S') AS QTD_PD_COMPRA,\r\n" + 
				"	(SELECT ISNULL(SUM(C2_QUANT - C2_QUJE - C2_PERDA), 0) FROM SC2010 WITH(NOLOCK) WHERE C2_PRODUTO = B1_COD AND C2_DATRF='' AND C2_LOCAL='11' AND D_E_L_E_T_='' AND C2_STATUS IN ('N', '')) AS QTD_OP,\r\n" + 
				"	C1_NUM AS NUM_SC,\r\n" + 
				"	C1_EMISSAO AS DT_SC,\r\n" + 
				"	C1_QUANT AS QTD_SC,\r\n" + 
				"	C1_QUANT - C1_QUJE AS SALDO,\r\n" + 
				"	C7_NUM AS NUM_PC,\r\n" + 
				"	C7_EMISSAO AS DT_PC,\r\n" + 
				"	C7_NUM AS QTD_PC,\r\n" + 
				"	C7_QUANT - C7_QUJE AS SALDO,\r\n" + 
				"	A2_NOME AS FORNECEDOR,\r\n" +
				"	C2_NUM AS NUM_OP,\r\n" + 
				"	C2_EMISSAO AS DT_OP,\r\n" + 
				"	C2_QUANT AS QTD_OP,\r\n" + 
				"	(C2_QUANT - C2_QUJE - C2_PERDA) AS SALDO\r\n" + 
				"FROM SC2010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON C2_PRODUTO = B1_COD AND SC2010.D_E_L_E_T_ = ''\r\n" +
				"LEFT JOIN SC1010 WITH(NOLOCK) ON C2_NUM = LEFT(C1_OP, 6) AND C1_QUJE < C1_QUANT AND SC1010.D_E_L_E_T_ = '' AND C1_RESIDUO <> 'S'\r\n" +
				"LEFT JOIN SC7010 WITH(NOLOCK) ON C2_NUM = LEFT(C7_OP, 6) AND SC7010.D_E_L_E_T_ = '' AND C7_QUJE < C7_QUANT AND C7_RESIDUO <> 'S'\r\n" +
				"LEFT JOIN SA2010 WITH(NOLOCK) ON C7_FORNECE = A2_COD AND C7_LOJA = A2_LOJA AND SA2010.D_E_L_E_T_ = ''\r\n";
				sql += "WHERE C2_PRODUTO IN (";   
					for (int i = 0; i < codigoProdutos.length; i++) {
						if (i != 0) {             
							sql += ",";           
						}                         
						sql += "'" + codigoProdutos[i] + "'";
					}                             
				sql += ")\r\n";                   
				sql += "	AND SC2010.D_E_L_E_T_ = ''\r\n" + 
				"	AND C2_DATRF = ''\r\n" + 
				"	AND C2_LOCAL = B1_LOCPAD\r\n" + 
				"	AND C2_STATUS IN ('N', '')";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++)
				linha.add(rs.getString(i));
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
