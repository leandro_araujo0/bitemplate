package br.com.BiTemplate.DAO;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import br.com.BiTemplate.configuration.ConfigClass;
import br.com.BiTemplate.enums.CLASPED;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.enums.UNIDADEVENDAS;
import br.com.BiTemplate.model.ChartDataAndLabels;
import br.com.BiTemplate.model.Cliente;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.model.ItemPedidoVenda;
import br.com.BiTemplate.model.ParametroVenda;
import br.com.BiTemplate.model.PedidoManual;
import br.com.BiTemplate.model.PedidoVenda;
import br.com.BiTemplate.model.Produto;
import br.com.BiTemplate.utils.CurrencyUtils;
import br.com.BiTemplate.utils.DataUtils;
import br.com.BiTemplate.utils.HtmlUtils;

public class VendasDAO2 {
	
	private static String ultimoPedido;
	
	private static final String _C6_TES = "AND ((C6_TES LIKE '5%') OR (C6_TES IN ('644', '649', '701', '702', '703')) OR (C6_CF IN ('5116', '6116')))\r\n";
	private static final String _joinSC5010 = "INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM\r\n"
			+ "AND C5_FILIAL = '01'\r\n"
			+ "AND SC5010.D_E_L_E_T_ = ''\r\n"
			+ "AND C5_TIPO = 'N'\r\n"
			+ "AND C5_XREFAT IN ('', '2')\r\n"
			+ "AND C5_CLIENTE <> '000422'\r\n";
	private static final String _joinSA1010 = "INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENT\r\n"
			+ "AND C5_LOJACLI = A1_LOJA\r\n"
			+ "AND A1_FILIAL = ''\r\n";
	
	public static ChartDataAndLabels getVendasAno(String ano, CLASPED unidadeVendas) {
		DataTable dataTable = getVendas(ParametroVenda.POREMISSAO, unidadeVendas, null, null, ano, null);
		
		ChartDataAndLabels cdl = new ChartDataAndLabels();
		
		Collection<String> chartLabels = new LinkedList<String>();
		for (Collection<Object> collection : dataTable.getData()) {
			chartLabels.add(((LinkedList<Object>) collection).get(0).toString().substring(0, 6));
		}
		List<String> uniqueChartLabels = new ArrayList<String>(new HashSet<String>(chartLabels));
		Collections.sort(uniqueChartLabels);
		cdl.setChartLabels(uniqueChartLabels);
		
		Collection<BigDecimal> chartData = new LinkedList<BigDecimal>();
		for (String string : uniqueChartLabels) {
			BigDecimal valor = BigDecimal.ZERO;
			for (Collection<Object> collection : dataTable.getData()) {
				if (string.equals(((LinkedList<Object>) collection).get(0).toString().substring(0, 6))) {
					valor = valor.add((BigDecimal) ((LinkedList<Object>) collection).get(2));
				}
			}
			chartData.add(valor);
		}
		cdl.setChartData(chartData);
		return cdl;
	}
	
	public static DataTable getVendas(ParametroVenda parametro, CLASPED clasped, String dia, String mes, String ano, String codigoCliente) {
		String sql = "";
		
		if (parametro != null && parametro.equals(ParametroVenda.POREMISSAO)) {
		sql += "SELECT C5_EMISSAO, SUM(C6_QTDVEN) AS NUM_QTDVEN, SUM(C6_VALOR) AS NUM_VALOR FROM (\r\n";
		} else {
			sql += "SELECT TIPO, C6_NUM, A1_NREDUZ, C5_EMISSAO, C6_ENTREG, C6_PRODUTO, C6_DESCRI, BM_DESC, C6_QTDVEN, C6_QTDENT, C9_QTDLIB, C6_QTDVEN - C6_QTDENT - C9_QTDLIB AS SALDO, C6_PRCVEN, C6_VALOR, C6_PRCVEN * (C6_QTDVEN - C9_QTDLIB - C6_QTDENT) AS VALOR_SALDO FROM (\r\n";
		}
				sql += "SELECT\r\n" + 
				"	'VENDAS' AS TIPO, C6_NUM, A1_NREDUZ, C5_EMISSAO, C6_ENTREG, C6_PRODUTO, C6_DESCRI, C6_QTDVEN, C6_QTDENT, ISNULL(C9_QTDLIB, 0) AS C9_QTDLIB, C6_PRCVEN, C6_VALOR\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM\r\n" + 
				"AND C5_FILIAL = '01'\r\n" + 
				"AND SC5010.D_E_L_E_T_ = ''\r\n" + 
				"AND C5_TIPO = 'N'\r\n" + 
				"AND C5_CLIENTE <> '000422'\r\n" + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENT\r\n" + 
				"AND A1_LOJA = C5_LOJACLI\r\n" + 
				"AND A1_FILIAL = ''\r\n" + 
				"LEFT JOIN SC9010 WITH(NOLOCK) ON C9_PEDIDO = C5_NUM\r\n" + 
				"AND C9_ITEM = C6_ITEM\r\n" + 
				"AND C9_PRODUTO = C6_PRODUTO\r\n" + 
				"AND SC9010.D_E_L_E_T_ = ''\r\n" + 
				"AND C9_NFISCAL = ''\r\n" + 
				"AND C9_LOTECTL <> ''\r\n" + 
				"WHERE\r\n" + 
				"C6_BLQ <> 'R'\r\n" + 
				"AND ((C6_TES LIKE '5%') OR (C6_TES IN ('644', '649', '701', '702', '703')) OR (C6_CF IN ('5116', '6116')))\r\n"; 
				if (clasped.equals(CLASPED.EQUIPAMENTOS)) {
					sql += "AND C5_CLASPED IN ('3', '6')\r\n";
				} else if (clasped.equals(CLASPED.MERCADOINTERNO)) {
					sql += "AND C5_CLASPED IN ('1', '2')\r\n";
				} else if (clasped.equals(CLASPED.EXPORTACAO)) {
					sql += "AND C5_CLASPED IN ('4')\r\n";
				}
				sql += (ano != null && !ano.equals("")) ? "AND YEAR(C5_EMISSAO) = " + ano + "\r\n" : ""; 
				sql += (mes != null && !mes.equals("")) ? "AND MONTH(C5_EMISSAO) = " + mes + "\r\n" : "";
				sql += "AND SC6010.D_E_L_E_T_ = ''" + 
				"UNION ALL\r\n" + 
				"SELECT\r\n" + 
				"	'DEVOLUCAO' AS TIPO, C6_NUM, A1_NREDUZ, D1_EMISSAO, C6_ENTREG, C6_PRODUTO, C6_DESCRI, D1_QUANT, D1_QUANT, 0 AS C9_QTDLIB, -C6_PRCVEN, -C6_VALOR\r\n" + 
				"FROM SD1010 WITH (NOLOCK)\r\n" + 
				"INNER JOIN SD2010 WITH (NOLOCK) ON D1_NFORI = D2_DOC AND SD2010.D_E_L_E_T_ = '' AND D1_ITEMORI = D2_ITEM AND D2_TIPO = 'N'\r\n" + 
				"INNER JOIN SC6010 WITH (NOLOCK) ON SC6010.D_E_L_E_T_ = '' AND C6_FILIAL <> 'ZZ' AND D2_PEDIDO = C6_NUM AND D2_ITEMPV = C6_ITEM\r\n" + 
				"INNER JOIN SC5010 WITH (NOLOCK) ON SC5010.D_E_L_E_T_ = '' AND C5_FILIAL <> 'ZZ' AND C5_NUM = C6_NUM\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON D1_COD = B1_COD AND SB1010.D_E_L_E_T_ = '' AND B1_FILIAL <> 'ZZ'\r\n" + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON SA1010.D_E_L_E_T_ = '' AND A1_FILIAL <> 'ZZ' AND A1_COD = D2_CLIENTE AND A1_LOJA = D2_LOJA\r\n" + 
				"WHERE\r\n" + 
				"SD1010.D_E_L_E_T_ = ''\r\n" + 
				"AND C5_CLASPED = 3\r\n";
				if (clasped.equals(CLASPED.EQUIPAMENTOS)) {
					sql += "AND C5_CLASPED IN ('3', '6')\r\n";
				} else if (clasped.equals(CLASPED.MERCADOINTERNO)) {
					sql += "AND C5_CLASPED IN ('1', '2')\r\n";
				} else if (clasped.equals(CLASPED.EXPORTACAO)) {
					sql += "AND C5_CLASPED IN ('4')\r\n";
				}
				sql += (ano != null && !ano.equals("")) ? "AND YEAR(D1_EMISSAO) = " + ano + "\r\n" : ""; 
				sql += (mes != null && !mes.equals("")) ? "AND MONTH(D1_EMISSAO) = " + mes + "\r\n" : ""; 
				sql += "AND D1_FILIAL <> 'ZZ'\r\n" + 
				"AND D1_TIPO = 'D'\r\n" + 
				") AS RESULTSET\r\n" + 
				"INNER JOIN SB1010 ON B1_FILIAL <> 'ZZ' AND SB1010.D_E_L_E_T_ = '' AND B1_COD = C6_PRODUTO\r\n" + 
				"INNER JOIN SBM010 ON BM_FILIAL <> 'ZZ' AND SBM010.D_E_L_E_T_ = '' AND BM_GRUPO = B1_GRUPO\r\n";
				if (parametro != null && parametro.equals(ParametroVenda.POREMISSAO)) {
					sql += "GROUP BY C5_EMISSAO";
				}
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while(rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					if (rs.getMetaData().getColumnName(i).startsWith("NUM_")) {
						linha.add(BigDecimal.valueOf(Double.parseDouble(rs.getString(i))));
					} else {
						linha.add(rs.getString(i));
					}
				}
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static ChartDataAndLabels getVendasMensal() {
		String sql = "SELECT CLASPED, SUM(CASE C6_BLQ WHEN 'R' THEN VALOR/C6_QTDVEN*C6_QTDENT ELSE VALOR END) AS VALOR FROM (\r\n"
				+ "	SELECT\r\n"
				+ "		SUBSTRING(C5_EMISSAO,1,6) AS EMISSAO,CASE A1_EST WHEN 'EX' THEN C6_VALOR * " + ConfigClass.taxaDolar + " ELSE C6_VALOR END AS VALOR,C6_BLQ,C6_PRCVEN,C6_QTDENT,C6_QTDVEN, CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '4' THEN 'EXPORTACAO' WHEN '6' THEN 'EQUIPAMENTOS' END AS CLASPED\r\n"
				+ "	FROM SC6010 WITH(NOLOCK)\r\n"
				+ _joinSC5010
				+ _joinSA1010
				+ "	WHERE MONTH(C5_EMISSAO) = MONTH(GETDATE())\r\n" 
				+ "		AND YEAR(C5_EMISSAO) = YEAR(GETDATE())\r\n"
				+ "		AND SC6010.D_E_L_E_T_ = ''\r\n" 
				+ "		AND C6_FILIAL = '01'\r\n"
				+ "		AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n"
				+ "		AND C5_XREFAT IN ('', '2')\r\n";
				sql += _C6_TES;
				sql += "	) AS RESULTSET\r\n"
				+ "GROUP BY CLASPED";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("CLASPED"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getVendasMensal();
			} else {
				e.printStackTrace();
			}
		}

		return null;
	}

	public static ChartDataAndLabels getCarteiraPendente() {
//		SELECT CASE WHEN (GROUPING(SEGMENTO) = 1) THEN 'TOTAL' ELSE SEGMENTO END AS SEGMENTO
		String sql = "SELECT CASE WHEN (GROUPING(CLASPED) = 1) THEN 'GLOBAL' ELSE CLASPED END AS CLASPED , SUM(C6_PRCVEN * (C6_QTDVEN - C6_QTDENT - QTD_EMP)) AS VALOR FROM (\r\n" + 
				"	SELECT\r\n" + 
				"	CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '4' THEN 'EXPORTACAO' WHEN '6' THEN 'EQUIPAMENTOS' ELSE C5_CLASPED END AS CLASPED,\r\n" + 
				"	C6_QTDVEN,\r\n" + 
				"	C6_QTDENT,\r\n" + 
				"	C6_VALOR,\r\n" + 
				"	CASE A1_EST WHEN 'EX' THEN C6_PRCVEN * 3.6 ELSE C6_PRCVEN END AS C6_PRCVEN,\r\n" + 
				"	ISNULL((SELECT SUM(C9_QTDLIB) FROM SC9010 WITH(NOLOCK) WHERE C9_PEDIDO = C5_NUM AND C6_ITEM = C9_ITEM AND SC9010.D_E_L_E_T_ = '' AND C9_LOTECTL <> '' AND C9_NFISCAL = '' AND C9_FILIAL = '01'), 0) AS QTD_EMP\r\n" + 
				"	FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010 + 
				"	INNER JOIN SA1010 ON C5_CLIENT = A1_COD\r\n" + 
				"		AND C5_LOJACLI = A1_LOJA\r\n" + 
				"		AND A1_FILIAL = ''\r\n" + 
				"		AND SA1010.D_E_L_E_T_ = ''\r\n" + 
				"	WHERE\r\n" + 
				"		SC6010.D_E_L_E_T_ = ''\r\n" + 
				"	   	AND C6_BLQ Not Like '%R%'\r\n" + 
				"		AND C5_XREFAT IN ('', '2')\r\n" + 
				"		AND C5_LIBEROK = 'S'" +
				"	   	AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" +
				"	   	AND C5_EMISSAO >= '20160801'\r\n" +
				"		AND C6_QTDVEN > C6_QTDENT\r\n" + 
				") AS RESULTSET\r\n" + 
				"GROUP BY CLASPED WITH ROLLUP\r\n" + 
				"ORDER BY VALOR DESC";
		
		ChartDataAndLabels cdl = new ChartDataAndLabels();
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("CLASPED"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getCarteiraPendente();
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ChartDataAndLabels getTopClientes(int numClientes, UNIDADE unidade, int ano) {
		String sql = "SELECT" + " TOP " + numClientes
				+ " CLIENTE, SUM(CASE C6_BLQ WHEN 'R' THEN VALOR/C6_QTDVEN*C6_QTDENT ELSE VALOR END) AS VALOR FROM (\r\n"
				+ "	SELECT\r\n"
				+ "		RTRIM(A1_COD) + ';' + RTRIM(A1_NOME) AS CLIENTE, SUBSTRING(C5_EMISSAO,1,6) AS EMISSAO,CASE A1_EST WHEN 'EX' THEN C6_VALOR * " + ConfigClass.taxaDolar + " ELSE C6_VALOR END AS VALOR,C6_BLQ,C6_PRCVEN,C6_QTDENT,C6_QTDVEN, CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '4' THEN 'EXPORTACAO' WHEN '6' THEN 'EQUIPAMENTOS' END AS CLASPED\r\n"
				+ "	FROM SC6010 WITH(NOLOCK)\r\n"
				+ _joinSC5010
				+ _joinSA1010
				+ "	WHERE\r\n"
				+ "		YEAR(C5_EMISSAO) = " + ano + "\r\n"
				+ "		AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n";
		if (unidade.equals(UNIDADE.ORTOPEDIA)) {
			sql += "AND C6_LOCAL IN ('01', '10')\r\n";
		} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
			sql += "AND C6_LOCAL IN ('11', '12', '14')\r\n";
		}
		sql += "		AND SC6010.D_E_L_E_T_ = ''\r\n"
				+ "		AND C6_FILIAL = '01'\r\n";
				sql += _C6_TES; 
				sql += "	) AS RESULTSET\r\n"
				+ "GROUP BY CLIENTE\r\n" + "ORDER BY VALOR DESC";
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("CLIENTE"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getTopClientes(numClientes, unidade, ano);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ChartDataAndLabels getVendasDetalhado(CLASPED clasped) {
		String sql = "SELECT EMISSAO, SUM(CASE C6_BLQ WHEN 'R' THEN VALOR/C6_QTDVEN*C6_QTDENT ELSE VALOR END) AS VALOR FROM (\r\n" + 
				"	SELECT\r\n" + 
				"		DAY(C5_EMISSAO) AS EMISSAO,CASE A1_EST WHEN 'EX' THEN C6_VALOR * " + ConfigClass.taxaDolar + " ELSE C6_VALOR END AS VALOR,C6_BLQ,C6_PRCVEN,C6_QTDENT,C6_QTDVEN, CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '4' THEN 'EXPORTACAO' WHEN '6' THEN 'EQUIPAMENTOS' END AS CLASPED\r\n" + 
				"	FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010 + 
				"	INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENT\r\n" + 
				"		AND C5_LOJACLI = A1_LOJA\r\n" + 
				"		AND A1_FILIAL = ''\r\n" + 
				"	WHERE MONTH(C5_EMISSAO) = MONTH(GETDATE())\r\n" + 
				"		AND YEAR(C5_EMISSAO) = YEAR(GETDATE())\r\n" + 
				"		AND SC6010.D_E_L_E_T_ = ''\r\n" + 
				"		AND C6_FILIAL = '01'\r\n";
				if (clasped.equals(CLASPED.REPOSICAO)) {
					sql += "		AND C5_CLASPED = 1\r\n";
				} else if (clasped.equals(CLASPED.CAIXAS)) {
					sql += "		AND C5_CLASPED = 2\r\n";
				} else if (clasped.equals(CLASPED.EQUIPAMENTOS)) {
					sql += "		AND C5_CLASPED IN ('3', '6')\r\n";
				} else if (clasped.equals(CLASPED.EXPORTACAO)) {
					sql += "		AND C5_CLASPED IN ('4')\r\n";
				}
				sql += "		AND C5_XREFAT IN ('', '2')\r\n"; 
				sql += _C6_TES; 
				sql += "	) AS RESULTSET\r\n" + 
				"GROUP BY EMISSAO";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("EMISSAO"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getVendasDetalhado(clasped);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ChartDataAndLabels getTopProdutos(int numProdutos, UNIDADE unidade, Integer ano) {
		String sql = "SELECT TOP " + numProdutos + " PRODUTO, SUM(VALOR) AS VALOR FROM (\r\n" + "	SELECT\r\n"
			+ "		RTRIM(C6_PRODUTO) + ';' + RTRIM(C6_DESCRI) AS PRODUTO,CASE C5_CLASPED WHEN '4' THEN C6_VALOR * " + ConfigClass.taxaDolar + " ELSE C6_VALOR END AS VALOR\r\n"
			+ "	FROM SC6010 WITH(NOLOCK)\r\n"
			+ _joinSC5010;
		if (unidade.equals(UNIDADE.ORTOPEDIA)) {
			sql += "AND C6_LOCAL IN ('01', '10')\r\n";
		} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
			sql += "AND C6_LOCAL IN ('11', '12', '14')\r\n";
		}
		sql += "	WHERE YEAR(C5_EMISSAO) = " + ano + "\r\n"
			+ "AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n"
			+ "		AND SC6010.D_E_L_E_T_ = ''\r\n"
			+ "		AND C6_FILIAL = '01'\r\n"
			+ "		AND C6_CLI <> '000422'\r\n"
			+ "		AND C6_BLQ <> 'R'"
			+ "		AND C5_XREFAT IN ('', '2')\r\n"
			+ "	) AS RESULTSET\r\n" + "GROUP BY PRODUTO\r\n" + "ORDER BY VALOR DESC";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("PRODUTO"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getTopProdutos(numProdutos, unidade, ano);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static List<PedidoVenda> getItensPedidosVendaDia(List<String> authorities) {
		String sql = "SELECT\r\n" + 
				"	A1_COD, CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '4' THEN 'EXPORTACAO' WHEN '6' THEN 'EQUIPAMENTOS' ELSE '' END AS C5_CLASPED, C6_NUM, CASE WHEN C6_LOCAL = '01' THEN 'ORTOPEDIA' WHEN C6_LOCAL IN ('11', '12', '14') THEN 'EQUIPAMENTOS' END AS UNIDADE, RTRIM(A1_NOME) AS A1_NOME, CASE A1_EST WHEN 'EX' THEN SUM(C6_VALOR * " + ConfigClass.taxaDolar + ") ELSE SUM(C6_VALOR) END AS VALOR_TOTAL\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010 + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENT\r\n" + 
				"	AND C5_LOJACLI = A1_LOJA\r\n" + 
				"	AND A1_FILIAL = ''\r\n" + 
				"WHERE MONTH(C5_EMISSAO) = MONTH(GETDATE())\r\n" + 
				"	AND YEAR(C5_EMISSAO) = YEAR(GETDATE())\r\n" + 
				"	AND DAY(C5_EMISSAO) = DAY(GETDATE())\r\n" + 
				"	AND SC6010.D_E_L_E_T_ = ''\r\n" + 
				"	AND C6_FILIAL = '01'\r\n" + 
				"	AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" +
				"	AND C6_CLI <> '000422'\r\n"; 
				sql += _C6_TES; 
				sql += "AND C5_XREFAT IN ('', '2')\r\n" + 
				"GROUP BY A1_COD, C5_CLASPED, C6_NUM, CASE WHEN C6_LOCAL = '01' THEN 'ORTOPEDIA' WHEN C6_LOCAL IN ('11', '12', '14') THEN 'EQUIPAMENTOS' END, RTRIM(A1_NOME), A1_EST\r\n" + 
				"ORDER BY VALOR_TOTAL DESC";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			List<PedidoVenda> pedidosVenda = new LinkedList<>();
			while (rs.next()) {
				PedidoVenda pedido = new PedidoVenda();
				pedido.setCliente(new Cliente().setCodigo(rs.getString("A1_COD")).setNome(rs.getString("A1_NOME")));
				pedido.setNumero(rs.getString("C6_NUM"));
				pedido.setUnidade(UNIDADE.valueOf(rs.getString("UNIDADE")));
				pedido.setClasPed(CLASPED.valueOf(rs.getString("C5_CLASPED")));
				
				if (authorities.contains("VENDAS-ORTOPEDIA")) {
					if (!pedido.getUnidade().equals(UNIDADE.ORTOPEDIA) && (!pedido.getClasPed().equals(CLASPED.CAIXAS) || !pedido.getClasPed().equals(CLASPED.REPOSICAO))) {
						continue;
					}
				} else if (authorities.contains("VENDAS-EQUIPAMENTOS")) {
					if (!pedido.getUnidade().equals(UNIDADE.EQUIPAMENTOS) && (!pedido.getClasPed().equals(CLASPED.EQUIPAMENTOS))) {
						continue;
					}
				} else if (authorities.contains("VENDAS-EXPORTACAO")) {
					if (!pedido.getUnidade().equals(UNIDADE.EXPORTACAO) && (!pedido.getClasPed().equals(CLASPED.EXPORTACAO))) {
						continue;
					}
				}
				
				pedido.setValorTotal(rs.getBigDecimal("VALOR_TOTAL").setScale(2, RoundingMode.HALF_EVEN));
				pedidosVenda.add(pedido);
			}
			return pedidosVenda;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static PedidoVenda findOne(String numero) {
		String sql = "SELECT\r\n" + 
				"C5_COMIS1, C5_COMIS2, C5_COMIS3, C5_COMIS4, C5_COMIS5, C5_EMISSAO,CASE C5_XTPVEND WHEN '1' THEN 'REPRESENTANTE' WHEN '2' THEN 'DISTRIBUIDOR' WHEN '3' THEN 'LICITACAO' WHEN '4' THEN 'VENDA DIRETA' ELSE '' END AS C5_XTPVEND,C6_NUM,A1_NOME,A1_COD,C5_XDTENTR,CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '4' THEN 'EXPORTACAO' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '6' THEN 'EQUIPAMENTOS' END AS C5_CLASPED\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010.replace("AND C5_CLIENTE <> '000422'\r\n", "") + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENTE\r\n" + 
				"AND A1_FILIAL = ''\r\n" + 
				"AND SA1010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE\r\n" + 
				"C6_NUM = ?\r\n" + 
				"AND C6_FILIAL = '01'\r\n" + 
				"AND C6_BLQ <> 'R'" +
				"AND SC6010.D_E_L_E_T_ = ''";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setString(1, numero);
			ResultSet rs = stmt.executeQuery();
			PedidoVenda pedido = new PedidoVenda();

			while (rs.next()) {
				pedido.setNumero(rs.getString("C6_NUM"));
				pedido.setCliente(new Cliente().setNome(rs.getString("A1_NOME")).setCodigo(rs.getString("A1_COD")));
				pedido.setDataEmissao(rs.getString("C5_EMISSAO"));
				pedido.setDataEntrega(rs.getString("C5_XDTENTR"));
				pedido.setClasPed(CLASPED.valueOf(rs.getString("C5_CLASPED")));
				pedido.setTipoVenda(rs.getString("C5_XTPVEND"));
				pedido.setComis1(rs.getBigDecimal("C5_COMIS1"));
				pedido.setComis2(rs.getBigDecimal("C5_COMIS2"));
				pedido.setComis3(rs.getBigDecimal("C5_COMIS3"));
				pedido.setComis4(rs.getBigDecimal("C5_COMIS4"));
				pedido.setComis5(rs.getBigDecimal("C5_COMIS5"));
			}
			pedido.setItensPedidoVenda(getItensPedidoVenda(numero));
			return pedido;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return findOne(numero);
			} else {
				e.printStackTrace();
			}
		}

		return null;
	}

	public static <E> List<ItemPedidoVenda> getItensPedidoVenda(String numero) {
		String sql = "SELECT\r\n" + 
				"B1_X_CUST, RTRIM(C6_ITEM) AS C6_ITEM, RTRIM(C6_PRODUTO) AS C6_PRODUTO, B1_DESC, C6_QTDVEN, C6_QTDENT, CASE C5_MOEDA WHEN '1' THEN C6_PRCVEN WHEN '2' THEN C6_PRCVEN * " + ConfigClass.taxaDolar + " END AS C6_PRCVEN, CASE C5_MOEDA WHEN '1' THEN C6_VALOR WHEN '2' THEN C6_VALOR * " + ConfigClass.taxaDolar + " END AS C6_VALOR, C5_MOEDA\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010.replace("AND C5_CLIENTE <> '000422'\r\n", "") + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON B1_COD = C6_PRODUTO\r\n" + 
				"AND B1_FILIAL = ''\r\n" + 
				"AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE\r\n" + 
				"C6_NUM = ?\r\n" + 
				"AND C6_FILIAL = '01'\r\n" + 
				"AND C6_BLQ <> 'R'\r\n" +
				"AND SC6010.D_E_L_E_T_ = ''";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setString(1, numero);
			ResultSet rs = stmt.executeQuery();
			LinkedList<ItemPedidoVenda> itensPedidoVenda = new LinkedList<>();
			while (rs.next()) {
				ItemPedidoVenda item = new ItemPedidoVenda();
				item.setItem(rs.getString("C6_ITEM"));
				item.setProduto(new Produto().setCodigo(rs.getString("C6_PRODUTO")).setDescricao(rs.getString("B1_DESC")));
				item.setQtdVen(rs.getBigDecimal("C6_QTDVEN"));
				item.setQtdEnt(rs.getBigDecimal("C6_QTDENT"));
				item.setPrcVen(rs.getBigDecimal("C6_PRCVEN"));
				item.setCusto(rs.getBigDecimal("B1_X_CUST"));
				itensPedidoVenda.add(item);
			}
			return itensPedidoVenda;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getItensPedidoVenda(numero);
			} else {
				e.printStackTrace();
			}
		}

		return null;
	}

	public static ChartDataAndLabels getHistoricoVendas(Cliente cliente) {
		String sql = "SELECT EMISSAO, SUM(CASE C6_BLQ WHEN 'R' THEN VALOR/C6_QTDVEN*C6_QTDENT ELSE VALOR END) AS VALOR FROM (\r\n"
				+ "SELECT\r\n"
				+ "SUBSTRING(C5_EMISSAO,1,6) AS EMISSAO,CASE A1_EST WHEN 'EX' THEN C6_VALOR * " + ConfigClass.taxaDolar + " ELSE C6_VALOR END AS VALOR,C6_BLQ,C6_PRCVEN,C6_QTDENT,C6_QTDVEN\r\n"
				+ "FROM SC6010 WITH(NOLOCK)\r\n"
				+ _joinSC5010;
				sql += "INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENT\r\n"
				+ "AND C5_LOJACLI = A1_LOJA\r\n"
						+ "AND A1_FILIAL = ''\r\n"
				+ "WHERE ";
				sql += "YEAR(C5_EMISSAO) >= YEAR(DATEADD(YEAR, -3, GETDATE()))\r\n";
				sql += "AND SC6010.D_E_L_E_T_ = ''\r\n"
				+ "AND C6_FILIAL = '01'\r\n";
				sql += _C6_TES;
				sql += "AND C5_CLIENTE = ?\r\n" +
				"AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n"
				+ "AND C5_XREFAT IN ('', '2')\r\n"
				+ ") AS RESULTSET\r\n" + "GROUP BY EMISSAO\r\n" + "ORDER BY EMISSAO";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setString(1, cliente.getCodigo());
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				cdl.getChartLabels().add(DataUtils.formatMes(rs.getString("EMISSAO")));
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getHistoricoVendas(cliente);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ChartDataAndLabels getProdutosMaisComprados(String codCliente) {
		String sql = "SELECT\r\n" + 
				"TOP 15 RTRIM(C6_PRODUTO) + ';' + RTRIM(C6_DESCRI) AS C6_PRODUTO,\r\n" + 
				"SUM(C6_VALOR) AS VALOR\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010;
				sql += _C6_TES; 
				sql += "AND C5_EMISSAO >= DATEADD(MONTH, -12, GETDATE())\r\n" + 
				"WHERE C6_FILIAL = '01'\r\n" + 
				"AND SC6010.D_E_L_E_T_ = ''\r\n" + 
				"AND C6_BLQ <> 'R'\r\n" +
				"AND C5_CLIENTE = ?\r\n" +
				"AND C5_LIBEROK = 'S'\r\n" +
				"GROUP BY RTRIM(C6_PRODUTO) + ';' + RTRIM(C6_DESCRI)\r\n" + 
				"ORDER BY VALOR DESC\r\n" + 
				"";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setString(1, codCliente);
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
				cdl.getChartLabels().add(rs.getString("C6_PRODUTO"));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getProdutosMaisComprados(codCliente);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static LinkedList<ItemPedidoVenda> getCarteiraPendente(String codCliente) {
		String sql = "SELECT C5_CLASPED,C6_NUM, A1_NREDUZ, C5_EMISSAO, C6_ENTREG, C6_PRODUTO, C6_DESCRI,C6_PRCVEN, C6_QTDVEN, C6_QTDENT, QTD_EMP, (C6_QTDVEN - C6_QTDENT - QTD_EMP) AS SALDO, ((C6_QTDVEN - C6_QTDENT) * C6_PRCVEN) AS VALOR_SALDO,\r\n" + 
				"(ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = B1_LOCPAD AND D_E_L_E_T_ = ''), 0) + ISNULL((SELECT SUM(DA_SALDO) FROM SDA010 WITH(NOLOCK) WHERE DA_PRODUTO = B1_COD AND DA_SALDO <> 0 AND D_E_L_E_T_ = '' AND DA_LOCAL = B1_LOCPAD), 0)) AS QTD_ESTOQUE,\r\n" + 
				"ISNULL((SELECT SUM(C2_QUANT - C2_QUJE - C2_PERDA) FROM SC2010 WITH(NOLOCK) WHERE C2_PRODUTO = B1_COD AND C2_DATRF='' AND C2_LOCAL=B1_LOCPAD AND D_E_L_E_T_='' AND C2_STATUS IN ('N', '')), 0) AS QTD_OP\r\n" + 
				"FROM (\r\n" + 
				"SELECT\r\n" + 
				"CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '4' THEN 'EXPORTACAO' WHEN '6' THEN 'EQUIPAMENTOS' END AS C5_CLASPED,C6_NUM,C5_EMISSAO,C6_ENTREG,C6_PRODUTO,C6_DESCRI,C6_QTDVEN,C6_QTDENT,\r\n" + 
				"(SELECT ISNULL(SUM(C9_QTDLIB), 0) FROM SC9010 WITH(NOLOCK) WHERE C9_PEDIDO = C5_NUM AND C9_ITEM = C6_ITEM AND C9_PRODUTO = C6_PRODUTO AND SC9010.D_E_L_E_T_ = '' AND C9_NFISCAL = '' AND C9_LOTECTL <> '') AS QTD_EMP,\r\n" + 
				"CASE A1_EST WHEN 'EXT' THEN C6_PRCVEN * 3.6 ELSE C6_PRCVEN END AS C6_PRCVEN,A1_NREDUZ\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010 + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENT\r\n" + 
				"AND A1_LOJA = C5_LOJACLI\r\n" + 
				"AND A1_FILIAL = ''\r\n" + 
				"WHERE\r\n" + 
				"C6_QTDVEN > C6_QTDENT\r\n" + 
				"AND C5_CLIENTE = '" + codCliente + "'" +
				"AND C6_BLQ <> 'R'\r\n" + 
				"AND SC6010.D_E_L_E_T_ = ''\r\n" + 
				"AND C5_LIBEROK = 'S'\r\n" +
				"AND C5_CLASPED IN ('1', '2', '3', '4', '6')" +
				"AND C5_CLIENTE <> '000422') AS RESULTSET\r\n" + 
				"INNER JOIN SB1010 ON C6_PRODUTO = B1_COD\r\n" + 
				"AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"ORDER BY C5_EMISSAO";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<ItemPedidoVenda> itensVenda = new LinkedList<>();
			while(rs.next()) {
				ItemPedidoVenda itemVenda = new ItemPedidoVenda();
				itemVenda.setPedido(new PedidoVenda().setNumero(rs.getString("C6_NUM")).setDataEmissao(rs.getString("C5_EMISSAO")).setDataEntrega(rs.getString("C6_ENTREG")));
				itemVenda.setProduto(new Produto().setCodigo(rs.getString("C6_PRODUTO")).setDescricao(rs.getString("C6_DESCRI")));
				itemVenda.setPrcVen(rs.getBigDecimal("C6_PRCVEN").setScale(2, RoundingMode.HALF_EVEN));
				itemVenda.setQtdVen(rs.getBigDecimal("C6_QTDVEN").setScale(2, RoundingMode.HALF_EVEN));
				itemVenda.setQtdEnt(rs.getBigDecimal("C6_QTDENT").setScale(2, RoundingMode.HALF_EVEN));
				itemVenda.setQtdEmp(rs.getBigDecimal("QTD_EMP").setScale(2, RoundingMode.HALF_EVEN));
				itensVenda.add(itemVenda);
			}
			return itensVenda;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getCarteiraPendente(codCliente);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ChartDataAndLabels getCompradoresProduto(String codigoProduto) {
		String sql = "SELECT TOP 15\r\n" + 
				"A1_COD, RTRIM(A1_NREDUZ) AS NOME,SUM(C6_QTDVEN) AS QTD_COMPRADA\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON C6_PRODUTO = B1_COD\r\n" + 
				"AND B1_FILIAL = ''\r\n" + 
				"AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				_joinSC5010 +
				"INNER JOIN SA1010 WITH(NOLOCK) ON C5_CLIENT = A1_COD\r\n" + 
				"AND C5_LOJACLI = A1_LOJA\r\n" + 
				"AND SA1010.D_E_L_E_T_ = ''\r\n" + 
				"AND A1_FILIAL = ''\r\n" + 
				"WHERE SC6010.D_E_L_E_T_ = ''\r\n" + 
				"AND C6_PRODUTO = ?\r\n" + 
				"AND C6_FILIAL = '01'\r\n" +
				"AND C6_BLQ <> 'R'\r\n" +
				"AND C5_LIBEROK = 'S'\r\n" +
				"AND C5_EMISSAO > DATEADD(MONTH, -24, GETDATE())\r\n" +
				"AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" +
				"GROUP BY A1_COD, A1_NREDUZ\r\n" + 
				"ORDER BY QTD_COMPRADA DESC";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setString(1, codigoProduto);
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("A1_COD") + ";" + rs.getString("NOME"));
				cdl.getChartData().add(rs.getBigDecimal("QTD_COMPRADA"));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getCompradoresProduto(codigoProduto);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static Iterable<ItemPedidoVenda> getCarteiraPendente(Produto produto) {
		String sql = "SELECT C5_CLASPED,C6_NUM, A1_NREDUZ, C5_EMISSAO, C6_ENTREG, C6_PRODUTO, C6_DESCRI,C6_PRCVEN, C6_QTDVEN, C6_QTDENT, QTD_EMP, (C6_QTDVEN - C6_QTDENT - QTD_EMP) AS SALDO, ((C6_QTDVEN - C6_QTDENT - QTD_EMP) * C6_PRCVEN) AS VALOR_SALDO,\r\n" + 
				"(ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = B1_LOCPAD AND D_E_L_E_T_ = ''), 0) + ISNULL((SELECT SUM(DA_SALDO) FROM SDA010 WITH(NOLOCK) WHERE DA_PRODUTO = B1_COD AND DA_SALDO <> 0 AND D_E_L_E_T_ = '' AND DA_LOCAL = B1_LOCPAD), 0)) AS QTD_ESTOQUE,\r\n" + 
				"ISNULL((SELECT SUM(C2_QUANT - C2_QUJE - C2_PERDA) FROM SC2010 WITH(NOLOCK) WHERE C2_PRODUTO = B1_COD AND C2_DATRF='' AND C2_LOCAL=B1_LOCPAD AND D_E_L_E_T_='' AND C2_STATUS IN ('N', '')), 0) AS QTD_OP\r\n" + 
				"FROM (\r\n" + 
				"SELECT\r\n" + 
				"CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '4' THEN 'EXPORTACAO' WHEN '6' THEN 'EQUIPAMENTOS' END AS C5_CLASPED,C6_NUM,C5_EMISSAO,C6_ENTREG,C6_PRODUTO,C6_DESCRI,C6_QTDVEN,C6_QTDENT,\r\n" + 
				"(SELECT ISNULL(SUM(C9_QTDLIB), 0) FROM SC9010 WITH(NOLOCK) WHERE C9_PEDIDO = C5_NUM AND C9_ITEM = C6_ITEM AND C9_PRODUTO = C6_PRODUTO AND SC9010.D_E_L_E_T_ = '' AND C9_NFISCAL = '' AND C9_LOTECTL <> '') AS QTD_EMP,\r\n" + 
				"CASE A1_EST WHEN 'EXT' THEN C6_PRCVEN * 3.6 ELSE C6_PRCVEN END AS C6_PRCVEN,A1_NREDUZ\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010 + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENT\r\n" + 
				"AND A1_LOJA = C5_LOJACLI\r\n" + 
				"AND A1_FILIAL = ''\r\n" + 
				"WHERE\r\n" + 
				"C6_QTDVEN > C6_QTDENT\r\n" + 
				"AND C6_BLQ <> 'R'\r\n" + 
				"AND SC6010.D_E_L_E_T_ = ''\r\n" + 
				"AND C5_LIBEROK = 'S'\r\n" +
				"AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" +
				"AND C6_PRODUTO = '" + produto.getCodigo() + "'\r\n" +
				"AND C5_CLIENTE <> '000422') AS RESULTSET\r\n" + 
				"INNER JOIN SB1010 ON C6_PRODUTO = B1_COD\r\n" + 
				"AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"ORDER BY C5_EMISSAO";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<ItemPedidoVenda> itensVenda = new LinkedList<>();
			while(rs.next()) {
				ItemPedidoVenda itemVenda = new ItemPedidoVenda();
				itemVenda.setPedido(new PedidoVenda().setNumero(rs.getString("C6_NUM")).setDataEmissao(rs.getString("C5_EMISSAO")).setDataEntrega(rs.getString("C6_ENTREG")).setCliente(new Cliente().setnReduz(rs.getString("A1_NREDUZ"))));
				itemVenda.setProduto(new Produto().setCodigo(rs.getString("C6_PRODUTO")).setDescricao(rs.getString("C6_DESCRI")));
				itemVenda.setPrcVen(rs.getBigDecimal("C6_PRCVEN").setScale(2, RoundingMode.HALF_EVEN));
				itemVenda.setQtdVen(rs.getBigDecimal("C6_QTDVEN").setScale(2, RoundingMode.HALF_EVEN));
				itemVenda.setQtdEnt(rs.getBigDecimal("C6_QTDENT").setScale(2, RoundingMode.HALF_EVEN));
				itemVenda.setQtdEmp(rs.getBigDecimal("QTD_EMP").setScale(2, RoundingMode.HALF_EVEN));
				itensVenda.add(itemVenda);
			}
			return itensVenda;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getCarteiraPendente(produto);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static DataTable getCarteiraPendenteTabela(CLASPED clasped) {
		String sql = "SELECT BM_DESC, C6_XLIBPED, A1_NOME, C6_LOCAL, UNIDADE,A1_MSBLQL,C5_CLASPED,C6_NUM, A1_NREDUZ, C5_EMISSAO, C6_ENTREG, C6_PRODUTO, C6_DESCRI,C6_PRCVEN, C6_QTDVEN, C6_QTDENT, QTD_EMP, (C6_QTDVEN - C6_QTDENT - QTD_EMP) AS SALDO, ((C6_QTDVEN - C6_QTDENT - QTD_EMP) * C6_PRCVEN) AS VALOR_SALDO,\r\n" + 
				"(ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = B1_LOCPAD AND D_E_L_E_T_ = ''), 0) + ISNULL((SELECT SUM(DA_SALDO) FROM SDA010 WITH(NOLOCK) WHERE DA_PRODUTO = B1_COD AND DA_SALDO <> 0 AND D_E_L_E_T_ = '' AND DA_LOCAL = B1_LOCPAD), 0)) AS QTD_ESTOQUE,\r\n" + 
				"ISNULL((SELECT SUM(C2_QUANT - C2_QUJE - C2_PERDA) FROM SC2010 WITH(NOLOCK) WHERE C2_PRODUTO = B1_COD AND C2_DATRF='' AND C2_LOCAL=B1_LOCPAD AND D_E_L_E_T_='' AND C2_STATUS IN ('N', '')), 0) AS QTD_OP\r\n" + 
				"FROM (\r\n" + 
				"SELECT\r\n" + 
				"C6_XLIBPED, A1_NOME, C6_LOCAL, CASE WHEN C6_LOCAL = '01' THEN 'ORTOPEDIA' WHEN C6_LOCAL IN ('11', '12', '14') THEN 'EQUIPAMENTOS' END AS UNIDADE,CASE A1_MSBLQL WHEN '0' THEN 'ATIVO' WHEN '1' THEN 'BLOQUEADO' WHEN '2' THEN 'ATIVO' END AS A1_MSBLQL, CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '4' THEN 'EXPORTACAO' WHEN '6' THEN 'EQUIPAMENTOS' END AS C5_CLASPED,C6_NUM,C5_EMISSAO,C6_ENTREG,C6_PRODUTO,C6_DESCRI,C6_QTDVEN,C6_QTDENT,\r\n" + 
				"(SELECT ISNULL(SUM(C9_QTDLIB), 0) FROM SC9010 WITH(NOLOCK) WHERE C9_PEDIDO = C5_NUM AND C9_ITEM = C6_ITEM AND C9_PRODUTO = C6_PRODUTO AND SC9010.D_E_L_E_T_ = '' AND C9_NFISCAL = '' AND C9_LOTECTL <> '') AS QTD_EMP,\r\n" + 
				"CASE A1_EST WHEN 'EXT' THEN C6_PRCVEN * 3.6 ELSE C6_PRCVEN END AS C6_PRCVEN,A1_NREDUZ\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010+ 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENT\r\n" + 
				"AND A1_LOJA = C5_LOJACLI\r\n" + 
				"AND A1_FILIAL = ''\r\n" + 
				"WHERE\r\n" + 
				"C6_QTDVEN > C6_QTDENT\r\n" + 
				"AND C6_BLQ <> 'R'\r\n" + 
				"AND SC6010.D_E_L_E_T_ = ''\r\n" +
				"AND C5_LIBEROK = 'S'\r\n";
				if (clasped == null) {
					sql += "AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n";
				} else if (clasped.equals(CLASPED.EQUIPAMENTOS)) {
					sql += "AND C5_CLASPED IN ('3', '6')\r\n";
				} else if (clasped.equals(CLASPED.EXPORTACAO)) {
					sql += "AND C5_CLASPED = '4'\r\n";
				} else if (clasped.equals(CLASPED.CAIXAS)) {
					sql += "AND C5_CLASPED = '2'\r\n";
				} else if (clasped.equals(CLASPED.REPOSICAO)) {
					sql += "AND C5_CLASPED = '1'\r\n";
				}
				sql += "AND C5_CLIENTE <> '000422') AS RESULTSET\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON C6_PRODUTO = B1_COD\r\n" +
				"INNER JOIN SBM010 WITH(NOLOCK) ON BM_FILIAL <> 'ZZ' AND BM_GRUPO = B1_GRUPO AND SBM010.D_E_L_E_T_ = ''\r\n" +
				"AND SB1010.D_E_L_E_T_ = ''\r\n" +
				"ORDER BY C5_EMISSAO";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while(rs.next()) {
				Collection<Object> row = new LinkedList<>();
				row.add(rs.getString("UNIDADE"));
				row.add(rs.getString("C6_LOCAL"));
				row.add(HtmlUtils.createLink("/pedidos-venda/" + rs.getString("C6_NUM") + "/detalhes", rs.getString("C6_NUM")));
				row.add(rs.getString("A1_MSBLQL"));
				row.add(rs.getString("A1_NOME"));
				row.add(rs.getString("C5_CLASPED"));
				row.add(rs.getString("C5_EMISSAO"));
				row.add(rs.getString("C6_XLIBPED"));
				row.add(rs.getString("C6_ENTREG"));
				row.add(HtmlUtils.createLink("/produtos/detalhes?codigoProduto=" + rs.getString("C6_PRODUTO"), rs.getString("C6_PRODUTO")));
				row.add(rs.getString("C6_DESCRI"));
				row.add(rs.getString("BM_DESC"));
				row.add(rs.getString("C6_QTDVEN"));
				row.add(rs.getString("C6_QTDENT"));
				row.add(rs.getString("QTD_EMP"));
				row.add(rs.getString("SALDO"));
				row.add(rs.getBigDecimal("QTD_ESTOQUE").setScale(2, RoundingMode.HALF_EVEN));
				row.add(rs.getString("QTD_OP"));
				row.add(rs.getBigDecimal("C6_PRCVEN"));
				row.add(rs.getBigDecimal("VALOR_SALDO"));
				data.add(row);
			}
			return new DataTable().setData(data);
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getCarteiraPendenteTabela(clasped);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static File getCarteiraPendenteExcel() {
		String sql = "SELECT C5_CLASPED,C6_NUM, A1_NREDUZ, C5_EMISSAO, C6_ENTREG, C6_PRODUTO, C6_DESCRI,C6_PRCVEN, C6_QTDVEN, C6_QTDENT, QTD_EMP, (C6_QTDVEN - C6_QTDENT - QTD_EMP) AS SALDO, ((C6_QTDVEN - C6_QTDENT - QTD_EMP) * C6_PRCVEN) AS VALOR_SALDO, C6_PRCVEN * QTD_EMP AS VALOR_EMPENHADO FROM (\r\n" + 
				"SELECT\r\n" + 
				"CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '4' THEN 'EXPORTACAO' WHEN '6' THEN 'EQUIPAMENTOS' END AS C5_CLASPED,C6_NUM,C5_EMISSAO,C6_ENTREG,C6_PRODUTO,C6_DESCRI,C6_QTDVEN,C6_QTDENT,ISNULL(C9_QTDLIB, 0) AS QTD_EMP,CASE A1_EST WHEN 'EXT' THEN C6_PRCVEN * 3.6 ELSE C6_PRCVEN END AS C6_PRCVEN,A1_NREDUZ\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010 + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENT\r\n" + 
				"AND A1_LOJA = C5_LOJACLI\r\n" + 
				"AND A1_FILIAL = ''\r\n" + 
				"LEFT JOIN SC9010 WITH(NOLOCK) ON C9_PEDIDO = C5_NUM\r\n" + 
				"AND C9_ITEM = C6_ITEM\r\n" + 
				"AND C9_PRODUTO = C6_PRODUTO\r\n" + 
				"AND SC9010.D_E_L_E_T_ = ''\r\n" + 
				"AND C9_NFISCAL = ''\r\n" + 
				"AND C9_LOTECTL <> ''\r\n" +
				"WHERE\r\n" + 
				"C6_QTDVEN > C6_QTDENT\r\n" + 
				"AND C6_BLQ <> 'R'\r\n" + 
				"AND C5_LIBEROK = 'S'\r\n" + 
				"AND C5_CLASPED IN ('1', '2', '3', '4', '6')" +
				"AND SC6010.D_E_L_E_T_ = ''\r\n";
				sql += _C6_TES;
				sql += "AND C5_CLIENTE <> '000422'" +
				") AS RESULTSET\r\n" + 
				"ORDER BY C5_EMISSAO";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			File file = File.createTempFile("Carteira-Excel", ".xls");
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("Carteira");
			int rownum = 0;
			int column = 0;
			HSSFRow row = sheet.createRow(rownum++);
			row.createCell(column++).setCellValue("Numero");
			row.createCell(column++).setCellValue("Cliente");
			row.createCell(column++).setCellValue("Clas. Ped");
			row.createCell(column++).setCellValue("Emissao");
			row.createCell(column++).setCellValue("Entrega");
			row.createCell(column++).setCellValue("Produto");
			row.createCell(column++).setCellValue("Descricao");
			row.createCell(column++).setCellValue("Qtd Vend");
			row.createCell(column++).setCellValue("Qtd Ent");
			row.createCell(column++).setCellValue("Qtd Emp");
			row.createCell(column++).setCellValue("Saldo");
			row.createCell(column++).setCellValue("Preço Venda");
			row.createCell(column++).setCellValue("Valor Saldo");
			row.createCell(column++).setCellValue("Valor Empenhado");
			while(rs.next()) {
				row = sheet.createRow(rownum++);
				column = 0;
				row.createCell(column++).setCellValue(rs.getString("C6_NUM"));
				row.createCell(column++).setCellValue(rs.getString("A1_NREDUZ"));
				row.createCell(column++).setCellValue(rs.getString("C5_CLASPED"));
				row.createCell(column++).setCellValue(rs.getString("C5_EMISSAO"));
				row.createCell(column++).setCellValue(rs.getString("C6_ENTREG"));
				row.createCell(column++).setCellValue(rs.getString("C6_PRODUTO"));
				row.createCell(column++).setCellValue(rs.getString("C6_DESCRI"));
				row.createCell(column++).setCellValue(rs.getDouble("C6_QTDVEN"));
				row.createCell(column++).setCellValue(rs.getDouble("C6_QTDENT"));
				row.createCell(column++).setCellValue(rs.getDouble("QTD_EMP"));
				row.createCell(column++).setCellValue(rs.getDouble("SALDO"));
				row.createCell(column++).setCellValue(rs.getDouble("C6_PRCVEN"));
				row.createCell(column++).setCellValue(rs.getDouble("VALOR_SALDO"));
				row.createCell(column++).setCellValue(rs.getDouble("VALOR_EMPENHADO"));
			}
			workbook.write(file);
			workbook.close();
			return file;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getCarteiraPendenteExcel();
			} else {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static ChartDataAndLabels getVendasAno(int ano, UNIDADE unidade) {
		String sql = "SELECT EMISSAO, SUM(CASE C6_BLQ WHEN 'R' THEN VALOR/C6_QTDVEN*C6_QTDENT ELSE VALOR END) AS VALOR FROM (\r\n"
				+ "SELECT\r\n"
				+ "SUBSTRING(C5_EMISSAO,1,6) AS EMISSAO,CASE A1_EST WHEN 'EX' THEN C6_VALOR * " + ConfigClass.taxaDolar + " ELSE C6_VALOR END AS VALOR,C6_BLQ,C6_PRCVEN,C6_QTDENT,C6_QTDVEN\r\n"
				+ "FROM SC6010 WITH(NOLOCK)\r\n"
				+ _joinSC5010;
				if (unidade.equals(UNIDADE.ORTOPEDIA)) {
					sql += "AND C6_LOCAL = '01'\r\n";
				} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
					sql += "AND C6_LOCAL IN ('11', '12', '14')\r\n";
				}
				sql += "INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENT\r\n"
				+ "AND C5_LOJACLI = A1_LOJA\r\n"
						+ "AND A1_FILIAL = ''\r\n"
				+ "WHERE ";
				sql += "YEAR(C5_EMISSAO) = '" + ano + "'\r\n";
				sql += "AND SC6010.D_E_L_E_T_ = ''\r\n"
				+ "AND C6_CLI <> '000422'"
				+ "AND C6_FILIAL = '01'\r\n";
				sql += _C6_TES;
				sql += "AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n"
				+ "AND C5_XREFAT IN ('', '2')\r\n"
				+ "AND C5_LIBEROK = 'S'\r\n"
				+ ") AS RESULTSET\r\n" + "GROUP BY EMISSAO\r\n" + "ORDER BY EMISSAO";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("EMISSAO"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getVendasAno(ano, unidade);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ChartDataAndLabels getVendasAnoHorizontal(int ano, UNIDADE unidade) {
		String sql = "SELECT EMISSAO, SUM(CASE C6_BLQ WHEN 'R' THEN VALOR/C6_QTDVEN*C6_QTDENT ELSE VALOR END) AS VALOR FROM (\r\n"
				+ "SELECT\r\n"
				+ "SUBSTRING(C5_EMISSAO,1,4) AS EMISSAO,CASE A1_EST WHEN 'EX' THEN C6_VALOR * " + ConfigClass.taxaDolar + " ELSE C6_VALOR END AS VALOR,C6_BLQ,C6_PRCVEN,C6_QTDENT,C6_QTDVEN\r\n"
				+ "FROM SC6010 WITH(NOLOCK)\r\n"
				+ _joinSC5010;
				if (unidade.equals(UNIDADE.ORTOPEDIA)) {
					sql += "AND C6_LOCAL = '01'\r\n";
				} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
					sql += "AND C6_LOCAL IN ('11', '12', '14')\r\n";
				}
				sql += "INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENT\r\n"
				+ "AND C5_LOJACLI = A1_LOJA\r\n"
				+ "AND A1_FILIAL = ''\r\n"
				+ "WHERE "
				+ "YEAR(C5_EMISSAO) = '" + ano + "'\r\n"
				+ "AND SC6010.D_E_L_E_T_ = ''\r\n"
				+ "AND C6_CLI <> '000422'"
				+ "AND C6_FILIAL = '01'\r\n";
				sql += _C6_TES;
				sql += "AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n"
				+ "AND C5_XREFAT IN ('', '2')\r\n"
				+ "AND C5_LIBEROK = 'S'\r\n"
				+ ") AS RESULTSET\r\n"
				+ "GROUP BY EMISSAO\r\n"
				+ "ORDER BY EMISSAO";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("EMISSAO"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getVendasAnoHorizontal(ano, unidade);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ChartDataAndLabels getVendasHorizontal(int ano) {
		String sql = "SELECT C5_CLASPED, SUM(CASE C6_BLQ WHEN 'R' THEN VALOR/C6_QTDVEN*C6_QTDENT ELSE VALOR END) AS VALOR FROM (\r\n" + 
				"SELECT\r\n" + 
				"CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '4' THEN 'EXPORTACAO' WHEN '6' THEN 'EQUIPAMENTOS' END AS C5_CLASPED, CASE A1_EST WHEN 'EX' THEN C6_VALOR * 3.6 ELSE C6_VALOR END AS VALOR,C6_BLQ,C6_PRCVEN,C6_QTDENT,C6_QTDVEN\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010 + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENT\r\n" + 
				"AND C5_LOJACLI = A1_LOJA\r\n" + 
				"AND A1_FILIAL = ''\r\n" + 
				"WHERE YEAR(C5_EMISSAO) = " + ano + "\r\n" + 
				"AND SC6010.D_E_L_E_T_ = ''\r\n" + 
				"AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" +
				"AND C6_CLI <> '000422'AND C6_FILIAL = '01'\r\n";
				sql += _C6_TES; 
				sql += "AND C5_XREFAT IN ('', '2')\r\n" + 
				"AND C6_LOCAL IN ('01', '11', '12', '14')) AS RESULTSET\r\n" + 
				"GROUP BY C5_CLASPED";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("C5_CLASPED"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getVendasHorizontal(ano);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ChartDataAndLabels getIndicadoresEntregas(Cliente cliente, Integer ano) {
		String sql = "SELECT CASE WHEN LEADTIME < 30 THEN '< 30' WHEN LEADTIME < 60 THEN '< 60' WHEN LEADTIME < 90 THEN '< 90' ELSE '>= 90' END AS LEADTIME, COUNT(CASE WHEN LEADTIME < 30 THEN '< 30' WHEN LEADTIME < 60 THEN '<60' WHEN LEADTIME < 90 THEN '< 90' ELSE '>= 90' END) AS OCORRENCIAS FROM (\r\n" + 
				"SELECT DATEDIFF(DAY, C5_EMISSAO, D2_EMISSAO) LEADTIME\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010 + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENT\r\n" + 
				"AND C5_LOJACLI = A1_LOJA\r\n" + 
				"AND A1_FILIAL = ''\r\n" + 
				"INNER JOIN SD2010 WITH(NOLOCK) ON D2_PEDIDO = C6_NUM\r\n" + 
				"AND D2_ITEMPV = C6_ITEM\r\n" + 
				"AND D2_LOJA = C5_LOJACLI\r\n" + 
				"AND SD2010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE\r\n" + 
				"SC6010.D_E_L_E_T_ = ''\r\n" + 
				"AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" +
				"AND C6_FILIAL = '01'\r\n";
				sql += _C6_TES;
				sql += "AND C5_XREFAT IN ('', '2')\r\n" + 
				"AND C6_CLI = ?\r\n" + 
				"AND YEAR(C5_EMISSAO) = ?\r\n" + 
				") AS RESULTSET\r\n" + 
				"GROUP BY CASE WHEN LEADTIME < 30 THEN '< 30' WHEN LEADTIME < 60 THEN '< 60' WHEN LEADTIME < 90 THEN '< 90' ELSE '>= 90' END\r\n" + 
				"ORDER BY CASE WHEN LEADTIME < 30 THEN '< 30' WHEN LEADTIME < 60 THEN '< 60' WHEN LEADTIME < 90 THEN '< 90' ELSE '>= 90' END";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setString(1, cliente.getCodigo());
			stmt.setInt(2, ano);
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("LEADTIME"));
				cdl.getChartData().add(rs.getBigDecimal("OCORRENCIAS").setScale(0, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getIndicadoresEntregas(cliente, ano);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static Iterable<ItemPedidoVenda> getProdutosCarteira(UNIDADE unidade) {
		String sql = "SELECT C6_PRODUTO, SUM(C6_QTDVEN) AS C6_QTDVEN, SUM(C6_QTDENT) AS C6_QTDENT, SUM(QTD_EMP) AS QTD_EMP FROM (\r\n" + 
				"SELECT\r\n" + 
				"C6_NUM,C6_PRODUTO,C6_QTDVEN,C6_QTDENT,ISNULL(C9_QTDLIB, 0) AS QTD_EMP\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010 + 
				"LEFT JOIN SC9010 WITH(NOLOCK) ON C9_PEDIDO = C5_NUM\r\n" + 
				"AND C9_ITEM = C6_ITEM\r\n" + 
				"AND C9_PRODUTO = C6_PRODUTO\r\n" + 
				"AND SC9010.D_E_L_E_T_ = ''\r\n" + 
				"AND C9_NFISCAL = ''\r\n" + 
				"AND C9_LOTECTL <> ''\r\n" + 
				"WHERE\r\n" + 
				"C6_QTDVEN > C6_QTDENT\r\n" + 
				"AND C5_LIBEROK = 'S'\r\n" + 
				"AND C6_BLQ <> 'R'\r\n" + 
				"AND SC6010.D_E_L_E_T_ = ''\r\n";
				sql += (unidade.equals(UNIDADE.ORTOPEDIA)) ? "AND C6_LOCAL = '01'\r\n" : (unidade.equals(UNIDADE.EQUIPAMENTOS)) ? "AND C6_LOCAL IN ('11', '12', '14')\r\n" : "";
				sql += ") AS RESULTSET\r\n" + 
				"GROUP BY C6_PRODUTO\r\n" + 
				"ORDER BY C6_PRODUTO";
		
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<ItemPedidoVenda> itensVenda = new LinkedList<>();
			while(rs.next()) {
				ItemPedidoVenda itemVenda = new ItemPedidoVenda();
				itemVenda.setProduto(new Produto().setCodigo(rs.getString("C6_PRODUTO")));
				itemVenda.setQtdVen(rs.getBigDecimal("C6_QTDVEN").setScale(2, RoundingMode.HALF_EVEN));
				itemVenda.setQtdEnt(rs.getBigDecimal("C6_QTDENT").setScale(2, RoundingMode.HALF_EVEN));
				itemVenda.setQtdEmp(rs.getBigDecimal("QTD_EMP").setScale(2, RoundingMode.HALF_EVEN));
				itensVenda.add(itemVenda);
			}
			return itensVenda;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getProdutosCarteira(unidade);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ChartDataAndLabels getHistoricoVendasProdutos(String[] codigoProdutos) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < codigoProdutos.length; i++) {
			if (i < (codigoProdutos.length - 1)) 
				sb.append("'" + codigoProdutos[i] + "',");
			else
				sb.append("'" + codigoProdutos[i] + "'");
		}
		String sql = "SELECT EMISSAO, SUM(C6_QTDVEN) AS C6_QTDVEN FROM (\r\n"
				+ "SELECT\r\n"
				+ "B1_LOCPAD, SUBSTRING(C5_EMISSAO,1,6) AS EMISSAO,CASE A1_EST WHEN 'EX' THEN C6_VALOR * " + ConfigClass.taxaDolar + " ELSE C6_VALOR END AS VALOR,C6_BLQ,C6_PRCVEN,C6_QTDENT,C6_QTDVEN\r\n"
				+ "FROM SC6010 WITH(NOLOCK)\r\n"
				+ _joinSC5010
				+ "INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENT\r\n"
				+ "AND C5_LOJACLI = A1_LOJA\r\n"
				+ "AND A1_FILIAL = ''\r\n"
				+ "INNER JOIN SB1010 WITH(NOLOCK) ON C6_PRODUTO = B1_COD AND B1_FILIAL = '' AND SB1010.D_E_L_E_T_ = ''\r\n"
				+ "WHERE "
				+ "YEAR(C5_EMISSAO) >= YEAR(DATEADD(YEAR, -4, GETDATE()))\r\n"
				+ "AND SC6010.D_E_L_E_T_ = ''\r\n"
				+ "AND C6_CLI <> '000422'"
				+ "AND C6_FILIAL = '01'\r\n";
				sql += _C6_TES;
				sql += "AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n"
				+ "AND C5_XREFAT IN ('', '2')\r\n"
				+ "AND C5_LIBEROK = 'S'\r\n"
				+ "AND C6_LOCAL = B1_LOCPAD\r\n";
				sql += "AND C6_PRODUTO IN (" + sb.toString() + ")\r\n"
				+ ") AS RESULTSET\r\n" + "GROUP BY EMISSAO\r\n" + "ORDER BY EMISSAO";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("EMISSAO"));
				cdl.getChartData().add(rs.getBigDecimal("C6_QTDVEN").setScale(0, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getHistoricoVendasProdutos(codigoProdutos);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static DataTable getTabelaPrecos(UNIDADEVENDAS unidadeVendas) {
		String sql = "SELECT\r\n" + 
					"DA1_QTDLOT,DA1_CODPRO,B1_DESC,DA1_PRCVEN,CASE DA1_MOEDA WHEN '1' THEN 'REAL' WHEN '2' THEN 'DOLAR' WHEN '4' THEN 'EURO' END AS DA1_MOEDA\r\n" + 
					"FROM DA1010 WITH(NOLOCK)\r\n" + 
					"INNER JOIN SB1010 WITH(NOLOCK) ON DA1_CODPRO = B1_COD\r\n" + 
					"AND SB1010.D_E_L_E_T_ = ''\r\n" + 
					"AND B1_FILIAL = ''\r\n" + 
					"WHERE DA1_CODTAB = ?\r\n" + 
					"AND DA1010.D_E_L_E_T_ = ''";
		try(PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			if (unidadeVendas.equals(UNIDADEVENDAS.MERCADOINTERNO)) {
				stmt.setString(1, "083");
			} else if (unidadeVendas.equals(UNIDADEVENDAS.EXPORTACAO)) {
				stmt.setString(1, "090");
			} else if (unidadeVendas.equals(UNIDADEVENDAS.EQUIPAMENTOS)) {
				stmt.setString(1, "100");
			}
			
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				linha.add(rs.getString("DA1_CODPRO"));
				linha.add(rs.getString("B1_DESC"));
				linha.add(rs.getBigDecimal("DA1_QTDLOT").setScale(0, RoundingMode.HALF_EVEN));
				linha.add(rs.getBigDecimal("DA1_PRCVEN").setScale(2, RoundingMode.HALF_EVEN));
				linha.add(rs.getString("DA1_MOEDA"));
				data.add(linha);
			}
			return new DataTable().setData(data);
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getTabelaPrecos(unidadeVendas);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static DataTable getComparativo(String tipoPeriodo, Integer anoInicial, Integer anoFinal) {
		String sql = "";
		if (tipoPeriodo.equals("MENSAL")) {
			sql += "SELECT MES, SUM(VALOR) FROM (\r\n";
		}
		if (tipoPeriodo.equals("BIMESTRAL")) {
			sql += "SELECT BIMESTRE, SUM(VALOR) FROM (\r\n";
			sql += 	"	SELECT\r\n" + 
					"		CASE WHEN MONTH(C5_EMISSAO) IN (1, 2) THEN '1 BI'\r\n" + 
					"			WHEN MONTH(C5_EMISSAO) IN (3, 4) THEN '2 BI'\r\n" + 
					"			WHEN MONTH(C5_EMISSAO) IN (5, 6) THEN '3 BI'\r\n" + 
					"			WHEN MONTH(C5_EMISSAO) IN (7, 8) THEN '4 BI'\r\n" + 
					"			WHEN MONTH(C5_EMISSAO) IN (9, 10) THEN '5 BI'\r\n" + 
					"			WHEN MONTH(C5_EMISSAO) IN (11, 12) THEN '6 BI'\r\n" + 
					"		END AS BIMESTRE,\r\n";
		}
		if (tipoPeriodo.equals("TRIMESTRAL")) {
			sql += "SELECT TRIMESTRE, SUM(VALOR) FROM (\r\n";
			sql += 	"	SELECT\r\n" + 
					"		CASE WHEN MONTH(C5_EMISSAO) IN (1, 2, 3) THEN '1 BI'\r\n" + 
					"			WHEN MONTH(C5_EMISSAO) IN (4, 5, 6) THEN '2 BI'\r\n" + 
					"			WHEN MONTH(C5_EMISSAO) IN (7, 8, 9) THEN '3 BI'\r\n" + 
					"			WHEN MONTH(C5_EMISSAO) IN (10, 11, 12) THEN '4 BI'\r\n" + 
					"		END AS TRIMESTRE,\r\n";
		}
		if (tipoPeriodo.equals("SEMESTRAL")) {
			sql += "SELECT SEMESTRE, SUM(VALOR) FROM (\r\n";
			sql += 	"	SELECT\r\n" + 
					"		CASE WHEN MONTH(C5_EMISSAO) IN (1, 2, 3, 4, 5, 6) THEN '1 BI'\r\n" + 
					"			WHEN MONTH(C5_EMISSAO) IN (7, 8, 9, 10, 11, 12) THEN '2 BI'\r\n" + 
					"		END AS SEMESTRE,\r\n";
		}
		sql +=  "		C6_VALOR AS VALOR\r\n" + 
				"	FROM SC5010 WITH(NOLOCK)\r\n" + 
				"	INNER JOIN SC6010 WITH(NOLOCK) ON C5_NUM = C6_NUM\r\n" + 
				"		AND SC6010.D_E_L_E_T_ = ''\r\n" + 
				"		AND C6_BLQ <> 'R'\r\n" + 
				"	WHERE SC5010.D_E_L_E_T_ = ''\r\n" + 
				"	AND YEAR(C5_EMISSAO) = 2018\r\n" + 
				"	AND C5_TIPO = 'N'\r\n" + 
				") AS RESULTSET\r\n" + 
				"GROUP BY BIMESTRE";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					linha.add(rs.getString(i));
				}
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public static DataTable getMapaVendas(UNIDADE unidade) {
		 String sql = "SELECT\r\n" + 
		 		"A1_EST,A1_NOME,A1_COD,\r\n" + 
		 		"SUM(CASE WHEN YEAR = '2016' THEN COMPRAS ELSE 0 END) AS '2016',\r\n" + 
		 		"SUM(CASE WHEN YEAR = '2017' THEN COMPRAS ELSE 0 END) AS '2017',\r\n" + 
		 		"SUM(CASE WHEN YEAR = '2018' THEN COMPRAS ELSE 0 END) AS '2018',\r\n" + 
		 		"SUM(CASE WHEN YEAR = '2019' THEN COMPRAS ELSE 0 END) AS '2019'\r\n" + 
		 		"FROM\r\n" + 
		 		"(\r\n" + 
		 		"   SELECT\r\n" + 
		 		"   YEAR(C5_EMISSAO) AS YEAR,\r\n" + 
		 		"   A1_EST,\r\n" + 
		 		"   A1_COD,\r\n" + 
		 		"   A1_NOME,\r\n" + 
		 		"   SUM(CASE WHEN C6_BLQ <> '' THEN C6_PRCVEN * C6_QTDENT WHEN A1_EST = 'EX' THEN C6_VALOR * " + ConfigClass.taxaDolar + " ELSE C6_VALOR END) AS COMPRAS\r\n" + 
		 		"   FROM SC6010 WITH(NOLOCK)\r\n" + 
		 		"   INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM\r\n" + 
		 		"   AND SC5010.D_E_L_E_T_ = ''\r\n" + 
		 		"   AND C5_TIPO = 'N'\r\n" + 
		 		"   AND C5_FILIAL = '01'\r\n" + 
		 		"   AND C5_CLIENTE <> '000422'\r\n" + 
		 		"   INNER JOIN SA1010 WITH(NOLOCK) ON C5_CLIENTE = A1_COD\r\n" + 
		 		"   AND C5_LOJACLI = A1_LOJA\r\n" + 
		 		"   AND C5_FILIAL = '01'\r\n" + 
		 		"   WHERE SC6010.D_E_L_E_T_ = ''\r\n" + 
		 		"   AND C5_EMISSAO BETWEEN '20160101' AND '20191231'\r\n" + 
		 		"   AND C6_BLQ <> 'R'\r\n" + 
		 		"   AND C6_FILIAL = '01'\r\n" + 
		 		"   AND ((C6_TES LIKE '5%') OR (C6_TES IN ('644', '646', '649', '701', '702', '703')) OR (C6_CF IN ('5116', '6116')))\r\n";
		 		if (unidade.equals(UNIDADE.EXPORTACAO)) {
		 			sql += "   AND A1_EST = 'EX'\r\n";
		 		} else if (unidade.equals(UNIDADE.ORTOPEDIANACIONAL)) {
		 			sql += "AND C6_LOCAL = '01'\r\n";
		 			sql += "   AND A1_EST <> 'EX'\r\n";
		 		} else if (unidade.equals(UNIDADE.ORTOPEDIAEXPORTACAO)) {
		 			sql += "AND C6_LOCAL = '01'\r\n";
		 			sql += "   AND A1_EST = 'EX'\r\n";
		 		} else if (unidade.equals(UNIDADE.EQUIPAMENTOSNACIONAL)) {
		 			sql += "AND C6_LOCAL IN ('11', '12', '14')\r\n";
		 			sql += "   AND A1_EST <> 'EX'\r\n";
		 		} else if (unidade.equals(UNIDADE.EQUIPAMENTOSEXPORTACAO)) {
		 			sql += "AND C6_LOCAL IN ('11', '12', '14')\r\n";
		 			sql += "   AND A1_EST = 'EX'\r\n";		 			
		 		}
		 		sql += "   GROUP BY YEAR(C5_EMISSAO), A1_EST, A1_NOME, A1_COD, C6_BLQ\r\n" + 
		 		")\r\n" + 
		 		"AS SUB\r\n" + 
		 		"GROUP BY A1_EST,A1_NOME,A1_COD\r\n" + 
		 		"ORDER BY 1, 7 DESC";
		 		
		 
		 try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			 ResultSet rs = stmt.executeQuery();
			 DataTable dataTable = new DataTable();
			 Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			 while (rs.next()) {
				 Collection<Object> linha = new LinkedList<Object>();
				 linha.add(rs.getString(1));
				 linha.add(HtmlUtils.createLink("/clientes/" + rs.getString(3) + "/detalhes", rs.getString(2)));
				 linha.add(rs.getBigDecimal(4).setScale(2, RoundingMode.HALF_EVEN));
				 linha.add(rs.getBigDecimal(5).setScale(2, RoundingMode.HALF_EVEN));
				 linha.add(rs.getBigDecimal(6).setScale(2, RoundingMode.HALF_EVEN));
				 linha.add(rs.getBigDecimal(7).setScale(2, RoundingMode.HALF_EVEN));
				 data.add(linha);
			 }
			 dataTable.setData(data);
			 return dataTable;
			} catch (SQLException e) {
				if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
					return getMapaVendas(unidade);
				} else {
					e.printStackTrace();
				}
			}
		return null;
	}

	public static DataTable getCarteiraPcp(String filtro, Iterable<PedidoManual> pedidosManuais) {
		String sql = "SELECT BM_DESC, A1_NOME, C6_XLIBPED, C6_ENTREG, C6_VALOR, C6_ITEM, C6_LOCAL, UNIDADE,A1_MSBLQL,C5_CLASPED,C6_NUM, A1_NREDUZ, C5_EMISSAO, C6_ENTREG, C6_PRODUTO, C6_DESCRI,C6_PRCVEN, C6_QTDVEN, C6_QTDENT, QTD_EMP, (C6_QTDVEN - C6_QTDENT - QTD_EMP) AS SALDO, ((C6_QTDVEN - C6_QTDENT - QTD_EMP) * C6_PRCVEN) AS VALOR_SALDO,\r\n" + 
				"(ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = B1_LOCPAD AND D_E_L_E_T_ = ''), 0) + ISNULL((SELECT SUM(DA_SALDO) FROM SDA010 WITH(NOLOCK) WHERE DA_PRODUTO = B1_COD AND DA_SALDO <> 0 AND D_E_L_E_T_ = '' AND DA_LOCAL = B1_LOCPAD), 0)) AS QTD_ESTOQUE,\r\n" + 
				"ISNULL((SELECT SUM(C2_QUANT - C2_QUJE - C2_PERDA) FROM SC2010 WITH(NOLOCK) WHERE C2_PRODUTO = B1_COD AND C2_DATRF='' AND C2_LOCAL=B1_LOCPAD AND D_E_L_E_T_='' AND C2_STATUS IN ('N', '')), 0) AS QTD_OP\r\n" + 
				"FROM (\r\n" + 
				"	SELECT\r\n" + 
				"		A1_NOME, C6_XLIBPED, C6_VALOR, C6_ITEM, C6_LOCAL, CASE WHEN C6_LOCAL = '01' THEN 'ORTOPEDIA' WHEN C6_LOCAL IN ('11', '12', '14') THEN 'EQUIPAMENTOS' END AS UNIDADE,CASE A1_MSBLQL WHEN '0' THEN 'ATIVO' WHEN '1' THEN 'BLOQUEADO' WHEN '2' THEN 'ATIVO' END AS A1_MSBLQL, CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '4' THEN 'EXPORTACAO' WHEN '6' THEN 'EQUIPAMENTOS' END AS C5_CLASPED,C6_NUM,C5_EMISSAO,C6_ENTREG,C6_PRODUTO,C6_DESCRI,C6_QTDVEN,C6_QTDENT,\r\n" + 
				"		(SELECT ISNULL(SUM(C9_QTDLIB), 0) FROM SC9010 WITH(NOLOCK) WHERE C9_PEDIDO = C5_NUM AND C9_ITEM = C6_ITEM AND C9_PRODUTO = C6_PRODUTO AND SC9010.D_E_L_E_T_ = '' AND C9_NFISCAL = '' AND C9_LOTECTL <> '') AS QTD_EMP,\r\n" + 
				"		CASE A1_EST WHEN 'EXT' THEN C6_PRCVEN * 3.6 ELSE C6_PRCVEN END AS C6_PRCVEN,A1_NREDUZ\r\n" + 
				"	FROM SC6010 WITH(NOLOCK)\r\n" + 
				"	INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM\r\n" + 
				"		AND C5_FILIAL = '01'\r\n" + 
				"		AND SC5010.D_E_L_E_T_ = ''\r\n" + 
				"		AND C5_TIPO = 'N'\r\n" + 
				"		AND C5_XREFAT IN ('', '2')\r\n" + 
				"	INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENT\r\n" + 
				"		AND A1_LOJA = C5_LOJACLI\r\n" + 
				"		AND A1_FILIAL = ''\r\n" + 
				"	INNER JOIN SB1010 WITH(NOLOCK) ON C6_PRODUTO = B1_COD\r\n" + 
				"		AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE\r\n" + 
				"	(C6_QTDVEN > C6_QTDENT\r\n" + 
				"	AND C6_BLQ <> 'R'\r\n" + 
				"	AND SC6010.D_E_L_E_T_ = ''\r\n" + 
				"	AND C5_LIBEROK = 'S'\r\n" + 
				"	AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" + 
				"	AND C6_LOCAL = '12')\r\n";
				for (PedidoManual pedidoManual : pedidosManuais) {
					sql += "OR\r\n" + 
					"	(C6_NUM = '" + pedidoManual.getNumero() + "'\r\n" + 
					"	AND C6_ITEM = '" + pedidoManual.getItem() + "'\r\n" + 
					"	AND SC6010.D_E_L_E_T_ = '')\r\n";
				}
				sql += ") AS RESULTSET\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON C6_PRODUTO = B1_COD\r\n";
				if (filtro.equals("PAS")) {
					sql += "AND B1_GRUPO IN ('0036', '0043', '0045', '0044', '0046', '0049', '0050', '0041', '0053')\r\n";
				}
				sql += "INNER JOIN SBM010 WITH(NOLOCK) ON BM_FILIAL <> 'ZZ' AND BM_GRUPO = B1_GRUPO AND SBM010.D_E_L_E_T_ = ''\r\n";
				sql += "AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"ORDER BY C5_EMISSAO";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				linha.add(HtmlUtils.createLink("/carteira-pcp/" + rs.getString("C6_NUM") + "/" + rs.getString("C6_ITEM") + "/acompanhamento", rs.getString("C6_NUM")));
				linha.add(rs.getString("C6_ITEM"));
				linha.add(rs.getString("A1_NOME"));
				linha.add(rs.getString("C6_PRODUTO"));
				linha.add(rs.getString("C6_DESCRI"));
				linha.add(rs.getString("BM_DESC"));
				linha.add("");//PFI
				linha.add("");//PROCESSO
				linha.add("");//OBS
				linha.add("");//MEMO
				linha.add(rs.getString("C6_QTDVEN"));
				linha.add(rs.getString("C6_QTDENT"));
				linha.add(rs.getString("C6_XLIBPED"));
				linha.add(rs.getString("C6_ENTREG"));
				linha.add("");
				for (int i = 0; i < 16; i++) {
					linha.add("");
				}
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static ItemPedidoVenda getItemPedido(String numeroPedido, String numItemPedido) {
		String sql = "SELECT C6_XDESCRI, C6_BLQ, C6_XLIBPED, C6_ITEM, A1_NOME, C5_CLASPED,C6_NUM, A1_NREDUZ, C5_EMISSAO, C6_ENTREG, C6_PRODUTO, C6_DESCRI,C6_PRCVEN, C6_QTDVEN, C6_QTDENT, QTD_EMP, (C6_QTDVEN - C6_QTDENT - QTD_EMP) AS SALDO, ((C6_QTDVEN - C6_QTDENT - QTD_EMP) * C6_PRCVEN) AS VALOR_SALDO,\r\n" + 
				"(ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = B1_LOCPAD AND D_E_L_E_T_ = ''), 0) + ISNULL((SELECT SUM(DA_SALDO) FROM SDA010 WITH(NOLOCK) WHERE DA_PRODUTO = B1_COD AND DA_SALDO <> 0 AND D_E_L_E_T_ = '' AND DA_LOCAL = B1_LOCPAD), 0)) AS QTD_ESTOQUE,\r\n" + 
				"ISNULL((SELECT SUM(C2_QUANT - C2_QUJE - C2_PERDA) FROM SC2010 WITH(NOLOCK) WHERE C2_PRODUTO = B1_COD AND C2_DATRF='' AND C2_LOCAL=B1_LOCPAD AND D_E_L_E_T_='' AND C2_STATUS IN ('N', '')), 0) AS QTD_OP\r\n" + 
				"FROM (\r\n" + 
				"SELECT\r\n" + 
				"C6_XDESCRI, C6_BLQ, C6_XLIBPED, C6_ITEM, A1_NOME, CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '4' THEN 'EXPORTACAO' WHEN '6' THEN 'EQUIPAMENTOS' END AS C5_CLASPED,C6_NUM,C5_EMISSAO,C6_ENTREG,C6_PRODUTO,C6_DESCRI,C6_QTDVEN,C6_QTDENT,\r\n" + 
				"(SELECT ISNULL(SUM(C9_QTDLIB), 0) FROM SC9010 WITH(NOLOCK) WHERE C9_PEDIDO = C5_NUM AND C9_ITEM = C6_ITEM AND C9_PRODUTO = C6_PRODUTO AND SC9010.D_E_L_E_T_ = '' AND C9_NFISCAL = '' AND C9_LOTECTL <> '') AS QTD_EMP,\r\n" + 
				"CASE A1_EST WHEN 'EXT' THEN C6_PRCVEN * 3.6 ELSE C6_PRCVEN END AS C6_PRCVEN,A1_NREDUZ\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010.replace("AND C5_CLIENTE <> '000422'\r\n", "") + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENT\r\n" + 
				"AND A1_LOJA = C5_LOJACLI\r\n" + 
				"AND A1_FILIAL = ''\r\n" + 
				"WHERE\r\n" + 
				"SC6010.D_E_L_E_T_ = ''\r\n" + 
				"AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" +
				"AND C6_NUM = '" + numeroPedido + "'\r\n" +
				"AND C6_ITEM = '" + numItemPedido + "'\r\n" +
				") AS RESULTSET\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON C6_PRODUTO = B1_COD\r\n" + 
				"AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"ORDER BY C5_EMISSAO";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			ItemPedidoVenda itemPedido = new ItemPedidoVenda();
			if (rs.next()) {
				itemPedido.setCliente(rs.getString("A1_NOME"));
				itemPedido.setItem(rs.getString("C6_ITEM"));
				itemPedido.setPedido(new PedidoVenda().setNumero(rs.getString("C6_NUM")).setDataEmissao(rs.getString("C5_EMISSAO")).setDataLiberacao(rs.getString("C6_XLIBPED")).setDataEntrega(rs.getString("C6_ENTREG")));
				itemPedido.setPrcVen(rs.getBigDecimal("C6_PRCVEN").setScale(2, RoundingMode.HALF_EVEN));
				itemPedido.setProduto(new Produto().setCodigo(rs.getString("C6_PRODUTO")).setDescricao(rs.getString("C6_DESCRI")));
				itemPedido.setQtdEmp(rs.getBigDecimal("QTD_EMP").setScale(2, RoundingMode.HALF_EVEN));
				itemPedido.setQtdEnt(rs.getBigDecimal("C6_QTDENT").setScale(2, RoundingMode.HALF_EVEN));
				itemPedido.setQtdVen(rs.getBigDecimal("C6_QTDVEN").setScale(2, RoundingMode.HALF_EVEN));
				itemPedido.setXDesc(rs.getString("C6_XDESCRI"));
				itemPedido.setBlq(rs.getString("C6_BLQ"));
			}
			return itemPedido;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static ChartDataAndLabels getDashBoardMes(int mes, int ano) {
		String sql = "SELECT EMISSAO, SUM(CASE C6_BLQ WHEN 'R' THEN VALOR/C6_QTDVEN*C6_QTDENT ELSE VALOR END) AS VALOR FROM (\r\n" + 
				"SELECT\r\n" + 
				"SUBSTRING(C5_EMISSAO,1,6) AS EMISSAO,CASE A1_EST WHEN 'EX' THEN C6_VALOR * 3.7 ELSE C6_VALOR END AS VALOR,C6_BLQ,C6_PRCVEN,C6_QTDENT,C6_QTDVEN\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM\r\n" + 
				"AND C5_FILIAL = '01'\r\n" + 
				"AND SC5010.D_E_L_E_T_ = ''\r\n" + 
				"AND C5_TIPO = 'N'\r\n" + 
				"AND C5_XREFAT IN ('', '2')\r\n" + 
				"AND C5_CLIENTE <> '000422'\r\n" + 
				"AND C5_CLASPED IN ('1', '2')\r\n" + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENT\r\n" + 
				"AND C5_LOJACLI = A1_LOJA\r\n" + 
				"AND A1_FILIAL = ''\r\n" + 
				"WHERE (YEAR(C5_EMISSAO) = '" + ano + "' OR YEAR(C5_EMISSAO) = '" + (ano - 1) + "')\r\n" + 
				"AND MONTH(C5_EMISSAO) = '" + mes + "'\r\n" + 
				"AND DAY(C5_EMISSAO) <= DAY(GETDATE())\r\n" +
				"AND SC6010.D_E_L_E_T_ = ''\r\n" + 
				"AND C6_FILIAL = '01'\r\n" + 
				"AND ((C6_TES LIKE '5%') OR (C6_TES IN ('644', '649', '701', '702', '703')))\r\n" + 
				"AND C5_XREFAT IN ('', '2')\r\n" + 
				"AND C6_LOCAL IN ('01', '11', '12', '14')) AS RESULTSET\r\n" + 
				"GROUP BY EMISSAO\r\n" + 
				"ORDER BY EMISSAO";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while(rs.next()) {
				cdl.getChartLabels().add(rs.getString("EMISSAO"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR"));
			}
			return cdl;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String atualizaUltimoPedido() {
		String sql = "SELECT TOP 1\r\n" + 
				"	C6_NUM\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM\r\n" + 
				"AND C5_FILIAL = '01'\r\n" + 
				"AND C5_TIPO = 'N'\r\n" + 
				"AND C5_XREFAT IN ('', '2')\r\n" + 
				"AND C5_CLIENTE <> '000422'\r\n" + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENT\r\n" + 
				"	AND C5_LOJACLI = A1_LOJA\r\n" + 
				"	AND A1_FILIAL = ''\r\n" + 
				"WHERE MONTH(C5_EMISSAO) = MONTH(GETDATE())\r\n" + 
				"	AND YEAR(C5_EMISSAO) = YEAR(GETDATE())\r\n" + 
				"	AND DAY(C5_EMISSAO) = DAY(GETDATE())\r\n" + 
				"	AND C6_FILIAL = '01'\r\n" + 
				"	AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" + 
				"	AND C6_CLI <> '000422'\r\n" + 
				"AND ((C6_TES LIKE '5%') OR (C6_TES IN ('644', '649', '701', '702', '703')))\r\n" + 
				"AND C5_XREFAT IN ('', '2')\r\n" + 
				"ORDER BY C5_EMISSAO DESC";
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				ultimoPedido = rs.getString(1);
				return ultimoPedido;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
		
	}

	public static boolean temNovoPedido() {
		if (ultimoPedido == null) {
			return false;
		}
		String tempPedido = ultimoPedido;
		if (!tempPedido.equals(atualizaUltimoPedido())) {
			return true;
		}
		return false;
	}

	public static PedidoVenda getUltimoPedido() {
		return findOne(ultimoPedido);
	}

	public static DataTable getVendasSemCusto() {
		String sql = "SELECT\r\n" + 
				"	C6_PRODUTO, B1_DESC, B1_LOCPAD, SUM(C6_QTDVEN) AS QTDVEN, SUM(C6_VALOR) AS VALOR\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON B1_FILIAL <> 'ZZ' AND B1_COD = C6_PRODUTO AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"INNER JOIN SC5010 WITH(NOLOCK) ON C5_FILIAL <> 'ZZ' AND C6_NUM = C5_NUM AND SC5010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE C6_FILIAL <> 'ZZ'\r\n" + 
				"	AND SC6010.D_E_L_E_T_ = ''\r\n" + 
				"	AND SB1010.B1_X_CUST = ''\r\n" + 
				"	AND C5_TIPO = 'N'\r\n" + 
				"	AND C6_BLQ <> 'R'\r\n" + 
				"	AND C5_EMISSAO > '20190101'\r\n" + 
				"	AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" + 
				"GROUP BY C6_PRODUTO, B1_DESC, B1_LOCPAD\r\n" + 
				"ORDER BY AVG(C6_VALOR) DESC";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					linha.add(rs.getString(i));
				}
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static DataTable getRelacaoVendas(
			String pedidoDe,
			String pedidoAte,
			String produtoDe,
			String produtoAte,
			String clienteDe,
			String clienteAte,
			String dataDe,
			String dataAte,
			String localDe,
			String localAte, 
			String linhaProdutos) {
		String sql = "SELECT\r\n" + 
				"	C5_NUM, C5_EMISSAO, C6_PRODUTO, B1_DESC, C6_QTDVEN, C6_QTDENT, CASE WHEN C6_BLQ = 'R' THEN 0 ELSE C6_QTDVEN - C6_QTDENT END AS SALDO, C6_VALOR, BM_DESC, C6_BLQ, A1_EST, A1_NOME, A1_COD\r\n" + 
				"	FROM SC6010 WITH(NOLOCK)\r\n" + 
				"	INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM\r\n" + 
				"	AND C5_FILIAL = '01'\r\n" + 
				"	AND SC5010.D_E_L_E_T_ = ''\r\n" + 
				"	AND C5_TIPO = 'N'\r\n" + 
				"	AND C5_XREFAT IN ('', '2')\r\n" + 
				"	AND C5_CLIENTE <> '000422'\r\n" + 
				"	AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" + 
				"	INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENT\r\n" + 
				"	AND C5_LOJACLI = A1_LOJA\r\n" + 
				"	AND A1_FILIAL = ''\r\n" + 
				"	INNER JOIN SB1010 WITH(NOLOCK) ON B1_FILIAL <> 'ZZ' AND B1_COD = C6_PRODUTO AND SB1010.R_E_C_D_E_L_ = ''\r\n" +
				"	LEFT JOIN SBM010 WITH(NOLOCK) ON BM_FILIAL <> 'ZZ' AND BM_GRUPO = B1_GRUPO AND SBM010.R_E_C_D_E_L_ = ''\r\n" +
				"	WHERE\r\n" + 
				"	SC6010.D_E_L_E_T_ = ''\r\n" + 
				"	AND C6_FILIAL <> 'ZZ'\r\n" + 
				"	AND ((C6_TES LIKE '5%') OR (C6_TES IN ('644', '649', '701', '702', '703')) OR (C6_CF IN ('5116', '6116')))\r\n";
		
		if (pedidoDe != null && !pedidoDe.equals("")) {
			sql += "AND C6_NUM >= '" + pedidoDe + "'\r\n";
		}
		if (pedidoAte != null && !pedidoAte.equals("")) {
			sql += "AND C6_NUM <= '" + pedidoAte + "'\r\n";
		}
		if (produtoDe != null && !produtoDe.equals("")) {
			sql += "AND C6_PRODUTO >= '" + produtoDe + "'\r\n";
		}
		if (produtoAte != null && !produtoAte.equals("")) {
			sql += "AND C6_PRODUTO <= '" + produtoAte + "'\r\n";
		}
		if (clienteDe != null && !clienteDe.equals("")) {
			sql += "AND C5_CLIENTE >= '" + clienteDe + "'\r\n";
		}
		if (clienteAte != null && !clienteAte.equals("")) {
			sql += "AND C5_CLIENTE <= '" + clienteAte + "'\r\n";
		}
		if (dataDe != null && !dataDe.equals("")) {
			sql += "AND C5_EMISSAO >= '" + dataDe + "'\r\n";
		}
		if (dataAte != null && !dataAte.equals("")) {
			sql += "AND C5_EMISSAO <= '" + dataAte + "'\r\n";
		}
		if (localDe != null && !localDe.equals("")) {
			sql += "AND C6_LOCAL >= '" + localDe + "'\r\n";
		}
		if (localAte != null && !localAte.equals("")) {
			sql += "AND C6_LOCAL <= '" + localAte + "'\r\n";
		}
		if (linhaProdutos != null && !linhaProdutos.equals("")) {
			sql += "AND BM_DESC = '" + linhaProdutos + "'\r\n";
		}
		
		Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					String columnName = rs.getMetaData().getColumnName(i);
					if (columnName.equals("C5_NUM")) {
						linha.add(HtmlUtils.createLink("/pedidos-venda/" + rs.getString(i) + "/detalhes", rs.getString(i)));
					} else if (columnName.equals("A1_COD")) {
						linha.add(HtmlUtils.createLink("/clientes/" + rs.getString(i) + "/detalhes/", rs.getString(i)));
					} else if (columnName.equals("C6_PRODUTO")) {
						linha.add(HtmlUtils.createLink("/produtos/detalhes?codigoProduto=" + rs.getString(i), rs.getString(i)));
					} else {
						linha.add(rs.getString(i));
					}
				}
				data.add(linha);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new DataTable(data);
	}

	public static DataTable getVendasPorLinha(UNIDADE unidade) {
		String sql = "SELECT \r\n" + 
				"	BM_DESC, \r\n" + 
				"	SUM(CASE WHEN MES = '1' THEN C6_VALOR ELSE 0 END) AS JANEIRO,\r\n" + 
				"	SUM(CASE WHEN MES = '2' THEN C6_VALOR ELSE 0 END) AS FEVEREIRO,\r\n" + 
				"	SUM(CASE WHEN MES = '3' THEN C6_VALOR ELSE 0 END) AS MARCO,\r\n" + 
				"	SUM(CASE WHEN MES = '4' THEN C6_VALOR ELSE 0 END) AS ABRIL,\r\n" + 
				"	SUM(CASE WHEN MES = '5' THEN C6_VALOR ELSE 0 END) AS MAIO,\r\n" + 
				"	SUM(CASE WHEN MES = '6' THEN C6_VALOR ELSE 0 END) AS JUNHO,\r\n" + 
				"	SUM(CASE WHEN MES = '7' THEN C6_VALOR ELSE 0 END) AS JULHO,\r\n" + 
				"	SUM(CASE WHEN MES = '8' THEN C6_VALOR ELSE 0 END) AS AGOSTO,\r\n" + 
				"	SUM(CASE WHEN MES = '9' THEN C6_VALOR ELSE 0 END) AS SETEMBRO,\r\n" + 
				"	SUM(CASE WHEN MES = '10' THEN C6_VALOR ELSE 0 END) AS OUTUBRO,\r\n" + 
				"	SUM(CASE WHEN MES = '11' THEN C6_VALOR ELSE 0 END) AS NOVEMBRO,\r\n" + 
				"	SUM(CASE WHEN MES = '12' THEN C6_VALOR ELSE 0 END) AS DEZEMBRO	\r\n" + 
				"FROM (\r\n" + 
				"	SELECT\r\n" + 
				"		C5_NUM, C5_EMISSAO, C6_PRODUTO, B1_DESC, C6_QTDVEN, C6_QTDENT, CASE WHEN C6_BLQ = 'R' THEN 0 ELSE C6_QTDVEN - C6_QTDENT END AS SALDO, C6_VALOR, BM_DESC, C6_BLQ, A1_EST, A1_NOME, A1_COD, YEAR(C5_EMISSAO) AS ANO, MONTH(C5_EMISSAO) AS MES\r\n" + 
				"		FROM SC6010 WITH(NOLOCK)\r\n" + 
				"		INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM\r\n" + 
				"		AND C5_FILIAL = '01'\r\n" + 
				"		AND SC5010.D_E_L_E_T_ = ''\r\n" + 
				"		AND C5_TIPO = 'N'\r\n" + 
				"		AND C5_XREFAT IN ('', '2')\r\n" + 
				"		AND C5_CLIENTE <> '000422'\r\n" + 
				"		AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" + 
				"		INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENT\r\n" + 
				"		AND C5_LOJACLI = A1_LOJA\r\n" + 
				"		AND A1_FILIAL = ''\r\n" + 
				"		INNER JOIN SB1010 WITH(NOLOCK) ON B1_FILIAL <> 'ZZ' AND B1_COD = C6_PRODUTO AND SB1010.R_E_C_D_E_L_ = ''\r\n" + 
				"		LEFT JOIN SBM010 WITH(NOLOCK) ON BM_FILIAL <> 'ZZ' AND BM_GRUPO = B1_GRUPO AND SBM010.R_E_C_D_E_L_ = ''\r\n" + 
				"		WHERE\r\n" + 
				"		SC6010.D_E_L_E_T_ = ''\r\n" + 
				"		AND C6_FILIAL <> 'ZZ'\r\n" + 
				"		AND ((C6_TES LIKE '5%') OR (C6_TES IN ('644', '649', '701', '702', '703')) OR (C6_CF IN ('5116', '6116')))\r\n" + 
				"		AND C5_EMISSAO >= '20190101'\r\n";
				if (unidade.equals(UNIDADE.ORTOPEDIA)) {
					sql += "AND B1_LOCPAD IN ('01', '10')\r\n";
					sql += "AND C5_CLASPED IN ('1', '2', '4')\r\n";
				} else {
					sql += "AND B1_LOCPAD IN ('11', '12', '14')\r\n";
					sql += "AND C5_CLASPED IN ('3', '4', '6')\r\n";
				}
				sql += ") AS RESULTSET\r\n" + 
				"GROUP BY BM_DESC";
		
		Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
					String bmDesc = rs.getString("BM_DESC");
					linha.add(bmDesc);
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20190101&dataAte=20190131&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("JANEIRO").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20190201&dataAte=20190231&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("FEVEREIRO").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20190301&dataAte=20190331&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("MARCO").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20190401&dataAte=20190431&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("ABRIL").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20190501&dataAte=20190531&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("MAIO").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20190601&dataAte=20190631&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("JUNHO").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20190701&dataAte=20190731&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("JULHO").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20190801&dataAte=20190831&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("AGOSTO").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20190901&dataAte=20190931&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("SETEMBRO").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20191001&dataAte=20191031&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("OUTUBRO").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20191101&dataAte=20191131&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("NOVEMBRO").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20191201&dataAte=20191231&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("DEZEMBRO").setScale(2, RoundingMode.HALF_EVEN))));
				data.add(linha);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new DataTable(data);
	}

	public static ChartDataAndLabels getQtdOrcamentos(UNIDADE unidade) {
		String sql = "SELECT\r\n" + 
				"DAY(CJ_EMISSAO), COUNT(CJ_NUM)\r\n" + 
				"FROM SCJ010\r\n" +
				"WHERE\r\n" + 
				"CJ_FILIAL <> 'ZZ' AND CJ_CLIENTE <> '000422' AND CJ_LOJA <> 'ZZ' AND YEAR(CJ_EMISSAO) = YEAR(GETDATE()) AND MONTH(CJ_EMISSAO) = MONTH(GETDATE()) AND SCJ010.R_E_C_N_O_ <> 0 AND SCJ010.D_E_L_E_T_ = ''\r\n";
		if (unidade.equals(UNIDADE.ORTOPEDIA)) {
			sql += "AND CJ_XCLASPD IN ('1', '2', '4')\r\n";
		} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
			sql += "AND CJ_XCLASPD IN ('3', '6')\r\n";
		}
				sql += "GROUP BY DAY(CJ_EMISSAO)";
		ChartDataAndLabels cdl = new ChartDataAndLabels();
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString(1));
				cdl.getChartData().add(rs.getBigDecimal(2).setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static ChartDataAndLabels getValorOrcamentos(UNIDADE unidade) {
		String sql = "SELECT\r\n" + 
				"DAY(CJ_EMISSAO) AS DIA, SUM(CK_VALOR) AS VALOR\r\n" + 
				"FROM SCK010\r\n" + 
				"INNER JOIN SCJ010 ON CJ_FILIAL <> 'ZZ' AND CJ_CLIENTE <> '000422' AND CJ_LOJA <> 'ZZ' AND YEAR(CJ_EMISSAO) = YEAR(GETDATE()) AND MONTH(CJ_EMISSAO) = MONTH(GETDATE()) AND SCJ010.R_E_C_N_O_ <> 0 AND SCJ010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE\r\n" + 
				"CK_FILIAL <> 'ZZ' AND CK_NUM = CJ_NUM AND CK_ITEM <> 'ZZ' AND CK_PRODUTO <> 'ZZ' AND SCK010.R_E_C_D_E_L_ = ''\r\n";
		if (unidade.equals(UNIDADE.ORTOPEDIA)) {
			sql += "AND CK_LOCAL IN ('01', '10')\r\n";
		} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
			sql += "AND CK_LOCAL IN ('11', '12')\r\n";
		}
				sql += "GROUP BY DAY(CJ_EMISSAO)";
		ChartDataAndLabels cdl = new ChartDataAndLabels();
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString(1));
				cdl.getChartData().add(rs.getBigDecimal(2).setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
