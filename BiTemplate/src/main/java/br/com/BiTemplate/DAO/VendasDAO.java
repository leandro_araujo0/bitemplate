package br.com.BiTemplate.DAO;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import be.ceau.chart.color.Color;
import be.ceau.chart.dataset.BarDataset;
import br.com.BiTemplate.configuration.ConfigClass;
import br.com.BiTemplate.enums.CLASPED;
import br.com.BiTemplate.enums.StatusOrcamento;
import br.com.BiTemplate.enums.TipoOrcamento;
import br.com.BiTemplate.enums.TipoVendasPorLinha;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.enums.UNIDADEVENDAS;
import br.com.BiTemplate.model.CardVenda;
import br.com.BiTemplate.model.ChartDataAndLabels;
import br.com.BiTemplate.model.Cliente;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.model.ItemPedidoVenda;
import br.com.BiTemplate.model.ParametroVenda;
import br.com.BiTemplate.model.PedidoFaturamento;
import br.com.BiTemplate.model.PedidoManual;
import br.com.BiTemplate.model.PedidoVenda;
import br.com.BiTemplate.model.Produto;
import br.com.BiTemplate.model.ProgramaVendas;
import br.com.BiTemplate.utils.CurrencyUtils;
import br.com.BiTemplate.utils.DataUtils;
import br.com.BiTemplate.utils.HtmlUtils;

public class VendasDAO {
	
	private static String ultimoPedido;
	
	private static final String _C6_TES = "AND ((C6_TES LIKE '5%') OR (C6_TES IN ('644', '649', '701', '702', '703')) OR (C6_CF IN ('5116', '6116', '6101', '5101', '6107', '5107')))\r\n";
	private static final String _joinSC5010 = "INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM\r\n"
			+ "AND C5_FILIAL <> 'ZZ'\r\n"
			+ "AND SC5010.D_E_L_E_T_ = ''\r\n"
			+ "AND C5_TIPO = 'N'\r\n"
			+ "AND C5_XREFAT IN ('', '2')\r\n"
			+ "AND C5_CLIENTE <> '000422'\r\n";
	
	
	public static DataTable getVendas(ParametroVenda parametro, CLASPED clasped, String dia, String mes, String ano, String codigoCliente, UNIDADE unidade, String dataDe, String dataAte, String cliente, String produtos, String codigoGrupoProduto, String linhaProdutos, String estado) {
		String sql = "";
		
		if (parametro != null && parametro.equals(ParametroVenda.POREMISSAO)) {
			sql += "SELECT C5_EMISSAO, SUM(C6_QTDVEN) AS NUM_QTDVEN, SUM(C6_VALOR) AS NUM_VALOR FROM (\r\n";
		} else if (parametro != null && parametro.equals(ParametroVenda.MENSALPORCLASPED)) { 
			sql += "SELECT C5_CLASPED, SUM(C6_VALOR) AS NUM_VALOR FROM (\r\n";
		} else if (parametro != null && parametro.equals(ParametroVenda.PORCLASPEDPORDIA)) {
			sql += "SELECT DAY(C5_EMISSAO), SUM(C6_VALOR) AS NUM_VALOR FROM (\r\n";
		} else if (parametro != null && parametro.equals(ParametroVenda.TOPCLIENTES)) {
			sql += "SELECT TOP 15 RTRIM(A1_COD) + ';' + RTRIM(A1_NOME) AS NOME, SUM(C6_VALOR) AS NUM_VALOR FROM (\r\n";
		} else if (parametro != null && parametro.equals(ParametroVenda.TOPPRODUTOS)) {
			if (unidade.equals(UNIDADE.ORTOPEDIA)) {
				sql += "SELECT TOP 15 LEFT(C6_PRODUTO, 4) AS FAMILIA, SUM(C6_VALOR) AS NUM_VALOR FROM (\r\n";
			} else {
				sql += "SELECT TOP 15 RTRIM(C6_PRODUTO) AS PRODUTO, SUM(C6_VALOR) AS NUM_VALOR FROM (\r\n";
			}
		} else if (parametro != null && parametro.equals(ParametroVenda.ANUALGERAL)) {
			sql += "SELECT CASE C5_CLASPED WHEN '1' THEN '4 - REPOSICAO' WHEN '2' THEN '3 - CAIXAS' WHEN '3' THEN '2 - EQUIPAMENTOS' WHEN '4' THEN '1 - EXPORTACAO' WHEN '6' THEN '2 - EQUIPAMENTOS' END AS C5_CLASPED, SUM(C6_VALOR) AS NUM_VALOR FROM (\r\n";
		} else if (parametro != null && parametro.equals(ParametroVenda.VENDASCLIENTEXTIPO)) {
			sql += "SELECT GRUPO, LEFT(C5_EMISSAO, 6) AS DATA, SUM(C6_VALOR) AS NUM_VALOR FROM (\r\n";
		} else if (parametro != null && parametro.equals(ParametroVenda.PROGRAMAVENDAS)) {
			sql += "SELECT ISNULL(SUM(C6_QTDVEN), 0) AS NUM_QTD_VEN FROM (\r\n";
		} else if (parametro != null && parametro.equals(ParametroVenda.MAPAVENDAS)) {
			sql +=    "SELECT A1_COD, A1_EST, A1_NREDUZ,\r\n"
					+ "SUM(CASE WHEN LEFT(C5_EMISSAO, 4) = '2017' THEN C6_VALOR ELSE 0 END) AS NUM_VENDAS_2017,\r\n"
					+ "SUM(CASE WHEN LEFT(C5_EMISSAO, 4) = '2018' THEN C6_VALOR ELSE 0 END) AS NUM_VENDAS_2018,\r\n"
					+ "SUM(CASE WHEN LEFT(C5_EMISSAO, 4) = '2019' THEN C6_VALOR ELSE 0 END) AS NUM_VENDAS_2019,\r\n"
					+ "SUM(CASE WHEN LEFT(C5_EMISSAO, 4) = '2020' THEN C6_VALOR ELSE 0 END) AS NUM_VENDAS_2020,\r\n"
					+ "SUM(CASE WHEN LEFT(C5_EMISSAO, 4) = '2021' THEN C6_VALOR ELSE 0 END) AS NUM_VENDAS_2021\r\n"
					+ " FROM (\r\n";
		} else {
			sql += "SELECT TIPO, C6_NUM, C6_ITEM, A1_NREDUZ, A1_EST, C5_EMISSAO, C6_ENTREG, C6_PRODUTO, C6_DESCRI, GRUPO, C6_QTDVEN, C6_QTDENT, C6_QTDVEN - C6_QTDENT AS SALDO, C6_PRCVEN AS NUM_C6_PRCVEN, C6_VALOR AS NUM_C6_VALOR, C6_PRCVEN * (C6_QTDVEN - C6_QTDENT) AS NUM_VALOR_SALDO FROM (\r\n";
		}
		sql += "SELECT\r\n" + 
				"'VENDAS' AS TIPO, BM_DESC AS GRUPO, C6_NUM, C6_ITEM, A1_NREDUZ, C5_EMISSAO, C6_ENTREG, C6_PRODUTO, C6_DESCRI, C6_QTDVEN, C6_QTDENT, CASE WHEN C5_MOEDA = '4' THEN  C6_PRCVEN * " + ConfigClass.taxaEuro + " WHEN A1_EST = 'EX' THEN C6_PRCVEN * " + ConfigClass.taxaDolar + " ELSE C6_PRCVEN END AS C6_PRCVEN, CASE WHEN C5_MOEDA = '4' THEN C6_VALOR * " + ConfigClass.taxaEuro + " WHEN A1_EST = 'EX' THEN C6_VALOR * " + ConfigClass.taxaDolar + " ELSE C6_VALOR END AS C6_VALOR, C5_CLASPED, A1_NOME, A1_COD, A1_EST\r\n" +  
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM AND C5_FILIAL <> 'ZZ' AND SC5010.D_E_L_E_T_ = '' AND C5_TIPO = 'N' AND C5_CLIENTE <> '000422'\r\n" + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENTE AND A1_LOJA = C5_LOJACLI AND A1_FILIAL <> 'ZZ'\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON SB1010.D_E_L_E_T_ = '' AND B1_FILIAL <> 'ZZ' AND B1_COD = C6_PRODUTO\r\n" + 
				"INNER JOIN SBM010 WITH(NOLOCK) ON SBM010.D_E_L_E_T_ = '' AND BM_FILIAL <> 'ZZ' AND B1_GRUPO = BM_GRUPO\r\n" +
				"WHERE\r\n" + 
				"C6_BLQ <> 'R'\r\n" + 
				"AND C5_LIBEROK = 'S'\r\n" +
				"AND ((C6_TES LIKE '5%') OR (C6_TES IN ('644', '649', '701', '702', '703')) OR (C6_CF IN ('5116', '6116', '6101', '5101', '6107', '5107')))\r\n"; 
		if (clasped == null) { 
			sql += "AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n";
		} else if (clasped.equals(CLASPED.EQUIPAMENTOS)) {
			sql += "AND C5_CLASPED IN ('3', '6')\r\n";
		} else if (clasped.equals(CLASPED.MERCADOINTERNO)) {
			sql += "AND C5_CLASPED IN ('1', '2')\r\n";
		} else if (clasped.equals(CLASPED.EXPORTACAO)) {
			sql += "AND C5_CLASPED IN ('4')\r\n";
		} else if (clasped.equals(CLASPED.CAIXAS)) {
			sql += "AND C5_CLASPED IN ('2')\r\n";
		} else if (clasped.equals(CLASPED.REPOSICAO)) {
			sql += "AND C5_CLASPED IN ('1')\r\n";
		}
		if (unidade != null && unidade.equals(UNIDADE.ORTOPEDIA)) {
			sql += "AND C6_LOCAL IN ('01', '10')\r\n";
		} else if (unidade != null && unidade.equals(UNIDADE.EQUIPAMENTOS)) {
			sql += "AND C6_LOCAL IN ('11', '12', '14')\r\n";
		} else if (unidade != null && unidade.equals(UNIDADE.ORTOPEDIANACIONAL)) {
			sql += "AND C6_LOCAL IN ('01', '10')\r\n";
			sql += "AND A1_EST <> ('EX')\r\n";
		} else if (unidade != null && unidade.equals(UNIDADE.ORTOPEDIAEXPORTACAO)) {
			sql += "AND C6_LOCAL IN ('01', '10')\r\n";
			sql += "AND A1_EST = ('EX')\r\n";
		} else if (unidade != null && unidade.equals(UNIDADE.EQUIPAMENTOSNACIONAL)) {
			sql += "AND C6_LOCAL IN ('11', '12', '14')\r\n";
			sql += "AND A1_EST <> ('EX')\r\n";
		} else if (unidade != null && unidade.equals(UNIDADE.EQUIPAMENTOSEXPORTACAO)) {
			sql += "AND C6_LOCAL IN ('11', '12', '14')\r\n";
			sql += "AND A1_EST = ('EX')\r\n";
		}
	
		if (parametro != null && parametro.equals(ParametroVenda.VENDASCLIENTEXTIPO)) {
			sql += "AND YEAR(C5_EMISSAO) > YEAR(DATEADD(YEAR, -2, GETDATE()))\r\n";
		}
		sql += (cliente != null && !cliente.equals("")) ? "AND A1_NOME LIKE '%" + cliente + "%'\r\n" : "";
		sql += (codigoCliente != null && !codigoCliente.equals("")) ? "AND A1_COD = '" + codigoCliente + "'\r\n" : "";
		sql += (dataDe != null && !dataDe.equals("")) ? "AND C5_EMISSAO >= " + dataDe + "\r\n" : "";
		sql += (dataAte != null && !dataAte.equals("")) ? "AND C5_EMISSAO <= " + dataAte + "\r\n" : "";
		sql += (ano != null && !ano.equals("")) ? "AND YEAR(C5_EMISSAO) = " + ano + "\r\n" : ""; 
		sql += (mes != null && !mes.equals("")) ? "AND MONTH(C5_EMISSAO) = " + mes + "\r\n" : "";
		sql += (dia != null && !dia.equals("")) ? "AND DAY(C5_EMISSAO) = " + dia + "\r\n" : "";
		if (produtos != null && !produtos.equals("")) {
			sql += "AND C6_PRODUTO IN (";
			
			for (int i = 0; i < produtos.replace(" ", ";").split(";").length; i++) {
				String produto = produtos.replace(" ", ";").split(";")[i];
				if (i > 0) {
					sql += ',';
				}
				sql += "'" + produto + "'";
			}
			
			sql += ")\r\n";
		}
		if (codigoGrupoProduto != null && !codigoGrupoProduto.equals("")) {
			sql += "AND B1_GRUPO = '" + codigoGrupoProduto + "'\r\n";
		}
		if (linhaProdutos != null && !linhaProdutos.equals("")) {
			sql += "AND BM_DESC = '" + linhaProdutos + "'\r\n";
		}
		if (estado != null && !estado.isEmpty()) {
			sql += "AND A1_EST = '" + estado + "'\r\n";
		}
		sql += "AND SC6010.D_E_L_E_T_ = ''\r\n" + 
		
				
				
				////////////////////UNION ALL
				"UNION ALL\r\n" +
				
				
				
				"SELECT\r\n" + 
				"	'DEVOLUCAO' AS TIPO, BM_DESC AS GRUPO, C6_NUM, C6_ITEM, A1_NREDUZ, D1_EMISSAO, C6_ENTREG, C6_PRODUTO, C6_DESCRI, D1_QUANT, D1_QUANT, -D1_VUNIT, -(D1_TOTAL - D1_VALDESC) AS D1_TOTAL, C5_CLASPED, A1_NOME, A1_COD, A1_EST\r\n" + 
				"FROM SD1010 WITH (NOLOCK)\r\n" + 
				"INNER JOIN SD2010 WITH (NOLOCK) ON D1_NFORI = D2_DOC AND SD2010.D_E_L_E_T_ = '' AND D1_ITEMORI = D2_ITEM AND D2_TIPO = 'N'\r\n" + 
				"INNER JOIN SC6010 WITH (NOLOCK) ON SC6010.D_E_L_E_T_ = '' AND C6_FILIAL <> 'ZZ' AND D2_PEDIDO = C6_NUM AND D2_ITEMPV = C6_ITEM\r\n" + 
				"INNER JOIN SC5010 WITH (NOLOCK) ON SC5010.D_E_L_E_T_ = '' AND C5_FILIAL <> 'ZZ' AND C5_NUM = C6_NUM\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON D1_COD = B1_COD AND SB1010.D_E_L_E_T_ = '' AND B1_FILIAL <> 'ZZ'\r\n" + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON SA1010.D_E_L_E_T_ = '' AND A1_FILIAL <> 'ZZ' AND A1_COD = D2_CLIENTE AND A1_LOJA = D2_LOJA\r\n" + 
				"INNER JOIN SBM010 WITH (NOLOCK) ON SBM010.D_E_L_E_T_ = '' AND BM_FILIAL <> 'ZZ' AND B1_GRUPO = BM_GRUPO\r\n" + 
				"WHERE\r\n" + 
				"SD1010.D_E_L_E_T_ = ''\r\n";
		if (clasped == null) { 
			sql += "AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n";
		} else if (clasped.equals(CLASPED.EQUIPAMENTOS)) {
			sql += "AND C5_CLASPED IN ('3', '6')\r\n";
		} else if (clasped.equals(CLASPED.MERCADOINTERNO)) {
			sql += "AND C5_CLASPED IN ('1', '2')\r\n";
		} else if (clasped.equals(CLASPED.EXPORTACAO)) {
			sql += "AND C5_CLASPED IN ('4')\r\n";
		} else if (clasped.equals(CLASPED.CAIXAS)) {
			sql += "AND C5_CLASPED IN ('2')";
		} else if (clasped.equals(CLASPED.REPOSICAO)) {
			sql += "AND C5_CLASPED IN ('1')";
		}
		if (unidade != null && unidade.equals(UNIDADE.ORTOPEDIA)) {
			sql += "AND C6_LOCAL IN ('01', '10')\r\n";
		} else if (unidade != null && unidade.equals(UNIDADE.EQUIPAMENTOS)) {
			sql += "AND C6_LOCAL IN ('11', '12', '14')\r\n";
		} else if (unidade != null && unidade.equals(UNIDADE.ORTOPEDIANACIONAL)) {
			sql += "AND C6_LOCAL IN ('01', '10')\r\n";
			sql += "AND A1_EST <> ('EX')\r\n";
		} else if (unidade != null && unidade.equals(UNIDADE.ORTOPEDIAEXPORTACAO)) {
			sql += "AND C6_LOCAL IN ('01', '10')\r\n";
			sql += "AND A1_EST = ('EX')\r\n";
		} else if (unidade != null && unidade.equals(UNIDADE.EQUIPAMENTOSNACIONAL)) {
			sql += "AND C6_LOCAL IN ('11', '12', '14')\r\n";
			sql += "AND A1_EST <> ('EX')\r\n";
		} else if (unidade != null && unidade.equals(UNIDADE.EQUIPAMENTOSEXPORTACAO)) {
			sql += "AND C6_LOCAL IN ('11', '12', '14')\r\n";
			sql += "AND A1_EST = ('EX')\r\n";
		}
		
		if (parametro != null && parametro.equals(ParametroVenda.VENDASCLIENTEXTIPO)) {
			sql += "AND YEAR(C5_EMISSAO) > YEAR(DATEADD(YEAR, -2, GETDATE()))\r\n";
		}
		sql += (cliente != null && !cliente.equals("")) ? "AND A1_NOME LIKE '%" + cliente + "%'\r\n" : "";
		sql += (codigoCliente != null && !codigoCliente.equals("")) ? "AND A1_COD = '" + codigoCliente + "'\r\n" : "";
		sql += (dataDe != null && !dataDe.equals("")) ? "AND D1_EMISSAO >= " + dataDe + "\r\n" : "";
		sql += (dataAte != null && !dataAte.equals("")) ? "AND D1_EMISSAO <= " + dataAte + "\r\n" : "";
		sql += (ano != null && !ano.equals("")) ? "AND YEAR(D1_EMISSAO) = " + ano + "\r\n" : ""; 
		sql += (mes != null && !mes.equals("")) ? "AND MONTH(D1_EMISSAO) = " + mes + "\r\n" : "";
		sql += (dia != null && !dia.equals("")) ? "AND DAY(D1_EMISSAO) = " + dia + "\r\n" : "";
		if (produtos != null && !produtos.equals("")) {
			sql += "AND D1_COD IN (";
			
			for (int i = 0; i < produtos.replace(" ", ";").split(";").length; i++) {
				String produto = produtos.replace(" ", ";").split(";")[i];
				if (i > 0) {
					sql += ',';
				}
				sql += "'" + produto + "'";
			}
			
			sql += ")\r\n";
		}
		if (codigoGrupoProduto != null && !codigoGrupoProduto.equals("")) {
			sql += "AND B1_GRUPO = '" + codigoGrupoProduto + "'\r\n";
		}
		if (linhaProdutos != null && !linhaProdutos.equals("")) {
			sql += "AND BM_DESC = '" + linhaProdutos + "'\r\n";
		}
		if (estado != null && !estado.isEmpty()) {
			sql += "AND A1_EST = '" + estado + "'\r\n";
		}
		sql += "AND D1_FILIAL <> 'ZZ'\r\n" + 
				"AND D1_TIPO = 'D'\r\n" + 
				") AS RESULTSET\r\n";
		if (parametro != null && parametro.equals(ParametroVenda.POREMISSAO)) {
			sql += "GROUP BY C5_EMISSAO";
		} else if (parametro != null && parametro.equals(ParametroVenda.MENSALPORCLASPED)) {
			sql += "GROUP BY C5_CLASPED";
		} else if (parametro != null && parametro.equals(ParametroVenda.PORCLASPEDPORDIA)) {
			sql += "GROUP BY DAY(C5_EMISSAO)\r\n" +
			"ORDER BY 1";
		} else if (parametro != null && parametro.equals(ParametroVenda.TOPCLIENTES)) {
			sql += "GROUP BY (RTRIM(A1_COD) + ';' + RTRIM(A1_NOME))\r\n" +
			"ORDER BY 2 DESC";
		} else if (parametro != null && parametro.equals(ParametroVenda.TOPPRODUTOS)) {
			if (unidade.equals(UNIDADE.ORTOPEDIA)) {
				sql += "GROUP BY LEFT(C6_PRODUTO, 4)\r\n";
			} else {
				sql += "GROUP BY C6_PRODUTO\r\n";
			}
			sql += "ORDER BY 2 DESC";
		} else if (parametro != null && parametro.equals(ParametroVenda.ANUALGERAL)) {
			sql += "GROUP BY CASE C5_CLASPED WHEN '1' THEN '4 - REPOSICAO' WHEN '2' THEN '3 - CAIXAS' WHEN '3' THEN '2 - EQUIPAMENTOS' WHEN '4' THEN '1 - EXPORTACAO' WHEN '6' THEN '2 - EQUIPAMENTOS' END\r\n" +
			"ORDER BY 1\r\n";
		} else if (parametro != null && parametro.equals(ParametroVenda.VENDASCLIENTEXTIPO)) {
			sql += "GROUP BY LEFT(C5_EMISSAO, 6), GRUPO\r\n" + 
					"ORDER BY 1, 2";
		} else if (parametro != null && parametro.equals(ParametroVenda.MAPAVENDAS)) {
			sql += "GROUP BY A1_COD, A1_EST, A1_NREDUZ\r\n" +
					"ORDER BY 5";
		}
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while(rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				String tempCliente = rs.getString(1);
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					if (parametro.equals(ParametroVenda.MAPAVENDAS)) {
						if (i == 1) {
							continue;
						} else if (i == 3) {
							linha.add(HtmlUtils.createLink("/clientes/" + tempCliente + "/detalhes/" + rs.getString(i), rs.getString(i)));
							continue;
						}
					}
					
					
					if (rs.getMetaData().getColumnName(i).startsWith("NUM_")) {
						linha.add(BigDecimal.valueOf(Double.parseDouble(rs.getString(i))).setScale(2, RoundingMode.HALF_EVEN));
					} else {
						linha.add(rs.getString(i));
					}
				}
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	
	public static ChartDataAndLabels getVendasAno(String ano, CLASPED unidadeVendas) {
		UNIDADE unidade = null;
		if (unidadeVendas == CLASPED.EXPORTACAOORTOPEDIA) {
			unidade = UNIDADE.ORTOPEDIAEXPORTACAO;
			unidadeVendas = CLASPED.EXPORTACAO;
		} else if (unidadeVendas == CLASPED.EXPORTACAOEQUIPAMENTOS) {
			unidade = UNIDADE.EQUIPAMENTOSEXPORTACAO;
			unidadeVendas = CLASPED.EXPORTACAO;
		}
		
		
		DataTable dataTable = getVendas(ParametroVenda.POREMISSAO, unidadeVendas, null, null, ano, null, unidade, null, null, null, null, null, null, null);
		
		ChartDataAndLabels cdl = new ChartDataAndLabels();
		
		LinkedList<String> chartLabels = new LinkedList<String>();
		String prefix = "";
		for (Collection<Object> collection : dataTable.getData()) {
			chartLabels.add(((LinkedList<Object>) collection).get(0).toString().substring(0, 6));
			if (prefix == "") {
				prefix = ((LinkedList<Object>) collection).get(0).toString().substring(0, 4);
			}
		}
		List<String> uniqueChartLabels = new ArrayList<String>();
		for (int i = 1; i <= 12; i++) {
			if (i < 10) {
				uniqueChartLabels.add(prefix + "0" + i);
			} else {
				uniqueChartLabels.add(prefix + i);
			}
		}
		
		
		
		
		cdl.setChartLabels(uniqueChartLabels);
		
		Collection<BigDecimal> chartData = new LinkedList<BigDecimal>();
		for (String string : uniqueChartLabels) {
			BigDecimal valor = BigDecimal.ZERO;
			for (Collection<Object> collection : dataTable.getData()) {
				if (string.equals(((LinkedList<Object>) collection).get(0).toString().substring(0, 6))) {
					valor = valor.add((BigDecimal) ((LinkedList<Object>) collection).get(2));
				}
			}
			chartData.add(valor);
		}
		cdl.setChartData(chartData);
		return cdl;
	}
	

	public static ChartDataAndLabels getVendasMensalPorClasPed() {
		String ano = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
		String mes = String.valueOf(Calendar.getInstance().get(Calendar.MONTH) + 1);
		
		DataTable dataTable = getVendas(ParametroVenda.MENSALPORCLASPED, null, null, mes, ano, null, null, null, null, null, null, null, null, null);
		
		ChartDataAndLabels cdl = new ChartDataAndLabels();
		
		for (Collection<Object> tempCollection : dataTable.getData()) {
			LinkedList<Object> collection = (LinkedList<Object>) tempCollection;
			String clasped = (String) collection.get(0);
			if (clasped.equals("1")) {
				cdl.getChartLabels().add("REPOSICAO");
			} else if (clasped.equals("2")) {
				cdl.getChartLabels().add("CAIXAS");
			} else if (clasped.equals("3") || clasped.equals("6")) {
				cdl.getChartLabels().add("EQUIPAMENTOS");
			} else if (clasped.equals("4")) {
				cdl.getChartLabels().add("EXPORTACAO");
			}
			cdl.getChartData().add((BigDecimal) collection.get(1));
		}
		
		return cdl;
	}

	public static ChartDataAndLabels getCarteiraPendente() {
		String sql = "SELECT CASE WHEN (GROUPING(CLASPED) = 1) THEN 'GLOBAL' ELSE CLASPED END AS CLASPED , SUM(C6_PRCVEN * (C6_QTDVEN - C6_QTDENT - QTD_EMP)) AS VALOR FROM (\r\n" + 
				"	SELECT\r\n" + 
				"	CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '4' THEN 'EXPORTACAO' WHEN '6' THEN 'EQUIPAMENTOS' ELSE C5_CLASPED END AS CLASPED,\r\n" + 
				"	C6_QTDVEN,\r\n" + 
				"	C6_QTDENT,\r\n" + 
				"	C6_VALOR,\r\n" + 
				"	CASE A1_EST WHEN 'EX' THEN C6_PRCVEN * 3.6 ELSE C6_PRCVEN END AS C6_PRCVEN,\r\n" + 
				"	ISNULL((SELECT SUM(C9_QTDLIB) FROM SC9010 WITH(NOLOCK) WHERE C9_PEDIDO = C5_NUM AND C6_ITEM = C9_ITEM AND SC9010.D_E_L_E_T_ = '' AND C9_LOTECTL <> '' AND C9_NFISCAL = '' AND C9_FILIAL <> 'ZZ'), 0) AS QTD_EMP\r\n" + 
				"	FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010 + 
				"	INNER JOIN SA1010 ON C5_CLIENTE = A1_COD\r\n" + 
				"		AND C5_LOJACLI = A1_LOJA\r\n" + 
				"		AND A1_FILIAL <> 'ZZ'\r\n" + 
				"		AND SA1010.D_E_L_E_T_ = ''\r\n" + 
				"	WHERE\r\n" + 
				"		SC6010.D_E_L_E_T_ = ''\r\n" + 
				"	   	AND C6_BLQ Not Like '%R%'\r\n" + 
				"		AND C5_XREFAT IN ('', '2')\r\n" + 
				"		AND C5_LIBEROK = 'S'" +
				"	   	AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" +
				"	   	AND C5_EMISSAO >= '20160801'\r\n" +
				"		AND C6_QTDVEN > C6_QTDENT\r\n" + 
				") AS RESULTSET\r\n" + 
				"GROUP BY CLASPED WITH ROLLUP\r\n" + 
				"ORDER BY VALOR DESC";
		
		ChartDataAndLabels cdl = new ChartDataAndLabels();
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("CLASPED"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getCarteiraPendente();
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ChartDataAndLabels getTopClientes(int numClientes, UNIDADE unidade, int ano) {
		ChartDataAndLabels cdl = new ChartDataAndLabels();
		DataTable vendas = getVendas(ParametroVenda.TOPCLIENTES, null, null, null, String.valueOf(ano), null, unidade, null, null, null, null, null, null, null);
		
		for (Collection<Object> collection : vendas.getData()) {
			LinkedList<Object> tempCollection = (LinkedList<Object>) collection;
			cdl.getChartLabels().add((String) tempCollection.get(0));
			cdl.getChartData().add((BigDecimal) tempCollection.get(1));
		}
		
		return cdl;
	}

	public static ChartDataAndLabels getVendasPorClaspedPorDia(CLASPED clasped) {
		String ano = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
		String mes = String.valueOf(Calendar.getInstance().get(Calendar.MONTH) + 1);
		
		DataTable dataTable = getVendas(ParametroVenda.PORCLASPEDPORDIA, clasped, null, mes, ano, null, null, null, null, null, null, null, null, null);
		ChartDataAndLabels cdl = new ChartDataAndLabels();
		
		for (Collection<Object> tempCollection : dataTable.getData()) {
			LinkedList<Object> collection = (LinkedList<Object>) tempCollection;
			cdl.addLabel((String) collection.get(0));
			cdl.addData((BigDecimal) collection.get(1));
		}
		
		return cdl;
	}

	public static ChartDataAndLabels getTopProdutos(int numProdutos, UNIDADE unidade, Integer ano) {
		ChartDataAndLabels cdl = new ChartDataAndLabels();
		
		DataTable dataTable = getVendas(ParametroVenda.TOPPRODUTOS, null, null, null, String.valueOf(ano), null, unidade, null, null, null, null, null, null, null);
		for (Collection<Object> collection : dataTable.getData()) {
			LinkedList<Object> tempCollection = (LinkedList<Object>) collection;
			cdl.getChartLabels().add((String) tempCollection.get(0));
			cdl.getChartData().add((BigDecimal) tempCollection.get(1));
		}
		return cdl;
	}

	public static List<PedidoVenda> getItensPedidosVendaDia(List<String> authorities) {
		String sql = "SELECT\r\n" + 
				"	A1_COD, CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '4' THEN 'EXPORTACAO' WHEN '6' THEN 'EQUIPAMENTOS' ELSE '' END AS C5_CLASPED, C6_NUM, CASE WHEN C6_LOCAL = '01' THEN 'ORTOPEDIA' WHEN C6_LOCAL IN ('11', '12', '14') THEN 'EQUIPAMENTOS' END AS UNIDADE, RTRIM(A1_NOME) AS A1_NOME, CASE A1_EST WHEN 'EX' THEN SUM(C6_VALOR * " + ConfigClass.taxaDolar + ") ELSE SUM(C6_VALOR) END AS VALOR_TOTAL\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010 + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENTE\r\n" + 
				"	AND C5_LOJACLI = A1_LOJA\r\n" + 
				"	AND A1_FILIAL <> 'ZZ'\r\n" + 
				"WHERE MONTH(C5_EMISSAO) = MONTH(GETDATE())\r\n" + 
				"	AND YEAR(C5_EMISSAO) = YEAR(GETDATE())\r\n" + 
				"	AND DAY(C5_EMISSAO) = DAY(GETDATE())\r\n" + 
				"	AND SC6010.D_E_L_E_T_ = ''\r\n" + 
				"	AND C6_FILIAL <> 'ZZ'\r\n" + 
				"	AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" +
				"	AND C6_CLI <> '000422'\r\n"; 
				sql += _C6_TES; 
				sql += "AND C5_XREFAT IN ('', '2')\r\n" + 
				"GROUP BY A1_COD, C5_CLASPED, C6_NUM, CASE WHEN C6_LOCAL = '01' THEN 'ORTOPEDIA' WHEN C6_LOCAL IN ('11', '12', '14') THEN 'EQUIPAMENTOS' END, RTRIM(A1_NOME), A1_EST\r\n" + 
				"ORDER BY VALOR_TOTAL DESC";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			List<PedidoVenda> pedidosVenda = new LinkedList<>();
			while (rs.next()) {
				PedidoVenda pedido = new PedidoVenda();
				pedido.setCliente(new Cliente().setCodigo(rs.getString("A1_COD")).setNome(rs.getString("A1_NOME")));
				pedido.setNumero(rs.getString("C6_NUM"));
				pedido.setUnidade(UNIDADE.valueOf(rs.getString("UNIDADE")));
				pedido.setClasPed(CLASPED.valueOf(rs.getString("C5_CLASPED")));
				
				if (authorities.contains("VENDAS-ORTOPEDIA")) {
					if (!pedido.getUnidade().equals(UNIDADE.ORTOPEDIA) && (!pedido.getClasPed().equals(CLASPED.CAIXAS) || !pedido.getClasPed().equals(CLASPED.REPOSICAO))) {
						continue;
					}
				} else if (authorities.contains("VENDAS-EQUIPAMENTOS")) {
					if (!pedido.getUnidade().equals(UNIDADE.EQUIPAMENTOS) && (!pedido.getClasPed().equals(CLASPED.EQUIPAMENTOS))) {
						continue;
					}
				} else if (authorities.contains("VENDAS-EXPORTACAO")) {
					if (!pedido.getUnidade().equals(UNIDADE.EXPORTACAO) && (!pedido.getClasPed().equals(CLASPED.EXPORTACAO))) {
						continue;
					}
				}
				
				pedido.setValorTotal(rs.getBigDecimal("VALOR_TOTAL").setScale(2, RoundingMode.HALF_EVEN));
				pedidosVenda.add(pedido);
			}
			return pedidosVenda;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static PedidoVenda findOne(String numero) {
		String sql = "SELECT\r\n" + 
				"C6_XLIBPED, C5_LIBEROK, C5_COMIS1, C5_COMIS2, C5_COMIS3, C5_COMIS4, C5_COMIS5, C5_EMISSAO,CASE C5_XTPVEND WHEN '1' THEN 'REPRESENTANTE' WHEN '2' THEN 'DISTRIBUIDOR' WHEN '3' THEN 'LICITACAO' WHEN '4' THEN 'VENDA DIRETA' ELSE '' END AS C5_XTPVEND,C6_NUM,A1_NOME,A1_COD,C5_XDTENTR,CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '4' THEN 'EXPORTACAO' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '6' THEN 'EQUIPAMENTOS' END AS C5_CLASPED\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010.replace("AND C5_CLIENTE <> '000422'\r\n", "") + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENTE\r\n" + 
				"AND A1_FILIAL <> 'ZZ'\r\n" + 
				"AND SA1010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE\r\n" + 
				"C6_NUM = ?\r\n" + 
				"AND C6_FILIAL <> 'ZZ'\r\n" + 
				"AND C6_BLQ <> 'R'" +
				"AND SC6010.D_E_L_E_T_ = ''";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setString(1, numero);
			ResultSet rs = stmt.executeQuery();
			PedidoVenda pedido = new PedidoVenda();

			while (rs.next()) {
				pedido.setNumero(rs.getString("C6_NUM"));
				pedido.setCliente(new Cliente().setNome(rs.getString("A1_NOME")).setCodigo(rs.getString("A1_COD")));
				pedido.setDataEmissao(rs.getString("C5_EMISSAO"));
				pedido.setDataLiberacao(rs.getString("C6_XLIBPED"));
				pedido.setDataEntrega(rs.getString("C5_XDTENTR"));
				pedido.setClasPed(CLASPED.valueOf(rs.getString("C5_CLASPED")));
				pedido.setTipoVenda(rs.getString("C5_XTPVEND"));
				pedido.setComis1(rs.getBigDecimal("C5_COMIS1"));
				pedido.setComis2(rs.getBigDecimal("C5_COMIS2"));
				pedido.setComis3(rs.getBigDecimal("C5_COMIS3"));
				pedido.setComis4(rs.getBigDecimal("C5_COMIS4"));
				pedido.setComis5(rs.getBigDecimal("C5_COMIS5"));
				pedido.setLiberok(rs.getString("C5_LIBEROK"));
			}
			pedido.setItensPedidoVenda(getItensPedidoVenda(numero));
			return pedido;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return findOne(numero);
			} else {
				e.printStackTrace();
			}
		}

		return null;
	}

	public static <E> List<ItemPedidoVenda> getItensPedidoVenda(String numero) {
		String sql = "SELECT\r\n" + 
				"B1_X_CUST, RTRIM(C6_ITEM) AS C6_ITEM, RTRIM(C6_PRODUTO) AS C6_PRODUTO, B1_DESC, C6_QTDVEN, C6_QTDENT, CASE C5_MOEDA WHEN '1' THEN C6_PRCVEN WHEN '2' THEN C6_PRCVEN * " + ConfigClass.taxaDolar + " END AS C6_PRCVEN, CASE C5_MOEDA WHEN '1' THEN C6_VALOR WHEN '2' THEN C6_VALOR * " + ConfigClass.taxaDolar + " END AS C6_VALOR, C5_MOEDA\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010.replace("AND C5_CLIENTE <> '000422'\r\n", "") + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON B1_COD = C6_PRODUTO\r\n" + 
				"AND B1_FILIAL <> 'ZZ'\r\n" + 
				"AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE\r\n" + 
				"C6_NUM = ?\r\n" + 
				"AND C6_FILIAL <> 'ZZ'\r\n" + 
				"AND C6_BLQ <> 'R'\r\n" +
				"AND SC6010.D_E_L_E_T_ = ''";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setString(1, numero);
			ResultSet rs = stmt.executeQuery();
			LinkedList<ItemPedidoVenda> itensPedidoVenda = new LinkedList<>();
			while (rs.next()) {
				ItemPedidoVenda item = new ItemPedidoVenda();
				item.setPedido(new PedidoVenda().setNumero(numero));
				item.setItem(rs.getString("C6_ITEM"));
				item.setProduto(new Produto().setCodigo(rs.getString("C6_PRODUTO")).setDescricao(rs.getString("B1_DESC")));
				item.setQtdVen(rs.getBigDecimal("C6_QTDVEN"));
				item.setQtdEnt(rs.getBigDecimal("C6_QTDENT"));
				item.setPrcVen(rs.getBigDecimal("C6_PRCVEN"));
				item.setCusto(rs.getBigDecimal("B1_X_CUST"));
				itensPedidoVenda.add(item);
			}
			return itensPedidoVenda;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getItensPedidoVenda(numero);
			} else {
				e.printStackTrace();
			}
		}

		return null;
	}

	public static ChartDataAndLabels getHistoricoVendas(Cliente cliente) {
		String sql = "SELECT EMISSAO, SUM(CASE C6_BLQ WHEN 'R' THEN VALOR/C6_QTDVEN*C6_QTDENT ELSE VALOR END) AS VALOR FROM (\r\n"
				+ "SELECT\r\n"
				+ "SUBSTRING(C5_EMISSAO,1,6) AS EMISSAO,CASE A1_EST WHEN 'EX' THEN C6_VALOR * " + ConfigClass.taxaDolar + " ELSE C6_VALOR END AS VALOR,C6_BLQ,C6_PRCVEN,C6_QTDENT,C6_QTDVEN\r\n"
				+ "FROM SC6010 WITH(NOLOCK)\r\n"
				+ _joinSC5010;
				sql += "INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENTE\r\n"
				+ "AND C5_LOJACLI = A1_LOJA\r\n"
						+ "AND A1_FILIAL <> 'ZZ'\r\n"
				+ "WHERE ";
				sql += "YEAR(C5_EMISSAO) >= YEAR(DATEADD(YEAR, -3, GETDATE()))\r\n";
				sql += "AND SC6010.D_E_L_E_T_ = ''\r\n"
				+ "AND C6_FILIAL <> 'ZZ'\r\n";
				sql += _C6_TES;
				sql += "AND C5_CLIENTE = ?\r\n" +
				"AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n"
				+ "AND C5_XREFAT IN ('', '2')\r\n"
				+ ") AS RESULTSET\r\n" + "GROUP BY EMISSAO\r\n" + "ORDER BY EMISSAO";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setString(1, cliente.getCodigo());
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				cdl.getChartLabels().add(DataUtils.formatMes(rs.getString("EMISSAO")));
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getHistoricoVendas(cliente);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ChartDataAndLabels getProdutosMaisComprados(String codCliente) {
		String sql = "SELECT\r\n" + 
				"TOP 15 RTRIM(C6_PRODUTO) + ';' + RTRIM(C6_DESCRI) AS C6_PRODUTO,\r\n" + 
				"SUM(C6_VALOR) AS VALOR\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010;
				sql += _C6_TES; 
				sql += "AND C5_EMISSAO >= DATEADD(MONTH, -12, GETDATE())\r\n" + 
				"WHERE C6_FILIAL <> 'ZZ'\r\n" + 
				"AND SC6010.D_E_L_E_T_ = ''\r\n" + 
				"AND C6_BLQ <> 'R'\r\n" +
				"AND C5_CLIENTE = ?\r\n" +
				"AND C5_LIBEROK = 'S'\r\n" +
				"GROUP BY RTRIM(C6_PRODUTO) + ';' + RTRIM(C6_DESCRI)\r\n" + 
				"ORDER BY VALOR DESC\r\n" + 
				"";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setString(1, codCliente);
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
				cdl.getChartLabels().add(rs.getString("C6_PRODUTO"));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getProdutosMaisComprados(codCliente);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static LinkedList<ItemPedidoVenda> getCarteiraPendente(String codCliente) {
		String sql = "SELECT C5_CLASPED,C6_NUM, A1_NREDUZ, C5_EMISSAO, C6_ENTREG, C6_PRODUTO, C6_DESCRI,C6_PRCVEN, C6_QTDVEN, C6_QTDENT, QTD_EMP, (C6_QTDVEN - C6_QTDENT - QTD_EMP) AS SALDO, ((C6_QTDVEN - C6_QTDENT) * C6_PRCVEN) AS VALOR_SALDO,\r\n" + 
				"(ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = B1_LOCPAD AND D_E_L_E_T_ = ''), 0) + ISNULL((SELECT SUM(DA_SALDO) FROM SDA010 WITH(NOLOCK) WHERE DA_PRODUTO = B1_COD AND DA_SALDO <> 0 AND D_E_L_E_T_ = '' AND DA_LOCAL = B1_LOCPAD), 0)) AS QTD_ESTOQUE,\r\n" + 
				"ISNULL((SELECT SUM(C2_QUANT - C2_QUJE - C2_PERDA) FROM SC2010 WITH(NOLOCK) WHERE C2_PRODUTO = B1_COD AND C2_DATRF='' AND C2_LOCAL=B1_LOCPAD AND D_E_L_E_T_='' AND C2_STATUS IN ('N', '')), 0) AS QTD_OP\r\n" + 
				"FROM (\r\n" + 
				"SELECT\r\n" + 
				"CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '4' THEN 'EXPORTACAO' WHEN '6' THEN 'EQUIPAMENTOS' END AS C5_CLASPED,C6_NUM,C5_EMISSAO,C6_ENTREG,C6_PRODUTO,C6_DESCRI,C6_QTDVEN,C6_QTDENT,\r\n" + 
				"(SELECT ISNULL(SUM(C9_QTDLIB), 0) FROM SC9010 WITH(NOLOCK) WHERE C9_PEDIDO = C5_NUM AND C9_ITEM = C6_ITEM AND C9_PRODUTO = C6_PRODUTO AND SC9010.D_E_L_E_T_ = '' AND C9_NFISCAL = '' AND C9_LOTECTL <> '') AS QTD_EMP,\r\n" + 
				"CASE A1_EST WHEN 'EXT' THEN C6_PRCVEN * 3.6 ELSE C6_PRCVEN END AS C6_PRCVEN,A1_NREDUZ\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010 + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENTE\r\n" + 
				"AND A1_LOJA = C5_LOJACLI\r\n" + 
				"AND A1_FILIAL <> 'ZZ'\r\n" + 
				"WHERE\r\n" + 
				"C6_QTDVEN > C6_QTDENT\r\n" + 
				"AND C5_CLIENTE = '" + codCliente + "'" +
				"AND C6_BLQ <> 'R'\r\n" + 
				"AND SC6010.D_E_L_E_T_ = ''\r\n" + 
				"AND C5_LIBEROK = 'S'\r\n" +
				"AND C5_CLASPED IN ('1', '2', '3', '4', '6')" +
				"AND C5_CLIENTE <> '000422') AS RESULTSET\r\n" + 
				"INNER JOIN SB1010 ON C6_PRODUTO = B1_COD\r\n" + 
				"AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"ORDER BY C5_EMISSAO";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<ItemPedidoVenda> itensVenda = new LinkedList<>();
			while(rs.next()) {
				ItemPedidoVenda itemVenda = new ItemPedidoVenda();
				itemVenda.setPedido(new PedidoVenda().setNumero(rs.getString("C6_NUM")).setDataEmissao(rs.getString("C5_EMISSAO")).setDataEntrega(rs.getString("C6_ENTREG")));
				itemVenda.setProduto(new Produto().setCodigo(rs.getString("C6_PRODUTO")).setDescricao(rs.getString("C6_DESCRI")));
				itemVenda.setPrcVen(rs.getBigDecimal("C6_PRCVEN").setScale(2, RoundingMode.HALF_EVEN));
				itemVenda.setQtdVen(rs.getBigDecimal("C6_QTDVEN").setScale(2, RoundingMode.HALF_EVEN));
				itemVenda.setQtdEnt(rs.getBigDecimal("C6_QTDENT").setScale(2, RoundingMode.HALF_EVEN));
				itemVenda.setQtdEmp(rs.getBigDecimal("QTD_EMP").setScale(2, RoundingMode.HALF_EVEN));
				itensVenda.add(itemVenda);
			}
			return itensVenda;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getCarteiraPendente(codCliente);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ChartDataAndLabels getCompradoresProduto(String codigoProduto) {
		String sql = "SELECT TOP 15\r\n" + 
				"A1_COD, RTRIM(A1_NREDUZ) AS NOME,SUM(C6_QTDVEN) AS QTD_COMPRADA\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON C6_PRODUTO = B1_COD\r\n" + 
				"AND B1_FILIAL <> 'ZZ'\r\n" + 
				"AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				_joinSC5010 +
				"INNER JOIN SA1010 WITH(NOLOCK) ON C5_CLIENTE = A1_COD\r\n" + 
				"AND C5_LOJACLI = A1_LOJA\r\n" + 
				"AND SA1010.D_E_L_E_T_ = ''\r\n" + 
				"AND A1_FILIAL <> 'ZZ'\r\n" + 
				"WHERE SC6010.D_E_L_E_T_ = ''\r\n" + 
				"AND C6_PRODUTO = ?\r\n" + 
				"AND C6_FILIAL <> 'ZZ'\r\n" +
				"AND C6_BLQ <> 'R'\r\n" +
				"AND C5_LIBEROK = 'S'\r\n" +
				"AND C5_EMISSAO > DATEADD(MONTH, -24, GETDATE())\r\n" +
				"AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" +
				"GROUP BY A1_COD, A1_NREDUZ\r\n" + 
				"ORDER BY QTD_COMPRADA DESC";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setString(1, codigoProduto);
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("A1_COD") + ";" + rs.getString("NOME"));
				cdl.getChartData().add(rs.getBigDecimal("QTD_COMPRADA"));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getCompradoresProduto(codigoProduto);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static Iterable<ItemPedidoVenda> getCarteiraPendente(Produto produto) {
		String sql = "SELECT C5_CLASPED,C6_NUM, A1_NREDUZ, C5_EMISSAO, C6_ENTREG, C6_PRODUTO, C6_DESCRI,C6_PRCVEN, C6_QTDVEN, C6_QTDENT, QTD_EMP, (C6_QTDVEN - C6_QTDENT - QTD_EMP) AS SALDO, ((C6_QTDVEN - C6_QTDENT - QTD_EMP) * C6_PRCVEN) AS VALOR_SALDO,\r\n" + 
				"(ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = B1_LOCPAD AND D_E_L_E_T_ = ''), 0) + ISNULL((SELECT SUM(DA_SALDO) FROM SDA010 WITH(NOLOCK) WHERE DA_PRODUTO = B1_COD AND DA_SALDO <> 0 AND D_E_L_E_T_ = '' AND DA_LOCAL = B1_LOCPAD), 0)) AS QTD_ESTOQUE,\r\n" + 
				"ISNULL((SELECT SUM(C2_QUANT - C2_QUJE - C2_PERDA) FROM SC2010 WITH(NOLOCK) WHERE C2_PRODUTO = B1_COD AND C2_DATRF='' AND C2_LOCAL=B1_LOCPAD AND D_E_L_E_T_='' AND C2_STATUS IN ('N', '')), 0) AS QTD_OP\r\n" + 
				"FROM (\r\n" + 
				"SELECT\r\n" + 
				"CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '4' THEN 'EXPORTACAO' WHEN '6' THEN 'EQUIPAMENTOS' END AS C5_CLASPED,C6_NUM,C5_EMISSAO,C6_ENTREG,C6_PRODUTO,C6_DESCRI,C6_QTDVEN,C6_QTDENT,\r\n" + 
				"(SELECT ISNULL(SUM(C9_QTDLIB), 0) FROM SC9010 WITH(NOLOCK) WHERE C9_PEDIDO = C5_NUM AND C9_ITEM = C6_ITEM AND C9_PRODUTO = C6_PRODUTO AND SC9010.D_E_L_E_T_ = '' AND C9_NFISCAL = '' AND C9_LOTECTL <> '') AS QTD_EMP,\r\n" + 
				"CASE A1_EST WHEN 'EXT' THEN C6_PRCVEN * 3.6 ELSE C6_PRCVEN END AS C6_PRCVEN,A1_NREDUZ\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010 + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENTE\r\n" + 
				"AND A1_LOJA = C5_LOJACLI\r\n" + 
				"AND A1_FILIAL <> 'ZZ'\r\n" + 
				"WHERE\r\n" + 
				"C6_QTDVEN > C6_QTDENT\r\n" + 
				"AND C6_BLQ <> 'R'\r\n" + 
				"AND SC6010.D_E_L_E_T_ = ''\r\n" + 
				"AND C5_LIBEROK = 'S'\r\n" +
				"AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" +
				"AND C6_PRODUTO = '" + produto.getCodigo() + "'\r\n" +
				"AND C5_CLIENTE <> '000422') AS RESULTSET\r\n" + 
				"INNER JOIN SB1010 ON C6_PRODUTO = B1_COD\r\n" + 
				"AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"ORDER BY C5_EMISSAO";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<ItemPedidoVenda> itensVenda = new LinkedList<>();
			while(rs.next()) {
				ItemPedidoVenda itemVenda = new ItemPedidoVenda();
				itemVenda.setPedido(new PedidoVenda().setNumero(rs.getString("C6_NUM")).setDataEmissao(rs.getString("C5_EMISSAO")).setDataEntrega(rs.getString("C6_ENTREG")).setCliente(new Cliente().setnReduz(rs.getString("A1_NREDUZ"))));
				itemVenda.setProduto(new Produto().setCodigo(rs.getString("C6_PRODUTO")).setDescricao(rs.getString("C6_DESCRI")));
				itemVenda.setPrcVen(rs.getBigDecimal("C6_PRCVEN").setScale(2, RoundingMode.HALF_EVEN));
				itemVenda.setQtdVen(rs.getBigDecimal("C6_QTDVEN").setScale(2, RoundingMode.HALF_EVEN));
				itemVenda.setQtdEnt(rs.getBigDecimal("C6_QTDENT").setScale(2, RoundingMode.HALF_EVEN));
				itemVenda.setQtdEmp(rs.getBigDecimal("QTD_EMP").setScale(2, RoundingMode.HALF_EVEN));
				itensVenda.add(itemVenda);
			}
			return itensVenda;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getCarteiraPendente(produto);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static DataTable getCarteiraPendenteTabela(CLASPED clasped) {
		String sql = "SELECT C5_CLIENTE, BM_DESC, C6_XLIBPED, A1_NOME, C6_LOCAL, UNIDADE,A1_MSBLQL,C5_CLASPED,C6_NUM, C6_ITEM, A1_NREDUZ, C5_EMISSAO, C6_ENTREG, C6_PRODUTO, C6_DESCRI,C6_PRCVEN, C6_QTDVEN, C6_QTDENT, QTD_EMP, (C6_QTDVEN - C6_QTDENT - QTD_EMP) AS SALDO, ((C6_QTDVEN - C6_QTDENT) * CASE WHEN C5_CONDPAG = '999' THEN 0 ELSE C6_PRCVEN END) AS VALOR_SALDO, C5_CONDPAG AS COND_PGTO,\r\n" + 
				"(ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = B1_LOCPAD AND D_E_L_E_T_ = ''), 0) + ISNULL((SELECT SUM(DA_SALDO) FROM SDA010 WITH(NOLOCK) WHERE DA_PRODUTO = B1_COD AND DA_SALDO <> 0 AND D_E_L_E_T_ = '' AND DA_LOCAL = B1_LOCPAD), 0)) AS QTD_ESTOQUE,\r\n" + 
				"ISNULL((SELECT SUM(C2_QUANT - C2_QUJE - C2_PERDA) FROM SC2010 WITH(NOLOCK) WHERE C2_PRODUTO = B1_COD AND C2_DATRF='' AND C2_LOCAL=B1_LOCPAD AND D_E_L_E_T_='' AND C2_STATUS IN ('N', '')), 0) AS QTD_OP\r\n" + 
				"FROM (\r\n" + 
				"SELECT\r\n" + 
				"C5_CLIENTE, C5_CONDPAG, C6_ITEM, C6_XLIBPED, A1_NOME, C6_LOCAL, CASE WHEN C6_LOCAL = '01' THEN 'ORTOPEDIA' WHEN C6_LOCAL IN ('11', '12', '14') THEN 'EQUIPAMENTOS' END AS UNIDADE,CASE A1_MSBLQL WHEN '0' THEN 'ATIVO' WHEN '1' THEN 'BLOQUEADO' WHEN '2' THEN 'ATIVO' END AS A1_MSBLQL, CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '4' THEN 'EXPORTACAO' WHEN '6' THEN 'EQUIPAMENTOS' END AS C5_CLASPED,C6_NUM,C5_EMISSAO,C6_ENTREG,C6_PRODUTO,C6_DESCRI,C6_QTDVEN,C6_QTDENT,\r\n" + 
				"(SELECT ISNULL(SUM(C9_QTDLIB), 0) FROM SC9010 WITH(NOLOCK) WHERE C9_PEDIDO = C5_NUM AND C9_ITEM = C6_ITEM AND C9_PRODUTO = C6_PRODUTO AND SC9010.D_E_L_E_T_ = '' AND C9_NFISCAL = '' AND C9_LOTECTL <> '') AS QTD_EMP,\r\n" + 
				"CASE WHEN C5_MOEDA = '4' THEN  C6_PRCVEN * " + ConfigClass.taxaEuro + " WHEN A1_EST = 'EX' THEN C6_PRCVEN * " + ConfigClass.taxaDolar + " ELSE C6_PRCVEN END AS C6_PRCVEN,A1_NREDUZ\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010+ 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENTE\r\n" + 
				"AND A1_LOJA = C5_LOJACLI\r\n" + 
				"AND A1_FILIAL <> 'ZZ'\r\n" + 
				"WHERE\r\n" + 
				"C6_QTDVEN > C6_QTDENT\r\n" + 
				"AND C6_BLQ <> 'R'\r\n" + 
				"AND SC6010.D_E_L_E_T_ = ''\r\n" +
				"AND C5_LIBEROK = 'S'\r\n";
				if (clasped == null) {
					sql += "AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n";
				} else if (clasped.equals(CLASPED.EQUIPAMENTOS)) {
					sql += "AND C5_CLASPED IN ('3', '6')\r\n";
				} else if (clasped.equals(CLASPED.EXPORTACAO)) {
					sql += "AND C5_CLASPED = '4'\r\n";
				} else if (clasped.equals(CLASPED.CAIXAS)) {
					sql += "AND C5_CLASPED = '2'\r\n";
				} else if (clasped.equals(CLASPED.REPOSICAO)) {
					sql += "AND C5_CLASPED = '1'\r\n";
				}
				sql += "AND C5_CLIENTE <> '000422') AS RESULTSET\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON C6_PRODUTO = B1_COD\r\n" +
				"INNER JOIN SBM010 WITH(NOLOCK) ON BM_FILIAL <> 'ZZ' AND BM_GRUPO = B1_GRUPO AND SBM010.D_E_L_E_T_ = ''\r\n" +
				"AND SB1010.D_E_L_E_T_ = ''\r\n" +
				"ORDER BY C5_EMISSAO";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while(rs.next()) {
				Collection<Object> row = new LinkedList<>();
				row.add(rs.getString("UNIDADE"));
				row.add(rs.getString("COND_PGTO"));
				row.add(rs.getString("C6_LOCAL"));
				row.add(HtmlUtils.createLink("/pedidos-venda/" + rs.getString("C6_NUM") + "/detalhes", rs.getString("C6_NUM")));
				row.add(rs.getString("C6_ITEM"));
				row.add(rs.getString("A1_MSBLQL"));
				row.add(rs.getString("C5_CLIENTE"));
				row.add(rs.getString("A1_NOME"));
				row.add(rs.getString("C5_CLASPED"));
				row.add(rs.getString("C5_EMISSAO"));
				row.add(rs.getString("C6_XLIBPED"));
				row.add(rs.getString("C6_ENTREG"));
				row.add(HtmlUtils.createLink("/produtos/detalhes?codigoProduto=" + rs.getString("C6_PRODUTO"), rs.getString("C6_PRODUTO")));
				row.add(rs.getString("C6_DESCRI"));
				row.add(rs.getString("BM_DESC"));
				row.add(rs.getString("C6_QTDVEN"));
				row.add(rs.getString("C6_QTDENT"));
				row.add(rs.getString("QTD_EMP"));
				row.add(rs.getString("SALDO"));
				row.add(rs.getBigDecimal("QTD_ESTOQUE").setScale(2, RoundingMode.HALF_EVEN));
				row.add(rs.getString("QTD_OP"));
				row.add(rs.getBigDecimal("C6_PRCVEN"));
				row.add(rs.getBigDecimal("VALOR_SALDO"));
				data.add(row);
			}
			return new DataTable().setData(data);
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getCarteiraPendenteTabela(clasped);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static File getCarteiraPendenteExcel() {
		String sql = "SELECT C5_CLASPED,C6_NUM, A1_NREDUZ, C5_EMISSAO, C6_ENTREG, C6_PRODUTO, C6_DESCRI,C6_PRCVEN, C6_QTDVEN, C6_QTDENT, QTD_EMP, (C6_QTDVEN - C6_QTDENT - QTD_EMP) AS SALDO, ((C6_QTDVEN - C6_QTDENT - QTD_EMP) * C6_PRCVEN) AS VALOR_SALDO, C6_PRCVEN * QTD_EMP AS VALOR_EMPENHADO FROM (\r\n" + 
				"SELECT\r\n" + 
				"CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '4' THEN 'EXPORTACAO' WHEN '6' THEN 'EQUIPAMENTOS' END AS C5_CLASPED,C6_NUM,C5_EMISSAO,C6_ENTREG,C6_PRODUTO,C6_DESCRI,C6_QTDVEN,C6_QTDENT,ISNULL(C9_QTDLIB, 0) AS QTD_EMP,CASE A1_EST WHEN 'EXT' THEN C6_PRCVEN * 3.6 ELSE C6_PRCVEN END AS C6_PRCVEN,A1_NREDUZ\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010 + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENTE\r\n" + 
				"AND A1_LOJA = C5_LOJACLI\r\n" + 
				"AND A1_FILIAL <> 'ZZ'\r\n" + 
				"LEFT JOIN SC9010 WITH(NOLOCK) ON C9_PEDIDO = C5_NUM\r\n" + 
				"AND C9_ITEM = C6_ITEM\r\n" + 
				"AND C9_PRODUTO = C6_PRODUTO\r\n" + 
				"AND SC9010.D_E_L_E_T_ = ''\r\n" + 
				"AND C9_NFISCAL = ''\r\n" + 
				"AND C9_LOTECTL <> ''\r\n" +
				"WHERE\r\n" + 
				"C6_QTDVEN > C6_QTDENT\r\n" + 
				"AND C6_BLQ <> 'R'\r\n" + 
				"AND C5_LIBEROK = 'S'\r\n" + 
				"AND C5_CLASPED IN ('1', '2', '3', '4', '6')" +
				"AND SC6010.D_E_L_E_T_ = ''\r\n";
				sql += _C6_TES;
				sql += "AND C5_CLIENTE <> '000422'" +
				") AS RESULTSET\r\n" + 
				"ORDER BY C5_EMISSAO";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			File file = File.createTempFile("Carteira-Excel", ".xls");
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("Carteira");
			int rownum = 0;
			int column = 0;
			HSSFRow row = sheet.createRow(rownum++);
			row.createCell(column++).setCellValue("Numero");
			row.createCell(column++).setCellValue("Cliente");
			row.createCell(column++).setCellValue("Clas. Ped");
			row.createCell(column++).setCellValue("Emissao");
			row.createCell(column++).setCellValue("Entrega");
			row.createCell(column++).setCellValue("Produto");
			row.createCell(column++).setCellValue("Descricao");
			row.createCell(column++).setCellValue("Qtd Vend");
			row.createCell(column++).setCellValue("Qtd Ent");
			row.createCell(column++).setCellValue("Qtd Emp");
			row.createCell(column++).setCellValue("Saldo");
			row.createCell(column++).setCellValue("Preço Venda");
			row.createCell(column++).setCellValue("Valor Saldo");
			row.createCell(column++).setCellValue("Valor Empenhado");
			while(rs.next()) {
				row = sheet.createRow(rownum++);
				column = 0;
				row.createCell(column++).setCellValue(rs.getString("C6_NUM"));
				row.createCell(column++).setCellValue(rs.getString("A1_NREDUZ"));
				row.createCell(column++).setCellValue(rs.getString("C5_CLASPED"));
				row.createCell(column++).setCellValue(rs.getString("C5_EMISSAO"));
				row.createCell(column++).setCellValue(rs.getString("C6_ENTREG"));
				row.createCell(column++).setCellValue(rs.getString("C6_PRODUTO"));
				row.createCell(column++).setCellValue(rs.getString("C6_DESCRI"));
				row.createCell(column++).setCellValue(rs.getDouble("C6_QTDVEN"));
				row.createCell(column++).setCellValue(rs.getDouble("C6_QTDENT"));
				row.createCell(column++).setCellValue(rs.getDouble("QTD_EMP"));
				row.createCell(column++).setCellValue(rs.getDouble("SALDO"));
				row.createCell(column++).setCellValue(rs.getDouble("C6_PRCVEN"));
				row.createCell(column++).setCellValue(rs.getDouble("VALOR_SALDO"));
				row.createCell(column++).setCellValue(rs.getDouble("VALOR_EMPENHADO"));
			}
			workbook.write(file);
			workbook.close();
			return file;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getCarteiraPendenteExcel();
			} else {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static ChartDataAndLabels getVendasAno(int ano, UNIDADE unidade) {
		String sql = "SELECT EMISSAO, SUM(CASE C6_BLQ WHEN 'R' THEN VALOR/C6_QTDVEN*C6_QTDENT ELSE VALOR END) AS VALOR FROM (\r\n"
				+ "SELECT\r\n"
				+ "SUBSTRING(C5_EMISSAO,1,6) AS EMISSAO,CASE A1_EST WHEN 'EX' THEN C6_VALOR * " + ConfigClass.taxaDolar + " ELSE C6_VALOR END AS VALOR,C6_BLQ,C6_PRCVEN,C6_QTDENT,C6_QTDVEN\r\n"
				+ "FROM SC6010 WITH(NOLOCK)\r\n"
				+ _joinSC5010;
				if (unidade.equals(UNIDADE.ORTOPEDIA)) {
					sql += "AND C6_LOCAL = '01'\r\n";
				} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
					sql += "AND C6_LOCAL IN ('11', '12', '14')\r\n";
				}
				sql += "INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENTE\r\n"
				+ "AND C5_LOJACLI = A1_LOJA\r\n"
						+ "AND A1_FILIAL <> 'ZZ'\r\n"
				+ "WHERE ";
				sql += "YEAR(C5_EMISSAO) = '" + ano + "'\r\n";
				sql += "AND SC6010.D_E_L_E_T_ = ''\r\n"
				+ "AND C6_CLI <> '000422'"
				+ "AND C6_FILIAL <> 'ZZ'\r\n";
				sql += _C6_TES;
				sql += "AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n"
				+ "AND C5_XREFAT IN ('', '2')\r\n"
				+ "AND C5_LIBEROK = 'S'\r\n"
				+ ") AS RESULTSET\r\n" + "GROUP BY EMISSAO\r\n" + "ORDER BY EMISSAO";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("EMISSAO"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getVendasAno(ano, unidade);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ChartDataAndLabels getVendasAnoHorizontal(int ano, UNIDADE unidade) {
		String sql = "SELECT EMISSAO, SUM(CASE C6_BLQ WHEN 'R' THEN VALOR/C6_QTDVEN*C6_QTDENT ELSE VALOR END) AS VALOR FROM (\r\n"
				+ "SELECT\r\n"
				+ "SUBSTRING(C5_EMISSAO,1,4) AS EMISSAO,CASE A1_EST WHEN 'EX' THEN C6_VALOR * " + ConfigClass.taxaDolar + " ELSE C6_VALOR END AS VALOR,C6_BLQ,C6_PRCVEN,C6_QTDENT,C6_QTDVEN\r\n"
				+ "FROM SC6010 WITH(NOLOCK)\r\n"
				+ _joinSC5010;
				if (unidade.equals(UNIDADE.ORTOPEDIA)) {
					sql += "AND C6_LOCAL = '01'\r\n";
				} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
					sql += "AND C6_LOCAL IN ('11', '12', '14')\r\n";
				}
				sql += "INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENTE\r\n"
				+ "AND C5_LOJACLI = A1_LOJA\r\n"
				+ "AND A1_FILIAL <> 'ZZ'\r\n"
				+ "WHERE "
				+ "YEAR(C5_EMISSAO) = '" + ano + "'\r\n"
				+ "AND SC6010.D_E_L_E_T_ = ''\r\n"
				+ "AND C6_CLI <> '000422'"
				+ "AND C6_FILIAL <> 'ZZ'\r\n";
				sql += _C6_TES;
				sql += "AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n"
				+ "AND C5_XREFAT IN ('', '2')\r\n"
				+ "AND C5_LIBEROK = 'S'\r\n"
				+ ") AS RESULTSET\r\n"
				+ "GROUP BY EMISSAO\r\n"
				+ "ORDER BY EMISSAO";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("EMISSAO"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getVendasAnoHorizontal(ano, unidade);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ChartDataAndLabels getVendasHorizontal(int ano) {
		String anoAsString = String.valueOf(ano);
		
		DataTable dataTable = getVendas(ParametroVenda.ANUALGERAL, null, null, null, anoAsString, null, null, null, null, null, null, null, null, null);
		
		ChartDataAndLabels cdl = new ChartDataAndLabels();
		
		for (Iterator<Collection<Object>> iterator = dataTable.getData().iterator(); iterator.hasNext();) {
			LinkedList<Object> linha = (LinkedList<Object>) iterator.next();
			cdl.addLabel((String) linha.get(0));
		}
		
		Collection<BigDecimal> chartData = new LinkedList<BigDecimal>();
		for (String string : cdl.getChartLabels()) {
			BigDecimal valor = BigDecimal.ZERO;
			for (Collection<Object> collection : dataTable.getData()) {
				if (string.equals(((LinkedList<Object>) collection).get(0).toString())) {
					valor = valor.add((BigDecimal) ((LinkedList<Object>) collection).get(1));
				}
			}
			chartData.add(valor);
		}
		cdl.setChartData(chartData);
		
		return cdl;
	}

	public static ChartDataAndLabels getIndicadoresEntregas(Cliente cliente, Integer ano) {
//		return null;
		
		
		String sql = "SELECT\r\n" + 
				"CASE WHEN DIAS_ENTREGA < 30 THEN '<30' WHEN DIAS_ENTREGA < 60 THEN '<60' WHEN DIAS_ENTREGA < 90 THEN '<90' ELSE '>120' END AS LEADTIME, COUNT(D2_COD) AS OCORRENCIAS\r\n" + 
				"FROM (\r\n" + 
				"SELECT\r\n" + 
				"C6_PRODUTO, D2_COD, C5_EMISSAO AS EMISSAO, D2_EMISSAO, DATEDIFF(DAY, C5_EMISSAO, D2_EMISSAO) AS DIAS_ENTREGA\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				"JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM AND SC5010.D_E_L_E_T_ = ''\r\n" + 
				"JOIN SD2010 WITH(NOLOCK) ON D2_PEDIDO = C5_NUM AND SD2010.D_E_L_E_T_ = '' AND D2_ITEMPV = C6_ITEM\r\n" + 
				"WHERE SC6010.D_E_L_E_T_ = ''\r\n" + 
				"AND YEAR(C5_EMISSAO) = '" + ano + "'\r\n" + 
				"AND C5_CLIENTE = '" + cliente.getCodigo() + "'\r\n" + 
				"AND C5_TIPO = 'N'\r\n" + 
				"AND C6_CF IN ('5116', '6116', '6101', '5101', '6107', '5107')\r\n" + 
				") AS RESULTSET\r\n" + 
				"GROUP BY CASE WHEN DIAS_ENTREGA < 30 THEN '<30' WHEN DIAS_ENTREGA < 60 THEN '<60' WHEN DIAS_ENTREGA < 90 THEN '<90' ELSE '>120' END\r\n" + 
				"ORDER BY 1";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("LEADTIME"));
				cdl.getChartData().add(rs.getBigDecimal("OCORRENCIAS").setScale(0, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getIndicadoresEntregas(cliente, ano);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static Iterable<ItemPedidoVenda> getProdutosCarteira(UNIDADE unidade) {
		String sql = "SELECT C6_PRODUTO, SUM(C6_QTDVEN) AS C6_QTDVEN, SUM(C6_QTDENT) AS C6_QTDENT, SUM(QTD_EMP) AS QTD_EMP FROM (\r\n" + 
				"SELECT\r\n" + 
				"C6_NUM,C6_PRODUTO,C6_QTDVEN,C6_QTDENT,ISNULL(C9_QTDLIB, 0) AS QTD_EMP\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010 + 
				"LEFT JOIN SC9010 WITH(NOLOCK) ON C9_PEDIDO = C5_NUM\r\n" + 
				"AND C9_ITEM = C6_ITEM\r\n" + 
				"AND C9_PRODUTO = C6_PRODUTO\r\n" + 
				"AND SC9010.D_E_L_E_T_ = ''\r\n" + 
				"AND C9_NFISCAL = ''\r\n" + 
				"AND C9_LOTECTL <> ''\r\n" + 
				"WHERE\r\n" + 
				"C6_QTDVEN > C6_QTDENT\r\n" + 
				"AND C5_LIBEROK = 'S'\r\n" + 
				"AND C6_BLQ <> 'R'\r\n" + 
				"AND SC6010.D_E_L_E_T_ = ''\r\n";
				sql += (unidade.equals(UNIDADE.ORTOPEDIA)) ? "AND C6_LOCAL = '01'\r\n" : (unidade.equals(UNIDADE.EQUIPAMENTOS)) ? "AND C6_LOCAL IN ('11', '12', '14')\r\n" : "";
				sql += ") AS RESULTSET\r\n" + 
				"GROUP BY C6_PRODUTO\r\n" + 
				"ORDER BY C6_PRODUTO";
		
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<ItemPedidoVenda> itensVenda = new LinkedList<>();
			while(rs.next()) {
				ItemPedidoVenda itemVenda = new ItemPedidoVenda();
				itemVenda.setProduto(new Produto().setCodigo(rs.getString("C6_PRODUTO")));
				itemVenda.setQtdVen(rs.getBigDecimal("C6_QTDVEN").setScale(2, RoundingMode.HALF_EVEN));
				itemVenda.setQtdEnt(rs.getBigDecimal("C6_QTDENT").setScale(2, RoundingMode.HALF_EVEN));
				itemVenda.setQtdEmp(rs.getBigDecimal("QTD_EMP").setScale(2, RoundingMode.HALF_EVEN));
				itensVenda.add(itemVenda);
			}
			return itensVenda;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getProdutosCarteira(unidade);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ChartDataAndLabels getHistoricoVendasProdutos(String[] codigoProdutos) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < codigoProdutos.length; i++) {
			if (i < (codigoProdutos.length - 1)) 
				sb.append("'" + codigoProdutos[i] + "',");
			else
				sb.append("'" + codigoProdutos[i] + "'");
		}
		String sql = "SELECT EMISSAO, SUM(C6_QTDVEN) AS C6_QTDVEN FROM (\r\n"
				+ "SELECT\r\n"
				+ "B1_LOCPAD, SUBSTRING(C5_EMISSAO,1,6) AS EMISSAO,CASE A1_EST WHEN 'EX' THEN C6_VALOR * " + ConfigClass.taxaDolar + " ELSE C6_VALOR END AS VALOR,C6_BLQ,C6_PRCVEN,C6_QTDENT,C6_QTDVEN\r\n"
				+ "FROM SC6010 WITH(NOLOCK)\r\n"
				+ _joinSC5010
				+ "INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENTE\r\n"
				+ "AND C5_LOJACLI = A1_LOJA\r\n"
				+ "AND A1_FILIAL <> 'ZZ'\r\n"
				+ "INNER JOIN SB1010 WITH(NOLOCK) ON C6_PRODUTO = B1_COD AND B1_FILIAL <> 'ZZ' AND SB1010.D_E_L_E_T_ = ''\r\n"
				+ "WHERE "
				+ "YEAR(C5_EMISSAO) >= YEAR(DATEADD(YEAR, -4, GETDATE()))\r\n"
				+ "AND SC6010.D_E_L_E_T_ = ''\r\n"
				+ "AND C6_CLI <> '000422'"
				+ "AND C6_FILIAL <> 'ZZ'\r\n";
				sql += _C6_TES;
				sql += "AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n"
				+ "AND C5_XREFAT IN ('', '2')\r\n"
				+ "AND C5_LIBEROK = 'S'\r\n"
				+ "AND C6_LOCAL = B1_LOCPAD\r\n";
				sql += "AND C6_PRODUTO IN (" + sb.toString() + ")\r\n"
				+ ") AS RESULTSET\r\n" + "GROUP BY EMISSAO\r\n" + "ORDER BY EMISSAO";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("EMISSAO"));
				cdl.getChartData().add(rs.getBigDecimal("C6_QTDVEN").setScale(0, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getHistoricoVendasProdutos(codigoProdutos);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static DataTable getTabelaPrecos(UNIDADEVENDAS unidadeVendas) {
		String sql = "SELECT\r\n" + 
					"DA1_QTDLOT,DA1_CODPRO,B1_DESC,DA1_PRCVEN,CASE DA1_MOEDA WHEN '1' THEN 'REAL' WHEN '2' THEN 'DOLAR' WHEN '4' THEN 'EURO' END AS DA1_MOEDA\r\n" + 
					"FROM DA1010 WITH(NOLOCK)\r\n" + 
					"INNER JOIN SB1010 WITH(NOLOCK) ON DA1_CODPRO = B1_COD\r\n" + 
					"AND SB1010.D_E_L_E_T_ = ''\r\n" + 
					"AND B1_FILIAL <> 'ZZ'\r\n" + 
					"WHERE DA1_CODTAB = ?\r\n" + 
					"AND DA1010.D_E_L_E_T_ = ''";
		try(PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			if (unidadeVendas.equals(UNIDADEVENDAS.MERCADOINTERNO)) {
				stmt.setString(1, "083");
			} else if (unidadeVendas.equals(UNIDADEVENDAS.EXPORTACAO)) {
				stmt.setString(1, "090");
			} else if (unidadeVendas.equals(UNIDADEVENDAS.EQUIPAMENTOS)) {
				stmt.setString(1, "100");
			}
			
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				linha.add(rs.getString("DA1_CODPRO"));
				linha.add(rs.getString("B1_DESC"));
				linha.add(rs.getBigDecimal("DA1_QTDLOT").setScale(0, RoundingMode.HALF_EVEN));
				linha.add(rs.getBigDecimal("DA1_PRCVEN").setScale(2, RoundingMode.HALF_EVEN));
				linha.add(rs.getString("DA1_MOEDA"));
				data.add(linha);
			}
			return new DataTable().setData(data);
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getTabelaPrecos(unidadeVendas);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static DataTable getComparativo(String tipoPeriodo, Integer anoInicial, Integer anoFinal) {
		String sql = "";
		if (tipoPeriodo.equals("MENSAL")) {
			sql += "SELECT MES, SUM(VALOR) FROM (\r\n";
		}
		if (tipoPeriodo.equals("BIMESTRAL")) {
			sql += "SELECT BIMESTRE, SUM(VALOR) FROM (\r\n";
			sql += 	"	SELECT\r\n" + 
					"		CASE WHEN MONTH(C5_EMISSAO) IN (1, 2) THEN '1 BI'\r\n" + 
					"			WHEN MONTH(C5_EMISSAO) IN (3, 4) THEN '2 BI'\r\n" + 
					"			WHEN MONTH(C5_EMISSAO) IN (5, 6) THEN '3 BI'\r\n" + 
					"			WHEN MONTH(C5_EMISSAO) IN (7, 8) THEN '4 BI'\r\n" + 
					"			WHEN MONTH(C5_EMISSAO) IN (9, 10) THEN '5 BI'\r\n" + 
					"			WHEN MONTH(C5_EMISSAO) IN (11, 12) THEN '6 BI'\r\n" + 
					"		END AS BIMESTRE,\r\n";
		}
		if (tipoPeriodo.equals("TRIMESTRAL")) {
			sql += "SELECT TRIMESTRE, SUM(VALOR) FROM (\r\n";
			sql += 	"	SELECT\r\n" + 
					"		CASE WHEN MONTH(C5_EMISSAO) IN (1, 2, 3) THEN '1 BI'\r\n" + 
					"			WHEN MONTH(C5_EMISSAO) IN (4, 5, 6) THEN '2 BI'\r\n" + 
					"			WHEN MONTH(C5_EMISSAO) IN (7, 8, 9) THEN '3 BI'\r\n" + 
					"			WHEN MONTH(C5_EMISSAO) IN (10, 11, 12) THEN '4 BI'\r\n" + 
					"		END AS TRIMESTRE,\r\n";
		}
		if (tipoPeriodo.equals("SEMESTRAL")) {
			sql += "SELECT SEMESTRE, SUM(VALOR) FROM (\r\n";
			sql += 	"	SELECT\r\n" + 
					"		CASE WHEN MONTH(C5_EMISSAO) IN (1, 2, 3, 4, 5, 6) THEN '1 BI'\r\n" + 
					"			WHEN MONTH(C5_EMISSAO) IN (7, 8, 9, 10, 11, 12) THEN '2 BI'\r\n" + 
					"		END AS SEMESTRE,\r\n";
		}
		sql +=  "		C6_VALOR AS VALOR\r\n" + 
				"	FROM SC5010 WITH(NOLOCK)\r\n" + 
				"	INNER JOIN SC6010 WITH(NOLOCK) ON C5_NUM = C6_NUM\r\n" + 
				"		AND SC6010.D_E_L_E_T_ = ''\r\n" + 
				"		AND C6_BLQ <> 'R'\r\n" + 
				"	WHERE SC5010.D_E_L_E_T_ = ''\r\n" + 
				"	AND YEAR(C5_EMISSAO) = 2018\r\n" + 
				"	AND C5_TIPO = 'N'\r\n" + 
				") AS RESULTSET\r\n" + 
				"GROUP BY BIMESTRE";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					linha.add(rs.getString(i));
				}
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public static DataTable getMapaVendas(UNIDADE unidade) {
		return getVendas(ParametroVenda.MAPAVENDAS, null, null, null, null, null, unidade, null, null, null, null, null, null, null);
	}

	public static DataTable getCarteiraPcp(String filtro, Iterable<PedidoManual> pedidosManuais) {
		String sql = "SELECT BM_DESC, A1_NOME, C6_XLIBPED, C6_ENTREG, C6_VALOR, C6_ITEM, C6_LOCAL, UNIDADE,A1_MSBLQL,C5_CLASPED,C6_NUM, A1_NREDUZ, C5_EMISSAO, C6_ENTREG, C6_PRODUTO, C6_DESCRI,C6_PRCVEN, C6_QTDVEN, C6_QTDENT, QTD_EMP, (C6_QTDVEN - C6_QTDENT - QTD_EMP) AS SALDO, ((C6_QTDVEN - C6_QTDENT - QTD_EMP) * C6_PRCVEN) AS VALOR_SALDO,C5_LIBEROK,\r\n" + 
				"(ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = B1_LOCPAD AND D_E_L_E_T_ = ''), 0) + ISNULL((SELECT SUM(DA_SALDO) FROM SDA010 WITH(NOLOCK) WHERE DA_PRODUTO = B1_COD AND DA_SALDO <> 0 AND D_E_L_E_T_ = '' AND DA_LOCAL = B1_LOCPAD), 0)) AS QTD_ESTOQUE,\r\n" + 
				"ISNULL((SELECT SUM(C2_QUANT - C2_QUJE - C2_PERDA) FROM SC2010 WITH(NOLOCK) WHERE C2_PRODUTO = B1_COD AND C2_DATRF='' AND C2_LOCAL=B1_LOCPAD AND D_E_L_E_T_='' AND C2_STATUS IN ('N', '')), 0) AS QTD_OP\r\n" +
				"FROM (\r\n" + 
				"	SELECT\r\n" + 
				"		A1_NOME, C6_XLIBPED, C6_VALOR, C6_ITEM, C6_LOCAL, CASE WHEN C6_LOCAL = '01' THEN 'ORTOPEDIA' WHEN C6_LOCAL IN ('11', '12', '14') THEN 'EQUIPAMENTOS' END AS UNIDADE,CASE A1_MSBLQL WHEN '0' THEN 'ATIVO' WHEN '1' THEN 'BLOQUEADO' WHEN '2' THEN 'ATIVO' END AS A1_MSBLQL, CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '4' THEN 'EXPORTACAO' WHEN '6' THEN 'EQUIPAMENTOS' END AS C5_CLASPED,C6_NUM,C5_EMISSAO,C6_ENTREG,C6_PRODUTO,C6_DESCRI,C6_QTDVEN,C6_QTDENT,C5_LIBEROK,\r\n" + 
				"		(SELECT ISNULL(SUM(C9_QTDLIB), 0) FROM SC9010 WITH(NOLOCK) WHERE C9_PEDIDO = C5_NUM AND C9_ITEM = C6_ITEM AND C9_PRODUTO = C6_PRODUTO AND SC9010.D_E_L_E_T_ = '' AND C9_NFISCAL = '' AND C9_LOTECTL <> '') AS QTD_EMP,\r\n" + 
				"		CASE A1_EST WHEN 'EXT' THEN C6_PRCVEN * 3.6 ELSE C6_PRCVEN END AS C6_PRCVEN,A1_NREDUZ\r\n" + 
				"	FROM SC6010 WITH(NOLOCK)\r\n" + 
				"	INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM\r\n" + 
				"		AND C5_FILIAL <> 'ZZ'\r\n" + 
				"		AND SC5010.D_E_L_E_T_ = ''\r\n" + 
				"		AND C5_TIPO = 'N'\r\n" + 
				"		AND C5_XREFAT IN ('', '2')\r\n" + 
				"	INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENTE\r\n" + 
				"		AND A1_LOJA = C5_LOJACLI\r\n" + 
				"		AND A1_FILIAL <> 'ZZ'\r\n" + 
				"	INNER JOIN SB1010 WITH(NOLOCK) ON C6_PRODUTO = B1_COD\r\n" + 
				"		AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"	INNER JOIN SF4010 ON SF4010.F4_CODIGO = C6_TES AND SF4010.D_E_L_E_T_ = ''\r\n" +
				"	WHERE\r\n" + 
				"		(C6_QTDVEN > C6_QTDENT\r\n" + 
				"		AND C6_BLQ <> 'R'\r\n" + 
				"		AND SC6010.D_E_L_E_T_ = ''\r\n" + 
				"		AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" +
				"		AND NOT C6_CF IN ('6922', '5922', '5118', '6118')\r\n" +
				"		AND (F4_DUPLIC = 'S' OR C6_CF IN ('5914', '6116', '5116', '5949', '6949'))\r\n";
				if (filtro.equals("PAS")) {
					sql += "	AND C5_LIBEROK = 'S'\r\n" +
					"	AND C6_LOCAL IN ('12'))\r\n";
				} else {
					sql += "		AND C6_LOCAL IN ('11', '12', '14'))\r\n";
				}
				for (PedidoManual pedidoManual : pedidosManuais) {
					sql += "OR\r\n" + 
					"	(C6_NUM = '" + pedidoManual.getNumero() + "'\r\n" + 
					"	AND C6_ITEM = '" + pedidoManual.getItem() + "'\r\n" + 
					"	AND SC6010.D_E_L_E_T_ = '')\r\n";
				}
				sql += ") AS RESULTSET\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON C6_PRODUTO = B1_COD\r\n";
				if (filtro.equals("PAS")) {
					sql += "AND B1_GRUPO IN ('0036', '0043', '0045', '0044', '0046', '0049', '0050', '0041', '0053', '0059')\r\n";
				}
				sql += "INNER JOIN SBM010 WITH(NOLOCK) ON BM_FILIAL <> 'ZZ' AND BM_GRUPO = B1_GRUPO AND SBM010.D_E_L_E_T_ = ''\r\n";
				sql += "AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"ORDER BY C5_EMISSAO";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				linha.add("");//PRIORIDADE
				linha.add(HtmlUtils.createLink("/carteira-pcp/" + rs.getString("C6_NUM") + "/" + rs.getString("C6_ITEM") + "/acompanhamento", rs.getString("C6_NUM")));
				linha.add(rs.getString("C6_ITEM"));
				linha.add(rs.getString("A1_NOME"));
				linha.add(rs.getString("C6_PRODUTO"));
				linha.add(rs.getString("C6_DESCRI"));
				linha.add(rs.getString("BM_DESC"));
				linha.add("");//MES PRODUCAO
				linha.add("");//PFI
				linha.add("");//PROCESSO
				linha.add("");//DESC OPC
				linha.add("");//MEMO
				linha.add(rs.getString("C6_QTDVEN"));
				linha.add(rs.getString("C6_QTDENT"));
				linha.add(rs.getString("C6_XLIBPED"));
				linha.add(rs.getString("C6_ENTREG"));
				for (int i = 0; i < 16; i++) {
					linha.add("");
				}
				linha.add(rs.getString("C5_LIBEROK"));
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static ItemPedidoVenda getItemPedido(String numeroPedido, String numItemPedido) {
		String sql = "SELECT C6_XDESCRI, C6_BLQ, C6_XLIBPED, C6_ITEM, A1_NOME, C5_CLASPED,C6_NUM, A1_NREDUZ, C5_EMISSAO, C6_ENTREG, C6_PRODUTO, C6_DESCRI,C6_PRCVEN, C6_QTDVEN, C6_QTDENT, QTD_EMP, (C6_QTDVEN - C6_QTDENT - QTD_EMP) AS SALDO, ((C6_QTDVEN - C6_QTDENT - QTD_EMP) * C6_PRCVEN) AS VALOR_SALDO,\r\n" + 
				"(ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = B1_LOCPAD AND D_E_L_E_T_ = ''), 0) + ISNULL((SELECT SUM(DA_SALDO) FROM SDA010 WITH(NOLOCK) WHERE DA_PRODUTO = B1_COD AND DA_SALDO <> 0 AND D_E_L_E_T_ = '' AND DA_LOCAL = B1_LOCPAD), 0)) AS QTD_ESTOQUE,\r\n" + 
				"ISNULL((SELECT SUM(C2_QUANT - C2_QUJE - C2_PERDA) FROM SC2010 WITH(NOLOCK) WHERE C2_PRODUTO = B1_COD AND C2_DATRF='' AND C2_LOCAL=B1_LOCPAD AND D_E_L_E_T_='' AND C2_STATUS IN ('N', '')), 0) AS QTD_OP\r\n" + 
				"FROM (\r\n" + 
				"SELECT\r\n" + 
				"C6_XDESCRI, C6_BLQ, C6_XLIBPED, C6_ITEM, A1_NOME, CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '4' THEN 'EXPORTACAO' WHEN '6' THEN 'EQUIPAMENTOS' END AS C5_CLASPED,C6_NUM,C5_EMISSAO,C6_ENTREG,C6_PRODUTO,C6_DESCRI,C6_QTDVEN,C6_QTDENT,\r\n" + 
				"(SELECT ISNULL(SUM(C9_QTDLIB), 0) FROM SC9010 WITH(NOLOCK) WHERE C9_PEDIDO = C5_NUM AND C9_ITEM = C6_ITEM AND C9_PRODUTO = C6_PRODUTO AND SC9010.D_E_L_E_T_ = '' AND C9_NFISCAL = '' AND C9_LOTECTL <> '') AS QTD_EMP,\r\n" + 
				"CASE A1_EST WHEN 'EXT' THEN C6_PRCVEN * 3.6 ELSE C6_PRCVEN END AS C6_PRCVEN,A1_NREDUZ\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				_joinSC5010.replace("AND C5_CLIENTE <> '000422'\r\n", "") + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENTE\r\n" + 
				"AND A1_LOJA = C5_LOJACLI\r\n" + 
				"AND A1_FILIAL <> 'ZZ'\r\n" + 
				"WHERE\r\n" + 
				"SC6010.D_E_L_E_T_ = ''\r\n" + 
				"AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" +
				"AND C6_NUM = '" + numeroPedido + "'\r\n" +
				"AND C6_ITEM = '" + numItemPedido + "'\r\n" +
				") AS RESULTSET\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON C6_PRODUTO = B1_COD\r\n" + 
				"AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"ORDER BY C5_EMISSAO";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			ItemPedidoVenda itemPedido = new ItemPedidoVenda();
			if (rs.next()) {
				itemPedido.setCliente(rs.getString("A1_NOME"));
				itemPedido.setItem(rs.getString("C6_ITEM"));
				itemPedido.setPedido(new PedidoVenda().setNumero(rs.getString("C6_NUM")).setDataEmissao(rs.getString("C5_EMISSAO")).setDataLiberacao(rs.getString("C6_XLIBPED")).setDataEntrega(rs.getString("C6_ENTREG")));
				itemPedido.setPrcVen(rs.getBigDecimal("C6_PRCVEN").setScale(2, RoundingMode.HALF_EVEN));
				itemPedido.setProduto(new Produto().setCodigo(rs.getString("C6_PRODUTO")).setDescricao(rs.getString("C6_DESCRI")));
				itemPedido.setQtdEmp(rs.getBigDecimal("QTD_EMP").setScale(2, RoundingMode.HALF_EVEN));
				itemPedido.setQtdEnt(rs.getBigDecimal("C6_QTDENT").setScale(2, RoundingMode.HALF_EVEN));
				itemPedido.setQtdVen(rs.getBigDecimal("C6_QTDVEN").setScale(2, RoundingMode.HALF_EVEN));
				itemPedido.setXDesc(rs.getString("C6_XDESCRI"));
				itemPedido.setBlq(rs.getString("C6_BLQ"));
			}
			return itemPedido;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static ChartDataAndLabels getDashBoardMes(int mes, int ano) {
		String sql = "SELECT EMISSAO, SUM(CASE C6_BLQ WHEN 'R' THEN VALOR/C6_QTDVEN*C6_QTDENT ELSE VALOR END) AS VALOR FROM (\r\n" + 
				"SELECT\r\n" + 
				"SUBSTRING(C5_EMISSAO,1,6) AS EMISSAO,CASE A1_EST WHEN 'EX' THEN C6_VALOR * 3.7 ELSE C6_VALOR END AS VALOR,C6_BLQ,C6_PRCVEN,C6_QTDENT,C6_QTDVEN\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM\r\n" + 
				"AND C5_FILIAL <> 'ZZ'\r\n" + 
				"AND SC5010.D_E_L_E_T_ = ''\r\n" + 
				"AND C5_TIPO = 'N'\r\n" + 
				"AND C5_XREFAT IN ('', '2')\r\n" + 
				"AND C5_CLIENTE <> '000422'\r\n" + 
				"AND C5_CLASPED IN ('1', '2')\r\n" + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENTE\r\n" + 
				"AND C5_LOJACLI = A1_LOJA\r\n" + 
				"AND A1_FILIAL <> 'ZZ'\r\n" + 
				"WHERE (YEAR(C5_EMISSAO) = '" + ano + "' OR YEAR(C5_EMISSAO) = '" + (ano - 1) + "')\r\n" + 
				"AND MONTH(C5_EMISSAO) = '" + mes + "'\r\n" + 
				"AND DAY(C5_EMISSAO) <= DAY(GETDATE())\r\n" +
				"AND SC6010.D_E_L_E_T_ = ''\r\n" + 
				"AND C6_FILIAL <> 'ZZ'\r\n" + 
				"AND ((C6_TES LIKE '5%') OR (C6_TES IN ('644', '649', '701', '702', '703')))\r\n" + 
				"AND C5_XREFAT IN ('', '2')\r\n" + 
				"AND C6_LOCAL IN ('01', '11', '12', '14')) AS RESULTSET\r\n" + 
				"GROUP BY EMISSAO\r\n" + 
				"ORDER BY EMISSAO";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while(rs.next()) {
				cdl.getChartLabels().add(rs.getString("EMISSAO"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR"));
			}
			return cdl;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String atualizaUltimoPedido() {
		String sql = "SELECT TOP 1\r\n" + 
				"	C6_NUM\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM\r\n" + 
				"AND C5_FILIAL <> 'ZZ'\r\n" + 
				"AND C5_TIPO = 'N'\r\n" + 
				"AND C5_XREFAT IN ('', '2')\r\n" + 
				"AND C5_CLIENTE <> '000422'\r\n" + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENTE\r\n" + 
				"	AND C5_LOJACLI = A1_LOJA\r\n" + 
				"	AND A1_FILIAL <> 'ZZ'\r\n" + 
				"WHERE MONTH(C5_EMISSAO) = MONTH(GETDATE())\r\n" + 
				"	AND YEAR(C5_EMISSAO) = YEAR(GETDATE())\r\n" + 
				"	AND DAY(C5_EMISSAO) = DAY(GETDATE())\r\n" + 
				"	AND C6_FILIAL <> 'ZZ'\r\n" + 
				"	AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" + 
				"	AND C6_CLI <> '000422'\r\n" + 
				"AND ((C6_TES LIKE '5%') OR (C6_TES IN ('644', '649', '701', '702', '703')))\r\n" + 
				"AND C5_XREFAT IN ('', '2')\r\n" + 
				"ORDER BY C5_EMISSAO DESC";
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				ultimoPedido = rs.getString(1);
				return ultimoPedido;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
		
	}

	public static boolean temNovoPedido() {
		if (ultimoPedido == null) {
			return false;
		}
		String tempPedido = ultimoPedido;
		if (!tempPedido.equals(atualizaUltimoPedido())) {
			return true;
		}
		return false;
	}

	public static PedidoVenda getUltimoPedido() {
		return findOne(ultimoPedido);
	}

	public static DataTable getVendasSemCusto() {
		String sql = "SELECT\r\n" + 
				"	C6_PRODUTO, B1_DESC, B1_LOCPAD, SUM(C6_QTDVEN) AS QTDVEN, SUM(C6_VALOR) AS VALOR\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON B1_FILIAL <> 'ZZ' AND B1_COD = C6_PRODUTO AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"INNER JOIN SC5010 WITH(NOLOCK) ON C5_FILIAL <> 'ZZ' AND C6_NUM = C5_NUM AND SC5010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE C6_FILIAL <> 'ZZ'\r\n" + 
				"	AND SC6010.D_E_L_E_T_ = ''\r\n" + 
				"	AND SB1010.B1_X_CUST = ''\r\n" + 
				"	AND C5_TIPO = 'N'\r\n" + 
				"	AND C6_BLQ <> 'R'\r\n" + 
				"	AND C5_EMISSAO > '20190101'\r\n" + 
				"	AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" + 
				"GROUP BY C6_PRODUTO, B1_DESC, B1_LOCPAD\r\n" + 
				"ORDER BY AVG(C6_VALOR) DESC";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					linha.add(rs.getString(i));
				}
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

//	public static DataTable getRelacaoVendas(
//			String pedidoDe,
//			String pedidoAte,
//			String produtoDe,
//			String produtoAte,
//			String clienteDe,
//			String clienteAte,
//			String dataDe,
//			String dataAte,
//			String localDe,
//			String localAte, 
//			String linhaProdutos) {
//		String sql = "SELECT\r\n" + 
//				"	C5_NUM, C5_EMISSAO, C6_PRODUTO, B1_DESC, C6_QTDVEN, C6_QTDENT, CASE WHEN C6_BLQ = 'R' THEN 0 ELSE C6_QTDVEN - C6_QTDENT END AS SALDO, C6_PRCVEN, C6_VALOR, BM_DESC, C6_BLQ, A1_EST, A1_NOME, A1_COD\r\n" + 
//				"	FROM SC6010 WITH(NOLOCK)\r\n" + 
//				"	INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM\r\n" + 
//				"	AND C5_FILIAL <> 'ZZ'\r\n" + 
//				"	AND SC5010.D_E_L_E_T_ = ''\r\n" + 
//				"	AND C5_TIPO = 'N'\r\n" + 
//				"	AND C5_XREFAT IN ('', '2')\r\n" + 
//				"	AND C5_CLIENTE <> '000422'\r\n" + 
//				"	AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" + 
//				"	INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENTE\r\n" + 
//				"	AND C5_LOJACLI = A1_LOJA\r\n" + 
//				"	AND A1_FILIAL <> 'ZZ'\r\n" + 
//				"	INNER JOIN SB1010 WITH(NOLOCK) ON B1_FILIAL <> 'ZZ' AND B1_COD = C6_PRODUTO AND SB1010.R_E_C_D_E_L_ = ''\r\n" +
//				"	LEFT JOIN SBM010 WITH(NOLOCK) ON BM_FILIAL <> 'ZZ' AND BM_GRUPO = B1_GRUPO AND SBM010.R_E_C_D_E_L_ = ''\r\n" +
//				"	WHERE\r\n" + 
//				"	SC6010.D_E_L_E_T_ = ''\r\n" + 
//				"	AND C6_FILIAL <> 'ZZ'\r\n" + 
//				"	AND ((C6_TES LIKE '5%') OR (C6_TES IN ('644', '649', '701', '702', '703')) OR (C6_CF IN ('5116', '6116')))\r\n";
//		
//		if (pedidoDe != null && !pedidoDe.equals("")) {
//			sql += "AND C6_NUM >= '" + pedidoDe + "'\r\n";
//		}
//		if (pedidoAte != null && !pedidoAte.equals("")) {
//			sql += "AND C6_NUM <= '" + pedidoAte + "'\r\n";
//		}
//		if (produtoDe != null && !produtoDe.equals("")) {
//			sql += "AND C6_PRODUTO >= '" + produtoDe + "'\r\n";
//		}
//		if (produtoAte != null && !produtoAte.equals("")) {
//			sql += "AND C6_PRODUTO <= '" + produtoAte + "'\r\n";
//		}
//		if (clienteDe != null && !clienteDe.equals("")) {
//			sql += "AND C5_CLIENTE >= '" + clienteDe + "'\r\n";
//		}
//		if (clienteAte != null && !clienteAte.equals("")) {
//			sql += "AND C5_CLIENTE <= '" + clienteAte + "'\r\n";
//		}
//		if (dataDe != null && !dataDe.equals("")) {
//			sql += "AND C5_EMISSAO >= '" + dataDe + "'\r\n";
//		}
//		if (dataAte != null && !dataAte.equals("")) {
//			sql += "AND C5_EMISSAO <= '" + dataAte + "'\r\n";
//		}
//		if (localDe != null && !localDe.equals("")) {
//			sql += "AND C6_LOCAL >= '" + localDe + "'\r\n";
//		}
//		if (localAte != null && !localAte.equals("")) {
//			sql += "AND C6_LOCAL <= '" + localAte + "'\r\n";
//		}
//		if (linhaProdutos != null && !linhaProdutos.equals("")) {
//			sql += "AND BM_DESC = '" + linhaProdutos + "'\r\n";
//		}
//		
//		Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
//		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
//			ResultSet rs = stmt.executeQuery();
//			while (rs.next()) {
//				Collection<Object> linha = new LinkedList<Object>();
//				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
//					String columnName = rs.getMetaData().getColumnName(i);
//					if (columnName.equals("C5_NUM")) {
//						linha.add(HtmlUtils.createLink("/pedidos-venda/" + rs.getString(i) + "/detalhes", rs.getString(i)));
//					} else if (columnName.equals("A1_COD")) {
//						linha.add(HtmlUtils.createLink("/clientes/" + rs.getString(i) + "/detalhes/", rs.getString(i)));
//					} else if (columnName.equals("C6_PRODUTO")) {
//						linha.add(HtmlUtils.createLink("/produtos/detalhes?codigoProduto=" + rs.getString(i), rs.getString(i)));
//					} else {
//						linha.add(rs.getString(i));
//					}
//				}
//				data.add(linha);
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return new DataTable(data);
//	}

	public static DataTable getVendasPorLinha(UNIDADE unidade, TipoVendasPorLinha tipoVendasPorLinha, String cliente, String dataDe) {
		String sql = "SELECT \r\n";
		if (tipoVendasPorLinha != null && tipoVendasPorLinha.equals(TipoVendasPorLinha.METAS)) {
			sql += "CASE WHEN C5_CLASPED = '2' THEN 'CAIXAS' ELSE BM_GRUPO END AS 'BM_GRUPO', CASE WHEN C5_CLASPED = '2' THEN 'CAIXAS' ELSE BM_DESC END AS GRUPO, SUM(C6_VALOR) AS NUM_TOTAL\r\n";
		} else if(tipoVendasPorLinha != null && tipoVendasPorLinha.equals(TipoVendasPorLinha.CLIENTE_ANALITICO)) {
			sql += "		BM_DESC,\r\n" + 
					"	SUM(CASE WHEN ANO = '2017' THEN C6_VALOR ELSE 0 END) AS '2017',\r\n" + 
					"	SUM(CASE WHEN ANO = '2018' THEN C6_VALOR ELSE 0 END) AS '2018',\r\n" + 
					"	SUM(CASE WHEN ANO = '2019' THEN C6_VALOR ELSE 0 END) AS '2019',\r\n" + 
					"	SUM(CASE WHEN ANO = '2020' THEN C6_VALOR ELSE 0 END) AS '2020',\r\n" + 
					"	SUM(CASE WHEN ANO = '2021' THEN C6_VALOR ELSE 0 END) AS '2021',\r\n" + 
					"	SUM(C6_VALOR) AS TOTAL\r\n";
		} else if(tipoVendasPorLinha != null && tipoVendasPorLinha.equals(TipoVendasPorLinha.PORCLIENTE)) {
			sql += 	"		BM_DESC,\r\n" + 
					"		BM_GRUPO,\r\n" +
					"		A1_NOME,\r\n" + 
					"		A1_COD,\r\n" +
					"	SUM(CASE WHEN ANO = '2017' THEN C6_VALOR ELSE 0 END) AS '2017',\r\n" + 
					"	SUM(CASE WHEN ANO = '2018' THEN C6_VALOR ELSE 0 END) AS '2018',\r\n" + 
					"	SUM(CASE WHEN ANO = '2019' THEN C6_VALOR ELSE 0 END) AS '2019',\r\n" + 
					"	SUM(CASE WHEN ANO = '2020' THEN C6_VALOR ELSE 0 END) AS '2020',\r\n" + 
					"	SUM(CASE WHEN ANO = '2021' THEN C6_VALOR ELSE 0 END) AS '2021',\r\n" + 
					"	SUM(C6_VALOR) AS TOTAL\r\n";
		} else {
		sql +=	"	BM_DESC, \r\n" + 
				"	SUM(CASE WHEN MES = '1' THEN C6_VALOR ELSE 0 END) AS JANEIRO,\r\n" + 
				"	SUM(CASE WHEN MES = '2' THEN C6_VALOR ELSE 0 END) AS FEVEREIRO,\r\n" + 
				"	SUM(CASE WHEN MES = '3' THEN C6_VALOR ELSE 0 END) AS MARCO,\r\n" + 
				"	SUM(CASE WHEN MES = '4' THEN C6_VALOR ELSE 0 END) AS ABRIL,\r\n" + 
				"	SUM(CASE WHEN MES = '5' THEN C6_VALOR ELSE 0 END) AS MAIO,\r\n" + 
				"	SUM(CASE WHEN MES = '6' THEN C6_VALOR ELSE 0 END) AS JUNHO,\r\n" + 
				"	SUM(CASE WHEN MES = '7' THEN C6_VALOR ELSE 0 END) AS JULHO,\r\n" + 
				"	SUM(CASE WHEN MES = '8' THEN C6_VALOR ELSE 0 END) AS AGOSTO,\r\n" + 
				"	SUM(CASE WHEN MES = '9' THEN C6_VALOR ELSE 0 END) AS SETEMBRO,\r\n" + 
				"	SUM(CASE WHEN MES = '10' THEN C6_VALOR ELSE 0 END) AS OUTUBRO,\r\n" + 
				"	SUM(CASE WHEN MES = '11' THEN C6_VALOR ELSE 0 END) AS NOVEMBRO,\r\n" + 
				"	SUM(CASE WHEN MES = '12' THEN C6_VALOR ELSE 0 END) AS DEZEMBRO, \r\n" +
				"	SUM(C6_VALOR) AS TOTAL \r\n";
		}
		sql += 	"FROM (\r\n" + 
				"	SELECT\r\n" + 
				"		BM_GRUPO, C5_NUM, C5_EMISSAO, C6_PRODUTO, B1_DESC, C6_QTDVEN, C6_QTDENT, CASE WHEN C6_BLQ = 'R' THEN 0 ELSE C6_QTDVEN - C6_QTDENT END AS SALDO, C6_VALOR, BM_DESC, C6_BLQ, A1_EST, A1_NOME, A1_COD, YEAR(C5_EMISSAO) AS ANO, MONTH(C5_EMISSAO) AS MES, C5_CLASPED\r\n" + 
				"	FROM SC6010 WITH(NOLOCK)\r\n" + 
				"	INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM\r\n" + 
				"		AND C5_FILIAL <> 'ZZ'\r\n" + 
				"		AND SC5010.D_E_L_E_T_ = ''\r\n" + 
				"		AND C5_TIPO = 'N'\r\n" + 
//				"		AND C5_XREFAT IN ('', '2')\r\n" + 
				"		AND C5_CLIENTE <> '000422'\r\n" + 
				"		AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n" + 
				"	INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENTE\r\n" + 
				"		AND C5_LOJACLI = A1_LOJA\r\n" + 
				"		AND A1_FILIAL <> 'ZZ'\r\n" + 
				"	INNER JOIN SB1010 WITH(NOLOCK) ON B1_FILIAL <> 'ZZ' AND B1_COD = C6_PRODUTO AND SB1010.R_E_C_D_E_L_ = ''\r\n" + 
				"	LEFT JOIN SBM010 WITH(NOLOCK) ON BM_FILIAL <> 'ZZ' AND BM_GRUPO = B1_GRUPO AND SBM010.R_E_C_D_E_L_ = ''\r\n" + 
				"	WHERE\r\n" + 
				"		SC6010.D_E_L_E_T_ = ''\r\n" + 
				"		AND A1_EST <> 'EX'" +
				"		AND C6_FILIAL <> 'ZZ'\r\n" + 
				"		AND ((C6_TES LIKE '5%') OR (C6_TES IN ('644', '649', '701', '702', '703')) OR (C6_CF IN ('5116', '6116', '6101', '5101', '6107', '5107')))\r\n";
				if (cliente != null && !cliente.equals("")) {
					sql += "AND A1_COD = '" + cliente + "'\r\n";
				}
				if (tipoVendasPorLinha != null && tipoVendasPorLinha.equals(TipoVendasPorLinha.PORCLIENTE)) {
					sql += "		AND YEAR(C5_EMISSAO) >= 2017\r\n";
				} else if (dataDe != null && !dataDe.equals("")) {
					sql += "		AND C5_EMISSAO >= '" + dataDe + "'\r\n";
				} else {
					sql += "		AND YEAR(C5_EMISSAO) = YEAR(GETDATE())\r\n";
				}
				if (unidade != null && unidade.equals(UNIDADE.ORTOPEDIA)) {
					sql += "AND B1_LOCPAD IN ('01', '10')\r\n";
					if (tipoVendasPorLinha != null && !tipoVendasPorLinha.equals(TipoVendasPorLinha.METAS)) {
						sql += "AND C5_CLASPED IN ('1')\r\n";
					} else {
						sql += "AND C5_CLASPED IN ('1', '2', '4')\r\n";
					}
				} else if (unidade != null && unidade.equals(UNIDADE.EQUIPAMENTOS)) {
					sql += "AND B1_LOCPAD IN ('11', '12', '14')\r\n";
					sql += "AND C5_CLASPED IN ('3', '4', '6')\r\n";
				}
				sql += "" + 
				"UNION ALL\r\n" + 
				"	SELECT\r\n" + 
				"		BM_GRUPO, C6_NUM, D1_EMISSAO, D1_COD, B1_DESC, -D1_QUANT, -D1_QUANT, 0 AS SALDO, -D1_TOTAL, BM_DESC, C6_BLQ, A1_EST, A1_NOME, A1_COD, YEAR(D1_EMISSAO) AS ANO, MONTH(D1_EMISSAO) AS MES, C5_CLASPED\r\n" + 
				"	FROM SD1010 WITH (NOLOCK)\r\n" + 
				"	INNER JOIN SD2010 WITH (NOLOCK) ON D1_NFORI = D2_DOC AND SD2010.D_E_L_E_T_ = '' AND D1_ITEMORI = D2_ITEM AND D2_TIPO = 'N'\r\n" + 
				"	INNER JOIN SC6010 WITH (NOLOCK) ON SC6010.D_E_L_E_T_ = '' AND C6_FILIAL <> 'ZZ' AND D2_PEDIDO = C6_NUM AND D2_ITEMPV = C6_ITEM\r\n" + 
				"	INNER JOIN SC5010 WITH (NOLOCK) ON SC5010.D_E_L_E_T_ = '' AND C5_FILIAL <> 'ZZ' AND C5_NUM = C6_NUM\r\n" + 
				"	INNER JOIN SB1010 WITH(NOLOCK) ON D1_COD = B1_COD AND SB1010.D_E_L_E_T_ = '' AND B1_FILIAL <> 'ZZ'\r\n" + 
				"	INNER JOIN SA1010 WITH(NOLOCK) ON SA1010.D_E_L_E_T_ = '' AND A1_FILIAL <> 'ZZ' AND A1_COD = D2_CLIENTE AND A1_LOJA = D2_LOJA\r\n" + 
				"	LEFT JOIN SBM010 WITH(NOLOCK) ON BM_FILIAL <> 'ZZ' AND BM_GRUPO = B1_GRUPO AND SBM010.R_E_C_D_E_L_ = ''\r\n" + 
				"	WHERE\r\n" + 
				"		SD1010.D_E_L_E_T_ = ''\r\n";
				if (cliente != null && !cliente.equals("")) {
					sql += "AND A1_COD = '" + cliente + "'\r\n";
				}
				if (unidade != null && unidade.equals(UNIDADE.ORTOPEDIA)) {
					sql += "AND B1_LOCPAD IN ('01', '10')\r\n";
					if (tipoVendasPorLinha != null && !tipoVendasPorLinha.equals(TipoVendasPorLinha.METAS)) {
						sql += "AND C5_CLASPED IN ('1')\r\n";
					} else {
						sql += "AND C5_CLASPED IN ('1', '2', '4')\r\n";
					}
				} else {
					sql += "AND B1_LOCPAD IN ('11', '12', '14')\r\n";
					sql += "AND C5_CLASPED IN ('3', '4', '6')\r\n";
				}
				sql += "AND YEAR(D1_EMISSAO) = YEAR(GETDATE())\r\n" + 
					"		AND A1_EST <> 'EX'" + 
				"			AND D1_FILIAL <> 'ZZ'\r\n" + 
				"			AND D1_TIPO = 'D'\r\n";
				sql += ") AS RESULTSET\r\n";
				if (tipoVendasPorLinha != null && tipoVendasPorLinha.equals(TipoVendasPorLinha.METAS)) {
					sql += "GROUP BY CASE WHEN C5_CLASPED = '2' THEN 'CAIXAS' ELSE BM_GRUPO END, CASE WHEN C5_CLASPED = '2' THEN 'CAIXAS' ELSE BM_DESC END\r\n";
				} else if (tipoVendasPorLinha != null && tipoVendasPorLinha.equals(TipoVendasPorLinha.PORCLIENTE)) {
					sql += "GROUP BY BM_GRUPO, BM_DESC, A1_NOME, A1_COD\r\n";
				} else {
					sql += "GROUP BY BM_GRUPO, BM_DESC\r\n";
				}
				if (tipoVendasPorLinha != null && !tipoVendasPorLinha.equals(TipoVendasPorLinha.METAS)) {
					sql += "ORDER BY 1, 2";
				} else if (tipoVendasPorLinha != null && !tipoVendasPorLinha.equals(TipoVendasPorLinha.METAS)) {
					sql += "ORDER BY 13 DESC";
				}
		
		Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
		Collection<Object> headers = new LinkedList<Object>();
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				headers.add(rs.getMetaData().getColumnName(i));
			}
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				if (tipoVendasPorLinha != null && tipoVendasPorLinha.equals(TipoVendasPorLinha.METAS)) {
					for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
						if (rs.getMetaData().getColumnName(i).startsWith("NUM_")) {
							linha.add(rs.getBigDecimal(i).setScale(2, RoundingMode.HALF_EVEN));
						} else {
							linha.add(rs.getString(i));
						}
					}
				} else if(tipoVendasPorLinha != null && !tipoVendasPorLinha.equals(TipoVendasPorLinha.METAS)) {
					String bmDesc = rs.getString("BM_DESC");
					linha.add(bmDesc);
					linha.add(rs.getString("A1_NOME"));
					
					String codigoCliente = rs.getString("A1_COD");
					
					
					String link = "/pedidos-venda/relacao-vendas?dataDe=20170101&&dataAte=20171231&codigoGrupoProduto=" + rs.getString("BM_GRUPO");
					if (codigoCliente != null && !codigoCliente.equals("")) {
						link += "&codigoCliente=" + codigoCliente;
					}
					BigDecimal valor = rs.getBigDecimal("2017").setScale(2, RoundingMode.HALF_EVEN);
					linha.add(HtmlUtils.createLink(link, CurrencyUtils.format(valor)));
					
					
					link = "/pedidos-venda/relacao-vendas?dataDe=20180101&&dataAte=20181231&codigoGrupoProduto=" + rs.getString("BM_GRUPO");
					if (codigoCliente != null && !codigoCliente.equals("")) {
						link += "&codigoCliente=" + codigoCliente;
					}
					valor = rs.getBigDecimal("2018").setScale(2, RoundingMode.HALF_EVEN);
					linha.add(HtmlUtils.createLink(link, CurrencyUtils.format(valor)));
					
					
					link = "/pedidos-venda/relacao-vendas?dataDe=20190101&&dataAte=20191231&codigoGrupoProduto=" + rs.getString("BM_GRUPO");
					if (codigoCliente != null && !codigoCliente.equals("")) {
						link += "&codigoCliente=" + codigoCliente;
					}
					valor = rs.getBigDecimal("2019").setScale(2, RoundingMode.HALF_EVEN);
					linha.add(HtmlUtils.createLink(link, CurrencyUtils.format(valor)));
					
					
					link = "/pedidos-venda/relacao-vendas?dataDe=20200101&&dataAte=20201231&codigoGrupoProduto=" + rs.getString("BM_GRUPO");
					if (codigoCliente != null && !codigoCliente.equals("")) {
						link += "&codigoCliente=" + codigoCliente;
					}
					valor = rs.getBigDecimal("2020").setScale(2, RoundingMode.HALF_EVEN);
					linha.add(HtmlUtils.createLink(link, CurrencyUtils.format(valor)));
					
					
					link = "/pedidos-venda/relacao-vendas?dataDe=20210101&&dataAte=20211231&codigoGrupoProduto=" + rs.getString("BM_GRUPO");
					if (codigoCliente != null && !codigoCliente.equals("")) {
						link += "&codigoCliente=" + codigoCliente;
					}
					valor = rs.getBigDecimal("2021").setScale(2, RoundingMode.HALF_EVEN);
					linha.add(HtmlUtils.createLink(link, CurrencyUtils.format(valor)));
					
					
					linha.add(CurrencyUtils.format(rs.getBigDecimal("TOTAL").setScale(2, RoundingMode.HALF_EVEN)));
				} else {
					String bmDesc = rs.getString("BM_DESC");
					linha.add(bmDesc);
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20210101&dataAte=20210131&unidade=ORTOPEDIA&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("JANEIRO").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20210201&dataAte=20210231&unidade=ORTOPEDIA&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("FEVEREIRO").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20210301&dataAte=20210331&unidade=ORTOPEDIA&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("MARCO").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20210401&dataAte=20210431&unidade=ORTOPEDIA&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("ABRIL").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20210501&dataAte=20210531&unidade=ORTOPEDIA&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("MAIO").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20210601&dataAte=20210631&unidade=ORTOPEDIA&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("JUNHO").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20210701&dataAte=20210731&unidade=ORTOPEDIA&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("JULHO").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20210801&dataAte=20210831&unidade=ORTOPEDIA&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("AGOSTO").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20210901&dataAte=20210931&unidade=ORTOPEDIA&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("SETEMBRO").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20211001&dataAte=20211031&unidade=ORTOPEDIA&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("OUTUBRO").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20211101&dataAte=20211131&unidade=ORTOPEDIA&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("NOVEMBRO").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(HtmlUtils.createLink("/vendas?dataDe=20211201&dataAte=20211231&unidade=ORTOPEDIA&linhaProdutos=" + bmDesc, CurrencyUtils.format(rs.getBigDecimal("DEZEMBRO").setScale(2, RoundingMode.HALF_EVEN))));
					linha.add(CurrencyUtils.format(rs.getBigDecimal("TOTAL").setScale(2, RoundingMode.HALF_EVEN)));
				}
				data.add(linha);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new DataTable(data).setHeaders(headers);
	}

	public static ChartDataAndLabels getQtdOrcamentos(UNIDADE unidade, StatusOrcamento statusOrcamento) {
		 DataTable dataTable = getOrcamentos(unidade, TipoOrcamento.INDICADOR, TipoOrcamento.QUANTIDADE, statusOrcamento, null, null, null, null);
		 ChartDataAndLabels cdl = new ChartDataAndLabels();
		 Iterator<Collection<Object>> iter = dataTable.getData().iterator();
		 while (iter.hasNext()) {
			 LinkedList<Object> col = (LinkedList<Object>) iter.next();
			 cdl.getChartLabels().add((String) col.get(0));
			 cdl.getChartData().add((BigDecimal) col.get(1));
		 }
		 return cdl;
	}
	
	public static ChartDataAndLabels getValorOrcamentos(UNIDADE unidade, StatusOrcamento statusOrcamento) {
		 DataTable dataTable = getOrcamentos(unidade, TipoOrcamento.INDICADOR, TipoOrcamento.VALOR, statusOrcamento, null, null, null, null);
		 ChartDataAndLabels cdl = new ChartDataAndLabels();
		 Iterator<Collection<Object>> iter = dataTable.getData().iterator();
		 while (iter.hasNext()) {
			 LinkedList<Object> col = (LinkedList<Object>) iter.next();
			 cdl.getChartLabels().add((String) col.get(0));
			 cdl.getChartData().add((BigDecimal) col.get(1));
		 }
		 return cdl;
	}


	private static DataTable getOrcamentos(UNIDADE unidade, TipoOrcamento tipoOrcamento, TipoOrcamento somarPor, StatusOrcamento status, String mes, String ano, String codCliente, String numOrcamento) {
		String sql = "SELECT\r\n"; 
		if (tipoOrcamento.equals(TipoOrcamento.INDICADOR)) {
			if (somarPor.equals(TipoOrcamento.VALOR)) {
				sql += 	"DAY(CJ_EMISSAO) AS DIA, SUM(CK_VALOR) AS NUM_VALOR\r\n";
			} else if (somarPor.equals(TipoOrcamento.QUANTIDADE)) {
				sql += 	"DAY(CJ_EMISSAO) AS DIA, SUM(CK_QTDVEN) AS NUM_QUANT\r\n";
			}
		} else if (tipoOrcamento.equals(TipoOrcamento.DASHBOARD)) {
			sql +=  "	CASE MONTH(CJ_EMISSAO) WHEN '01' THEN 'JANEIRO' WHEN '02' THEN 'FEVEREIRO' WHEN '03' THEN 'MARCO' WHEN '04' THEN 'ABRIL' WHEN '05' THEN 'MAIO' WHEN '06' THEN 'JUNHO' WHEN '07' THEN 'JULHO'\r\n" + 
					"	WHEN '08' THEN 'AGOSTO' WHEN '09' THEN 'SETEMRBO' WHEN '10' THEN 'OUTUBRO' WHEN '11' THEN 'NOVEMBRO' WHEN '12' THEN 'DEZEMBRO' END AS MES,\r\n" + 
					"	SUM(CK_VALOR) AS NUM_RECEBIDOS,\r\n" + 
					"	ISNULL(SUM(CASE WHEN CJ_STATUS = 'A' THEN CK_VALOR END), 0) AS NUM_ABERTOS,\r\n" + 
					"	ISNULL(SUM(CASE WHEN CJ_STATUS = 'B' THEN CK_VALOR END), 0) AS NUM_APROVADOS,\r\n" + 
					"	ISNULL(SUM(CASE WHEN CJ_STATUS = 'C' THEN CK_VALOR END), 0) AS NUM_CANCELADOS\r\n";
		} else if (tipoOrcamento.equals(TipoOrcamento.SINTETICO)) {
			sql +=  "	A1_COD,\r\n" +
					"	A1_NOME,\r\n" +
					"	SUM(CASE WHEN CJ_STATUS = 'A' THEN CK_VALOR ELSE 0 END) AS NUM_ABERTOS,\r\n" + 
					"	SUM(CASE WHEN CJ_STATUS = 'B' THEN CK_VALOR ELSE 0 END) AS NUM_APROVADOS,\r\n" + 
					"	SUM(CASE WHEN CJ_STATUS = 'C' THEN CK_VALOR ELSE 0 END) AS NUM_CANCELADOS,\r\n" +
					"	SUM(CK_VALOR) AS NUM_RECEBIDOS\r\n";
		} else if (tipoOrcamento.equals(TipoOrcamento.PORORCAMENTO)) {
			sql += "	A1_COD AS COD_CLIENTE, A1_NOME AS NOME_CLIENTE, MAX(CASE CJ_STATUS WHEN 'A' THEN 'ABERTO' WHEN 'B' THEN 'APROVADO' WHEN 'C' THEN 'CANCELADO' END) AS STATUS, CJ_NUM AS 'N.ORCAMENTO', MAX(CJ_EMISSAO) AS EMISSAO, SUM(CK_VALOR) AS NUM_VALOR\r\n";
		} else if (tipoOrcamento.equals(TipoOrcamento.DETALHES)) {
			sql += "A1_COD, A1_NOME, CJ_NUM, CK_PRODUTO, B1_DESC, CK_QTDVEN, CK_PRCVEN, CK_VALOR\r\n";
		} else {
			sql += 	"CK_NUM, CK_ITEM, CK_PRODUTO, CK_DESCRI, CK_QTDVEN, CK_PRCVEN, CK_VALOR, CK_CLIENTE, A1_NOME, CJ_STATUS, CJ_EMISSAO, YEAR(CJ_EMISSAO) AS ANO, MONTH(CJ_EMISSAO) AS MES, DAY(CJ_EMISSAO) AS DIA\r\n"; 
		}
		sql += 	"FROM SCK010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SCJ010 WITH(NOLOCK) ON CK_NUM = CJ_NUM AND SCJ010.D_E_L_E_T_ = '' AND CJ_FILIAL <> 'ZZ'\r\n" + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = CK_CLIENTE AND A1_LOJA = CK_LOJA AND SA1010.D_E_L_E_T_ = '' AND A1_FILIAL <> 'ZZ'\r\n" +
				"INNER JOIN SB1010 WITH(NOLOCK) ON CK_PRODUTO = B1_COD AND SB1010.D_E_L_E_T_ = '' AND B1_FILIAL <> 'ZZ'\r\n" +
				"WHERE\r\n" + 
				"SCK010.D_E_L_E_T_ = ''\r\n";
		if (unidade != null) {
			if (unidade.equals(UNIDADE.ORTOPEDIA)) {
				sql += "AND CK_LOCAL = '01'\r\n";
			} else {
				sql += "AND CK_LOCAL = '12'\r\n";
			}
		}
		if (tipoOrcamento.equals(TipoOrcamento.INDICADOR)) {
			sql += "AND YEAR(CJ_EMISSAO) = YEAR(GETDATE())\r\n";
			sql += "AND MONTH(CJ_EMISSAO) = MONTH(GETDATE())\r\n";
		}
		sql += 	"AND CK_FILIAL <> 'ZZ'\r\n";
		if (status != null && status.equals(StatusOrcamento.ABERTOS)) {
			sql += "AND CJ_STATUS = 'A'\r\n";
		} else if (status != null && status.equals(StatusOrcamento.APROVADOS)) {
			sql += "AND CJ_STATUS = 'B'\r\n";
		} else if (status != null && status.equals(StatusOrcamento.CANCELADOS)) {
			sql += "AND CJ_STATUS = 'C'\r\n";
		}
		if (mes != null && !mes.equals("")) {
			sql += "AND MONTH(CJ_EMISSAO) = " + mes + "\r\n";
		}
		if (ano != null && !ano.equals("")) {
			sql += "AND YEAR(CJ_EMISSAO) = " + ano + "\r\n";
		}
		if (codCliente != null && !codCliente.equals("")) {
			sql += "AND A1_COD = '" + codCliente + "'\r\n";
		}
		if (numOrcamento != null && !numOrcamento.equals("")) {
			sql += "AND CJ_NUM = '" + numOrcamento + "'\r\n";
		}
		if (tipoOrcamento.equals(TipoOrcamento.INDICADOR)) {
			sql += 	"GROUP BY DAY(CJ_EMISSAO)\r\n" + 
					"ORDER BY 1";
		} else if (tipoOrcamento.equals(TipoOrcamento.DASHBOARD)) {
			sql += 	"	AND YEAR(CJ_EMISSAO) = YEAR(GETDATE())\r\n" +
					"	GROUP BY MONTH(CJ_EMISSAO)\r\n" + 
					"	ORDER BY MONTH(CJ_EMISSAO)";
		} else if (tipoOrcamento.equals(TipoOrcamento.SINTETICO)) {
			sql += 	"GROUP BY A1_COD, A1_NOME\r\n" +
					"ORDER BY 6";
		} else if (tipoOrcamento.equals(TipoOrcamento.PORORCAMENTO)) {
			sql += 	"GROUP BY A1_COD, A1_NOME, CJ_NUM\r\n";
		}
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<Object> headers = new LinkedList<Object>();
			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				headers.add(rs.getMetaData().getColumnName(i));
			}
			
			
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					if (rs.getMetaData().getColumnName(i).equals("N.ORCAMENTO")) {
						linha.add(HtmlUtils.createLink("/orcamentos/" + rs.getString(i) + "/detalhes", rs.getString(i)));
						continue;
					}
					if (rs.getMetaData().getColumnName(i).startsWith("NUM_")) {
						linha.add(rs.getBigDecimal(i).setScale(2, RoundingMode.HALF_EVEN));
					} else {
						linha.add(rs.getString(i));
					}
				}
				data.add(linha);
			}
			return new DataTable(data, headers);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}


	public static DataTable getRelacaoOrcamentos(String dia, String mes, String ano, UNIDADE unidade) {
		String sql = "SELECT\r\n" + 
				"CJ_NUM, CJ_EMISSAO, A1_NOME, CK_PRODUTO, B1_DESC, CK_QTDVEN, CK_PRCVEN, CK_VALOR\r\n" + 
				"FROM SCK010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SCJ010 WITH(NOLOCK) ON CJ_FILIAL <> 'ZZ' AND CJ_CLIENTE <> '000422' AND CJ_LOJA <> 'ZZ' AND YEAR(CJ_EMISSAO) = YEAR(GETDATE()) AND MONTH(CJ_EMISSAO) = MONTH(GETDATE()) AND SCJ010.R_E_C_N_O_ <> 0 AND SCJ010.D_E_L_E_T_ = ''\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON CK_PRODUTO = B1_COD AND SB1010.D_E_L_E_T_ = '' AND B1_FILIAL <> 'ZZ'\r\n" + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON CJ_CLIENTE = A1_COD AND CJ_LOJA = A1_LOJA AND SA1010.D_E_L_E_T_ = '' AND A1_FILIAL <> 'ZZ'\r\n" + 
				"WHERE\r\n" + 
				"CK_FILIAL <> 'ZZ' AND CK_NUM = CJ_NUM AND CK_ITEM <> 'ZZ' AND CK_PRODUTO <> 'ZZ' AND SCK010.R_E_C_D_E_L_ = ''\r\n"; 
				sql += (unidade.equals(UNIDADE.ORTOPEDIA)) ? "AND CK_LOCAL IN ('01', '10')\r\n" : (unidade.equals(UNIDADE.EQUIPAMENTOS)) ? "AND CK_LOCAL IN ('11', '12', '14')\r\n" : ""; 
				sql += (dia != null && !dia.equals("")) ? "AND DAY(CJ_EMISSAO) = " + dia + "\r\n" : "";
				sql += (mes != null && !mes.equals("")) ? "AND MONTH(CJ_EMISSAO) = " + mes + "\r\n" : "";
				sql += (ano != null && !ano.equals("")) ? "AND YEAR(CJ_EMISSAO) = " + ano + "\r\n" : "";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					if (rs.getMetaData().getColumnName(i).startsWith("NUM_")) {
						linha.add(rs.getBigDecimal(i).setScale(2, RoundingMode.HALF_EVEN));
					} else {
						linha.add(rs.getString(i));
					}
				}
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}


	public static DataTable getVendasPorLinhaMeta(UNIDADE unidade) {
		return getVendasPorLinha(unidade, TipoVendasPorLinha.METAS, null, null);
	}


	public static DataTable getPedidosFaturamento(Iterable<PedidoFaturamento> pfs) {
		if (!pfs.iterator().hasNext()) {
			return new DataTable();
		}
		
		String sql = "SELECT\r\n" + 
				"C6_NUM, C6_ITEM, A1_NOME, C6_XLIBPED, RTRIM(C6_PRODUTO) AS C6_PRODUTO, RTRIM(B1_DESC) AS B1_DESC, '' AS DT_ATU, '' AS LOCALIZ, '' AS STATUS, C6_QTDVEN, " +
				"ISNULL(C9_QTDLIB, 0) AS QTDEMP,\r\n" +
				"C6_QTDENT, '' AS DT_EXP\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON SB1010.D_E_L_E_T_ = '' AND B1_FILIAL <> 'ZZ' AND B1_COD = C6_PRODUTO\r\n" +
				"INNER JOIN SA1010 WITH(NOLOCK) ON SA1010.D_E_L_E_T_ = '' AND A1_FILIAL <> 'ZZ' AND A1_COD = C6_CLI AND A1_LOJA = C6_LOJA\r\n" +
				"LEFT JOIN SC9010 WITH(NOLOCK) ON C9_PEDIDO = C6_NUM AND C9_ITEM = C6_ITEM AND C9_LOTECTL <> '' AND SC9010.D_E_L_E_T_ = '' AND C9_FILIAL <> '' AND C9_NFISCAL = ''\r\n" +
				"WHERE\r\n" + 
				"SC6010.D_E_L_E_T_ = ''\r\n" + 
				"AND C6_FILIAL <> 'ZZ'\r\n" + 
				"AND C6_NUM + C6_ITEM IN (\r\n";
		Iterator<PedidoFaturamento> iter = pfs.iterator();
		while (iter.hasNext()) {
			PedidoFaturamento pf = iter.next();
			sql += "'" + pf.getNumeroPedido() + pf.getItemPedido() + "'";
			if (iter.hasNext()) {
				sql += ",\r\n";
			} else {
				sql += ")";
			}
		}
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					linha.add(rs.getString(i));
				}
				
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return new DataTable();
	}


	public static List<DataTable> getVendasLinha(String codigoCliente) {
		DataTable tableGeral = getVendas(ParametroVenda.VENDASCLIENTEXTIPO, null, null, null, null, codigoCliente, null, null, null, null, null, null, null, null);

		HashSet<String> linhas = new HashSet<String>();
		LinkedList<Collection<Object>> data = (LinkedList<Collection<Object>>) tableGeral.getData();
		for (int i = 0; i < tableGeral.getData().size(); i++) {
			LinkedList<Object> collection = (LinkedList<Object>) data.get(i);
			linhas.add((String) collection.get(0));
		}
		
		
		//TODO FAZER COM QUE O LOOPING FAÇA COMECE EM JAN DE 2 ANOS ATRÁS
//		int mesAtual = Calendar.getInstance().get(Calendar.MONTH) + 1;
//		for (String linha : linhas) {
//			for (int i = 0; i < mesAtual + 24; i++) {
//				
//			}
//		}
		
		
		
		return null;
	}


	public static ChartDataAndLabels getDashBoardOrcamentos(UNIDADE unidade) {
		DataTable dataTable = getOrcamentos(unidade, TipoOrcamento.DASHBOARD, TipoOrcamento.VALOR, null, null, null, null, null);
		Iterator<Collection<Object>> iter = dataTable.getData().iterator();
		
		BarDataset datasetRecebidos = new BarDataset();
		datasetRecebidos.setLabel("RECEBIDOS");
		datasetRecebidos.setBackgroundColor(new Color(33,  93, 213, 1));
		datasetRecebidos.setBorderColor(Color.BLACK);
		datasetRecebidos.setBorderWidth(1);
		
		BarDataset datasetAprovados = new BarDataset();
		datasetAprovados.setLabel("APROVADOS");
		datasetAprovados.setBackgroundColor(new Color(172, 229, 172, 1));
		datasetAprovados.setBorderColor(Color.BLACK);
		datasetAprovados.setBorderWidth(1);
		
		BarDataset datasetAbertos = new BarDataset();
		datasetAbertos.setLabel("ABERTOS");
		datasetAbertos.setBackgroundColor(new Color(239, 230, 153, 1));
		datasetAbertos.setBorderColor(Color.BLACK);
		datasetAbertos.setBorderWidth(1);
		
		BarDataset datasetCancelados = new BarDataset();
		datasetCancelados.setLabel("CANCELADOS");
		datasetCancelados.setBackgroundColor(new Color(196, 132, 144, 1));
		datasetCancelados.setBorderColor(Color.BLACK);
		datasetCancelados.setBorderWidth(1);
		
		ChartDataAndLabels cdl = new ChartDataAndLabels();
		while (iter.hasNext()) {
			LinkedList<Object> col = (LinkedList<Object>) iter.next();
			cdl.getChartLabels().add((String) col.get(0));
			datasetRecebidos.addData((BigDecimal) col.get(1));
			datasetAbertos.addData((BigDecimal) col.get(2));
			datasetAprovados.addData((BigDecimal) col.get(3));
			datasetCancelados.addData((BigDecimal) col.get(4));
		}
		
		Collection<BarDataset> datasets = new LinkedList<BarDataset>();
		datasets.add(datasetRecebidos);
		datasets.add(datasetAprovados);
		datasets.add(datasetAbertos);
		datasets.add(datasetCancelados);
		cdl.setDatasets(datasets);
		return cdl;
	}


	public static DataTable getOrcamentosSintetico(String mes, String ano, TipoOrcamento tipoOrcamento,
			UNIDADE unidade) {
		return getOrcamentos(unidade, tipoOrcamento, TipoOrcamento.VALOR, null, mes, ano, null, null);
	}


	public static DataTable getOrcamentosAnalitico(String codCliente, StatusOrcamento statusOrcamento) {
		return getOrcamentos(null, TipoOrcamento.PORORCAMENTO, TipoOrcamento.VALOR, statusOrcamento, null, null, codCliente, null);
	}


	public static DataTable getOrcamentoDetalhes(String numOrcamento) {
		return getOrcamentos(null, TipoOrcamento.DETALHES, null, null, null, null, null, numOrcamento);
	}


	public static void setConclusaoPrograma(ProgramaVendas programaVendas) {
		DataTable dataTable = getVendas(ParametroVenda.PROGRAMAVENDAS, CLASPED.REPOSICAO, null, null, null, programaVendas.getCliente(), null, programaVendas.getDataInicio(), programaVendas.getDataLimite(), null, programaVendas.getProdutos(), null, null, null);
		LinkedList<Collection<Object>> data = (LinkedList<Collection<Object>>) dataTable.getData();
		LinkedList<Object> linha = (LinkedList<Object>) data.get(0);
		BigDecimal qtdVendas = (BigDecimal) linha.get(0);
		
		programaVendas.setQuantidadeVendida(qtdVendas);
	}


	public static List<CardVenda> getVendasPorLinhaPorCliente(String cliente) {
		Calendar calendar = Calendar.getInstance();
		
		String dia = "" + calendar.get(Calendar.DAY_OF_MONTH);
		String mes = "" + (calendar.get(Calendar.MONTH) + 1);
		if (mes.length() == 1) {
			mes = "0" + mes;
		}
		String ano = "" + (calendar.get(Calendar.YEAR) - 1);
		String dataDe = ano + mes + dia;
		
		
		
		DataTable dataTable = getVendasPorLinha(null, TipoVendasPorLinha.METAS, cliente, dataDe);
		List<CardVenda> cardsVendas = new LinkedList<CardVenda>();
		LinkedList<Object> tempCollection = new LinkedList<Object>(dataTable.getData());
		for (int i = 0; i < tempCollection.size(); i++) {
			@SuppressWarnings("unchecked")
			LinkedList<Object> row = (LinkedList<Object>) tempCollection.get(i);
			CardVenda cardVenda = new CardVenda();
			cardVenda.setCodigoGrupoProduto((String) row.get(0));
			cardVenda.setSeguimento((String) row.get(1));
			cardVenda.setValor((BigDecimal) row.get(2));
			cardsVendas.add(cardVenda);
		}
		return cardsVendas;
	}


	public static DataTable getVendasCards(UNIDADE unidade) {
		String sql = "SELECT C6_PRODUTO, B1_DESC, YEAR(C5_EMISSAO) AS ANO, SUM(C6_QTDVEN) AS NUM_QTDVEN FROM (\r\n" + 
				"SELECT\r\n" + 
				"	'VENDAS' AS TIPO, B1_DESC, BM_DESC AS GRUPO, C6_NUM, C6_ITEM, A1_NREDUZ, C5_EMISSAO, C6_ENTREG, C6_PRODUTO, C6_DESCRI, C6_QTDVEN, C6_QTDENT, CASE WHEN C5_MOEDA = '4' THEN  C6_PRCVEN * 6.5 WHEN A1_EST = 'EX' THEN C6_PRCVEN * 5.6 ELSE C6_PRCVEN END AS C6_PRCVEN, CASE WHEN C5_MOEDA = '4' THEN C6_VALOR * 6.5 WHEN A1_EST = 'EX' THEN C6_VALOR * 5.6 ELSE C6_VALOR END AS C6_VALOR, C5_CLASPED, A1_NOME, A1_COD, A1_EST\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM AND C5_FILIAL <> 'ZZ' AND SC5010.D_E_L_E_T_ = '' AND C5_TIPO = 'N' AND C5_CLIENTE <> '000422'\r\n" + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON A1_COD = C5_CLIENTE AND A1_LOJA = C5_LOJACLI AND A1_FILIAL <> 'ZZ'\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON SB1010.D_E_L_E_T_ = '' AND B1_FILIAL <> 'ZZ' AND B1_COD = C6_PRODUTO\r\n" + 
				"INNER JOIN SBM010 WITH(NOLOCK) ON SBM010.D_E_L_E_T_ = '' AND BM_FILIAL <> 'ZZ' AND B1_GRUPO = BM_GRUPO\r\n" + 
				"WHERE\r\n" + 
				"C6_BLQ <> 'R'\r\n" + 
				"AND C5_LIBEROK = 'S'\r\n" + 
				"AND ((C6_TES LIKE '5%') OR (C6_TES IN ('644', '649', '701', '702', '703')) OR (C6_CF IN ('5116', '6116', '6101', '5101', '6107', '5107')))\r\n" + 
				"AND C5_CLASPED IN ('3', '6')\r\n" + 
				"AND YEAR(C5_EMISSAO) >= 2016\r\n" + 
				"AND SC6010.D_E_L_E_T_ = ''\r\n" + 
				"UNION ALL\r\n" + 
				"SELECT\r\n" + 
				"	'DEVOLUCAO' AS TIPO, B1_DESC, BM_DESC AS GRUPO, C6_NUM, C6_ITEM, A1_NREDUZ, D1_EMISSAO, C6_ENTREG, C6_PRODUTO, C6_DESCRI, -D1_QUANT, -D1_QUANT, -D1_VUNIT, -D1_TOTAL, C5_CLASPED, A1_NOME, A1_COD, A1_EST\r\n" + 
				"FROM SD1010 WITH (NOLOCK)\r\n" + 
				"INNER JOIN SD2010 WITH (NOLOCK) ON D1_NFORI = D2_DOC AND SD2010.D_E_L_E_T_ = '' AND D1_ITEMORI = D2_ITEM AND D2_TIPO = 'N'\r\n" + 
				"INNER JOIN SC6010 WITH (NOLOCK) ON SC6010.D_E_L_E_T_ = '' AND C6_FILIAL <> 'ZZ' AND D2_PEDIDO = C6_NUM AND D2_ITEMPV = C6_ITEM\r\n" + 
				"INNER JOIN SC5010 WITH (NOLOCK) ON SC5010.D_E_L_E_T_ = '' AND C5_FILIAL <> 'ZZ' AND C5_NUM = C6_NUM\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON D1_COD = B1_COD AND SB1010.D_E_L_E_T_ = '' AND B1_FILIAL <> 'ZZ'\r\n" + 
				"INNER JOIN SA1010 WITH(NOLOCK) ON SA1010.D_E_L_E_T_ = '' AND A1_FILIAL <> 'ZZ' AND A1_COD = D2_CLIENTE AND A1_LOJA = D2_LOJA\r\n" + 
				"INNER JOIN SBM010 WITH (NOLOCK) ON SBM010.D_E_L_E_T_ = '' AND BM_FILIAL <> 'ZZ' AND B1_GRUPO = BM_GRUPO\r\n" + 
				"WHERE\r\n" + 
				"SD1010.D_E_L_E_T_ = ''\r\n" + 
				"AND C5_CLASPED IN ('3', '6')\r\n" + 
				"AND YEAR(D1_EMISSAO) >= 2016\r\n" + 
				"AND D1_FILIAL <> 'ZZ'\r\n" + 
				"AND D1_TIPO = 'D'\r\n" + 
				") AS RESULTSET\r\n" + 
				"WHERE\r\n" + 
				"	(C6_PRODUTO LIKE 'AC%' OR C6_PRODUTO LIKE 'TD%' OR C6_PRODUTO LIKE 'ALR%' OR C6_PRODUTO LIKE 'LVU%' OR C6_PRODUTO LIKE 'FL%' OR C6_PRODUTO LIKE 'MC%' OR C6_PRODUTO LIKE 'GS%')\r\n" + 
				"	AND (B1_DESC LIKE 'AUTOCLAVE%' OR B1_DESC LIKE 'TERMODESINFECTORA%' OR B1_DESC LIKE 'LAV%' OR B1_DESC LIKE 'FOCO%' OR B1_DESC LIKE 'MESA%' OR B1_DESC LIKE 'GABINETE%')\r\n" + 
				"GROUP BY C6_PRODUTO, YEAR(C5_EMISSAO), B1_DESC";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			DataTable dataTable = new DataTable(data);
			
			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				dataTable.getHeaders().add(rs.getMetaData().getColumnName(i));
			}
			
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1;  i <= rs.getMetaData().getColumnCount(); i++) {
					if (rs.getMetaData().getColumnName(i).startsWith("NUM_")) {
						linha.add(rs.getInt(i));
					} else {
						linha.add(rs.getString(i));
					}
				}
				data.add(linha);
			}
			
			return dataTable;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}


	public static DataTable getTabelaPrecosCliente(String codigoCliente) {
		String sql = "DECLARE @CLIENTE AS VARCHAR(255)\r\n" + 
				"SET @CLIENTE = '" + codigoCliente + "'\r\n" + 
				"SELECT A1_NREDUZ, D2_COD, B1_DESC, DA1_PRCVEN, A1_TABELA FROM (\r\n" + 
				"	SELECT DISTINCT D2_COD FROM SD2010 WITH(NOLOCK) WHERE D2_CLIENTE = @CLIENTE AND SD2010.D_E_L_E_T_ = '' AND D2_TIPO = 'N' AND D2_EMISSAO > '20190101' AND D2_FILIAL <> 'ZZ'\r\n" + 
				") AS RESULTSET\r\n" + 
				"JOIN SB1010 WITH(NOLOCK) ON B1_COD = D2_COD AND SB1010.D_E_L_E_T_ = '' AND B1_FILIAL <> 'ZZ'\r\n" + 
				"JOIN SA1010 WITH(NOLOCK) ON SA1010.D_E_L_E_T_ = '' AND A1_COD = @CLIENTE\r\n" + 
				"JOIN DA1010 WITH(NOLOCK) ON DA1010.D_E_L_E_T_ = '' AND DA1_CODTAB = A1_TABELA AND DA1_CODPRO = B1_COD";
	try(PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
		ResultSet rs = stmt.executeQuery();
		Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
		while (rs.next()) {
			Collection<Object> linha = new LinkedList<Object>();
			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				linha.add(rs.getString(i));
			}
			data.add(linha);
		}
		return new DataTable().setData(data);
	} catch (SQLException e) {
		e.printStackTrace();
	}
	return null;
}

}
