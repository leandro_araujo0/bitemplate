package br.com.BiTemplate.DAO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.model.DetalhesOP;
import br.com.BiTemplate.model.OrdemProducao;
import br.com.BiTemplate.model.PedidoCompra;
import br.com.BiTemplate.model.Processo;
import br.com.BiTemplate.model.Produto;
import br.com.BiTemplate.utils.HtmlUtils;

public class OrdemProducaoDAO {
	
	public static Iterable<OrdemProducao> findByProduto(Produto produto) {
		String sql = "SELECT\r\n" + 
				"	SC2010.C2_NUM,\r\n" + 
				"	SC2010.C2_XUSUARI,\r\n" + 
				"	SC2010.C2_LOCAL,\r\n" + 
				"	RTRIM(C2_PRODUTO) AS C2_PRODUTO,\r\n" + 
				"	B1_DESC,\r\n" + 
				"	SC2010.C2_QUANT,\r\n" + 
				"	SC2010.C2_QUJE,\r\n" + 
				"	SC2010.C2_PERDA,\r\n" + 
				"	SC2010.C2_EMISSAO,\r\n" + 
				"	SC2010.C2_DATPRF,\r\n" + 
				"	SC2010.C2_OBS,\r\n" + 
				"	(SELECT TOP 1 H1_DESCRI FROM SH6010 WITH(NOLOCK) INNER JOIN SH1010 WITH(NOLOCK) ON H1_FILIAL = '01' AND H6_RECURSO = H1_CODIGO WHERE H6_FILIAL = '01' AND SUBSTRING(H6_OP, 1, 6) = C2_NUM ORDER BY H6_DATAFIN DESC) AS ULT_APT,\r\n" + 
				"	(SELECT TOP 1 H6_DATAFIN FROM SH6010 WITH(NOLOCK) WHERE H6_FILIAL = '01' AND SUBSTRING(H6_OP, 1, 6) = C2_NUM ORDER BY H6_DATAFIN DESC) AS DATA_ULT_APT\r\n" + 
				"FROM SC2010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON B1_COD = C2_PRODUTO\r\n" + 
				"WHERE SC2010.D_E_L_E_T_ = ''\r\n" + 
				"AND SC2010.C2_FILIAL = '01'\r\n" + 
				"AND SC2010.C2_PRODUTO = ?\r\n" + 
				"AND SC2010.C2_DATRF = ''\r\n" + 
				"ORDER BY C2_NUM, C2_ITEM, C2_SEQUEN";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setString(1, produto.getCodigo());
			ResultSet rs = stmt.executeQuery();
			LinkedList<OrdemProducao> ops = new LinkedList<>();
			while (rs.next()) {
				OrdemProducao op = new OrdemProducao();
				op.setNumero(rs.getString("C2_NUM"));
				op.setUsuario(rs.getString("C2_XUSUARI"));
				op.setLocal(rs.getString("C2_LOCAL"));
				op.setProduto(new Produto().setCodigo(rs.getString("C2_PRODUTO")).setDescricao(rs.getString("B1_DESC")));
				op.setQuant(rs.getBigDecimal("C2_QUANT"));
				op.setQuje(rs.getBigDecimal("C2_QUJE"));
				op.setPerda(rs.getBigDecimal("C2_PERDA"));
				op.setEmissao(rs.getString("C2_EMISSAO"));
				op.setDatprf(rs.getString("C2_DATPRF"));
				op.setObs(rs.getString("C2_OBS"));
				op.setUltApto(rs.getString("ULT_APT"));
				op.setDatUltApto(rs.getString("DATA_ULT_APT"));
				ops.add(op);
			}
			return ops;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return findByProduto(produto);
			} else {
				e.printStackTrace();
			}
		}
		
		
		
		return null;
	}

	public static Iterable<OrdemProducao> findAll() {
		String sql = "SELECT\r\n" + 
				"	SC2010.C2_NUM,\r\n" + 
				"	SC2010.C2_XUSUARI,\r\n" + 
				"	SC2010.C2_LOCAL,\r\n" + 
				"	RTRIM(C2_PRODUTO) AS C2_PRODUTO,\r\n" + 
				"	B1_DESC,\r\n" + 
				"	SC2010.C2_QUANT,\r\n" + 
				"	SC2010.C2_QUJE,\r\n" + 
				"	SC2010.C2_PERDA,\r\n" + 
				"	SC2010.C2_EMISSAO,\r\n" + 
				"	SC2010.C2_DATPRF,\r\n" + 
				"	SC2010.C2_OBS,\r\n" + 
				"	(SELECT TOP 1 H1_DESCRI FROM SH6010 WITH(NOLOCK) INNER JOIN SH1010 WITH(NOLOCK) ON H1_FILIAL = '01' AND H6_RECURSO = H1_CODIGO WHERE H6_FILIAL = '01' AND H6_OP = C2_NUM + C2_ITEM + C2_SEQUEN ORDER BY H6_DATAFIN DESC) AS ULT_APT,\r\n" + 
				"	(SELECT TOP 1 H6_DATAFIN FROM SH6010 WITH(NOLOCK) WHERE H6_FILIAL = '01' AND H6_OP = C2_NUM + C2_ITEM + C2_SEQUEN ORDER BY H6_DATAFIN DESC) AS DATA_ULT_APT\r\n" + 
				"FROM SC2010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON B1_COD = C2_PRODUTO\r\n" + 
				"WHERE SC2010.D_E_L_E_T_ = ''\r\n" + 
				"AND SC2010.C2_FILIAL = '01'\r\n" + 
				"AND SC2010.C2_DATRF = ''\r\n" + 
				"AND C2_LOCAL IN ('01', '10', '11', '12', '03', '04', '05', '50', '31')\r\n" + 
				"ORDER BY C2_NUM, C2_ITEM, C2_SEQUEN";
		
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<OrdemProducao> ops = new LinkedList<>();
			while (rs.next()) {
				OrdemProducao op = new OrdemProducao();
				op.setNumero(rs.getString("C2_NUM"));
				op.setUsuario(rs.getString("C2_XUSUARI"));
				op.setLocal(rs.getString("C2_LOCAL"));
				op.setProduto(new Produto().setCodigo(rs.getString("C2_PRODUTO")).setDescricao(rs.getString("B1_DESC")));
				op.setQuant(rs.getBigDecimal("C2_QUANT"));
				op.setQuje(rs.getBigDecimal("C2_QUJE"));
				op.setPerda(rs.getBigDecimal("C2_PERDA"));
				op.setEmissao(rs.getString("C2_EMISSAO"));
				op.setDatprf(rs.getString("C2_DATPRF"));
				op.setObs(rs.getString("C2_OBS"));
				op.setUltApto(rs.getString("ULT_APT"));
				op.setDatUltApto(rs.getString("DATA_ULT_APT"));
				ops.add(op);
			}
			return ops;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return findAll();
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public static Iterable<OrdemProducao> findAllTabela() {
		String sql = "SELECT\r\n" + 
				"	SC2010.C2_NUM,\r\n" + 
				"	SC2010.C2_XUSUARI,\r\n" + 
				"	SC2010.C2_LOCAL,\r\n" + 
				"	RTRIM(C2_PRODUTO) AS C2_PRODUTO,\r\n" + 
				"	B1_DESC,\r\n" + 
				"	SC2010.C2_QUANT,\r\n" + 
				"	SC2010.C2_QUJE,\r\n" + 
				"	SC2010.C2_PERDA,\r\n" + 
				"	SC2010.C2_EMISSAO,\r\n" + 
				"	SC2010.C2_DATPRF,\r\n" + 
				"	SC2010.C2_OBS,\r\n" + 
				"	(SELECT TOP 1 H1_DESCRI FROM SH6010 WITH(NOLOCK) INNER JOIN SH1010 WITH(NOLOCK) ON H1_FILIAL = '01' AND H6_RECURSO = H1_CODIGO WHERE H6_FILIAL = '01' AND H6_OP = C2_NUM + C2_ITEM + C2_SEQUEN ORDER BY H6_DATAFIN DESC) AS ULT_APT,\r\n" + 
				"	(SELECT TOP 1 H6_DATAFIN FROM SH6010 WITH(NOLOCK) WHERE H6_FILIAL = '01' AND H6_OP = C2_NUM + C2_ITEM + C2_SEQUEN ORDER BY H6_DATAFIN DESC) AS DATA_ULT_APT\r\n" + 
				"FROM SC2010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON B1_COD = C2_PRODUTO\r\n" + 
				"WHERE SC2010.D_E_L_E_T_ = ''\r\n" + 
				"AND SC2010.C2_FILIAL = '01'\r\n" + 
				"AND SC2010.C2_DATRF = ''\r\n" + 
				"AND C2_LOCAL IN ('01', '10', '11', '12', '03', '04', '05', '50', '31')\r\n" + 
				"ORDER BY C2_NUM, C2_ITEM, C2_SEQUEN";
		
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<OrdemProducao> ops = new LinkedList<>();
			while (rs.next()) {
				OrdemProducao op = new OrdemProducao();
				op.setNumero(rs.getString("C2_NUM"));
				op.setUsuario(rs.getString("C2_XUSUARI"));
				op.setLocal(rs.getString("C2_LOCAL"));
				op.setProduto(new Produto().setCodigo(rs.getString("C2_PRODUTO")).setDescricao(rs.getString("B1_DESC")));
				op.setQuant(rs.getBigDecimal("C2_QUANT"));
				op.setQuje(rs.getBigDecimal("C2_QUJE"));
				op.setPerda(rs.getBigDecimal("C2_PERDA"));
				op.setEmissao(rs.getString("C2_EMISSAO"));
				op.setDatprf(rs.getString("C2_DATPRF"));
				op.setObs(rs.getString("C2_OBS"));
				op.setUltApto(rs.getString("ULT_APT"));
				op.setDatUltApto(rs.getString("DATA_ULT_APT"));
				ops.add(op);
			}
			return ops;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return findAllTabela();
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static OrdemProducao findOne(String numOp) {
		if (numOp == null) {
			return new OrdemProducao();
		}
		String num = numOp.substring(0, 6);
		String item = null;
		String sequen = null;
		
		if (numOp.length() > 6) {
			item = numOp.substring(6, 8);
			sequen = numOp.substring(8, 11);
		}
		
		String sql = "SELECT\r\n" + 
				"	SC2010.C2_NUM,\r\n" + 
				"	SC2010.C2_LOTECTL,\r\n" + 
				"	SC2010.C2_XUSUARI,\r\n" + 
				"	SC2010.C2_LOCAL,\r\n" + 
				"	RTRIM(C2_PRODUTO) AS C2_PRODUTO,\r\n" + 
				"	B1_DESC,\r\n" + 
				"	SC2010.C2_QUANT,\r\n" + 
				"	SC2010.C2_QUJE,\r\n" + 
				"	SC2010.C2_PERDA,\r\n" + 
				"	SC2010.C2_EMISSAO,\r\n" + 
				"	SC2010.C2_DATPRF,\r\n" + 
				"	SC2010.C2_OBS,\r\n" +
				"	(SELECT TOP 1 C1_NUM FROM SC1010 WITH(NOLOCK) WHERE LEFT(C1_OP, 6) = C2_NUM AND SC1010.D_E_L_E_T_ = '' AND C1_QUANT > C1_QUJE AND C1_RESIDUO <> 'S' ORDER BY C1_EMISSAO DESC) AS SC,\r\n" +
				"	(SELECT TOP 1 C7_NUM FROM SC7010 WITH(NOLOCK) WHERE LEFT(C7_OP, 6) = SC2010.C2_NUM AND SC7010.D_E_L_E_T_ = '' AND C7_QUANT > C7_QUJE AND C7_RESIDUO <> 'S' ORDER BY C7_EMISSAO DESC) AS PC,\r\n" +
				"	(SELECT TOP 1 H1_DESCRI FROM SH6010 WITH(NOLOCK) INNER JOIN SH1010 WITH(NOLOCK) ON H1_FILIAL = '01' AND H6_RECURSO = H1_CODIGO WHERE H6_FILIAL = '01' AND H6_OP = C2_NUM + C2_ITEM + C2_SEQUEN ORDER BY H6_DATAFIN DESC) AS ULT_APT,\r\n" + 
				"	(SELECT TOP 1 H6_DATAFIN FROM SH6010 WITH(NOLOCK) WHERE H6_FILIAL = '01' AND H6_OP = C2_NUM + C2_ITEM + C2_SEQUEN ORDER BY H6_DATAFIN DESC) AS DATA_ULT_APT\r\n" + 
				"FROM SC2010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON B1_COD = C2_PRODUTO\r\n" + 
				"WHERE SC2010.D_E_L_E_T_ = ''\r\n" + 
				"AND SC2010.C2_FILIAL = '01'\r\n" + 
				"AND (SC2010.C2_NUM = '" + num + "')\r\n";
		if (numOp.length() > 6) {
			sql += "AND C2_ITEM = '" + item + "'\r\n";
			sql += "AND C2_SEQUEN = '" + sequen + "'\r\n";
		}
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				OrdemProducao op = new OrdemProducao();
				op.setNumero(numOp);
				op.setLote(rs.getString("C2_LOTECTL"));
				op.setUsuario(rs.getString("C2_XUSUARI"));
				op.setLocal(rs.getString("C2_LOCAL"));
				op.setProduto(new Produto().setCodigo(rs.getString("C2_PRODUTO")).setDescricao(rs.getString("B1_DESC")));
				op.setQuant(rs.getBigDecimal("C2_QUANT"));
				op.setQuje(rs.getBigDecimal("C2_QUJE"));
				op.setPerda(rs.getBigDecimal("C2_PERDA"));
				op.setEmissao(rs.getString("C2_EMISSAO"));
				op.setDatprf(rs.getString("C2_DATPRF"));
				op.setObs(rs.getString("C2_OBS"));
				op.setPc(rs.getString("PC"));
				op.setSc(rs.getString("SC"));
				op.setUltApto(rs.getString("ULT_APT"));
				op.setDatUltApto(rs.getString("DATA_ULT_APT"));
				DataTable relacaoOps = getRelacaoOps(null, null, null, null, null, numOp, null, null, null, null, null);
				try {
					String status = (String) relacaoOps.getData().iterator().next().toArray()[3];
					op.setStatus(status);
				} catch(Exception e) {
				}
				return op;
			}
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return findOne(numOp);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static DetalhesOP findDetalhes(OrdemProducao op) {
		String sql = "SELECT\r\n" + 
				"G2_PRODUTO, C2_NUM,C2_PRODUTO,C2_XDESC,G2_RECURSO,G2_DESCRI, G2_XDESEXT,H6_DATAFIN,H6_QTDPROD,H6_QTDPERD,H6_OPERADO\r\n" + 
				"FROM SC2010 WITH(NOLOCK)\r\n" + 
				"LEFT JOIN SG2010 WITH(NOLOCK) ON C2_PRODUTO = G2_PRODUTO\r\n" + 
				"AND SG2010.D_E_L_E_T_ = ''\r\n" + 
				"AND G2_FILIAL = '01'\r\n" + 
				"LEFT JOIN SH6010 WITH(NOLOCK) ON H6_OP = C2_NUM + C2_ITEM + C2_SEQUEN\r\n" + 
				"AND H6_PRODUTO = C2_PRODUTO\r\n" + 
				"AND H6_OPERAC = G2_OPERAC\r\n" + 
				"AND SH6010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE C2_FILIAL = '01'\r\n" + 
				"AND SC2010.D_E_L_E_T_ = ''\r\n" + 
				"--AND (SELECT C2_DATRF FROM SC2010 C2 WITH(NOLOCK) WHERE SC2010.C2_NUM = C2.C2_NUM AND SC2010.C2_ITEM = C2.C2_ITEM AND C2.C2_SEQUEN = '001' AND C2.C2_DATRF = '') = ''\r\n" + 
				"AND C2_NUM = ?\r\n" + 
				"ORDER BY C2_NUM, C2_ITEM, C2_SEQUEN, G2_OPERAC DESC";
		
		DetalhesOP dop = new DetalhesOP();
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setString(1, op.getNumero());
			ResultSet rs = stmt.executeQuery();
			dop.setOp(op);
			while (rs.next()) {
				Processo processo = new Processo();
				processo.setProduto(rs.getString("G2_PRODUTO"));
				processo.setRecurso(rs.getString("G2_RECURSO"));
				processo.setDescri(rs.getString("G2_DESCRI"));
				processo.setXDescri(rs.getString("G2_XDESEXT"));
				processo.setDataFin(rs.getString("H6_DATAFIN"));
				processo.setqtdProd(rs.getBigDecimal("H6_QTDPROD"));
				processo.setQtdPerd(rs.getBigDecimal("H6_QTDPERD"));
				processo.setOperador(rs.getString("H6_OPERADO"));
				dop.getProcessos().add(processo);
			}
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return findDetalhes(op);
			} else {
				e.printStackTrace();
			}
		}
		
		sql = "SELECT C7_NUM, A2_NOME, C7_EMISSAO, C7_QUANT, C7_QUJE, D1_DOC, D1_DTDIGIT\r\n" +
			  "FROM SC7010 WITH(NOLOCK)\r\n" +
			  "INNER JOIN SA2010 WITH(NOLOCK) ON A2_FILIAL <> 'ZZ' AND A2_COD = C7_FORNECE\r\n" +
			  "LEFT JOIN SD1010 WITH(NOLOCK) ON SD1010.D_E_L_E_T_ = '' AND D1_FILIAL <> 'ZZ' AND D1_PEDIDO = C7_NUM AND D1_ITEMPC = C7_ITEM\r\n" +
			  "WHERE\r\n" +
			  "		C7_FILIAL <> 'ZZ'\r\n" +
			  "		AND C7_OP LIKE '" + op.getNumero() + "%'\r\n" +
			  "		AND SC7010.D_E_L_E_T_ = ''";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			List<PedidoCompra> pedidos = new LinkedList<PedidoCompra>();
			while (rs.next()) {
				PedidoCompra pc = new PedidoCompra();
				pc.setNumero(rs.getString(1));
				pc.setFornecedor(rs.getString(2));
				pc.setEmissao(rs.getString(3));
				pc.setQuantidade(rs.getString(4));
				pc.setQuantFin(rs.getString(5));
				pc.setNF(rs.getString(6));
				pc.setDtEntrega(rs.getString(7));
				pedidos.add(pc);
			}
			dop.setPedidos(pedidos);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dop;
	}

	public static Collection<Collection<Object>> getEmpenhosEstoque() {
		Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
		String sql = "SELECT\r\n" + 
				"D4_OP AS NUMERO,D4_COD AS PRODUTO,B1_DESC AS DESCRICAO,D4_QUANT AS QUANTIDADE,BF_QUANT - BF_EMPENHO AS QUANTIDADE_ESTOQUE, BF_LOCAL AS ARMAZEM, BF_LOTECTL AS LOTE\r\n" + 
				"FROM SD4010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SC2010 WITH(NOLOCK) ON D4_OP = C2_NUM + C2_ITEM + C2_SEQUEN\r\n" + 
				"AND SC2010.D_E_L_E_T_ = ''\r\n" + 
				"AND C2_FILIAL = '01'\r\n" + 
				"AND C2_DATRF = ''\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON B1_COD = D4_COD\r\n" + 
				"AND B1_FILIAL = ''\r\n" + 
				"AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"INNER JOIN SBF010 WITH(NOLOCK) ON BF_PRODUTO = D4_COD\r\n" + 
				"AND BF_QUANT - BF_EMPENHO > 0\r\n" + 
				"AND SBF010.D_E_L_E_T_ = ''\r\n" + 
				"AND BF_FILIAL = '01'\r\n" + 
				"WHERE\r\n" + 
				"SD4010.D_E_L_E_T_ = ''\r\n" + 
				"AND D4_FILIAL = '01'\r\n" + 
				"AND D4_QUANT > 0";
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				LinkedList<Object> row = new LinkedList<>();
				row.add(rs.getString(1));
				row.add(rs.getString(2));
				row.add(rs.getString(3));
				row.add(rs.getString(4));
				row.add(rs.getString(5));
				row.add(rs.getString(6));
				row.add(rs.getString(7));
				data.add(row);
			}
			return data;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getEmpenhosEstoque();
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static DataTable getEmpenhosOP(String codigoProduto, String numOP, String lote, String armazem, String emissao, String usuario, String pvs, boolean mostraEmpAtend) {
		String sql = "SELECT\r\n" + 
				"C2_PRODUTO, C2_XDESC, D4_OP, B1_UM, C2_EMISSAO, C2_XUSUARI, D4_COD, B1_DESC,D4_QUANT,RTRIM(D4_LOTECTL) AS D4_LOTECTL,D4_LOCAL,D4_QTDEORI, C2_OBS, ZS_DATA,\r\n" + 
				"ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = B1_LOCPAD AND D_E_L_E_T_ = ''), 0) AS QTD_ESTOQUE,\r\n" +
				"ISNULL((SELECT SUM(DA_SALDO) FROM SDA010 WITH(NOLOCK) WHERE DA_FILIAL <> 'ZZ' AND D_E_L_E_T_ = '' AND DA_PRODUTO = B1_COD AND DA_SALDO <> 0 AND DA_LOCAL = B1_LOCPAD), 0) AS QTD_ENDERECAR,\r\n" + 
				"ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = '98' AND D_E_L_E_T_ = ''), 0) AS QTD_INSPECAO,\r\n" + 
				"ISNULL((SELECT SUM(D4_QUANT) FROM SD4010 D4 WITH(NOLOCK) INNER JOIN SC2010 C2 WITH(NOLOCK) ON C2_FILIAL <> 'ZZ' AND D4.D4_OP = C2.C2_NUM + C2.C2_ITEM + C2.C2_SEQUEN AND C2.D_E_L_E_T_ = '' AND C2.C2_DATRF = '' WHERE SD4010.D4_COD = D4.D4_COD AND D4.D4_LOTECTL = '' AND D4.D_E_L_E_T_ = ''), 0) AS QTD_EMPENHO,\r\n" + 
				"ISNULL((SELECT SUM(C2_QUANT - C2_QUJE - C2_PERDA) FROM SC2010 WITH(NOLOCK) WHERE C2_FILIAL <> 'ZZ' AND C2_PRODUTO = B1_COD AND C2_DATRF='' AND C2_LOCAL=B1_LOCPAD AND D_E_L_E_T_='' AND C2_STATUS IN ('N', '')), 0) AS QTD_OP,\r\n" +
				"ISNULL((SELECT SUM(C1_QUANT - C1_QUJE) FROM SC1010 C1 WITH(NOLOCK) WHERE C1.D_E_L_E_T_='' AND C1_QUJE < C1_QUANT AND C1_RESIDUO <> 'S' AND C1_PRODUTO = B1_COD AND C1_LOCAL = B1_LOCPAD), 0) AS QTD_SC,\r\n" +
				"ISNULL((SELECT SUM(C7_QUANT - C7_QUJE) FROM SC7010 WITH(NOLOCK) WHERE D_E_L_E_T_='' AND C7_PRODUTO = B1_COD AND (C7_QUANT - C7_QUJE > 0) AND C7_RESIDUO<>'S' AND C7_LOCAL = B1_LOCPAD), 0) AS QTD_PC\r\n" +
				"FROM SD4010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SC2010 WITH(NOLOCK) ON LEFT(D4_OP, 6) = C2_NUM\r\n";
		
		if (numOP != null && numOP.length() > 6) {
			sql += "AND C2_ITEM = SUBSTRING(D4_OP, 7, 2)\r\n";
		} else {
			sql += "AND C2_ITEM = '01'\r\n";
		}
		if (numOP != null && numOP.length() > 8) {
			sql += "AND C2_SEQUEN = SUBSTRING(D4_OP, 9, 3)\r\n";
		} else {
			sql += "AND C2_SEQUEN = '001'\r\n";
		}
				sql += "AND SC2010.D_E_L_E_T_ = ''\r\n" +
				"INNER JOIN SB1010 WITH(NOLOCK) ON D4_COD = B1_COD\r\n" + 
				"AND SB1010.D_E_L_E_T_ = ''\r\n" +
				"LEFT JOIN SZS010 WITH(NOLOCK) ON ZS_OP = D4_OP AND SZS010.D_E_L_E_T_ = '' AND ZS_FILIAL <> 'ZZ' AND ZS_PRODUTO = D4_COD AND ZS_LOTECTL = D4_LOTECTL AND ZS_QUANT = D4_QTDEORI AND ZS_ESTORNO = 'N' AND ZS_TIPPROC <> 'D'\r\n" +
				"WHERE SD4010.D_E_L_E_T_ = ''\r\n" +
				"AND (D4_COD NOT LIKE '%PI%' OR D4_COD LIKE 'PI%')\r\n";
		if (numOP != null && !numOP.equals("")) {
			if (numOP.replace("\t", ";").replace(" ", ";").contains(";")) {
				String[] numeros = numOP.replace("\t", ";").replace(" ", ";").split(";");
				for (int i = 0; i < numeros.length; i++) {
					if (i == 0) {
							sql += "AND (";
					} else {
						sql += " OR ";
					}
					if (numeros[i].length() > 6) {
						sql += "D4_OP = '" + numeros[i] + "'";
					} else {
						sql += "LEFT(D4_OP, 6) = '" + numeros[i] + "'";
					}
				}
				sql += ")\r\n";
			} else if (numOP.contains("~")) {
				String[] numeros = numOP.split("~");
				sql += "AND D4_OP >= '" + numeros[0] + "' AND D4_OP <= '" + numeros[1] + "'\r\n";
			} else {
				if (numOP.length() > 6) {
					sql += "AND D4_OP = '" + numOP + "'\r\n";
				} else {
					sql += "AND LEFT(D4_OP, 6) = '" + numOP + "'\r\n";
				}
			}
		} else {
			sql += "--AND C2_DATRF = ''\r\n";
			sql += "AND D4_QUANT > 0\r\n";
		}
		if (lote != null && !lote.equals("")) {
			sql += "AND D4_LOTECTL = '" + lote + "'\r\n";
		} else {
			if (!mostraEmpAtend) {
				sql += "AND D4_LOTECTL = ''\r\n";
			}
		}
		if (armazem != null && !armazem.equals("")) {
			sql += "AND D4_LOCAL = '" + armazem + "'\r\n";
		}
		if (usuario != null && !usuario.equals("")) {
			sql += "AND UPPER(C2_XUSUARI) like '" + usuario.toUpperCase() + "%'\r\n";
		}
		if (emissao != null && !emissao.equals("")) {
			if (emissao.contains(">") || emissao.contains("<")) {
				sql += "AND C2_EMISSAO " + emissao + "\r\n";
			} else {
				sql += "AND C2_EMISSAO = '" + emissao + "'\r\n";
			}
		}
		if (codigoProduto != null && !codigoProduto.equals("")) {
			sql += "AND D4_COD = '" + codigoProduto + "'\r\n";
		}
		sql += "ORDER BY 1, 2, 3, 4, 5, 6";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				linha.add(HtmlUtils.createLink("/ordens-producao/" + rs.getString("D4_OP") + "/detalhes", rs.getString("D4_OP")));
				linha.add(rs.getString("C2_PRODUTO"));
				linha.add(rs.getString("C2_XDESC"));
				linha.add(rs.getString("C2_EMISSAO"));
				linha.add(rs.getString("C2_XUSUARI"));
				String produto = rs.getString("D4_COD");
				linha.add(produto);
				linha.add(rs.getString("B1_DESC"));
				linha.add(rs.getString("B1_UM"));
				linha.add(rs.getBigDecimal("D4_QTDEORI").setScale(2, RoundingMode.HALF_EVEN));
				BigDecimal empenhoOp = rs.getBigDecimal("D4_QUANT").setScale(2, RoundingMode.HALF_EVEN);
				linha.add(empenhoOp);
				String loteEmpenho = rs.getString("D4_LOTECTL");
				linha.add(loteEmpenho);
				linha.add(rs.getString("D4_LOCAL"));
				linha.add(rs.getString("C2_OBS"));
				BigDecimal qtdEstoque = rs.getBigDecimal("QTD_ESTOQUE").setScale(2, RoundingMode.HALF_EVEN);
				linha.add(HtmlUtils.createLink("/saldo-por-endereco?produto=" + produto + "&descricao=&lote=&armazem=", qtdEstoque));
				BigDecimal qtdEnderecar = rs.getBigDecimal("QTD_ENDERECAR").setScale(2, RoundingMode.HALF_EVEN);
				linha.add(qtdEnderecar);
				BigDecimal qtdInspecao = rs.getBigDecimal("QTD_INSPECAO").setScale(2, RoundingMode.HALF_EVEN);
				linha.add(qtdInspecao);
				if (empenhoOp.compareTo(qtdEstoque) <= 0 || !loteEmpenho.equals("")) {
					linha.add("atende");
				} else if (empenhoOp.compareTo(qtdEstoque.add(qtdEnderecar.add(qtdInspecao))) <= 0 || !loteEmpenho.equals("")) {
					linha.add("enderecar/inspecionar");
				} else {
					linha.add("nao atende");
				}
				BigDecimal qtdEmpenho = rs.getBigDecimal("QTD_EMPENHO").setScale(2, RoundingMode.HALF_EVEN);
				linha.add(HtmlUtils.createLink("/relacao-empenhos?op=&lote=&armazem=&emissao=&usuario=&codigoProduto=" + produto, qtdEmpenho));
				BigDecimal qtdOp = rs.getBigDecimal("QTD_OP").setScale(2, RoundingMode.HALF_EVEN);
				linha.add(HtmlUtils.createLink("/relacao-ops?situacao=abertas&codigoProduto=" + produto, qtdOp));
				BigDecimal qtdSc = rs.getBigDecimal("QTD_SC").setScale(2, RoundingMode.HALF_EVEN);
				linha.add(HtmlUtils.createLink("/relacao-scs?abertos=true&codigoProduto=" + produto, qtdSc));
				BigDecimal qtdPc = rs.getBigDecimal("QTD_PC").setScale(2, RoundingMode.HALF_EVEN);
				linha.add(HtmlUtils.createLink("/relacao-pcs?abertos=true&codigoProduto=" + produto, qtdPc));
				linha.add(qtdEstoque.add(qtdEnderecar).add(qtdInspecao).subtract(qtdEmpenho).add(qtdOp).add(qtdSc).add(qtdPc));
				linha.add(rs.getString("ZS_DATA"));
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static DataTable getRelacaoOps(String usuario, String emissaoDe, String emissaoAte, String sequencia, String armazem, String numero, String codigoProduto, String descricaoProduto, String status, String lote, String situacao) {
		String sql = "SELECT\r\n" + 
				"C2_DATRF, C2_LOCAL, C2_NUM + C2_ITEM + C2_SEQUEN AS OP_COMPLETA, C2_NUM, C2_ITEM, C2_SEQUEN, C2_PRODUTO, C2_XDESC, C2_QUANT, C2_QUJE, C2_LOTECTL, C2_XUSUARI, C2_EMISSAO, C2_XQTDPRT, C2_OBS,\r\n" + 
				"(SELECT COUNT(D4_OP) FROM SD4010 WITH (NOLOCK) WHERE LEFT(D4_OP, 6) = C2_NUM AND SD4010.D_E_L_E_T_ = '' AND D4_LOCAL IN ('10', '11', '91') AND D4_LOTECTL = '' AND D4_QUANT > 0) AS EMPENHOS,\r\n" + 
				"(SELECT COUNT(H6_OP) FROM SH6010 WITH (NOLOCK) WHERE LEFT(H6_OP, 6) = C2_NUM AND SH6010.D_E_L_E_T_ = '') AS APONTAMENTOS,\r\n" +
				"(SELECT DISTINCT COUNT(C7_NUM) AS C7_NUM FROM SC7010 WITH(NOLOCK) WHERE LEFT(C7_OP, 6) = SC2010.C2_NUM AND SC7010.D_E_L_E_T_ = '' AND C7_QUANT > C7_QUJE AND C7_RESIDUO <> 'S') AS PC,\r\n" +
				"(SELECT COUNT(D4_COD) FROM (\r\n" + 
					"SELECT\r\n" + 
					"D4_COD, D4_QUANT, (SELECT ISNULL(SUM(B8_SALDO - B8_EMPENHO), 0) FROM SB8010 WITH (NOLOCK) JOIN SB1010 WITH(NOLOCK) ON B1_COD = B8_PRODUTO AND SB1010.D_E_L_E_T_ = '' AND B1_FILIAL <> 'ZZ' WHERE SB8010.D_E_L_E_T_ = '' AND B8_PRODUTO = D4_COD AND B8_SALDO > 0 AND B8_LOCAL = B1_LOCPAD) AS ESTOQUE,\r\n" + 
					"(SELECT ISNULL(SUM(BF_QUANT - BF_EMPENHO), 0) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = D4_COD AND BF_LOCAL = '98' AND D_E_L_E_T_ = '') AS QTD_INSPECAO\r\n" +
					"FROM SD4010 WITH (NOLOCK)\r\n" + 
					"WHERE LEFT(D4_OP, 6) = C2_NUM\r\n" + 
					"AND SD4010.D_E_L_E_T_ = ''\r\n" + 
					"AND D4_LOTECTL = ''\r\n" +
					"AND D4_COD NOT LIKE '%PI%'\r\n" +
					"AND D4_QUANT > 0\r\n" +
					") AS RESULTSET\r\n" + 
					"WHERE D4_QUANT > (ESTOQUE + QTD_INSPECAO)) AS EMPENHO_ESTOQUE\r\n" +
				"FROM SC2010 WITH (NOLOCK)\r\n" + 
				"WHERE\r\n" + 
				"SC2010.D_E_L_E_T_ = ''\r\n" +
				"AND C2_PRODUTO NOT LIKE '%PI%'\r\n";
				
		if (situacao != null && situacao.equals("abertas")) {
			sql += "AND C2_DATRF = ''\r\n";
		}
		if(usuario != null && !usuario.equals("")) {
			if (usuario.contains(";")) {
				String[] usuarios = usuario.split(";");
				sql += "AND (";
				for (int i = 0; i < usuarios.length; i++) {
					if (i < usuarios.length - 1)
						sql += "UPPER(C2_XUSUARI) like '" + usuarios[i].toUpperCase() + "%' OR \r\n";
					else
						sql += "UPPER(C2_XUSUARI) like '" + usuarios[i].toUpperCase() + "%'\r\n";;
				}
				sql += ")\r\n";
			} else {
				sql += "AND UPPER(C2_XUSUARI) like '" + usuario.toUpperCase() + "%'\r\n";
			}
		}
		if (emissaoDe != null && !emissaoDe.equals("")) {
			sql += "AND C2_EMISSAO >= '" + emissaoDe + "'\r\n";
		}
		if (emissaoAte != null && !emissaoAte.equals("")) {
			sql += "AND C2_EMISSAO <= '" + emissaoAte + "'\r\n";
		}
		if (sequencia != null && !sequencia.equals("")) {
			if (sequencia.contains("<>")) {
				sql += "AND C2_SEQUEN <> '" + sequencia.split("<>")[1] + "'\r\n";
			} else {
				sql += "AND C2_SEQUEN = '" + sequencia + "'\r\n";
			}
		}
		if (armazem != null && !armazem.equals("")) {
			if (armazem.contains(";")) {
				String[] armazens = armazem.split(";");
				for (int i = 0; i < armazens.length; i++) {
					if (i == 0) {
						sql += "AND C2_LOCAL IN (";
					} else {
						sql += ",";
					}
					sql += "'" + armazens[i] + "'";
				}
				sql += ")\r\n";
			} else {
				sql += "AND C2_LOCAL = '" + armazem + "'\r\n";
			}
			
			
			
		}
		if (numero != null && !numero.equals("")) {
			if (numero.replace(" ", ";").contains(";")) {
				String[] numeros = numero.replace(" ", ";").split(";");
				for (int i = 0; i < numeros.length; i++) {
					if (i == 0) {
						sql += (numeros[i].length() > 6) ? "AND C2_NUM + C2_ITEM + C2_SEQUEN IN (" : "AND C2_NUM IN (";
					} else {
						sql += ",";
					}
					sql += "'" + numeros[i] + "'";
				}
				sql += ")\r\n";
			} else if (numero.contains("~")) {
				String[] numeros = numero.split("~");
				sql += "AND C2_NUM >= '" + numeros[0] + "' AND C2_NUM <= '" + numeros[1] + "'\r\n";
			} else {
				sql += (numero.length() > 6) ? "AND C2_NUM + C2_ITEM + C2_SEQUEN = '" + numero + "'\r\n" : "AND C2_NUM = '" + numero + "'\r\n";
			}
		}
		if (codigoProduto != null && !codigoProduto.equals("")) {
			sql += "AND C2_PRODUTO like '" + codigoProduto + "'\r\n";
		}
		if (descricaoProduto != null && !descricaoProduto.equals("")) {
			sql += "AND C2_XDESC LIKE '%" + descricaoProduto + "%'\r\n";
		}
		if (lote != null && !lote.equals("")) {
			sql += "AND C2_LOTECTL = '" + lote + "'\r\n";
		}
		
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				long impressoes = rs.getLong("C2_XQTDPRT");
				long empenhos = rs.getLong("EMPENHOS");
				long apontamentos = rs.getLong("APONTAMENTOS");
				long empenhoEstoque = rs.getLong("EMPENHO_ESTOQUE");
				String pedidoCompra = rs.getString("PC");
				String statusOp = "";
								
				if (!rs.getString("C2_DATRF").equals("        ")) {
					statusOp += "F";
				} else if (empenhos == 0) { // EMPENHOS = 0 QUER DIZER NENHUM EMPENHO PENDENTE NA OP!	
					statusOp += "E";
				} else if (empenhoEstoque == 0) { // EMPENHOSESTOQUE = 0 QUER DIZER QUE O ESTOQUE DISPONIVEL É SUPERIOR À QUANTIDADE EMPENHADA
					statusOp += "D";
				}
				if (impressoes > 0) {
					if (statusOp.length() > 0) {
						statusOp += " - ";
					}
					statusOp += "I";
				}
				if (apontamentos > 0) {
					if (statusOp.length() > 0) {
						statusOp += " - ";
					}
					statusOp += "A";
				}
				if (pedidoCompra != null && !pedidoCompra.equals("0")) {
					if (statusOp.length() > 0) {
						statusOp += " - ";
					}
					statusOp += "PC";
				}
				if (status != null && !statusOp.contains(status.toUpperCase())) {
					continue;
				}
				linha.add(HtmlUtils.createLink("/ordens-producao/" + rs.getString("C2_NUM") + "/detalhes", rs.getString("OP_COMPLETA")));
				linha.add(rs.getString("C2_NUM"));
				linha.add(rs.getString("C2_EMISSAO"));
				linha.add(statusOp);
				linha.add(rs.getString("C2_ITEM"));
				linha.add(rs.getString("C2_SEQUEN"));
				linha.add(rs.getString("C2_LOCAL"));
				linha.add(rs.getString("C2_PRODUTO"));
				linha.add(rs.getString("C2_XDESC"));
				linha.add(rs.getString("C2_QUANT"));
				linha.add(rs.getString("C2_QUJE"));
				linha.add(rs.getString("C2_LOTECTL"));
				linha.add(rs.getString("C2_XUSUARI"));
				linha.add(rs.getString("C2_OBS"));
				linha.add(impressoes);
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getRelacaoOps(usuario, emissaoDe, emissaoAte, sequencia, armazem, numero, codigoProduto, descricaoProduto, status, lote, situacao);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static DataTable getUltimasOps(String codigoProduto, int qtd) {
		String sql = "SELECT TOP " + qtd + "\r\n" + 
				"C2_NUM, C2_QUANT,C2_EMISSAO, ISNULL(C7_NUM, '') AS C7_NUM, ISNULL(A2_NOME, '') AS A2_NOME\r\n" + 
				"FROM SC2010 WITH(NOLOCK)\r\n" + 
				"LEFT JOIN SC7010 WITH(NOLOCK) ON C7_OP = C2_NUM + C2_ITEM + C2_SEQUEN\r\n" + 
				"AND SC7010.D_E_L_E_T_ = ''\r\n" + 
				"LEFT JOIN SA2010 WITH(NOLOCK) ON C7_FORNECE = A2_COD\r\n" + 
				"AND C7_LOJA = A2_LOJA\r\n" + 
				"AND SA2010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE\r\n" + 
				"SC2010.D_E_L_E_T_ = ''\r\n" + 
				"AND C2_PRODUTO = '" + codigoProduto + "'\r\n" + 
				"ORDER BY C2_EMISSAO DESC";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				linha.add(HtmlUtils.createLink("/relacao-ops?numero=" + rs.getString("C2_NUM"), rs.getString("C2_NUM")));
				linha.add(rs.getString("C2_QUANT"));
				linha.add(rs.getString("C2_EMISSAO"));
				linha.add(rs.getString("C7_NUM"));
				linha.add(rs.getString("A2_NOME"));
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getStatus(String op) {
		String num = op.substring(0, 6);
		String item = null;
		String sequen = null;
		
		if (op.length() > 6) {
			item = op.substring(6, 8);
			sequen = op.substring(8,  11);
		}
		
		String sql = "SELECT\r\n" + 
				"C2_DATRF,\r\n" + 
				"(SELECT COUNT(H6_OP) FROM SH6010 WITH(NOLOCK) WHERE LEFT(H6_OP, 6) = C2_NUM AND SH6010.D_E_L_E_T_ = '') AS APONTAMENTOS\r\n" + 
				"FROM SC2010 WITH(NOLOCK)\r\n";
				sql += "WHERE C2_NUM = '" + num + "'\r\n";
				if (item != null) {
					sql += "AND C2_ITEM = '" + item + "'\r\n";
				}
				if (sequen != null) {
					sql += "AND C2_SEQUEN = '" + sequen + "'\r\n"; 
				} else {
					sql += "AND C2_SEQUEN = '001'\r\n";
				}
				sql += "AND SC2010.D_E_L_E_T_ = ''";
		
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				if (!rs.getString("C2_DATRF").equals("        ")) {
					return "FINALIZADA";
				} else if (rs.getInt("APONTAMENTOS") > 0) {
					return "ANDAMENTO";
				} else {
					return "ABERTA";
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static boolean temOpsFilhas() {
		String sql = "SELECT\r\n" + 
				"C2_NUM\r\n" + 
				"FROM SC2010 WITH(NOLOCK)\r\n" +
				"INNER JOIN SB1010 WITH(NOLOCK) ON C2_PRODUTO = B1_COD AND SB1010.D_E_L_E_T_ = ''\r\n" +
				"WHERE C2_SEQUEN <> '001'\r\n" + 
				"AND SC2010.D_E_L_E_T_ = ''\r\n" + 
				"AND C2_DATRF = ''\r\n" +
				"AND B1_COD NOT LIKE '%PI%'\r\n" + 
				"AND C2_LOCAL IN ('01', '10', '11', '12')";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static DataTable getOpsFilhas() {
		String sql = "SELECT\r\n" + 
				"C2_NUM + C2_ITEM + C2_SEQUEN AS OP_COMPLETA, C2_NUM, C2_EMISSAO, C2_ITEM, C2_SEQUEN, C2_LOCAL, C2_PRODUTO, C2_XDESC, C2_QUANT, C2_QUJE, C2_LOTECTL, C2_XUSUARI, C2_OBS\r\n" + 
				"FROM SC2010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON C2_PRODUTO = B1_COD AND SB1010.D_E_L_E_T_ = ''\r\n" +
				"WHERE C2_SEQUEN <> '001'\r\n" + 
				"AND SC2010.D_E_L_E_T_ = ''\r\n" + 
				"AND C2_DATRF = ''\r\n" + 
				"AND B1_COD NOT LIKE '%PI%'\r\n" + 
				"AND C2_LOCAL IN ('01', '10', '11', '12')";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				linha.add(HtmlUtils.createLink("/ordens-producao/" + rs.getString("C2_NUM") + "/detalhes", rs.getString("OP_COMPLETA")));
				linha.add(rs.getString("C2_NUM"));
				linha.add(rs.getString("C2_EMISSAO"));
				linha.add("");
				linha.add(rs.getString("C2_ITEM"));
				linha.add(rs.getString("C2_SEQUEN"));
				linha.add(rs.getString("C2_LOCAL"));
				linha.add(rs.getString("C2_PRODUTO"));
				linha.add(rs.getString("C2_XDESC"));
				linha.add(rs.getString("C2_QUANT"));
				linha.add(rs.getString("C2_QUJE"));
				linha.add(rs.getString("C2_LOTECTL"));
				linha.add(rs.getString("C2_XUSUARI"));
				linha.add(rs.getString("C2_OBS"));
				linha.add("");
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static DataTable getGetEmpenhosPendentes(List<String> todasOps) {
		if (todasOps == null || todasOps.size() == 0) {
			return new DataTable();
		}
		
		String sql = "SELECT * FROM (\r\n" +
				"SELECT\r\n" + 
				"RTRIM(D4_OP) AS D4_OP, C2_PRODUTO, C2_XDESC, RTRIM(D4_COD) AS D4_COD, B1_DESC, D4_QUANT,\r\n" +
				"(SELECT ISNULL(SUM(BF_QUANT - BF_EMPENHO), 0) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = D4_COD AND BF_LOCAL = B1_LOCPAD AND D_E_L_E_T_ = '') AS QTD_ESTOQUE,\r\n" +
				"(SELECT ISNULL(SUM(DA_SALDO), 0) FROM SDA010 WITH(NOLOCK) WHERE DA_PRODUTO = D4_COD AND DA_SALDO <> 0 AND D_E_L_E_T_ = '' AND DA_LOCAL = B1_LOCPAD) AS QTD_ENDERECAR,\r\n"+
				"(SELECT ISNULL(SUM(BF_QUANT - BF_EMPENHO), 0) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = D4_COD AND BF_LOCAL = '98' AND D_E_L_E_T_ = '') AS QTD_INSPECAO,\r\n"+
				"(SELECT ISNULL(SUM(D4_QUANT), 0) FROM SD4010 D4 WITH(NOLOCK) INNER JOIN SC2010 C2 WITH(NOLOCK) ON D4.D4_OP = C2.C2_NUM + C2.C2_ITEM + C2.C2_SEQUEN AND C2.D_E_L_E_T_ = '' AND C2.C2_DATRF = '' WHERE SD4010.D4_COD = D4.D4_COD AND D4.D_E_L_E_T_ = '' AND D4.D4_LOTECTL = '') AS QTD_EMPENHO,\r\n"+
				"(SELECT ISNULL(SUM(C2_QUANT - C2_QUJE - C2_PERDA), 0) FROM SC2010 WITH(NOLOCK) WHERE C2_PRODUTO = D4_COD AND C2_DATRF='' AND C2_LOCAL=B1_LOCPAD AND D_E_L_E_T_='' AND C2_STATUS IN ('N', '')) AS QTD_OP,\r\n"+
				"(SELECT ISNULL(SUM(C1_QUANT - C1_QUJE), 0) FROM SC1010 C1 WITH(NOLOCK) WHERE C1.D_E_L_E_T_='' AND C1_QUJE < C1_QUANT AND C1_RESIDUO <> 'S' AND C1_PRODUTO = D4_COD) AS QTD_SC,\r\n"+
				"(SELECT ISNULL(SUM(C7_QUANT - C7_QUJE), 0) FROM SC7010 WITH(NOLOCK) WHERE D_E_L_E_T_='' AND C7_PRODUTO = D4_COD AND (C7_QUANT - C7_QUJE > 0) AND C7_RESIDUO<>'S') AS QTD_PC\r\n"+
				"FROM SD4010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON D4_COD = B1_COD\r\n" +
				"INNER JOIN SC2010 WITH(NOLOCK) ON D4_OP = C2_NUM + C2_ITEM + C2_SEQUEN AND SC2010.D_E_L_E_T_ = '' AND C2_FILIAL <> 'ZZ'\r\n" +
				"AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE SD4010.D_E_L_E_T_ = ''\r\n" + 
				"AND D4_QUANT > 0\r\n" + 
				"AND D4_LOTECTL = ''\r\n" +
				"AND D4_COD NOT LIKE '%PI%'\r\n"; 
				//TODO criar rotina aqui de filtrar as OPs do pedido
				for (int i = 0; i < todasOps.size(); i++) {
					String string = todasOps.get(i);
					if (i == 0) {
						sql += "AND (\r\n";
						sql += "	(D4_OP LIKE '" + string + "%')\r\n";
					} else {
						sql += "	OR (D4_OP LIKE '" + string + "%')\r\n";
					}
					if (i == todasOps.size() - 1) {
						sql += ")\r\n";
					}
				}
				sql += ") AS RESULTSET\r\n" + 
				"WHERE D4_QUANT > QTD_ESTOQUE";
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				LinkedList<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					if (i < 6) {
						linha.add(rs.getObject(i));
					} else {
						if (rs.getMetaData().getColumnName(i).equals("QTD_OP")) {
							linha.add(HtmlUtils.createLink("/relacao-ops?numero=&sequencia=&lote=&codigoProduto=" + linha.get(3) + "&descricaoProduto=&emissaoDe=&emissaoAte=&armazem=&status=&usuario=&situacao=abertas", rs.getBigDecimal(i).setScale(2, RoundingMode.HALF_EVEN)));
						} else if (rs.getMetaData().getColumnName(i).equals("QTD_EMPENHO")) {
							linha.add(HtmlUtils.createLink("/relacao-empenhos?codigoProduto=" + linha.get(3) + "&op=&lote=&armazem=&emissao=&usuario=", rs.getBigDecimal(i).setScale(2, RoundingMode.HALF_EVEN)));
						} else if (rs.getMetaData().getColumnName(i).equals("QTD_SC")) {
							linha.add(HtmlUtils.createLink("/relacao-scs?abertos=true&codigoProduto=" + linha.get(3), rs.getBigDecimal(i).setScale(2, RoundingMode.HALF_EVEN)));
						} else if (rs.getMetaData().getColumnName(i).equals("QTD_PC")) {
							linha.add(HtmlUtils.createLink("/relacao-pcs?numero=&nomeFornecedor=&codigoFornecedor=&emissaoDe=&emissaoAte=&codigoProduto=" + linha.get(3) + "&op=", rs.getBigDecimal(i).setScale(2, RoundingMode.HALF_EVEN)));
						} else {
							linha.add(rs.getBigDecimal(i).setScale(2, RoundingMode.HALF_EVEN));
						}
					}
				}
				data.add(linha );
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new DataTable();
	}

}
