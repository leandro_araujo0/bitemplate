package br.com.BiTemplate.DAO;

import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import br.com.BiTemplate.enums.CLASPED;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.ChartDataAndLabels;
import br.com.BiTemplate.model.CheckListExpedicao;
import br.com.BiTemplate.model.Cliente;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.model.ItemPedidoVenda;
import br.com.BiTemplate.model.Produto;
import br.com.BiTemplate.utils.DataUtils;
import br.com.BiTemplate.utils.HtmlUtils;

public class FaturamentoDAO {
	
	private static final String _cfFaturamento = "AND ((D2_TES LIKE '5%') OR (D2_TES IN ('644', '649', '701', '702', '703')) OR (D2_CF IN ('5116', '6116', '6101', '5101', '6107', '5107', '6933', '5933')))\r\n";
	private static final String _D2_TIPO = "AND D2_TIPO IN ('N','C','P')\r\n";
	private static final String _joinSF4010 = "INNER JOIN SF4010 F4 WITH(NOLOCK) ON F4_FILIAL <> 'ZZ'\r\n" + 
			"AND F4_CODIGO = D2_TES\r\n" + 
			"AND F4.D_E_L_E_T_ = ''\r\n"; 
//			"AND F4_DUPLIC = 'S'\r\n";
	private static final String _joinSC5010 = "INNER JOIN SC5010 WITH(NOLOCK) ON D2_PEDIDO = C5_NUM\r\n" + 
			"AND C5_FILIAL <> 'ZZ'\r\n" + 
			"AND SC5010.D_E_L_E_T_  = ''\r\n" + 
			"AND C5_TIPO = 'N'\r\n" + 
			"AND C5_CLASPED IN ('1', '2', '3', '4', '6')\r\n";
	private static final String _joinSB1010 = "INNER JOIN SB1010 B1 WITH(NOLOCK) ON B1_FILIAL <> 'ZZ'\r\n" + 
			"AND B1_COD = D2_COD\r\n" + 
			"AND B1.D_E_L_E_T_ = ' '\r\n";
	private static final String _joinSA1010 = "INNER JOIN SA1010 A1 WITH(NOLOCK) ON A1_FILIAL <> 'ZZ'\r\n" + 
			"AND A1_COD = D2_CLIENTE\r\n" + 
			"AND A1_LOJA = D2_LOJA\r\n" + 
			"AND A1.D_E_L_E_T_ = ''\r\n";

	public static ChartDataAndLabels getFaturamentoAno(int ano, UNIDADE unidade) {
		String sql = "SELECT EMISSAO, SUM(CASE WHEN TIPO = 'FATURAMENTO' THEN VALOR ELSE -VALOR END) AS VALOR FROM (\r\n" +
				"SELECT SUBSTRING(D2_EMISSAO, 1, 6) AS EMISSAO, SUM(D2_VALBRUT) AS VALOR, 'FATURAMENTO' AS TIPO\r\n" + 
				"FROM SD2010 D2 WITH(NOLOCK)\r\n" + 
				_joinSB1010 + 
				_joinSA1010 +
				_joinSF4010 + 
				_joinSC5010 + 
				"WHERE D2_FILIAL <> 'ZZ'\r\n" + 
				"AND YEAR(D2_EMISSAO) = '" + ano + "'\r\n";
				sql += unidade.equals(UNIDADE.ORTOPEDIA)? "AND B1_LOCPAD IN ('01', '50', '97')\r\n" : (unidade.equals(UNIDADE.EQUIPAMENTOS)) ? "AND B1_LOCPAD IN ('11', '12', '14', '96')\r\n" : "";
				sql += "AND D2.D_E_L_E_T_ = ''\r\n" + 
				_D2_TIPO +
				_cfFaturamento +
				"GROUP BY SUBSTRING(D2_EMISSAO, 1, 6)\r\n" + 
				"UNION ALL\r\n" + 
				"SELECT SUBSTRING(D1_EMISSAO, 1, 6), SUM(D1_TOTAL) AS VALOR, 'DEVOLUCAO' AS TIPO\r\n" + 
				"FROM SD1010 WITH (NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON B1_FILIAL <> 'ZZ' AND B1_COD = D1_COD AND SD1010.D_E_L_E_T_ = ''\r\n" +
				"INNER JOIN SD2010 WITH(NOLOCK) ON D1_NFORI = D2_DOC AND SD2010.D_E_L_E_T_ = '' AND D1_ITEMORI = D2_ITEM\r\n" +
				"WHERE D1_FILIAL <> 'ZZ'\r\n" + 
				"AND YEAR(D1_EMISSAO) = '" + ano + "'\r\n" +
				_cfFaturamento +
				"AND SD1010.D_E_L_E_T_ = ''\r\n";
				sql += unidade.equals(UNIDADE.ORTOPEDIA)? "AND B1_LOCPAD IN ('01', '50', '97')\r\n" : (unidade.equals(UNIDADE.EQUIPAMENTOS)) ? "AND B1_LOCPAD IN ('11', '12', '14', '96')\r\n" : "";
				sql += "AND D1_TIPO = 'D'\r\n" + 
				"GROUP BY SUBSTRING(D1_EMISSAO, 1, 6)\r\n" +
				") AS RESULTSET\r\n" +
				"GROUP BY EMISSAO";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("EMISSAO"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getFaturamentoAno(ano, unidade);
			} else {
				e.printStackTrace();
			}
		}

		return null;
	}


	public static ChartDataAndLabels getFaturamentoMensal() {
		String sql = "SELECT CLASPED, SUM(VALOR) AS VALOR FROM (\r\n" + 
				"SELECT\r\n" + 
				"CASE C5_CLASPED WHEN '1' THEN 'REPOSICAO' WHEN '2' THEN 'CAIXAS' WHEN '3' THEN 'EQUIPAMENTOS' WHEN '4' THEN 'EXPORTACAO' WHEN '6' THEN 'EQUIPAMENTOS' END AS CLASPED, D2_VALBRUT AS VALOR\r\n" + 
				"FROM SD2010 D2 WITH(NOLOCK)\r\n" + 
				_joinSB1010 +
				_joinSA1010 +
				_joinSF4010 +
				_joinSC5010 + 
				"AND SC5010.D_E_L_E_T_  = ''\r\n" + 
				"WHERE D2_FILIAL <> 'ZZ'\r\n" + 
				"AND YEAR(D2_EMISSAO) = YEAR(GETDATE())\r\n" + 
				"AND MONTH(D2_EMISSAO) = MONTH(GETDATE())\r\n" + 
				"AND D2.D_E_L_E_T_ = ''\r\n" + 
				_D2_TIPO + 
				_cfFaturamento +
				") AS RESULTSET\r\n" + 
				"GROUP BY CLASPED\r\n" + 
				"ORDER BY CLASPED";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("CLASPED"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getFaturamentoMensal();
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ChartDataAndLabels getFaturamentoDiario(UNIDADE unidade, CLASPED clasped) {
		String sql = "SELECT DIA, SUM(VALOR) AS VALOR FROM (\r\n" + 
				"SELECT\r\n" + 
				"DAY(D2_EMISSAO) AS DIA, D2_VALBRUT AS VALOR\r\n" + 
				"FROM SD2010 D2 WITH(NOLOCK)\r\n" + 
				_joinSB1010 +
				_joinSA1010 +
				_joinSF4010 +
				_joinSC5010 + 
				"WHERE D2_FILIAL <> 'ZZ'\r\n" + 
				"AND YEAR(D2_EMISSAO) = YEAR(GETDATE())\r\n" + 
				"AND MONTH(D2_EMISSAO) = MONTH(GETDATE())\r\n" +
				"AND C5_CLASPED = ?\r\n" +
				"AND D2.D_E_L_E_T_ = ''\r\n" + 
				_D2_TIPO +
				_cfFaturamento +
				") AS RESULTSET\r\n" + 
				"GROUP BY DIA\r\n" + 
				"ORDER BY DIA";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setString(1, clasped.getNumber());
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("DIA"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getFaturamentoDiario(unidade, clasped);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}


	public static ChartDataAndLabels getFaturamentoHorizontal(int ano) {
		String sql = "SELECT\r\n" + 
				"CASE B1_LOCPAD WHEN '01' THEN 'ORTOPEDIA' WHEN '11' THEN 'EQUIPAMENTOS' WHEN '12' THEN 'EQUIPAMENTOS' WHEN '14' THEN 'EQUIPAMENTOS' WHEN '50' THEN 'ORTOPEDIA' END AS UNIDADE, SUM(D2_VALBRUT) AS VALOR\r\n" + 
				"FROM SD2010 D2 WITH(NOLOCK)\r\n" + 
				_joinSB1010 +
				_joinSA1010 +
				_joinSF4010 +
				_joinSC5010 + 
				"WHERE D2_FILIAL <> 'ZZ'\r\n" + 
				"AND YEAR(D2_EMISSAO) = ?\r\n" + 
				"AND B1_LOCPAD IN ('01', '11', '12', '14', '50')\r\n" + 
				"AND D2.D_E_L_E_T_ = ''\r\n" +
				_D2_TIPO + 
				_cfFaturamento +
				"GROUP BY CASE B1_LOCPAD WHEN '01' THEN 'ORTOPEDIA' WHEN '11' THEN 'EQUIPAMENTOS' WHEN '12' THEN 'EQUIPAMENTOS' WHEN '14' THEN 'EQUIPAMENTOS' WHEN '50' THEN 'ORTOPEDIA' END";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setLong(1, ano);
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("UNIDADE"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getFaturamentoHorizontal(ano);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}


	public static ChartDataAndLabels getFaturamentoHorizontal(int ano, UNIDADE unidade) {
		String sql = "SELECT\r\n" + 
				"'EMISSAO' AS EMISSAO, SUM(D2_VALBRUT) AS VALOR\r\n" + 
				"FROM SD2010 D2 WITH(NOLOCK)\r\n" + 
				_joinSB1010 +
				_joinSA1010 +
				_joinSF4010 +
				_joinSC5010 + 
				"WHERE D2_FILIAL <> 'ZZ'\r\n" + 
				"AND YEAR(D2_EMISSAO) = ?\r\n";
				if (unidade.equals(UNIDADE.ORTOPEDIA)) {
					sql += "AND B1_LOCPAD IN ('01', '50')\r\n";
				} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
					sql += "AND B1_LOCPAD IN ('11', '12', '14', '50')\r\n";
				}
				sql += "AND B1_LOCPAD IN ('01', '11', '12', '14', '50')\r\n" +
				"AND D2.D_E_L_E_T_ = ''\r\n" + 
				_D2_TIPO + 
				_cfFaturamento +
				"GROUP BY CASE B1_LOCPAD WHEN '01' THEN 'ORTOPEDIA' WHEN '11' THEN 'EQUIPAMENTOS' WHEN '12' THEN 'EQUIPAMENTOS' WHEN '14' THEN 'EQUIPAMENTOS' WHEN '50' THEN 'ORTOPEDIA' END";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setLong(1, ano);
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("EMISSAO"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getFaturamentoHorizontal(ano, unidade);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}


	public static ChartDataAndLabels getHistoricoFaturamento(Cliente cliente) {
		String sql = "SELECT\r\n" + 
				"SUBSTRING(D2_EMISSAO, 1, 6) AS EMISSAO, SUM(D2_VALBRUT) AS VALOR\r\n" + 
				"FROM SD2010 D2 WITH(NOLOCK)\r\n" + 
				_joinSB1010 +
				_joinSA1010 +
				_joinSF4010 +
				_joinSC5010 + 
				"WHERE D2_FILIAL <> 'ZZ'\r\n" + 
				"AND D2_EMISSAO >=	DATEADD(YEAR, -3, GETDATE())\r\n" + 
				"AND D2.D_E_L_E_T_ = ''\r\n" + 
				_D2_TIPO + 
				"AND D2_CLIENTE = ?\r\n" + 
				_cfFaturamento +
				"GROUP BY SUBSTRING(D2_EMISSAO, 1, 6)\r\n" + 
				"ORDER BY EMISSAO";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setString(1, cliente.getCodigo());
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("EMISSAO"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getHistoricoFaturamento(cliente);
			} else {
				e.printStackTrace();
			}
		}

		return null;
	}


	public static DataTable getPedidosFaturados(CLASPED clasped, UNIDADE unidade, String dia, String mes, String ano, String dataDe, String dataAte, String codigoCliente, String descricaoProduto,
			String lote, String nomeCliente, String naturezaCliente, String pedidoDe, String pedidoAte, String nfInicial, String nfFinal, String produto) {
		
		
		String sql = "SELECT\r\n" + 
				"'FATURAMENTO' AS TIPO, D2_EMISSAO, D2_COD, B1_DESC, D2_CF, B1_LOCPAD, D2_QUANT, D2_PEDIDO, D2_CLIENTE, A1_NOME, A1_EST, D2_VALBRUT, D2_DOC, C5_MENNOTA, D2_LOTECTL, D2_PRCVEN, BM_DESC AS GRUPO, LEFT(D2_EMISSAO, 4) AS ANO, SUBSTRING(D2_EMISSAO, 5, 2) AS MES\r\n" + 
				"FROM SD2010 D2 WITH(NOLOCK)\r\n" + 
				_joinSB1010 +
				_joinSA1010 +
				_joinSF4010 +
				_joinSC5010 + 
				"INNER JOIN SBM010 WITH(NOLOCK) ON SBM010.D_E_L_E_T_ = '' AND BM_FILIAL <> 'ZZ' AND B1_GRUPO = BM_GRUPO\r\n" +
				"WHERE D2_FILIAL <> 'ZZ'\r\n";
				if (clasped != null) {
					sql += "AND C5_CLASPED = " + clasped.getNumber() + "\r\n";
				}
				if (ano != null && !ano.equals("")) {
					sql += "AND YEAR(D2_EMISSAO) = " + ano + "\r\n";
				}
				if (mes != null && !mes.equals("")) {
					sql += "AND MONTH(D2_EMISSAO) = " + DataUtils.getMes(mes) + "\r\n";
				}
				if (dia != null && !dia.equals("")) {
					sql += "AND DAY(D2_EMISSAO) = " + dia + "\r\n";
				}
				if (dataDe != null && !dataDe.equals("")) {
					sql += "AND D2_EMISSAO >= '" + dataDe + "'\r\n";
				}
				if (dataAte != null && !dataAte.equals("")) {
					sql += "AND D2_EMISSAO <= '" + dataAte + "'\r\n";
				}
				if (codigoCliente != null && !codigoCliente.equals("")) {
					sql += "AND A1_COD = '" + codigoCliente + "'\r\n";
				}
				if (unidade != null) {
					sql += (unidade.equals(UNIDADE.ORTOPEDIA)) ? "AND B1_LOCPAD = '01'\r\n" : (unidade.equals(UNIDADE.EQUIPAMENTOS)) ? "AND B1_LOCPAD IN ('11', '12', '14')\r\n" : "";
				}
				if (descricaoProduto != null && !descricaoProduto.equals("")) {
					sql += "AND B1_DESC LIKE '%" + descricaoProduto + "%'\r\n";
				}
				if (lote != null && !lote.equals("")) {
					sql += "AND D2_LOTECTL = '" + lote + "'\r\n";
				}
				if (nomeCliente != null && !nomeCliente.equals("")) {
					sql += "AND (A1_NOME LIKE '%" + nomeCliente + "%' OR A1_NREDUZ LIKE '%" + nomeCliente + "%')\r\n";
				}
				if (naturezaCliente != null && !naturezaCliente.equals("")) {
					sql += "AND A1_NATUREZ = '" + naturezaCliente + "'\r\n";
				}
				if (pedidoDe != null && !pedidoDe.equals("")) {
					sql += "AND C5_NUM >= '" + pedidoDe + "'\r\n";
				}
				if (pedidoAte != null && !pedidoAte.equals("")) {
					sql += "AND C5_NUM <= '" + pedidoAte + "'\r\n";
				}
				if (nfInicial != null && !nfInicial.equals("")) {
					sql += "AND D2_DOC >= '" + nfInicial + "'\r\n";
				}
				if (nfFinal != null && !nfFinal.equals("")) {
					sql += "AND D2_DOC <= '" + nfFinal + "'\r\n";
				}
				if (produto != null && !produto.equals("")) {
					if (produto.replace(" ", ";").contains(";")) {
						String[] produtos = produto.replace(" ", ";").split(";");
						for (int i = 0; i < produtos.length; i++) {
							if (i == 0) {
								sql += "AND (";
							} else {
								sql += "OR ";
							}
							sql += "D2_COD LIKE '" + produtos[i] + "%'\r\n";
						}
						sql += ")\r\n";
					} else {
						sql += "AND D2_COD LIKE '" + produto + "%'\r\n";
					}
				}
				sql += "AND D2.D_E_L_E_T_ = ''\r\n" + 
				_cfFaturamento +
				_D2_TIPO +
				"UNION ALL\r\n" + 
				"SELECT\r\n" + 
				"	'DEVOLUCAO' AS TIPO, D1_EMISSAO, D1_COD, B1_DESC, D1_CF, B1_LOCPAD, -D1_QUANT, D2_PEDIDO, D2_CLIENTE, A1_NOME, A1_EST, -(D1_TOTAL - D1_VALDESC) AS D1_TOTAL, D1_DOC, C5_MENNOTA, D1_LOTECTL, D2_PRCVEN, BM_DESC AS GRUPO, LEFT(D1_EMISSAO, 4) AS ANO, SUBSTRING(D1_EMISSAO, 5, 2) AS MES\r\n" + 
				"FROM SD1010 WITH (NOLOCK)\r\n" + 
				"INNER JOIN SD2010 WITH (NOLOCK) ON D1_NFORI = D2_DOC AND SD2010.D_E_L_E_T_ = '' AND D1_ITEMORI = D2_ITEM AND D2_TIPO = 'N'\r\n" +
				"INNER JOIN SC6010 WITH (NOLOCK) ON SC6010.D_E_L_E_T_ = '' AND C6_FILIAL <> 'ZZ' AND D2_PEDIDO = C6_NUM AND D2_ITEMPV = C6_ITEM\r\n" +
				"INNER JOIN SC5010 WITH (NOLOCK) ON SC5010.D_E_L_E_T_ = '' AND C5_FILIAL <> 'ZZ' AND C5_NUM = C6_NUM\r\n" +
				"INNER JOIN SB1010 WITH(NOLOCK) ON D1_COD = B1_COD AND SB1010.D_E_L_E_T_ = '' AND B1_FILIAL <> 'ZZ'\r\n" +
				"INNER JOIN SA1010 WITH(NOLOCK) ON SA1010.D_E_L_E_T_ = '' AND A1_FILIAL <> 'ZZ' AND A1_COD = D2_CLIENTE AND A1_LOJA = D2_LOJA\r\n" +
				"INNER JOIN SBM010 WITH(NOLOCK) ON SBM010.D_E_L_E_T_ = '' AND BM_FILIAL <> 'ZZ' AND B1_GRUPO = BM_GRUPO\r\n" +
				"WHERE\r\n" + 
				"SD1010.D_E_L_E_T_ = ''\r\n" +
				_cfFaturamento;
				if (clasped != null) {
					sql += "AND C5_CLASPED = " + clasped.getNumber() + "\r\n";
				}
				if (ano != null && !ano.equals("")) {
					sql += "AND YEAR(D1_EMISSAO) = " + ano + "\r\n";
				}
				if (mes != null && !mes.equals("")) {
					sql += "AND MONTH(D1_EMISSAO) = " + DataUtils.getMes(mes) + "\r\n";
				}
				if (dia != null && !dia.equals("")) {
					sql += "AND DAY(D1_EMISSAO) = " + dia + "\r\n";
				}
				if (dataDe != null && !dataDe.equals("")) {
					sql += "AND D1_EMISSAO >= '" + dataDe + "'\r\n";
				}
				if (dataAte != null && !dataAte.equals("")) {
					sql += "AND D1_EMISSAO <= '" + dataAte + "'\r\n";
				}
				if (unidade != null) {
					sql += (unidade.equals(UNIDADE.ORTOPEDIA)) ? "AND B1_LOCPAD = '01'\r\n" : (unidade.equals(UNIDADE.EQUIPAMENTOS)) ? "AND B1_LOCPAD IN ('11', '12', '14')\r\n" : "";
				}
				if (codigoCliente != null && !codigoCliente.equals("")) {
					sql += "AND A1_COD = '" + codigoCliente + "'\r\n";
				}
				if (descricaoProduto != null && !descricaoProduto.equals("")) {
					sql += "AND B1_DESC LIKE '%" + descricaoProduto + "%'\r\n";
				}
				if (lote != null && !lote.equals("")) {
					sql += "AND D1_LOTECTL = '" + lote + "'\r\n";
				}
				if (nomeCliente != null && !nomeCliente.equals("")) {
					sql += "AND (A1_NOME LIKE '%" + nomeCliente + "%' OR A1_NREDUZ LIKE '%" + nomeCliente + "%')\r\n";
				}
				if (naturezaCliente != null && !naturezaCliente.equals("")) {
					sql += "AND A1_NATUREZ = '" + naturezaCliente + "'\r\n";
				}
				if (pedidoDe != null && !pedidoDe.equals("")) {
					sql += "AND C5_NUM >= '" + pedidoDe + "'\r\n";
				}
				if (pedidoAte != null && !pedidoAte.equals("")) {
					sql += "AND C5_NUM <= '" + pedidoAte + "'\r\n";
				}
				if (nfInicial != null && !nfInicial.equals("")) {
					sql += "AND D2_DOC >= '" + nfInicial + "'\r\n";
				}
				if (nfFinal != null && !nfFinal.equals("")) {
					sql += "AND D2_DOC <= '" + nfFinal + "'\r\n";
				}
				if (produto != null && !produto.equals("")) {
					if (produto.replace(" ", ";").contains(";")) {
						String[] produtos = produto.replace(" ", ";").split(";");
						for (int i = 0; i < produtos.length; i++) {
							if (i == 0) {
								sql += "AND (";
							} else {
								sql += "OR ";
							}
							sql += "D1_COD LIKE '" + produtos[i] + "%'\r\n";
						}
						sql += ")\r\n";
					} else {
						sql += "AND D1_COD LIKE '" + produto + "%'\r\n";
					}
				}
				sql += "AND D1_FILIAL <> 'ZZ'\r\n" + 
				"AND D1_TIPO = 'D'\r\n" + 
				"ORDER BY 1";
				
				try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
					ResultSet rs = stmt.executeQuery();
					DataTable dataTable = new DataTable();
					Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
					while (rs.next()) {
						Collection<Object> linha = new LinkedList<Object>();
						linha.add(rs.getString("TIPO"));
						linha.add(rs.getString("D2_EMISSAO"));
						linha.add(HtmlUtils.createLink("/pedidos-venda/" + rs.getString("D2_PEDIDO") + "/detalhes", rs.getString("D2_PEDIDO")));
						linha.add(rs.getString("D2_DOC"));
						linha.add(rs.getString("D2_CLIENTE"));
						linha.add(rs.getString("A1_NOME"));
						linha.add(rs.getString("D2_COD"));
						linha.add(rs.getString("B1_DESC"));
						linha.add(rs.getString("D2_LOTECTL"));
						linha.add(rs.getBigDecimal("D2_QUANT"));
						linha.add(rs.getBigDecimal("D2_PRCVEN"));
						linha.add(rs.getBigDecimal("D2_VALBRUT"));
						linha.add(rs.getString("A1_EST"));
						linha.add(rs.getString("B1_LOCPAD"));
						linha.add(rs.getString("D2_CF"));
						linha.add(rs.getString("C5_MENNOTA"));
						linha.add(rs.getString("GRUPO"));
						linha.add(rs.getString("ANO"));
						linha.add(rs.getString("MES"));
						data.add(linha);
					}
					dataTable.setData(data);
					return dataTable;
				} catch (SQLException e) {
					e.printStackTrace();
				}
		return null;
	}


	public static DataTable getRelacaoFaturamento(String produto, String descricao, String codigoCliente,
			String nomeCliente, String dataDe, String dataAte, String lote, String pedidoDe, String pedidoAte, String nfInicial, String nfFinal, String naturezaCliente) {
		String sql = "SELECT\r\n" + 
				"D2_COD,B1_DESC,D2_LOCAL,D2_QUANT,D2_PRCVEN,D2_VALBRUT,D2_EMISSAO,D2_DOC,A1_NOME,A1_COD,D2_CF,D2_LOTECTL,D2_PEDIDO\r\n" + 
				"FROM SD2010 D2 WITH(NOLOCK)\r\n" + 
				_joinSB1010 + 
				_joinSA1010 +
				_joinSF4010 + 
				_joinSC5010 + 
				"WHERE D2_FILIAL <> 'ZZ'\r\n" + 
				"AND D2.D_E_L_E_T_ = ''\r\n" + 
				_D2_TIPO +
				_cfFaturamento;
				if (produto != null && !produto.equals("")) {
					produto = produto.replace(" ", ";");
					if (produto.contains(";")) {
						for (int i = 0; i < produto.split(";").length; i++) {
							if (i == 0) {
								sql += "AND (\r\n";
							} else {
								sql += "OR \r\n";
							}
							sql += "D2_COD LIKE '" + produto.split(";")[i] + "%'\r\n";
						}
						sql += ")";
					} else {
						sql += "AND D2_COD LIKE '" + produto + "%'\r\n";
					}
				}
				if (descricao != null && !descricao.equals("")) {
					sql += "AND B1_DESC LIKE '%" + descricao + "%'\r\n";
				}
				if (codigoCliente != null && !codigoCliente.equals("")) {
					sql += "AND D2_CLIENTE = '" + codigoCliente + "'\r\n";
				}
				if (nomeCliente != null && !nomeCliente.equals("")) {
					sql += "AND A1_NOME LIKE '" + nomeCliente + "%'\r\n";
				}
				if (dataDe != null && !dataDe.equals("")) {
					sql += "AND D2_EMISSAO >= '" + dataDe + "'\r\n";
				}
				if (dataAte != null && !dataAte.equals("")) {
					sql += "AND D2_EMISSAO <= '" + dataAte + "'\r\n";
				}
				if (lote != null && !lote.equals("")) {
					if (lote.contains(";")) {
						for (int i = 0; i < lote.split(";").length; i++) {
							if (i == 0) {
								sql += "AND (\r\n";
							} else {
								sql += "OR \r\n";
							}
							sql += "D2_LOTECTL = '" + lote.split(";")[i] + "'\r\n";
						}
						sql += ")";
					} else {
						sql += "AND D2_LOTECTL = '" + lote + "'\r\n";
					}
				}
				if (pedidoDe != null && !pedidoDe.equals("")) {
					sql += "AND D2_PEDIDO >= '" + pedidoDe + "'\r\n";
				}
				if (pedidoAte != null && !pedidoAte.equals("")) {
					sql += "AND D2_PEDIDO <= '" + pedidoAte + "'\r\n";
				}
				if (nfInicial != null && !nfInicial.equals("")) {
					sql += "AND D2_DOC >= '" + nfInicial + "'\r\n";
				}
				if (nfFinal != null && !nfFinal.equals("")) {
					sql += "AND D2_DOC <= '" + nfFinal + "'\r\n";
				}
				if (naturezaCliente != null && !naturezaCliente.equals("")) {
					sql += "AND A1_NATUREZ = '" + naturezaCliente + "'\r\n";
				}
				sql += "ORDER BY D2_EMISSAO";
				
		
		Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				linha.add(rs.getString("D2_COD"));
				linha.add(rs.getString("B1_DESC"));
				linha.add(rs.getString("D2_LOCAL"));
				linha.add(rs.getString("D2_QUANT"));
				linha.add(rs.getString("D2_PRCVEN"));
				linha.add(rs.getString("D2_VALBRUT"));
				linha.add(rs.getString("D2_LOTECTL"));
				linha.add(rs.getString("D2_EMISSAO"));
				linha.add(rs.getString("D2_PEDIDO"));
				linha.add(rs.getString("D2_DOC"));
				linha.add(rs.getString("A1_COD"));
				linha.add(rs.getString("A1_NOME"));
				linha.add(rs.getString("D2_CF"));
				data.add(linha);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
//		public static DataTable getRelacaoFaturamento(String produto, String descricao, String codigoCliente,
//				String nomeCliente, String dataDe, String dataAte, String lote, String pedidoDe, String pedidoAte, String nfInicial, String nfFinal) {
		
		sql = "SELECT\r\n" + 
				"	'DEVOLUCAO' AS TIPO, D1_EMISSAO, D1_COD, B1_DESC, D1_CF, B1_LOCPAD, -D1_QUANT, '' AS D2_PEDIDO, D1_FORNECE, A2_NOME, D1_TOTAL, D1_DOC\r\n" + 
				"FROM SD1010 WITH (NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH (NOLOCK) ON B1_FILIAL <> 'ZZ' AND B1_COD = D1_COD AND SD1010.D_E_L_E_T_ = ''\r\n" + 
				"INNER JOIN SA1010 WITH (NOLOCK) ON A1_FILIAL <> 'ZZ' AND A1_COD = D1_FORNECE AND A1_LOJA = D1_LOJA AND SA1010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE D1_FILIAL <> 'ZZ'\r\n";
		if (produto != null && !produto.equals("")) {
			sql += "AND D1_COD = '" + produto + "'\r\n";
		}
		if (descricao != null && !descricao.equals("")) {
			sql += "AND B1_DESC LIKE '%" + descricao + "%'\r\n";
		}
		if (codigoCliente != null && !codigoCliente.equals("")) {
			sql += "AND A1_COD = '" + codigoCliente + "'\r\n";
		}
		if (nomeCliente != null && !nomeCliente.equals("")) {
			sql += "AND A1_NOME LIKE '%" + nomeCliente + "%'\r\n";
		}
		if (dataDe != null && !dataDe.equals("")) {
			sql += "AND D1_EMISSAO >= '" + dataDe + "'\r\n";
		}
		if (dataAte != null && !dataAte.equals("")) {
			sql += "AND D1_EMISSAO <= '" + dataAte + "'\r\n";
		}
		if (lote != null && !lote.equals("")) {
			sql += "AND D1_LOTECTL = '" + lote + "'\r\n";
		}
		if (pedidoDe != null && !pedidoDe.equals("")) {
			sql += "AND D1_PEDIDO >= '" + pedidoDe + "'\r\n";
		}
		if (pedidoAte != null && !pedidoAte.equals("")) {
			sql += "AND D1_PEDIDO <= '" + pedidoAte + "'\r\n";
		}
		if (nfInicial != null && !nfInicial.equals("")) {
			sql += "AND D1_DOC >= '" + nfInicial + "'\r\n";
		}
		if (nfFinal != null && !nfFinal.equals("")) {
			sql += "AND D1_DOC <= '" + nfFinal + "'\r\n";
		}
		sql += "	AND YEAR(D1_EMISSAO) = '2019'\r\n" + 
		"	AND SD1010.D_E_L_E_T_ = ''\r\n" + 
		"	AND B1_LOCPAD IN ('11', '12', '14', '96')\r\n" + 
		"	AND D1_TIPO = 'D'";
		
//		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
//			ResultSet rs = stmt.executeQuery();
//			while (rs.next()) {
//				Collection<Object> linha = new LinkedList<Object>();
//				linha.add(rs.getString("D1_COD"));
//				linha.add(rs.getString("B1_DESC"));
//				linha.add(rs.getString("D1_LOCAL"));
//				linha.add(rs.getString("D1_QUANT"));
//				linha.add(rs.getString("D1_VUNIT"));
//				linha.add(rs.getString("D1_TOTAL"));
//				linha.add(rs.getString("D1_LOTECTL"));
//				linha.add(rs.getString("D1_EMISSAO"));
//				linha.add(rs.getString("D1_PEDIDO"));
//				linha.add(rs.getString("D1_DOC"));
//				linha.add(rs.getString("A1_COD"));
//				linha.add(rs.getString("A1_NOME"));
//				linha.add(rs.getString("D1_CF"));
//				data.add(linha);
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
 		return new DataTable(data);
	}


	public static LinkedList<String> getNfsPedido(String numPedido) {
		String sql = "SELECT DISTINCT\r\n" + 
				"D2_DOC\r\n" + 
				"FROM SD2010 WITH(NOLOCK)\r\n" + 
				"WHERE SD2010.D_E_L_E_T_ = ''\r\n" + 
				"AND D2_FILIAL <> 'ZZ'\r\n" + 
				"AND D2_PEDIDO = '" + numPedido + "'";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<String> nfsPedido = new LinkedList<String>();
			while (rs.next()) {
				nfsPedido.add(rs.getString(1));
			}
			return nfsPedido;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}


	public static CheckListExpedicao getCheckList(String documento, String numPedido) {
		String sql = "";
		
		if (documento != null && !documento.equals("")) {
			sql += "SELECT\r\n" + 
					"A1_NOME, D2_COD, B1_DESC, C6_XDESCRI, D2_LOTECTL, D2_QUANT, D2_PEDIDO\r\n" + 
					"FROM SD2010 WITH(NOLOCK)\r\n" + 
					"INNER JOIN SB1010 WITH (NOLOCK) ON D2_COD = B1_COD AND B1_FILIAL <> 'ZZ' AND SB1010.D_E_L_E_T_ = ''\r\n" + 
					"INNER JOIN SA1010 WITH (NOLOCK) ON D2_CLIENTE = A1_COD AND SA1010.D_E_L_E_T_ = '' AND A1_FILIAL <> 'ZZ'\r\n" + 
					"INNER JOIN SC6010 WITH (NOLOCK) ON D2_PEDIDO = C6_NUM AND SC6010.D_E_L_E_T_ = '' AND C6_FILIAL <> 'ZZ' AND D2_ITEMPV = C6_ITEM\r\n" + 
					"WHERE SD2010.D_E_L_E_T_ = ''\r\n" + 
					"AND D2_FILIAL <> 'ZZ'\r\n" + 
					"AND D2_DOC = '" + documento + "'";
		} else {
			sql += "SELECT\r\n" + 
					"A1_NOME, C6_PRODUTO, B1_DESC, C6_XDESCRI, C9_LOTECTL, C9_QTDLIB, C6_NUM\r\n" + 
					"FROM SC6010 WITH (NOLOCK)\r\n" + 
					"INNER JOIN SC5010 WITH (NOLOCK) ON C6_NUM = C5_NUM AND SC5010.D_E_L_E_T_ = '' AND C5_FILIAL <> 'ZZ'\r\n" + 
					"INNER JOIN SA1010 WITH (NOLOCK) ON C5_CLIENTE = A1_COD AND C5_LOJACLI = A1_LOJA AND SA1010.D_E_L_E_T_ = '' AND A1_FILIAL <> 'ZZ'\r\n" + 
					"INNER JOIN SB1010 WITH (NOLOCK) ON C6_PRODUTO = B1_COD AND B1_FILIAL <> 'ZZ' AND SB1010.D_E_L_E_T_ = ''\r\n" + 
					"INNER JOIN SC9010 WITH (NOLOCK) ON C9_PEDIDO = C6_NUM AND C9_ITEM = C6_ITEM AND SC9010.D_E_L_E_T_ = '' AND C9_FILIAL <> 'ZZ'\r\n" + 
					"WHERE\r\n" + 
					"SC6010.D_E_L_E_T_ = ''\r\n" + 
					"AND C6_FILIAL <> 'ZZ'\r\n" + 
					"AND C6_NUM = '" + numPedido + "'";
		}
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			CheckListExpedicao cle = new CheckListExpedicao();
			if (documento != null && !documento.equals("")) {
				while (rs.next()) {
					cle.setCliente(rs.getString("A1_NOME"));
					ItemPedidoVenda itemPedido = new ItemPedidoVenda();
					itemPedido.setProduto(new Produto().setCodigo(rs.getString("D2_COD")).setDescricao(rs.getString("B1_DESC")));
					itemPedido.setQtdVen(rs.getBigDecimal("D2_QUANT"));
					itemPedido.setXDesc(rs.getString("C6_XDESCRI"));
					itemPedido.setLote(rs.getString("D2_LOTECTL"));
					cle.setNf(documento);
					cle.setPedido(rs.getString("D2_PEDIDO"));
					cle.add(itemPedido);
				}
			} else {
				while (rs.next()) {
					cle.setCliente(rs.getString("A1_NOME"));
					ItemPedidoVenda itemPedido = new ItemPedidoVenda();
					itemPedido.setProduto(new Produto().setCodigo(rs.getString("C6_PRODUTO")).setDescricao(rs.getString("B1_DESC")));
					itemPedido.setQtdVen(rs.getBigDecimal("C9_QTDLIB"));
					itemPedido.setXDesc(rs.getString("C6_XDESCRI"));
					itemPedido.setLote(rs.getString("C9_LOTECTL"));
					cle.setNf(documento);
					cle.setPedido(rs.getString("C6_NUM"));
					cle.add(itemPedido);
				}
			}
			return cle;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
