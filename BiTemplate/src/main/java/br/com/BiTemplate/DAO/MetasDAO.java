package br.com.BiTemplate.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import br.com.BiTemplate.model.DataTable;

public class MetasDAO {

	public static Collection<String> getCabecalhoTabelaVendas() {
		String sql = "SELECT TOP 1 * FROM SZR010 WITH(NOLOCK) WHERE SZR010.D_E_L_E_T_ = ''";
		
		try (PreparedStatement stmt = Database.getConnectionTeste().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<String> cabecalho = new LinkedList<String>();
			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				cabecalho.add(rs.getMetaData().getColumnName(i));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static DataTable getTabelaMetaVendas() {
		String sql = "SELECT * FROM SZR010 WITH(NOLOCK) WHERE SZR010.D_E_L_E_T_ = ''";
		try (PreparedStatement stmt = Database.getConnectionTeste().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					linha.add(rs.getString(i));
				}
				data.add(linha);
			}
			return new DataTable().setData(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
