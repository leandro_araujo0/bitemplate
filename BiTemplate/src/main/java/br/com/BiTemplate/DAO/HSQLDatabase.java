package br.com.BiTemplate.DAO;

import java.sql.Connection;
import java.sql.SQLException;

public class HSQLDatabase {

	private static final String url = "jdbc:hsqldb:hsql://localhost/ortosintese";
	private static final String user = "SA";
	private static final String password = "";

	public static Connection getConnection() {
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			return java.sql.DriverManager.getConnection(url, user, password);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
