package br.com.BiTemplate.DAO;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import be.ceau.chart.LineChart;
import be.ceau.chart.color.Color;
import be.ceau.chart.data.LineData;
import be.ceau.chart.dataset.LineDataset;
import br.com.BiTemplate.enums.TipoRelatorioRecebimento;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.ChartDataAndLabels;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.utils.CurrencyUtils;

public class FinanceiroDAO {
	
	private static final String condicaoNatureza = "AND (E5_NATUREZ NOT IN ('5100','5200','5300', '5503') AND E5_NATUREZ NOT LIKE '55%')\r\n";

	public static ChartDataAndLabels getBaixas(int ano, UNIDADE unidade) {
		//TODO Refazer rotina para otimizar a query
		
		
		String sql = "SELECT\r\n" + "SUBSTRING(E5_DATA, 1, 6) AS EMISSAO,\r\n"
				+ "SUM(CASE E5_RECPAG WHEN 'P' THEN E5_VALOR WHEN 'R' THEN -E5_VALOR END) AS VALOR\r\n"
				+ "FROM SE5010 WITH(NOLOCK)\r\n"
				+ "LEFT JOIN SE2010 WITH(NOLOCK) ON E2_NUM = E5_NUMERO\r\n"
				+ "AND E2_PARCELA = E5_PARCELA\r\n"
				+ "AND E2_FORNECE = E5_CLIFOR\r\n"
				+ "AND SE2010.D_E_L_E_T_ = ''\r\n"
				+ "AND E2_PREFIXO = E5_PREFIXO\r\n"
				+ "WHERE\r\n" + "YEAR(E5_DATA) >= ?\r\n"
				+ "AND SE5010.D_E_L_E_T_ = ''\r\n"
				+ "AND (E5_RECPAG = 'P' OR (E5_RECPAG = 'R' AND E5_TIPODOC = 'ES'))\r\n"
				+ condicaoNatureza
				+ "AND E5_TIPODOC NOT IN ('DC','JR')\r\n"
//				+ "AND E5_RECONC = 'x'\r\n"
				+ "AND E5_BANCO <> ''\r\n";
		if (unidade == null) {
			sql += "AND CASE WHEN E5_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E5_XUNID = 'O' THEN 'ORTOPEDIA' WHEN E2_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E2_XUNID = 'O' THEN 'ORTOPEDIA'" +
					"ELSE (SELECT TOP 1 CASE WHEN E5.E5_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E5.E5_XUNID = 'O' THEN 'ORTOPEDIA' WHEN E2.E2_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E2.E2_XUNID = 'O' THEN 'ORTOPEDIA' ELSE 'N_CLASS' END FROM SE5010 E5 WITH(NOLOCK) LEFT JOIN SE2010 E2 WITH(NOLOCK) ON E5.E5_NUMERO = E2.E2_NUM AND E5.E5_TIPO = E2.E2_TIPO AND E5.E5_FORNECE = E2.E2_FORNECE AND E5.E5_PREFIXO = E2.E2_PREFIXO WHERE E5.E5_PREFIXO = SUBSTRING(SE2010.E2_TITPAI, 1, 3)" +
					"AND E5.E5_NUMERO = SUBSTRING(SE2010.E2_TITPAI, 4, 11) AND E5.E5_FORNECE = SUBSTRING(SE2010.E2_TITPAI, 18, 6) AND E5.E5_LOJA = SUBSTRING(SE2010.E2_TITPAI, 24, 2) AND E5.E5_TIPO = SUBSTRING(SE2010.E2_TITPAI, 15, 3))" +
					"END = 'N_CLASS'\r\n";
		} else if (unidade.equals(UNIDADE.ORTOPEDIA)) {
			sql += "AND CASE WHEN E5_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E5_XUNID = 'O' THEN 'ORTOPEDIA' WHEN E2_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E2_XUNID = 'O' THEN 'ORTOPEDIA'" +
					"ELSE (SELECT TOP 1 CASE WHEN E5.E5_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E5.E5_XUNID = 'O' THEN 'ORTOPEDIA' WHEN E2.E2_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E2.E2_XUNID = 'O' THEN 'ORTOPEDIA' ELSE 'N_CLASS' END FROM SE5010 E5 WITH(NOLOCK) LEFT JOIN SE2010 E2 WITH(NOLOCK) ON E5.E5_NUMERO = E2.E2_NUM AND E5.E5_TIPO = E2.E2_TIPO AND E5.E5_FORNECE = E2.E2_FORNECE AND E5.E5_PREFIXO = E2.E2_PREFIXO WHERE E5.E5_PREFIXO = SUBSTRING(SE2010.E2_TITPAI, 1, 3)" +
					"AND E5.E5_NUMERO = SUBSTRING(SE2010.E2_TITPAI, 4, 11) AND E5.E5_FORNECE = SUBSTRING(SE2010.E2_TITPAI, 18, 6) AND E5.E5_LOJA = SUBSTRING(SE2010.E2_TITPAI, 24, 2) AND E5.E5_TIPO = SUBSTRING(SE2010.E2_TITPAI, 15, 3))" +
					"END = 'ORTOPEDIA'\r\n";
		} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
			sql += "AND CASE WHEN E5_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E5_XUNID = 'O' THEN 'ORTOPEDIA' WHEN E2_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E2_XUNID = 'O' THEN 'ORTOPEDIA'" +
					"ELSE (SELECT TOP 1 CASE WHEN E5.E5_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E5.E5_XUNID = 'O' THEN 'ORTOPEDIA' WHEN E2.E2_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E2.E2_XUNID = 'O' THEN 'ORTOPEDIA' ELSE 'N_CLASS' END FROM SE5010 E5 WITH(NOLOCK) LEFT JOIN SE2010 E2 WITH(NOLOCK) ON E5.E5_NUMERO = E2.E2_NUM AND E5.E5_TIPO = E2.E2_TIPO AND E5.E5_FORNECE = E2.E2_FORNECE AND E5.E5_PREFIXO = E2.E2_PREFIXO WHERE E5.E5_PREFIXO = SUBSTRING(SE2010.E2_TITPAI, 1, 3)" +
					"AND E5.E5_NUMERO = SUBSTRING(SE2010.E2_TITPAI, 4, 11) AND E5.E5_FORNECE = SUBSTRING(SE2010.E2_TITPAI, 18, 6) AND E5.E5_LOJA = SUBSTRING(SE2010.E2_TITPAI, 24, 2) AND E5.E5_TIPO = SUBSTRING(SE2010.E2_TITPAI, 15, 3))" +
					"END = 'EQUIPAMENTOS'\r\n";
		}
		sql += "GROUP BY SUBSTRING(E5_DATA, 1, 6)\r\n" + "ORDER BY SUBSTRING(E5_DATA, 1, 6)";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setInt(1, ano);
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("EMISSAO"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getBaixas(ano, unidade);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static File getRelacaoBaixas(int ano) {
		String sql = "SELECT\r\n" + 
				"CASE WHEN E5_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E5_XUNID = 'O' THEN 'ORTOPEDIA' WHEN E2_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E2_XUNID = 'O' THEN 'ORTOPEDIA'" +
				"	ELSE (SELECT TOP 1 CASE WHEN E5.E5_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E5.E5_XUNID = 'O' THEN 'ORTOPEDIA' WHEN E2.E2_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E2.E2_XUNID = 'O' THEN 'ORTOPEDIA' ELSE 'N_CLASS' END FROM SE5010 E5 WITH(NOLOCK) LEFT JOIN SE2010 E2 WITH(NOLOCK) ON E5.E5_NUMERO = E2.E2_NUM AND E5.E5_TIPO = E2.E2_TIPO AND E5.E5_FORNECE = E2.E2_FORNECE AND E5.E5_PREFIXO = E2.E2_PREFIXO WHERE E5.E5_PREFIXO = SUBSTRING(SE2010.E2_TITPAI, 1, 3)" +
				"	AND E5.E5_NUMERO = SUBSTRING(SE2010.E2_TITPAI, 4, 11) AND E5.E5_FORNECE = SUBSTRING(SE2010.E2_TITPAI, 18, 6) AND E5.E5_LOJA = SUBSTRING(SE2010.E2_TITPAI, 24, 2) AND E5.E5_TIPO = SUBSTRING(SE2010.E2_TITPAI, 15, 3))" +
				"END AS UNIDADE,\r\n" + 
				"E5_NUMERO,\r\n" + 
				"E5_CLIFOR,\r\n" + 
				"E5_DATA,\r\n" + 
				"CASE E5_RECPAG WHEN 'P' THEN E5_VALOR WHEN 'R' THEN -E5_VALOR END AS E5_VALOR,\r\n" + 
				"E5_NATUREZ,\r\n" + 
				"E5_HISTOR,\r\n" + 
				"E5_TIPODOC,\r\n" + 
				"E5_BENEF,\r\n" + 
				"E5_CCUSTO,\r\n" + 
				"E5_RATEIO,\r\n" + 
				"E5_TIPO,\r\n" + 
				"E2_TITPAI,\r\n" + 
				"E5_RECONC,\r\n" + 
				"SUBSTRING(E2_TITPAI, 1, 3) AS PREFIXO,\r\n" + 
				"SUBSTRING(E2_TITPAI, 4, 11) AS NUMERO,\r\n" + 
				"SUBSTRING(E2_TITPAI, 15, 3) AS TIPO,\r\n" + 
				"SUBSTRING(E2_TITPAI, 18, 6) AS FORNECEDOR,\r\n" + 
				"SUBSTRING(E2_TITPAI, 24, 2) AS LOJA\r\n" + 
				"FROM SE5010 WITH(NOLOCK)\r\n" + 
				"LEFT JOIN SE2010 WITH(NOLOCK) ON E2_NUM = E5_NUMERO\r\n" + 
				"AND E2_PARCELA = E5_PARCELA\r\n" + 
				"AND E2_FORNECE = E5_CLIFOR\r\n" + 
				"AND SE2010.D_E_L_E_T_ = ''\r\n" + 
				"AND E2_PREFIXO = E5_PREFIXO\r\n" + 
				"WHERE YEAR(E5_DATA) >= '2017'\r\n" + 
				"AND E5_SITUACA <> 'C'" +
				"AND SE5010.D_E_L_E_T_ = ''\r\n" + 
				"AND (E5_RECPAG = 'P' OR (E5_RECPAG = 'R' AND E5_TIPODOC = 'ES'))\r\n" + 
				condicaoNatureza + 
				"AND E5_TIPODOC NOT IN ('DC','JR')\r\n" + 
				"AND E5_BANCO <> ''\r\n" + 
//				"AND E5_RECONC = 'x'\r\n" + 
				"ORDER BY E5_DATA";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
//			stmt.setInt(1, ano);
			ResultSet rs = stmt.executeQuery();
			File tempFile = File.createTempFile("relatorio-baixas", ".xls");
			tempFile.deleteOnExit();
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("dados");
			int columnCount = rs.getMetaData().getColumnCount();
			int rowNum = 0;
			HSSFRow row = sheet.createRow(rowNum++);
			for (int colNum = 0; colNum < columnCount; colNum++) {
				row.createCell(colNum).setCellValue(rs.getMetaData().getColumnName(colNum + 1));
			}
			while (rs.next()) {
				row = sheet.createRow(rowNum++);
				for(int colNum = 0; colNum < columnCount; colNum++) {
					row.createCell(colNum).setCellValue(rs.getString(colNum + 1));
				}
			}
			workbook.write(tempFile);
			workbook.close();
			return tempFile;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getRelacaoBaixas(ano);
			} else {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static File getRelacaoBaixas2(int ano) {
		String sql = "SELECT\r\n"
				+ "SE5010.* FROM SE5010 WITH(NOLOCK)\r\n"
				+ "LEFT JOIN SE2010 WITH(NOLOCK) ON E2_NUM = E5_NUMERO\r\n"
				+ "AND E2_PARCELA = E5_PARCELA\r\n"
				+ "AND E2_FORNECE = E5_CLIFOR\r\n"
				+ "AND SE2010.D_E_L_E_T_ = ''\r\n"
				+ "AND E2_PREFIXO = E5_PREFIXO\r\n"
				+ "WHERE\r\n" + "YEAR(E5_DATA) >= ?\r\n"
				+ "AND SE5010.D_E_L_E_T_ = ''\r\n"
				+ "AND (E5_RECPAG = 'P' OR (E5_RECPAG = 'R' AND E5_TIPODOC = 'ES'))\r\n"
				+ condicaoNatureza
				+ "AND E5_TIPODOC NOT IN ('DC','JR')\r\n"
				+ "AND E5_BANCO <> ''\r\n"
//				+ "AND E5_RECONC = 'x'\r\n"
				+ "ORDER BY E5_DATA";

		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setInt(1, ano);
			ResultSet rs = stmt.executeQuery();
			File tempFile = File.createTempFile("relatorio-baixas", ".xls");
			tempFile.deleteOnExit();
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("dados");
			int columnCount = rs.getMetaData().getColumnCount();
			int rowNum = 0;
			HSSFRow row = sheet.createRow(rowNum++);
			for (int colNum = 0; colNum < columnCount; colNum++) {
				row.createCell(colNum).setCellValue(rs.getMetaData().getColumnName(colNum + 1));
			}
			while (rs.next()) {
				row = sheet.createRow(rowNum++);
				for(int colNum = 0; colNum < columnCount; colNum++) {
					row.createCell(colNum).setCellValue(rs.getObject(colNum + 1).toString());
				}
			}
			workbook.write(tempFile);
			workbook.close();
			return tempFile;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getRelacaoBaixas2(ano);
			} else {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static ChartDataAndLabels getBaixasHorizontal(int ano, UNIDADE unidade) {
		String sql = "SELECT\r\n" + "SUBSTRING(E5_DATA, 1, 4) AS EMISSAO,\r\n"
				+ "SUM(CASE E5_RECPAG WHEN 'P' THEN E5_VALOR WHEN 'R' THEN -E5_VALOR END) AS VALOR\r\n"
				+ "FROM SE5010 WITH(NOLOCK)\r\n"
				+ "LEFT JOIN SE2010 WITH(NOLOCK) ON E2_NUM = E5_NUMERO\r\n"
				+ "AND E2_PARCELA = E5_PARCELA\r\n"
				+ "AND E2_FORNECE = E5_CLIFOR\r\n"
				+ "AND SE2010.D_E_L_E_T_ = ''\r\n"
				+ "AND E2_PREFIXO = E5_PREFIXO\r\n"
				+ "WHERE\r\n"
				+ "YEAR(E5_DATA) >= ?\r\n"
				+ "AND SE5010.D_E_L_E_T_ = ''\r\n"
				+ "AND (E5_RECPAG = 'P' OR (E5_RECPAG = 'R' AND E5_TIPODOC = 'ES'))\r\n"
				+ condicaoNatureza
				+ "AND E5_TIPODOC NOT IN ('DC','JR')\r\n"
//				+ "AND E5_RECONC = 'x'\r\n"
				+ "AND E5_BANCO <> ''\r\n";
		if (unidade == null) {
			sql += "AND CASE WHEN E5_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E5_XUNID = 'O' THEN 'ORTOPEDIA' WHEN E2_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E2_XUNID = 'O' THEN 'ORTOPEDIA'\r\n" + 
					"					ELSE (SELECT TOP 1 CASE WHEN E5.E5_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E5.E5_XUNID = 'O' THEN 'ORTOPEDIA' WHEN E2.E2_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E2.E2_XUNID = 'O' THEN 'ORTOPEDIA' ELSE 'N_CLASS' END FROM SE5010 E5 WITH(NOLOCK) LEFT JOIN SE2010 E2 WITH(NOLOCK) ON E5.E5_NUMERO = E2.E2_NUM AND E5.E5_TIPO = E2.E2_TIPO AND E5.E5_FORNECE = E2.E2_FORNECE AND E5.E5_PREFIXO = E2.E2_PREFIXO WHERE E5.E5_PREFIXO = SUBSTRING(SE2010.E2_TITPAI, 1, 3)\r\n" + 
					"					AND E5.E5_NUMERO = SUBSTRING(SE2010.E2_TITPAI, 4, 11) AND E5.E5_FORNECE = SUBSTRING(SE2010.E2_TITPAI, 18, 6) AND E5.E5_LOJA = SUBSTRING(SE2010.E2_TITPAI, 24, 2) AND E5.E5_TIPO = SUBSTRING(SE2010.E2_TITPAI, 15, 3))\r\n" + 
					"				END = 'N_CLASS'\r\n";
		} else if (unidade.equals(UNIDADE.ORTOPEDIA)) {
			sql += "AND CASE WHEN E5_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E5_XUNID = 'O' THEN 'ORTOPEDIA' WHEN E2_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E2_XUNID = 'O' THEN 'ORTOPEDIA'\r\n" + 
					"					ELSE (SELECT TOP 1 CASE WHEN E5.E5_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E5.E5_XUNID = 'O' THEN 'ORTOPEDIA' WHEN E2.E2_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E2.E2_XUNID = 'O' THEN 'ORTOPEDIA' ELSE 'N_CLASS' END FROM SE5010 E5 WITH(NOLOCK) LEFT JOIN SE2010 E2 WITH(NOLOCK) ON E5.E5_NUMERO = E2.E2_NUM AND E5.E5_TIPO = E2.E2_TIPO AND E5.E5_FORNECE = E2.E2_FORNECE AND E5.E5_PREFIXO = E2.E2_PREFIXO WHERE E5.E5_PREFIXO = SUBSTRING(SE2010.E2_TITPAI, 1, 3)\r\n" + 
					"					AND E5.E5_NUMERO = SUBSTRING(SE2010.E2_TITPAI, 4, 11) AND E5.E5_FORNECE = SUBSTRING(SE2010.E2_TITPAI, 18, 6) AND E5.E5_LOJA = SUBSTRING(SE2010.E2_TITPAI, 24, 2) AND E5.E5_TIPO = SUBSTRING(SE2010.E2_TITPAI, 15, 3))\r\n" + 
					"				END = 'ORTOPEDIA'\r\n";
		} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
			sql += "AND CASE WHEN E5_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E5_XUNID = 'O' THEN 'ORTOPEDIA' WHEN E2_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E2_XUNID = 'O' THEN 'ORTOPEDIA'\r\n" + 
					"					ELSE (SELECT TOP 1 CASE WHEN E5.E5_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E5.E5_XUNID = 'O' THEN 'ORTOPEDIA' WHEN E2.E2_XUNID = 'E' THEN 'EQUIPAMENTOS' WHEN E2.E2_XUNID = 'O' THEN 'ORTOPEDIA' ELSE 'N_CLASS' END FROM SE5010 E5 WITH(NOLOCK) LEFT JOIN SE2010 E2 WITH(NOLOCK) ON E5.E5_NUMERO = E2.E2_NUM AND E5.E5_TIPO = E2.E2_TIPO AND E5.E5_FORNECE = E2.E2_FORNECE AND E5.E5_PREFIXO = E2.E2_PREFIXO WHERE E5.E5_PREFIXO = SUBSTRING(SE2010.E2_TITPAI, 1, 3)\r\n" + 
					"					AND E5.E5_NUMERO = SUBSTRING(SE2010.E2_TITPAI, 4, 11) AND E5.E5_FORNECE = SUBSTRING(SE2010.E2_TITPAI, 18, 6) AND E5.E5_LOJA = SUBSTRING(SE2010.E2_TITPAI, 24, 2) AND E5.E5_TIPO = SUBSTRING(SE2010.E2_TITPAI, 15, 3))\r\n" + 
					"				END = 'EQUIPAMENTOS'\r\n";
		}
		sql += "GROUP BY SUBSTRING(E5_DATA, 1, 4)\r\n";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setInt(1, ano);
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("EMISSAO"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getBaixasHorizontal(ano, unidade);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	@SuppressWarnings("unused")
	public static DataTable getTabelaBaixas(String unidade) {
	//TODO continuar o desenvolvimento
	
	
		String sql = "SELECT \r\n" + 
				"	E5_BENEF, \r\n" + 
				"	SUM(CASE WHEN MONTH(E5_DATA) = 1 THEN E5_VALOR ELSE 0 END) AS JANEIRO,\r\n" + 
				"	SUM(CASE WHEN MONTH(E5_DATA) = 2 THEN E5_VALOR ELSE 0 END) AS FEVEREIRO,\r\n" + 
				"	SUM(CASE WHEN MONTH(E5_DATA) = 3 THEN E5_VALOR ELSE 0 END) AS MARCO,\r\n" + 
				"	SUM(CASE WHEN MONTH(E5_DATA) = 4 THEN E5_VALOR ELSE 0 END) AS ABRIL,\r\n" + 
				"	SUM(CASE WHEN MONTH(E5_DATA) = 5 THEN E5_VALOR ELSE 0 END) AS MAIO,\r\n" + 
				"	SUM(CASE WHEN MONTH(E5_DATA) = 6 THEN E5_VALOR ELSE 0 END) AS JUNHO,\r\n" + 
				"	SUM(CASE WHEN MONTH(E5_DATA) = 7 THEN E5_VALOR ELSE 0 END) AS JULHO,\r\n" + 
				"	SUM(CASE WHEN MONTH(E5_DATA) = 8 THEN E5_VALOR ELSE 0 END) AS AGOSTO,\r\n" + 
				"	SUM(CASE WHEN MONTH(E5_DATA) = 9 THEN E5_VALOR ELSE 0 END) AS SETEMBRO,\r\n" + 
				"	SUM(CASE WHEN MONTH(E5_DATA) = 10 THEN E5_VALOR ELSE 0 END) AS OUTUBRO,\r\n" + 
				"	SUM(CASE WHEN MONTH(E5_DATA) = 11 THEN E5_VALOR ELSE 0 END) AS NOVEMBRO,\r\n" + 
				"	SUM(CASE WHEN MONTH(E5_DATA) = 12 THEN E5_VALOR ELSE 0 END) AS DEZEMBRO,\r\n" + 
				"	SUM(E5_VALOR) AS TOTAL\r\n" + 
				"FROM (\r\n" + 
				"	SELECT\r\n" + 
				"CASE\r\n" + 
					"WHEN E2_PARCELA <> '' THEN (SELECT DISTINCT TOP 1 CASE E2_XUNID WHEN 'O' THEN 'ORTOPEDIA' WHEN 'E' THEN 'EQUIPAMENTOS' END FROM SE2010 E2 WITH(NOLOCK) WHERE E2.D_E_L_E_T_ = '' AND E2.E2_NUM = SE2010.E2_NUM AND E2_XUNID <> '' AND E2.E2_FORNECE = SE2010.E2_FORNECE)\r\n" +
					"WHEN E5_XUNID = 'E' THEN 'EQUIPAMENTOS'\r\n" +
					"WHEN E5_XUNID = 'O' THEN 'ORTOPEDIA'\r\n" +
					"WHEN E2_XUNID = 'E' THEN 'EQUIPAMENTOS'\r\n" +
					"WHEN E2_XUNID = 'O' THEN 'ORTOPEDIA'\r\n" +
					"ELSE (\r\n" +
					"	SELECT TOP 1 CASE\r\n" +
					"		WHEN E5.E5_XUNID = 'E' THEN 'EQUIPAMENTOS'\r\n" + 
					"		WHEN E5.E5_XUNID = 'O' THEN 'ORTOPEDIA'\r\n" +
					"		WHEN E2.E2_XUNID = 'E' THEN 'EQUIPAMENTOS'\r\n" +
					"		WHEN E2.E2_XUNID = 'O' THEN 'ORTOPEDIA'\r\n" + 
					"		ELSE 'N_CLASS'\r\n" +
					"END FROM SE5010 E5 WITH(NOLOCK) LEFT JOIN SE2010 E2 WITH(NOLOCK) ON E5.E5_NUMERO = E2.E2_NUM AND E5.E5_TIPO = E2.E2_TIPO AND E5.E5_FORNECE = E2.E2_FORNECE AND E5.E5_PREFIXO = E2.E2_PREFIXO WHERE E5.E5_PREFIXO = SUBSTRING(SE2010.E2_TITPAI, 1, 3) AND E5.E5_NUMERO = SUBSTRING(SE2010.E2_TITPAI, 4, 11) AND E5.E5_FORNECE = SUBSTRING(SE2010.E2_TITPAI, 18, 6) AND E5.E5_LOJA = SUBSTRING(SE2010.E2_TITPAI, 24, 2) AND E5.E5_TIPO = SUBSTRING(SE2010.E2_TITPAI, 15, 3))\r\n" +
				"END AS UNIDADE,\r\n" + 
				"	E5_BENEF,\r\n" + 
				"	E5_DATA,\r\n" + 
				"	CASE E5_RECPAG WHEN 'P' THEN E5_VALOR WHEN 'R' THEN -E5_VALOR END AS E5_VALOR\r\n" + 
				"	FROM SE5010 WITH(NOLOCK)\r\n" + 
				"	LEFT JOIN SE2010 WITH(NOLOCK) ON E2_NUM = E5_NUMERO\r\n" + 
				"	AND E2_PARCELA = E5_PARCELA\r\n" + 
				"	AND E2_FORNECE = E5_CLIFOR\r\n" + 
				"	AND SE2010.D_E_L_E_T_ = ''\r\n" + 
				"	AND E2_PREFIXO = E5_PREFIXO\r\n" + 
				"	WHERE YEAR(E5_DATA) = '2020'\r\n" +
				"	AND E5_SITUACA <> 'C'" +
				"	AND SE5010.D_E_L_E_T_ = ''\r\n" + 
				"	AND (E5_RECPAG = 'P' OR (E5_RECPAG = 'R' AND E5_TIPODOC = 'ES'))\r\n" + 
				"	AND (E5_NATUREZ NOT IN ('5100','5200','5300', '5503') AND E5_NATUREZ NOT LIKE '55%')\r\n" + 
				"	AND E5_TIPODOC NOT IN ('DC','JR')\r\n" + 
				"	AND E5_BANCO <> ''\r\n" + 
				") AS RESULTSET\r\n";
				if (false) {
					sql += "WHERE\r\n";
					if (unidade.equals("ORTOPEDIA")) {
						sql += "UNIDADE = 'ORTOPEDIA'\r\n";
					} else if (unidade.equals("EQUIPAMENTOS")){
						sql += "UNIDADE = 'EQUIPAMENTOS'\r\n";
					} else {
						sql += "UNIDADE NOT IN ('ORTOPEDIA', 'EQUIPAMENTOS')\r\n";
					}
				}
				sql += "GROUP BY E5_BENEF\r\n" + 
				"ORDER BY TOTAL DESC";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					if (i > 1) {
						linha.add(rs.getBigDecimal(i).setScale(2, RoundingMode.HALF_EVEN));
					} else {
						linha.add(rs.getString(i));
					}
				}
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static ChartDataAndLabels getRecebimentosMensal(UNIDADE unidade) {
		String sql = "SELECT\r\n" + 
//				"E5_DATA, E5_TIPO, E5_VALOR, E1_VALOR, E5_NATUREZ, E5_BANCO, E5_AGENCIA, E5_CONTA, E5_DOCUMEN, E5_RECPAG, E5_HISTOR, E5_TIPODOC, E5_LA, E5_PREFIXO, E5_NUMERO, E5_PARCELA, E5_CLIFOR, E5_CLIENTE, A1_NOME, E5_TPDESC\r\n" + 
				"SUBSTRING(E5_DATA, 1, 6) AS EMISSAO, SUM(E5_VALOR) AS VALOR\r\n" +
				"FROM SE5010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SE1010 WITH(NOLOCK) ON E5_PREFIXO + E5_NUMERO + E5_PARCELA = E1_PREFIXO + E1_NUM + E1_PARCELA AND SE1010.D_E_L_E_T_ = ''\r\n" + 
				"LEFT JOIN SA1010 WITH(NOLOCK) ON E5_CLIFOR = A1_COD AND SA1010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE\r\n" + 
				"SE5010.D_E_L_E_T_ = ''\r\n" + 
				"AND E5_TIPODOC NOT IN ('CP', 'BA')\r\n" +
				"AND E5_TIPO <> ''" +
				"AND E5_TPDESC <> 'C'\r\n" +
				"AND E5_RECPAG = 'R'\r\n" +
				"AND E1_TIPO NOT IN ('NCC', 'DAC')\r\n" + 
				"AND E1_TIPOLIQ <> 'LIQ'\r\n";
				if (unidade.equals(UNIDADE.ORTOPEDIA)) {
					sql += "AND E1_NATUREZ IN ('5100')\r\n"; 
				} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
					sql += "AND E1_NATUREZ IN ('5200')\r\n"; 
				} else if (unidade.equals(UNIDADE.EXPORTACAO)) {
					sql += "AND E1_NATUREZ IN ('5300')\r\n"; 
				}
				sql += "AND YEAR(E5_DATA) > YEAR(DATEADD(YEAR, -2, GETDATE()))\r\n" +
				"GROUP BY SUBSTRING(E5_DATA, 1, 6)\r\n" +
				"ORDER BY 1";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString(1));
				cdl.getChartData().add(rs.getBigDecimal(2));
			}
			return cdl;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getValorAReceberTotal(UNIDADE unidade) {
		String sql = "SELECT\r\n" + 
				"SUM(CASE E1_TIPO WHEN 'RA' THEN -E1_SALDO WHEN 'NCC' THEN -E1_SALDO ELSE E1_SALDO END) AS VALOR_A_RECEBER\r\n" + 
				"FROM SE1010 WITH(NOLOCK)\r\n" + 
				"WHERE SE1010.D_E_L_E_T_ = ''\r\n";
		if (unidade == null) {
			sql += "AND E1_NATUREZ IN ('5100', '5200', '5300')\r\n"; 
		} else if (unidade.equals(UNIDADE.ORTOPEDIA)) {
			sql += "AND E1_NATUREZ IN ('5100')\r\n"; 
		} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
			sql += "AND E1_NATUREZ IN ('5200')\r\n"; 
		} else if (unidade.equals(UNIDADE.EXPORTACAO)) {
			sql += "AND E1_NATUREZ IN ('5300')\r\n"; 
		}
				sql += "AND E1_SALDO > 0\r\n" + 
				"AND E1_TIPOLIQ <> 'LIQ'\r\n" +
				"AND E1_TIPO NOT IN ('RA', 'NCC')";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				return rs.getBigDecimal(1).divide(BigDecimal.valueOf(1000000), 2, RoundingMode.HALF_EVEN).toString();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getValorAReceberAtrasado(UNIDADE unidade) {
		String sql = "SELECT\r\n" + 
				"SUM(CASE E1_TIPO WHEN 'RA' THEN -E1_SALDO WHEN 'NCC' THEN -E1_SALDO ELSE E1_SALDO END) AS VALOR_A_RECEBER\r\n" + 
				"FROM SE1010 WITH(NOLOCK)\r\n" + 
				"WHERE SE1010.D_E_L_E_T_ = ''\r\n";
		if (unidade == null) {
			sql += "AND E1_NATUREZ IN ('5100', '5200', '5300')\r\n"; 
		} else if (unidade.equals(UNIDADE.ORTOPEDIA)) {
			sql += "AND E1_NATUREZ IN ('5100')\r\n"; 
		} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
			sql += "AND E1_NATUREZ IN ('5200')\r\n"; 
		} else if (unidade.equals(UNIDADE.EXPORTACAO)) {
			sql += "AND E1_NATUREZ IN ('5300')\r\n"; 
		} 
				sql += "AND E1_SALDO > 0\r\n" + 
				"AND E1_TIPO NOT IN ('RA', 'NCC')\r\n" + 
				"AND CAST(E1_VENCREA AS DATE) < CAST(GETDATE() AS DATE)";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				return rs.getBigDecimal(1).divide(BigDecimal.valueOf(1000000), 2, RoundingMode.HALF_EVEN).toString();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<String> getValorRecebimentoMesAtual(UNIDADE unidade) {
		String sql = "SELECT\r\n" + 
//						"E5_DATA, E5_TIPO, E5_VALOR, E1_VALOR, E5_NATUREZ, E5_BANCO, E5_AGENCIA, E5_CONTA, E5_DOCUMEN, E5_RECPAG, E5_HISTOR, E5_TIPODOC, E5_LA, E5_PREFIXO, E5_NUMERO, E5_PARCELA, E5_CLIFOR, E5_CLIENTE, A1_NOME, E5_TPDESC\r\n" + 
				"SUM(E5_VALOR) AS VALOR\r\n" +
				"FROM SE5010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SE1010 WITH(NOLOCK) ON E5_PREFIXO + E5_NUMERO + E5_PARCELA = E1_PREFIXO + E1_NUM + E1_PARCELA AND SE1010.D_E_L_E_T_ = ''\r\n" + 
				"LEFT JOIN SA1010 WITH(NOLOCK) ON E5_CLIFOR = A1_COD AND SA1010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE\r\n" + 
				"SE5010.D_E_L_E_T_ = ''\r\n"; 
				if (unidade == null) {
					sql += "AND E1_NATUREZ IN ('5100', '5200', '5300')\r\n";
				} else if (unidade.equals(UNIDADE.ORTOPEDIA)) {
					sql += "AND E1_NATUREZ IN ('5100')\r\n"; 
				} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
					sql += "AND E1_NATUREZ IN ('5200')\r\n"; 
				} else if (unidade.equals(UNIDADE.EXPORTACAO)) {
					sql += "AND E1_NATUREZ IN ('5300')\r\n"; 
				}
				sql += "AND E5_TIPODOC NOT IN ('CP', 'BA')\r\n" + 
				"AND E5_TIPO <> ''AND E5_TPDESC <> 'C'\r\n" + 
				"AND E5_RECPAG = 'R'\r\n" + 
				"AND E1_TIPO NOT IN ('NCC', 'DAC')\r\n" + 
				"AND E1_TIPOLIQ <> 'LIQ'\r\n" + 
				"AND YEAR(E5_DATA) = YEAR(GETDATE())\r\n" + 
//				"AND MONTH(E5_DATA) = MONTH(8)\r\n" +
				"AND MONTH(E5_DATA) = MONTH(GETDATE())\r\n" +
//				"GROUP BY SUBSTRING(E5_DATA, 1, 6)\r\n" +
				"ORDER BY 1";		
		
		List<String> recebimentos = new LinkedList<String>();
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				if (rs.getBigDecimal(1) == null || rs.getBigDecimal(1).compareTo(BigDecimal.ZERO) == 0) {
					recebimentos.add(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_EVEN).toString());
				} else {
					recebimentos.add(rs.getBigDecimal(1).divide(BigDecimal.valueOf(1000000), 2, RoundingMode.HALF_EVEN).toString());
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		sql = "SELECT\r\n" + 
				"SUM(E1_SALDO) AS RECEB_MES_CORRENTE\r\n" + 
				"FROM SE1010 WITH(NOLOCK)\r\n" + 
				"WHERE SE1010.D_E_L_E_T_ = ''\r\n";
		if (unidade == null) {
			sql += "AND E1_NATUREZ IN ('5100', '5200', '5300')\r\n";
		} else if (unidade.equals(UNIDADE.ORTOPEDIA)) {
			sql += "AND E1_NATUREZ IN ('5100')\r\n"; 
		} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
			sql += "AND E1_NATUREZ IN ('5200')\r\n"; 
		} else if (unidade.equals(UNIDADE.EXPORTACAO)) {
			sql += "AND E1_NATUREZ IN ('5300')\r\n"; 
		} 
				sql += "AND MONTH(E1_VENCREA) = MONTH(GETDATE())\r\n" +
//				sql += "AND MONTH(E1_VENCTO) = MONTH(8)\r\n" +
				"AND YEAR(E1_VENCREA) = YEAR(GETDATE())\r\n" + 
				"AND E1_TIPO NOT IN ('RA', 'NCC')\r\n" + 
				"AND E1_TIPOLIQ <> 'LIQ'\r\n" +
				"ORDER BY 1";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				if (rs.getBigDecimal(1) == null || rs.getBigDecimal(1).compareTo(BigDecimal.ZERO) == 0) {
					recebimentos.add(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_EVEN).toString());
				} else {
					recebimentos.add(rs.getBigDecimal(1).divide(BigDecimal.valueOf(1000000), 2, RoundingMode.HALF_EVEN).toString());
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		
		return recebimentos;
	}

	public static DataTable getValorAReceberTotalAnalitico(UNIDADE unidade, TipoRelatorioRecebimento tipo) {
		String sql = "SELECT\r\n" + 
				"E1_PREFIXO, E1_NUM, E1_PARCELA, E1_CLIENTE, E1_NOMCLI, A1_NOME, CASE A1_MSBLQL WHEN '1' THEN 'BLOQUEADO' ELSE 'LIBERADO' END AS A1_MSBLQL, E1_VALOR, E1_SALDO, E1_NATUREZ, E1_EMISSAO, E1_VENCTO, E1_VENCREA, CASE WHEN E1_VENCREA < GETDATE() THEN 'ATRASADO' ELSE 'NORMAL' END AS SITUACAO\r\n" + 
				"FROM SE1010 WITH(NOLOCK)\r\n" + 
				"LEFT JOIN SA1010 WITH(NOLOCK) ON E1_CLIENTE = A1_COD AND SA1010.D_E_L_E_T_ = '' AND SE1010.D_E_L_E_T_ = ''\r\n" +
				"WHERE SE1010.D_E_L_E_T_ = ''\r\n";
		if (unidade == null) {
			sql += "AND E1_NATUREZ IN ('5100', '5200', '5300')\r\n"; 
		} else if (unidade.equals(UNIDADE.ORTOPEDIA)) {
			sql += "AND E1_NATUREZ IN ('5100')\r\n";
		} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
			sql += "AND E1_NATUREZ IN ('5200')\r\n";
		} else if (unidade.equals(UNIDADE.EXPORTACAO)) {
			sql += "AND E1_NATUREZ IN ('5300')\r\n";
		}
		if (tipo != null && tipo.equals(TipoRelatorioRecebimento.ATRASADO)) {
			sql += "AND CAST(E1_VENCREA AS DATE) < CAST(GETDATE() AS DATE)\r\n";
		}
				sql += "AND E1_SALDO > 0\r\n" + 
				"AND E1_TIPO NOT IN ('RA', 'NCC')";
		
 		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Object> headers = new LinkedList<Object>();
			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				headers.add(rs.getMetaData().getColumnName(i));
			}
			
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					linha.add(rs.getString(i));
				}
				data.add(linha);
			}
			
			DataTable dataTable = new DataTable();
			dataTable.setHeaders(headers);
			dataTable.setData(data);
			return dataTable;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static LineChart getGraficoInadimplenciaMensal(UNIDADE unidade) {
		String sql = "SELECT DISTINCT\r\n" + 
				"LEFT(SE1010.E1_EMISSAO, 6) AS EMISSAO,\r\n" + 
				"	(SELECT\r\n" + 
				"		SUM (E1.E1_VALLIQ)\r\n" + 
				"		FROM SE1010 E1 WITH(NOLOCK)\r\n" + 
				"		WHERE E1.D_E_L_E_T_ = ''\r\n" + 
				"		AND E1.E1_FILIAL <> 'ZZ'\r\n" + 
				"		AND E1.E1_VENCREA < LEFT(SE1010.E1_EMISSAO, 6) + '31'\r\n" + 
				"		AND (E1.E1_BAIXA > (LEFT(SE1010.E1_EMISSAO, 6) + '31') OR E1.E1_BAIXA = '')\r\n" + 
				"		AND E1.E1_TIPO NOT IN ('RA', 'NCC')\r\n";
		if (unidade != null && unidade.equals(UNIDADE.ORTOPEDIA)) {
			sql += "	AND E1_NATUREZ IN ('5100')";	
		} else if (unidade != null && unidade.equals(UNIDADE.EQUIPAMENTOS)) {
			sql += "	AND E1_NATUREZ IN ('5200')";
		} else if (unidade != null && unidade.equals(UNIDADE.EXPORTACAO)) {
			sql += "	AND E1_NATUREZ IN ('5300')";
		}
		sql += 	"	) AS VALOR\r\n" +  
				"FROM SE1010 WITH(NOLOCK)\r\n" + 
				"WHERE SE1010.D_E_L_E_T_ = ''\r\n" + 
				"AND SE1010.E1_FILIAL <> ''\r\n" + 
				"AND YEAR(E1_EMISSAO) > YEAR(DATEADD(YEAR, -3, GETDATE()))\r\n" +
				"AND LEFT(CONVERT(DATE, E1_EMISSAO), 8) < LEFT(CONVERT(DATE, GETDATE()), 8)\r\n" +
				"ORDER BY 1";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.addLabel(rs.getString(1));
				cdl.addData(rs.getBigDecimal(2));
			}
			LineData lineData = new LineData();
			LineDataset dataset = new LineDataset().setBorderWidth(2).setBorderColor(new Color(185, 9, 9));
			dataset.setData(cdl.getChartData());
			lineData.setLabels(cdl.getChartLabels());
			lineData.addDataset(dataset);
			return new LineChart(lineData);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static DataTable getRecebimentoPorPeriodo(UNIDADE unidade, String periodo, String dataDe, String dataAte, String numTitulo, String codCliente, String pedVenda) {
		String sql = "SELECT\r\n" + 
//				"(SELECT DISTINCT C5_NUM FROM SC5010 WITH(NOLOCK) JOIN SD2010 WITH (NOLOCK) ON SD2010.D_E_L_E_T_ = '' AND D2_FILIAL <> 'ZZ' AND D2_DOC = E5_NUMERO AND D2_PEDIDO = C5_NUM WHERE SC5010.D_E_L_E_T_ = '' AND C5_FILIAL <> 'ZZ') AS PEDIDO,\r\n" +
				"E5_TIPO, E5_VALOR, E1_VALOR, E5_RECONC, E5_NATUREZ, E5_BANCO, E5_AGENCIA, E5_CONTA, E5_DOCUMEN, E5_RECPAG, E5_HISTOR, E5_TIPODOC, E5_LA, E5_PREFIXO, E5_NUMERO, E5_PARCELA, E5_CLIFOR, E5_CLIENTE, A1_NOME, E5_TPDESC, E1_BAIXA, E5_DATA, E5_DTDIGIT, E1_EMISSAO, E1_VENCREA, CASE WHEN E1_BAIXA <= E1_VENCREA THEN 'EM DIA' ELSE 'ATRASADO' END AS SITUACAO\r\n" + 
//				"(SELECT DISTINCT TOP 1 C5_EMISSAO FROM SC5010 WITH(NOLOCK) JOIN SD2010 WITH(NOLOCK) ON D2_PEDIDO = C5_NUM AND SD2010.D_E_L_E_T_ = '' WHERE SC5010.D_E_L_E_T_ = '' AND D2_DOC = E5_NUMERO) AS EMISSAO_PEDIDO\r\n" + 
				"FROM SE5010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SE1010 WITH(NOLOCK) ON E5_PREFIXO + E5_NUMERO + E5_PARCELA = E1_PREFIXO + E1_NUM + E1_PARCELA AND SE1010.D_E_L_E_T_ = ''\r\n" + 
				"LEFT JOIN SA1010 WITH(NOLOCK) ON E5_CLIFOR = A1_COD AND SA1010.D_E_L_E_T_ = ''\r\n" +
				"\r\n" +
				"\r\n" +
				"WHERE\r\n" + 
				"SE5010.D_E_L_E_T_ = ''\r\n" + 
				"AND E5_TIPODOC NOT IN ('CP', 'BA')\r\n" +
				"AND E5_TIPO <> ''" +
				"AND E5_TPDESC <> 'C'\r\n" +
				"AND E5_RECPAG = 'R'\r\n" +
				"AND E1_TIPO NOT IN ('NCC', 'DAC')\r\n" + 
				"AND E1_TIPOLIQ <> 'LIQ'\r\n";
		if (periodo != null && !periodo.equals("")) {
			sql += 	"AND LEFT(E5_DATA, 6) = '" + periodo + "'\r\n"; 
		}
		if (unidade != null && unidade.equals(UNIDADE.ORTOPEDIA)) {
			sql += "AND E1_NATUREZ IN ('5100')\r\n";
		} else if (unidade != null && unidade.equals(UNIDADE.EQUIPAMENTOS)) {
			sql += "AND E1_NATUREZ IN ('5200')\r\n";
		} else if (unidade != null && unidade.equals(UNIDADE.EXPORTACAO)) {
			sql += "AND E1_NATUREZ IN ('5300')\r\n";
		}
		if (dataDe != null && !dataDe.equals("")) {
			sql += "AND E5_DATA >= '" + dataDe + "'\r\n";
		}
		if (dataAte != null && !dataAte.equals("")) {
			sql += "AND E5_DATA <= '" + dataAte + "'\r\n";
		}
		if (numTitulo != null && !numTitulo.equals("")) {
			sql += "AND E5_NUM = '" + numTitulo + "'\r\n";
		}
		if (codCliente != null && !codCliente.equals("")) {
			sql += "AND (E5_CLIENTE = '" + codCliente + "') OR (E5_CLIFOR = '" + codCliente + "')\r\n";
		}
		
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			
			LinkedList<Object> headers = new LinkedList<Object>();
			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				headers.add(rs.getMetaData().getColumnName(i));
			}
			
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					linha.add(rs.getString(i));
				}
				data.add(linha);
			}
			return new DataTable(data, headers);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static DataTable getValorAReceberPorCliente() {
		String sql = "SELECT DISTINCT\r\n" + 
				"LEFT(E1_VENCREA, 4) AS EMISSAO\r\n" + 
				"FROM SE1010 WITH(NOLOCK)\r\n" + 
				"LEFT JOIN SA1010 WITH(NOLOCK) ON E1_CLIENTE = A1_COD AND SA1010.D_E_L_E_T_ = '' AND SE1010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE SE1010.D_E_L_E_T_ = ''\r\n" + 
				"AND E1_NATUREZ IN ('5100', '5200', '5300')\r\n" + 
				"AND E1_SALDO > 0\r\n" + 
				"AND E1_TIPO NOT IN ('RA', 'NCC')\r\n" + 
				"AND E1_VENCREA <= GETDATE()\r\n" + 
				"ORDER BY 1";
		LinkedList<String> anos = new LinkedList<>();
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				anos.add(rs.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		sql = "SELECT\r\n" + 
				"E1_CLIENTE AS COD_CLIENTE, E1_NOMCLI AS NOME, A1_NOME AS NOME2,\r\n";
		for (int i = 0; i < anos.size(); i++) {
			sql += "SUM(CASE WHEN LEFT(E1_VENCREA, 4) = '" + anos.get(i) + "' THEN E1_SALDO ELSE 0 END) AS '" + anos.get(i) + "',\r\n";
		}
		sql += "SUM(E1_SALDO) AS TOTAL\r\n";
				sql += "FROM SE1010 WITH(NOLOCK)\r\n" + 
				"LEFT JOIN SA1010 WITH(NOLOCK) ON E1_CLIENTE = A1_COD AND SA1010.D_E_L_E_T_ = '' AND SE1010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE SE1010.D_E_L_E_T_ = ''\r\n" + 
				"AND E1_NATUREZ IN ('5100', '5200', '5300')\r\n" + 
				"AND E1_SALDO > 0\r\n" + 
				"AND E1_TIPO NOT IN ('RA', 'NCC')\r\n" +
				"AND E1_VENCREA <= GETDATE()\r\n" +
				"GROUP BY E1_CLIENTE, E1_NOMCLI, A1_NOME\r\n" +
				"ORDER BY TOTAL DESC";
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			DataTable dataTable = new DataTable();
			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				dataTable.getHeaders().add(rs.getMetaData().getColumnName(i));
			}
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					if (i > 3) {
						linha.add(rs.getBigDecimal(i).setScale(2, RoundingMode.HALF_EVEN));
					} else {
						linha.add(rs.getString(i));
					}
				}
				dataTable.getData().add(linha);
			}
			return dataTable;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static DataTable getClientesCarteiraInadimplentes() {
		DataTable dataTable = new DataTable();
		String sql = "SELECT A1_COD AS CODIGO_CLIENTE, A1_NREDUZ AS NOME_CLIENTE, \r\n" + 
				"(SELECT SUM(E1_SALDO) FROM SE1010 WITH(NOLOCK) WHERE SE1010.D_E_L_E_T_ = '' AND E1_NATUREZ IN ('5100', '5200', '5300') AND CAST(E1_VENCREA AS DATE) < CAST(GETDATE() AS DATE) AND E1_SALDO > 0 AND E1_TIPO NOT IN ('RA', 'NCC') AND E1_CLIENTE = A1_COD) AS TIT_ATRASO,\r\n" + 
				"CASE A1_MSBLQL WHEN '0' THEN 'LIBERADO' WHEN '1' THEN 'BLOQUEADO' WHEN '2' THEN 'LIBERADO' END AS SITUACAO, VENCIMENTO AS DIAS_VENCIMENTO, ISNULL(SUM(C6_PRCVEN * (C6_QTDVEN - C6_QTDENT)), 0) AS TOTAL_CARTEIRA FROM (\r\n" + 
				"SELECT\r\n" + 
				"E1_CLIENTE, DATEDIFF(DAY, MAX(E1_VENCREA), GETDATE()) AS VENCIMENTO\r\n" + 
				"FROM SE1010 WITH(NOLOCK)\r\n" + 
				"WHERE SE1010.D_E_L_E_T_ = ''\r\n" + 
				"AND E1_SALDO > 0\r\n" + 
				"AND E1_VENCREA < GETDATE()\r\n" + 
				"AND E1_TIPO NOT IN ('RA','NCC')\r\n" + 
				"GROUP BY E1_CLIENTE\r\n" + 
				") AS RESULTSET\r\n" + 
				"LEFT JOIN SC5010 WITH(NOLOCK) ON C5_FILIAL <> 'ZZ' AND SC5010.D_E_L_E_T_ = '' AND C5_TIPO = 'N' AND C5_XREFAT IN ('', '2') AND C5_CLIENTE = E1_CLIENTE\r\n" + 
				"LEFT JOIN SC6010 WITH(NOLOCK) ON C6_QTDVEN > C6_QTDENT AND C6_BLQ <> 'R' AND SC6010.D_E_L_E_T_ = '' AND C5_LIBEROK = 'S' AND C5_CLASPED IN ('1', '2', '3', '4', '6') AND C6_NUM = C5_NUM\r\n" + 
				"JOIN SA1010 WITH(NOLOCK) ON SA1010.D_E_L_E_T_ = '' AND A1_COD = E1_CLIENTE\r\n" + 
				"WHERE\r\n" + 
				"VENCIMENTO > 0\r\n" + 
				"GROUP BY VENCIMENTO, A1_COD, A1_NREDUZ, A1_MSBLQL\r\n" + 
				"ORDER BY 5 ASC";
		
		
		Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				dataTable.getHeaders().add(rs.getMetaData().getColumnName(i));
			}
			
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					if (rs.getMetaData().getColumnName(i).equals("TOTAL_CARTEIRA") || rs.getMetaData().getColumnName(i).equals("TIT_ATRASO")) {
						BigDecimal carteira = rs.getBigDecimal(i);
						linha.add(CurrencyUtils.format(carteira));
					} else {
						linha.add(rs.getString(i));
					}
				}
				data.add(linha);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		dataTable.setData(data);
		return dataTable;
	}

	public static DataTable getRelacaoBaixas(String dataDe, String dataAte, String dataDigitacaoDe, String dataDigitacaoAte, String nomeFornecedor) {
		String sql = 	"SELECT RESULTSET.*, I3_DESC FROM (	\r\n" + 
				"	SELECT\r\n" + 
				"	E2_FILIAL, E5_FILIAL, E2_PREFIXO, E2_NUM, E2_PARCELA, F1_FORNECE, E2_NOMFOR, F1_XUNID, ZO_XUNID, E2_XUNID, E5_XUNID, E2_HIST, E5_HISTOR, E5_DATA, E5_VALOR AS VAL_ORI, CASE WHEN DE_PERC IS NOT NULL THEN E5_VALOR * DE_PERC / 100 WHEN D1_PERC IS NOT NULL THEN (E5_VALOR * D1_PERC) WHEN CV4_PERCEN IS NOT NULL THEN (E5_VALOR * CV4_PERCEN) / 100 ELSE E5_VALOR END AS VALOR_RAT, E5_TIPODOC,\r\n" +
				"	D1_COD, D1_XDESC, B1_LOCPAD,\r\n" +
				"	CASE WHEN E2_XUNID IS NOT NULL AND E2_XUNID <> '' THEN E2_XUNID WHEN E5_XUNID IS NOT NULL AND E5_XUNID <> '' THEN E5_XUNID ELSE F1_XUNID END AS UNID,\r\n" +
				"	CASE WHEN (CV4_CCD IS NOT NULL AND CV4_CCD <> '') THEN CV4_CCD WHEN (DE_CC IS NOT NULL AND DE_CC <> '') THEN DE_CC WHEN (E2_CCD IS NOT NULL AND E2_CCD <> '') THEN E2_CCD WHEN (D1_CC IS NOT NULL AND D1_CC <> '') THEN D1_CC END AS CENTRO_CUSTO\r\n" + 
				"	FROM SE5010\r\n" + 
				"	JOIN SE2010 WITH(NOLOCK) ON E2_NUM = E5_NUMERO AND E2_FORNECE = E5_FORNECE AND E2_PREFIXO = E5_PREFIXO AND E5_PARCELA = E2_PARCELA AND SE2010.D_E_L_E_T_ = '' AND E5_FILORIG = E2_FILIAL\r\n" + 
				"	LEFT JOIN SF1010 WITH(NOLOCK) ON F1_DOC = E2_NUM AND F1_SERIE = E2_PREFIXO AND F1_FORNECE = E2_FORNECE\r\n" + 
				"	LEFT JOIN (SELECT DISTINCT D1_COD, D1_XDESC, B1_LOCPAD, D1_ITEM, D1_CC, D1_DOC, D1_FORNECE, SD1010.D_E_L_E_T_, D1_TOTAL / (SELECT SUM(D1_TOTAL) FROM SD1010 D1 WITH(NOLOCK) JOIN SF4010 WITH(NOLOCK) ON F4_CODIGO = D1_TES AND SF4010.D_E_L_E_T_ = '' AND F4_DUPLIC = 'S' WHERE SD1010.D_E_L_E_T_ = '' AND D1.D1_DOC = SD1010.D1_DOC AND D1.D1_FORNECE = SD1010.D1_FORNECE) AS D1_PERC FROM SD1010 WITH(NOLOCK) JOIN SF4010 WITH(NOLOCK) ON F4_CODIGO = D1_TES AND SF4010.D_E_L_E_T_ = '' AND F4_DUPLIC = 'S' JOIN SB1010 WITH(NOLOCK) ON SB1010.D_E_L_E_T_ = '' AND B1_COD = D1_COD) AS SD1010 ON D1_DOC = F1_DOC AND D1_FORNECE = F1_FORNECE AND SD1010.D_E_L_E_T_ = ''\r\n" + 
				"	LEFT JOIN (SELECT DISTINCT DE_CC, DE_PERC, DE_DOC, DE_FORNECE, DE_SERIE, SDE010.D_E_L_E_T_ FROM SDE010 WITH(NOLOCK)) AS SDE010 ON DE_DOC = F1_DOC AND DE_FORNECE = F1_FORNECE AND DE_SERIE = F1_SERIE AND SDE010.D_E_L_E_T_ = ''\r\n" +
				"	LEFT JOIN SZO010 WITH(NOLOCK) ON SZO010.D_E_L_E_T_ = '' AND ZO_DOC = F1_DOC AND ZO_FORNEC = F1_FORNECE\r\n" +
				"	LEFT JOIN CV4010 WITH(NOLOCK) ON CV4_FILIAL + CV4_DTSEQ + CV4_SEQUEN = E2_ARQRAT AND CV4010.D_E_L_E_T_ = ''\r\n" +
				"	WHERE SE5010.D_E_L_E_T_ = ''\r\n" + 
				"	AND SE2010.D_E_L_E_T_ = ''\r\n" + 
				"	AND E5_TIPODOC NOT IN ('CP', 'DC')\r\n" + 
				"	AND E5_MOTBX NOT IN ('DAC', 'CMP')\r\n";
				if (dataDe != null && !dataDe.equals("")) {
					sql += 	"	AND E5_DATA >= '" + dataDe + "'\r\n";
				}
				if (dataAte != null && !dataAte.equals("")) {
					sql += "	AND E5_DATA <= '" + dataAte + "'\r\n";
				}
				if (dataDigitacaoDe != null && !dataDigitacaoDe.isEmpty()) {
					sql += "AND E5_DTDIGIT >= '" + dataDigitacaoDe + "'\r\n";
				}
				if (dataDigitacaoAte != null && !dataDigitacaoAte.isEmpty()) {
					sql += "AND E5_DTDIGIT <= '" + dataDigitacaoAte + "'\r\n";
				}
				if (nomeFornecedor != null && !nomeFornecedor.isEmpty()) {
					sql += "AND E2_NOMFOR LIKE '%" + nomeFornecedor + "%'\r\n";
				}
				sql += ") AS RESULTSET\r\n" + 
				"LEFT JOIN SI3010 ON SI3010.D_E_L_E_T_ = '' AND I3_CUSTO = CENTRO_CUSTO\r\n"; 
		
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<Object> headers = new LinkedList<Object>();
			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				headers.add(rs.getMetaData().getColumnName(i));
			}
			
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					if (rs.getMetaData().getColumnName(i).equals("VAL_ORI") || rs.getMetaData().getColumnName(i).equals("VALOR_RAT")) {
						linha.add(rs.getBigDecimal(i).setScale(2, RoundingMode.HALF_EVEN));
					} else {
						linha.add(rs.getString(i));
					}
				}
				data.add(linha);
			}
			return new DataTable(data, headers);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
