package br.com.BiTemplate.DAO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import br.com.BiTemplate.model.DataTable;

public class MovimentoDAO {

	public static DataTable getMovimentos(String produtoDe, String produtoAte, String loteDe, String loteAte,
			String armazemDe, String armazemAte, String dataInicial, String dataFinal) {
		Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
		
		String sql = "SELECT\r\n" + 
				"	D5_PRODUTO, B1_DESC, CASE WHEN D5_ORIGLAN > 500 THEN 'S' ELSE 'E' END AS 'E/S', D5_ORIGLAN, D5_QUANT, D5_LOCAL, D5_LOTECTL, D3_LOCALIZ, D5_DATA, D5_DOC, D5_SERIE, D5_OP\r\n" + 
				"FROM SD5010 WITH(NOLOCK)\r\n" + 
				"LEFT JOIN SD3010 WITH(NOLOCK) ON D3_FILIAL <> 'ZZ' AND D3_COD = D5_PRODUTO AND D5_DATA = D3_EMISSAO AND D5_DOC = D3_DOC AND D3_TM = D5_ORIGLAN\r\n" +
				"INNER JOIN SB1010 WITH(NOLOCK) ON D5_PRODUTO = B1_COD AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE\r\n" +
				"SD5010.D_E_L_E_T_ = ''\r\n" + 
				"AND D5_ESTORNO <> 'S'\r\n"; 
		
		if (produtoDe != null && !produtoDe.equals("")) {
			sql += "AND D5_PRODUTO >= '" + produtoDe + "'\r\n";
		}
		if (produtoAte != null && !produtoAte.equals("")) {
			sql += "AND D5_PRODUTO <= '" + produtoAte + "'\r\n";
		}
		if (loteDe != null && !loteDe.equals("")) {
			sql += "AND D5_LOTECTL >= '" + loteDe + "'\r\n";
		}
		if (loteAte != null && !loteAte.equals("")) {
			sql += "AND D5_LOTECTL <= '" + loteAte + "'\r\n";
		}
		if (armazemDe != null && !armazemDe.equals("")) {
			sql += "AND D5_LOCAL >= '" + armazemDe + "'\r\n";
		}
		if (armazemAte != null && !armazemAte.equals("")) {
			sql += "AND D5_LOCAL <= '" + armazemAte + "'\r\n";
		}
		if (dataInicial != null && !dataInicial.equals("")) {
			sql += "AND D5_DATA >= '" + dataInicial + "'\r\n";
		}
		if (dataFinal != null && !dataFinal.equals("")) {
			sql += "AND D5_DATA <= '" + dataFinal + "'\r\n";
		}
		
		sql += "ORDER BY D5_DATA DESC\r\n";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					try {
						linha.add(BigDecimal.valueOf(Double.parseDouble(rs.getObject(i).toString())).setScale(2, RoundingMode.HALF_EVEN));
					} catch (Exception e) {
						linha.add(rs.getObject(i));
					}
				}
				data.add(linha);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new DataTable(data);
	}

}
