package br.com.BiTemplate.DAO;

import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.ChartDataAndLabels;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.utils.HtmlUtils;

public class EntradasDAO {

	public static DataTable getRelacaoEntradas(String produtoDe, String produtoAte, String numPedido, String itemPedido, String nf, String codigoFornecedor, String nomeFornecedor, String entradaDe, String entradaAte, String loteDe, String loteAte, String armazem, String descricao) {
		String sql = "SELECT\r\n" + 
				"LEFT(D1_OP, 6) AS D1_OP, C7_NUM, C7_EMISSAO, D1_DTDIGIT, D1_FORNECE, D1_EMISSAO, D1_DOC, D1_PEDIDO, A2_NOME, D1_LOTECTL, D1_COD, D1_XDESC, D1_UM, D1_QUANT, D1_VUNIT, D1_TOTAL, C7_QUANT, CASE C7_RESIDUO WHEN 'R' THEN '0' ELSE C7_QUANT - D1_QUANT END AS SALDO, D1_CF\r\n" +
				"FROM SD1010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SA2010 WITH(NOLOCK) ON D1_FORNECE = A2_COD\r\n" + 
				"AND D1_LOJA = A2_LOJA\r\n" + 
				"AND SA2010.D_E_L_E_T_ = ''\r\n" +
				"INNER JOIN SB1010 WITH(NOLOCK) ON B1_COD = D1_COD AND SB1010.D_E_L_E_T_ = ''\r\n" +
				"LEFT JOIN SC7010 WITH(NOLOCK) ON C7_FILIAL <> 'ZZ' AND C7_NUM = D1_PEDIDO AND SC7010.D_E_L_E_T_ = '' AND D1_ITEMPC = C7_ITEM\r\n" +
				"WHERE \r\n" + 
				"SD1010.D_E_L_E_T_ = ''\r\n"; 
				if (produtoDe != null && produtoDe != "") {
					sql += "AND D1_COD >= '" + produtoDe.toUpperCase() + "'\r\n";
				}
				if (produtoAte != null && produtoAte != "") {
					sql += "AND D1_COD <= '" + produtoAte.toUpperCase() + "'\r\n";
				}
				if (descricao != null && descricao != "") {
					sql += "AND B1_DESC LIKE '%" + descricao + "%'\r\n";
				}
				if (numPedido != null && numPedido != "") {
					sql += "AND D1_PEDIDO = '" + numPedido.toUpperCase() + "'\r\n";
				}
				if (itemPedido != null && itemPedido != "") {
					sql += "AND D1_ITEMPC = '" + itemPedido.toUpperCase() + "'\r\n";
				}
				if (nf != null && nf != "") {
					sql += "AND D1_DOC = '" + nf.toUpperCase() + "'\r\n";
				}
				if (codigoFornecedor != null && codigoFornecedor != "") {	
					sql += "AND D1_FORNECE = '" + codigoFornecedor.toUpperCase() + "'\r\n";
				}
				if (nomeFornecedor != null && nomeFornecedor != "") {
					sql += "AND A2_NOME LIKE '%" + nomeFornecedor.toUpperCase() + "%'\r\n";
				}
				if (entradaDe != null && entradaDe != "") {
					sql += "AND D1_DTDIGIT >= " + entradaDe + "\r\n";
				}
				if (entradaAte != null && entradaAte != "") {
					sql += "AND D1_DTDIGIT <= " + entradaAte + "\r\n";
				}
				if (loteDe != null && !loteDe.equals("")) {
					sql += "AND D1_LOTECTL >= '" + loteDe + "'\r\n";
				}
				if (loteAte != null && !loteAte.equals("")) {
					sql += "AND D1_LOTECTL <= '" + loteAte + "'\r\n";
				}
				if (armazem != null && !armazem.equals("")) {
					sql += "AND B1_LOCPAD = '" + armazem + "'\r\n";
				}
				sql += "AND D1_FILIAL = '01'\r\n" + 
				"AND D1_PEDIDO <> ''\r\n" + 
				"ORDER BY D1_EMISSAO DESC";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
//				linha.add(HtmlUtils.createLink("/pedidos-compra/" + rs.getString("C7_NUM") + "/detalhes", rs.getString("C7_NUM")));
				
				linha.add(HtmlUtils.createLink("/relacao-pcs?numero=" + rs.getString("C7_NUM"), rs.getString("C7_NUM")));
				
				
				linha.add(rs.getString("C7_EMISSAO"));
				linha.add(HtmlUtils.createLink("/relacao-entradas?codigoFornecedor=" + rs.getString("D1_FORNECE"), rs.getString("A2_NOME")));
				linha.add(rs.getString("D1_DTDIGIT"));
				linha.add(HtmlUtils.createLink("/relacao-entradas?nf=" + rs.getString("D1_DOC") + "&codigoFornecedor=" + rs.getString("D1_FORNECE"), rs.getString("D1_DOC")));
				linha.add(HtmlUtils.createLink("/ordens-producao/" + rs.getString("D1_OP") + "/detalhes", rs.getString("D1_OP")));
				linha.add(rs.getString("D1_LOTECTL"));
				linha.add(rs.getString("D1_COD"));
				linha.add(rs.getString("D1_XDESC"));
				linha.add(rs.getString("D1_UM"));
				linha.add(rs.getString("C7_QUANT"));
				linha.add(rs.getString("D1_QUANT"));
				linha.add(rs.getString("SALDO"));
				linha.add(rs.getBigDecimal("D1_VUNIT").setScale(2, RoundingMode.HALF_EVEN));
				linha.add(rs.getBigDecimal("D1_TOTAL").setScale(2, RoundingMode.HALF_EVEN));
				linha.add(rs.getString("D1_CF"));
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static ChartDataAndLabels getEntradasMensal(UNIDADE unidade) {
		ChartDataAndLabels cdl = new ChartDataAndLabels();
		String sql = "SELECT\r\n" + 
				"	LEFT(D1_DTDIGIT, 6) AS DATA_ENTRADA, SUM(D1_TOTAL) AS D1_TOTAL\r\n" + 
				"FROM SD1010 WITH(NOLOCK)\r\n" +
				"INNER JOIN SB1010 WITH(NOLOCK) ON D1_COD = B1_COD AND SB1010.D_E_L_E_T_ = '' AND B1_FILIAL <> 'ZZ'\r\n" +
				"WHERE\r\n" + 
				"	SD1010.D_E_L_E_T_ = ''\r\n" + 
				"	AND D1_PEDIDO <> ''\r\n" + 
				"	AND YEAR(D1_DTDIGIT) = YEAR(GETDATE())\r\n";
				if (unidade.equals(UNIDADE.ORTOPEDIA)) {
					sql += "	AND B1_LOCPAD = '10'\r\n";
				} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
					sql += "	AND B1_LOCPAD = '11'\r\n";
				}
				sql += "GROUP BY LEFT(D1_DTDIGIT, 6)\r\n" + 
				"ORDER BY 1";
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString(1));
				cdl.getChartData().add(rs.getBigDecimal(2).setScale(2, RoundingMode.HALF_EVEN));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return cdl;
	}
}
