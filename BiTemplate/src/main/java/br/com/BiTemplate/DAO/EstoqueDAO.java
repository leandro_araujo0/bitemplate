package br.com.BiTemplate.DAO;

import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.enums.UNIDADEVENDAS;
import br.com.BiTemplate.model.ChartDataAndLabels;
import br.com.BiTemplate.model.Cliente;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.model.ItemEstoque;
import br.com.BiTemplate.model.Produto;
import br.com.BiTemplate.utils.CurrencyUtils;
import br.com.BiTemplate.utils.DataUtils;
import br.com.BiTemplate.utils.HtmlUtils;

public class EstoqueDAO {

	public static ChartDataAndLabels getValorEstoque(UNIDADE unidade) {
		String sql = "SELECT CASE WHEN GROUPING(TIPO_PRODUTO) = '1' THEN 'TOTAL' ELSE TIPO_PRODUTO END AS TIPO_PRODUTO, SUM(SALDO * D1_VUNIT) AS VALOR FROM (\r\n" + 
				"	SELECT\r\n" + 
				"	CASE SUBSTRING(B8_PRODUTO, 1, 2) WHEN 'CC' THEN 'CC' WHEN 'MP' THEN 'MP' WHEN 'ME' THEN 'ME' WHEN 'MA' THEN 'MA' WHEN 'IF' THEN 'IF' ELSE 'OUTROS' END AS TIPO_PRODUTO,B8_PRODUTO AS PRODUTO,B8_LOTECTL AS LOTE,B8_SALDO AS QTD,B8_EMPENHO AS EMPENHO,B8_SALDO - B8_EMPENHO AS SALDO,B1_UM,D1_VUNIT\r\n" + 
				"	FROM SB8010 WITH(NOLOCK)\r\n" + 
				"	INNER JOIN SB1010 WITH(NOLOCK) ON B8_PRODUTO = B1_COD\r\n" + 
				"	AND B1_FILIAL = ''\r\n" + 
				"	AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"	INNER JOIN SD1010 WITH(NOLOCK) ON B8_LOTECTL = D1_LOTECTL\r\n" + 
				"	AND D1_FILIAL = '01'\r\n" + 
				"	AND SD1010.D_E_L_E_T_ = ''\r\n" + 
				"	AND B8_PRODUTO = D1_COD\r\n" + 
				"	AND D1_PEDIDO <> ''\r\n" + 
				"	AND B1_UM = D1_UM\r\n" + 
				"	WHERE B8_FILIAL = '01'\r\n" + 
				"	AND (B8_SALDO <> 0 OR B8_SALDO2 <> 0)\r\n";
				if (unidade.equals(UNIDADE.ORTOPEDIA)) {
					sql += "	AND B8_LOCAL = '10'\r\n";
				} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
					sql += "	AND B8_LOCAL = '11'\r\n";
				}
				sql += "	AND SB8010.D_E_L_E_T_ = ''\r\n" + 
				") AS RESULTSET\r\n" + 
				"GROUP BY ROLLUP(TIPO_PRODUTO)\r\n" + 
				"ORDER BY VALOR DESC";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("TIPO_PRODUTO"));
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getValorEstoque(unidade);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ChartDataAndLabels getValorEstoqueAcabado(UNIDADE unidade) {
		String sql = "SELECT SUM(QUANTIDADE * PRECO) AS VALOR_TOTAL, SUM(SALDO * PRECO) AS VALOR_ESTOQUE, SUM(EMPENHO * PRECO) AS VALOR_EMPENHADO FROM (\r\n" + 
				"SELECT\r\n" + 
				"BF_PRODUTO AS PRODUTO,BF_QUANT AS QUANTIDADE,BF_EMPENHO AS EMPENHO,BF_QUANT - BF_EMPENHO AS SALDO, DA1_PRCVEN AS PRECO\r\n" + 
				"FROM SBF010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN DA1010 WITH(NOLOCK) ON BF_PRODUTO = DA1_CODPRO\r\n"; 
				sql +="INNER JOIN SB1010 WITH(NOLOCK) ON BF_PRODUTO = B1_COD\r\n" +
				"WHERE SBF010.D_E_L_E_T_ = ''\r\n" +
				"AND B1_MSBLQL <> '1'\r\n" +
				"AND BF_FILIAL = '01'\r\n";
				if (unidade.equals(UNIDADE.ORTOPEDIA)) {
					sql += "AND BF_LOCAL = '01'\r\n" +
							"AND DA1_CODTAB = '083'\r\n"; 
				} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
					sql += "AND BF_LOCAL = '12'\r\n" + 
							"AND DA1_CODTAB = '100'\r\n"; 
				}
				sql += ") AS RESULTSET";
				
				try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
					ResultSet rs = stmt.executeQuery();
					ChartDataAndLabels cdl = new ChartDataAndLabels();
					if (rs.next()) {
						cdl.getChartLabels().add("VALOR TOTAL");
						cdl.getChartData().add(rs.getBigDecimal("VALOR_TOTAL").setScale(2, RoundingMode.HALF_EVEN));
						cdl.getChartLabels().add("VALOR_ESTOQUE");
						cdl.getChartData().add(rs.getBigDecimal("VALOR_ESTOQUE").setScale(2, RoundingMode.HALF_EVEN));
						cdl.getChartLabels().add("VALOR EMPENHADO");
						cdl.getChartData().add(rs.getBigDecimal("VALOR_EMPENHADO").setScale(2, RoundingMode.HALF_EVEN));
					}
					return cdl;
				} catch (SQLException e) {
					if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
						return getValorEstoqueAcabado(unidade);
					} else {
						e.printStackTrace();
					}
				}
		return null;
	}

	public static ChartDataAndLabels getProdutosAntigos(int quantidade, UNIDADE ortopedia) {
		String sql = "SELECT\r\n" + 
				"TOP " + quantidade + " RTRIM(B8_PRODUTO) AS B8_PRODUTO,RTRIM(B1_DESC) AS B1_DESC,B8_LOTECTL,B8_SALDO - B8_EMPENHO AS SALDO,B8_DATA\r\n" + 
				"FROM SB8010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON B8_PRODUTO = B1_COD\r\n" + 
				"AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"AND B1_FILIAL = ''\r\n" + 
				"LEFT JOIN DA1010 WITH(NOLOCK) ON B8_PRODUTO = DA1_CODPRO\r\n" + 
				"AND DA1010.D_E_L_E_T_ = ''\r\n" + 
				"AND DA1_FILIAL = ''\r\n" + 
				"AND DA1_CODTAB = '083'\r\n" + 
				"WHERE B8_FILIAL = '01'\r\n" + 
				"AND SB8010.D_E_L_E_T_ = ''\r\n" + 
				"AND B8_SALDO > 0\r\n" + 
				"AND B8_LOCAL = '01'\r\n" + 
				"ORDER BY B8_DATA, B8_PRODUTO";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(DataUtils.formatData(rs.getString("B8_DATA")) + " - " + rs.getString("B8_PRODUTO") + " - " + rs.getString("B1_DESC") + " - " + rs.getString("B8_LOTECTL"));
				cdl.getChartData().add(rs.getBigDecimal("SALDO").setScale(2, RoundingMode.HALF_EVEN));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getProdutosAntigos(quantidade, ortopedia);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static String getValorAcimaXAnos(int anos, UNIDADE unidade) {
		String sql = "SELECT\r\n" + 
				"SUM(B8_SALDO * DA1_PRCVEN) AS VALOR\r\n" + 
				"FROM SB8010 WITH(NOLOCK)\r\n" + 
				"LEFT JOIN DA1010 WITH(NOLOCK) ON B8_PRODUTO = DA1_CODPRO\r\n" + 
				"AND DA1010.D_E_L_E_T_ = ''\r\n" + 
				"AND DA1_FILIAL = ''\r\n" + 
				"AND DA1_CODTAB = '083'\r\n" + 
				"WHERE B8_FILIAL = '01'\r\n" + 
				"AND SB8010.D_E_L_E_T_ = ''\r\n" + 
				"AND B8_SALDO > 0\r\n" + 
				"AND B8_LOCAL = '01'\r\n" + 
				"AND B8_DATA < DATEADD(YEAR, -" + anos + ", GETDATE())";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				return CurrencyUtils.format(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
			}
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getValorAcimaXAnos(anos, unidade);
			} else {
				e.printStackTrace();
			}
		}
		return "Valor N/E";
	}

	public static Iterable<ItemEstoque> findAll(UNIDADE unidade) {
		String sql = "SELECT\r\n" + 
				"BF_PRODUTO AS PRODUTO,B1_DESC,BF_QUANT AS QUANTIDADE,BF_EMPENHO AS EMPENHO,BF_QUANT - BF_EMPENHO AS SALDO, DA1_PRCVEN AS PRECO,BF_LOTECTL, B8_DATA\r\n" + 
				"FROM SBF010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN DA1010 WITH(NOLOCK) ON BF_PRODUTO = DA1_CODPRO\r\n";
				sql += (unidade.equals(UNIDADE.ORTOPEDIA)) ? "AND DA1_CODTAB = '083'\r\n" : (unidade.equals(UNIDADE.EQUIPAMENTOS)) ? "AND DA1_CODTAB = '100'\r\n" : ""; 
				sql +="INNER JOIN SB1010 WITH(NOLOCK) ON BF_PRODUTO = B1_COD\r\n" + 
				"INNER JOIN SB8010 WITH(NOLOCK) ON B8_PRODUTO = BF_PRODUTO\r\n" + 
				"AND B8_LOTECTL = BF_LOTECTL\r\n" + 
				"AND B8_LOCAL = BF_LOCAL\r\n" + 
				"WHERE SBF010.D_E_L_E_T_ = ''\r\n" + 
				"AND B1_MSBLQL <> '1'\r\n" + 
				"AND BF_FILIAL = '01'\r\n" + 
				"AND BF_QUANT > BF_EMPENHO\r\n" + 
				"AND BF_LOCAL IN ('01', '12')\r\n" +
				"ORDER BY B8_DATA";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<ItemEstoque> itensEstoque = new LinkedList<>();
			while (rs.next()) {
				ItemEstoque item = new ItemEstoque();
				item.setCodigo(rs.getString("PRODUTO"));
				item.setdescricao(rs.getString("B1_DESC"));
				item.setQuant(rs.getBigDecimal("QUANTIDADE").setScale(2, RoundingMode.HALF_EVEN));
				item.setEmpenho(rs.getBigDecimal("EMPENHO").setScale(2, RoundingMode.HALF_EVEN));
				item.setPrcVen(rs.getBigDecimal("PRECO").setScale(2, RoundingMode.HALF_EVEN));
				item.setLote(rs.getString("BF_LOTECTL"));
				item.setData(rs.getString("B8_DATA"));
				itensEstoque.add(item);
			}
			return itensEstoque;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return findAll(unidade);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static DataTable getRelacaoEstoque(String unidade, String codigoProduto, String local, String lote, String dataDe, String dataAte, String estoqueMeses) {
		String sql = "SELECT * FROM (\r\n" + 
				"	SELECT\r\n" + 
				"	B8_PRODUTO AS PRODUTO, B1_DESC AS DESCRICAO, B8_LOTECTL AS LOTE, B8_SALDO AS SALDO,\r\n" + 
				"	(SELECT TOP 1 D3_EMISSAO FROM SD3010 WITH(NOLOCK) WHERE D3_FILIAL <> 'ZZ' AND B8_PRODUTO = D3_COD AND D3_LOCAL = B8_LOCAL AND SD3010.D_E_L_E_T_ = '' AND D3_LOTECTL = B8_LOTECTL ORDER BY D3_EMISSAO ASC) AS DATA_ENTRADA\r\n" + 
				"	FROM SB8010 WITH(NOLOCK)\r\n" + 
				"	JOIN SB1010 WITH(NOLOCK) ON B1_FILIAL <> 'ZZ' AND B1_COD = B8_PRODUTO AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"	WHERE SB8010.D_E_L_E_T_ = ''\r\n" + 
				"	AND B8_SALDO > 0\r\n";
		if (dataDe != null && !dataDe.equals("")) {
			sql +=	"	AND B8_DATA >= '" + dataDe + "'\r\n"; 
		}
		if (dataAte != null && !dataAte.equals("")) {
			sql +=	"	AND B8_DATA <= '" + dataAte + "'\r\n"; 
		}
		if (lote != null && !lote.equals("")) {
			sql +=	"	AND B8_LOTECTL = '" + lote + "'\r\n"; 
		}
		if (codigoProduto != null && !codigoProduto.equals("")) {
			sql +=	"	AND B8_PRODUTO = '" + codigoProduto + "'\r\n"; 
		}
		if (local != null && !local.equals("")) {
			sql +=	"	AND B8_LOCAL = '" + local + "'\r\n"; 
		}
				sql += ") AS RESULTSET\r\n" + 
				"WHERE\r\n" + 
				"1 = 1\r\n";
		if (estoqueMeses != null && !estoqueMeses.equals(""))
				sql += " AND DATA_ENTRADA <= DATEADD(MONTH, -" + estoqueMeses + ", GETDATE())\r\n";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<Object> headers = new LinkedList<Object>();
			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				headers.add(rs.getMetaData().getColumnName(i));
			}
			
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					linha.add(rs.getString(i));
				}
				data.add(linha);
			}
			return new DataTable(data, headers);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static DataTable getEstoqueCalculoVendas(UNIDADEVENDAS unidadeVendas, String segmento, String classe, String sistema, String carac1, String matPrima, String carac2, Long estoqueMinimo, String codigoProduto, String descricaoProduto, Long numClientesProduto, String grupo) {
		String sql = "SELECT TOP 250 BF_PRODUTO, B1_DESC, BM_DESC AS GRUPO, SUM(SALDO_ESTOQUE) AS SALDO_ESTOQUE, CMM, AVG(PRECO) AS PRECO, (SUM(SALDO_ESTOQUE) * (CMM) * AVG(PRECO) * .01) AS FATOR FROM (\r\n" + 
				"SELECT\r\n" + 
				"BF_PRODUTO,B1_DESC,BF_QUANT - BF_EMPENHO AS SALDO_ESTOQUE,DA1_PRCVEN AS PRECO, BM_DESC,\r\n" + 
				"ISNULL((SELECT SUM(C6_QTDVEN / 4) FROM SC6010 INNER JOIN SC5010 ON C6_NUM = C5_NUM AND SC5010.D_E_L_E_T_ = '' AND C5_FILIAL = '01' AND C5_LIBEROK = 'S' AND C5_TIPO = 'N' WHERE C6_PRODUTO = B1_COD AND SC6010.D_E_L_E_T_ = '' AND C6_BLQ <> 'R' AND C5_EMISSAO BETWEEN DATEADD(MONTH, -4, GETDATE()) AND GETDATE()), 0) AS CMM\r\n" + 
				"FROM SBF010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON BF_PRODUTO = B1_COD\r\n" + 
				"AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"AND B1_FILIAL = ''\r\n";
				if (unidadeVendas.equals(UNIDADEVENDAS.MERCADOINTERNO)) {
					sql += "AND B1_LOCPAD = '01'\r\n";
				} else if (unidadeVendas.equals(UNIDADEVENDAS.EXPORTACAO)) {
					sql += "AND B1_LOCPAD IN ('01', '12')\r\n"; 
				} else if (unidadeVendas.equals(UNIDADEVENDAS.EQUIPAMENTOS)) {
					sql += "AND B1_LOCPAD IN ('12')\r\n"; 
				}
//				if (unidadeVendas.equals("unidadeVendas")) {
//					sql += "INNER JOIN SZA010 ON B1_XUNIDAD = ZA_COD\r\n"
//							+ "AND ZA_DESC = '" + unidadeVendas + "'\r\n";
//				}
				if (!segmento.equals("segmento")) {
					sql += "INNER JOIN SZB010 WITH(NOLOCK) ON B1_XSEGMEN = ZB_COD\r\n"
							+ "AND ZB_DESC = '" + segmento + "'\r\n";
				}
				if (!classe.equals("classe")) {
					sql += "INNER JOIN SZC010 WITH(NOLOCK) ON B1_XCLASSE = ZC_COD\r\n"
							+ "AND ZC_DESC = '" + classe + "'\r\n";
				}
				if (!sistema.equals("sistema")) {
					sql += "INNER JOIN SZD010 WITH(NOLOCK) ON B1_XSISTEM = ZD_COD\r\n"
							+ "AND ZD_DESC = '" + sistema + "'\r\n";
				}
				if (!carac1.equals("carac1")) {
					sql += "INNER JOIN SZE010 WITH(NOLOCK) ON B1_XCARAC1 = ZE_COD\r\n"
							+ "AND ZE_DESC = '" + carac1 + "'\r\n";
				}
				if (!matPrima.equals("matPrima")) {
					sql += "INNER JOIN SZF010 WITH(NOLOCK) ON B1_XMP = ZF_COD\r\n"
							+ "AND ZF_DESC = '" + matPrima + "'\r\n";
				}
				if (!carac2.equals("carac2")) {
					sql += "INNER JOIN SZG010 WITH(NOLOCK) ON B1_XCARAC2 = ZG_COD\r\n"
							+ "AND ZG_DESC = '" + carac2 + "'\r\n";
				}
				sql += "LEFT JOIN DA1010 WITH(NOLOCK) ON BF_PRODUTO = DA1_CODPRO\r\n" +
				"AND DA1_CODTAB = CASE B1_LOCPAD WHEN '01' THEN '083' WHEN '12' THEN '100' END\r\n" +
				"AND DA1010.D_E_L_E_T_ = ''\r\n" +
				"INNER JOIN SBM010 ON B1_GRUPO = BM_GRUPO AND BM_FILIAL <> 'ZZ' AND SBM010.D_E_L_E_T_ = ''\r\n" +
				"WHERE BF_LOCAL = '01'\r\n" + 
				"AND SBF010.D_E_L_E_T_ = ''\r\n" + 
				"AND BF_QUANT > BF_EMPENHO\r\n";
				if (codigoProduto != null) {
					sql += "AND BF_PRODUTO LIKE '%" + codigoProduto.toUpperCase() + "%'\r\n";
				}
				if (descricaoProduto != null) {
					sql += "AND B1_DESC LIKE '%" + descricaoProduto.replace(" ", "%").toUpperCase() + "%'\r\n";
				}
				if (estoqueMinimo != null) {
					sql += " AND ISNULL((SELECT SUM(BF.BF_QUANT - BF.BF_EMPENHO) FROM SBF010 BF WHERE BF.BF_FILIAL = '01' AND BF.BF_PRODUTO = SBF010.BF_PRODUTO AND BF.BF_LOCAL = '01'), 0) >= " + estoqueMinimo + "\r\n";
				}
				if (grupo != null && grupo != "") {
					sql += "AND BM_DESC = '" + grupo + "'\r\n";
				}
				sql += "AND BF_FILIAL = '01'\r\n" + 
				"AND DA1_PRCVEN <> ''\r\n" + 
				") AS RESULTSET\r\n" + 
				"GROUP BY BF_PRODUTO, B1_DESC, CMM, BM_DESC\r\n" + 
				"ORDER BY FATOR DESC";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			DataTable dt = new DataTable();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				for (Cliente cliente : getClientes(new Produto(rs.getString("BF_PRODUTO")), unidadeVendas, numClientesProduto)) {
					Collection<Object> row = new LinkedList<Object>();
					row.add(HtmlUtils.createLink("/produtos/detalhes?codigoProduto=" + rs.getString("BF_PRODUTO"), rs.getString("BF_PRODUTO")));
					row.add(rs.getString("B1_DESC"));
					row.add(rs.getString("GRUPO"));
					row.add(rs.getBigDecimal("SALDO_ESTOQUE").setScale(2, RoundingMode.HALF_EVEN));
					row.add(rs.getBigDecimal("CMM").setScale(2, RoundingMode.HALF_EVEN));
					row.add(rs.getBigDecimal("PRECO").setScale(2, RoundingMode.HALF_EVEN));
					row.add(HtmlUtils.createLink("/clientes/" + cliente.getCodigo() + "/detalhes", cliente.getCodigo()));
					row.add(HtmlUtils.createLink("/clientes/" + cliente.getCodigo() + "/detalhes", cliente.getNome()));
					row.add(HtmlUtils.createLink("/clientes/" + cliente.getEmail() + "/detalhes", cliente.getEmail()));
					row.add(cliente.getPoderCompra());
					row.add(cliente.getComprasMes());
					data.add(row);
				}
			}
			dt.setData(data);
			return dt;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static List<Cliente> getClientes(Produto produto, UNIDADEVENDAS unidadeVendas, Long numClientesProduto) {
		String sql = "SELECT TOP ";
				sql += (numClientesProduto != null && numClientesProduto != 0) ? numClientesProduto : "5"; 
				
				sql += "\r\nMAX(A1_EMAIL) AS EMAIL, A1_COD AS CODCLI, MAX(NOME) AS NOMECLI, MAX(MEDIA_COMPRAS) AS MED_COMP, MAX(COMPRAS_MES) AS COMPR_MES, SUM(C6_VALOR) AS VLR_TOTAL FROM (\r\n" + 
						"	SELECT DISTINCT\r\n" + 
						"	A1_EMAIL, A1_COD, RTRIM(A1_NREDUZ) AS NOME, C6_VALOR,\r\n" + 
						"	ISNULL((SELECT CASE A1_EST WHEN 'EX' THEN SUM(C6_VALOR) * 3.6 ELSE SUM(C6_VALOR) END / 12 FROM SC6010 WITH(NOLOCK) INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM AND C5_FILIAL = '01' AND SC5010.D_E_L_E_T_ = '' AND C5_TIPO = 'N' AND C5_CLIENTE = A1_COD AND C5_EMISSAO > DATEADD(MONTH,-12,GETDATE()) WHERE SC6010.D_E_L_E_T_ = '' AND C6_FILIAL = '01' AND C6_BLQ <> 'R' AND C5_XREFAT IN ('', '2') AND C5_CLASPED IN ('1', '2', '3', '4') AND ((C6_TES LIKE '5%') OR (C6_TES IN ('701', '702', '703')))),0) AS MEDIA_COMPRAS,\r\n" + 
						"	ISNULL((SELECT CASE A1_EST WHEN 'EX' THEN SUM(C6_VALOR) * 3.6 ELSE SUM(C6_VALOR) END FROM SC6010 WITH(NOLOCK) INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM AND C5_FILIAL = '01' AND SC5010.D_E_L_E_T_ = '' AND C5_TIPO = 'N' AND C5_CLIENTE = A1_COD AND YEAR(C5_EMISSAO) = YEAR(GETDATE()) AND MONTH(C5_EMISSAO) = MONTH(GETDATE()) WHERE SC6010.D_E_L_E_T_ = '' AND C6_FILIAL = '01' AND C6_BLQ <> 'R' AND C5_XREFAT IN ('', '2') AND C5_CLASPED IN ('1', '2', '3', '4') AND ((C6_TES LIKE '5%') OR (C6_TES IN ('701', '702', '703')))),0) AS COMPRAS_MES\r\n" + 
						"	FROM SC6010 WITH(NOLOCK)\r\n" + 
						"	INNER JOIN SB1010 WITH(NOLOCK) ON C6_PRODUTO = B1_COD\r\n" + 
						"	AND B1_FILIAL = ''\r\n" + 
						"	AND SB1010.D_E_L_E_T_ = ''\r\n" + 
						"	INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM\r\n" + 
						"	AND C5_FILIAL = '01'\r\n" + 
						"	AND SC5010.D_E_L_E_T_ = ''\r\n" + 
						"	AND C5_CLASPED IN ('1', '2', '3', '4')\r\n" + 
						"	AND C5_TIPO = 'N'\r\n" + 
						"	AND C5_EMISSAO > DATEADD(MONTH, -12, GETDATE())\r\n" + 
						"	INNER JOIN SA1010 WITH(NOLOCK) ON C5_CLIENT = A1_COD\r\n" + 
						"	AND C5_LOJACLI = A1_LOJA\r\n" + 
						"	AND SA1010.D_E_L_E_T_ = ''\r\n" + 
						"	AND A1_FILIAL = ''\r\n" + 
						"	WHERE SC6010.D_E_L_E_T_ = ''\r\n" + 
						"	AND C6_PRODUTO = ?\r\n" +  
						"	AND C6_FILIAL = '01'\r\n" + 
						"	AND C6_BLQ <> 'R'\r\n" + 
						"   AND A1_MSBLQL <> '1'\r\n" + 
						"   AND C5_CLIENTE <> '000422'	AND C5_LIBEROK = 'S'\r\n"; 
						if (unidadeVendas.equals(UNIDADEVENDAS.EXPORTACAO)) {
							sql += " AND A1_EST = 'EX'\r\n";
						} else {
							sql += "AND A1_EST <> 'EX'\r\n";
						}
						sql += ") AS RESULTSET\r\n" + 
						"WHERE MEDIA_COMPRAS > 0\r\n" + 
						"GROUP BY A1_COD\r\n" + 
						"ORDER BY (SUM(C6_VALOR) * (MAX(MEDIA_COMPRAS) - MAX(COMPRAS_MES)) / 1000000) DESC";
				
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setString(1, produto.getCodigo());
			ResultSet rs = stmt.executeQuery();
			LinkedList<Cliente> clientes = new LinkedList<Cliente>();
			while (rs.next()) {
				Cliente cliente = new Cliente();
				cliente.setCodigo(rs.getString("CODCLI"));
				cliente.setNome(rs.getString("NOMECLI"));
				cliente.setMediaCompras(rs.getBigDecimal("MED_COMP").setScale(2, RoundingMode.HALF_EVEN));
				cliente.setComprasMes(rs.getBigDecimal("COMPR_MES").setScale(2, RoundingMode.HALF_EVEN));
				cliente.setEmail(rs.getString("EMAIL"));
				clientes.add(cliente);
			}
			return clientes;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static DataTable getSaldoPorEndereco(String produto, String lote, String armazem, String descricao) {
		String sql = "SELECT \r\n" + 
				"	BF_PRODUTO AS PRODUTO,\r\n" + 
				"	B1_DESC AS DESCRICAO,\r\n" + 
				"	B1_UM AS UM,\r\n" +
				"	BF_LOCAL AS ARMAZEM,\r\n" + 
				"	B1_LOCPAD,\r\n" +
				"	BF_LOTECTL AS LOTE,\r\n" + 
				"	B8_DATA AS DATA,\r\n" + 
				"	BF_LOCALIZ AS ENDERECO,\r\n" + 
				"	BF_QUANT AS QUANTIDADE,\r\n" + 
				"	BF_EMPENHO AS EMPENHO,\r\n" + 
				"	BF_QUANT - BF_EMPENHO AS SALDO,\r\n" + 
				"	(SELECT TOP 1 B8_DTVALID FROM SB8010 WITH(NOLOCK) WHERE B8_LOTECTL = BF_LOTECTL) AS VALID_LOTE\r\n" + 
				"FROM SBF010 WITH(NOLOCK)\r\n" +
				"JOIN SB8010 ON BF_PRODUTO = B8_PRODUTO AND BF_LOCAL = B8_LOCAL AND BF_LOTECTL = B8_LOTECTL AND SB8010.D_E_L_E_T_ = ''\r\n" +
				"INNER JOIN SB1010 WITH(NOLOCK) ON BF_PRODUTO = B1_COD AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE\r\n" + 
				"	SBF010.D_E_L_E_T_ = ''\r\n";
				if (armazem != null && !armazem.equals("")) {
					sql += "AND BF_LOCAL = '" + armazem + "'\r\n";
				}
				if (produto != null && !produto.equals("")) {
					sql += "AND BF_PRODUTO = '" + produto + "'\r\n";
				}
				if (lote != null && !lote.equals("")) {
					sql += "AND BF_LOTECTL = '" + lote + "'\r\n";
				}
				if (descricao != null && !descricao.equals("")) {
					sql += "AND B1_DESC LIKE '%" + descricao + "%'";
				}
				sql += "	AND BF_QUANT - BF_EMPENHO > 0\r\n" + 
				"ORDER BY BF_PRODUTO, (SELECT TOP 1 B8_DTVALID FROM SB8010 WHERE B8_LOTECTL = BF_LOTECTL)";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				linha.add(rs.getString("PRODUTO"));
				linha.add(rs.getString("DESCRICAO"));
				linha.add(rs.getString("UM"));
				linha.add(rs.getString("ARMAZEM"));
				linha.add(rs.getString("B1_LOCPAD"));
				linha.add(rs.getString("LOTE"));
				linha.add(rs.getString("DATA"));
				linha.add(rs.getString("ENDERECO"));
				linha.add(rs.getString("QUANTIDADE"));
				linha.add(rs.getString("EMPENHO"));
				linha.add(rs.getString("SALDO"));
				linha.add(rs.getString("VALID_LOTE"));
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static DataTable getItensPontoPedido(UNIDADE unidade) {
		String sql = "SELECT\r\n" + 
				"	B1_COD AS CODIGO,\r\n" + 
				"	B1_DESC AS DESCRICAO,\r\n" + 
				"	QTD_ESTOQUE AS ESTQ,\r\n" + 
				"	QTD_INSPECAO AS INSPEC,\r\n" + 
				"	QTD_ENDERECAR AS ENDER,\r\n" + 
				"	QTD_ESTOQUE + QTD_INSPECAO + QTD_ENDERECAR AS TOTAL_ESTQ,\r\n" + 
				"	QTD_SC AS SC,\r\n" + 
				"	QTD_PD_COMPRA AS PC,\r\n" + 
				"	QTD_OP AS OP,\r\n" + 
				"	QTD_SC + QTD_PD_COMPRA + QTD_OP AS ENTRADAS,\r\n" + 
				"	B1_EMIN AS ESTQ_MIN,\r\n" + 
				"	B1_EMAX AS ESTQ_MAX,\r\n" + 
				"	B1_ESTSEG AS ESTQ_SEG,\r\n" + 
				"	B1_PE AS LEAD_T,\r\n" + 
				"	B1_TIPE AS PERIODO,\r\n" + 
				"	B1_LM AS LOTE_MIN,\r\n" + 
				"	B1_PRODSBP AS PROV,\r\n" + 
				"	CASE WHEN QTD_ESTOQUE + QTD_INSPECAO + QTD_ENDERECAR + QTD_SC + QTD_PD_COMPRA + QTD_OP < B1_EMIN THEN 'PROVIDENCIAR' WHEN QTD_ESTOQUE + QTD_INSPECAO + QTD_ENDERECAR < B1_EMIN THEN 'ACOMPANHAMENTO' ELSE 'OK' END AS STATUS\r\n" + 
				"	FROM (\r\n" + 
				"		SELECT\r\n" + 
				"		B1_COD, B1_DESC,\r\n" + 
				"		ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH (NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = B1_LOCPAD AND D_E_L_E_T_ = ''), 0) AS QTD_ESTOQUE,\r\n" + 
				"		ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH (NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = '98' AND D_E_L_E_T_ = ''), 0) AS QTD_INSPECAO,\r\n" + 
				"		ISNULL((SELECT SUM(DA_SALDO) FROM SDA010 WITH (NOLOCK) WHERE DA_PRODUTO = B1_COD AND DA_SALDO <> 0 AND D_E_L_E_T_ = '' AND DA_LOCAL = B1_LOCPAD), 0) AS QTD_ENDERECAR,\r\n" + 
				"		ISNULL((SELECT SUM(C1_QUANT - C1_QUJE) FROM SC1010 C1 WITH (NOLOCK) WHERE C1.D_E_L_E_T_='' AND C1_QUJE < C1_QUANT AND C1_RESIDUO <> 'S' AND C1_PRODUTO = B1_COD AND C1_LOCAL = B1_LOCPAD), 0) AS QTD_SC,\r\n" + 
				"		ISNULL((SELECT SUM(C7_QUANT - C7_QUJE) FROM SC7010 WITH (NOLOCK) WHERE D_E_L_E_T_='' AND C7_PRODUTO = B1_COD AND (C7_QUANT - C7_QUJE > 0) AND C7_RESIDUO <> 'S' AND C7_LOCAL = B1_LOCPAD), 0) AS QTD_PD_COMPRA,\r\n" + 
				"		ISNULL((SELECT SUM(C2_QUANT - C2_QUJE - C2_PERDA) FROM SC2010 WITH (NOLOCK) WHERE C2_PRODUTO = B1_COD AND C2_DATRF='' AND C2_LOCAL = B1_LOCPAD AND SC2010.D_E_L_E_T_='' AND C2_STATUS IN ('N', '')), 0) AS QTD_OP,\r\n" + 
				"		B1_EMIN, B1_EMAX, B1_ESTSEG, B1_QE, B1_PE, B1_TIPE, B1_LE, B1_LM, B1_PRODSBP\r\n" + 
				"		FROM SB1010 WITH(NOLOCK)\r\n" + 
				"		WHERE SB1010.D_E_L_E_T_ = ''\r\n" + 
				"		AND B1_FILIAL <> 'ZZ'\r\n";
		if (unidade.equals(UNIDADE.ORTOPEDIA)) {
			sql += "	AND B1_LOCPAD = '10'\r\n"; 
		} else {
			sql += "	AND B1_LOCPAD = '11'\r\n"; 
		}
				sql += "AND B1_EMIN > 0\r\n" + 
				"		AND B1_COD NOT LIKE 'BN%'\r\n" + 
				") AS RESULTSET";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			
			LinkedList<Object> headers = new LinkedList<Object>();
			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				headers.add(rs.getMetaData().getColumnName(i));
			}
			
			Collection<Collection<Object>> data = new LinkedList<>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					linha.add(rs.getString(i));
				}
				data.add(linha);
			}
					
			return new DataTable(data, headers);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new DataTable();
	}

	public static DataTable getItensEstoque(UNIDADE unidade, String local) {
		String sql = "SELECT\r\n" + 
				"B1_COD, B1_DESC, BF_QUANT, BF_EMPENHO, BF_LOTECTL, BF_LOCAL, BF_LOCALIZ, B8_DATA\r\n" + 
				"FROM SBF010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON BF_PRODUTO = B1_COD AND SB1010.D_E_L_E_T_ = '' AND B1_FILIAL <> 'ZZ'\r\n" +
				"INNER JOIN SB8010 WITH(NOLOCK) ON SB8010.D_E_L_E_T_ = '' AND B8_FILIAL <> 'ZZ' AND BF_PRODUTO = B8_PRODUTO AND BF_LOTECTL = B8_LOTECTL AND BF_LOCAL = B8_LOCAL\r\n" +
				"WHERE SBF010.D_E_L_E_T_ = ''\r\n";
				sql += "AND BF_LOCAL = '" + local + "'\r\n";
				
			sql += "AND BF_FILIAL <> 'ZZ'\r\n" + 
					"AND BF_QUANT > BF_EMPENHO";
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<Object> headers = new LinkedList<Object>();
			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				headers.add(rs.getMetaData().getColumnName(i));
			}
			
			
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					linha.add(rs.getString(i));
				}
				data.add(linha);
			}
			return new DataTable(data, headers);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new DataTable();
	}

	public static DataTable getSaldoPorArmazem(String codigoProduto) {
		String sql = "SELECT\r\n" + 
				"BF_LOCAL ,SUM(BF_QUANT - BF_EMPENHO) AS QUANTIDADE\r\n" + 
				"FROM SBF010\r\n" + 
				"WHERE SBF010.D_E_L_E_T_ = ''\r\n" + 
				"AND BF_PRODUTO = '" + codigoProduto + "'\r\n" + 
				"AND BF_QUANT > BF_EMPENHO\r\n" + 
				"GROUP BY BF_LOCAL";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					linha.add(rs.getString(i));
				}
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
