package br.com.BiTemplate.DAO;

import java.sql.Connection;
import java.sql.SQLException;

public class Database {

	private static final String url = "jdbc:sqlserver://192.168.0.223";
//	private static final String url = "jdbc:sqlserver://localhost";
	private static final String urlTeste = "jdbc:sqlserver://192.168.0.243;databaseName=TOTVS";
	private static final String user = "leandro";
	private static final String userTeste = "leandro2";
	private static final String password = "Ort0s!nt3s3@487";
	private static final String passwordTeste = "12121212";

	public static Connection getConnection() {
//		return getConnectionTeste();
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			return java.sql.DriverManager.getConnection(url, user, password);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Connection getConnectionTeste() {
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			return java.sql.DriverManager.getConnection(urlTeste, userTeste, passwordTeste);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
