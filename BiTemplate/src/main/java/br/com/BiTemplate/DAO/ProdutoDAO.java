package br.com.BiTemplate.DAO;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import br.com.BiTemplate.model.ChartDataAndLabels;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.model.DetalhesProduto;
import br.com.BiTemplate.model.FollowUpProduto;
import br.com.BiTemplate.model.Produto;
import br.com.BiTemplate.model.VariacaoPrecoProduto;
import br.com.BiTemplate.utils.CurrencyUtils;
import br.com.BiTemplate.utils.HtmlUtils;

public class ProdutoDAO {

	public static List<Produto> findByCodigo(String codigo) {
		List<Produto> produtos = new LinkedList<>();

		Connection con = Database.getConnection();
		String sql = "SELECT RTRIM(B1_COD) AS B1_COD, B1_DESC FROM SB1010 WITH(NOLOCK) WHERE B1_COD like '%' + ? + '%'";
		try (PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setString(1, codigo);
			ResultSet resultSet = stmt.executeQuery();
			while (resultSet.next()) {
				Produto produto = new Produto();
				produto.setCodigo(resultSet.getString("B1_COD"));
				produto.setDescricao(resultSet.getString("B1_DESC"));
				produtos.add(produto);
			}
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return findByCodigo(codigo);
			} else {
				e.printStackTrace();
			}
		}
		return produtos;
	}

	public static LinkedList<Produto> getProdutos() {
		LinkedList<Produto> produtos = new LinkedList<>();
		Connection con = Database.getConnection();
		String sql = "SELECT RTRIM(B1_COD) AS B1_COD, B1_DESC FROM SB1010 WITH(NOLOCK) WHERE SB1010.D_E_L_E_T_ = '' AND B1_FILIAL = '' AND B1_LOCPAD = '11'";
		try (PreparedStatement stmt = con.prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Produto produto = new Produto();
				produto.setCodigo(rs.getString("B1_COD"));
				produto.setDescricao(rs.getString("B1_DESC"));
				produtos.add(produto);
			}
			return produtos;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getProdutos();
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static BigDecimal findPrecoInicial(Produto produto, String dataInicial) {
		String sql = "SELECT TOP 1 D1_VUNIT FROM SD1010 WITH(NOLOCK) WHERE D1_COD = ? AND D1_EMISSAO > ? AND SD1010.D_E_L_E_T_ = '' AND D1_FILIAL = '01'";
		Connection con = Database.getConnection();
		try (PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setString(1, produto.getCodigo());
			stmt.setString(2, dataInicial);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				return new BigDecimal(rs.getLong("D1_VUNIT"));
			}
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return findPrecoInicial(produto, dataInicial);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static BigDecimal findPrecoFinal(Produto produto, String dataFinal) {
		String sql = "SELECT TOP 1 D1_VUNIT FROM SD1010 WITH(NOLOCK) WHERE D1_COD = ? AND D1_EMISSAO < ? AND SD1010.D_E_L_E_T_ = '' AND D1_FILIAL = '01' ORDER BY D1_EMISSAO DESC";
		Connection con = Database.getConnection();
		try (PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setString(1, produto.getCodigo());
			stmt.setString(2, dataFinal);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				return new BigDecimal(rs.getLong("D1_VUNIT"));
			}
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return findPrecoFinal(produto, dataFinal);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static LinkedList<VariacaoPrecoProduto> getVariacaoPrecoProdutos(String dataInicial, String dataFinal,
			String fornecedor) {
		Connection con = Database.getConnection();

		if (fornecedor.equals(""))
			fornecedor = null;

		String sql = "SELECT RTRIM(B1_COD) AS B1_COD, B1_DESC,\n";
		sql += "(SELECT TOP 1 D1_VUNIT FROM SD1010 WITH(NOLOCK) WHERE D1_COD = B1_COD AND D1_EMISSAO > ? AND SD1010.D_E_L_E_T_ = '' AND D1_FILIAL = '01' AND D1_PEDIDO <> '' ";
		if (fornecedor != null)
			sql += "AND D1_FORNECE = '" + fornecedor + "' ";
		sql += "ORDER BY D1_EMISSAO) AS PRIMEIRO_PRECO,\n";
		sql += "(SELECT TOP 1 A2_NOME FROM SD1010 WITH(NOLOCK) INNER JOIN SA2010 WITH(NOLOCK) ON D1_FORNECE = A2_COD AND D1_LOJA = A2_LOJA AND A2_FILIAL = '' WHERE D1_COD = B1_COD AND D1_EMISSAO > ? AND SD1010.D_E_L_E_T_ = '' AND D1_FILIAL = '01' AND D1_PEDIDO <> '' ";
		if (fornecedor != null)
			sql += " AND D1_FORNECE = '" + fornecedor + "' ";
		sql += "ORDER BY D1_EMISSAO) AS PRIMEIRO_FORNECEDOR,\n";
		sql += "(SELECT TOP 1 D1_VUNIT FROM SD1010 WITH(NOLOCK) WHERE D1_COD = B1_COD AND D1_EMISSAO < ? AND SD1010.D_E_L_E_T_ = '' AND D1_FILIAL = '01' AND D1_PEDIDO <> '' ";
		if (fornecedor != null)
			sql += " AND D1_FORNECE = '" + fornecedor + "' ";
		sql += "ORDER BY D1_EMISSAO DESC) AS ULTIMO_PRECO,\n";
		sql += "(SELECT TOP 1 A2_NOME FROM SD1010 WITH(NOLOCK) INNER JOIN SA2010 WITH(NOLOCK) ON D1_FORNECE = A2_COD AND D1_LOJA = A2_LOJA AND A2_FILIAL = '' WHERE D1_COD = B1_COD AND D1_EMISSAO < ? AND SD1010.D_E_L_E_T_ = '' AND D1_FILIAL = '01' AND D1_PEDIDO <> '' ";
		if (fornecedor != null)
			sql += " AND D1_FORNECE = '" + fornecedor + "' ";
		sql += "ORDER BY D1_EMISSAO DESC) AS ULTIMO_FORNECEDOR\n";
		sql += " FROM SB1010 SB1010 WITH(NOLOCK) WHERE B1_FILIAL = '' AND B1_LOCPAD = '11' AND SB1010.D_E_L_E_T_ = ''";

		try (PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setString(1, dataInicial);
			stmt.setString(2, dataInicial);
			stmt.setString(3, dataFinal);
			stmt.setString(4, dataFinal);
			ResultSet rs = stmt.executeQuery();
			LinkedList<VariacaoPrecoProduto> variacaoPrecoProdutos = new LinkedList<>();
			while (rs.next()) {
				VariacaoPrecoProduto vpp = new VariacaoPrecoProduto();
				vpp.setProduto(new Produto(rs.getString("B1_COD")).setDescricao(rs.getString("B1_DESC")));
				vpp.setDataInicial(dataInicial);
				vpp.setDataFinal(dataFinal);
				vpp.setPrecoInicial(rs.getBigDecimal("PRIMEIRO_PRECO"));
				vpp.setPrecoFinal(rs.getBigDecimal("ULTIMO_PRECO"));
				vpp.setFornecedorInicial(rs.getString("PRIMEIRO_FORNECEDOR"));
				vpp.setFornecedorFinal(rs.getString("ULTIMO_FORNECEDOR"));
				variacaoPrecoProdutos.add(vpp);
			}
			return variacaoPrecoProdutos;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getVariacaoPrecoProdutos(dataInicial, dataFinal, fornecedor);
			} else {
				e.printStackTrace();
			}
		}

		return null;
	}

	public static LinkedList<Produto> findAll() {
		LinkedList<Produto> produtos = new LinkedList<>();
		String sql = "SELECT RTRIM(B1_COD) AS B1_COD, B1_DESC FROM SB1010 WITH(NOLOCK) WHERE B1_LOCPAD IN ('01', '10', '11', '12')";

		Connection con = Database.getConnection();

		try (PreparedStatement stmt = con.prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Produto produto = new Produto();
				produto.setCodigo(rs.getString("B1_COD"));
				produto.setDescricao(rs.getString("B1_DESC"));
				produtos.add(produto);
			}
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return findAll();
			} else {
				e.printStackTrace();
			}
		}

		return produtos;
	}

	public static DetalhesProduto findDetalhesProduto(String codigo) {
		String sql = "SELECT RTRIM(B1_COD) AS B1_COD, B1_DESC, B1_UM,\r\n"
				+ "CASE WHEN B1_LOCPAD = '11' THEN 'Desenhos JPG/' + RTRIM(REPLACE(Z2_CODDESE,'-','')) + '.jpg' WHEN B1_LOCPAD = '12' THEN 'Desenhos JPG/' + RTRIM(REPLACE(Z2_CODDESE,'-','')) + '.jpg' WHEN B1_LOCPAD = '10' THEN REPLACE(RTRIM(Z2_ARQDESE), ';', '') + '.pdf' WHEN B1_LOCPAD = '01' THEN REPLACE(RTRIM(Z2_ARQDESE), ';', '') + '.pdf' END AS DESENHO,"
				+ "ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = B1_LOCPAD AND D_E_L_E_T_ = ''), 0) AS QTD_ESTOQUE,"
				+ "ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND D_E_L_E_T_ = ''), 0) AS QTD_TODOS_ESTOQUES,"
				+ "ISNULL((SELECT SUM(D4_QUANT) FROM SD4010 D4 WITH(NOLOCK) INNER JOIN SC2010 C2 WITH(NOLOCK) ON D4.D4_OP = C2.C2_NUM + C2.C2_ITEM + C2.C2_SEQUEN AND C2.D_E_L_E_T_ = '' AND C2.C2_DATRF = '' WHERE B1_COD = D4.D4_COD AND D4.D_E_L_E_T_ = '' AND D4.D4_LOTECTL = ''), 0) AS QTD_EMPENHO,\r\n"
				+ "ISNULL((SELECT SUM(C2_QUANT - C2_QUJE - C2_PERDA) FROM SC2010 WITH(NOLOCK) WHERE C2_PRODUTO = B1_COD AND C2_DATRF='' AND C2_LOCAL=B1_LOCPAD AND D_E_L_E_T_='' AND C2_STATUS IN ('N', '')), 0) AS QTD_OP,"
				+ "ISNULL((SELECT SUM(DA_SALDO) FROM SDA010 WITH(NOLOCK) WHERE DA_PRODUTO = B1_COD AND DA_SALDO <> 0 AND D_E_L_E_T_ = '' AND DA_LOCAL = B1_LOCPAD), 0) AS QTD_ENDERECAR,"
				+ "ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = '98' AND D_E_L_E_T_ = ''), 0) AS QTD_INSPECAO,"
				+ "ISNULL((SELECT SUM(C1_QUANT - C1_QUJE) FROM SC1010 C1 WITH(NOLOCK) WHERE C1.D_E_L_E_T_='' AND C1_QUJE < C1_QUANT AND C1_RESIDUO <> 'S' AND C1_PRODUTO = B1_COD), 0) AS QTD_SC,"
				+ "ISNULL((SELECT SUM(C6_QTDVEN - C6_QTDENT) FROM SC6010 C6 WITH(NOLOCK) INNER JOIN SC5010 C5 WITH(NOLOCK) ON C6_NUM = C5_NUM AND C5_TIPO = 'N' AND C5_FILIAL = '01' WHERE C5_CLIENTE <> '000422' AND C6_FILIAL = '01' AND C6_PRODUTO = B1_COD AND C6.D_E_L_E_T_ = '' AND C5.D_E_L_E_T_ = '' AND C6_BLQ <> 'R' AND C6_QTDENT < C6_QTDVEN AND C5_LIBEROK = 'S' AND ((C6_TES LIKE '5%') OR (C6_TES IN ('701', '702', '703')) OR C6_CF IN ('5949'))) - ISNULL((SELECT SUM(C9_QTDLIB) FROM SC9010 C9 INNER JOIN SC5010 ON C9_PEDIDO = C5_NUM AND C5_LIBEROK = 'S' WHERE C9_FILIAL = '01' AND C5_TIPO = 'N' AND C9_PRODUTO=B1_COD AND C9.D_E_L_E_T_='' AND C9_LOTECTL<>'' AND C9_NFISCAL=''), 0), 0) AS QTD_PV,"
				+ "ISNULL((SELECT SUM(C7_QUANT - C7_QUJE) FROM SC7010 WITH(NOLOCK) WHERE D_E_L_E_T_='' AND C7_PRODUTO = B1_COD AND (C7_QUANT - C7_QUJE > 0) AND C7_RESIDUO<>'S'), 0) AS QTD_PC,\n"
				+ "ISNULL((SELECT SUM(D3_QUANT) / 4 FROM SD3010 WITH(NOLOCK) WHERE SD3010.D_E_L_E_T_ = '' AND D3_FILIAL = '01' AND D3_OP <> '' AND D3_COD = B1_COD AND D3_EMISSAO > DATEADD(MONTH, -4, GETDATE()) AND D3_ESTORNO <> 'S' AND D3_LOCAL = B1_LOCPAD), 0) AS PMM,\n"
				+ "(SELECT ISNULL(SUM(D5_QUANT) / 3, 0) FROM SD5010 WITH(NOLOCK) WHERE D5_PRODUTO = B1_COD AND SD5010.D_E_L_E_T_ = '' AND D5_DATA >= DATEADD(MONTH, -3, GETDATE()) AND D5_ORIGLAN = '499' AND D5_LOCAL = '96') AS CMM\r\n"
				+ "FROM SB1010 WITH(NOLOCK)\r\n"
				+ "LEFT JOIN SZ2010 WITH(NOLOCK) ON B1_COD = Z2_PRODUTO\r\n"
				+ "WHERE B1_FILIAL <> 'ZZ' AND SB1010.D_E_L_E_T_ = '' AND B1_COD = ?";
		
		Connection con = Database.getConnection();

		try (PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setString(1, codigo);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				DetalhesProduto dp = new DetalhesProduto(new Produto().setCodigo(codigo).setDescricao(rs.getString("B1_DESC")));
				dp.setQtdEstoque(rs.getBigDecimal("QTD_ESTOQUE"));
				dp.setQtdEmpenhada(rs.getBigDecimal("QTD_EMPENHO").setScale(2, RoundingMode.HALF_EVEN));
				dp.setQtdOP(rs.getBigDecimal("QTD_OP"));
				dp.setQtdPV(rs.getBigDecimal("QTD_PV"));
				dp.setQtdEnderecar(rs.getBigDecimal("QTD_ENDERECAR"));
				dp.setQtdInspecao(rs.getBigDecimal("QTD_INSPECAO"));
				dp.setQtdSC(rs.getBigDecimal("QTD_SC"));
				dp.setQtdPC(rs.getBigDecimal("QTD_PC"));
				dp.setPmm(rs.getBigDecimal("PMM").setScale(2, RoundingMode.HALF_EVEN));
				dp.setCmm(rs.getBigDecimal("CMM").setScale(2, RoundingMode.HALF_EVEN));
				dp.setDesenho(rs.getString("DESENHO"));
				dp.setUm(rs.getString("B1_UM"));
				dp.setQtdTodosEstoques(rs.getBigDecimal("QTD_TODOS_ESTOQUES").setScale(2, RoundingMode.HALF_EVEN));
				return dp;
			}
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return findDetalhesProduto(codigo);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ChartDataAndLabels getBarData(String codigoProduto, String dataInicial, String dataFinal,
			String fornecedor) {
		String sql = "SELECT SUBSTRING(D1_EMISSAO, 1, 6) AS EMISSAO, AVG(D1_VUNIT) AS MEDIA_VALOR FROM SD1010 WITH(NOLOCK) WHERE D1_COD = ? AND D1_EMISSAO BETWEEN ? AND ? ";
		if (fornecedor != null && !fornecedor.equals(""))
			sql += "AND D1_FORNECE = ? ";
		sql += " AND D1_FILIAL = '01' AND SD1010.D_E_L_E_T_ = '' GROUP BY SUBSTRING(D1_EMISSAO, 1, 6) ORDER BY SUBSTRING(D1_EMISSAO, 1, 6)";
		Connection con = Database.getConnection();
		try (PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setString(1, codigoProduto);
			stmt.setString(2, dataInicial);
			stmt.setString(3, dataFinal);
			if (fornecedor != null && !fornecedor.equals(""))
				stmt.setString(4, fornecedor);
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartData().add(rs.getBigDecimal("MEDIA_VALOR").setScale(2, RoundingMode.HALF_EVEN));
				cdl.getChartLabels().add(rs.getString("EMISSAO"));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getBarData(codigoProduto, dataInicial, dataFinal, fornecedor);
			} else {
				e.printStackTrace();
			}
		}

		return null;
	}

	public static Produto findOne(String codigoProduto) {
		Produto produto = new Produto();
		String sql = "SELECT RTRIM(B1_COD) AS B1_COD, B1_DESC, B1_UM FROM SB1010 WITH(NOLOCK) WHERE B1_COD = ?";
		Connection con = Database.getConnection();
		try (PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setString(1, codigoProduto);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				produto.setCodigo(rs.getString("B1_COD"));
				produto.setDescricao(rs.getString("B1_DESC"));
				produto.setUM(rs.getString("B1_UM"));
				return produto;
			}
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return findOne(codigoProduto);
			} else {
				e.printStackTrace();
			}
		}
		
		
		return null;
	}

	public static ChartDataAndLabels getDataCMM(String codigoProduto) {
		String sql = "SELECT\r\n" + 
				"SUBSTRING(C5_EMISSAO, 1, 6) AS MES, SUM(C6_QTDVEN) AS QTD\r\n" + 
				"FROM SC6010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM\r\n" + 
				"AND SC5010.D_E_L_E_T_ = ''\r\n" + 
				"AND C5_FILIAL = '01'\r\n" + 
				"AND C5_LIBEROK = 'S'\r\n" + 
				"AND C5_TIPO = 'N'\r\n" + 
				"WHERE C6_PRODUTO = ?\r\n" + 
				"AND SC6010.D_E_L_E_T_ = ''\r\n" + 
				"AND C6_BLQ <> 'R'\r\n" + 
				"AND C5_EMISSAO BETWEEN DATEADD(YEAR,-3,GETDATE())\r\n" + 
				"AND GETDATE()\r\n" + 
				"GROUP BY SUBSTRING(C5_EMISSAO, 1, 6)\r\n" +
				"ORDER BY 1 ASC";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setString(1, codigoProduto);
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("MES"));
				cdl.getChartData().add(rs.getBigDecimal("QTD"));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getDataCMM(codigoProduto);
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static List<DetalhesProduto> findDetalhesProdutos() {
		String sql = "SELECT RTRIM(B1_COD) AS B1_COD, B1_DESC,"
				+ "ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = B1_LOCPAD AND D_E_L_E_T_ = ''), 0) AS QTD_ESTOQUE,"
				+ "ISNULL((SELECT SUM(C2_QUANT - C2_QUJE - C2_PERDA) FROM SC2010 WITH(NOLOCK) WHERE C2_PRODUTO = B1_COD AND C2_DATRF='' AND C2_LOCAL=B1_LOCPAD AND D_E_L_E_T_='' AND C2_STATUS IN ('N', '')), 0) AS QTD_OP,"
				+ "ISNULL((SELECT SUM(DA_SALDO) FROM SDA010 WITH(NOLOCK) WHERE DA_PRODUTO = B1_COD AND DA_SALDO <> 0 AND D_E_L_E_T_ = '' AND DA_LOCAL = B1_LOCPAD), 0) AS QTD_ENDERECAR,"
				+ "ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = '98' AND D_E_L_E_T_ = ''), 0) AS QTD_INSPECAO,"
				+ "ISNULL((SELECT SUM(C1_QUANT - C1_QUJE) FROM SC1010 C1 WITH(NOLOCK) WHERE C1.D_E_L_E_T_='' AND C1_QUJE < C1_QUANT AND C1_RESIDUO <> 'S' AND C1_PRODUTO = B1_COD), 0) AS QTD_SC,"
				+ "ISNULL((SELECT SUM(C6_QTDVEN - C6_QTDENT) FROM SC6010 C6 WITH(NOLOCK) INNER JOIN SC5010 C5 ON C6_NUM = C5_NUM AND C5_FILIAL = '01' WHERE C6_FILIAL = '01' AND C6_PRODUTO = B1_COD AND C6.D_E_L_E_T_ = '' AND C5.D_E_L_E_T_ = '' AND C6_BLQ <> 'R' AND C6_QTDENT < C6_QTDVEN AND C5_LIBEROK = 'S' AND ((C6_TES LIKE '5%') OR (C6_TES IN ('701', '702', '703')))) - ISNULL((SELECT SUM(C9_QTDLIB) FROM SC9010 C9 INNER JOIN SC5010 ON C9_PEDIDO = C5_NUM AND C5_LIBEROK = 'S' WHERE C9_FILIAL = '01' AND C9_PRODUTO=B1_COD AND C9.D_E_L_E_T_='' AND C9_LOTECTL<>'' AND C9_NFISCAL=''), 0), 0) AS QTD_PV,"
				+ "ISNULL((SELECT SUM(C7_QUANT - C7_QUJE) FROM SC7010 WITH(NOLOCK) WHERE D_E_L_E_T_='' AND C7_PRODUTO = B1_COD AND (C7_QUANT - C7_QUJE > 0) AND C7_RESIDUO<>'S'), 0) AS QTD_PC,\n"
				+ "ISNULL((SELECT SUM(C6_QTDVEN / 4) FROM SC6010 INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM AND SC5010.D_E_L_E_T_ = '' AND C5_FILIAL = '01' AND C5_LIBEROK = 'S' AND C5_TIPO = 'N' WHERE C6_PRODUTO = B1_COD AND SC6010.D_E_L_E_T_ = '' AND C6_BLQ <> 'R' AND C5_EMISSAO BETWEEN DATEADD(MONTH, -4, GETDATE()) AND GETDATE()), 0) AS CMM\r\n"
				+ "FROM SB1010 WITH(NOLOCK) WHERE B1_FILIAL = '' AND B1_LOCPAD IN ('01', '12') AND SB1010.D_E_L_E_T_ = '' AND B1_MSBLQL <> '1' AND B1_TIPO = 'PA'";

		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			List<DetalhesProduto> detalhesProdutos = new LinkedList<>();
			while (rs.next()) {
				DetalhesProduto dp = new DetalhesProduto(new Produto().setCodigo(rs.getString("B1_COD")).setDescricao(rs.getString("B1_DESC")));
				dp.setQtdEstoque(rs.getBigDecimal("QTD_ESTOQUE"));
				dp.setQtdOP(rs.getBigDecimal("QTD_OP"));
				dp.setQtdPV(rs.getBigDecimal("QTD_PV"));
				dp.setQtdEnderecar(rs.getBigDecimal("QTD_ENDERECAR"));
				dp.setQtdInspecao(rs.getBigDecimal("QTD_INSPECAO"));
				dp.setQtdSC(rs.getBigDecimal("QTD_SC"));
				dp.setQtdPC(rs.getBigDecimal("QTD_PC"));
				dp.setCmm(rs.getBigDecimal("CMM"));
				detalhesProdutos.add(dp);
			}
			return detalhesProdutos;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return findDetalhesProdutos();
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static File findAllExcel() {
		String sql = "SELECT RTRIM(B1_COD) AS B1_COD, B1_DESC,"
				+ "ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = B1_LOCPAD AND D_E_L_E_T_ = ''), 0) AS QTD_ESTOQUE,"
				+ "ISNULL((SELECT SUM(C2_QUANT - C2_QUJE - C2_PERDA) FROM SC2010 WITH(NOLOCK) WHERE C2_PRODUTO = B1_COD AND C2_DATRF='' AND C2_LOCAL=B1_LOCPAD AND D_E_L_E_T_='' AND C2_STATUS IN ('N', '')), 0) AS QTD_OP,"
				+ "ISNULL((SELECT SUM(DA_SALDO) FROM SDA010  WITH(NOLOCK)WHERE DA_PRODUTO = B1_COD AND DA_SALDO <> 0 AND D_E_L_E_T_ = '' AND DA_LOCAL = B1_LOCPAD), 0) AS QTD_ENDERECAR,"
				+ "ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = '98' AND D_E_L_E_T_ = ''), 0) AS QTD_INSPECAO,"
				+ "ISNULL((SELECT SUM(C1_QUANT - C1_QUJE) FROM SC1010 C1 WITH(NOLOCK) WHERE C1.D_E_L_E_T_='' AND C1_QUJE < C1_QUANT AND C1_RESIDUO <> 'S' AND C1_PRODUTO = B1_COD), 0) AS QTD_SC,"
				+ "ISNULL((SELECT SUM(C6_QTDVEN - C6_QTDENT) FROM SC6010 C6 WITH(NOLOCK) INNER JOIN SC5010 C5 WITH(NOLOCK) ON C6_NUM = C5_NUM AND C5_FILIAL = '01' WHERE C6_FILIAL = '01' AND C6_PRODUTO = B1_COD AND C6.D_E_L_E_T_ = '' AND C5.D_E_L_E_T_ = '' AND C6_BLQ <> 'R' AND C6_QTDENT < C6_QTDVEN AND C5_LIBEROK = 'S' AND ((C6_TES LIKE '5%') OR (C6_TES IN ('701', '702', '703')))) - ISNULL((SELECT SUM(C9_QTDLIB) FROM SC9010 C9 INNER JOIN SC5010 ON C9_PEDIDO = C5_NUM AND C5_LIBEROK = 'S' WHERE C9_FILIAL = '01' AND C9_PRODUTO=B1_COD AND C9.D_E_L_E_T_='' AND C9_LOTECTL<>'' AND C9_NFISCAL=''), 0), 0) AS QTD_PV,"
				+ "ISNULL((SELECT SUM(C7_QUANT - C7_QUJE) FROM SC7010 WITH(NOLOCK) WHERE D_E_L_E_T_='' AND C7_PRODUTO = B1_COD AND (C7_QUANT - C7_QUJE > 0) AND C7_RESIDUO<>'S'), 0) AS QTD_PC,\n"
				+ "ISNULL((SELECT SUM(C6_QTDVEN / 4) FROM SC6010 WITH(NOLOCK) INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM AND SC5010.D_E_L_E_T_ = '' AND C5_FILIAL = '01' AND C5_LIBEROK = 'S' AND C5_TIPO = 'N' WHERE C6_PRODUTO = B1_COD AND SC6010.D_E_L_E_T_ = '' AND C6_BLQ <> 'R' AND C5_EMISSAO BETWEEN DATEADD(MONTH, -4, GETDATE()) AND GETDATE()), 0) AS CMM\r\n"
				+ "FROM SB1010 WITH(NOLOCK) WHERE B1_FILIAL = '' AND B1_LOCPAD IN ('01', '12') AND SB1010.D_E_L_E_T_ = '' AND B1_MSBLQL <> '1' AND B1_TIPO = 'PA' ORDER BY B1_COD";

		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			File file = File.createTempFile("PRODUTOS", "xls");
			file.deleteOnExit();
			ResultSet rs = stmt.executeQuery();
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("PRODUTOS");
			int rowNum = 0;
			HSSFRow row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue("PRODUTO");
			row.createCell(1).setCellValue("DESCRICAO");
			row.createCell(2).setCellValue("QTD_ESTOQUE");
			row.createCell(3).setCellValue("QTD_OP");
			row.createCell(4).setCellValue("QTD_PV");
			row.createCell(5).setCellValue("QTD_ENDERECAR");
			row.createCell(6).setCellValue("QTD_INSPECAO");
			row.createCell(7).setCellValue("CMM");
			while (rs.next()) {
				row = sheet.createRow(rowNum++);
				row.createCell(0).setCellValue(rs.getString("B1_COD"));
				row.createCell(1).setCellValue(rs.getString("B1_DESC"));
				row.createCell(2).setCellValue(rs.getInt("QTD_ESTOQUE"));
				row.createCell(3).setCellValue(rs.getLong("QTD_OP"));
				row.createCell(4).setCellValue(rs.getLong("QTD_PV"));
				row.createCell(5).setCellValue(rs.getLong("QTD_ENDERECAR"));
				row.createCell(6).setCellValue(rs.getLong("QTD_INSPECAO"));
				row.createCell(7).setCellValue(rs.getLong("CMM"));
			}
			workbook.write(file);
			workbook.close();
			return file;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return findAllExcel(); 
			} else {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static DataTable findDetalhesProdutosTabela(String tipo, String produto, String descricao, String bloqueio) {
		DataTable dt = new DataTable();
		String sql = "SELECT RTRIM(B1_COD) AS B1_COD, RTRIM(B1_DESC) AS B1_DESC, B1_XANVISA, B1_TIPO,CASE B1_MSBLQL WHEN '1' THEN 'BLOQUEADO' ELSE 'DESBLOQUEADO' END AS B1_MSBLQL,"
				+ "ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = B1_LOCPAD AND D_E_L_E_T_ = ''), 0) AS QTD_ESTOQUE,"
				+ "ISNULL((SELECT SUM(C2_QUANT - C2_QUJE - C2_PERDA) FROM SC2010 WITH(NOLOCK) WHERE C2_PRODUTO = B1_COD AND C2_DATRF='' AND C2_LOCAL=B1_LOCPAD AND D_E_L_E_T_='' AND C2_STATUS IN ('N', '')), 0) AS QTD_OP,"
				+ "ISNULL((SELECT SUM(DA_SALDO) FROM SDA010 WITH(NOLOCK) WHERE DA_PRODUTO = B1_COD AND DA_SALDO <> 0 AND D_E_L_E_T_ = '' AND DA_LOCAL = B1_LOCPAD), 0) AS QTD_ENDERECAR,"
				+ "ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = '98' AND D_E_L_E_T_ = ''), 0) AS QTD_INSPECAO,"
				+ "ISNULL((SELECT SUM(C1_QUANT - C1_QUJE) FROM SC1010 C1 WITH(NOLOCK) WHERE C1.D_E_L_E_T_='' AND C1_QUJE < C1_QUANT AND C1_RESIDUO <> 'S' AND C1_PRODUTO = B1_COD), 0) AS QTD_SC,"
				+ "ISNULL((SELECT SUM(C6_QTDVEN - C6_QTDENT) FROM SC6010 C6 WITH(NOLOCK) INNER JOIN SC5010 C5 WITH(NOLOCK) ON C6_NUM = C5_NUM AND C5_FILIAL = '01' WHERE C6_FILIAL = '01' AND C6_PRODUTO = B1_COD AND C6.D_E_L_E_T_ = '' AND C5.D_E_L_E_T_ = '' AND C6_BLQ <> 'R' AND C6_QTDENT < C6_QTDVEN AND C5_LIBEROK = 'S' AND ((C6_TES LIKE '5%') OR (C6_TES IN ('701', '702', '703')))) - ISNULL((SELECT SUM(C9_QTDLIB) FROM SC9010 C9 INNER JOIN SC5010 ON C9_PEDIDO = C5_NUM AND C5_LIBEROK = 'S' WHERE C9_FILIAL = '01' AND C9_PRODUTO=B1_COD AND C9.D_E_L_E_T_='' AND C9_LOTECTL<>'' AND C9_NFISCAL=''), 0), 0) AS QTD_PV,"
				+ "ISNULL((SELECT SUM(C7_QUANT - C7_QUJE) FROM SC7010 WITH(NOLOCK) WHERE D_E_L_E_T_='' AND C7_PRODUTO = B1_COD AND (C7_QUANT - C7_QUJE > 0) AND C7_RESIDUO<>'S'), 0) AS QTD_PC,\n"
				+ "ISNULL((SELECT SUM(C6_QTDVEN / 4) FROM SC6010 WITH(NOLOCK) INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM AND SC5010.D_E_L_E_T_ = '' AND C5_FILIAL = '01' AND C5_LIBEROK = 'S' AND C5_TIPO = 'N' WHERE C6_PRODUTO = B1_COD AND SC6010.D_E_L_E_T_ = '' AND C6_BLQ <> 'R' AND C5_EMISSAO BETWEEN DATEADD(MONTH, -4, GETDATE()) AND GETDATE()), 0) AS CMM,\r\n"
				+ "ISNULL((SELECT SUM(D3_QUANT) / 4 FROM SD3010 WITH(NOLOCK) WHERE SD3010.D_E_L_E_T_ = '' AND D3_FILIAL = '01' AND D3_OP <> '' AND D3_COD = B1_COD AND D3_EMISSAO > DATEADD(MONTH, -4, GETDATE()) AND D3_ESTORNO <> 'S' AND D3_LOCAL = B1_LOCPAD), 0) AS PMM\r\n"
				+ "FROM SB1010 WITH(NOLOCK) WHERE B1_FILIAL = '' AND B1_LOCPAD IN ('01', '10', '11', '12') AND SB1010.D_E_L_E_T_ = ''\r\n";
				if (bloqueio != null && bloqueio.equals("Nao"))
					sql += "AND B1_MSBLQL <> '1'\r\n";
				
				if (tipo != null && !tipo.equals("Todos")) {
					sql += "AND B1_TIPO = '" + tipo + "'\r\n";					
				}
				if (produto != null && !produto.equals(""))
					sql += "AND B1_COD LIKE '%" + produto + "%'\r\n";
				if (descricao != null && !descricao.equals(""))
					sql += "AND B1_DESC LIKE '%" + descricao + "%'\r\n";
				sql += "ORDER BY B1_COD";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				LinkedList<Object> row = new LinkedList<>();
				row.add("");
				row.add("<input type='checkbox' onClick='addProduto(\"" + rs.getString("B1_COD") + "\", this)'/>");
				row.add(HtmlUtils.createLink("/produtos/detalhes?codigoProduto=" + rs.getString("B1_COD"), rs.getString("B1_COD")));
				row.add(rs.getString("B1_DESC"));
				row.add(rs.getString("B1_TIPO"));
				row.add(rs.getString("B1_XANVISA"));
				row.add(rs.getString("B1_MSBLQL"));
				row.add(rs.getLong("QTD_ESTOQUE"));
				row.add(rs.getLong("QTD_ENDERECAR"));
				row.add(rs.getLong("QTD_INSPECAO"));
				row.add(HtmlUtils.createLink("/ordens-producao/produto?codigoProduto=" + rs.getString("B1_COD"), rs.getLong("QTD_OP")));
				row.add("<span class='qtdPv'>" + rs.getLong("QTD_PV") + "</span>");
				row.add("<span class='cmm'>" + rs.getLong("CMM") + "</span>");
				row.add("<span class='pmm'>" + rs.getLong("PMM") + "</span>");
				data.add(row);
			}
			dt.setData(data);
			return dt;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return findDetalhesProdutosTabela(tipo, produto, descricao, bloqueio);
			} else {
				e.printStackTrace();
			}
		}
		return dt;
	}

	public static List<String> getUnidades() {
		String sql = "SELECT ZA_DESC FROM SZA010 WITH(NOLOCK)";
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<String> unidades = new LinkedList<String>();
			while (rs.next()) {
				unidades.add(rs.getString(1));
			}
			return unidades;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getUnidades();
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static List<String> getSegmentos() {
		String sql = "SELECT ZB_DESC FROM SZB010 WITH(NOLOCK)";
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<String> unidades = new LinkedList<String>();
			while (rs.next()) {
				unidades.add(rs.getString(1));
			}
			return unidades;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getSegmentos();
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static List<String> getClasses() {
		String sql = "SELECT ZC_DESC FROM SZC010 WITH(NOLOCK)";
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<String> unidades = new LinkedList<String>();
			while (rs.next()) {
				unidades.add(rs.getString(1));
			}
			return unidades;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getClasses();
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static List<String> getSistemas() {
		String sql = "SELECT ZD_DESC FROM SZD010 WITH(NOLOCK)";
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<String> unidades = new LinkedList<String>();
			while (rs.next()) {
				unidades.add(rs.getString(1));
			}
			return unidades;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getClasses();
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static List<String> getCaracs1() {
		String sql = "SELECT ZE_DESC FROM SZE010 WITH(NOLOCK)";
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<String> unidades = new LinkedList<String>();
			while (rs.next()) {
				unidades.add(rs.getString(1));
			}
			return unidades;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getCaracs1();
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static List<String> getMatsPrimas() {
		String sql = "SELECT ZF_DESC FROM SZF010 WITH(NOLOCK)";
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<String> unidades = new LinkedList<String>();
			while (rs.next()) {
				unidades.add(rs.getString(1));
			}
			return unidades;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getMatsPrimas();
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static List<String> getCaracs2() {
		String sql = "SELECT ZG_DESC FROM SZG010 WITH(NOLOCK)";
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<String> unidades = new LinkedList<String>();
			while (rs.next()) {
				unidades.add(rs.getString(1));
			}
			return unidades;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getCaracs2();
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static Iterable<String> getGrupos() {
		String sql = "SELECT DISTINCT\r\n" + 
				"BM_DESC\r\n" + 
				"FROM SB1010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SBM010 ON B1_GRUPO = BM_GRUPO\r\n" + 
				"AND B1_LOCPAD = '01'\r\n" + 
				"AND SB1010.D_E_L_E_T_ = ''\r\n" + 
				"AND B1_FILIAL <> 'ZZ'\r\n" + 
				"AND B1_TIPO = 'PA'";
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<String> grupos = new LinkedList<String>();
			while (rs.next()) {
				grupos.add(rs.getString(1));
			}
			return grupos;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static boolean validaProduto(String codigoProduto) {
		String sql = "SELECT B1_COD FROM SB1010 WHERE SB1010.D_E_L_E_T_ = '' AND B1_FILIAL <> 'ZZ' AND B1_COD = '" + codigoProduto + "'\r\n";
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static DataTable getAcompanhamentoProduto(Iterable<FollowUpProduto> fups) {
		String sql = "SELECT\r\n" + 
				"	RTRIM(B1_COD) AS B1_COD, B1_DESC, \r\n" +
				"ISNULL((SELECT SUM(D4_QUANT) FROM SD4010 D4 WITH(NOLOCK) INNER JOIN SC2010 C2 WITH(NOLOCK) ON D4.D4_OP = C2.C2_NUM + C2.C2_ITEM + C2.C2_SEQUEN AND C2.D_E_L_E_T_ = '' AND C2.C2_DATRF = '' WHERE B1_COD = D4.D4_COD AND D4.D_E_L_E_T_ = '' AND D4.D4_LOTECTL = ''), 0) AS NUM_QTD_EMPENHO,\r\n" +
				"ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = B1_LOCPAD AND D_E_L_E_T_ = ''), 0) AS NUM_QTD_ESTOQUE," +
				"ISNULL((SELECT SUM(DA_SALDO) FROM SDA010 WITH(NOLOCK) WHERE DA_PRODUTO = B1_COD AND DA_SALDO <> 0 AND D_E_L_E_T_ = '' AND DA_LOCAL = B1_LOCPAD), 0) AS NUM_QTD_ENDERECAR," +
				"ISNULL((SELECT SUM(BF_QUANT - BF_EMPENHO) FROM SBF010 WITH(NOLOCK) WHERE BF_PRODUTO = B1_COD AND BF_LOCAL = '98' AND D_E_L_E_T_ = ''), 0) AS NUM_QTD_INSPECAO,\r\n" +
				"C1_NUM, C1_QUANT, C1_QUJE, C1_EMISSAO, C7_NUM, C7_QUANT, C7_QUJE, C7_EMISSAO, A2_NOME, C2_NUM, C2_QUANT, C2_QUJE, C2_PERDA\r\n" +
				"FROM SB1010 WITH(NOLOCK)\r\n" + 
				"LEFT JOIN SC1010 WITH(NOLOCK) ON B1_COD = C1_PRODUTO AND SC1010.D_E_L_E_T_ = '' AND C1_FILIAL <> 'ZZ' AND C1_QUJE < C1_QUANT AND C1_RESIDUO <> 'S'\r\n" + 
				"LEFT JOIN SC7010 WITH(NOLOCK) ON B1_COD = C7_PRODUTO AND SC7010.D_E_L_E_T_ = '' AND C7_FILIAL <> 'ZZ' AND C7_QUJE < C7_QUANT AND C7_RESIDUO <> 'S'\r\n" + 
				"LEFT JOIN SA2010 WITH(NOLOCK) ON C7_FORNECE = A2_COD AND C7_LOJA = A2_LOJA AND SA2010.D_E_L_E_T_ = '' AND A2_FILIAL <> 'ZZ'\r\n" + 
				"LEFT JOIN SC2010 WITH(NOLOCK) ON B1_COD = C2_PRODUTO AND SC2010.D_E_L_E_T_ = '' AND C2_FILIAL <> 'ZZ' AND C2_QUJE < C2_QUANT AND C2_DATRF = ''\r\n" + 
				"WHERE SB1010.D_E_L_E_T_ = ''\r\n" + 
				"AND B1_FILIAL <> 'ZZ'\r\n" + 
				"AND B1_COD IN (\r\n";
		Iterator<FollowUpProduto> iter = fups.iterator();
		while (iter.hasNext()) {
			FollowUpProduto fup = iter.next();
			sql += "'" + fup.getCodigoProduto() + "'";
			if (iter.hasNext()) {
				sql += ",\r\n";
			}
		}
				
		sql += ")\r\n"; 
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				LinkedList<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					if (i == 1) {
						linha.add(HtmlUtils.createLink("/follow-up-produto/acompanhamento?produto=" + rs.getString(i), rs.getString(i)));
					} else if (i == 7) {
						linha.add(HtmlUtils.createLink("/relacao-scs?numero=" + rs.getString(i) + "&codigoProduto=&solicitante=&emissao=&codigoFornecedor=&fornecedor=&abertos=false", rs.getString(i)));
					} else if (i == 11) {
						linha.add(HtmlUtils.createLink("/relacao-pcs?numero=" + rs.getString(i) + "&nomeFornecedor=&codigoFornecedor=&emissaoDe=&emissaoAte=&codigoProduto=&op=", rs.getString(i)));
					} else if (i == 16) {
						linha.add(HtmlUtils.createLink("http://localhost/relacao-ops?numero=" + rs.getString(i) + "&sequencia=&lote=&codigoProduto=&descricaoProduto=&emissaoDe=&emissaoAte=&armazem=&status=&usuario=&situacao=todas", rs.getString(i)));
					} else {						
						if (rs.getMetaData().getColumnName(i).startsWith("NUM_")) {
							linha.add(rs.getBigDecimal(i).setScale(2, RoundingMode.HALF_EVEN));
						} else {
							linha.add(rs.getString(i));
						}
					}
				}
				data.add(linha);
			}
			
//			for (Collection<Object> collection : data) {
//				LinkedList<Object> tempCollection = (LinkedList<Object>) collection;
//				
//				System.out.println(tempCollection.get(0));
//			}
//			
			
			
			
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return new DataTable();
	}

	public static DataTable getCustos() {
		String sql = "SELECT\r\n" + 
				"B1_COD,\r\n" + 
				"B1_DESC,\r\n" + 
				"B1_LOCPAD,\r\n" + 
				"B1_X_CUST,\r\n" + 
				"DA1_PRCVEN,\r\n" + 
				"CASE WHEN B1_X_CUST = 0 THEN 0 ELSE DA1_PRCVEN / B1_X_CUST END AS MKUP\r\n" + 
				"FROM SB1010 WITH(NOLOCK)\r\n" + 
				"JOIN DA1010 WITH(NOLOCK) ON DA1_CODPRO = B1_COD\r\n" + 
				"AND DA1010.D_E_L_E_T_ = ''\r\n" + 
				"AND DA1_CODTAB = '101'\r\n" + 
				"AND B1_MSBLQL <> '1'\r\n" +
				"WHERE\r\n" +
				"SB1010.D_E_L_E_T_ = ''";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					linha.add(rs.getObject(i));
				}
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static DataTable findByMarkupAbaixo(int mkup) {
		String sql = "SELECT TOP 500 RESULTSET.*, ISNULL(PRCVEN / NULLIF(CUSTO, 0), 0) AS MKUP FROM (\r\n" + 
				"SELECT\r\n" + 
				"B1_COD AS CODIGO, B1_DESC AS DESCRICAO, B1_GRUPO AS GRUPO, ISNULL(B1_X_CUST, 0) AS CUSTO, ISNULL(DA1_PRCVEN, 0) AS PRCVEN,\r\n" + 
				"ISNULL((SELECT SUM(C6_QTDVEN / 4) FROM SC6010 WITH(NOLOCK) INNER JOIN SC5010 WITH(NOLOCK) ON C6_NUM = C5_NUM AND SC5010.D_E_L_E_T_ = '' AND C5_FILIAL <> 'ZZ' AND C5_LIBEROK = 'S' AND C5_TIPO = 'N' WHERE C6_PRODUTO = B1_COD AND SC6010.D_E_L_E_T_ = '' AND C6_BLQ <> 'R' AND C5_EMISSAO BETWEEN DATEADD(MONTH, -4, GETDATE()) AND GETDATE()), 0) AS CMM\r\n" + 
				"FROM SB1010 WITH(NOLOCK)\r\n" + 
				"LEFT JOIN DA1010 ON DA1_CODTAB = '101' AND DA1_CODPRO = B1_COD AND DA1010.D_E_L_E_T_ = '' AND DA1_FILIAL <> 'ZZ'\r\n" + 
				"WHERE SB1010.D_E_L_E_T_ = ''\r\n" + 
				"AND B1_FILIAL <> 'ZZ'\r\n" + 
				"AND B1_LOCPAD = '01'\r\n" + 
				"AND B1_MSBLQL <> '1'\r\n" + 
				") AS RESULTSET\r\n" + 
				"WHERE\r\n" + 
				"((GRUPO NOT IN ('0001', 'KT01') AND PRCVEN / NULLIF(CUSTO, 0) < 1.97) OR ((GRUPO IN ('0001', 'KT01') AND PRCVEN / NULLIF(CUSTO, 0) < 1.8)) OR CUSTO = 0)\r\n" + 
				"ORDER BY MKUP DESC, CMM DESC";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<Object> headers = new LinkedList<Object>();
			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				headers.add(rs.getMetaData().getColumnName(i));
			}
			
			
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					if (rs.getMetaData().getColumnName(i).equals("CUSTO") || rs.getMetaData().getColumnName(i).equals("PRCVEN")) {
						linha.add(CurrencyUtils.format(rs.getBigDecimal(i)));
					} else if (rs.getMetaData().getColumnName(i).equals("MKUP")) {
						linha.add(rs.getBigDecimal(i).setScale(2, RoundingMode.HALF_EVEN));
					} else {
						linha.add(rs.getString(i));
					}
				}
				data.add(linha);
			}
			
			return new DataTable(data, headers);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
