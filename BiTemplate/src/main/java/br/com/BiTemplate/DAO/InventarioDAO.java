package br.com.BiTemplate.DAO;

import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.utils.HtmlUtils;

public class InventarioDAO {

	public static DataTable getInventario(String data, String armazem) {
		String sql = "SELECT UM_MAIS_COLETA - UM_COLETA AS UM_COLETA, (UM_MAIS_COLETA - UM_COLETA) * 100.0 / NUM_MESTRES AS REPRES4, NUM_MESTRES, MESTRE_N_COLETADO, UM_MAIS_COLETA, " +
				"COLETAS_OK, MESTRE_N_COLETADO * 100.0 / NUM_MESTRES AS REPRES1, UM_MAIS_COLETA * 100.0 / NUM_MESTRES AS REPRES2, COLETAS_OK * 100.0 / NUM_MESTRES AS REPRES3 FROM(\r\n" + 
				"SELECT \r\n" + 
				"(SELECT COUNT(CBA_CODINV) FROM CBA010 WITH(NOLOCK) WHERE CBA_LOCAL = '" + armazem + "' AND CBA_DATA = '" + data + "' AND CBA010.D_E_L_E_T_ = '') AS NUM_MESTRES,\r\n" + 
				"(SELECT COUNT(CBA_CODINV) FROM CBA010 WITH(NOLOCK) WHERE CBA_CONTR = 0 AND CBA_DATA = '" + data + "' AND CBA_LOCAL = '" + armazem + "' AND CBA010.D_E_L_E_T_ = '') AS MESTRE_N_COLETADO,\r\n" + 
				"(SELECT COUNT(CBA_CODINV) FROM CBA010 WITH(NOLOCK) WHERE CBA_CONTR > 0 AND CBA_LOCAL = '" + armazem + "' AND CBA_DATA = '" + data + "' AND CBA010.D_E_L_E_T_ = '') AS UM_MAIS_COLETA,\r\n" + 
				"(SELECT COUNT(CBA_CODINV) FROM CBA010 WITH(NOLOCK) WHERE CBA_LOCAL = '" + armazem + "' AND CBA_DATA = '" + data + "' AND CBA010.D_E_L_E_T_ = '' AND CBA_ANALIS = 1) AS COLETAS_OK,\r\n" +
				"(SELECT COUNT(CBA_CODINV) FROM CBA010 WITH(NOLOCK) WHERE CBA_LOCAL = '" + armazem + "' AND CBA_DATA = '" + data + "' AND CBA010.D_E_L_E_T_ = '' AND CBA_CONTR = 1 AND CBA_CONTS = 2) AS UM_COLETA\r\n" +
				") AS RESULTSET";
		
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> tableData = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				linha.add(rs.getString("NUM_MESTRES"));
				linha.add(HtmlUtils.createLink("/inventario/mestres-pendentes", rs.getString("MESTRE_N_COLETADO")));
				linha.add(rs.getBigDecimal("REPRES1").setScale(2, RoundingMode.HALF_EVEN) + "%");
				linha.add(rs.getString("UM_MAIS_COLETA"));
				linha.add(rs.getBigDecimal("REPRES2").setScale(2, RoundingMode.HALF_EVEN) + "%");
				linha.add(rs.getString("UM_COLETA"));
				linha.add(rs.getBigDecimal("REPRES4").setScale(2, RoundingMode.HALF_EVEN) + "%");
				linha.add(rs.getString("COLETAS_OK"));
				linha.add(rs.getBigDecimal("REPRES3").setScale(2, RoundingMode.HALF_EVEN) + "%");
				tableData.add(linha);
			}
			return new DataTable(tableData);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static DataTable getMestresPendentes(String data, String armazem) {
		String sql = "SELECT CBA_CODINV, CBA_LOCALI FROM CBA010 WITH(NOLOCK) WHERE CBA_CONTR = 0 AND CBA_DATA = '" + data + "' AND CBA_LOCAL = '" + armazem + "' AND CBA010.D_E_L_E_T_ = ''";
		
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> tableData = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				linha.add(rs.getString("CBA_CODINV"));
				linha.add(rs.getString("CBA_LOCALI"));
				tableData.add(linha);
			}
			return new DataTable(tableData);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
