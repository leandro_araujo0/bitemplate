package br.com.BiTemplate.DAO;

import java.math.BigDecimal;

import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.ChartDataAndLabels;
import br.com.BiTemplate.utils.MetaUtils;

public class DatabaseMetas {

	public static ChartDataAndLabels getMetas(UNIDADE unidade) {
		ChartDataAndLabels cdl = new ChartDataAndLabels();
		if (unidade.equals(UNIDADE.ORTOPEDIA)) { 
			cdl.getChartData().add(new BigDecimal(5115776.22));//Meta Janeiro
			cdl.getChartData().add(new BigDecimal(4507513.71));//Meta Fevereiro
			cdl.getChartData().add(new BigDecimal(5512994.84));//Meta Março
			cdl.getChartData().add(new BigDecimal(4964463.04));//Meta Abril
			cdl.getChartData().add(new BigDecimal(5156004.13));//Meta Maio
			cdl.getChartData().add(new BigDecimal(5775807.72));//Meta Junho
			cdl.getChartData().add(new BigDecimal(4915167.59));//Meta Julho
			cdl.getChartData().add(new BigDecimal(6442706.85));//Meta Agosto
			cdl.getChartData().add(new BigDecimal(4698223.93));//Meta Setembro
			cdl.getChartData().add(new BigDecimal(4751597.31));//Meta Outubro
			cdl.getChartData().add(new BigDecimal(4759006.76));//Meta Novembro
			cdl.getChartData().add(new BigDecimal(5400737.91));//Meta Dezembro
		} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
			cdl.getChartData().add(new BigDecimal(2915092.74));//Meta Janeiro
			cdl.getChartData().add(BigDecimal.valueOf(2012248.57));//Meta Fevereiro
			cdl.getChartData().add(BigDecimal.valueOf(1330070.31));//Meta Março
			cdl.getChartData().add(BigDecimal.valueOf(1217348.25));//Meta Abril
			cdl.getChartData().add(BigDecimal.valueOf(1828966.41));//Meta Maio
			cdl.getChartData().add(BigDecimal.valueOf(2291227.96));//Meta Junho
			cdl.getChartData().add(BigDecimal.valueOf(2346559.82));//Meta Julho
			cdl.getChartData().add(BigDecimal.valueOf(2310131.83));//Meta Agosto
			cdl.getChartData().add(BigDecimal.valueOf(2617612.12));//Meta Setembro
			cdl.getChartData().add(BigDecimal.valueOf(2662001.62));//Meta Outubro
			cdl.getChartData().add(BigDecimal.valueOf(2490603.41));//Meta Novembro
			cdl.getChartData().add(BigDecimal.valueOf(2978136.94));//Meta Dezembro
		} else if (unidade.equals(UNIDADE.EXPORTACAO)) {
			cdl.getChartData().add(new BigDecimal(1039657.75));//Meta Janeiro
			cdl.getChartData().add(BigDecimal.valueOf(916043.11));//Meta Fevereiro
			cdl.getChartData().add(BigDecimal.valueOf(1120382.82));//Meta Março
			cdl.getChartData().add(BigDecimal.valueOf(1008907));//Meta Abril
			cdl.getChartData().add(BigDecimal.valueOf(1047833.1));//Meta Maio
			cdl.getChartData().add(BigDecimal.valueOf(1173793.18));//Meta Junho
			cdl.getChartData().add(BigDecimal.valueOf(998888.9));//Meta Julho
			cdl.getChartData().add(BigDecimal.valueOf(1309324.29));//Meta Agosto
			cdl.getChartData().add(BigDecimal.valueOf(954800.35));//Meta Setembro
			cdl.getChartData().add(BigDecimal.valueOf(965647.19));//Meta Outubro
			cdl.getChartData().add(BigDecimal.valueOf(967152.99));//Meta Novembro
			cdl.getChartData().add(BigDecimal.valueOf(1097569.32));//Meta Dezembro
		}
		return cdl;
	}

	public static ChartDataAndLabels getMetasRecalc(UNIDADE unidade) {
		ChartDataAndLabels cdlMetas = getMetas(unidade);
		ChartDataAndLabels cdl = new ChartDataAndLabels();
		if (unidade.equals(UNIDADE.ORTOPEDIA)) {
			cdl.getChartData().add(new BigDecimal(5115776.22 * .9));//Meta Janeiro
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 2, 4507513.71 *  .8748));//Meta Fevereiro
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 3, 5512994.84 *  .8748));//Meta Março
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 4, 4964463.04 *  .8748));//Meta Abril
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 5, 5156004.13 *  .8748));//Meta Maio
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 6, 5775807.72 *  .8748));//Meta Junho
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 7, 4915167.59 *  .8748));//Meta Julho
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 8, 6442706.85 *  .8748));//Meta Agosto
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 9, 4698223.93 *  .8748));//Meta Setembro
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 10, 4751597.31 * .8748));//Meta Outubro
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 11, 4759006.76 * .8748));//Meta Novembro
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 12, 5400737.91 * .8748));//Meta Dezembro
		} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
			cdl.getChartData().add(new BigDecimal(2915092.74));//Meta Janeiro
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 2, 2012248.57));//Meta Fevereiro
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 3, 1330070.31));//Meta Março
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 4, 1217348.25));//Meta Abril
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 5, 1828966.41));//Meta Maio
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 6, 2291227.96));//Meta Junho
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 7, 2346559.82));//Meta Julho
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 8, 2310131.83));//Meta Agosto
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 9, 2617612.12));//Meta Setembro
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 10, 2662001.62));//Meta Outubro
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 11, 2490603.41));//Meta Novembro
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 12, 2978136.94));//Meta Dezembro
		} else if (unidade.equals(UNIDADE.EXPORTACAO)) {
			cdl.getChartData().add(new BigDecimal(1039657.75));//Meta Janeiro
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 2, 916043.11   * .6232));//Meta Fevereiro
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 3, 1120382.82  * .6232));//Meta Março
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 4, 1008907     * .6232));//Meta Abril
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 5, 1047833.1   * .6232));//Meta Maio
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 6, 1173793.18  * .6232));//Meta Junho
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 7, 998888.9    * .6232));//Meta Julho
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 8, 1309324.29  * .6232));//Meta Agosto
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 9, 954800.35   * .6232));//Meta Setembro
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 10, 965647.19  * .6232));//Meta Outubro
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 11, 967152.99  * .6232));//Meta Novembro
			cdl.getChartData().add(MetaUtils.recalcula(cdl, cdlMetas, 12, 1097569.32 * .6232));//Meta Dezembro
		}
		return cdl;
	}

	public static BigDecimal getMetaRecalc(UNIDADE unidade, int mes) {
		ChartDataAndLabels cdl = getMetasRecalc(unidade);
		for (int i = 0; i <= mes; i++) {
			if (i == mes) {
				return (BigDecimal) cdl.getChartData().toArray()[i];
			}
		}
		return null;
	}

}
