package br.com.BiTemplate.DAO;

import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.ChartDataAndLabels;
import br.com.BiTemplate.model.DataTable;

public class RecursosHumanosDAO {

	public static ChartDataAndLabels getGraficoEscolaridade(UNIDADE unidade) {
		ChartDataAndLabels cdl = new ChartDataAndLabels();
		String sql = "SELECT\r\n" + "RTRIM(X5_DESCRI) AS X5_DESCRI,COUNT(X5_DESCRI) AS QTD_CARGOS\r\n"
				+ "FROM SRA010\r\n" + "JOIN SX5010 ON RA_GRINRAI = X5_CHAVE\r\n" + "AND X5_TABELA = '26'\r\n"
				+ "AND SX5010.D_E_L_E_T_ = ''\r\n" + "WHERE SRA010.D_E_L_E_T_ = ''\r\n";
		if (unidade.equals(UNIDADE.ORTOPEDIA)) {
			sql += "AND RA_FILIAL = '02'\r\n";
		} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
			sql += "AND RA_FILIAL = '01'\r\n";
		}
		sql += "AND RA_DEMISSA = ''\r\n" + "GROUP BY X5_DESCRI\r\n" + "ORDER BY 2 DESC";

		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString(1));
				cdl.getChartData().add(rs.getBigDecimal(2));
			}
			return cdl;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static DataTable getGraficoEscolaridadeMensal(UNIDADE unidade) {
		String sql = "SELECT RESULTSET.*, X5_DESCRI, COUNT(X5_DESCRI) AS CONTAGEM FROM (\r\n" + "SELECT\r\n"
				+ "DISTINCT LEFT(C5_EMISSAO, 6) AS MES\r\n" + "FROM SC5010 WITH(NOLOCK)\r\n" + "WHERE \r\n"
				+ "SC5010.D_E_L_E_T_ = ''\r\n" + "AND C5_EMISSAO >= DATEADD(MONTH, -12, GETDATE())\r\n"
				+ ") AS RESULTSET\r\n"
				+ "JOIN SRA010 WITH(NOLOCK) ON SRA010.D_E_L_E_T_ = '' AND (LEFT(RA_DEMISSA, 6) > MES OR RA_DEMISSA = '')\r\n";
		if (unidade.equals(UNIDADE.ORTOPEDIA)) {
			sql += "AND RA_FILIAL = '02'\r\n";
		} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
			sql += "AND RA_FILIAL = '01'\r\n";
		}
		sql += "JOIN SX5010 WITH(NOLOCK) ON RA_GRINRAI = X5_CHAVE AND X5_TABELA = '26' AND SX5010.D_E_L_E_T_ = ''\r\n"
				+ "GROUP BY MES, X5_DESCRI\r\n" + "ORDER BY 2, 1";

		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();

			LinkedList<Object> headers = new LinkedList<Object>();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();

			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				headers.add(rs.getMetaData().getColumnName(i));
			}

			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					linha.add(rs.getString(i));
				}
				data.add(linha);
			}
			return new DataTable(data, headers);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static DataTable getEscolaridadeAnalitico(UNIDADE unidade, String grauEscolaridade, String customWhere) {
		String sql = "SELECT\r\n"
				+ "CASE RA_FILIAL WHEN '01' THEN 'EQUIPAMENTOS' WHEN '02' THEN 'ORTOPEDIA' END AS UNIDADE, RA_NOME, RA_ADMISSA, DATEDIFF(MONTH, RA_ADMISSA, GETDATE()) AS MESES_EMPRESA, RJ_DESC, X5_DESCRI, RA_UFCP, RA_EMAIL\r\n"
				+ "FROM SRA010 WITH(NOLOCK)\r\n"
				+ "JOIN SX5010 WITH(NOLOCK) ON RA_GRINRAI = X5_CHAVE AND X5_TABELA = '26' AND SX5010.D_E_L_E_T_ = ''\r\n"
				+ "JOIN SRJ010 WITH(NOLOCK) ON RJ_FUNCAO = RA_CODFUNC AND SRJ010.D_E_L_E_T_ = '' AND RJ_FILIAL <> 'ZZ'\r\n"
				+ "WHERE SRA010.D_E_L_E_T_ = ''\r\n" + "AND RA_DEMISSA = ''\r\n";
		if (unidade == UNIDADE.ORTOPEDIA) {
			sql += "AND RA_FILIAL = '02'\r\n";
		} else if (unidade == UNIDADE.EQUIPAMENTOS) {
			sql += "AND RA_FILIAL = '01'\r\n";
		}
		if (grauEscolaridade != null && !grauEscolaridade.equals("")) {
			sql += "AND X5_DESCRI = '" + grauEscolaridade + "'\r\n";
		}
		if (customWhere != null) {
			sql += "AND " + customWhere + "\r\n";
		}
		sql += "ORDER BY 2, 1";

		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();

			LinkedList<Object> headers = new LinkedList<Object>();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();

			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				headers.add(rs.getMetaData().getColumnName(i));
			}

			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					linha.add(rs.getString(i));
				}
				data.add(linha);
			}
			return new DataTable(data, headers);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static DataTable getTabelaRotatividade(UNIDADE unidade) {
		String sql = 
				"DECLARE @FILIAL AS VARCHAR(255)\r\n";
		if (unidade.equals(UNIDADE.ORTOPEDIA)) {
			sql += "SET @FILIAL = '02'\r\n";
		} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
			sql += "SET @FILIAL = '01'\r\n";
		}
				sql += "	SELECT *, ((ADMISSOES + DEMISSOES) / 2.0 / QTD_FUNCIONARIOS * 100) AS TURNOVER FROM (\r\n" + 
				"	SELECT RESULTSET.*,\r\n" + 
				"		ISNULL((SELECT SUM(CASE WHEN RA_NOME IS NULL THEN 0 ELSE 1 END) AS QTD FROM SRA010 WHERE RA_FILIAL = @FILIAL AND SRA010.D_E_L_E_T_ = '' AND LEFT(RA_ADMISSA, 6) = PERIODO), 0) AS ADMISSOES,\r\n" + 
				"		ISNULL((SELECT SUM(CASE WHEN RA_NOME IS NULL THEN 0 ELSE 1 END) AS QTD FROM SRA010 WHERE RA_FILIAL = @FILIAL AND SRA010.D_E_L_E_T_ = '' AND LEFT(RA_DEMISSA, 6) = PERIODO), 0) AS DEMISSOES,\r\n" + 
				"		ISNULL((SELECT SUM(CASE WHEN RA_NOME IS NULL THEN 0 ELSE 1 END) AS QTD FROM SRA010 WHERE RA_FILIAL = @FILIAL AND SRA010.D_E_L_E_T_ = '' AND (LEFT(RA_DEMISSA, 6) >= PERIODO OR LEFT(RA_DEMISSA, 6) = '') AND LEFT(RA_ADMISSA, 6) < PERIODO), 0) AS QTD_FUNCIONARIOS\r\n" + 
				"	FROM (\r\n" + 
				"		SELECT DISTINCT\r\n" + 
				"			LEFT(C5_EMISSAO,6) AS PERIODO\r\n" + 
				"		FROM SC5010\r\n" + 
				"		WHERE C5_EMISSAO >= DATEADD(MONTH, -12, GETDATE())\r\n" + 
				"	) AS RESULTSET\r\n" + 
				") AS RESULTSET2\r\n" + 
				"ORDER BY 1, 2, 3, 4";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			LinkedList<Object> headers = new LinkedList<Object>();
			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				headers.add(rs.getMetaData().getColumnName(i));
			}
			
			
			
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					if (rs.getMetaData().getColumnName(i).equals("TURNOVER")) {
						linha.add(rs.getBigDecimal(i).setScale(2, RoundingMode.HALF_EVEN).toString());
					} else {
						linha.add(rs.getObject(i));
					}
				}
				data.add(linha);
			}
			return new DataTable(data, headers);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static DataTable getFuncionarios(UNIDADE unidade, String tipo, String periodo, String dataDe, String dataAte) {
		String sql = "SELECT\r\n"
				+ "CASE RA_FILIAL WHEN '01' THEN 'EQUIPAMENTOS' WHEN '02' THEN 'ORTOPEDIA' END AS UNIDADE, RA_NOME, RA_ADMISSA, RA_DEMISSA, DATEDIFF(MONTH, RA_ADMISSA, GETDATE()) AS MESES_EMPRESA, RJ_DESC, X5_DESCRI, RA_UFCP, RA_EMAIL\r\n"
				+ "FROM SRA010 WITH(NOLOCK)\r\n"
				+ "JOIN SX5010 WITH(NOLOCK) ON RA_GRINRAI = X5_CHAVE AND X5_TABELA = '26' AND SX5010.D_E_L_E_T_ = ''\r\n"
				+ "JOIN SRJ010 WITH(NOLOCK) ON RJ_FUNCAO = RA_CODFUNC AND SRJ010.D_E_L_E_T_ = '' AND RJ_FILIAL <> 'ZZ'\r\n"
				+ "WHERE SRA010.D_E_L_E_T_ = ''\r\n";
		if (unidade == UNIDADE.ORTOPEDIA) {
			sql += "AND RA_FILIAL = '02'\r\n";
		} else if (unidade == UNIDADE.EQUIPAMENTOS) {
			sql += "AND RA_FILIAL = '01'\r\n";
		}
		if (tipo != null && tipo.equals("ADMISSAO")) {
			sql += "AND LEFT(RA_ADMISSA, 6) = '" + periodo + "'\r\n";
		} else if (tipo.equals("DEMISSAO")) {
			sql += "AND LEFT(RA_DEMISSA, 6) = '" + periodo + "'\r\n";
		}
		sql += "ORDER BY 2, 1";

		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();

			LinkedList<Object> headers = new LinkedList<Object>();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();

			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				headers.add(rs.getMetaData().getColumnName(i));
			}

			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					linha.add(rs.getString(i));
				}
				data.add(linha);
			}
			return new DataTable(data, headers);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
