package br.com.BiTemplate.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.utils.HtmlUtils;

public class SolicitacaoComprasDAO {

	public static DataTable getRelacaoScs(String codigoProduto, String emissao, String fornecedor, String codigoFornecedor, String numero, String solicitante, boolean abertos) {
		String sql = "SELECT\r\n" + 
				"	C1_NUM, C1_SOLICIT, C1_PRODUTO, C1_QUANT, C1_QUJE, C1_EMISSAO, C1_DATPRF, C1_FORNECE, A2_NOME, B1_DESC, C1_LOCAL, C1_CODCOMP\r\n" + 
				"FROM SC1010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN SB1010 WITH(NOLOCK) ON C1_PRODUTO = B1_COD\r\n" +
				"AND SB1010.D_E_L_E_T_ = ''\r\n" +
				"LEFT JOIN SA2010 WITH(NOLOCK) ON C1_FORNECE = A2_COD\r\n" + 
				"WHERE\r\n" + 
				"	SC1010.D_E_L_E_T_=''\r\n" + 
				"	AND C1_RESIDUO <> 'S'\r\n";
				if (abertos) {
					sql += "	AND C1_QUANT > C1_QUJE\r\n";
				}
				if (codigoProduto != null && !codigoProduto.equals("")) {
					sql += "AND C1_PRODUTO = '" + codigoProduto + "'\r\n";
				}
				if (emissao != null && !emissao.equals("")) {
					sql += "AND C1_EMISSAO = '" + emissao + "'\r\n";
				}
				if (fornecedor != null && !fornecedor.equals("")) {
					sql += "AND A2_NOME LIKE '" + fornecedor + "'\r\n";
				}
				if (codigoFornecedor != null && !codigoFornecedor.equals("")) {
					sql += "AND C1_FORNECE = '" + codigoFornecedor + "'\r\n";
				}
				if (numero != null && !numero.equals("")) {
					sql += "AND C1_NUM = '" + numero + "'\r\n";
				}
				if (solicitante != null && !solicitante.equals("")) {
					sql += "AND C1_SOLICIT LIKE '%" + solicitante + "%'";
				}
				sql += "ORDER BY C1_NUM";
				
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			Collection<Collection<Object>> data = new LinkedList<Collection<Object>>();
			while (rs.next()) {
				Collection<Object> linha = new LinkedList<Object>();
				linha.add(HtmlUtils.createLink("/relacao-scs?numero=" + rs.getString("C1_NUM"), rs.getString("C1_NUM")));
				linha.add(rs.getString("C1_SOLICIT"));
				linha.add(rs.getString("C1_PRODUTO"));
				linha.add(rs.getString("B1_DESC"));
				linha.add(rs.getString("C1_LOCAL"));
				linha.add(rs.getString("C1_QUANT"));
				linha.add(HtmlUtils.createLink("/relacao-pcs?sc=" + rs.getString("C1_NUM") + "&codigoProduto=" + rs.getString("C1_PRODUTO"), rs.getString("C1_QUJE")));
				linha.add(rs.getString("C1_EMISSAO"));
				linha.add(rs.getString("C1_DATPRF"));
				linha.add(rs.getString("C1_CODCOMP"));
				linha.add(rs.getString("C1_FORNECE"));
				linha.add(rs.getString("A2_NOME"));
				data.add(linha);
			}
			return new DataTable(data);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getStatus(String sc) {
		//TODO Devolver o status da SC
		String sql = "SELECT\r\n" + 
				"SUM(C1_QUANT - C1_QUJE) AS SC_PENDENTES, SUM(C7_QUJE - C7_QUANT) AS PC_PENDENTES\r\n" + 
				"FROM SC1010\r\n" + 
				"LEFT JOIN SC7010 ON C7_NUMSC = C1_NUM AND C7_ITEMSC = C1_ITEM AND C7_FILIAL <> 'ZZ' AND SC7010.D_E_L_E_T_ = ''\r\n" + 
				"WHERE C1_NUM = '" + sc + "'\r\n" + 
				"AND C1_FILIAL <> 'ZZ'\r\n" + 
				"AND SC1010.D_E_L_E_T_ = ''";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				long scPendentes = rs.getLong("SC_PENDENTES");
				long pcPendentes = rs.getLong("PC_PENDENTES");
				
				if (scPendentes > 0) {
					return "SCPENDENTE";
				} else if (pcPendentes > 0) {
					return "PCPENDENTE";
				} else {
					return "PCENTREGUE";
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
