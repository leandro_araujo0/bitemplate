package br.com.BiTemplate.DAO;

import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.model.ChartDataAndLabels;

public class ProducaoDAO {

	public static ChartDataAndLabels getProducaoAnual(int ano, UNIDADE unidade) {
		String sql = "SELECT\r\n" + 
				"SUBSTRING(D3_EMISSAO, 1, 6) AS EMISSAO, SUM(D3_QUANT * DA1_PRCVEN) AS VALOR\r\n" + 
				"FROM SD3010 WITH(NOLOCK)\r\n" + 
				"INNER JOIN DA1010 WITH(NOLOCK) ON DA1_CODPRO = D3_COD\r\n";
				if (unidade.equals(UNIDADE.ORTOPEDIA)) {
					sql += "AND DA1_CODTAB = '083'\r\n"; 
				} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
					sql += "AND DA1_CODTAB = '100'\r\n"; 
				}
				sql += "AND DA1010.D_E_L_E_T_ = ''\r\n" + 
				"AND DA1_FILIAL = ''\r\n" + 
				"WHERE YEAR(D3_EMISSAO) = ?\r\n" + 
				"AND D3_LOCAL = ?\r\n" + 
				"AND D3_TM = '010'\r\n" + 
				"AND D3_OP <> ''\r\n" + 
				"AND D3_ESTORNO <> 'S'\r\n" + 
				"AND D3_FILIAL = '01'\r\n" + 
				"AND SD3010.D_E_L_E_T_ = ''\r\n" + 
				"GROUP BY SUBSTRING(D3_EMISSAO, 1, 6)\r\n" + 
				"ORDER BY EMISSAO";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setInt(1, ano);
			if (unidade.equals(UNIDADE.ORTOPEDIA)) {
				stmt.setString(2, "01");
			} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
				stmt.setString(2, "12");
			}
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
				cdl.getChartLabels().add(rs.getString("EMISSAO"));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getProducaoAnual(ano, unidade);
			} else {
				e.printStackTrace();
			}
		}
		
		return null;
	}

	public static ChartDataAndLabels getCargaMaquina() {
		String sql = "SELECT G2_RECURSO, SUM(SALDO_OP * TEMPO_CALC) AS CARGA FROM (	\r\n" + 
				"	SELECT\r\n" + 
				"	G2_RECURSO,\r\n" + 
				"	C2_QUANT - H6_QTDPROD - C2_PERDA AS SALDO_OP,\r\n" + 
				"	G2_TEMPAD,\r\n" + 
				"	G2_LOTEPAD,\r\n" + 
				"	(FLOOR(G2_TEMPAD) + ((ABS(G2_TEMPAD) - FLOOR(G2_TEMPAD)) / 60)) / CASE G2_LOTEPAD WHEN '0' THEN '1' ELSE G2_LOTEPAD END AS TEMPO_CALC\r\n" + 
				"	FROM SC2010 WITH(NOLOCK)\r\n" + 
				"	INNER JOIN SH6010 WITH(NOLOCK) ON H6_OP = C2_NUM + C2_ITEM + C2_SEQUEN\r\n" + 
				"	AND H6_FILIAL = '01'\r\n" + 
				"	AND SH6010.D_E_L_E_T_ = ''\r\n" + 
				"	INNER JOIN SG2010 WITH(NOLOCK) ON G2_PRODUTO = H6_PRODUTO\r\n" + 
				"	AND G2_RECURSO = H6_RECURSO\r\n" + 
				"	AND G2_OPERAC = H6_OPERAC\r\n" + 
				"	AND SG2010.D_E_L_E_T_ = ''\r\n" + 
				"	AND G2_FILIAL = '01'\r\n" + 
				"	WHERE C2_DATRF = ''\r\n" + 
				"	AND SC2010.D_E_L_E_T_ = ''\r\n" + 
				"	AND C2_FILIAL = '01'\r\n" + 
				"	AND C2_QUANT > C2_QUJE\r\n" + 
				") AS RESULTSET\r\n" + 
				"GROUP BY G2_RECURSO\r\n" + 
				"ORDER BY G2_RECURSO";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartLabels().add(rs.getString("G2_RECURSO"));
				cdl.getChartData().add(rs.getBigDecimal("CARGA"));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getCargaMaquina();
			} else {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static ChartDataAndLabels getProducaoAnualHorizontal(int ano, UNIDADE unidade) {
		String sql = "SELECT\r\n" + 
				"SUBSTRING(D3_EMISSAO, 1, 4) AS EMISSAO, SUM(D3_QUANT * DA1_PRCVEN) AS VALOR\r\n" + 
				"FROM SD3010 WITH(NOLOCK)\r\n" + 
				"LEFT JOIN DA1010 WITH(NOLOCK) ON DA1_CODPRO = D3_COD\r\n";
				if (unidade.equals(UNIDADE.ORTOPEDIA)) {
					sql += "AND DA1_CODTAB = '083'\r\n"; 
				} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
					sql += "AND DA1_CODTAB = '100'\r\n"; 
				}
				sql += "AND DA1010.D_E_L_E_T_ = ''\r\n" + 
				"AND DA1_FILIAL = ''\r\n" + 
				"WHERE YEAR(D3_EMISSAO) = ?\r\n" + 
				"AND D3_LOCAL = ?\r\n" + 
				"AND D3_TM = '010'\r\n" + 
				"AND D3_OP <> ''\r\n" + 
				"AND D3_ESTORNO <> 'S'\r\n" + 
				"AND D3_FILIAL = '01'\r\n" + 
				"AND SD3010.D_E_L_E_T_ = ''\r\n" + 
				"GROUP BY SUBSTRING(D3_EMISSAO, 1, 4)\r\n" + 
				"ORDER BY EMISSAO";
		
		try (PreparedStatement stmt = Database.getConnection().prepareStatement(sql)) {
			stmt.setInt(1, ano);
			if (unidade.equals(UNIDADE.ORTOPEDIA)) {
				stmt.setString(2, "01");
			} else if (unidade.equals(UNIDADE.EQUIPAMENTOS)) {
				stmt.setString(2, "12");
			}
			ResultSet rs = stmt.executeQuery();
			ChartDataAndLabels cdl = new ChartDataAndLabels();
			while (rs.next()) {
				cdl.getChartData().add(rs.getBigDecimal("VALOR").setScale(2, RoundingMode.HALF_EVEN));
				cdl.getChartLabels().add(rs.getString("EMISSAO"));
			}
			return cdl;
		} catch (SQLException e) {
			if (e.getErrorCode() == 1205 && e.getSQLState().equals("40001")) {
				return getProducaoAnualHorizontal(ano, unidade);
			} else {
				e.printStackTrace();
			}
		}
		
		return null;
	}

}
