package br.com.BiTemplate.model;

import java.util.Collection;
import java.util.LinkedList;

public class Estrutura {
	
	private Collection<ItemEstrutura> itensEstrutura;
	
	public Estrutura() {
		this.itensEstrutura = new LinkedList<>();
	}

	public Collection<ItemEstrutura> getItensEstrutura() {
		return itensEstrutura;
	}

	public void setItensEstrutura(Collection<ItemEstrutura> itensEstrutura) {
		this.itensEstrutura = itensEstrutura;
	}

	public void add(ItemEstrutura itemEstrutura) {
		this.itensEstrutura.add(itemEstrutura);
	}

}
