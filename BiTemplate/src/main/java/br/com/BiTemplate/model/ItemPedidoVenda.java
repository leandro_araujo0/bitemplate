package br.com.BiTemplate.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.utils.CurrencyUtils;

public class ItemPedidoVenda {

	private Produto produto;
	private BigDecimal quantidade;
	private PedidoVenda pedido;
	private String cliente;
	private String codCliente;
	private BigDecimal qtdVen;
	private BigDecimal qtdEnt;
	private BigDecimal prcVen;
	private UNIDADE unidade;
	private String item;
	private BigDecimal qtdEmp;
	private BigDecimal custo;
	private String blq;
	private String xDesc;
	private String tipo;
	private String lote;

	public ItemPedidoVenda() {
		this.qtdVen = BigDecimal.ZERO;
		this.qtdEnt = BigDecimal.ZERO;
		this.prcVen = BigDecimal.ZERO;
	}

	public UNIDADE getUnidade() {
		return unidade;
	}

	public void setUnidade(UNIDADE unidade) {
		this.unidade = unidade;
	}

	public String getItem() {
		return item;
	}

	public BigDecimal getQtdVen() {
		return qtdVen;
	}

	public BigDecimal getQtdEnt() {
		return qtdEnt;
	}

	public BigDecimal getPrcVen() {
		return prcVen;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}

	public void setPedido(PedidoVenda pedido) {
		this.pedido = pedido;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public BigDecimal getQuantidade() {
		return quantidade;
	}

	public String getCliente() {
		return cliente;
	}

	public BigDecimal getValor() {
		return this.prcVen.multiply(this.qtdVen.subtract(this.qtdEnt));
	}

	public BigDecimal getSaldo() {
		if (this.qtdEmp != null)
			return this.qtdVen.subtract(this.qtdEnt).subtract(this.qtdEmp);
		else
			return this.qtdVen.subtract(this.qtdEnt);
	}

	public String getValorCurrency() {
		DecimalFormat decimalFormat = new DecimalFormat("R$ ##,###,###,##0.00",
				new DecimalFormatSymbols(new Locale("pt", "BR")));
		decimalFormat.setMinimumFractionDigits(2);
		decimalFormat.setParseBigDecimal(true);
		return decimalFormat.format(getValor());
	}

	public void setItem(String item) {
		this.item = item;
	}

	public void setQtdVen(BigDecimal qtdVen) {
		this.qtdVen = qtdVen;
	}

	public void setQtdEnt(BigDecimal qtdEnt) {
		this.qtdEnt = qtdEnt;
	}

	public void setPrcVen(BigDecimal prcVen) {
		this.prcVen = prcVen;
	}

	public BigDecimal getValorTotal() {
		return this.prcVen.multiply(this.qtdVen).setScale(2, RoundingMode.HALF_EVEN);
	}

	public String getValorAsCurrency() {
		return CurrencyUtils.format(this.getValor());
	}

	public String getCustoAsCurrency() {
		return CurrencyUtils.format(this.custo);
	}

	public String getPrcVenAsCurrency() {
		return CurrencyUtils.format(this.getPrcVen());
	}

	public String getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(String codCliente) {
		this.codCliente = codCliente;
	}

	public void setQtdEmp(BigDecimal qtdEmp) {
		this.qtdEmp = qtdEmp;
	}

	public BigDecimal getQtdEmp() {
		return qtdEmp;
	}

	public PedidoVenda getPedido() {
		return pedido;
	}

	public String getTotalPedidos(List<ItemPedidoVenda> itensPedidosVenda) {
		BigDecimal valorTotal = BigDecimal.ZERO;
		for (ItemPedidoVenda itemPedidoVenda : itensPedidosVenda) {
			valorTotal = valorTotal.add(itemPedidoVenda.getValorTotal());
		}
		return CurrencyUtils.format(valorTotal);
	}

	public String getValorTotalAsCurrency() {
		return CurrencyUtils.format(this.qtdVen.multiply(this.prcVen));
	}

	public boolean eFinalizado() {
		if (this.eFaturado() || this.eResiduado()) {
			return true;
		}
		return false;
	}

	private boolean eResiduado() {
		if (this.blq.equals("R ")) {
			return true;
		}
		return false;
	}

	public boolean eFaturado() {
		if (this.qtdVen.compareTo(this.qtdEnt) == 0) {
			return true;
		} else {
			return false;
		}
	}

	public void setCusto(BigDecimal custo) {
		this.custo = custo;
	}

	public BigDecimal getCusto() {
		return custo;
	}

	public String getBlq() {
		return blq;
	}

	public void setBlq(String blq) {
		this.blq = blq;
	}

	public void setXDesc(String xDesc) {
		this.xDesc = xDesc;
	}

	public String getxDesc() {
		return xDesc;
	}

	public void setxDesc(String xDesc) {
		this.xDesc = xDesc;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public String getLote() {
		return lote;
	}

}
