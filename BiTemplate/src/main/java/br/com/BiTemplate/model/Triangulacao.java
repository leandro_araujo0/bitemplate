package br.com.BiTemplate.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Triangulacao {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private double qtd;
	private String scChapa;
	private String pcChapa;
	private String opPolim;
	private String scPolim;
	private String pcPolim;
	private String opCorteDobra;
	private String scCorteDobra;
	private String pcCorteDobra;
	private String nfEntrada;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getQtd() {
		return qtd;
	}

	public void setQtd(double qtd) {
		this.qtd = qtd;
	}

	public String getScChapa() {
		return scChapa;
	}

	public void setScChapa(String scChapa) {
		this.scChapa = scChapa;
	}

	public String getPcChapa() {
		return pcChapa;
	}

	public void setPcChapa(String pcChapa) {
		this.pcChapa = pcChapa;
	}

	public String getOpPolim() {
		return opPolim;
	}

	public void setOpPolim(String opPolim) {
		this.opPolim = opPolim;
	}

	public String getScPolim() {
		return scPolim;
	}

	public void setScPolim(String scPolim) {
		this.scPolim = scPolim;
	}

	public String getPcPolim() {
		return pcPolim;
	}

	public void setPcPolim(String pcPolim) {
		this.pcPolim = pcPolim;
	}

	public String getOpCorteDobra() {
		return opCorteDobra;
	}

	public void setOpCorteDobra(String opCorteDobra) {
		this.opCorteDobra = opCorteDobra;
	}

	public String getScCorteDobra() {
		return scCorteDobra;
	}

	public void setScCorteDobra(String scCorteDobra) {
		this.scCorteDobra = scCorteDobra;
	}

	public String getPcCorteDobra() {
		return pcCorteDobra;
	}

	public void setPcCorteDobra(String pcCorteDobra) {
		this.pcCorteDobra = pcCorteDobra;
	}

	public String getNfEntrada() {
		return nfEntrada;
	}

	public void setNfEntrada(String nfEntrada) {
		this.nfEntrada = nfEntrada;
	}
}
