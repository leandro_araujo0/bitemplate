package br.com.BiTemplate.model;

import java.util.Collection;
import java.util.LinkedList;

public class DataTable {

	private Collection<Collection<Object>> data;
	private Collection<Object> headers;

	public DataTable() {
		this.data = new LinkedList<Collection<Object>>();
		this.headers = new LinkedList<Object>();
	}

	public DataTable(Collection<Collection<Object>> data) {
		this.data = data;
		this.headers = new LinkedList<Object>();
	}

	public DataTable(Collection<Collection<Object>> data, LinkedList<Object> headers) {
		this.data = data;
		this.headers = headers;
	}

	public DataTable setData(Collection<Collection<Object>> data) {
		this.data = data;
		return this;
	}

	public Collection<Collection<Object>> getData() {
		return data;
	}

	public DataTable setHeaders(Collection<Object> headers) {
		this.headers = headers;
		return this;
	}

	public Collection<Object> getHeaders() {
		return headers;
	}

}
