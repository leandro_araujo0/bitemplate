package br.com.BiTemplate.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;

import br.com.BiTemplate.enums.MetaLinha;
import br.com.BiTemplate.utils.CurrencyUtils;

public class Meta {

	private MetaLinha metaLinha;
	private BigDecimal meta;
	private BigDecimal valor;
	
	public Meta() {
		this.valor = BigDecimal.ZERO;
	}

	public Meta(MetaLinha ancora, BigDecimal meta) {
		this();
		this.metaLinha = ancora;
		this.meta = meta;
	}

	public MetaLinha getMetaLinha() {
		return metaLinha;
	}

	public void setMetaLinha(MetaLinha metaLinha) {
		this.metaLinha = metaLinha;
	}

	public BigDecimal getMeta() {
		return meta;
	}

	public void setMeta(BigDecimal meta) {
		this.meta = meta;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((metaLinha == null) ? 0 : metaLinha.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Meta other = (Meta) obj;
		if (metaLinha != other.metaLinha)
			return false;
		return true;
	}

	public void addValor(BigDecimal valor) {
		this.valor = this.valor.add(valor);
	}
	
	public String getValorAsCurrency() {
		return CurrencyUtils.format(this.valor);
	}
	
	public BigDecimal getPorcentagemRelativa() {
		BigDecimal metaPorDia = this.meta.divide(BigDecimal.valueOf(365), RoundingMode.HALF_EVEN);
		BigDecimal metaDiaAtual = metaPorDia.multiply(BigDecimal.valueOf(Calendar.getInstance().get(Calendar.DAY_OF_YEAR)));
		BigDecimal porcentagemRelativa = this.valor.divide(metaDiaAtual, RoundingMode.HALF_EVEN).multiply(BigDecimal.valueOf(100));
		return porcentagemRelativa.setScale(0);
	}
	
	public BigDecimal getPorcentagemAbsoluta() {
		BigDecimal porcentagemAbsoluta = this.valor.divide(meta, RoundingMode.HALF_EVEN).multiply(BigDecimal.valueOf(100));
		return porcentagemAbsoluta.setScale(0);
	}

}
