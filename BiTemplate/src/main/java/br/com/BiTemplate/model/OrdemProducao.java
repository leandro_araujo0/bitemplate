package br.com.BiTemplate.model;

import java.math.BigDecimal;

public class OrdemProducao {

	private String numero;
	private String usuario;
	private String local;
	private BigDecimal quant;
	private BigDecimal quje;
	private BigDecimal perda;
	private String datprf;
	private String obs;
	private String ultApto;
	private String datUltApto;
	private String emissao;
	private Produto produto;
	private String pc;
	private String sc;
	private String lote;
	private String status;

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public void setQuant(BigDecimal quant) {
		this.quant = quant;
	}

	public void setQuje(BigDecimal quje) {
		this.quje = quje;
	}

	public void setPerda(BigDecimal perda) {
		this.perda = perda;
	}

	public void setDatprf(String datprf) {
		this.datprf = datprf;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	public String getNumero() {
		return numero;
	}

	public String getUsuario() {
		return usuario;
	}

	public String getLocal() {
		return local;
	}

	public BigDecimal getQuant() {
		return quant;
	}

	public BigDecimal getQuje() {
		return quje;
	}

	public BigDecimal getPerda() {
		return perda;
	}

	public String getDatprf() {
		return datprf;
	}

	public String getObs() {
		return obs;
	}

	public BigDecimal getSaldo() {
		return this.quant.subtract(this.quje).subtract(this.perda);
	}

	public void setUltApto(String ultApto) {
		this.ultApto = ultApto;
	}

	public void setDatUltApto(String datUltApto) {
		this.datUltApto = datUltApto;
	}

	public String getUltApto() {
		return ultApto;
	}

	public String getDatUltApto() {
		return datUltApto;
	}

	public void setEmissao(String emissao) {
		this.emissao = emissao;
	}

	public String getEmissao() {
		return emissao;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Produto getProduto() {
		return produto;
	}

	public String getPc() {
		return pc;
	}

	public void setPc(String pc) {
		this.pc = pc;
	}

	public void setSc(String sc) {
		this.sc = sc;
	}

	public String getSc() {
		return sc;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public String getLote() {
		return lote;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getStatus() {
		return status;
	}

}
