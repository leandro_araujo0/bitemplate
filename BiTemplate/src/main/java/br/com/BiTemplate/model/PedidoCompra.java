package br.com.BiTemplate.model;

public class PedidoCompra {

	private String numero;
	private String fornecedor;
	private String emissao;
	private String quantidade;
	private String quantFin;
	private String nf;
	private String dtEntrega;

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setFornecedor(String fornecedor) {
		this.fornecedor = fornecedor;
	}

	public void setEmissao(String emissao) {
		this.emissao = emissao;
	}

	public void setQuantidade(String quantidade) {
		this.quantidade = quantidade;
	}

	public void setQuantFin(String quantFin) {
		this.quantFin = quantFin;
	}

	public String getNumero() {
		return numero;
	}

	public String getFornecedor() {
		return fornecedor;
	}

	public String getEmissao() {
		return emissao;
	}

	public String getQuantidade() {
		return quantidade;
	}

	public String getQuantFin() {
		return quantFin;
	}

	public void setNF(String nf) {
		this.nf = nf;
	}

	public void setDtEntrega(String dtEntrega) {
		this.dtEntrega = dtEntrega;
	}

	public String getNf() {
		return nf;
	}

	public void setNf(String nf) {
		this.nf = nf;
	}

	public String getDtEntrega() {
		return dtEntrega;
	}

}
