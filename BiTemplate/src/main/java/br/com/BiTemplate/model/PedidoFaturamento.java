package br.com.BiTemplate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class PedidoFaturamento {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String numeroPedido;
	private String itemPedido;
	private String mes;
	private String ano;
	@Lob
	@Column(name = "memorando", columnDefinition = "VARCHAR(5000)")
	private String status;
	private String ultAtualiz;
	private String localizacao;
	private String dtExp;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNumeroPedido() {
		return numeroPedido;
	}

	public void setNumeroPedido(String numeroPedido) {
		this.numeroPedido = numeroPedido;
	}

	public String getItemPedido() {
		return itemPedido;
	}

	public void setItemPedido(String itemPedido) {
		this.itemPedido = itemPedido;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public String getMes() {
		return mes;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public String getAno() {
		return this.ano;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((itemPedido == null) ? 0 : itemPedido.hashCode());
		result = prime * result + ((numeroPedido == null) ? 0 : numeroPedido.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PedidoFaturamento other = (PedidoFaturamento) obj;
		if (itemPedido == null) {
			if (other.itemPedido != null)
				return false;
		} else if (!itemPedido.equals(other.itemPedido))
			return false;
		if (numeroPedido == null) {
			if (other.numeroPedido != null)
				return false;
		} else if (!numeroPedido.equals(other.numeroPedido))
			return false;
		return true;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "PedidoFaturamento [numeroPedido=" + numeroPedido + ", itemPedido=" + itemPedido + ", status=" + status
				+ "]";
	}

	public String getUltAtualiz() {
		return ultAtualiz;
	}

	public void setUltAtualiz(String ultAtualiz) {
		this.ultAtualiz = ultAtualiz;
	}

	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public String getDtExp() {
		return this.dtExp;
	}

	public void setDtExp(String dtExp) {
		this.dtExp = dtExp;
	}

}