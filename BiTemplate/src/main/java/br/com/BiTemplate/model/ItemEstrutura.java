package br.com.BiTemplate.model;

import java.math.BigDecimal;

public class ItemEstrutura {

	private Produto produtoPai;
	private BigDecimal quantidade;
	private DetalhesProduto detalhesProdutoFilho;
	private int nivel;
	private BigDecimal qtdBase;
	private String tipoProdutoPai;
	
	public ItemEstrutura(Produto produto) {
		this.detalhesProdutoFilho = new DetalhesProduto(produto);
	}

	public Produto getProduto() {
		return this.detalhesProdutoFilho.getProduto();
	}

	public ItemEstrutura setProduto(Produto produto) {
		return this;
	}

	public BigDecimal getQuantidade() {
		return quantidade;
	}

	public ItemEstrutura setQuantidade(BigDecimal bigDecimal) {
		this.quantidade = bigDecimal;
		return this;
	}

	public void addQuantidade(BigDecimal quantidade) {
		this.quantidade = this.quantidade.add(quantidade);
	}

	public DetalhesProduto getDetalhesProduto() {
		return detalhesProdutoFilho;
	}

	public void setDetalhesProduto(DetalhesProduto detalhesProduto) {
		this.detalhesProdutoFilho = detalhesProduto;
	}

	public boolean ePI() {
		if (this.detalhesProdutoFilho.getProduto().getCodigo().contains("PI")) {
			return true;
		}
		return false;
	}

	public boolean eEmbalagem() {
		if (this.detalhesProdutoFilho.getProduto().getCodigo().startsWith("ME")) {
			return true;
		}
		return false;
	}

	public BigDecimal getResumo() {
		BigDecimal resumo = BigDecimal.ZERO;
		resumo = resumo.add(this.detalhesProdutoFilho.getQtdEstoque())
				.add(this.detalhesProdutoFilho.getQtdEnderecar())
				.add(this.detalhesProdutoFilho.getQtdInspecao())
				.add(this.detalhesProdutoFilho.getQtdSC())
				.add(this.detalhesProdutoFilho.getQtdPC())
				.add(this.detalhesProdutoFilho.getQtdOP())
				.subtract(this.detalhesProdutoFilho.getQtdEmpenhada())
				.subtract((this.quantidade != null) ? this.quantidade : BigDecimal.ZERO);
		return resumo;
	}

	public int getNivel() {
		return this.nivel;
	}
	
	public ItemEstrutura setNivel(int nivel) {
		this.nivel = nivel;
		return this;
	}

	public boolean validaProduto() {
		if (this.ePI() || this.eEmbalagem() || this.eFantasma()) {
			return true;
		}
		return false;
	}

	private boolean eFantasma() {
		if (this.getProduto().getFantasm().equals("S")) {
			return true;
		}
		return false;
	}

	public Produto getProdutoPai() {
		return produtoPai;
	}

	public ItemEstrutura setProdutoPai(Produto produtoPai) {
		this.produtoPai = produtoPai;
		return this;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((detalhesProdutoFilho == null) ? 0 : detalhesProdutoFilho.hashCode());
		result = prime * result + ((produtoPai == null) ? 0 : produtoPai.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemEstrutura other = (ItemEstrutura) obj;
		if (detalhesProdutoFilho == null) {
			if (other.detalhesProdutoFilho != null)
				return false;
		} else if (!detalhesProdutoFilho.equals(other.detalhesProdutoFilho))
			return false;
		if (produtoPai == null) {
			if (other.produtoPai != null)
				return false;
		} else if (!produtoPai.equals(other.produtoPai))
			return false;
		return true;
	}

	public BigDecimal getQtdBase() {
		return this.qtdBase;
	}

	public ItemEstrutura setQtdBase(BigDecimal bigDecimal) {
		qtdBase = bigDecimal;
		return this;
	}

	public void setTipoProdutoPai(String tipoProdutoPai) {
		this.tipoProdutoPai = tipoProdutoPai;
	}

	public DetalhesProduto getDetalhesProdutoFilho() {
		return detalhesProdutoFilho;
	}

	public void setDetalhesProdutoFilho(DetalhesProduto detalhesProdutoFilho) {
		this.detalhesProdutoFilho = detalhesProdutoFilho;
	}

	public String getTipoProdutoPai() {
		return tipoProdutoPai;
	}

	public void addQtdBase(BigDecimal qtd) {
		this.qtdBase = this.qtdBase.add(qtd);
	}
}
