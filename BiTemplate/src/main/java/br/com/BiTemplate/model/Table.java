package br.com.BiTemplate.model;

public class Table {

	private String[][] data;

	public Table(String[][] data) {
		this.data = data;
	}

	public Table setData(String[][] data) {
		this.data = data;
		return this;
	}

	public String[][] getData() {
		return data;
	}

}
