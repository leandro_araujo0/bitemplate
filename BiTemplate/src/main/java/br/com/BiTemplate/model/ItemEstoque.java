package br.com.BiTemplate.model;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import br.com.BiTemplate.utils.CurrencyUtils;
import br.com.BiTemplate.utils.DataUtils;

public class ItemEstoque {

	private String codigo;
	private String descricao;
	private BigDecimal quant;
	private BigDecimal empenho;
	private BigDecimal prcVen;
	private String lote;
	private String data;
	private List<Cliente> clientes;
	
	public ItemEstoque() {
		clientes = new LinkedList<Cliente>();
	}

	public ItemEstoque setCodigo(String codigo) {
		this.codigo = codigo;
		return this;
	}

	public void setdescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setQuant(BigDecimal quant) {
		this.quant = quant;
	}

	public void setEmpenho(BigDecimal empenho) {
		this.empenho = empenho;
	}

	public void setPrcVen(BigDecimal prcVen) {
		this.prcVen = prcVen;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCodigo() {
		return codigo;
	}

	public BigDecimal getQuant() {
		return quant;
	}

	public BigDecimal getEmpenho() {
		return empenho;
	}

	public BigDecimal getPrcVen() {
		return prcVen;
	}

	public String getLote() {
		return lote;
	}

	public String getData() {
		return data;
	}
	
	public String getDataFormatada() {
		return DataUtils.formatData(data);
	}

	public BigDecimal getSaldo() {
		return this.quant.subtract(this.empenho);
	}
	
	public BigDecimal getValorTotal() {
		return getSaldo().multiply(this.prcVen);
	}
	
	
	public String getValorTotalCurrency() {
		return CurrencyUtils.format(getSaldo().multiply(this.prcVen));
	}

	public List<Cliente> getClientes() {
		return clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemEstoque other = (ItemEstoque) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}

	public void addQuant(BigDecimal quant) {
 		if (this.quant != null) {
			this.quant = this.quant.add(quant);
		} else {
			this.quant = quant;
		}
	}
	
}
