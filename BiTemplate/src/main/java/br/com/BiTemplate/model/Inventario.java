package br.com.BiTemplate.model;

import java.util.List;

public class Inventario {

	private String codInv;
	private String data;
	private String armazem;
	private String endereco;
	private List<ContagemInventario> contagens;

	public void setCodInv(String codInv) {
		this.codInv = codInv;
	}

	public void setData(String data) {
		this.data = data;
	}

	public void setArmazem(String armazem) {
		this.armazem = armazem;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public void setContagens(List<ContagemInventario> contagens) {
		this.contagens = contagens;
	}

	public String getCodInv() {
		return codInv;
	}

	public String getData() {
		return data;
	}

	public String getArmazem() {
		return armazem;
	}

	public String getEndereco() {
		return endereco;
	}

	public List<ContagemInventario> getContagens() {
		return contagens;
	}

}
