package br.com.BiTemplate.model;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class AcompanhamentoPedido {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String numPedido;
	private String itemPed;
	private String ops;
	private String processo;
	@Lob
	@Column(name = "memorando", columnDefinition = "VARCHAR(5000)")
	private String memorando;
	private String obs;
	private String prevEntrega;
	private String chassi;
	private String caldInt;
	private String sistElev;
	private String porta;
	private String hidraulica;
	private String painelEletrico;
	private String eletrica;
	private String revestimento;
	private String embalagem;
	private String montagem;
	private String opRevestimento;
	private String opEmbalagem;
	private String opAjusteRevestimento;
	private String opPneumatica;
	private String camara;
	private String estrutura;
	private String gerador;
	private String loteOpPai;
	private String mesProducao;
	private String prioridade;
	private String tipoProduto;
	private String base;
	private String estruturaBase;
	private String cjElevacao;
	private String suporteLongitudinal;
	private String dorsoMovimento;
	private String leitoMovimento;
	private String assentoMovimento;
	private String dorsoAtroscopia;
	private String tracaoTibia;
	private String membrosInferiores;
	private String perneiraEsquerda;
	private String perneiraDireita;
	private String apoioPelvico;
	private String cabeceira;
	private String complementoDorso;
	private String opPintura;
	private String apoioLateral;
	private String apoioOmbro;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNumPedido() {
		return numPedido;
	}

	public void setNumPedido(String numPedido) {
		this.numPedido = numPedido;
	}

	public String getItemPed() {
		return itemPed;
	}

	public void setItemPed(String itemPed) {
		this.itemPed = itemPed;
	}

	public String getOps() {
		return ops;
	}

	public void setOps(String ops) {
		this.ops = ops;
	}

	public String getProcesso() {
		return processo;
	}

	public void setProcesso(String processo) {
		this.processo = processo;
	}

	public String getMemorando() {
		return memorando;
	}

	public void setMemorando(String memorando) {
		this.memorando = memorando;
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	public String getPrevEntrega() {
		return prevEntrega;
	}

	public void setPrevEntrega(String prevEntrega) {
		this.prevEntrega = prevEntrega;
	}

	public String getChassi() {
		return chassi;
	}

	public void setChassi(String chassi) {
		this.chassi = chassi;
	}

	public String getCaldInt() {
		return caldInt;
	}

	public void setCaldInt(String caldInt) {
		this.caldInt = caldInt;
	}

	public String getSistElev() {
		return sistElev;
	}

	public void setSistElev(String sistElev) {
		this.sistElev = sistElev;
	}

	public String getPorta() {
		return porta;
	}

	public void setPorta(String porta) {
		this.porta = porta;
	}

	public String getHidraulica() {
		return hidraulica;
	}

	public void setHidraulica(String hidraulica) {
		this.hidraulica = hidraulica;
	}

	public String getPainelEletrico() {
		return painelEletrico;
	}

	public void setPainelEletrico(String painelEletrico) {
		this.painelEletrico = painelEletrico;
	}

	public String getEletrica() {
		return eletrica;
	}

	public void setEletrica(String eletrica) {
		this.eletrica = eletrica;
	}

	public String getRevestimento() {
		return revestimento;
	}

	public void setRevestimento(String revestimento) {
		this.revestimento = revestimento;
	}

	public String getMontagem() {
		return montagem;
	}

	public void setMontagem(String montagem) {
		this.montagem = montagem;
	}

	public String getEmbalagem() {
		return embalagem;
	}

	public void setEmbalagem(String embalagem) {
		this.embalagem = embalagem;
	}

	public List<String> getTodasOps() {
		LinkedList<String> ops = new LinkedList<String>();
		if (this.ops != null && !this.ops.equals("")) {
			for (String string : this.ops.split(";")) {
				ops.add(string);
			}
		}
		if (this.ops != null && !this.ops.equals(""))
			ops.add(this.ops);
		if (this.chassi != null && !this.chassi.equals(""))
			ops.add(this.chassi);
		if (this.caldInt != null && !this.caldInt.equals(""))
			ops.add(this.caldInt);
		if (this.sistElev != null && !this.sistElev.equals(""))
			ops.add(this.sistElev);
		if (this.porta != null && !this.porta.equals(""))
			ops.add(this.porta);
		if (this.hidraulica != null && !this.hidraulica.equals(""))
			ops.add(this.hidraulica);
		if (this.painelEletrico != null && !this.painelEletrico.equals(""))
			ops.add(this.painelEletrico);
		if (this.montagem != null && !this.montagem.equals(""))
			ops.add(this.montagem);
		if (this.opRevestimento != null && !this.opRevestimento.equals(""))
			ops.add(this.opRevestimento);
		if (this.opEmbalagem != null && !this.opEmbalagem.equals(""))
			ops.add(this.opEmbalagem);
		if (this.opAjusteRevestimento != null && !this.opAjusteRevestimento.equals(""))
			ops.add(this.opAjusteRevestimento);
		if (this.camara != null && !this.camara.equals(""))
			ops.add(this.camara);
		if (this.estrutura != null && !this.estrutura.equals(""))
			ops.add(this.estrutura);
		if (this.gerador != null && !this.gerador.equals(""))
			ops.add(this.gerador);
		return ops;
	}

	public String getOpRevestimento() {
		return opRevestimento;
	}

	public void setOpRevestimento(String opRevestimento) {
		this.opRevestimento = opRevestimento;
	}

	public String getOpEmbalagem() {
		return opEmbalagem;
	}

	public void setOpEmbalagem(String opEmbalagem) {
		this.opEmbalagem = opEmbalagem;
	}

	public String getOpAjusteRevestimento() {
		return opAjusteRevestimento;
	}

	public void setOpAjusteRevestimento(String opAjusteRevestimento) {
		this.opAjusteRevestimento = opAjusteRevestimento;
	}

	public String getOpPneumatica() {
		return opPneumatica;
	}

	public void setOpPneumatica(String opPneumatica) {
		this.opPneumatica = opPneumatica;
	}

	public String getCamara() {
		return camara;
	}

	public void setCamara(String camara) {
		this.camara = camara;
	}

	public String getEstrutura() {
		return estrutura;
	}

	public void setEstrutura(String estrutura) {
		this.estrutura = estrutura;
	}

	public String getGerador() {
		return gerador;
	}

	public void setGerador(String gerador) {
		this.gerador = gerador;
	}

	public String getLoteOpPai() {
		return loteOpPai;
	}

	public void setLoteOpPai(String loteOpPai) {
		this.loteOpPai = loteOpPai;
	}

	public String getMesProducao() {
		return mesProducao;
	}

	public void setMesProducao(String mesProducao) {
		this.mesProducao = mesProducao;
	}

	public String getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(String prioridade) {
		this.prioridade = prioridade;
	}
	
	public String getTipoProduto() {
		if (tipoProduto == null)
			return "";
		return tipoProduto;
	}

	public void setTipoProduto(String tipoProduto) {
		this.tipoProduto = tipoProduto;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getEstruturaBase() {
		return estruturaBase;
	}

	public void setEstruturaBase(String estruturaBase) {
		this.estruturaBase = estruturaBase;
	}

	public String getCjElevacao() {
		return cjElevacao;
	}

	public void setCjElevacao(String cjElevacao) {
		this.cjElevacao = cjElevacao;
	}

	public String getSuporteLongitudinal() {
		return suporteLongitudinal;
	}

	public void setSuporteLongitudinal(String suporteLongitudinal) {
		this.suporteLongitudinal = suporteLongitudinal;
	}

	public String getDorsoMovimento() {
		return dorsoMovimento;
	}

	public void setDorsoMovimento(String dorsoMovimento) {
		this.dorsoMovimento = dorsoMovimento;
	}

	public String getLeitoMovimento() {
		return leitoMovimento;
	}

	public void setLeitoMovimento(String leitoMovimento) {
		this.leitoMovimento = leitoMovimento;
	}

	public String getAssentoMovimento() {
		return assentoMovimento;
	}

	public void setAssentoMovimento(String assentoMovimento) {
		this.assentoMovimento = assentoMovimento;
	}

	public String getDorsoAtroscopia() {
		return dorsoAtroscopia;
	}

	public void setDorsoAtroscopia(String dorsoAtroscopia) {
		this.dorsoAtroscopia = dorsoAtroscopia;
	}

	public String getTracaoTibia() {
		return tracaoTibia;
	}

	public void setTracaoTibia(String tracaoTibia) {
		this.tracaoTibia = tracaoTibia;
	}

	public String getMembrosInferiores() {
		return membrosInferiores;
	}

	public void setMembrosInferiores(String membrosInferiores) {
		this.membrosInferiores = membrosInferiores;
	}

	public String getPerneiraEsquerda() {
		return perneiraEsquerda;
	}

	public void setPerneiraEsquerda(String perneiraEsquerda) {
		this.perneiraEsquerda = perneiraEsquerda;
	}

	public String getPerneiraDireita() {
		return perneiraDireita;
	}

	public void setPerneiraDireita(String perneiraDireita) {
		this.perneiraDireita = perneiraDireita;
	}

	public String getApoioPelvico() {
		return apoioPelvico;
	}

	public void setApoioPelvico(String apoioPelvico) {
		this.apoioPelvico = apoioPelvico;
	}

	public String getCabeceira() {
		return cabeceira;
	}

	public void setCabeceira(String cabeceira) {
		this.cabeceira = cabeceira;
	}

	public String getComplementoDorso() {
		return complementoDorso;
	}

	public void setComplementoDorso(String complementoDorso) {
		this.complementoDorso = complementoDorso;
	}

	public String getOpPintura() {
		return opPintura;
	}

	public void setOpPintura(String opPintura) {
		this.opPintura = opPintura;
	}

	public String getApoioLateral() {
		return apoioLateral;
	}

	public void setApoioLateral(String apoioLateral) {
		this.apoioLateral = apoioLateral;
	}

	public String getApoioOmbro() {
		return apoioOmbro;
	}

	public void setApoioOmbro(String apoioOmbro) {
		this.apoioOmbro = apoioOmbro;
	}

}
