package br.com.BiTemplate.model;

import java.util.LinkedList;
import java.util.List;

public class CheckListExpedicao {

	private String cliente;
	private List<ItemPedidoVenda> itensPedidoVenda;
	private String documento;
	private String pedido;

	public CheckListExpedicao() {
		this.itensPedidoVenda = new LinkedList<ItemPedidoVenda>();
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public void add(ItemPedidoVenda itemPedido) {
		this.itensPedidoVenda.add(itemPedido);
	}

	public List<ItemPedidoVenda> getItensPedidoVenda() {
		return itensPedidoVenda;
	}

	public void setItensPedidoVenda(List<ItemPedidoVenda> itensPedidoVenda) {
		this.itensPedidoVenda = itensPedidoVenda;
	}

	public String getCliente() {
		return cliente;
	}

	public void setNf(String documento) {
		this.documento = documento;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public void setPedido(String pedido) {
		this.pedido = pedido;
	}

	public String getPedido() {
		return pedido;
	}

}
