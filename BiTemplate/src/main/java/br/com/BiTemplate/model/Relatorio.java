package br.com.BiTemplate.model;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Relatorio {
	
	@Id
	private String nome;
	private Calendar date;
	
	public Relatorio() {
		
	}
	
	public Relatorio(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}
	public boolean jaEnviado() {
		if (this.date != null && 
			this.date.get(Calendar.DAY_OF_MONTH) == Calendar.getInstance().get(Calendar.DAY_OF_MONTH) && 
			this.date.get(Calendar.MONTH) == Calendar.getInstance().get(Calendar.MONTH) && 
			this.date.get(Calendar.YEAR) == Calendar.getInstance().get(Calendar.YEAR)) {
			return true;
		}
		return false;
	}

}
