package br.com.BiTemplate.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class VariacaoPrecoProduto {

	private Produto produto;
	private BigDecimal precoInicial;
	private BigDecimal precoFinal;
	private String dataInicial;
	private String dataFinal;
	private String fornecedorInicial;
	private String fornecedorFinal;

	public String getFornecedorInicial() {
		return fornecedorInicial;
	}

	public void setFornecedorInicial(String fornecedorInicial) {
		this.fornecedorInicial = fornecedorInicial;
	}

	public String getFornecedorFinal() {
		return fornecedorFinal;
	}

	public void setFornecedorFinal(String fornecedorFinal) {
		this.fornecedorFinal = fornecedorFinal;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public BigDecimal getPrecoInicial() {
		return precoInicial;
	}

	public void setPrecoInicial(BigDecimal precoInicial) {
		this.precoInicial = precoInicial;
	}

	public BigDecimal getPrecoFinal() {
		return precoFinal;
	}

	public void setPrecoFinal(BigDecimal precoFinal) {
		this.precoFinal = precoFinal;
	}

	public String getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(String dataInicial) {
		this.dataInicial = dataInicial;
	}

	public String getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(String dataFinal) {
		this.dataFinal = dataFinal;
	}

	public int getVariacao() {
		if (this.precoInicial != null && this.precoFinal != null)
			return this.precoFinal.subtract(this.precoInicial).intValue();
		return 0;
	}

	public BigDecimal getVariacaoPorcento() {
		if (this.precoInicial != null && this.precoFinal != null)
		return this.precoFinal.subtract(this.precoInicial).divide(this.precoInicial, RoundingMode.HALF_EVEN).multiply(new BigDecimal(100)).setScale(2,
				RoundingMode.HALF_EVEN);
		return precoFinal;
	}

}
