package br.com.BiTemplate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class FollowUpProduto {

	@Id
	private String codigoProduto;
	private String descricao;
	@Lob
	@Column(name = "memorando", columnDefinition = "VARCHAR(5000)")
	private String memorando;
	
	public String getCodigoProduto() {
		return codigoProduto;
	}

	public FollowUpProduto setCodigoProduto(String codigoProduto) {
		this.codigoProduto = codigoProduto;
		return this;
	}

	public String getMemorando() {
		return memorando;
	}

	public void setMemorando(String memorando) {
		this.memorando = memorando;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoProduto == null) ? 0 : codigoProduto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FollowUpProduto other = (FollowUpProduto) obj;
		if (codigoProduto == null) {
			if (other.codigoProduto != null)
				return false;
		} else if (!codigoProduto.equals(other.codigoProduto))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FollowUpProduto [codigoProduto=" + codigoProduto + "]";
	}

}
