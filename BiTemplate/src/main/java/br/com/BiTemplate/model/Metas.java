package br.com.BiTemplate.model;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedList;

import br.com.BiTemplate.enums.MetaLinha;

public class Metas {
	
	private Meta ancora;
	private Meta placasEParafusos;
	private Meta quadril;
	private Meta caixas;
	private Meta coluna;
	private Meta signus;
	private Meta joelho;
	private Meta placaTubo;
	private Meta ortolock;
	private Meta miniEMicro;
	private Meta bucoMaxilo;
	private Meta fixadores;
	private Meta medicinaEsportiva;
	private Meta manufaturaAditiva;
	private Meta diversos;
	
	public Metas() {
		this.ancora = new Meta(MetaLinha.ANCORA, BigDecimal.valueOf(900000));
		this.placasEParafusos = new Meta(MetaLinha.PLACASEPARAFUSOS, BigDecimal.valueOf(18000000));
		this.quadril = new Meta(MetaLinha.QUADRIL, BigDecimal.valueOf(13000000));
		this.caixas = new Meta(MetaLinha.CAIXAS, BigDecimal.valueOf(11000000));
		this.coluna = new Meta(MetaLinha.COLUNA, BigDecimal.valueOf(4000000));
		this.signus = new Meta(MetaLinha.SIGNUS, BigDecimal.valueOf(2200000));
		this.joelho = new Meta(MetaLinha.JOELHO, BigDecimal.valueOf(1000000));
		this.placaTubo = new Meta(MetaLinha.PLACATUBO, BigDecimal.valueOf(2000000));
		this.ortolock = new Meta(MetaLinha.ORTOLOCK, BigDecimal.valueOf(1000000));
		this.miniEMicro = new Meta(MetaLinha.MINIEMICRO, BigDecimal.valueOf(300000));
		this.bucoMaxilo = new Meta(MetaLinha.BUCOMAXILO, BigDecimal.valueOf(1000000));
		this.fixadores = new Meta(MetaLinha.FIXADORES, BigDecimal.valueOf(300000));
		this.medicinaEsportiva = new Meta(MetaLinha.MEDICINAESPORTIVA, BigDecimal.valueOf(400000));
		this.manufaturaAditiva = new Meta(MetaLinha.MANUFATURAADITIVA, BigDecimal.valueOf(1500000));
		this.diversos = new Meta(MetaLinha.DIVERSOS, BigDecimal.valueOf(4000000));
	}
	
	public Iterable<Meta> getMetas(DataTable dataTable) {
		LinkedList<Meta> metas = new LinkedList<Meta>();
		
		for (Collection<Object> collection : dataTable.getData()) {
			LinkedList<Object> tempCollection = (LinkedList<Object>) collection;
			String grupo = (String) tempCollection.get(0);
			
			switch(grupo) {
			case "0069"://ANCORA MONTADA
				ancora.addValor((BigDecimal) tempCollection.get(2));
				break;
			case "0009"://PLACAS E PARAFUSOS - 1
				placasEParafusos.addValor((BigDecimal) tempCollection.get(2));
				break;
			case "0004"://PLACAS E PARAFUSOS - 2
				placasEParafusos.addValor((BigDecimal) tempCollection.get(2));
				break;
			case "0070"://QUADRIL
				quadril.addValor((BigDecimal) tempCollection.get(2));
				break;
//			case "0070"://CAIXAS ------ REALIZAR ESTA CONSULTA SEPARADAMENTE 
//				break;
			case "0071"://COLUNA
				coluna.addValor((BigDecimal) tempCollection.get(2));
				break;
			case "0027"://SIGNUS
				signus.addValor((BigDecimal) tempCollection.get(2));
				break;
			case "0035"://JOELHO
				joelho.addValor((BigDecimal) tempCollection.get(2));
				break;
			case "0077"://PLACA TUBO
				placaTubo.addValor((BigDecimal) tempCollection.get(2));
				break;
			case "0018"://ORTOLOCK
				ortolock.addValor((BigDecimal) tempCollection.get(2));
				break;
			case "0074"://MINI E MICRO
				miniEMicro.addValor((BigDecimal) tempCollection.get(2));
				break;
			case "0075"://BUCOMAXILO
				bucoMaxilo.addValor((BigDecimal) tempCollection.get(2));
				break;
			case "0005"://FIXADORES
				fixadores.addValor((BigDecimal) tempCollection.get(2));
				break;
			case "0076"://MEDICINA ESPORTIVA
				medicinaEsportiva.addValor((BigDecimal) tempCollection.get(2));
				break;
			case "0078"://MANUFATURA ADITIVA
				manufaturaAditiva.addValor((BigDecimal) tempCollection.get(2));
				break;
			case "CAIXAS"://CAIXAS
				caixas.addValor((BigDecimal) tempCollection.get(2));
				break;
			default://DIVERSOS
				diversos.addValor((BigDecimal) tempCollection.get(2));
				break;
			}
		}
		
		metas.add(placasEParafusos);
		metas.add(quadril);
		metas.add(caixas);
		metas.add(coluna);
		metas.add(signus);
		metas.add(joelho);
		metas.add(placaTubo);
		metas.add(ortolock);
		metas.add(ancora);
		metas.add(miniEMicro);
		metas.add(bucoMaxilo);
		metas.add(fixadores);
		metas.add(medicinaEsportiva);
		metas.add(manufaturaAditiva);
		metas.add(diversos);
		
		return metas;
	}
	
	public Meta getMeta(MetaLinha metaLinha) {
		
		switch (metaLinha) {
		case ANCORA:
			return ancora;
		case PLACASEPARAFUSOS:
			return placasEParafusos;
		case QUADRIL:
			return quadril;
		case CAIXAS:
			return caixas;
		case COLUNA:
			return coluna;
		case SIGNUS:
			return signus;
		case JOELHO:
			return joelho;
		case PLACATUBO:
			return placaTubo;
		case ORTOLOCK:
			return ortolock;
		case MINIEMICRO:
			return miniEMicro;
		case BUCOMAXILO:
			return bucoMaxilo;
		case FIXADORES:
			return fixadores;
		case MEDICINAESPORTIVA:
			return medicinaEsportiva;
		case MANUFATURAADITIVA:
			return manufaturaAditiva;
		case DIVERSOS:
			return diversos;
		default:
			return null;
		}
		
	}

}
