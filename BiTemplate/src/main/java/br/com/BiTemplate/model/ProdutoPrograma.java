package br.com.BiTemplate.model;

import java.math.BigDecimal;

import javax.persistence.Embeddable;

@Embeddable
public class ProdutoPrograma {

	private BigDecimal valorPromocional;
	private String codigo;

	public BigDecimal getValorPromocional() {
		return valorPromocional;
	}

	public void setValorPromocional(BigDecimal valorPromocional) {
		this.valorPromocional = valorPromocional;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

}
