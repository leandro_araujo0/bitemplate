package br.com.BiTemplate.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import br.com.BiTemplate.enums.CLASPED;
import br.com.BiTemplate.enums.UNIDADE;
import br.com.BiTemplate.utils.CurrencyUtils;
import br.com.BiTemplate.utils.DataUtils;

public class PedidoVenda {

	private String numero;
	private Cliente cliente;
	private String dataEntrega;
	private List<ItemPedidoVenda> itensPedidoVenda;
	private String dataEmissao;
	private UNIDADE unidade;
	private BigDecimal valorTotal;
	private CLASPED clasPed;
	private String tipoVenda;
	private String dataLiberacao;
	private BigDecimal comis1;
	private BigDecimal comis2;
	private BigDecimal comis3;
	private BigDecimal comis4;
	private BigDecimal comis5;
	private String liberok;

	public List<ItemPedidoVenda> getItensPedidoVenda() {
		return itensPedidoVenda;
	}

	public void setItensPedidoVenda(List<ItemPedidoVenda> itensPedidoVenda) {
		this.itensPedidoVenda = itensPedidoVenda;
	}

	public String getNumero() {
		return numero;
	}

	public String getDataEntrega() {
		return dataEntrega;
	}

	public String getDataEmissaoFormatada() {
		return DataUtils.formatData(this.dataEmissao);
	}

	public String getDataEntregaFormatada() {
		return (this.dataEntrega != null) ? DataUtils.formatData(this.dataEntrega) : null;
	}

	public PedidoVenda setNumero(String numero) {
		this.numero = numero;
		return this;
	}

	public PedidoVenda setDataEntrega(String dataEntrega) {
		this.dataEntrega = dataEntrega;
		return this;
	}

	public BigDecimal getTotalPedido() {
		if (this.itensPedidoVenda == null)
			return BigDecimal.ZERO;
		BigDecimal valorTotal = BigDecimal.ZERO;
		for (ItemPedidoVenda itemPedidoVenda : itensPedidoVenda) {
			valorTotal = valorTotal.add(itemPedidoVenda.getValorTotal());
		}
		return valorTotal.setScale(2, RoundingMode.HALF_EVEN);
	}

	public String getTotalPedidoAsCurrency() {
		return (this.getTotalPedido() != null) ? CurrencyUtils.format(this.getTotalPedido()) : null;
	}

	public String getDataEmissaoPtBr() {
		return DataUtils.formatData(this.dataEmissao);
	}

	public String getDataEntregaPtBr() {
		return DataUtils.formatData(this.dataEntrega);
	}
	
	public String getDataLiberacaoPtBr() {
		return DataUtils.formatData(this.dataLiberacao);
	}

	public Cliente getCliente() {
		return cliente;
	}

	public PedidoVenda setCliente(Cliente cliente) {
		this.cliente = cliente;
		return this;
	}

	public PedidoVenda setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
		return this;
	}

	public String getDataEmissao() {
		return dataEmissao;
	}

	public void setUnidade(UNIDADE unidade) {
		this.unidade = unidade;
	}

	public UNIDADE getUnidade() {
		return unidade;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public String getValorTotalAsCurrency() {
		return CurrencyUtils.format(this.valorTotal);
	}

	public CLASPED getClasPed() {
		return clasPed;
	}

	public PedidoVenda setClasPed(CLASPED clasPed) {
		this.clasPed = clasPed;
		return this;
	}

	public void setTipoVenda(String tipoVenda) {
		this.tipoVenda = tipoVenda;
	}

	public String getTipoVenda() {
		return tipoVenda;
	}

	public PedidoVenda setDataLiberacao(String dataLiberacao) {
		this.dataLiberacao = dataLiberacao;
		return this;
	}

	public String getDataLiberacao() {
		return dataLiberacao;
	}

	public BigDecimal getComis1() {
		return comis1;
	}

	public void setComis1(BigDecimal comis1) {
		this.comis1 = comis1;
	}

	public BigDecimal getComis2() {
		return comis2;
	}

	public void setComis2(BigDecimal comis2) {
		this.comis2 = comis2;
	}

	public BigDecimal getComis3() {
		return comis3;
	}

	public void setComis3(BigDecimal comis3) {
		this.comis3 = comis3;
	}

	public BigDecimal getComis4() {
		return comis4;
	}

	public void setComis4(BigDecimal comis4) {
		this.comis4 = comis4;
	}

	public String getTotalComissaoAsCurrency() {
		if (this.getTotalPedido() != null && this.getComissaoTotal() != null)
			return CurrencyUtils.format(this.getTotalPedido().divide(BigDecimal.valueOf(100.0))
					.multiply(this.getComissaoTotal()).setScale(2, RoundingMode.HALF_EVEN));
		else {
			return "null";
		}
	}

	private BigDecimal getComissaoTotal() {
		BigDecimal comissaoTotal = BigDecimal.ZERO;
		if (this.comis1 != null)
			comissaoTotal = comissaoTotal.add(this.comis1);
		if (this.comis2 != null)
			comissaoTotal = comissaoTotal.add(this.comis2);
		if (this.comis3 != null)
			comissaoTotal = comissaoTotal.add(this.comis3);
		if (this.comis4 != null)
			comissaoTotal = comissaoTotal.add(this.comis4);
		if (this.comis5 != null)
			comissaoTotal = comissaoTotal.add(this.comis5);
		return comissaoTotal.setScale(2, RoundingMode.HALF_EVEN);
	}

	public BigDecimal getComis5() {
		return comis5;
	}

	public void setComis5(BigDecimal comis5) {
		this.comis5 = comis5;
	}

	public String getLiberok() {
		return liberok;
	}

	public void setLiberok(String liberok) {
		this.liberok = liberok;
	}
	
	public boolean estaLiberado() {
		if (this.liberok.equals("S")) {
			return true;
		} else {
			return false;
		}
	}
}
