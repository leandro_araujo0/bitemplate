package br.com.BiTemplate.model;

import java.util.LinkedList;
import java.util.List;

public class DetalhesOP {

	private OrdemProducao op;
	private List<Processo> processos;
	private List<PedidoCompra> pedidos;

	public DetalhesOP() {
		processos = new LinkedList<>();
	}

	public void setOp(OrdemProducao op) {
		this.op = op;
	}

	public OrdemProducao getOp() {
		return op;
	}

	public List<Processo> getProcessos() {
		return processos;
	}

	public void setProcessos(List<Processo> processos) {
		this.processos = processos;
	}

	public List<PedidoCompra> getPedidos() {
		return pedidos;
	}

	public void setPedidos(List<PedidoCompra> pedidos) {
		this.pedidos = pedidos;
	}

}
