package br.com.BiTemplate.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PedidoManual {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String numero;
	private String item;

	public String getNumero() {
		return this.numero;
	}

	public String getItem() {
		return this.item;
	}

	public PedidoManual setNumero(String numero) {
		this.numero = numero;
		return this;
	}

	public PedidoManual setItem(String item) {
		this.item = item;
		return this;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
