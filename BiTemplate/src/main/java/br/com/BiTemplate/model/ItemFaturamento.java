package br.com.BiTemplate.model;

import java.math.BigDecimal;

public class ItemFaturamento {
	
	private String numeroPedido;
	private Produto produto;
	private BigDecimal quant;
	private Cliente cliente;
	private BigDecimal valorTotal;
	private String nf;
	private String armazem;
	private String cfop;
	
	public String getNumeroPedido() {
		return numeroPedido;
	}
	public void setNumeroPedido(String numeroPedido) {
		this.numeroPedido = numeroPedido;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public BigDecimal getQuant() {
		return quant;
	}
	public void setQuant(BigDecimal quant) {
		this.quant = quant;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public BigDecimal getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}
	public String getNf() {
		return nf;
	}
	public void setNf(String nf) {
		this.nf = nf;
	}
	public void setArmazem(String armazem) {
		this.armazem = armazem;
	}
	public String getArmazem() {
		return armazem;
	}
	public String getCfop() {
		return cfop;
	}
	public void setCfop(String cfop) {
		this.cfop = cfop;
	}

}
