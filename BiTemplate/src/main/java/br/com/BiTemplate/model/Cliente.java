package br.com.BiTemplate.model;

import java.math.BigDecimal;

import br.com.BiTemplate.utils.CurrencyUtils;

public class Cliente {

	private String codigo;
	private String nome;
	private String nReduz;
	private String estado;
	private String telefone;
	private BigDecimal mediaCompras;
	private BigDecimal comprasMes;
	private String mediaFaturamento;
	private String natureza;
	private String email;
	private BigDecimal carteiraTotal;
	private String situacao;
	private String cnpj;

	public Cliente setCodigo(String codigo) {
		this.codigo = codigo;
		return this;
	}

	public Cliente setNome(String nome) {
		this.nome = nome;
		return this;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getNome() {
		return nome;
	}

	public String getnReduz() {
		return nReduz;
	}

	public Cliente setnReduz(String nReduz) {
		this.nReduz = nReduz;
		return this;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public BigDecimal getMediaCompras() {
		return mediaCompras;
	}

	public void setMediaCompras(BigDecimal bigDecimal) {
		this.mediaCompras = bigDecimal;
	}

	public BigDecimal getComprasMes() {
		return comprasMes;
	}
	
	public String getComprasMesAsCurrency() {
		return (this.comprasMes != null) ? CurrencyUtils.format(this.comprasMes) : null;
	}

	public void setComprasMes(BigDecimal bigDecimal) {
		this.comprasMes = bigDecimal;
	}

	public String getMediaFaturamento() {
		return mediaFaturamento;
	}

	public void setMediaFaturamento(String mediaFaturamento) {
		this.mediaFaturamento = mediaFaturamento;
	}

	public String getMediaComprasAsCurrency() {
		return (this.mediaCompras != null) ? CurrencyUtils.format(this.mediaCompras) : null;
	}
	
	public String getPoderCompraAsCurrency() {
		return CurrencyUtils.format(getPoderCompra());
	}

	public BigDecimal getPoderCompra() {
		return (this.mediaCompras == null) ? BigDecimal.ZERO : this.mediaCompras
				.subtract((this.comprasMes == null) ? BigDecimal.ZERO : this.comprasMes);
	}

	public void setNatureza(String natureza) {
		this.natureza = natureza;
	}

	public String getNatureza() {
		return natureza;
	}

	public String getEmail() {
		return this.email;
	}

	public Cliente setEmail(String string) {
		email = string;
		return this;
	}
	
	public String getCarteiraTotalAsCurrency() {
		return CurrencyUtils.format(this.getCarteiraTotal());
	}

	private BigDecimal getCarteiraTotal() {
		return this.carteiraTotal;
	}

	public void setCarteiraTotal(BigDecimal carteiraTotal) {
		this.carteiraTotal = carteiraTotal;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	
	public String getSituacao() {
		return this.situacao;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCnpj() {
		return cnpj;
	}
}
