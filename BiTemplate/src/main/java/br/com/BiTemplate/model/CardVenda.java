package br.com.BiTemplate.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.LinkedList;

public class CardVenda {

	private String seguimento;
	private BigDecimal valor;
	private String codigoGrupoProduto;

	public void setSeguimento(String seguimento) {
		this.seguimento = seguimento;
	}

	public String getSeguimento() {
		return seguimento;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	public BigDecimal getPorcentagemRelativa(LinkedList<CardVenda> cardsVendas) {
		BigDecimal total = BigDecimal.ZERO;
		for (CardVenda cardVenda : cardsVendas) {
			total = total.add(cardVenda.valor);
		}
		return this.valor.multiply(BigDecimal.valueOf(100)).divide(total, 2, RoundingMode.HALF_EVEN);
	}
	
	public BigDecimal getPorcentagemReferencia(LinkedList<CardVenda> cardsVendas) {
		BigDecimal maiorValor = BigDecimal.ZERO;
		for (CardVenda cardVenda : cardsVendas) {
			if (cardVenda.getValor().compareTo(maiorValor) >= 0) {
				maiorValor = cardVenda.getValor();
			}
		}
		return this.valor.divide(maiorValor, 2, RoundingMode.HALF_EVEN).multiply(BigDecimal.valueOf(100));
	}
	
	public BigDecimal getMediaMensal() {
		int diaDoAno = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
		if (this.valor == null || this.valor.compareTo(BigDecimal.ZERO) == 0) {
			return BigDecimal.ZERO;
		} else {
			return this.valor.multiply(BigDecimal.valueOf(30)).divide(BigDecimal.valueOf(diaDoAno), 2, RoundingMode.HALF_EVEN);
		}
	}
	
	public int getPorcentagemAbsoluta() {
		return 0;
	}

	public String getCodigoGrupoProduto() {
		return codigoGrupoProduto;
	}

	public void setCodigoGrupoProduto(String codigoGrupoProduto) {
		this.codigoGrupoProduto = codigoGrupoProduto;
	}


}
