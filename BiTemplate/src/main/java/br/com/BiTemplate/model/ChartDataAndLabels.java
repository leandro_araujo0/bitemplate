package br.com.BiTemplate.model;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedList;

import be.ceau.chart.dataset.BarDataset;

public class ChartDataAndLabels {

	private Collection<BigDecimal> chartData;
	private Collection<String> chartLabels;
	private Collection<BarDataset> chartDatasets;

	public ChartDataAndLabels() {
		chartData = new LinkedList<>();
		chartLabels = new LinkedList<>();
	}

	public Collection<BigDecimal> getChartData() {
		return chartData;
	}

	public void setChartData(Collection<BigDecimal> chartData) {
		this.chartData = chartData;
	}

	public Collection<String> getChartLabels() {
		return chartLabels;
	}

	public void setChartLabels(Collection<String> chartLabels) {
		this.chartLabels = chartLabels;
	}

	public String[] getChartLabelsToArray() {
		return chartLabels.toArray(new String[0]);
	}

	public void addLabel(String label) {
		this.chartLabels.add(label);
	}

	public void addData(BigDecimal data) {
		this.chartData.add(data);
	}

	public ChartDataAndLabels setChartDataSets(Collection<BarDataset> datasets) {
		this.chartDatasets = datasets;
		return this;
	}

	public Collection<BarDataset> getChartDatasets() {
		return chartDatasets;
	}

	public void setDatasets(Collection<BarDataset> chartDatasets) {
		this.chartDatasets = chartDatasets;
	}

}
