package br.com.BiTemplate.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Usuario {

	@Id
	private String username;
	private String password;
	@ElementCollection
	private List<String> authorities;
	private String accountLocked;
	private boolean enabled;
	private int failureAttempts = 0;

	public Usuario() {

	}

	public Usuario(String username, String password, String... authorities) {
		this.username = username;
		this.password = password;
		this.authorities = new ArrayList<>(Arrays.asList(authorities));
		this.enabled = true;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAccountLocked() {
		return accountLocked;
	}

	public void setAccountLocked(String accountLocked) {
		this.accountLocked = accountLocked;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void reportLoginFailure() {
		this.failureAttempts++;
		if (this.failureAttempts > 5) {
			this.enabled = false;
		}
	}

	public void reportLoginSuccess() {
		this.failureAttempts = 0;
	}

	public int getFailureAttempts() {
		return failureAttempts;
	}

	public void setFailureAttempts(int failureAttempts) {
		this.failureAttempts = failureAttempts;
	}

	public List<String> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(List<String> authorities) {
		this.authorities = authorities;
	}

	public boolean hasAuthority(String string) {
		if (this.authorities.contains("ADMIN")) {
			return true;
		}
		return false;
	}


}
