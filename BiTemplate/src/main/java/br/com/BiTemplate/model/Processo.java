package br.com.BiTemplate.model;

import java.math.BigDecimal;

public class Processo {

	private String recurso;
	private String descri;
	private String dataFin;
	private BigDecimal qtdProd;
	private BigDecimal qtdPerd;
	private String operador;
	private String xDescri;
	private String produto;

	public void setRecurso(String recurso) {
		this.recurso = recurso;
	}

	public void setDescri(String descri) {
		this.descri = descri;
	}

	public void setDataFin(String dataFin) {
		this.dataFin = dataFin;
	}

	public void setqtdProd(BigDecimal qtdProd) {
		this.qtdProd = qtdProd;
	}

	public void setQtdPerd(BigDecimal qtdPerd) {
		this.qtdPerd = qtdPerd;
	}

	public void setOperador(String operador) {
		this.operador = operador;
	}

	public BigDecimal getQtdProd() {
		return qtdProd;
	}

	public void setQtdProd(BigDecimal qtdProd) {
		this.qtdProd = qtdProd;
	}

	public String getRecurso() {
		return recurso;
	}

	public String getDescri() {
		return descri;
	}

	public String getDataFin() {
		return dataFin;
	}

	public BigDecimal getQtdPerd() {
		return qtdPerd;
	}

	public String getOperador() {
		return operador;
	}

	public void setXDescri(String xDescri) {
		this.xDescri = xDescri;
	}

	public String getXDescri() {
		return xDescri;
	}

	public void setProduto(String produto) {
		this.produto = produto;
	}

	public String getxDescri() {
		return xDescri;
	}

	public void setxDescri(String xDescri) {
		this.xDescri = xDescri;
	}

	public String getProduto() {
		return produto;
	}

}
