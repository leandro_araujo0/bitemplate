package br.com.BiTemplate.model;

public class Produto {

	private String codigo;
	private String descricao;
	private String UM;
	private String fantasm;
	private String grupo;
	private String descAlx;

	public Produto() {
		this.fantasm = "N";
	}

	public Produto(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public Produto setCodigo(String codigo) {
		this.codigo = codigo;
		return this;
	}

	public String getDescricao() {
		return descricao;
	}

	public Produto setDescricao(String descricao) {
		this.descricao = descricao;
		return this;
	}

	public String getLinkCode() {
		return this.codigo.replace("/", "%2F");
	}

	public String getUM() {
		return UM;
	}

	public void setUM(String UM) {
		this.UM = UM;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}

	public String getFantasm() {
		return this.fantasm;
	}

	public Produto setFantasm(String fantasm) {
		this.fantasm = fantasm;
		return this;
	}

	public Produto setGrupo(String grupo) {
		this.grupo = grupo;
		return this;
	}
	
	public String getGrupo() {
		return grupo;
	}

	public Produto setDescAlx(String descAlx) {
		this.descAlx = descAlx;
		return this;
	}

	public String getDescAlx() {
		return descAlx;
	}

}
