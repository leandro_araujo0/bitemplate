package br.com.BiTemplate.model;

import java.math.BigDecimal;

public class DetalhesProduto {

	private Produto produto;
	private BigDecimal qtdEstoque;
	private BigDecimal qtdOP;
	private BigDecimal qtdPV;
	private BigDecimal qtdEnderecar;
	private BigDecimal qtdInspecao;
	private BigDecimal qtdSC;
	private BigDecimal qtdPC;
	private BigDecimal cmm;
	private BigDecimal pmm;
	private BigDecimal qtdEmpenhada;
	private String desenho;
	private String um;
	private boolean opcional;
	private BigDecimal qtdTodosEstoques;

	public DetalhesProduto(Produto produto) {
		this.produto = produto;
	}

	public BigDecimal getCmm() {
		return cmm;
	}

	public void setCmm(BigDecimal bigDecimal) {
		this.cmm = bigDecimal;
	}

	public BigDecimal getQtdEstoque() {
		return (this.qtdEstoque != null) ? this.qtdEstoque : BigDecimal.ZERO;
	}

	public BigDecimal getQtdOP() {
		return (this.qtdOP != null) ? qtdOP : BigDecimal.ZERO;
	}

	public BigDecimal getQtdPV() {
		return qtdPV;
	}

	public BigDecimal getQtdEnderecar() {
		return (this.qtdEnderecar != null) ? this.qtdEnderecar : BigDecimal.ZERO;
	}

	public BigDecimal getQtdInspecao() {
		return (this.qtdInspecao != null) ? this.qtdInspecao : BigDecimal.ZERO;
	}

	public DetalhesProduto setProduto(Produto produto) {
		this.produto = produto;
		return this;
	}

	public DetalhesProduto setQtdEstoque(BigDecimal bigDecimal) {
		this.qtdEstoque = bigDecimal;
		return this;
	}

	public DetalhesProduto setQtdOP(BigDecimal bigDecimal) {
		this.qtdOP = bigDecimal;
		return this;
	}

	public DetalhesProduto setQtdPV(BigDecimal bigDecimal) {
		this.qtdPV = bigDecimal;
		return this;
	}

	public DetalhesProduto setQtdEnderecar(BigDecimal bigDecimal) {
		this.qtdEnderecar = bigDecimal;
		return this;
	}

	public DetalhesProduto setQtdInspecao(BigDecimal bigDecimal) {
		this.qtdInspecao = bigDecimal;
		return this;
	}

	public DetalhesProduto setQtdSC(BigDecimal bigDecimal) {
		this.qtdSC = bigDecimal;
		return this;
	}

	public DetalhesProduto setQtdPC(BigDecimal bigDecimal) {
		this.qtdPC = bigDecimal;
		return this;
	}

	public BigDecimal getQtdSC() {
		return (this.qtdSC != null) ? this.qtdSC : BigDecimal.ZERO;
	}

	public BigDecimal getQtdPC() {
		return (this.qtdPC != null) ? this.qtdPC : BigDecimal.ZERO;
	}

	public Produto getProduto() {
		return produto;
	}

	public DetalhesProduto setPmm(BigDecimal bigDecimal) {
		this.pmm = bigDecimal;
		return this;
	}

	public BigDecimal getPmm() {
		return pmm;
	}

	public DetalhesProduto setQtdEmpenhada(BigDecimal bigDecimal) {
		this.qtdEmpenhada = bigDecimal;
		return this;
	}

	public BigDecimal getQtdEmpenhada() {
		return (this.qtdEmpenhada != null) ? this.qtdEmpenhada : BigDecimal.ZERO;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((produto == null) ? 0 : produto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DetalhesProduto other = (DetalhesProduto) obj;
		if (produto == null) {
			if (other.produto != null)
				return false;
		} else if (!produto.equals(other.produto))
			return false;
		return true;
	}

	public BigDecimal getSaldo() {
		BigDecimal saldo = BigDecimal.ZERO;
		saldo = saldo.add((this.qtdEstoque != null) ? this.qtdEstoque : BigDecimal.ZERO);
		saldo = saldo.add((this.qtdEnderecar != null) ? this.qtdEnderecar : BigDecimal.ZERO);
		saldo = saldo.add((this.qtdInspecao != null) ? this.qtdInspecao : BigDecimal.ZERO);
		saldo = saldo.add((this.qtdOP != null) ? this.qtdOP : BigDecimal.ZERO);
		saldo = saldo.add((this.qtdSC != null) ? this.qtdSC : BigDecimal.ZERO);
		saldo = saldo.add((this.qtdPC != null) ? this.qtdPC : BigDecimal.ZERO);
		saldo = saldo.subtract((this.qtdEmpenhada != null) ? this.qtdEmpenhada : BigDecimal.ZERO);
		saldo = saldo.subtract((this.qtdPV != null) ? this.qtdPV : BigDecimal.ZERO);
		return saldo;
	}

	public void setDesenho(String desenho) {
		this.desenho = desenho;
	}

	public String getDesenho() {
		return desenho;
	}

	public String getUm() {
		return um;
	}

	public DetalhesProduto setUm(String um) {
		this.um = um;
		return this;
	}

	public void setOpcional(boolean opcional) {
		this.opcional = opcional;
	}

	public boolean isOpcional() {
		return opcional;
	}

	public void setQtdTodosEstoques(BigDecimal qtdTodosEstoques) {
		this.qtdTodosEstoques = qtdTodosEstoques;
	}

	public BigDecimal getQtdTodosEstoques() {
		return qtdTodosEstoques;
	}

}
