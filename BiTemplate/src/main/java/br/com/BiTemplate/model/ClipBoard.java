package br.com.BiTemplate.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

@Component
@SessionScope
public class ClipBoard implements Serializable {
	private static final long serialVersionUID = -6497836980839714984L;
	
	private Set<String> itensClipBoard;
	
	public ClipBoard() {
		this.setItensClipBoard(new HashSet<String>());
	}

	public Set<String> getItensClipBoard() {
		return itensClipBoard;
	}

	public void setItensClipBoard(Set<String> itensClipBoard) {
		this.itensClipBoard = itensClipBoard;
	}
	
	public void addItem(String item) {
		itensClipBoard.add(item);
	}

	public void limpaItens() {
		this.itensClipBoard.clear();
	}

}
