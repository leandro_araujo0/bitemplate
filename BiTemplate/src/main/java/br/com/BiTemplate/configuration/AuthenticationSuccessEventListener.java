package br.com.BiTemplate.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import br.com.BiTemplate.model.Usuario;
import br.com.BiTemplate.repository.UsuarioRepository;

@Component
public class AuthenticationSuccessEventListener
    implements ApplicationListener<AuthenticationSuccessEvent>{

	@Autowired
	private UsuarioRepository usuarioRepository;

  public void onApplicationEvent(AuthenticationSuccessEvent ev) {

		String username = ev.getAuthentication().getName();
		
		Usuario usuario = usuarioRepository.findById(username).orElse(null);
		
		if (usuario != null) {
			usuario.reportLoginSuccess();
			usuarioRepository.save(usuario);
		}
	}
}