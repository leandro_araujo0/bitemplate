package br.com.BiTemplate.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private DataSource dataSource;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/", "/login", "/favicon.ico", "/novo-ip").permitAll()
		.antMatchers("/ponto-pedido", "/ponto-pedido**").hasAnyAuthority("ADMIN", "FINANCEIRO", "PRODUCAO", "COMPRAS", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO", "ALMOXARIFADO")
		.antMatchers("/vendas*").hasAnyAuthority("ADMIN", "FINANCEIRO", "VENDAS-ORTOPEDIA", "VENDAS-EQUIPAMENTOS")
		.antMatchers("/historico-produto**", "/historico-produto/**").hasAnyAuthority("ADMIN", "FINANCEIRO", "COMPRAS", "PRODUCAO", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO", "ALMOXARIFADO")
		.antMatchers("/saldo-por-endereco", "/saldo-por-endereco**").hasAnyAuthority("FABRICA", "FINANCEIRO", "ADMIN", "COMPRAS", "PRODUCAO", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO", "ALMOXARIFADO", "ADMIN", "COMPRAS", "PRODUCAO", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/relacao-ops", "/relacao-ops**").hasAnyAuthority("ADMIN", "FINANCEIRO", "PRODUCAO", "COMPRAS", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO", "ALMOXARIFADO", "FABRICA", "ADMIN", "COMPRAS", "PRODUCAO", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/relacao-empenhos", "/relacao-empenhos**").hasAnyAuthority("ADMIN", "FINANCEIRO", "PRODUCAO", "COMPRAS", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO", "ALMOXARIFADO")
		.antMatchers("/relacao-scs", "/relacao-scs**").hasAnyAuthority("ADMIN", "FINANCEIRO", "COMPRAS", "PRODUCAO", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO", "ALMOXARIFADO")
		.antMatchers("/relacao-pcs", "/relacao-pcs**").hasAnyAuthority("ADMIN", "FINANCEIRO", "PRODUCAO", "COMPRAS", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO", "ALMOXARIFADO")
		.antMatchers("/kardex-produto", "/kardex-produto**").hasAnyAuthority("ADMIN", "FINANCEIRO", "PRODUCAO", "COMPRAS", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO", "ALMOXARIFADO")
		.antMatchers("/indicadores/producao/**").hasAnyAuthority("ADMIN", "FINANCEIRO", "PRODUCAO", "COMPRAS", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/indicadores/estoques/**").hasAnyAuthority("ADMIN", "FINANCEIRO", "PRODUCAO", "COMPRAS", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/utilitarios/**").hasAnyAuthority("ADMIN", "FINANCEIRO", "PRODUCAO", "COMPRAS", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/utilitarios/calculo-vendas**", "/utilitarios/calculo-vendas/**").hasAnyAuthority("ADMIN", "FINANCEIRO", "PRODUCAO", "COMPRAS", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/carteira/getProdutosCarteira**").hasAnyAuthority("ADMIN", "FINANCEIRO", "PRODUCAO", "COMPRAS", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/carteira", "/carteira/**").hasAnyAuthority("ADMIN", "FINANCEIRO", "PRODUCAO", "COMPRAS", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/status**", "/status/**").hasAnyAuthority("ADMIN", "FINANCEIRO", "PRODUCAO", "COMPRAS", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/check-list-expedicao**").hasAnyAuthority("ADMIN", "FINANCEIRO", "PRODUCAO", "COMPRAS", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/indicadores/vendas-por-linha", "/indicadores/vendas-por-linha/**").hasAnyAuthority("ADMIN", "FINANCEIRO", "FINANCEIRO", "PRODUCAO", "COMPRAS", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/carteira-pcp", "/carteira-pcp/**").hasAnyAuthority("FABRICA", "FINANCEIRO", "ADMIN", "COMPRAS", "PRODUCAO", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/follow-up-produto", "/follow-up-produto/**").hasAnyAuthority("FABRICA", "FINANCEIRO", "ADMIN", "COMPRAS", "PRODUCAO", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/carteira-faturamento", "/carteira-faturamento/**").hasAnyAuthority("FABRICA", "FINANCEIRO", "ADMIN", "COMPRAS", "PRODUCAO", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/indicadores/vendas/**").hasAnyAuthority("ADMIN", "FINANCEIRO", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/indicadores/mapa-vendas/**").hasAnyAuthority("ADMIN", "FINANCEIRO", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/programa-vendas", "/programa-vendas/**", "/programa-vendas/novo**").hasAnyAuthority("ADMIN", "FINANCEIRO", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/clientes/**").hasAnyAuthority("ADMIN", "FINANCEIRO", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/pedidos-venda/**").hasAnyAuthority("ADMIN", "FINANCEIRO", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/lista-de-precos/**").hasAnyAuthority("ADMIN", "FINANCEIRO", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/faturamento").hasAnyAuthority("ADMIN", "FINANCEIRO", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/check-list-expedicao**", "/check-list-expedicao/**").hasAnyAuthority("ADMIN", "FINANCEIRO", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/indicadores/faturamento/**").hasAnyAuthority("ADMIN", "FINANCEIRO", "FATURAMENTO", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/faturamento/pedidos").hasAnyAuthority("ADMIN", "FINANCEIRO", "FATURAMENTO", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/indicadores/compras/**").hasAnyAuthority("ADMIN", "FINANCEIRO", "COMPRAS", "PRODUCAO", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/pedidos-compra/**").hasAnyAuthority("ADMIN", "FINANCEIRO", "COMPRAS", "PRODUCAO", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/relacao-entradas**").hasAnyAuthority("ADMIN", "FINANCEIRO", "COMPRAS", "PRODUCAO", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA", "VENDAS-EXPORTACAO")
		.antMatchers("/indicadores/recebimento/analitico/valor-a-receber**").hasAnyAuthority("ADMIN", "FINANCEIRO", "VENDAS-EQUIPAMENTOS", "VENDAS-ORTOPEDIA")
		.antMatchers("/indicadores/recebimento**", "/indicadores/recebimento/**").hasAnyAuthority("ADMIN", "FINANCEIRO")
		.antMatchers("/custos", "/custos/**").hasAnyAuthority("ADMIN", "CUSTOS", "FINANCEIRO")
		.antMatchers("/indicadores/recursos-humanos", "/indicadores/recursos-humanos/**").hasAnyAuthority("ADMIN", "RECURSOS-HUMANOS")
		.antMatchers("/recursos-humanos/escolaridade/analitico", "/recursos-humanos/escolaridade/analitico/**").hasAnyAuthority("ADMIN", "RECURSOS-HUMANOS")
		.antMatchers("/relacao-estoques", "/relacao-estoques/**").hasAnyAuthority("ADMIN", "PRODUCAO")
		.antMatchers("/acompanhamento", "/indicadores", "/indicadores/", "/produtos/**", "/ordens-producao/**", "/estoques/itens-estoque", "/triangulacao/**").authenticated()
		.antMatchers("/img/**", "/css/**", "/js/**", "/pdf/Desenhos JPG/**", "/pdf/**").permitAll()
		.anyRequest().hasAuthority("ADMIN")
		.and().formLogin().permitAll()
		.and().logout().permitAll().logoutSuccessUrl("/").deleteCookies("JSESSIONID")
		.and().rememberMe();
	}
	

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().dataSource(dataSource).usersByUsernameQuery("select u.username, u.password, u.enabled from usuario u where u.username = ?")
		.authoritiesByUsernameQuery("select usuario_username, authorities from usuario_authorities where usuario_username = ?");
	}

}
