package br.com.BiTemplate.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.PathResourceResolver;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/pdf/**")
		.addResourceLocations("file:///O:/Desenhos em PDF/")
		.setCachePeriod(300)
		.resourceChain(true)
		.addResolver(new PathResourceResolver());
	}

}
