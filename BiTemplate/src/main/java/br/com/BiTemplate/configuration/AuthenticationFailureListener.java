package br.com.BiTemplate.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;

import br.com.BiTemplate.model.Usuario;
import br.com.BiTemplate.repository.UsuarioRepository;

@Component
public class AuthenticationFailureListener implements ApplicationListener<AuthenticationFailureBadCredentialsEvent> {

	@Autowired
	private UsuarioRepository usuarioRepository;

	public void onApplicationEvent(AuthenticationFailureBadCredentialsEvent ev) {

		String username = ev.getAuthentication().getName();
		
		Usuario usuario = usuarioRepository.findById(username).orElse(null);
		
		if (usuario != null) {
			usuario.reportLoginFailure();
			usuarioRepository.save(usuario);
		}
	}
}