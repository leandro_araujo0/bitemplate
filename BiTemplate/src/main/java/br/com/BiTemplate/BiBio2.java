package br.com.BiTemplate;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import br.com.BiTemplate.DAO.FinanceiroDAO;
import br.com.BiTemplate.DAO.ProdutoDAO;
import br.com.BiTemplate.model.DataTable;
import br.com.BiTemplate.model.Relatorio;
import br.com.BiTemplate.repository.RelatorioRepository;

@SpringBootApplication
@EnableAutoConfiguration(exclude = { org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration.class })
public class BiBio2 extends SpringBootServletInitializer {
	
	public static boolean debug = false;

	public static void main(String[] args) {
		for (String string : args) {
			if (string.equals("-debug")) {
				debug = true;
				System.out.println("Debug mode - on");
			}
		}
		SpringApplication.run(BiBio2.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(RelatorioRepository rr) {
		return args -> {
			if (debug) {
				return;
			}
			
			Date date = new Date();
			Timer timer = new Timer();

			timer.schedule(new TimerTask(){
			     public void run(){
			    	 int diaSemana = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
			    	 
			    	 if (diaSemana == Calendar.SATURDAY || diaSemana == Calendar.SUNDAY) {
			    		 return;
			    	 }
			    	 
			    	 
			    	 String nome = "clientesInadimplentes";
			    	 Relatorio relatorio = rr.findById(nome).orElse(null);
			    	 
			    	 if (relatorio == null) {
			    		 relatorio = new Relatorio(nome);
			    		 rr.save(relatorio);
			    	 }
			    	 
			    	 if (!relatorio.jaEnviado()) {
//		    		 if (true) {
			    		 reportarClientesInadimplentes();
			    		 relatorio.setDate(Calendar.getInstance());
			    		 rr.save(relatorio);
			    	 }
			    	 
			    	 nome = "custosMarkupBaixo";
			    	 relatorio = rr.findById(nome).orElse(null);
			    	 
			    	 if (relatorio == null) {
			    		 relatorio = new Relatorio(nome);
			    		 rr.save(relatorio);
			    	 }
			    	 
			    	 if (!relatorio.jaEnviado()) {
			    		 reportarMarkupsBaixos();
			    		 relatorio.setDate(Calendar.getInstance());
			    		 rr.save(relatorio);
			    	 }
			    	 
			     }
			},date, 1*30*60*1000);//24H*60M*60S*1000MS add 24 hours delay between job executions.
		};
	}

	protected void reportarMarkupsBaixos() {
		DataTable dataTable = ProdutoDAO.findByMarkupAbaixo(2);
		
		
		String assunto = "Custos x Markup";
		String corpo = "<h1>Relação de Produtos com Markup Comprometido</h1>";
		
		corpo += "<table style=\"border: 1px solid black; border-collapse: collapse; width: 1000px;\">";
		corpo += "	<thead>";
		corpo += "		<tr>";
		for (Object object : dataTable.getHeaders()) {
			String celula = (String) object;
			corpo += "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 10px;\">";
			corpo += celula;
			corpo += "</th>";
		}
		
		
		corpo += "		</tr>";
		corpo += "	</thead>";
		corpo += "	<tbody>";
		
		for (Collection<Object> collection : dataTable.getData()) {
			LinkedList<Object> linha = (LinkedList<Object>) collection;
//			System.out.println(linha);
			corpo += "<tr>";
			int i = 0;
			for (Object object : linha) {
				String celula = object.toString();
				if (i > 1) {
					corpo += "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 10px; text-align: center;\">";
				} else {
					corpo += "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 10px;\">";
				}
				corpo += celula;
				corpo += "</td>";
				i++;
			}
			corpo += "</tr>";
		}
		
		corpo += "	</tbody>";
		corpo += "</table>";
		
		
		String to = "leandro@ortosintese.com.br, silmaramartins@ortosintese.com.br, ortosintese@ortosintese.com.br";
//		String to = "leandro@ortosintese.com.br";
		enviarEmail(assunto, corpo, to);
	}

	private void reportarClientesInadimplentes() {
		DataTable dataTable = FinanceiroDAO.getClientesCarteiraInadimplentes();
		
		
		String assunto = "Recebimentos";
		String corpo = "<h1>Relação de Clientes Inadimplentes</h1>";
		
		corpo += "<table style=\"border: 1px solid black; border-collapse: collapse; width: 1000px;\">";
		corpo += "	<thead>";
		corpo += "		<tr>";
		for (Object object : dataTable.getHeaders()) {
			String celula = (String) object;
			corpo += "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 10px;\">";
			corpo += celula;
			corpo += "</th>";
		}
		
		
		corpo += "		</tr>";
		corpo += "	</thead>";
		corpo += "	<tbody>";
		
		for (Collection<Object> collection : dataTable.getData()) {
			LinkedList<Object> linha = (LinkedList<Object>) collection;
			corpo += "<tr>";
			int i = 0;
			for (Object object : linha) {
				String celula = (String) object;
				if (i > 1) {
					corpo += "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 10px; text-align: center;\">";
				} else {
					corpo += "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 10px;\">";
				}
				corpo += celula;
				corpo += "</td>";
				i++;
			}
			corpo += "</tr>";
		}
		
		corpo += "	</tbody>";
		corpo += "</table>";
		
		
		String to = "leandro@ortosintese.com.br, financeiro@ortosintese.com.br, ortosintese@ortosintese.com.br";
//		String to = "leandro@ortosintese.com.br";
		enviarEmail(assunto, corpo, to);
	}

	public static void enviarEmail(String assunto, String corpo, String to) {
		Properties props = new Properties();
	    props.put("mail.smtp.host", "smtp.office365.com");
	    props.put("mail.smtp.socketFactory.port", "587");
	    props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	    props.put("mail.smtp.auth", "true");
	    props.put("mail.smtp.port", "587");
	    
	    props.put("mail.smtp.starttls.enable", "true");
	    
	    Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
	    	protected PasswordAuthentication getPasswordAuthentication() {
                 return new PasswordAuthentication("suporte@ortosintese.com.br", "Lynce478");
	    	}
	    });
	    
//	    session.setDebug(true);
	    
	    try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("suporte@ortosintese.com.br")); 
			//Remetente
		    Address[] toUser = InternetAddress.parse(to);
		    message.setRecipients(Message.RecipientType.TO, toUser);
		    message.setSubject(assunto);//Assunto
		    message.setContent(corpo, "text/html; charset=UTF-8");
		    /**Método para enviar a mensagem criada*/
      Transport.send(message);
 
      System.out.println("Feito!!!");
    	} catch (MessagingException e) {
	        throw new RuntimeException(e);
	    }
	}
}
