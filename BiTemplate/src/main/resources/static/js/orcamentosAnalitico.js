$(document).ready(function() {
	$.ajax({
		type: 'POST',
		success: function(dataTable) {
			var columns = [];
			
			dataTable.headers.forEach(function(str, i) {
				columns.push({ title: str, class: str });
			});
			
			var table = $('#myTable').DataTable({
				columns: columns,
				data: dataTable.data,
				aaSorting: [ 
					[ 3, 'asc' ],
				],
		        columnDefs: [
		        {
		            render: function(data, type, row) {
		                if (type === 'display' || type === 'filter')
		                    return formatCurrency(data);
		                else
		                    return data;
		            },
		            targets: [5],
		        	}
		        ],
			});
		},
	});
});
