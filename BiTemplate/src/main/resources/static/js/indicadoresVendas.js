var graficoVendasOrtopedia
var graficoVendasEquipamentos;
var graficoVendasExportacaoOrtopedia
var graficoVendasExportacaoEquipamentos;
var graficoVendasHorizontal;
var graficoVendasMensal;
var graficoVendasDetalhadoCaixas;
var graficoVendasDetalhadoReposicao;
var graficoVendasDetalhadoExportacao;
var graficoVendasDetalhadoEquipamentos;
var graficoCarteiraPendente;
var graficoTopClientesOrtopedia;
var graficoTopClientesEquipamentos;
var graficoTopProdutosOrtopedia;
var graficoTopProdutosEquipamentos;
var graficoQtdOrcamentosOrtopedia;
var graficoQtdOrcamentosEquipamentos;
var graficoValorOrcamentosOrtopedia;
var graficoValorOrcamentosEquipamentos;
var graficoQtdOrcamentosOrtopediaAprovados;
var graficoValorOrcamentosOrtopediaAprovados;
var graficoQtdOrcamentosEquipamentosAprovados;
var graficoValorOrcamentosEquipamentosAprovados;
var graficoQtdOrcamentosOrtopediaCancelados;
var graficoValorOrcamentosOrtopediaCancelados;
var graficoQtdOrcamentosEquipamentosCancelados;
var graficoValorOrcamentosEquipamentosCancelados;

var month = new Date().getMonth() + 1;
var year = new Date().getFullYear();

$(document).ready(function() {
    $('canvas').after('<div class="spinner"></div>');
});

function atualizarGraficos() {
	$('.spinner').addClass('d-none');    
	
	var metas = $("#metas").attr('value');
	var url = '/indicadores/vendas/graficoVendasOrtopedia';
	url = (metas === 'true') ? url + '?metas=true' : url;
	$.get(url, function(chartData) {
        if (graficoVendasOrtopedia != null) {
            graficoVendasOrtopedia.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('graficoVendasOrtopedia').getContext('2d');
            graficoVendasOrtopedia = new Chart(ctx, chartData);
            graficoVendasOrtopedia.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
                return formatCurrencyAsMillion(value);
            }
            graficoVendasOrtopedia.options.tooltips.callbacks.label = function(label, a) {
            	if (label.index == new Date().getMonth() && a.datasets[label.datasetIndex].label.split(" ")[1] == new Date().getFullYear().toString()) {
            		return [formatCurrency(label.yLabel), "Previsão: " + formatCurrency(label.yLabel / new Date().getDate() * new Date(new Date().getYear(), new Date().getMonth(), 0).getDate())];
            	} else {
            		return formatCurrency(label.yLabel);
            	}
            }
            graficoVendasOrtopedia.options.scales.yAxes[0].ticks.beginAtZero = true;
            graficoVendasOrtopedia.options.onClick = function (evt, item) {
            	var ano = graficoVendasOrtopedia.getElementAtEvent(evt)[0]._view.datasetLabel.replace('Vendas ', '');
                if (ano.includes('Meta')) {
                    return;
                }
                var mes = graficoVendasOrtopedia.getElementAtEvent(evt)[0]._view.label;
                abrirLink(evt, '/pedidos-venda/relacao-vendas?clasped=MERCADOINTERNO' + '&mes=' + mes + '&ano=' + ano);
            };
        }
        graficoVendasOrtopedia.update();
        $('#graficoVendasOrtopedia').next().addClass('d-none');
    });
    
	url = '/indicadores/vendas/graficoVendasEquipamentos';
	url = (metas === 'true') ? url + '?metas=true' : url;
    $.get(url, function(chartData) {
        if (graficoVendasEquipamentos != null) {
            graficoVendasEquipamentos.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('graficoVendasEquipamentos').getContext('2d');
            graficoVendasEquipamentos = new Chart(ctx, chartData);
            graficoVendasEquipamentos.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
                return formatCurrencyAsMillion(value);
            }
            graficoVendasEquipamentos.options.tooltips.callbacks.label = function(label, a) {
            	if (label.index == new Date().getMonth() && a.datasets[label.datasetIndex].label.split(" ")[1] == new Date().getFullYear().toString()) {
            		return [formatCurrency(label.yLabel), "Previsão: " + formatCurrency(label.yLabel / new Date().getDate() * new Date(new Date().getYear(), new Date().getMonth(), 0).getDate())];
            	} else {
            		return formatCurrency(label.yLabel);
            	}
            }
            graficoVendasEquipamentos.options.onClick = function (evt, item) {
                var ano = graficoVendasEquipamentos.getElementAtEvent(evt)[0]._view.datasetLabel.replace('Vendas ', '');
                if (ano.includes('Meta')) {
                    return;
                }
                var mes = graficoVendasEquipamentos.getElementAtEvent(evt)[0]._view.label;
                abrirLink(evt, '/pedidos-venda/relacao-vendas?clasped=EQUIPAMENTOS' + '&mes=' + mes + '&ano=' + ano);
            };
            graficoVendasEquipamentos.options.scales.yAxes[0].ticks.beginAtZero = true;
        }
        graficoVendasEquipamentos.update();
        $('#graficoVendasEquipamentos').next().addClass('d-none');
    });
    
	url = '/indicadores/vendas/graficoVendasExportacaoOrtopedia';
	url = (metas === 'true') ? url + '?metas=true' : url;
    $.get(url, function(chartData) {
    	var tempGrafico = graficoVendasExportacaoOrtopedia;
        if (tempGrafico != null) {
        	tempGrafico.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('graficoVendasExportacaoOrtopedia').getContext('2d');
            tempGrafico = new Chart(ctx, chartData);
            tempGrafico.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
                return formatCurrencyAsMillion(value);
            }
            tempGrafico.options.tooltips.callbacks.label = function(label, a) {
            	if (label.index == new Date().getMonth() && a.datasets[label.datasetIndex].label.split(" ")[1] == new Date().getFullYear().toString()) {
            		return [formatCurrency(label.yLabel), "Previsão: " + formatCurrency(label.yLabel / new Date().getDate() * new Date(new Date().getYear(), new Date().getMonth(), 0).getDate())];
            	} else {
            		return formatCurrency(label.yLabel);
            	}
            }
            tempGrafico.options.onClick = function (evt, item) {
                var ano = tempGrafico.getElementAtEvent(evt)[0]._view.datasetLabel.replace('Vendas ', '');
                if (ano.includes('Meta')) {
                    return;
                }
                var mes = tempGrafico.getElementAtEvent(evt)[0]._view.label;
                abrirLink(evt, '/pedidos-venda/relacao-vendas?clasped=EXPORTACAO' + '&mes=' + mes + '&ano=' + ano + '&unidade=ORTOPEDIAEXPORTACAO');
            };
            tempGrafico.options.scales.yAxes[0].ticks.beginAtZero = true;
        }
        tempGrafico.update();
        $('#graficoVendasExportacaoOrtopedia').next().addClass('d-none');
    });
    
	url = '/indicadores/vendas/graficoVendasExportacaoEquipamentos';
	url = (metas === 'true') ? url + '?metas=true' : url;
    $.get(url, function(chartData) {
    	var tempGrafico = graficoVendasExportacaoEquipamentos;
        if (tempGrafico != null) {
        	tempGrafico.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('graficoVendasExportacaoEquipamentos').getContext('2d');
            tempGrafico = new Chart(ctx, chartData);
            tempGrafico.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
                return formatCurrencyAsMillion(value);
            }
            tempGrafico.options.tooltips.callbacks.label = function(label, a) {
            	if (label.index == new Date().getMonth() && a.datasets[label.datasetIndex].label.split(" ")[1] == new Date().getFullYear().toString()) {
            		return [formatCurrency(label.yLabel), "Previsão: " + formatCurrency(label.yLabel / new Date().getDate() * new Date(new Date().getYear(), new Date().getMonth(), 0).getDate())];
            	} else {
            		return formatCurrency(label.yLabel);
            	}
            }
            tempGrafico.options.onClick = function (evt, item) {
                var ano = tempGrafico.getElementAtEvent(evt)[0]._view.datasetLabel.replace('Vendas ', '');
                if (ano.includes('Meta')) {
                    return;
                }
                var mes = tempGrafico.getElementAtEvent(evt)[0]._view.label;
                abrirLink(evt, '/pedidos-venda/relacao-vendas?clasped=EXPORTACAO' + '&mes=' + mes + '&ano=' + ano + '&unidade=EQUIPAMENTOSEXPORTACAO');
            };
            tempGrafico.options.scales.yAxes[0].ticks.beginAtZero = true;
        }
        tempGrafico.update();
        $('#graficoVendasExportacaoEquipamentos').next().addClass('d-none');
    });
    
    $.get('/indicadores/vendas/graficoVendasHorizontal', function(chartData) {
        if (graficoVendasHorizontal != null) {
            graficoVendasHorizontal.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('graficoVendasHorizontal').getContext('2d');
            graficoVendasHorizontal = new Chart(ctx, chartData);
            graficoVendasHorizontal.options.scales.xAxes[0].ticks.beginAtZero = true;
            graficoVendasHorizontal.options.scales.xAxes[0].ticks.callback = function(value, index, values) {
                return formatCurrency(value);
            }
            graficoVendasHorizontal.options.tooltips.callbacks.label = function(label) {
                return formatCurrency(label.xLabel);
            }
            graficoVendasHorizontal.options.scales.xAxes[0].ticks.suggestedMax = 60000000;
            graficoVendasHorizontal.options.legend.display = false;
        }
        graficoVendasHorizontal.update();
        $('#graficoVendasHorizontal').next().addClass('d-none');
    });
    
    $.get('/indicadores/vendas/graficoVendasDetalhadoCaixas', function(chartData) {
        if (graficoVendasDetalhadoCaixas != null) {
            graficoVendasDetalhadoCaixas.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('graficoVendasDetalhadoCaixas').getContext('2d');
            graficoVendasDetalhadoCaixas = new Chart(ctx, chartData);
            graficoVendasDetalhadoCaixas.options.legend.display = false;
            graficoVendasDetalhadoCaixas.options.scales.yAxes[0].ticks.beginAtZero = true;
            graficoVendasDetalhadoCaixas.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
                return formatCurrencyAsMil(value);
            };
            graficoVendasDetalhadoCaixas.options.tooltips.callbacks.label = function(label) {
                return formatCurrency(label.yLabel);
            };
            graficoVendasDetalhadoCaixas.options.onClick = function (evt, item) {
                if (item[0] != null) {
                    var obj = item[0]['_model'].label;
                    abrirLink(evt, '/pedidos-venda/relacao-vendas?clasped=CAIXAS&dia=' + obj + '&mes=' + month + '&ano=' + year);
                }
            };
        }
        graficoVendasDetalhadoCaixas.update();
        $('#graficoVendasDetalhadoCaixas').next().addClass('d-none');
    });
    
    $.get('/indicadores/vendas/graficoVendasDetalhadoReposicao', function(chartData) {
        if (graficoVendasDetalhadoReposicao != null) {
            graficoVendasDetalhadoReposicao.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('graficoVendasDetalhadoReposicao').getContext('2d');
            graficoVendasDetalhadoReposicao = new Chart(ctx, chartData);
            graficoVendasDetalhadoReposicao.options.legend.display = false;
            graficoVendasDetalhadoReposicao.options.scales.yAxes[0].ticks.beginAtZero = true;
            graficoVendasDetalhadoReposicao.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
                return formatCurrencyAsMil(value);
            };
            graficoVendasDetalhadoReposicao.options.tooltips.callbacks.label = function(label) {
                return formatCurrency(label.yLabel);
            };
            graficoVendasDetalhadoReposicao.options.onClick = function (evt, item) {
                if (item[0] != null) {
                    var obj = item[0]['_model'].label.split(';')[0];
                    abrirLink(evt, '/pedidos-venda/relacao-vendas?clasped=REPOSICAO&dia=' + obj + '&mes=' + month + '&ano=' + year);
                }
            };
        }
        graficoVendasDetalhadoReposicao.update();
        $('#graficoVendasDetalhadoReposicao').next().addClass('d-none');
    });

    $.get('/indicadores/vendas/graficoVendasDetalhadoExportacao', function(chartData) {
        if (graficoVendasDetalhadoExportacao != null) {
            graficoVendasDetalhadoExportacao.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('graficoVendasDetalhadoExportacao').getContext('2d');
            graficoVendasDetalhadoExportacao = new Chart(ctx, chartData);
            graficoVendasDetalhadoExportacao.options.legend.display = false;
            graficoVendasDetalhadoExportacao.options.scales.yAxes[0].ticks.beginAtZero = true;
            graficoVendasDetalhadoExportacao.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
                return formatCurrencyAsMil(value);
            };
            graficoVendasDetalhadoExportacao.options.tooltips.callbacks.label = function(label) {
                return formatCurrency(label.yLabel);
            };
            graficoVendasDetalhadoExportacao.options.onClick = function (evt, item) {
                if (item[0] != null) {
                    var obj = item[0]['_model'].label.split(';')[0];
                    abrirLink(evt, '/pedidos-venda/relacao-vendas?clasped=EXPORTACAO&dia=' + obj + '&mes=' + month + '&ano=' + year);
                }
            };
        }
        graficoVendasDetalhadoExportacao.update();
        $('#graficoVendasDetalhadoExportacao').next().addClass('d-none');
    });
    
    $.get('/indicadores/vendas/graficoVendasDetalhadoEquipamentos', function(chartData) {
        if (graficoVendasDetalhadoEquipamentos != null) {
            graficoVendasDetalhadoEquipamentos.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('graficoVendasDetalhadoEquipamentos').getContext('2d');
            graficoVendasDetalhadoEquipamentos = new Chart(ctx, chartData);
            graficoVendasDetalhadoEquipamentos.options.legend.display = false;
            graficoVendasDetalhadoEquipamentos.options.scales.yAxes[0].ticks.beginAtZero = true;
            graficoVendasDetalhadoEquipamentos.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
                return formatCurrencyAsMil(value);
            };
            graficoVendasDetalhadoEquipamentos.options.tooltips.callbacks.label = function(label) {
                return formatCurrency(label.yLabel);
            };
            graficoVendasDetalhadoEquipamentos.options.onClick = function (evt, item) {
                if (item[0] != null) {
                    var obj = item[0]['_model'].label.split(';')[0];
                    abrirLink(evt, '/pedidos-venda/relacao-vendas?clasped=EQUIPAMENTOS&dia=' + obj + '&mes=' + month + '&ano=' + year);
                }
            };
        }
        graficoVendasDetalhadoEquipamentos.update();
        $('#graficoVendasDetalhadoEquipamentos').next().addClass('d-none');
    });


    $.get('/indicadores/vendas/tabelaVendasDiaria', function(pedidosVenda) {
        //#tableVendasDia
        var body = $('#tableVendasDia').find('tbody');
        $(body).remove();
        body = $('#tableVendasDia').append('<tbody></tbody>');
        
        var linha;

        linha = '<tr><th>Numero</th>';
        $(pedidosVenda).each(function() {
            linha += '<td><a href="/pedidos-venda/' + this.numero + '/detalhes">' + this.numero + '</a></td>';
        });
        $(body).append(linha + '</tr>');

        linha = '<tr><th>Cliente</th>';
        $(pedidosVenda).each(function() {
            linha += '<td><a href="/clientes/' + this.cliente.codigo + '/detalhes">' + this.cliente.nome + '</a></td>';
        });
        $(body).append(linha + '</tr>');

        linha = '<tr><th>Unidade</th>';
        $(pedidosVenda).each(function() {
            linha += '<td>' + this.unidade + '</td>';
        });
        $(body).append(linha + '</tr>');

        linha = '<tr><th>ClasPed</th>';
        $(pedidosVenda).each(function() {
            linha += '<td>' + this.clasPed + '</td>';
        });
        $(body).append(linha + '</tr>');

        linha = '<tr><th>Valor Total</th>';
        $(pedidosVenda).each(function() {
            linha += '<td class="nowrap">' + this.valorTotalAsCurrency + '</td>';
        });
        $(body).append(linha + '</tr>');
    });
    
    $.get('/indicadores/vendas/graficoTopClientesOrtopedia', function(chartData) {
        if (graficoTopClientesOrtopedia != null) {
            graficoTopClientesOrtopedia.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('graficoTopClientesOrtopedia').getContext('2d');
            graficoTopClientesOrtopedia = new Chart(ctx, chartData);
            graficoTopClientesOrtopedia.options.scales.xAxes[0].ticks.callback = function(value, index, values) {
                return formatCurrency(value);
            }
            graficoTopClientesOrtopedia.options.legend.display = false;
            graficoTopClientesOrtopedia.options.tooltips.callbacks.label = function(label) {
                return formatCurrency(label.xLabel);
            }
            graficoTopClientesOrtopedia.options.scales.xAxes[0].ticks.beginAtZero = true;
            graficoTopClientesOrtopedia.options.onClick = function (evt, item) {
                if (item[0] != null) {
                    var obj = item[0]['_model'].label.split(';')[0];
                    abrirLink(evt, '/clientes/' + obj + '/detalhes');
                }
            };
        }
        graficoTopClientesOrtopedia.update();
        $('#graficoTopClientesOrtopedia').next().addClass('d-none');
    });

    
    $.get('/indicadores/vendas/graficoTopClientesEquipamentos', function(chartData) {
        if (graficoTopClientesEquipamentos != null) {
            graficoTopClientesEquipamentos.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('graficoTopClientesEquipamentos').getContext('2d');
            graficoTopClientesEquipamentos = new Chart(ctx, chartData);
            graficoTopClientesEquipamentos.options.scales.xAxes[0].ticks.callback = function(value, index, values) {
                return formatCurrency(value);
            }
            graficoTopClientesEquipamentos.options.legend.display = false;
            graficoTopClientesEquipamentos.options.tooltips.callbacks.label = function(label) {
                return formatCurrency(label.xLabel);
            }
            graficoTopClientesEquipamentos.options.scales.xAxes[0].ticks.beginAtZero = true;
            graficoTopClientesEquipamentos.options.onClick = function (evt, item) {
                if (item[0] != null) {
                    var obj = item[0]['_model'].label.split(';')[0];
                    abrirLink(evt, '/clientes/' + obj + '/detalhes');
                }
            };
        }
        graficoTopClientesEquipamentos.update();
        $('#graficoTopClientesEquipamentos').next().addClass('d-none');
    });

    
    $.get('/indicadores/vendas/graficoTopProdutosOrtopedia', function(chartData) {
        if (graficoTopProdutosOrtopedia != null) {
            graficoTopProdutosOrtopedia.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('graficoTopProdutosOrtopedia').getContext('2d');
            graficoTopProdutosOrtopedia = new Chart(ctx, chartData);
            graficoTopProdutosOrtopedia.options.scales.xAxes[0].ticks.callback = function(value, index, values) {
                return formatCurrency(value);
            }
            graficoTopProdutosOrtopedia.options.legend.display = false;
            graficoTopProdutosOrtopedia.options.tooltips.callbacks.label = function(label) {
                return formatCurrency(label.xLabel);
            }
            graficoTopProdutosOrtopedia.options.scales.xAxes[0].ticks.beginAtZero = true;
//            graficoTopProdutosOrtopedia.options.onClick = function (evt, item) {
//                if (item[0] != null) {
//                    var obj = item[0]['_model'].label.split(';')[0];
//                    abrirLink(evt, '/produtos/detalhes?codigoProduto=' + obj);
//                }
//            };
        }
        graficoTopProdutosOrtopedia.update();
        $('#graficoTopProdutosOrtopedia').next().addClass('d-none');
    });

    
    $.get('/indicadores/vendas/graficoTopProdutosEquipamentos', function(chartData) {
        if (graficoTopProdutosEquipamentos != null) {
            graficoTopProdutosEquipamentos.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('graficoTopProdutosEquipamentos').getContext('2d');
            graficoTopProdutosEquipamentos = new Chart(ctx, chartData);
            graficoTopProdutosEquipamentos.options.scales.xAxes[0].ticks.callback = function(value, index, values) {
                return formatCurrency(value);
            }
            graficoTopProdutosEquipamentos.options.legend.display = false;
            graficoTopProdutosEquipamentos.options.tooltips.callbacks.label = function(label) {
                return formatCurrency(label.xLabel);
            }
            graficoTopProdutosEquipamentos.options.scales.xAxes[0].ticks.beginAtZero = true;
//            graficoTopProdutosEquipamentos.options.onClick = function (evt, item) {
//                if (item[0] != null) {
//                    var obj = item[0]['_model'].label.split(';')[0];
//                    abrirLink(evt, '/produtos/detalhes?codigoProduto=' + obj);
//                }
//            };
        }
        graficoTopProdutosEquipamentos.update();
        $('#graficoTopProdutosEquipamentos').next().addClass('d-none');
    });
    
    $.get('/indicadores/vendas/qtdOrcamentos?unidade=ORTOPEDIA', function(chartData) {
        if (graficoQtdOrcamentosOrtopedia != null) {
        	graficoQtdOrcamentosOrtopedia.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('qtdOrcamentosOrtopedia').getContext('2d');
            graficoQtdOrcamentosOrtopedia = new Chart(ctx, chartData);
            graficoQtdOrcamentosOrtopedia.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
                return value;
            }
            graficoQtdOrcamentosOrtopedia.options.legend.display = false;
            graficoQtdOrcamentosOrtopedia.options.tooltips.callbacks.label = function(label) {
                return label.yLabel;
            }
            graficoQtdOrcamentosOrtopedia.options.scales.yAxes[0].ticks.beginAtZero = true;
            graficoQtdOrcamentosOrtopedia.options.onClick = function (evt, item) {
                if (item[0] != null) {
                    var dia = item[0]['_model'].label;
                    var mes = new Date().getMonth() + 1;
                    var ano = new Date().getFullYear();
                    abrirLink(evt, '/relacao-orcamentos?dia=' + dia + '&mes=' + mes + '&ano=' + ano + '&unidade=ORTOPEDIA');
                }
            };
        }
        graficoQtdOrcamentosOrtopedia.update();
        $('#qtdOrcamentosOrtopedia').next().addClass('d-none');
    });
    
    $.get('/indicadores/vendas/valorOrcamentos?unidade=ORTOPEDIA', function(chartData) {
    	if (graficoValorOrcamentosOrtopedia != null) {
    		graficoValorOrcamentosOrtopedia.data.datasets = chartData.data.datasets;
    	} else {
    		var ctx = document.getElementById('valorOrcamentosOrtopedia').getContext('2d');
    		graficoValorOrcamentosOrtopedia = new Chart(ctx, chartData);
    		graficoValorOrcamentosOrtopedia.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
    			return formatCurrencyAsMil(value);
    		}
    		graficoValorOrcamentosOrtopedia.options.legend.display = false;
    		graficoValorOrcamentosOrtopedia.options.tooltips.callbacks.label = function(label) {
                return formatCurrency(label.yLabel);
    		}
    		graficoValorOrcamentosOrtopedia.options.scales.yAxes[0].ticks.beginAtZero = true;
    		graficoValorOrcamentosOrtopedia.options.onClick = function (evt, item) {
    			if (item[0] != null) {
    				var dia = item[0]['_model'].label;
                    var mes = new Date().getMonth() + 1;
                    var ano = new Date().getFullYear();
                    abrirLink(evt, '/relacao-orcamentos?dia=' + dia + '&mes=' + mes + '&ano=' + ano + '&unidade=ORTOPEDIA');
    			}
    		};
    	}
    	graficoValorOrcamentosOrtopedia.update();
    	$('#valorOrcamentosOrtopedia').next().addClass('d-none');
    });
    
    $.get('/indicadores/vendas/qtdOrcamentos?unidade=EQUIPAMENTOS', function(chartData) {
    	if (graficoQtdOrcamentosEquipamentos != null) {
    		graficoQtdOrcamentosEquipamentos.data.datasets = chartData.data.datasets;
    	} else {
    		var ctx = document.getElementById('qtdOrcamentosEquipamentos').getContext('2d');
    		graficoQtdOrcamentosEquipamentos = new Chart(ctx, chartData);
    		graficoQtdOrcamentosEquipamentos.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
    			return value;
    		}
    		graficoQtdOrcamentosEquipamentos.options.legend.display = false;
    		graficoQtdOrcamentosEquipamentos.options.tooltips.callbacks.label = function(label) {
    			return label.yLabel;
    		}
    		graficoQtdOrcamentosEquipamentos.options.scales.yAxes[0].ticks.beginAtZero = true;
    		graficoQtdOrcamentosEquipamentos.options.onClick = function (evt, item) {
    			if (item[0] != null) {
    				var dia = item[0]['_model'].label;
                    var mes = new Date().getMonth() + 1;
                    var ano = new Date().getFullYear();
                    abrirLink(evt, '/relacao-orcamentos?dia=' + dia + '&mes=' + mes + '&ano=' + ano + '&unidade=EQUIPAMENTOS');
    			}
    		};
    	}
    	graficoQtdOrcamentosEquipamentos.update();
    	$('#qtdOrcamentosEquipamentos').next().addClass('d-none');
    });
    
    $.get('/indicadores/vendas/valorOrcamentos?unidade=EQUIPAMENTOS', function(chartData) {
    	if (graficoValorOrcamentosEquipamentos != null) {
    		graficoValorOrcamentosEquipamentos.data.datasets = chartData.data.datasets;
    	} else {
    		var ctx = document.getElementById('valorOrcamentosEquipamentos').getContext('2d');
    		graficoValorOrcamentosEquipamentos = new Chart(ctx, chartData);
    		graficoValorOrcamentosEquipamentos.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
    			return formatCurrencyAsMil(value);
    		}
    		graficoValorOrcamentosEquipamentos.options.legend.display = false;
    		graficoValorOrcamentosEquipamentos.options.tooltips.callbacks.label = function(label) {
                return formatCurrency(label.yLabel);
    		}
    		graficoValorOrcamentosEquipamentos.options.scales.yAxes[0].ticks.beginAtZero = true;
    		graficoValorOrcamentosEquipamentos.options.onClick = function (evt, item) {
    			if (item[0] != null) {
    				var dia = item[0]['_model'].label;
                    var mes = new Date().getMonth() + 1;
                    var ano = new Date().getFullYear();
                    abrirLink(evt, '/relacao-orcamentos?dia=' + dia + '&mes=' + mes + '&ano=' + ano + '&unidade=EQUIPAMENTOS');
    			}
    		};
    	}
    	graficoValorOrcamentosEquipamentos.update();
    	$('#valorOrcamentosEquipamentos').next().addClass('d-none');
    });
    
    
    $.get('/indicadores/vendas/qtdOrcamentos?unidade=ORTOPEDIA&statusOrcamento=APROVADOS', function(chartData) {
    	if (graficoQtdOrcamentosOrtopediaAprovados != null) {
    		graficoQtdOrcamentosOrtopediaAprovados.data.datasets = chartData.data.datasets;
    	} else {
    		var ctx = document.getElementById('qtdOrcamentosOrtopediaAprovados').getContext('2d');
    		graficoQtdOrcamentosOrtopediaAprovados = new Chart(ctx, chartData);
    		graficoQtdOrcamentosOrtopediaAprovados.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
    			return value;
    		}
    		graficoQtdOrcamentosOrtopediaAprovados.options.legend.display = false;
    		graficoQtdOrcamentosOrtopediaAprovados.options.tooltips.callbacks.label = function(label) {
    			return label.yLabel;
    		}
    		graficoQtdOrcamentosOrtopediaAprovados.options.scales.yAxes[0].ticks.beginAtZero = true;
    		graficoQtdOrcamentosOrtopediaAprovados.options.onClick = function (evt, item) {
    			if (item[0] != null) {
    				var dia = item[0]['_model'].label;
    				var mes = new Date().getMonth() + 1;
    				var ano = new Date().getFullYear();
    				abrirLink(evt, '/relacao-orcamentos?dia=' + dia + '&mes=' + mes + '&ano=' + ano + '&unidade=ORTOPEDIA');
    			}
    		};
    	}
    	graficoQtdOrcamentosOrtopediaAprovados.update();
    	$('#qtdOrcamentosOrtopediaAprovados').next().addClass('d-none');
    });
    
    $.get('/indicadores/vendas/valorOrcamentos?unidade=ORTOPEDIA&statusOrcamento=APROVADOS', function(chartData) {
    	if (graficoValorOrcamentosOrtopediaAprovados != null) {
    		graficoValorOrcamentosOrtopediaAprovados.data.datasets = chartData.data.datasets;
    	} else {
    		var ctx = document.getElementById('valorOrcamentosOrtopediaAprovados').getContext('2d');
    		graficoValorOrcamentosOrtopediaAprovados = new Chart(ctx, chartData);
    		graficoValorOrcamentosOrtopediaAprovados.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
    			return formatCurrencyAsMil(value);
    		}
    		graficoValorOrcamentosOrtopediaAprovados.options.legend.display = false;
    		graficoValorOrcamentosOrtopediaAprovados.options.tooltips.callbacks.label = function(label) {
    			return formatCurrency(label.yLabel);
    		}
    		graficoValorOrcamentosOrtopediaAprovados.options.scales.yAxes[0].ticks.beginAtZero = true;
    		graficoValorOrcamentosOrtopediaAprovados.options.onClick = function (evt, item) {
    			if (item[0] != null) {
    				var dia = item[0]['_model'].label;
    				var mes = new Date().getMonth() + 1;
    				var ano = new Date().getFullYear();
    				abrirLink(evt, '/relacao-orcamentos?dia=' + dia + '&mes=' + mes + '&ano=' + ano + '&unidade=ORTOPEDIA');
    			}
    		};
    	}
    	graficoValorOrcamentosOrtopediaAprovados.update();
    	$('#valorOrcamentosOrtopediaAprovados').next().addClass('d-none');
    });
    
    $.get('/indicadores/vendas/qtdOrcamentos?unidade=EQUIPAMENTOS&statusOrcamento=APROVADOS', function(chartData) {
    	if (graficoQtdOrcamentosEquipamentosAprovados != null) {
    		graficoQtdOrcamentosEquipamentosAprovados.data.datasets = chartData.data.datasets;
    	} else {
    		var ctx = document.getElementById('qtdOrcamentosEquipamentosAprovados').getContext('2d');
    		graficoQtdOrcamentosEquipamentosAprovados = new Chart(ctx, chartData);
    		graficoQtdOrcamentosEquipamentosAprovados.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
    			return value;
    		}
    		graficoQtdOrcamentosEquipamentosAprovados.options.legend.display = false;
    		graficoQtdOrcamentosEquipamentosAprovados.options.tooltips.callbacks.label = function(label) {
    			return label.yLabel;
    		}
    		graficoQtdOrcamentosEquipamentosAprovados.options.scales.yAxes[0].ticks.beginAtZero = true;
    		graficoQtdOrcamentosEquipamentosAprovados.options.onClick = function (evt, item) {
    			if (item[0] != null) {
    				var dia = item[0]['_model'].label;
    				var mes = new Date().getMonth() + 1;
    				var ano = new Date().getFullYear();
    				abrirLink(evt, '/relacao-orcamentos?dia=' + dia + '&mes=' + mes + '&ano=' + ano + '&unidade=EQUIPAMENTOS');
    			}
    		};
    	}
    	graficoQtdOrcamentosEquipamentosAprovados.update();
    	$('#qtdOrcamentosEquipamentosAprovados').next().addClass('d-none');
    });
    
    $.get('/indicadores/vendas/valorOrcamentos?unidade=EQUIPAMENTOS&statusOrcamento=APROVADOS', function(chartData) {
    	if (graficoValorOrcamentosEquipamentosAprovados != null) {
    		graficoValorOrcamentosEquipamentosAprovados.data.datasets = chartData.data.datasets;
    	} else {
    		var ctx = document.getElementById('valorOrcamentosEquipamentosAprovados').getContext('2d');
    		graficoValorOrcamentosEquipamentosAprovados = new Chart(ctx, chartData);
    		graficoValorOrcamentosEquipamentosAprovados.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
    			return formatCurrencyAsMil(value);
    		}
    		graficoValorOrcamentosEquipamentosAprovados.options.legend.display = false;
    		graficoValorOrcamentosEquipamentosAprovados.options.tooltips.callbacks.label = function(label) {
    			return formatCurrency(label.yLabel);
    		}
    		graficoValorOrcamentosEquipamentosAprovados.options.scales.yAxes[0].ticks.beginAtZero = true;
    		graficoValorOrcamentosEquipamentosAprovados.options.onClick = function (evt, item) {
    			if (item[0] != null) {
    				var dia = item[0]['_model'].label;
    				var mes = new Date().getMonth() + 1;
    				var ano = new Date().getFullYear();
    				abrirLink(evt, '/relacao-orcamentos?dia=' + dia + '&mes=' + mes + '&ano=' + ano + '&unidade=EQUIPAMENTOS');
    			}
    		};
    	}
    	graficoValorOrcamentosEquipamentosAprovados.update();
    	$('#valorOrcamentosEquipamentosAprovados').next().addClass('d-none');
    });
    
    
    $.get('/indicadores/vendas/qtdOrcamentos?unidade=ORTOPEDIA&statusOrcamento=CANCELADOS', function(chartData) {
    	if (graficoQtdOrcamentosOrtopediaCancelados != null) {
    		graficoQtdOrcamentosOrtopediaCancelados.data.datasets = chartData.data.datasets;
    	} else {
    		var ctx = document.getElementById('qtdOrcamentosOrtopediaCancelados').getContext('2d');
    		graficoQtdOrcamentosOrtopediaCancelados = new Chart(ctx, chartData);
    		graficoQtdOrcamentosOrtopediaCancelados.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
    			return value;
    		}
    		graficoQtdOrcamentosOrtopediaCancelados.options.legend.display = false;
    		graficoQtdOrcamentosOrtopediaCancelados.options.tooltips.callbacks.label = function(label) {
    			return label.yLabel;
    		}
    		graficoQtdOrcamentosOrtopediaCancelados.options.scales.yAxes[0].ticks.beginAtZero = true;
    		graficoQtdOrcamentosOrtopediaCancelados.options.onClick = function (evt, item) {
    			if (item[0] != null) {
    				var dia = item[0]['_model'].label;
    				var mes = new Date().getMonth() + 1;
    				var ano = new Date().getFullYear();
    				abrirLink(evt, '/relacao-orcamentos?dia=' + dia + '&mes=' + mes + '&ano=' + ano + '&unidade=ORTOPEDIA');
    			}
    		};
    	}
    	graficoQtdOrcamentosOrtopediaCancelados.update();
    	$('#qtdOrcamentosOrtopediaCancelados').next().addClass('d-none');
    });
    
    $.get('/indicadores/vendas/valorOrcamentos?unidade=ORTOPEDIA&statusOrcamento=CANCELADOS', function(chartData) {
    	if (graficoValorOrcamentosOrtopediaCancelados != null) {
    		graficoValorOrcamentosOrtopediaCancelados.data.datasets = chartData.data.datasets;
    	} else {
    		var ctx = document.getElementById('valorOrcamentosOrtopediaCancelados').getContext('2d');
    		graficoValorOrcamentosOrtopediaCancelados = new Chart(ctx, chartData);
    		graficoValorOrcamentosOrtopediaCancelados.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
    			return formatCurrencyAsMil(value);
    		}
    		graficoValorOrcamentosOrtopediaCancelados.options.legend.display = false;
    		graficoValorOrcamentosOrtopediaCancelados.options.tooltips.callbacks.label = function(label) {
    			return formatCurrency(label.yLabel);
    		}
    		graficoValorOrcamentosOrtopediaCancelados.options.scales.yAxes[0].ticks.beginAtZero = true;
    		graficoValorOrcamentosOrtopediaCancelados.options.onClick = function (evt, item) {
    			if (item[0] != null) {
    				var dia = item[0]['_model'].label;
    				var mes = new Date().getMonth() + 1;
    				var ano = new Date().getFullYear();
    				abrirLink(evt, '/relacao-orcamentos?dia=' + dia + '&mes=' + mes + '&ano=' + ano + '&unidade=ORTOPEDIA');
    			}
    		};
    	}
    	graficoValorOrcamentosOrtopediaCancelados.update();
    	$('#valorOrcamentosOrtopediaCancelados').next().addClass('d-none');
    });
    
    $.get('/indicadores/vendas/qtdOrcamentos?unidade=EQUIPAMENTOS&statusOrcamento=CANCELADOS', function(chartData) {
    	if (graficoQtdOrcamentosEquipamentosCancelados != null) {
    		graficoQtdOrcamentosEquipamentosCancelados.data.datasets = chartData.data.datasets;
    	} else {
    		var ctx = document.getElementById('qtdOrcamentosEquipamentosCancelados').getContext('2d');
    		graficoQtdOrcamentosEquipamentosCancelados = new Chart(ctx, chartData);
    		graficoQtdOrcamentosEquipamentosCancelados.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
    			return value;
    		}
    		graficoQtdOrcamentosEquipamentosCancelados.options.legend.display = false;
    		graficoQtdOrcamentosEquipamentosCancelados.options.tooltips.callbacks.label = function(label) {
    			return label.yLabel;
    		}
    		graficoQtdOrcamentosEquipamentosCancelados.options.scales.yAxes[0].ticks.beginAtZero = true;
    		graficoQtdOrcamentosEquipamentosCancelados.options.onClick = function (evt, item) {
    			if (item[0] != null) {
    				var dia = item[0]['_model'].label;
    				var mes = new Date().getMonth() + 1;
    				var ano = new Date().getFullYear();
    				abrirLink(evt, '/relacao-orcamentos?dia=' + dia + '&mes=' + mes + '&ano=' + ano + '&unidade=EQUIPAMENTOS');
    			}
    		};
    	}
    	graficoQtdOrcamentosEquipamentosCancelados.update();
    	$('#qtdOrcamentosEquipamentosCancelados').next().addClass('d-none');
    });
    
    $.get('/indicadores/vendas/valorOrcamentos?unidade=EQUIPAMENTOS&statusOrcamento=CANCELADOS', function(chartData) {
    	if (graficoValorOrcamentosEquipamentosCancelados != null) {
    		graficoValorOrcamentosEquipamentosCancelados.data.datasets = chartData.data.datasets;
    	} else {
    		var ctx = document.getElementById('valorOrcamentosEquipamentosCancelados').getContext('2d');
    		graficoValorOrcamentosEquipamentosCancelados = new Chart(ctx, chartData);
    		graficoValorOrcamentosEquipamentosCancelados.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
    			return formatCurrencyAsMil(value);
    		}
    		graficoValorOrcamentosEquipamentosCancelados.options.legend.display = false;
    		graficoValorOrcamentosEquipamentosCancelados.options.tooltips.callbacks.label = function(label) {
    			return formatCurrency(label.yLabel);
    		}
    		graficoValorOrcamentosEquipamentosCancelados.options.scales.yAxes[0].ticks.beginAtZero = true;
    		graficoValorOrcamentosEquipamentosCancelados.options.onClick = function (evt, item) {
    			if (item[0] != null) {
    				var dia = item[0]['_model'].label;
    				var mes = new Date().getMonth() + 1;
    				var ano = new Date().getFullYear();
    				abrirLink(evt, '/relacao-orcamentos?dia=' + dia + '&mes=' + mes + '&ano=' + ano + '&unidade=EQUIPAMENTOS');
    			}
    		};
    	}
    	graficoValorOrcamentosEquipamentosCancelados.update();
    	$('#valorOrcamentosEquipamentosCancelados').next().addClass('d-none');
    });
}
atualizarGraficos();
//setInterval(atualizarGraficos, 60000);

$(document).ready(function() {
    for (var i = new Date().getFullYear(); i >= 2014; i--) {
        $('#anoRef').append('<option>' + i + '</option>');
    }
    
    $('#anoRef').change(function() {
        var ano = $('#anoRef').val();
        if (ano != 'Ano Referencia')
        	abrirLink(null, '/indicadores/vendas?ano=' + ano);
    });
});