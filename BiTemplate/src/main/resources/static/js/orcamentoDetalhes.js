$(document).ready(function() {
	$.ajax({
		type: 'POST',
		success: function(dataTable) {
			var columns = [];
			
			dataTable.headers.forEach(function(str, i) {
				if (i == 4) {
					columns.push({ title: str, class: str + ' nowrap' });
				} else {
					columns.push({ title: str, class: str });
				}
			});
			
			var table = $('#myTable').DataTable({
				columns: columns,
				data: dataTable.data,
				aaSorting: [ 
					[ 7, 'desc' ],
				],
		        buttons: [
		            'copyHtml5',
		            'excelHtml5',
		        ],
				dom: 'Blpftip',
		        columnDefs: [
		        {
		            render: function(data, type, row) {
		                if (type === 'display' || type === 'filter')
		                    return formatCurrency(data);
		                else
		                    return data;
		            },
		            targets: [6, 7],
		        	}
		        ],
			});
		},
	});
});