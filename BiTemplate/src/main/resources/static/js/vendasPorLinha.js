$(document).ready(function() {
	$('#tableVendasOrtopedia').DataTable({
		columns: [
			{ title: 'Linha' },
			{ title: 'Janeiro' },
			{ title: 'Fevereiro' },
			{ title: 'Março' },
			{ title: 'Abril' },
			{ title: 'Maio' },
			{ title: 'Junho' },
			{ title: 'Julho' },
			{ title: 'Agosto' },
			{ title: 'Setembro' },
			{ title: 'Outubro' },
			{ title: 'Novembro' },
			{ title: 'Dezembro' },
			{ title: 'Total' },
		],
		iDisplayLength: 25,
		ajax: {
			url: '/indicadores/vendas-por-linha/vendas/ORTOPEDIA',
			type: 'POST',
		},
        dom: 'Bflrptpi',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
		columnDefs: [
//		    {
//		        render: function(data, type, row) {
//		            if (type === 'display' || type === 'filter')
//		                return formatCurrency(data);
//		            else
//		                return data;
//		        },
//		        targets: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
//		    }
		],
	});
	
	
	$('#tableVendasOrtopediaClientes').DataTable({
		columns: [
			{ title: 'Grupo' },
			{ title: 'Cliente' },
			{ title: '2017' },
			{ title: '2018' },
			{ title: '2019' },
			{ title: '2020' },
			{ title: '2021' },
			{ title: 'Total' },
		],
		iDisplayLength: 25,
		ajax: {
			url: '/indicadores/vendas-por-linha/vendas/ORTOPEDIA/por-cliente',
			type: 'POST',
		},
        dom: 'Bflrptpi',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
		columnDefs: [
		],
	});
	
	
	
	$('#tableVendasEquipamentos').DataTable({
		columns: [
			{ title: 'Linha' },
			{ title: 'Janeiro' },
			{ title: 'Fevereiro' },
			{ title: 'Março' },
			{ title: 'Abril' },
			{ title: 'Maio' },
			{ title: 'Junho' },
			{ title: 'Julho' },
			{ title: 'Agosto' },
			{ title: 'Setembro' },
			{ title: 'Outubro' },
			{ title: 'Novembro' },
			{ title: 'Dezembro' },
		],
		iDisplayLength: 25,
		ajax: {
			url: '/indicadores/vendas-por-linha/vendas/EQUIPAMENTOS',
			type: 'POST',
		},
        dom: 'Bflrptpi',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
		columnDefs: [
//		    {
//		        render: function(data, type, row) {
//		            if (type === 'display' || type === 'filter')
//		                return formatCurrency(data);
//		            else
//		                return data;
//		        },
//		        targets: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
//		    }
		],
	});
	
	$.post('/indicadores/vendas-por-linha/metas/ortopedia', {unidade: 'ORTOPEDIA'}, function(data) {
		console.log(data);
	});
	
	$('.progress').each(function() {
		$(this).find('.progress-bar').width($(this).find('.value').html());
	});
	
	
	$.post('/indicadores/vendas-por-linha/cards/EQUIPAMENTOS', function(data) {
		$('#AC96').find('.valor').html(data.AC96);
		$('#AC100GP').find('.valor').html(data.AC100GP);
		$('#AC127').find('.valor').html(data.AC127);
		$('#AC254').find('.valor').html(data.AC254);
		$('#AC254GP').find('.valor').html(data.AC254GP);
		$('#AC365').find('.valor').html(data.AC365);
		$('#AC365GP').find('.valor').html(data.AC365GP);
		$('#AC523').find('.valor').html(data.AC523);
		$('#AC523GP').find('.valor').html(data.AC523GP);
		$('#AC600').find('.valor').html(data.AC600);
		$('#AC697').find('.valor').html(data.AC697);
		$('#AC697GP').find('.valor').html(data.AC697GP);
		$('#ALR').find('.valor').html(data.ALR);
		$('#TD290').find('.valor').html(data.TD290);
		$('#GS').find('.valor').html(data.GS);
		$('#FL').find('.valor').html(data.FL);
		$('#MC').find('.valor').html(data.MC);
		$('#LVU').find('.valor').html(data.LVU);
		
		
		console.log(data);
	});
});