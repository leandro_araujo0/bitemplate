$(document).ready( function () {
	$('#emissao').datepicker({
		dateFormat: 'yymmdd',
		changeMonth: true,
	    changeYear: true,
	    yearRange: '2014:'+(new Date).getFullYear(),
	});
	
    var table = $('#myTable').DataTable({
    	ajax: {
    		type: "POST",
    	},
    	columns: [
    		{ title: 'Pedido' },
    		{ title: 'Nome Fornecedor', class: 'nowrap' },
    		{ title: 'Emissao' },
    		{ title: 'Comprador' },
    		{ title: 'Num SC' },
    		{ title: 'Emissao SC' },
    		{ title: 'Solicit' },
    		{ title: 'OP' },
    		{ title: 'Tipo' },
    		{ title: 'Item Pedido' },
    		{ title: 'Produto', class: 'nowrap context-item codigoProduto' },
    		{ title: 'Descricao', class: 'nowrap' },
    		{ title: 'UM' },
    		{ title: 'Armazem' },
    		{ title: 'Qtd Ped' },
    		{ title: 'Qtd Ent' },
    		{ title: 'Qtd Pend' },
    		{ title: 'Preco Unit' },
    		{ title: 'Valor Total' },
    		{ title: 'Obs Ped'},
    		{ title: 'Hist. Compras'},
    	],
    	iDisplayLength: 10000,
    	dom: 'Blfrpti',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
        initComplete: function() {
        	$('.context-item').mousedown(function (e) {
        		e.preventDefault();
        		if (e.which == 3) {
        			var codigoProduto = this.innerHTML;
        			
        			var x = e.originalEvent.x;
        			var y = e.originalEvent.y;
        			
        			var opcoes = [];
        			
        			opcoes.push(
        				{
        					nome: 'Saldo por Endereco',
        					link: '/saldo-por-endereco?produtoDe=' + codigoProduto + '&produtoAte=' + codigoProduto + '&lote=&armazem=',
        				},
        				{
        					nome: 'Historico Produto',
        					link: '/historico-produto?codigoProduto=' + codigoProduto,
        				},
        				{
        					nome: 'Ordens Producao',
        					link: '/relacao-ops?numero=&sequencia=&lote=&codigoProduto=' + codigoProduto + '&descricaoProduto=&emissao=&armazem=&status=&usuario=&situacao=abertas',
        				},
        				{
        					nome: 'Kardex Produto',
        					link: '/kardex-produto?produtoDe=' + codigoProduto + '&produtoAte=' + codigoProduto + '&loteDe=&loteAte=&armazemDe=&armazemAte=&dataInicial=&dataFinal=',
        				},
        				{
        					nome: 'NFs Entrada',
        					link: 'http://192.168.0.141/relacao-entradas?produtoDe=' + codigoProduto + '&produtoAte=' + codigoProduto + '&numPedido=&nf=&codigoFornecedor=&nomeFornecedor=&emissaoDe=&emissaoAte=',
        				},
        				{
        					nome: 'Abrir Todos',
        					link: '#',
        					callback: "function() {" +
        						"var itens = [];" +
        						"$('.codigoProduto').each(function() {" +
        							"var codigoProduto = $(this).html();" +
        							"if (itens.includes(codigoProduto) || codigoProduto == 'Produto') {" +
        								"return" +
        							"}" +
        							"window.open('/historico-produto?codigoProduto=' + codigoProduto, '_blank');" +
        							"itens.push(codigoProduto);" +
        						"});" +
        					"}",
        				},
        			);
        			
        			criarContextMenu(x, y, opcoes);
        		}
        	});
        },
    });
});
