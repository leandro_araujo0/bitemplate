function adicionarManual(numero, item) {
	
	var tempNumero = numero;
	if (String(numero).length < 6) {
		for (i = 0; i < 6 - String(numero).length; i++) {
			tempNumero = '0' + tempNumero;
		}
	}
	var tempItem = item;
	if (String(item).length < 2) {
		for (i = 0; i < 2 - String(item).length; i++) {
			tempItem = '0' + item;
		}
	}
	
	var pedidoManual = {
			numero: tempNumero,
			item: tempItem,
	};
	
	$.ajax({
		url: '/carteira-pcp/adicionar-manual',
		data: pedidoManual,
		type: 'POST',
		success: function() {
			window.location.href = window.location.href;
		},
	});
}

function atualizaClipBoard() {
	$.post('/carteira-pcp/clipBoard', function(data) {
		if (data.itensClipBoard.length > 0) {
			$('#clipBoard').removeClass('d-none');
		} else {
			$('#clipBoard').addClass('d-none');
		}
		$(data.itensClipBoard).each(function() {
			$('#clipBoard .itens ul').append('<li>' + this + '</li>');
		});
	});
}

$(document).ready( function () {
	atualizaClipBoard();
	$(window).focus(atualizaClipBoard);
	
	$('#btn-gerar-empenhos').click(function() {
		var ops = "";
		
		$('#clipBoard .itens ul li').each(function() {
			if (ops.length > 0) {
				ops += " ";
			}
			ops += this.textContent;
		});
		
		window.location.href = '/relacao-empenhos?codigoProduto=&pvs=&op=' + ops + '&lote=&armazem=&emissao=&usuario=';
	});
	
	$('#btn-limpar-itens').click(function() {
		$.post('/carteira-pcp/limparClipBoard', function() {
			$('#clipBoard').addClass('d-none');
			$('#clipBoard .itens ul').html('');
		});
	});
	
	$('.op-div').bind('copy', function() {
		var item = this.innerText;
		$.post('/carteira-pcp/addClipBoardItem', {item: item}, function() {
			$('#clipBoard').removeClass('d-none');
			$('#clipBoard .itens ul').append('<li>' + item + '</li>');
		});
	});
	atualizaAcompanhamento();
	
	var timeout = null
	$('#numPedidoDest').on('keyup', function() {
	    var text = this.value
	    clearTimeout(timeout)
	    timeout = setTimeout(function() {
			if (text.length == 6) {
				$('#selectItemPed').html('');
				$.get('/carteira-pcp/get-itens-pedido?numeroPedido=' + text, (itensPedidoVenda) => {
					$(itensPedidoVenda).each(function () {
						var option = new Option(this.produto.codigo + ' - ' + this.produto.descricao, this.item);
						var select = document.getElementById('selectItemPed');
						select.add(option);
					});
				});
			} else if (text.length > 6) {
				$(this).val(text.substring(0, 6));
			}
	    }, 500)
	})
	
	$('#btnTransferirPedido').click(function() {
		$.post(
			'/carteira-pcp/transferir-pedido', 
			{ 
				numPedOri: $('#numPedido').val(),
				numItemPedOri: $('#numItemPedido').val(),
				numPedDest: $('#numPedidoDest').val(),
				numItemPedDest: $('#selectItemPed').val()
			},
			function(result) {
				if (result == true) {
					var timeOut = 5000;
					$('#div-resultado-text').html('Pedido Transferido com Sucesso! Atualizando Página em ' + timeOut / 1000 + ' segundos');
					$('#div-resultado').removeClass('d-none');
					$('#div-resultado').fadeOut(timeOut);
					setTimeout(location.reload.bind(location), timeOut);
				}
			}
		);
	});
	
	
	$('#btnTrasnferirPedido').click(() => {
		$('#formTransferirProduto').toggleClass('d-none');
	});
	
	$('.op-div, .sc-div, .memo-div').blur(function() {
		atualizaAcompanhamento();
		salvaAcompanhamento();
	});
	
	$('.sc-div, .op-div, #prioridade').keypress(function(e) {
		if (e.keyCode === 13) {
			atualizaAcompanhamento();
			salvaAcompanhamento();
			$('#editOpsButton').click();
			e.preventDefault();
		}
	});
	
	$('#prevEntrega').keypress(function(e) {
		if (e.keyCode === 13) {
			$('#editPrevEntregaButton').click();
			e.preventDefault();
		}
	});
	
	async function atualizaIcone(button) {
		$(button).attr('src', ($(button).attr('src').includes('editButton.jpg')) ? '/img/saveButton.jpg?t=' + new Date().getTime() : '/img/editButton.jpg?t=' + new Date().getTime());
	}
	
	$('#editOpsButton').click(function() {
		atualizaIcone(this);
		
		$('.sc-div, .op-div, .memo-div').each(function() {
			if ($(this).attr('contentEditable') === 'true') {
				$(this).attr('contentEditable', 'false');
			} else {
				$(this).attr('contentEditable', 'true');
			}
			
			if ($(this).attr('contentEditable') === 'true') {
				$(this).removeClass('blocked');
			} else {
				$(this).addClass('blocked');
				atualizaAcompanhamento();
				salvaAcompanhamento();
			}
		});

	});
	
	$('#editPrevEntregaButton').click(function() {
		$('#prevEntrega').attr('contentEditable', $('#prevEntrega').attr('contentEditable') === 'true' ? 'false' : 'true');
		if ($('#prevEntrega').attr('contentEditable') === 'true') {
			$('#prevEntrega').removeClass('blocked');
			$('#prevEntrega').focus();
		} else {
			$('#prevEntrega').addClass('blocked');
			atualizaAcompanhamento();
			salvaAcompanhamento();
		}
	});
	
	$('#editMemoButton').click(function() {
		$('#memorando').attr('contentEditable', $('#memorando').attr('contentEditable') === 'true' ? 'false' : 'true');
		if ($('#memorando').attr('contentEditable') === 'true') {
			$('memorando').removeClass('blocked');
			$('#memorando').focus();
		} else {
			$('memorando').addClass('blocked');
			atualizaAcompanhamento();
			salvaAcompanhamento();
		}
	});
	
	$('#processo, #mesProducao').change(function() {
		salvaAcompanhamento();
	});
	
	$('#tipoProduto').change(function() {
		var ap = {
			id: $('#id').val(),
			tipoProduto: $('#tipoProduto').val(),
		};
		$.ajax ({
			url: '/carteira-pcp/alteraTipoProduto',
			type: 'POST',
			data: ap,
			success: function() {
				location.reload();
			},
		});
	});
	
//	setTimeout(getFaltaPedidos, 5000)
//	function getFaltaPedidos() {
//		$('#tabelaFaltas').DataTable({
//			ajax: {
//				type: 'POST',
//			},
//			iDisplayLength: 10000,
//			dom: 'Blfrpti',
//	        buttons: [
//	            'copyHtml5',
//	            'excelHtml5',
//	        ],
//			columns: [
//				{ title: 'OP', class: '' },
//				{ title: 'Produto Op', class: 'nowrap' },
//				{ title: 'Desc Produto', class: '' },
//				{ title: 'Cod Empenho', class: 'context-item' },
//				{ title: 'Desc Empenho', class: 'nowrap' },
//				{ title: 'Qtd Emp', class: '' },
//				{ title: 'Qtd Estoque', class: '' },
//				{ title: 'Qtd Enderecar', class: '' },
//				{ title: 'Qtd Inspecao', class: '' },
//				{ title: 'Qtd Tot Emp', class: '' },
//				{ title: 'Qtd Op', class: '' },
//				{ title: 'Qtd SC', class: '' },
//				{ title: 'Qtd PC', class: '' },
//			],
//			initComplete: function() {
//	        	$('.context-item:not(th)').mousedown(function (e) {
//	        		if (e.which == 3) {
//	        			e.preventDefault();
//	        			var codigoProduto = this.innerHTML;
//	        			
//	        			var x = e.originalEvent.x;
//	        			var y = e.originalEvent.y;
//	        			
//	        			var opcoes = [];
//	        			
//	        			opcoes.push(
//	        				{
//	        					nome: 'Saldo por Endereco',
//	        					link: '/saldo-por-endereco?produtoDe=' + codigoProduto + '&produtoAte=' + codigoProduto + '&lote=&armazem=',
//	        				},
//	        				{
//	        					nome: 'Historico Produto',
//	        					link: '/historico-produto?codigoProduto=' + codigoProduto,
//	        				},
//	        				{
//	        					nome: 'Ordens Producao',
//	        					link: '/relacao-ops?numero=&sequencia=&lote=&codigoProduto=' + codigoProduto + '&descricaoProduto=&emissao=&armazem=&status=&usuario=&situacao=abertas',
//	        				},
//	        				{
//	        					nome: 'Kardex Produto',
//	        					link: '/kardex-produto?produtoDe=' + codigoProduto + '&produtoAte=' + codigoProduto + '&loteDe=&loteAte=&armazemDe=&armazemAte=&dataInicial=&dataFinal=',
//	        				},
//	        				{
//	        					nome: 'NFs Entrada',
//	        					link: 'http://192.168.0.141/relacao-entradas?produtoDe=' + codigoProduto + '&produtoAte=' + codigoProduto + '&numPedido=&nf=&codigoFornecedor=&nomeFornecedor=&emissaoDe=&emissaoAte=',
//	        				},
//	        			);
//	        			
//	        			criarContextMenu(x, y, opcoes);
//	        		}
//	        	});
//			},
//		});
//	}
	
	$('#memorando').click(function() {
		$(this).attr('contentEditable', 'true');
		$(this).addClass('fullScreen');
		$(this).focus();
		
		$(this).keydown(function(e) {
			if (e.keyCode == '27') {
				$(this).removeClass('fullScreen');
				$(this).unbind('keydown');
				$(this).attr('contentEditable', 'false');
				salvaAcompanhamento();
			}
		});
	});
});

async function salvaAcompanhamento() {
	var acompanhamentoPedido = {
		id: $('#id').val(),
		numPedido: $('#numPedido').val(),
		itemPed: $('#numItemPedido').val(),
		ops: $('#ops').text(),
		mesProducao: $('#mesProducao').val(),
		processo: $('#processo').val(),
		memorando: $('#memorando').html(),
		obs: $('#obs').text(),
		prevEntrega: $('#prevEntrega').text(),
		chassi: $('#chassi').text(),
		camara: $('#camara').text(),
		estrutura: $('#estrutura').text(),
		gerador: $('#gerador').text(),
		caldInt: $('#caldInt').text(),
		sistElev: $('#sistElev').text(),
		porta: $('#porta').text(),
		hidraulica: $('#hidraulica').text(),
		opPneumatica: $('#opPneumatica').text(),
		painelEletrico: $('#painelEletrico').text(),
		eletrica: $('#eletrica').text(),
		revestimento: $('#revestimento').text(),
		embalagem: $('#embalagem').text(),
		montagem: $('#montagem').text(),
		opRevestimento: $('#opRevestimento').text(),
		opEmbalagem: $('#opEmbalagem').text(),
		opAjusteRevestimento: $('#opAjusteRevestimento').text(),
		prioridade: $('#prioridade').text(),
		base: $('#base').text(),
		estruturaBase: $('#estruturaBase').text(),
		cjElevacao: $('#cjElevacao').text(),
		suporteLongitudinal: $('#suporteLongitudinal').text(),
		dorsoMovimento: $('#dorsoMovimento').text(),
		leitoMovimento: $('#leitoMovimento').text(),
		assentoMovimento: $('#assentoMovimento').text(),
		dorsoAtroscopia: $('#dorsoAtroscopia').text(),
		tracaoTibia: $('#tracaoTibia').text(),
		membrosInferiores: $('#membrosInferiores').text(),
		perneiraEsquerda: $('#perneiraEsquerda').text(),
		perneiraDireita: $('#perneiraDireita').text(),
		apoioPelvico: $('#apoioPelvico').text(),
		cabeceira: $('#cabeceira').text(),
		complementoDorso: $('#complementoDorso').text(),
		opPintura: $('#opPintura').text(),
		
		tipoProduto: $('#tipoProduto').val(),
	}
	$.ajax ({
		url: '/carteira-pcp/acompanhamentoPedido',
		type: 'POST',
		data: acompanhamentoPedido,
	});
}

async function atualizaAcompanhamento() {
	$('.op-div, .sc-div').each(function() {
		var texts = $(this).text().split(';');
		if (texts == '' || texts == '<br>') {
			return;
		}
		var text = '';
		for (i = 0; i < texts.length; i++) {
			if (i === 0) {
				if ($(this).hasClass('op-div')) {
					text += '<a href="/ordens-producao/' + texts[i] + '/detalhes">' + texts[i] + "</a>";
				} else {
					text += '<a href="/relacao-scs?numero=' + texts[i] + '&abertos=false">' + texts[i] + '</a>';
				}
			} else {
				if ($(this).hasClass('op-div')) {
					text += ';' + '<a href="/ordens-producao/' + texts[i] + '/detalhes">' + texts[i] + "</a>";
				} else {
					text += ';' + '<a href="/relacao-scs?numero=texts[i]' + + '&abertos=false' + '">' + texts[i] + "</a>";
				}
			}
		}
		$(this).html(text);
	});
	
	
	
	$('.op-div').each(function() {
		var op = $(this).text();
		var inputBox = this;
		if (op === '') {
			return;
		}
		$(inputBox).removeClass('andamento');
		$(inputBox).removeClass('aberta');
		$(inputBox).removeClass('finalizada');
		$.post('/status/op', {op: op}, function(data) {
			if (data === 'FINALIZADA') {
				$(inputBox).addClass('finalizada');
			}
			if (data === 'ANDAMENTO') {
				$(inputBox).addClass('andamento');
			}
			if (data === 'ABERTA') {
				$(inputBox).addClass('aberta');
			}
		});
	});
	
	
	$('.sc-div').each(function() {
		var sc = $(this).text();
		var inputBox = this;
		if (sc === '') {
			return;
		}
		$(inputBox).removeClass('aberta');
		$(inputBox).removeClass('andamento');
		$(inputBox).removeClass('finalizada');
		$.post('/status/sc', {sc: sc}, function(data) {
			if (data === 'PCENTREGUE') {
				$(inputBox).addClass('finalizada');
			}
			if (data === 'PCPENDENTE') {
				$(inputBox).addClass('andamento');
			}
			if (data === 'SCPENDENTE') {
				$(inputBox).addClass('aberta');
			}
		});
	});
}