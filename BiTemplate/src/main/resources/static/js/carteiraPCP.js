var table;

$(document).ready(function() {
	$('#btnMostrarValores').click(function() {
		$('.d-none').removeClass('d-none');
	});
	
	table = $('#myTable').DataTable({
		'createdRow': function( row, data, dataIndex) {
			var size = $(row).find('td').length;
			for (i = 0; i < size; i++) {
				if ((data[i]) === null) {
					continue;
				}
				$(row).find('td:nth-child(' + (parseFloat(i) + parseFloat(1)) + ')').attr('title', data[i]);
			}
		},
    	iDisplayLength: 10000,
        dom: 'Blpftp',
        buttons: [
        	{
                extend: 'copy',
                exportOptions: {
                	columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32 ],
                }
            },
            {
                extend: 'excel',
                exportOptions: {
                	columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32 ],
                	orthogonal: 'export',
                },
            }
        ],
		ajax: {
			type: 'POST',
		},
    	aaSorting: [[12, 'asc']],
		columns: [
			{ title: 'Pr.', class: 'prioridade' },
			{ title: 'Num Pedido' },
			{ title: 'Item Pedido' },
			{ title: 'Cliente' },
			{ title: 'Produto', class: 'nowrap context-item' },
			{ title: 'Descricao', class: '' },
			{ title: 'Grupo', class: '' },
			{ title: 'Mes Producao', class: '' },
			{ title: 'PFI' },
			{ title: 'Processo', class: 'nowrap' },
			{ title: 'Desc Opc' },
			{ title: 'Memo' },
			{ title: 'Qtd' },
			{ title: 'Qtd Ent' },
			{ title: 'Dt Liberacao' },
			{ title: 'Dt Comercial' },
			{ title: 'OP Chassi' },
			{ title: 'OP Camara' },
			{ title: 'OP Estrutura' },
			{ title: 'OP Gerador' },
			{ title: 'OP Mont Porta' },
			{ title: 'OP Sist Elev' },
			{ title: 'OP Porta' },
			{ title: 'OP Hidraulica' },
			{ title: 'OP Pneumatica' },
			{ title: 'OP Eletrica' },
			{ title: 'OP Painel Eletrico' },
			{ title: 'OP Revestimento' },
			{ title: 'OP Embalagem' },
			{ title: 'OP Ajuste Revestimento' },
			{ title: 'SC Revestimento' },
			{ title: 'SC Embalagem' },
			{ title: 'Liberado', class: 'liberado' },
		],
        columnDefs: [
            {
                render: function(data, type, row) {
                    if (type === 'display') {
                    	if (data != null && data.length > 30 && !data.includes('</a>')) {
                    		return data.substring(0, 30) + '...';
                    	} else {
                    		return data;
                    	}
                    } else {
                        return data;
                    }
                },
                targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32],
            }
        ],
        initComplete: function() {
        	$('tr:not(":first")').each(function() {
        		var liberado = $(this).find('.liberado').html();
        		if (liberado != 'S') {
        			$(this).addClass('bloqueado');
        		}
        	});
        	
        	$(table).find('input').focus();
        	
			$('.context-item').mousedown(function (e) {
    			if (e.which == 3) {
    				var codigoProduto = this.innerHTML;
    				
    				var x = e.originalEvent.x;
    				var y = e.originalEvent.y;
    				
    				var opcoes = [];
    				
    				opcoes.push(
    					{
	    					nome: 'Saldo por Endereco',
	    					link: '/saldo-por-endereco?codigoProduto=' + codigoProduto + '&lote=&armazem=',
    					},
    					{
    						nome: 'Historico Produto',
	    					link: '/historico-produto?codigoProduto=' + codigoProduto,
    					},
    					{
    						nome: 'Ordens Producao',
	    					link: '/relacao-ops?numero=&sequencia=&lote=&codigoProduto=' + codigoProduto + '&descricaoProduto=&emissao=&armazem=&status=&usuario=&situacao=abertas',
    					},
    				);
    				
    				criarContextMenu(x, y, opcoes);
    			}
    		});
        },
	});
});