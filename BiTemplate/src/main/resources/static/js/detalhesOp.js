$(document).ready(function() {
	$('.context-item').mousedown(function (e) {
		if (e.which == 3) {
			e.preventDefault();
			var lote = this.innerHTML;
			
			var x = e.originalEvent.x;
			var y = e.originalEvent.y;
			
			var opcoes = [];
			
			opcoes.push(
				{
					nome: 'Kardex Por Lote',
					link: '/kardex-produto?produtoDe=&produtoAte=&loteDe=' + lote + '&loteAte=' + lote + '&armazemDe=&armazemAte=&dataInicial=&dataFinal=',
				},
				{
					nome: 'Saldo por Endereço',
					link: 'http://192.168.0.5/saldo-por-endereco?produtoDe=&produtoAte=&descricao=&loteDe=' + lote + '&loteAte=' + lote + '&armazemDe=&armazemAte=',
				},
			);
			
			criarContextMenu(x, y, opcoes);
		}
	});
});