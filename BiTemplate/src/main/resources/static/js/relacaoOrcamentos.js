$(document).ready(function() {
	var table = $('#myTable').DataTable({
		ajax: {
			type: 'POST',
		},
		iDisplayLength: 10000,
		columns: [
			//CJ_NUM, CJ_EMISSAO, A1_NOME, CK_PRODUTO, B1_DESC, CK_QTDVEN, CK_PRCVEN, CK_VALOR
			{ title: 'Num Orc' },
			{ title: 'Emissao' },
			{ title: 'Cliente' },
			{ title: 'Produto' },
			{ title: 'Descricao' },
			{ title: 'Qtd Orc' },
			{ title: 'Prc Venda' },
			{ title: 'Valor Total' },
		],
		dom: 'Blfrpti',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
	});
});