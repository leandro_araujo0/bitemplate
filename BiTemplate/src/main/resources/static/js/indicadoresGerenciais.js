$(document).ready(function() {
    atualizarGraficos();
    setInterval(atualizarGraficos, 60000);

    for (var i = new Date().getFullYear(); i >= 2014; i--) {
        $('#anoRef').append('<option>' + i + '</option>');
    }
    
    $('#anoRef').change(function() {
        var ano = $('#anoRef').val();
        if (ano != 'Ano Rerefencia')
        	abrirLink('/indicadores/gerencial?ano=' + ano);
    });
});

function atualizarGraficos() {
    $('canvas').after('<div class="spinner"></div>');
    $.get('/indicadores/gerencial/graficoGerencialMensal', 'unidade=ORTOPEDIA', function(chartData) {
        var ctx = document.getElementById('graficoGerencialOrtopediaMensal').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
            return formatCurrency(value);
        }
        chart.options.tooltips.callbacks.label = function(label) {
            return formatCurrency(label.yLabel);
        }
        chart.options.scales.yAxes[0].ticks.beginAtZero = true;
        chart.update();
        $('#graficoGerencialOrtopediaMensal').next().toggleClass('d-none');
    });
    
    $.get('/indicadores/gerencial/graficoGerencialMensal', 'unidade=EQUIPAMENTOS', function(chartData) {
        var ctx = document.getElementById('graficoGerencialEquipamentosMensal').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
            return formatCurrency(value);
        }
        chart.options.tooltips.callbacks.label = function(label) {
            return formatCurrency(label.yLabel);
        }
        chart.options.scales.yAxes[0].ticks.beginAtZero = true;
        chart.update();
        $('#graficoGerencialEquipamentosMensal').next().toggleClass('d-none');
    });
    
    
    
    $.get('/indicadores/gerencial/graficoGerencialAnual', 'unidade=ORTOPEDIA', function(chartData) {
        var ctx = document.getElementById('graficoGerencialOrtopediaAnual').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.scales.xAxes[0].ticks.callback = function(value, index, values) {
            return formatCurrency(value);
        }
        chart.options.tooltips.callbacks.label = function(label) {
            return formatCurrency(label.xLabel);
        }
        chart.options.scales.xAxes[0].ticks.beginAtZero = true;
        chart.options.legend.display = false;
        chart.options.scales.xAxes[0].ticks.suggestedMax = 60000000;
        chart.update();
        $('#graficoGerencialOrtopediaAnual').next().toggleClass('d-none');
    });
    
    
    
    $.get('/indicadores/gerencial/graficoGerencialAnual', 'unidade=EQUIPAMENTOS', function(chartData) {
        var ctx = document.getElementById('graficoGerencialEquipamentosAnual').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.scales.xAxes[0].ticks.callback = function(value, index, values) {
            return formatCurrency(value);
        }
        chart.options.tooltips.callbacks.label = function(label) {
            return formatCurrency(label.xLabel);
        }
        chart.options.scales.yAxes[0].ticks.beginAtZero = true;
        chart.options.legend.display = false;
        chart.options.scales.xAxes[0].ticks.suggestedMax = 60000000;
        chart.update();
        $('#graficoGerencialEquipamentosAnual').next().toggleClass('d-none');
    });
    
    
}