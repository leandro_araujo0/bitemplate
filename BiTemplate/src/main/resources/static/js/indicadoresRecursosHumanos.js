var graficoEscolaridadeOrtopedia;
var graficoEscolaridadeMensalOrtopedia;
var graficoEscolaridadeEquipamentos;
var graficoEscolaridadeMensalEquipamentos;
var graficoRotatividadeOrtopedia;
var graficoRotatividadeEquipamentos;

$(document).ready(function() {
	$('canvas').after('<div class="spinner"></div>');
	
	var url = '/indicadores/recursos-humanos/graficoEscolaridade?unidade=ORTOPEDIA';
	$.get(url, function(chartData) {
		var tempGrafico = graficoEscolaridadeOrtopedia;
        if (tempGrafico != null) {
        	tempGrafico.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('graficoEscolaridadeOrtopedia').getContext('2d');
            tempGrafico = new Chart(ctx, chartData);
            tempGrafico.options.scales.yAxes[0].ticks.beginAtZero = true;
        }
        tempGrafico.options.onClick = function (evt, item) {
            if (item[0] != null) {
                var obj = item[0]['_model'].label.split(';')[0];
                abrirLink(evt, '/recursos-humanos/escolaridade/analitico?unidade=ORTOPEDIA' + '&grauEscolaridade=' + obj);
            }
        };
        tempGrafico.options.legend.display = false;
        tempGrafico.update();
        $('#graficoEscolaridadeOrtopedia').next().addClass('d-none');
    });
	
	
	url = '/indicadores/recursos-humanos/graficoEscolaridade?unidade=EQUIPAMENTOS';
	$.get(url, function(chartData) {
		var tempGrafico = graficoEscolaridadeEquipamentos;
        if (tempGrafico != null) {
        	tempGrafico.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('graficoEscolaridadeEquipamentos').getContext('2d');
            tempGrafico = new Chart(ctx, chartData);
            tempGrafico.options.scales.yAxes[0].ticks.beginAtZero = true;
        }
        tempGrafico.options.onClick = function (evt, item) {
            if (item[0] != null) {
                var obj = item[0]['_model'].label.split(';')[0];
                abrirLink(evt, '/recursos-humanos/escolaridade/analitico?unidade=EQUIPAMENTOS' + '&grauEscolaridade=' + obj);
            }
        };
        tempGrafico.options.legend.display = false;
        tempGrafico.update();
        $('#graficoEscolaridadeEquipamentos').next().addClass('d-none');
    });
	
	
	url = '/indicadores/recursos-humanos/graficoEscolaridadeMensal?unidade=ORTOPEDIA';
	$.get(url, function(chartData) {
		var tempGrafico = graficoEscolaridadeMensalOrtopedia;
        if (tempGrafico != null) {
        	tempGrafico.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('graficoEscolaridadeMensalOrtopedia').getContext('2d');
            tempGrafico = new Chart(ctx, {
            	data: chartData.data,
            	type: 'bar',
            	title: {
					display: true,
					text: 'Escolaridade Mensal Ortopedia'
				},
            	options: {
            		scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					},
            	},
            });
        }
        tempGrafico.options.legend.position = 'right';
        tempGrafico.options.legend.display = true;
        tempGrafico.options.legend.align = 'center';
        tempGrafico.update();
        $('#graficoEscolaridadeMensalOrtopedia').next().addClass('d-none');
    });
	
	url = '/indicadores/recursos-humanos/graficoEscolaridadeMensal?unidade=EQUIPAMENTOS';
	$.get(url, function(chartData) {
		var tempGrafico = graficoEscolaridadeMensalEquipamentos;
        if (tempGrafico != null) {
        	tempGrafico.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('graficoEscolaridadeMensalEquipamentos').getContext('2d');
            tempGrafico = new Chart(ctx, {
            	data: chartData.data,
            	type: 'bar',
            	title: {
					display: true,
					text: 'Escolaridade Mensal Ortopedia'
				},
            	options: {
            		scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					},
            	},
            });
        }
        tempGrafico.options.legend.position = 'right';
        tempGrafico.options.legend.display = true;
        tempGrafico.options.legend.align = 'center';
        tempGrafico.update();
        $('#graficoEscolaridadeMensalEquipamentos').next().addClass('d-none');
    });
	
	
	
	$.post('/indicadores/recursos-humanos/tabelaRotatividade?unidade=ORTOPEDIA', function(dataTable) {
		var headers = [];
		dataTable.headers.forEach(function(titulo) {
			headers.push({ title: titulo, class: 'text-center', });
		});
		
		var tabelaRotatividade = $('#tabelaRotatividadeOrtopedia').DataTable({
			columns: headers,
			data: dataTable.data,
	    	iDisplayLength: 10000,
	    	dom: 't',
	        columnDefs: [
		        {
		            render: function(data, type, row) {
		                if (type === 'display' || type === 'filter')
		                    return data + '%';
		                else
		                    return data;
		            },
		            targets: [4],
	        	}
	        ],
	        initComplete: function() {
	        	$(this).find('tr').each(function(a, b, c) {
	        		if (a == 0) return;
	        		
	        		$($(this).find('td')[1]).click(function() {
	        			var periodo = $(this).parent().find('td')[0].innerHTML;
	        			window.location = '/indicadores/recursos-humanos/analitico?unidade=ORTOPEDIA&tipo=ADMISSAO&periodo=' + periodo;
	        		});
	        		$($(this).find('td')[2]).click(function() {
	        			var periodo = $(this).parent().find('td')[0].innerHTML;
	        			window.location = '/indicadores/recursos-humanos/analitico?unidade=ORTOPEDIA&tipo=DEMISSAO&periodo=' + periodo;
	        		});
	        	});
	        },
		});
		
		var chartData = {
			datasets: [],
			labels: [],
		};
		
		var dataset = {
			data: [],
			borderColor: '#0089eb',
			fill: false,
		};
		
		dataTable.data.forEach(function(val) {
			chartData.labels.push(val[0]);
			dataset.data.push(val[4]);
		})
		chartData.datasets.push(dataset);
		
		
		var tempGrafico = graficoRotatividadeOrtopedia;
        if (tempGrafico != null) {
        	tempGrafico.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('graficoRotatividadeOrtopedia').getContext('2d');
            tempGrafico = new Chart(ctx, {
            	data: chartData,
            	type: 'line',
            	options: {
            	    elements: {
            	        line: {
            	            tension: 0
            	        }
            	    },
            	    scales: {
            	        xAxes: [{
            	            gridLines: {
            	                drawOnChartArea: false
            	            }
            	        }],
            	        yAxes: [{
            	            gridLines: {
            	                drawOnChartArea: false
            	            }
            	        }]
            	    },
            	},
            });
        }
        tempGrafico.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
            return value + '%';
        }
        tempGrafico.options.tooltips.callbacks.label = function(label) {
            return label.yLabel + '%';
        }
        tempGrafico.options.legend.display = false;
        tempGrafico.update();
        $('#graficoRotatividadeOrtopedia').next().addClass('d-none');
	});
	
	$.post('/indicadores/recursos-humanos/tabelaRotatividade?unidade=EQUIPAMENTOS', function(dataTable) {
		var headers = [];
		dataTable.headers.forEach(function(titulo) {
			headers.push({ title: titulo, class: 'text-center' });
		});
		
		var tabelaRotatividade = $('#tabelaRotatividadeEquipamentos').DataTable({
			columns: headers,
			data: dataTable.data,
	    	iDisplayLength: 10000,
	    	dom: 't',
	        columnDefs: [
		        {
		            render: function(data, type, row) {
		                if (type === 'display' || type === 'filter')
		                    return data + '%';
		                else
		                    return data;
		            },
		            targets: [4],
	        	}
	        ],
	        initComplete: function() {
	        	$(this).find('tr').each(function(a, b, c) {
	        		if (a == 0) return;
	        		
	        		$($(this).find('td')[1]).click(function() {
	        			var periodo = $(this).parent().find('td')[0].innerHTML;
	        			window.location = '/indicadores/recursos-humanos/analitico?unidade=EQUIPAMENTOS&tipo=ADMISSAO&periodo=' + periodo;
	        		});
	        		$($(this).find('td')[2]).click(function() {
	        			var periodo = $(this).parent().find('td')[0].innerHTML;
	        			window.location = '/indicadores/recursos-humanos/analitico?unidade=EQUIPAMENTOS&tipo=DEMISSAO&periodo=' + periodo;
	        		});
	        	});
	        },
		});
		
		var chartData = {
				datasets: [],
				labels: [],
			};
			
			var dataset = {
				data: [],
				borderColor: '#0089eb',
				fill: false,
			};
			
			dataTable.data.forEach(function(val) {
				chartData.labels.push(val[0]);
				dataset.data.push(val[4]);
			})
			chartData.datasets.push(dataset);
			
			
			var tempGrafico = graficoRotatividadeEquipamentos;
	        if (tempGrafico != null) {
	        	tempGrafico.data.datasets = chartData.data.datasets;
	        } else {
	            var ctx = document.getElementById('graficoRotatividadeEquipamentos').getContext('2d');
	            tempGrafico = new Chart(ctx, {
	            	data: chartData,
	            	type: 'line',
	            	options: {
	            	    elements: {
	            	        line: {
	            	            tension: 0
	            	        }
	            	    },
	            	    scales: {
	            	        xAxes: [{
	            	            gridLines: {
	            	                drawOnChartArea: false
	            	            }
	            	        }],
	            	        yAxes: [{
	            	            gridLines: {
	            	                drawOnChartArea: false
	            	            }
	            	        }]
	            	    },
	            	},
	            });
	        }
	        tempGrafico.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
	            return value + '%';
	        }
	        tempGrafico.options.tooltips.callbacks.label = function(label) {
	            return label.yLabel + '%';
	        }
	        tempGrafico.options.legend.display = false;
	        tempGrafico.update();
	        $('#graficoRotatividadeEquipamentos').next().addClass('d-none');
	});
});