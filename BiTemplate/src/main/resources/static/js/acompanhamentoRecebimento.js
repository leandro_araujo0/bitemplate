$(document).ready(function() {
	$.post('/acompanhamento-recebimento/titulos-em-aberto', function(dataTable) {
		var headers = [];
		dataTable.headers.forEach(function(titulo) {
			headers.push({ title: titulo, class: 'nowrap' });
		});
		var currencyCells = [];
		for (i = 0; i < headers.length; i++) {
			if (i > 2) {
				currencyCells.push(i);
			}
		}
		
		
		$('#tabelaRecebimento').DataTable({
	        dom: 'Blpftip',
	        buttons: [
	            'copyHtml5',
	            'excelHtml5',
	        ],
			iDisplayLength: 100,
			columns: headers,
			data: dataTable.data,
			aaSorting: [ 
				[ headers.length - 1, 'desc' ],
			],
	        columnDefs: [
	        {
	            render: function(data, type, row) {
	                if (type === 'display' || type === 'filter')
	                    return formatCurrency(data);
	                else
	                    return data;
	            },
	            targets: currencyCells,
	        	}
	        ],
		});
	});
});