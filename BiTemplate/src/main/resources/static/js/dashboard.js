$(document).ready(function () {
	$.post('/indicadores/dashboard/grafico-dashboard-vendas', function(chartData) {
		var ctx = document.getElementById('grafico-dashboard-vendas').getContext('2d');
        graficoDashboardVendas = new Chart(ctx, chartData);
        graficoDashboardVendas.options.legend.display = false;
        graficoDashboardVendas.options.scales.yAxes[0].ticks.beginAtZero = true;
        graficoDashboardVendas.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
            return formatCurrency(value);
        }
        graficoDashboardVendas.options.tooltips.callbacks.label = function(label) {
            return formatCurrency(label.yLabel);
        }
        graficoDashboardVendas.update();
	});
});