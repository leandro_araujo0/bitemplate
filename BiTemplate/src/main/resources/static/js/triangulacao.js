$(document).ready(function() {
	var table = $('#myTable').DataTable({
		columns: [
			{ title: 'id' },
			{ title: 'Qtd' },
			{ title: 'SC Chapa' },
			{ title: 'PC Chapa' },
			{ title: 'OP Polim' },
			{ title: 'SC Polim' },
			{ title: 'PC Polim' },
			{ title: 'OP Corte/Dobra' },
			{ title: 'SC Corte/Dobra' },
			{ title: 'PC Corte/Dobra' },
			{ title: 'NF Entrada' },
		],
		initComplete: function() {
			
		},
	});
	
	$('.context-item').mousedown(function (e) {
		if (e.which == 3) {
			var id = this.innerHTML;
			
			var x = e.originalEvent.x;
			var y = e.originalEvent.y;
			
			var opcoes = [];
			
			opcoes.push(
				{
					nome: 'Alterar',
					link: '/triangulacao/' + id + '/form',
				},
				{
					nome: 'Remover',
					link: '/triangulacao/' + id + '/remover',
				},
			);
			
			criarContextMenu(x, y, opcoes);
		}
	});
});