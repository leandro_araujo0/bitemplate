$(document).ready(function () {
    for (var i = new Date().getFullYear(); i >= 2014; i--) {
        $('#anoRef').append('<option>' + i + '</option>');
    }

    $('#anoRef').change(function () {
        var ano = $('#anoRef').val();
        if (ano != 'Ano Rerefencia')
        	abrirLink('/indicadores/faturamento?ano=' + ano);
    });

    $('canvas').after('<div class="spinner"></div>');

    $.get('/indicadores/faturamento/graficoFaturamentoOrtopedia', function (chartData) {
        var ctx = document.getElementById('graficoFaturamentoOrtopedia').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.scales.yAxes[0].ticks.callback = function (value, index, values) {
            return formatCurrency(value);
        }
        chart.options.tooltips.callbacks.label = function (label) {
            return formatCurrency(label.yLabel);
        }
        chart.options.scales.yAxes[0].ticks.beginAtZero = true;
        chart.options.onClick = function (evt, item) {
            var ano = chart.getElementAtEvent(evt)[0]._view.datasetLabel.replace('Faturamento ', '');
            var mes = chart.getElementAtEvent(evt)[0]._view.label;
            abrirLink(evt, '/faturamento/pedidos?unidade=ORTOPEDIA' + '&mes=' + mes + '&ano=' + ano);
        };
        chart.update();
        $('#graficoFaturamentoOrtopedia').next().toggleClass('d-none');
    });

    $.get('/indicadores/faturamento/graficoFaturamentoEquipamentos', function (chartData) {
        var ctx = document.getElementById('graficoFaturamentoEquipamentos').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.scales.yAxes[0].ticks.callback = function (value, index, values) {
            return formatCurrency(value);
        }
        chart.options.tooltips.callbacks.label = function (label) {
            return formatCurrency(label.yLabel);
        }
        chart.options.scales.yAxes[0].ticks.beginAtZero = true;
        chart.options.onClick = function (evt, item) {
            var ano = chart.getElementAtEvent(evt)[0]._view.datasetLabel.replace('Faturamento ', '');
            var mes = chart.getElementAtEvent(evt)[0]._view.label;
            abrirLink(evt, '/faturamento/pedidos?unidade=EQUIPAMENTOS' + '&mes=' + mes + '&ano=' + ano);
        };
        chart.update();
        $('#graficoFaturamentoEquipamentos').next().toggleClass('d-none');
    });

    $.get('/indicadores/faturamento/graficoFaturamentoHorizontal', function (chartData) {
        var ctx = document.getElementById('graficoFaturamentoHorizontal').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.scales.xAxes[0].ticks.beginAtZero = true;
        chart.options.scales.xAxes[0].ticks.callback = function (value, index, values) {
            return formatCurrency(value);
        }
        chart.options.tooltips.callbacks.label = function (label) {
            return formatCurrency(label.xLabel);
        }
        chart.options.scales.xAxes[0].ticks.suggestedMax = 60000000;
        chart.options.legend.display = false;
        chart.options.onClick = function (evt, item) {
            var ano = chart.getElementAtEvent(evt)[0]._model.datasetLabel;
            var unidade = chart.getElementAtEvent(evt)[0]._model.label;
            abrirLink(evt, '/faturamento/pedidos?unidade=' + unidade + '&ano=' + ano);
        };    
        chart.update();
        $('#graficoFaturamentoHorizontal').next().toggleClass('d-none');
    });

    $.get('/indicadores/faturamento/graficoFaturamentoMensal', function (chartData) {
        var ctx = document.getElementById('graficoFaturamentoMensal').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.legend.display = false;
        chart.options.scales.xAxes[0].ticks.callback = function (value, index, values) {
            return formatCurrency(value);
        }
        chart.options.tooltips.callbacks.label = function (label) {
            return formatCurrency(label.xLabel);
        }
        chart.options.scales.xAxes[0].ticks.suggestedMax = 4000000;
        chart.options.scales.yAxes[0].ticks.beginAtZero = true;
        chart.options.onClick = function (evt, item) {
            if (item[0] != null) {
                var obj = item[0]['_model'].label;
                abrirLink(evt, '/faturamento/pedidos?clasped=' + obj + '&mes=' + (new Date().getMonth() + 1) + '&ano=' + new Date().getFullYear());
            }
        };
        chart.update();
        $('#graficoFaturamentoMensal').next().toggleClass('d-none');
    });


    $.get('/indicadores/faturamento/graficoFaturamentoDetalhadoCaixas', function (chartData) {
        var ctx = document.getElementById('graficoFaturamentoDetalhadoCaixas').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.legend.display = false;
        chart.options.scales.yAxes[0].ticks.callback = function (value, index, values) {
            return formatCurrency(value);
        }
        chart.options.tooltips.callbacks.label = function (label) {
            return formatCurrency(label.yLabel);
        }
        chart.options.scales.yAxes[0].ticks.beginAtZero = true;
        chart.options.onClick = function (evt, item) {
            if (item[0] != null) {
                var obj = item[0]['_model'].label;
                abrirLink(evt, '/faturamento/pedidos?clasped=CAIXAS&dia=' + obj + '&mes=' + (new Date().getMonth() + 1) + '&ano=' + new Date().getFullYear());
            }
        };
        chart.update();
        $('#graficoFaturamentoDetalhadoCaixas').next().toggleClass('d-none');
    });

    $.get('/indicadores/faturamento/graficoFaturamentoDetalhadoReposicao', function (chartData) {
        var ctx = document.getElementById('graficoFaturamentoDetalhadoReposicao').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.legend.display = false;
        chart.options.scales.yAxes[0].ticks.callback = function (value, index, values) {
            return formatCurrency(value);
        }
        chart.options.tooltips.callbacks.label = function (label) {
            return formatCurrency(label.yLabel);
        }
        chart.options.scales.yAxes[0].ticks.beginAtZero = true;
        chart.options.onClick = function (evt, item) {
            if (item[0] != null) {
                var obj = item[0]['_model'].label;
                abrirLink(evt, '/faturamento/pedidos?clasped=REPOSICAO&dia=' + obj + '&mes=' + (new Date().getMonth() + 1) + '&ano=' + new Date().getFullYear());
            }
        };        
        chart.update();
        $('#graficoFaturamentoDetalhadoReposicao').next().toggleClass('d-none');
    });

    $.get('/indicadores/faturamento/graficoFaturamentoDetalhadoExportacao', function (chartData) {
        var ctx = document.getElementById('graficoFaturamentoDetalhadoExportacao').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.legend.display = false;
        chart.options.scales.yAxes[0].ticks.callback = function (value, index, values) {
            return formatCurrency(value);
        }
        chart.options.tooltips.callbacks.label = function (label) {
            return formatCurrency(label.yLabel);
        }
        chart.options.scales.yAxes[0].ticks.beginAtZero = true;
        chart.options.onClick = function (evt, item) {
            if (item[0] != null) {
                var obj = item[0]['_model'].label;
                abrirLink(evt, '/faturamento/pedidos?clasped=EXPORTACAO&dia=' + obj + '&mes=' + (new Date().getMonth() + 1) + '&ano=' + new Date().getFullYear());
            }
        };        
        chart.update();
        $('#graficoFaturamentoDetalhadoExportacao').next().toggleClass('d-none');
    });

    $.get('/indicadores/faturamento/graficoFaturamentoDetalhadoEquipamentos', function (chartData) {
        var ctx = document.getElementById('graficoFaturamentoDetalhadoEquipamentos').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.legend.display = false;
        chart.options.scales.yAxes[0].ticks.callback = function (value, index, values) {
            return formatCurrency(value);
        }
        chart.options.tooltips.callbacks.label = function (label) {
            return formatCurrency(label.yLabel);
        }
        chart.options.scales.xAxes[0].ticks.suggestedMax = 8000000;
        chart.options.scales.yAxes[0].ticks.beginAtZero = true;
        chart.options.onClick = function (evt, item) {
            if (item[0] != null) {
                var obj = item[0]['_model'].label;
                abrirLink(evt, '/faturamento/pedidos?clasped=EQUIPAMENTOS&dia=' + obj + '&mes=' + (new Date().getMonth() + 1) + '&ano=' + new Date().getFullYear());
            }
        };        
        chart.update();
        $('#graficoFaturamentoDetalhadoEquipamentos').next().toggleClass('d-none');
    });
});