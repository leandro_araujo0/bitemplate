$(document).ready( function () {
	var table = $('#myTable').dataTable({
    	ajax: {
    		type: "POST",
    	},
    	columns: [
    		{ title: 'Unidade' },
    		{ title: 'Colaborador'},
    		{ title: 'Data Admissão'},
    		{ title: 'Anos Empresa'},
    		{ title: 'Cargo'},
    		{ title: 'Escolaridade'},
    		{ title: 'UF'},
    		{ title: 'Email'},
    	],
    	iDisplayLength: 10000,
    	dom: 'Blfrpti',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
        initComplete: function() {
        },
        columnDefs: [
	        {
	            render: function(data, type, row) {
	                if (type === 'display' || type === 'filter') {
	                	var periodo = Number(data);
	                	var anos = Math.floor(periodo / 12);
	                	var meses = periodo % 12;
	                    return anos + "A " + meses + "M";
	                } else {
	                    return data;
	                }
	            },
	            targets: [3],
        	},
        ],
    });
});
