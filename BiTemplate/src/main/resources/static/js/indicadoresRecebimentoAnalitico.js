$(document).ready(function() {
	
	$.post('', function(dataTable) {
		var headers = [];
		
		dataTable.headers.forEach(function(titulo) {
			headers.push({ title: titulo, class: 'nowrap' });
		});
		
		var table = $(myTable).DataTable({
			data: dataTable.data,
			columns: headers,
			iDisplayLength: 1000,
	    	dom: 'Blfrpti',
	        buttons: [
	            'copyHtml5',
	            'excelHtml5',
	            'csv',
	        ],
		});	
	});
});