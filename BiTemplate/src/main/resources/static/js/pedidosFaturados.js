$(document).ready(function() {
    var table = $('#myTable').DataTable({
    	aaSorting: [
    		[ 1, 'desc' ],
    	],
        columns: [
        	{ title: 'Tipo' },
        	{ title: 'Emissao' },
            { title: 'Numero Pedido' },
            { title: 'NF' },
            { title: 'Cliente' },
            { title: 'Nome Cliente' },
            { title: 'Produto' },
            { title: 'Descricao' },
            { title: 'Lote' },
            { title: 'Quantidade' },
            { title: 'Valor Unit' },
            { title: 'Valor Total' },
            { title: 'Est' },
            { title: 'Armazem' },
            { title: 'CFOP' },
            { title: 'Mens Nota' },
        ],
        dom: 'Blpftip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
        data: data.data,
    });
    return;
});