$(document).ready( function () {
	$('#entradaDe, #entradaAte').datepicker({
		dateFormat: 'yymmdd',
		changeMonth: true,
	    changeYear: true,
	    yearRange: '2014:'+(new Date).getFullYear(),
	});
	
    var table = $('#myTable').DataTable({
    	ajax: {
    		type: "POST",
    	},
    	aaSorting: [[0, 'desc']],
    	iDisplayLength: 100,
    	columns: [
    		{ title: 'Ped Compra' },
    		{ title: 'Emissao' },
    		{ title: 'Fornecedor' },
    		{ title: 'Dt Entrada' },
    		{ title: 'NF' },
    		{ title: 'OP' },
    		{ title: 'Lote' },
    		{ title: 'Produto', class: 'context-item' },
    		{ title: 'Descricao' },
    		{ title: 'UM' },
    		{ title: 'Qtd Ped' },
    		{ title: 'Qtd Ent' },
    		{ title: 'Saldo' },
    		{ title: 'Valor Unitario' },
    		{ title: 'Valor Total' },
    		{ title: 'CFOP' },
    	],
//        columnDefs: [
//            {
//                render: function(data, type, row) {
//                    if (type === 'display' || type === 'filter')
//                        return formatCurrency(data);
//                    else
//                        return data;
//                },
//                targets: [9, 10],
//            }
//        ],
    	dom: 'Blpftip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
        initComplete: function() {
        	$('.context-item').mousedown(function (e) {
        		if (e.which == 3) {
        			var codigoProduto = this.innerHTML;
        			
        			var x = e.originalEvent.x;
        			var y = e.originalEvent.y;
        			
        			var opcoes = [];
        			
        			opcoes.push(
        				{
        					nome: 'Saldo por Endereco',
        					link: '/saldo-por-endereco?codigoProduto=' + codigoProduto + '&lote=&armazem=',
        				},
        				{
        					nome: 'Historico Produto',
        					link: '/historico-produto?codigoProduto=' + codigoProduto,
        				},
        				{
        					nome: 'Ordens Producao',
        					link: '/relacao-ops?numero=&sequencia=&lote=&codigoProduto=' + codigoProduto + '&descricaoProduto=&emissao=&armazem=&status=&usuario=&situacao=abertas',
        				},
        			);
        			
        			criarContextMenu(x, y, opcoes);
        		}
        	});
        },
    });
} );
