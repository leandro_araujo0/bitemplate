var pedidosOrtopedia;
var pedidosEquipamentos;
var graficoComprasSegmentoOrtopedia;
var graficoComprasSegmentoEquipamentos;

$(document).ready(function () {
    $('canvas').after('<div class="spinner"></div>');
    $.get('/indicadores/compras/graficoComprasOrtopedia', function (chartData) {
        var ctx = document.getElementById('graficoComprasOrtopedia').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.scales.yAxes[0].ticks.callback = function (value, index, values) {
            return formatCurrency(value);
        }
        chart.options.tooltips.callbacks.label = function (label) {
            return formatCurrency(label.yLabel);
        }
        chart.options.scales.yAxes[0].ticks.beginAtZero = true;
        chart.options.onClick = function (evt, item) {
            var ano = chart.getElementAtEvent(evt)[0]._view.datasetLabel.replace('Compras ', '');
            var mes = chart.getElementAtEvent(evt)[0]._view.label;
            abrirLink(evt, '/relacao-pcs?unidade=ORTOPEDIA' + '&mes=' + mes + '&ano=' + ano);
        };
        chart.update();
        $('#graficoComprasOrtopedia').next().addClass('d-none');
    });


    $.get('/indicadores/compras/graficoComprasEquipamentos', function (chartData) {
        var ctx = document.getElementById('graficoComprasEquipamentos').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.scales.yAxes[0].ticks.callback = function (value, index, values) {
            return formatCurrency(value);
        }
        chart.options.tooltips.callbacks.label = function (label) {
            return formatCurrency(label.yLabel);
        }
        chart.options.scales.yAxes[0].ticks.beginAtZero = true;
        chart.options.onClick = function (evt, item) {
            var ano = chart.getElementAtEvent(evt)[0]._view.datasetLabel.replace('Compras ', '');
            var mes = chart.getElementAtEvent(evt)[0]._view.label;
            abrirLink(evt, '/relacao-pcs?unidade=EQUIPAMENTOS' + '&mes=' + mes + '&ano=' + ano);
        };
        chart.update();
        $('#graficoComprasEquipamentos').next().addClass('d-none');
    });

    getPedidos();

    getComprasSegmento();

    $('#btn-analise-compra').click(function() {
		$('#totalPedidos').html('-') // Total Pedidos
		$('#valorTotalPedidos').html('-') // Valor Total
		$('#qtdComprasMaior').html('-') // Qtd Maior
		$('#valorComprasMaior').html('-') // Valor Maior
		$('#qtdComprasMenor').html('-') // Qtd Menor
		$('#valorComprasMenor').html('-') // Dif Menor
    	
		analiseCompras('ORTOPEDIA');
		analiseCompras('EQUIPAMENTOS');
    });
    
    $('#btn-analise-compra').click();
});

function analiseCompras(unidade) {
	var analiseComprasDe = $('#analise-compras-de').val();
	var analiseComprasAte = $('#analise-compras-ate').val();
	
	$.ajax({
		type: 'POST',
		url: '/indicadores/compras/analiseCompras',
		data: {
			analiseComprasDe: analiseComprasDe,
			analiseComprasAte: analiseComprasAte,
			unidade: unidade,
		},
		success: function(data) {
			var table;
			if (unidade === 'ORTOPEDIA') {
				table = $('#tabelaAnaliseOrtopedia');
			} else if (unidade === 'EQUIPAMENTOS') {
				table = $('#tabelaAnaliseEquipamentos');
			}
			
			$(table).find('#totalPedidos').html(data.data[0][0]) // Total Pedidos
			$(table).find('#valorTotalPedidos').html(formatCurrency(data.data[0][1])) // Valor Total
			$(table).find('#qtdComprasMaior').html(data.data[0][2]) // Qtd Maior
			$(table).find('#valorComprasMaior').html(formatCurrency(data.data[0][4])) // Valor Maior
			$(table).find('#qtdComprasMenor').html(data.data[0][5]) // Qtd Menor
			$(table).find('#valorComprasMenor').html(formatCurrency(data.data[0][7])) // Dif Menor
		},
	});
}

function getPedidos() {
    if (pedidosOrtopedia != null)
        pedidosOrtopedia.destroy();
    pedidosOrtopedia = $('#pedidosDiaOrtopedia').DataTable({
        dom: 'flrpti',
        columns: [
        	{ title: 'Numero'},
        	{ title: 'Produto'},
        	{ title: 'Descricao'},
        	{ title: 'Quant'},
        	{ title: 'Preco'},
        	{ title: 'Ultimo Preco'},
        	{ title: 'Saving'},
        	{ title: 'Media Preco'},
        	{ title: 'Saving Media'},
        ],
        ajax: {
            url: '/indicadores/compras/pedidos',
            type: 'get',
            data: {
                unidade: 'ORTOPEDIA',
                periodo: $('#selectPeriodo').val(),
            },
        }
    });
    
    if (pedidosEquipamentos != null)
        pedidosEquipamentos.destroy();
    pedidosEquipamentos = $('#pedidosDiaEquipamentos').DataTable({
        dom: 'flrpti',
        columns: [
        	{ title: 'Numero'},
        	{ title: 'Produto'},
        	{ title: 'Descricao'},
        	{ title: 'Quant'},
        	{ title: 'Preco'},
        	{ title: 'Ultimo Preco'},
        	{ title: 'Saving'},
        	{ title: 'Media Preco'},
        	{ title: 'Saving Media'},
        ],
        ajax: {
            url: '/indicadores/compras/pedidos',
            type: 'get',
            data: {
                unidade: 'EQUIPAMENTOS',
                periodo: $('#selectPeriodo').val(),
            },
        }
    });
}

function getComprasSegmento() {
    $.get('/indicadores/compras/comprasSegmento', { unidade: 'ORTOPEDIA', periodo: $('#selectPeriodoSegmento').val() }, function (chartData) {
        if (graficoComprasSegmentoOrtopedia != null) {
            if (chartData.data.datasets[0].data != null) {
                graficoComprasSegmentoOrtopedia.data = chartData.data;
            } else {
                graficoComprasSegmentoOrtopedia.destroy();
                graficoComprasSegmentoOrtopedia = null;
                return;
            }
        } else {
            var ctx = document.getElementById('graficoComprasSegmentoOrtopedia').getContext('2d');
            graficoComprasSegmentoOrtopedia = new Chart(ctx, chartData);
            graficoComprasSegmentoOrtopedia.options.scales.xAxes[0].ticks.beginAtZero = true;
            graficoComprasSegmentoOrtopedia.options.legend.display = false;
            graficoComprasSegmentoOrtopedia.options.scales.xAxes[0].ticks.callback = function (value, index, values) {
                return formatCurrency(value);
            }
            graficoComprasSegmentoOrtopedia.options.tooltips.callbacks.label = function (label) {
                return formatCurrency(label.xLabel);
            }
        }
        graficoComprasSegmentoOrtopedia.update();
        $('#graficoComprasSegmentoOrtopedia').next().addClass('d-none');
    });


    $.get('/indicadores/compras/comprasSegmento', { unidade: 'EQUIPAMENTOS', periodo: $('#selectPeriodoSegmento').val() }, function (chartData) {
        if (graficoComprasSegmentoEquipamentos != null) {
            if (chartData.data.datasets[0].data != null) {
                graficoComprasSegmentoEquipamentos.data = chartData.data;
            } else {
                graficoComprasSegmentoEquipamentos.destroy();
                graficoComprasSegmentoEquipamentos = null;
                return;
            }
        } else {
            var ctx = document.getElementById('graficoComprasSegmentoEquipamentos').getContext('2d');
            graficoComprasSegmentoEquipamentos = new Chart(ctx, chartData);
            graficoComprasSegmentoEquipamentos.options.scales.xAxes[0].ticks.beginAtZero = true;
            graficoComprasSegmentoEquipamentos.options.legend.display = false;
            graficoComprasSegmentoEquipamentos.options.scales.xAxes[0].ticks.callback = function (value, index, values) {
                return formatCurrency(value);
            }
            graficoComprasSegmentoEquipamentos.options.tooltips.callbacks.label = function (label) {
                return formatCurrency(label.xLabel);
            }
        }
        graficoComprasSegmentoEquipamentos.update();
        $('#graficoComprasSegmentoEquipamentos').next().addClass('d-none');
    });
}