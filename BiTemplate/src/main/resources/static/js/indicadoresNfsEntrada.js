var nfsEntradaMensalOrtopedia;
var nfsEntradaMensalEquipamentos;

$(document).ready(function() {
	$.get('/indicadores/nfs-entrada/nfsEntradaMensal?unidade=ORTOPEDIA', function(chartData) {
        if (nfsEntradaMensalOrtopedia != null) {
            nfsEntradaMensalOrtopedia.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('nfsEntradaMensalOrtopedia').getContext('2d');
            nfsEntradaMensalOrtopedia = new Chart(ctx, chartData);
            nfsEntradaMensalOrtopedia.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
                return formatCurrency(value);
            }
            nfsEntradaMensalOrtopedia.options.tooltips.callbacks.label = function(label) {
                return formatCurrency(label.yLabel);
            }
            nfsEntradaMensalOrtopedia.options.scales.yAxes[0].ticks.beginAtZero = true;
            nfsEntradaMensalOrtopedia.options.legend.display = false;
//            nfsEntradaMensalOrtopedia.options.onClick = function (evt, item) {
//            	var ano = nfsEntradaMensalOrtopedia.getElementAtEvent(evt)[0]._view.datasetLabel.replace('Vendas ', '');
//                if (ano.includes('Meta')) {
//                    return;
//                }
//                var mes = nfsEntradaMensalOrtopedia.getElementAtEvent(evt)[0]._view.label;
//                abrirLink(evt, '/pedidos-venda/relacao-vendas?clasped=MERCADOINTERNO' + '&mes=' + mes + '&ano=' + ano);
//            };
        }
        nfsEntradaMensalOrtopedia.update();
        $('#nfsEntradaMensalOrtopedia').next().addClass('d-none');
    });
	
	$.get('/indicadores/nfs-entrada/nfsEntradaMensal?unidade=EQUIPAMENTOS', function(chartData) {
        if (nfsEntradaMensalEquipamentos != null) {
            nfsEntradaMensalEquipamentos.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('nfsEntradaMensalEquipamentos').getContext('2d');
            nfsEntradaMensalEquipamentos = new Chart(ctx, chartData);
            nfsEntradaMensalEquipamentos.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
                return formatCurrency(value);
            }
            nfsEntradaMensalEquipamentos.options.tooltips.callbacks.label = function(label) {
                return formatCurrency(label.yLabel);
            }
            nfsEntradaMensalEquipamentos.options.scales.yAxes[0].ticks.beginAtZero = true;
            nfsEntradaMensalEquipamentos.options.legend.display = false;
//            nfsEntradaMensalEquipamentos.options.onClick = function (evt, item) {
//            	var ano = nfsEntradaMensalEquipamentos.getElementAtEvent(evt)[0]._view.datasetLabel.replace('Vendas ', '');
//                if (ano.includes('Meta')) {
//                    return;
//                }
//                var mes = nfsEntradaMensalEquipamentos.getElementAtEvent(evt)[0]._view.label;
//                abrirLink(evt, '/pedidos-venda/relacao-vendas?clasped=MERCADOINTERNO' + '&mes=' + mes + '&ano=' + ano);
//            };
        }
        nfsEntradaMensalEquipamentos.update();
        $('#nfsEntradaMensalEquipamentos').next().addClass('d-none');
    });
});