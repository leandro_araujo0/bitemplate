$(document).ready(function() {
	$('.date-picker').datepicker({
		dateFormat: 'yymmdd',
		changeMonth: true,
	    changeYear: true,
	    yearRange: '2014:'+(new Date).getFullYear(),
	});
	
	$('.disable-enter').keypress(function(e) {
		return e.which != 13;
	});
	
	$('body').append('<a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>');
	$('#return-to-top').click(function() {      // When arrow is clicked
		$('body,html').animate({
			scrollTop : 0                       // Scroll to top of body
		}, 500);
	});
	
	$('.dropdown-nav').hover(function() {
		if (window.innerWidth >= 1000)
			$(this).find('.main-drop.dropdown-itens').removeClass('d-none');
	}, function() {
		if (window.innerWidth >= 1000)
			$(this).find('.main-drop.dropdown-itens').addClass('d-none');
	});
	
	$('.dropdown-submenu').hover(function() {
		$(this).find('>.dropdown-submenu-link').addClass('selected');
		$(this).find('>.dropdown-subitens').removeClass('d-none');
	}, function() {
		$(this).find('.dropdown-subitens').addClass('d-none');
		$(this).find('.dropdown-submenu-link').removeClass('selected');
	});
});

function clamp(min, max, value) {
	if (value < min) {
		return min;
	} else if (value > max) {
		return max;
	} else {
		return value;
	}
}

var reabrir;
function criarContextMenu(x, y, opcoes) {
	$('body').mousedown(function(e) {
		if (e.originalEvent.x >= $('.context-menu')[0].offsetLeft && e.originalEvent.x <= $('.context-menu')[0].offsetLeft + $('.context-menu')[0].offsetWidth) {
			if (e.originalEvent.y >= $('.context-menu')[0].offsetTop && e.originalEvent.y <= $('.context-menu')[0].offsetTop + $('.context-menu')[0].offsetHeight) {
				return;
			}
		}
		
		if ($('.context-menu')[0] != null) {
			$('body').bind('oncontextmenu', function() { return false });
			$('.context-menu').remove();
			$('body').unbind('mousedown');
		}
	});
	
	if ($('.context-menu')[0] =! null) {
		$('.context-menu').remove();
		reabrir = true;
	} else {
		reabrir = false;
	}
	
	var context = '<div class="context-menu" style="' + 
		'left: ' + clamp(0, $(document).width() - 400, x) + 'px;\r\n' +
		'top: ' + y + 'px;\r\n' +
		'" oncontextmenu="return false">' + 
		'<div class="opcoes">';
		$(opcoes).each(function() {
			if (this.callback != null) {
				context += '<div class="item"><a class="context-link" href="#" onclick="(' + this.callback + ')()">' + this.nome + '</a></div>';
			} else {
				context += '<div class="item"><a class="context-link" href="' + this.link + '">' + this.nome + '</a></div>';
			}
		});
		context += '</div></div>';
	$('body').append(context);
//	$('.context-link').mouseup(function(e) {
//		if (e.which == 1 || e.which == 2) {
//			$('.context-menu').remove();
//		}
//	});
}

function addTextAreaCallback(textArea, callback, delay) {
    var timer = null;
    textArea.onkeypress = function() {
        if (timer) {
            window.clearTimeout(timer);
        }
        timer = window.setTimeout( function() {
            timer = null;
            callback();
        }, delay );
    };
    textArea = null;
}


function formatCurrency(value) {
    var tempValue = Number(value).toFixed(2).split('.');
    if (value >= 0) {
    	tempValue[0] = "R$ " + tempValue[0].split(/(?=(?:...)*$)/).join('.');
    } else {
    	tempValue[0] = tempValue[0].replace('-', '');
    	tempValue[0] = "- R$ " + tempValue[0].split(/(?=(?:...)*$)/).join('.');
    }
    return tempValue.join(',');
}

function formatCurrencyAsMillion(value) {
	var tempValue = Number(value / 1000000).toFixed(2).split('.');
	if (value >= 0) {
		tempValue[0] = "R$ " + tempValue[0].split(/(?=(?:...)*$)/).join('.');
	} else {
		tempValue[0] = tempValue[0].replace('-', '');
		tempValue[0] = "- R$ " + tempValue[0].split(/(?=(?:...)*$)/).join('.');
	}
	return tempValue.join(',') + " MI";
}

function formatCurrencyAsMil(value) {
	var tempValue = Number(value / 1000).toFixed(1).split('.');
	if (value >= 0) {
		tempValue[0] = "R$ " + tempValue[0].split(/(?=(?:...)*$)/).join('.');
	} else {
		tempValue[0] = tempValue[0].replace('-', '');
		tempValue[0] = "- R$ " + tempValue[0].split(/(?=(?:...)*$)/).join('.');
	}
	return tempValue.join(',') + " MIL";
}

function abrirLink(evt, link) {
	if (evt != null && evt.ctrlKey) {
		window.open(link);
	} else {
		window.location.href = link;
	}
}

function menuToggle(element) {
	$('nav').toggleClass('expanded-nav');
	element.classList.toggle('change');
	$('.nav-content').toggleClass('hide-mobile');
}


//===== Scroll to Top ==== 
$(window).scroll(function() {
    var scrollTop = $(this).scrollTop();
    if (scrollTop >= 500) {        // If page is scrolled more than 50px
        $('#return-to-top').fadeIn(200);    // Fade in the arrow
    } else {
        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
    }
    
    if (scrollTop >= 20) {
    	$('nav').addClass('nav-fixed');
    	$('.menu-lateral').addClass('menu-lateral-fixed');
    	$('.menu-direito').addClass('menu-lateral-fixed');
        $('#indicadores').addClass('indicadores-fixed');
        $('body').addClass('body-fixed');
    } else {
    	$('nav').removeClass('nav-fixed');
    	$('.menu-lateral').removeClass('menu-lateral-fixed');
    	$('.menu-direito').removeClass('menu-lateral-fixed');
        $('#indicadores').removeClass('indicadores-fixed');
        $('body').removeClass('body-fixed');
    }
});