$(document).ready(function() {
	$('#memorando').focus(function() {
		$('#btn-salvar').removeClass('disabled');
		$('#btn-salvar').html('Salvar');
	});
	
	
	$('#btn-salvar').click(function() {
		var fup = {
			codigoProduto: $('#codigo').html(),
			descricao: $('#descricao').html(),
			memorando: $('#memorando').html(),
		};
		
		
		$.ajax({
			url: '/follow-up-produto/salvar',
			type: 'POST',
			data: fup,
			success: function(data) {
				if (data) {
					$('#btn-salvar').html('Salvo');
					$('#btn-salvar').addClass('disabled');
				} else {
					$('#btn-salvar').html('Erro');
				}
			}
		});
	});
});