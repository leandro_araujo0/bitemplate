var table;
var unidadeVendas;

$(document).ready(function() {
});

function createTable() {
    if (table != null) {
        table.destroy();
    }

    unidade = $('#selectUnidade').val();
    unidadeVendas = $('#selectMercado').val();
    var codigoCliente = $('#codigoCliente').val();
    
    
    var columns;
    
    if (codigoCliente != '') {
    	columns = [
    		{ title: 'Cliente' },
    		{ title: 'Produto' },
    		{ title: 'Descricao' },
    		{ title: 'Preço Venda' },
    		{ title: 'Tabela' },
    	];
    } else {
        columns = [
	    	{ title: 'Produto' },
	        { title: 'Descricao', class: "nowrap" },
	        { title: 'Quantidade' },
	        { title: 'Preco de Venda' },
	        { title: 'Moeda' }
        ];
    }

    table = $('#table').DataTable({
        dom: 'Blpftip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
        aaSorting: [[1, 'asc']],
        columns: columns,
        ajax: {
            url: '/lista-de-precos/getTabela',
            type: 'POST',
            data: {
                unidadeVendas: unidadeVendas,
                codigoCliente: codigoCliente,
            },
        }
    });
}