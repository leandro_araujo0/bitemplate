$(document).ready(function() {
	var table = $('#myTable').DataTable({
		iDisplayLength: 10,
		columns: [
			{ title: 'Produto' },
			{ title: 'Desc' },
			{ title: 'E/S' },
			{ title: 'Tipo Mov' },
			{ title: 'Qtd' },
			{ title: 'Armazem' },
			{ title: 'Lote' },
			{ title: 'Endereco' },
			{ title: 'Emissao' },
			{ title: 'Doc' },
			{ title: 'Serie' },
			{ title: 'Op', class: 'op' },
		],
		ajax: {
			type: 'POST'
		},
		dom: 'Blfrpti',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
        initComplete: function() {
            $('#myTable tfoot th').each( function () {
                var title = $(this).text();
                if (title != '')
                	$(this).html( '<input class="w-100" type="text" placeholder="' + title + '" />' );
            });

            table.columns().every( function () {
                var that = this;
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that.search( this.value )
                            .draw();
                    }
                });
            });
            
        	$('#myTable tr .op').not(':first').on('contextmenu', function(e) {
        		e.preventDefault();
        		var op = e.currentTarget.innerHTML.substring(0, 6);
        		
    			var x = e.originalEvent.x;
    			var y = e.originalEvent.y;
    			
    			var opcoes = [];
    			
    			opcoes.push(
    				{
    					nome: 'Detalhes Op',
    					link: '/ordens-producao/' + op + '/detalhes',
    				},
    			);
    			criarContextMenu(x, y, opcoes);
			});
        },
	});
});