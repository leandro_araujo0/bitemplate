$(document).ready(function() {
	$.ajax({
		type: 'POST',
		success: function(dataTable) {
			var columns = [];
			
			dataTable.headers.forEach(function(str, i) {
				if (i > 1) {
					columns.push({ title: str, class: str + ' drilldown' });
				} else {
					columns.push({ title: str, class: str });
				}
			});
			
			var table = $('#myTable').DataTable({
				columns: columns,
				data: dataTable.data,
				aaSorting: [ 
					[ 4, 'desc' ],
				],
		        columnDefs: [
	            {
	                render: function(data, type, row) {
	                    if (type === 'display' || type === 'filter')
	                        return formatCurrency(data);
	                    else
	                        return data;
	                },
	                targets: [2, 3, 4, 5],
	            	}
	            ],
			});
			
			$('#myTable').on('click', 'td', function(e) {
				var codCliente = $(this).parent().find('.A1_COD').html();
				if ($(this).hasClass('NUM_ABERTOS')) {
					document.location.href = '/orcamentos/analitico?codCliente=' + codCliente + '&statusOrcamento=ABERTOS';
				} else if ($(this).hasClass('NUM_APROVADOS')) {
					document.location.href = '/orcamentos/analitico?codCliente=' + codCliente + '&statusOrcamento=APROVADOS';
				} else if ($(this).hasClass('NUM_CANCELADOS')) {
					document.location.href = '/orcamentos/analitico?codCliente=' + codCliente + '&statusOrcamento=CANCELADOS';
				} else if ($(this).hasClass('NUM_RECEBIDOS')) {
					document.location.href = '/orcamentos/analitico?codCliente=' + codCliente + '&statusOrcamento=RECEBIDOS';
				}
			});
		},
	});
});