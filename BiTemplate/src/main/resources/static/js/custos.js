$(document).ready(function () {
	var table = $('#myTable').DataTable({
		iDisplayLength: 10,
		aaSorting: [[0, 'asc']],
		columns: [
			{ title: 'Produto'},
			{ title: 'Descricao'},
			{ title: 'Armazem'},
			{ title: 'Custo'},
			{ title: 'Preco Venda'},
			{ title: 'Mkup'},
		],
		ajax: {
			type: 'POST',
		},
		dom: 'Blfrpti',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
        columnDefs: [
	        {
	            render: function(data, type, row) {
	                if (type === 'display' || type === 'filter')
	                    return formatCurrency(data);
	                else
	                    return data;
	            },
	            targets: [3, 4],
        	},
        	{
	            render: function(data, type, row) {
	                if (type === 'display' || type === 'filter')
	                    return Math.round( data * 100 + Number.EPSILON ) / 100;
	                else
	                    return data;
	            },
	            targets: [5],
        	}
        ],
        initComplete: function() {
            $('#myTable tfoot th').each( function () {
                var title = $(this).text();
                if (title != '')
                	$(this).html( '<input class="w-100" type="text" placeholder="' + title + '" />' );
            });

            table.columns().every( function () {
                var that = this;
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that.search( this.value )
                            .draw();
                    }
                });
            });
        },
	});
});