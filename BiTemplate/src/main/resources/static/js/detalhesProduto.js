$(document).ready(function () {
    var codigoProduto = $('#tdCodigo').html();

    $.get('/produtos/chartData', 'codigoProduto=' + codigoProduto, function (chartData) {
        var ctx = document.getElementById('graficoConsumoProduto').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.elements.line.tension = 0;
        chart.options.legend.display = false;
        chart.options.scales.yAxes[0].ticks.beginAtZero = true;
        chart.options.onClick = function (evt, item) {
            if (item[0] != null) {
                var ano = item[0]['_xScale'].ticks[item[0]['_index']].substring(0, 4);
                var mes = item[0]['_xScale'].ticks[item[0]['_index']].substring(4, 6);
                abrirLink('/pedidos-venda/relacao-vendas?ano=' + ano + '&mes=' + mes + '&codigoProduto=' + $('#codigoProduto').html());
            }
        };
        chart.update();
    });
    $.get('/produtos/graficoCompradoresProduto', 'codigoProduto=' + codigoProduto, function (chartData) {
        var ctx = document.getElementById('graficoCompradoresProduto').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.elements.line.tension = 0;
        chart.options.legend.display = false;
        chart.options.scales.xAxes[0].ticks.beginAtZero = true;
        chart.options.onClick = function (evt, item) {
            if (item[0] != null) {
                var obj = item[0]['_model'].label.split(';')[0];
                window.open('/clientes/' + obj + '/detalhes', 'parent');
            }
        };
        chart.update();
    });
});