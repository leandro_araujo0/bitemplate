var produtos = [];
var qtdPv = 0;
var cmm = 0;
var pmm = 0;
var chart;
var table;

$(document).ready(function() {
    $(btnPreForm).click(function() {
        gerarLista();
    });

    $('#preForm input').keypress(function(event) {
        var code = event.which || event.keyCode;
        if (code == 13) {
            gerarLista();
            return;
        }
    });

    $(btnMostrarPreForm).click(function() {
        table.destroy();
        $('#btn-limpar').click();
        $(preForm).removeClass('d-none');
        $(produto).focus();
        produto.select();
    });

});

function gerarLista() {
    $(preForm).addClass('d-none');
    table = $('#myTable').DataTable({
        iDisplayLength: 10000,
        responsive: true,
    	dom: 'Blfrpti',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
        ajax: {
            url: '/produtos/lista',
            type: 'get',
            data: {
                tipo: tipo.value,
                produto: produto.value,
                descricao: descricao.value,
                bloqueio: bloqueio.value,
            },
        },
        columnDefs: [ {orderable : false, targets : 0 }
        ],
        initComplete: function() {
            $('#myTable tfoot th').each(function() {
                var title = $(this).text();
                if (title != '')
                    $(this).html('<input class="w-100" type="text" placeholder="'+title+'" />');
            });

            table.columns().every(function() {
                var that = this;
                $('input', this.footer()).on('keyup change', function() {
                    if (that.search() !== this.value) {
                        that.search(this.value).draw();
                    }
                });
             });
             $(checkboxSelectAll).click();
        },
    });


    $('#checkboxSelectAll').click(function() {
        $('input[type="checkbox"]').not(':first').each(function() {
            if($(this).prop('checked') != $('#checkboxSelectAll').prop('checked')) {
                $(this).click();
            }
        });
    });

    $('#btn-grafico').click(function() {
        $.ajax({
            url: '/produtos/getGrafico',
            type: 'post',
            data: {
                codigoProdutos: produtos
            },
        }).done(function(chartData) {
            if (chart != null) {
                if (chartData.data.datasets[0].data == null) {
                    chart.destroy();
                    chart = null;
                    return;
                } else {
                    chart.data = chartData.data;
                }
            } else {
                var ctx = document.getElementById('canvas-graficoProdutos').getContext('2d');
                chart = new Chart(ctx, chartData);
                chart.options.elements.line.tension = 0;
                chart.options.legend.display = false;
                chart.options.scales.yAxes[0].ticks.beginAtZero = true;
                chart.options.onClick = function (evt, item) {
                    if (item[0] != null) {
                        var ano = item[0]['_xScale'].ticks[item[0]['_index']].substring(0, 4);
                        var mes = item[0]['_xScale'].ticks[item[0]['_index']].substring(4, 6);
                        $('body').append('<form action="/pedidos-venda/relacao-vendas" id="formProdutos" method="post"></form>');
                        $('#formProdutos').append('<input type="hidden" name="mes" value="' + mes + '"/>');
                        $('#formProdutos').append('<input type="hidden" name="ano" value="' + ano+ '"/>');
                        for (var i = 0; i < produtos.length; i++) {
                            $('#formProdutos').append('<input type="hidden" name="codigosProdutos" value="' + produtos[i] + '"/>');
                        }
                        $('#formProdutos').append('<input name="_csrf" type="hidden" value="' + document.querySelector("meta[name='_csrf']").getAttribute('content') + '">');
                        $('#formProdutos').submit();
                    }
                };
            }
            chart.update();
            return;
        });

        $('#div-grafico').removeClass('d-none');
    });

    $('#btn-limpar').click(function() {
        produtos = [];
        qtdPv = 0;
        cmm = 0;
        pmm = 0;
        $('input[type="checkbox"]').prop('checked', false);
        table.column(0).nodes().to$().find('input[type="checkbox"]').prop('checked', false);
        $('#produtosSelecionados').addClass('d-none');
    });

    $('#fechar-div-grafico').click(function() {
        $('#div-grafico').addClass('d-none');
    });
}

function addProduto(produto, checkbox) {
    var cmm = Number($(checkbox).last().parent().parent().find('.cmm').html());
    var qtdPv = Number($(checkbox).last().parent().parent().find('.qtdPv').html());
    var pmm = Number($(checkbox).last().parent().parent().find('.pmm').html());
    if (checkbox.checked) {
        this.produtos.push(produto);
        this.cmm += cmm
        this.qtdPv += qtdPv;
        this.pmm += pmm;
    } else {
        this.produtos.splice(this.produtos.indexOf(produto), 1);
        this.cmm -= cmm;
        this.qtdPv -= qtdPv;
        this.pmm -= pmm;
    }
    if (this.produtos.length > 0) {
        $('#qtdProdutos').html(produtos.length);
        $('#qtdCMM').html(this.cmm);
        $('#qtdPv').html(this.qtdPv);
        $('#qtdPMM').html(this.pmm);
        $('#produtosSelecionados').removeClass('d-none');
    } else {
        $('#produtosSelecionados').addClass('d-none');
    }
}