$(document).ready(function() {
	var table = $(myTable).DataTable({
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
		dom: 'Blpftip',
		iDisplayLength: '100',
		columns: [
			{ title: 'Data' },
			{ title: 'Cod Invent' },
			{ title: 'Armazem' },
			{ title: 'Enderecos' },
			{ title: 'Contagens Def' },
			{ title: 'Contagens Realiz' },
		],
		ajax: '/utilitarios/acompanhamento-inventario/tabela',
	});
});