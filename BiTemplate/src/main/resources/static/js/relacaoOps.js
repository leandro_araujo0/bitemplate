$(document).ready(function() {
	var table = $(myTable).DataTable({
		iDisplayLength: 100,
		columns: [
			{ title: 'OP' },
			{ title: 'Numero' },
			{ title: 'Emissao' },
			{ title: 'Status?' },
			{ title: 'It' },
			{ title: 'Seq' },
			{ title: 'Arm' },
			{ title: 'Produto', class: 'nowrap' },
			{ title: 'Descricao', class: 'nowrap' },
			{ title: 'Qtd' },
			{ title: 'Qtd Prod' },
			{ title: 'Lote' },
			{ title: 'Usuario' },
			{ title: 'Obs' },
			{ title: 'Impressoes' },
		],
		ajax: {
			type: 'POST'
		},
		dom: 'Blfrpti',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
        initComplete: function() {
            $('#myTable tfoot th').each( function () {
                var title = $(this).text();
                if (title != '')
                	$(this).html( '<input class="w-100" type="text" placeholder="' + title + '" />' );
            });

            table.columns().every( function () {
                var that = this;
                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that.search( this.value )
                            .draw();
                    }
                });
            });
            
            $('th:nth-child(4)').attr('title', 'E - OP TOTALMENTE EMPENHADA\r\nI - OP IMPRESSA\r\nA - OP CONTEM APONTAMENTO\r\nD - ESTOQUE DISPONÍVEL\r\nF - OP COM PC EM ABERTO');
        },
	});
});