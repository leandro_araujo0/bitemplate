var table;

$(document).ready(function () {
    addKeyListener();
    
	$('#dataOps, #dataPcs').datepicker({
		dateFormat: 'yymmdd',
		changeMonth: true,
	    changeYear: true,
	    yearRange: '2014:'+(new Date).getFullYear(),
	});
    
    document.getElementsByClassName('codigoProduto')[0].addEventListener('paste', (e) => {
        e.preventDefault();
        const text = (e.originalEvent || e).clipboardData.getData('text/plain');
        window.document.execCommand('insertText', false, text);
        var linhas = text.split('\r\n');

        $(linhas).each(function(index, item) {
        	if (this != '') {
        		$('.codigoProduto').last().val(this.split('	')[0]);
        		$('.qtdBase').last().val(this.split('	')[1].replace(',', '.'));
        		if (index != linhas.length - 1) {
        			adicionarProduto();
        		}
            }
        });
        removerProduto();
    });
});

function alteraClasse(selector, classe) {
    $(selector).toggleClass(classe);
}

function importarProdutos() {
    var unidade = $('#unidade').val();
    var tipoImportacao = $('#tipoImportacao').val();
    var url;

    if (unidade == null || tipoImportacao == null) {
        return;
    }

    if (tipoImportacao == 'CADASTRO') {
        url = '/produtos/getProdutos';
    } else if (tipoImportacao == 'CARTEIRA') {
        url = '/carteira/getProdutosCarteira';
    }

    $.ajax({
        url: url,
        method: 'POST',
        data: {
            unidade: unidade,
            tipoImportacao: tipoImportacao,
        }
    }).done(function (produtosCarteira) {
        $(produtosCarteira).each(function () {
            $('.form-produto').last().find('.codigoProduto').val(this.produto.codigo);
            $('.form-produto').last().find('.qtdBase').val(this.saldo);

            adicionarProduto();
        });

        removerProduto();

        $('#importacao').click();
    });
}

function removerProduto() {
    $('.form-produto').not(':first').last().remove();
}

function removerProdutos() {
    $('.form-produto').not(':last').remove();
}

function addKeyListener() {
    $('.form-row').find('input').keydown(function () {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == 13) {
            processar();
            $('#button-form').focus();
        }
    });

    $('.qtdBase').last().keydown(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (!event.shiftKey && keycode == 9) {
            adicionarProduto();
            event.preventDefault();
        }
        if (keycode == 13) {
            processar();
            $('#button-form').focus();
        }
        $('.qtdBase').not(':last').off('keydown');
    });
}

function adicionarProduto() {
    $('.form-produto').last().after($('.form-produto').last().clone());
    $('.form-produto').last().find('input').val('');
    $('.form-produto').last().find('input.qtdBase').val('1');
    $('.codigoProduto').last().focus();
    addKeyListener();
}

//function opcionais() {
//	var codigoProdutos = $('.codigoProduto').map(function () { return $(this).val() }).toArray();
//	$.post('/utilitarios/processamento/opcionais', { codigoProdutos: codigoProdutos }, function(data) {
//		$(data).each(function() {
//			$('body').append('<div class="opcionais"><h2>Opcionais</h2></div>');
//		});
//	});
//}

function processar() {
	if ($('#button-form').hasClass('disabled')) {
		return;
	}
	
	var tempo = 0;
	var contador = setInterval(function() {
		$('#button-form').html("Processando... (" + tempo + ")");
		tempo += 1;
	}, 1000);
	
	
	$('#button-form').addClass('disabled');
	
	
    var nivelMaximo = $('#nivelMaximo').val();
    var dataOps = $('#dataOps').val();
    var dataPcs = $('#dataPcs').val();
    var analisaSaldos = $('#analisaSaldos').prop('checked');
    var codigoProdutos = $('.codigoProduto').map(function () { return $(this).val() }).toArray();
    var qtdsBase = $('.qtdBase').map(function () { return $(this).val() }).toArray();
    var exportarExcel = $('#exportarExcel').prop('checked');
    var processarEstoque = $('#processarEstoque').prop('checked');
    
    if (table != null) {
        table.destroy();
    }
    
    var columns = [];
	columns.push({ title: 'Cascata' });
	columns.push({ title: 'Produto Pai' });
	columns.push({ title: 'Descricao' });
	columns.push({ title: 'Qtd Base' });
	columns.push({ title: 'Nivel' });
    columns.push({ title: 'Opcional' });
    columns.push({ title: 'Codigo' });
    columns.push({ title: 'Descricao' });
    columns.push({ title: 'UM' });
    columns.push({ title: 'Qtd Necess' });
    if (processarEstoque) {
    	columns.push({ title: 'Qtd Recalc' });
	} else {
		columns.push({ title: 'Qtd Recalc', class: 'd-none' });
	}
    columns.push({ title: 'Qtd Estoque' });
    columns.push({ title: 'Empenho Ops' });
    columns.push({ title: 'Qtd Enderecar' });
    columns.push({ title: 'Qtd Inspecao' });
    columns.push({ title: 'Nec. Empenho' });
    columns.push({ title: 'Qtd OP' });
    columns.push({ title: 'Qtd SC' });
    columns.push({ title: 'Qtd PC' });
    
    
    table = $('#myTable').DataTable({
        dom: 'Blpftip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
        ajax: {
            url: '/utilitarios/processamento/getMRP',
            type: 'POST',
            data: {
                codigoProdutos: codigoProdutos,
                qtdsBase: qtdsBase,
                nivelMaximo: nivelMaximo,
                analisaSaldos: analisaSaldos,
                dataOps: dataOps,
                dataPcs: dataPcs,
                processarEstoque: processarEstoque,
            },
        },
        columns: columns,
		columnDefs: [
			{
				"targets": [ 0 ],
				"visible": false,
				"searchable": false
			},
		],
        error: function (xhr, error, code) {
            console.log(xhr);
            console.log(code);
            $('#button-form').removeClass('disabled');
        	$('#button-form').html('Processar (' + tempo + ')');
            clearInterval(contador);
        },
        initComplete: function() {
        	$('#button-form').removeClass('disabled');
        	$('#button-form').html('Processar (' + tempo + ')');
            clearInterval(contador);
            
            if (exportarExcel) {
            	$('.btn.btn-secondary.buttons-excel.buttons-html5').click();
            }
S        },
    });
};