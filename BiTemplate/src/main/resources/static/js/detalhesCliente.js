$(document).ready( function () {
    var codigoCliente = document.getElementById('scriptDetalhesCliente').getAttribute('codigoCliente');

    $.get('/clientes/graficoVendas', 'codCliente=' + codigoCliente, function(chartData) {
    var ctx = document.getElementById('graficoComprasCliente').getContext('2d');
    var chart = new Chart(ctx, chartData);
    chart.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
        return formatCurrency(value);
    }
    chart.options.tooltips.callbacks.label = function(label) {
        return formatCurrency(label.yLabel);
    }
    chart.options.onClick = function (evt, item) {
        if (item[0] != null) {
            var ano = item[0]['_xScale'].ticks[item[0]['_index']].substring(3, 7);
            var mes = item[0]['_xScale'].ticks[item[0]['_index']].substring(0, 2);
            abrirLink(evt, '/pedidos-venda/relacao-vendas?ano=' + ano + '&mes=' + mes + '&codigoCliente=' + codigoCliente);
        }
    };
    chart.options.legend.display = false;
    chart.options.scales.yAxes[0].ticks.beginAtZero = true;
    chart.update();
    });

    $.get('/clientes/graficoEntregasCliente', {codCliente: codigoCliente, ano: 3}, function(chartData) {
        var ctx = document.getElementById('graficoEntregasCliente1').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.tooltips.callbacks.label = function(tooltipItem, data) {
            return data.datasets[0].data[tooltipItem.index] + ' %'
        }
        chart.options.tooltips.bodyFontSize = 17;
        chart.options.tooltips.titleFontSize = 60;
        chart.options.title.display = true;
        
        chart.update();
    });

    $.get('/clientes/graficoEntregasCliente', {codCliente: codigoCliente, ano: 2}, function(chartData) {
        var ctx = document.getElementById('graficoEntregasCliente2').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.tooltips.callbacks.label = function(tooltipItem, data) {
            return data.datasets[0].data[tooltipItem.index] + ' %'
        }
        chart.options.tooltips.bodyFontSize = 17;
        chart.options.tooltips.titleFontSize = 60;
        chart.options.title.display = true;
        chart.update();
    });


    $.get('/clientes/graficoEntregasCliente', {codCliente: codigoCliente, ano: 1}, function(chartData) {
        var ctx = document.getElementById('graficoEntregasCliente3').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.tooltips.callbacks.label = function(tooltipItem, data) {
            return data.datasets[0].data[tooltipItem.index] + ' %'
        }
        chart.options.tooltips.bodyFontSize = 17;
        chart.options.tooltips.titleFontSize = 60;
        chart.options.title.display = true;
        chart.update();
    });


    $.get('/clientes/graficoEntregasCliente', {codCliente: codigoCliente, ano: 0}, function(chartData) {
        var ctx = document.getElementById('graficoEntregasCliente4').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.tooltips.callbacks.label = function(tooltipItem, data) {
            return data.datasets[0].data[tooltipItem.index] + ' %'
        }
        chart.options.tooltips.bodyFontSize = 17;
        chart.options.tooltips.titleFontSize = 60;
        chart.options.title.display = true;
        chart.update();
    });

    $.get('/clientes/graficoProdutosMaisComprados', 'codCliente=' + codigoCliente, function(chartData) {
        var ctx = document.getElementById('graficoProdutosMaisComprados').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.scales.xAxes[0].ticks.callback = function(value, index, values) {
            return formatCurrency(value);
        }
        chart.options.tooltips.callbacks.label = function(label) {
            return formatCurrency(label.xLabel);
        }
        chart.options.onClick = function (evt, item) {
            if (item[0] != null) {
                var obj = item[0]['_model'].label.split(';')[0];
                window.open('/produtos/detalhes?codigoProduto=' + obj, '_blank');
            }
        };
        chart.options.legend.display = false;
        chart.options.scales.xAxes[0].ticks.beginAtZero = true;
        chart.update();
    });
    
    var tableMapaVendas = $('#tableMapaVendas').DataTable({
    	dom: 't',
    	ajax: {
    		type: 'POST',
    		url: '/clientes/getMapaVendas',
    		data: {
    			codigoCliente: codigoCliente
			},
    	},
    	columns: [
    		{ title: 'Ano' },
    		{ title: 'Janeiro' },
    		{ title: 'Fevereiro' },
    		{ title: 'Março' },
    		{ title: 'Abril' },
    		{ title: 'Maio' },
    		{ title: 'Junho' },
    		{ title: 'Julho' },
    		{ title: 'Agosto' },
    		{ title: 'Setembro' },
    		{ title: 'Outubro' },
    		{ title: 'Novembro' },
    		{ title: 'Dezembro' },
    		{ title: 'Total' }
    	],
        columnDefs: [
            {
                render: function(data, type, row) {
                    if (type === 'display' || type === 'filter')
                        return formatCurrency(data);
                    else
                        return data;
                },
                targets: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
            }
        ],
    });
    
//    $.post('/clientes/getVendasLinha', {codigoCliente: codigoCliente}, function() {
//    	console.log('teste');
//    });
    
	$('.progress').each(function() {
		$(this).find('.progress-bar').width($(this).find('.porcentagemReferencia').html() + '%');
	});
	
	for(i = 1; i <= $('.metaCard').length; i++) {
		var cardSelecionado = $('.metaCard:nth-child(' + i + ')');
		for(j = 1; j <= $('.metaCard').length; j++) {
			console.log($(cardSelecionado).find('H3').html());
			console.log($('.metaCard:nth-child(' + j + ')').find('H3').html());
			
			console.log(Number($(cardSelecionado).find('.value').html().replace('%', '')) > Number($('.metaCard:nth-child(' + j + ')').find('.value').html().replace('%', '')));
			if (Number($(cardSelecionado).find('.value').html().replace('%', '')) > Number($('.metaCard:nth-child(' + j + ')').find('.value').html().replace('%', ''))) {
				$('.metaCard:nth-child(' + j + ')').before($(cardSelecionado));
				i = 0;
				break;
			} else {
				if (i == j) {
					break;
				}
			}

		}
	}
	
	
	$('#vendasPorGrupo').DataTable({
		dom: 'Blfrpti',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
		columns: [
			{ title: 'Grupo' },
			{ title: 'Cliente', class: 'd-none' },
			{ title: '2017' },
			{ title: '2018' },
			{ title: '2019' },
			{ title: '2020' },
			{ title: '2021' },
			{ title: 'Total' },
		],
		iDisplayLength: 25,
		ajax: {
			url: '/indicadores/vendas-por-linha/vendas/por-cliente?codigoCliente=' + codigoCliente,
			type: 'POST',
		},
		columnDefs: [
		],
		aaSorting: [[3, 'DESC']],
	});
    
});