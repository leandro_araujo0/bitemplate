var dashboardOrcamentosOrtopedia;
var dashboardOrcamentosEquipamentos;

$(document).ready(function(){
	$('canvas').after('<div class="spinner"></div>');
	
	var url = '/indicadores/orcamentos/dashboard?unidade=ORTOPEDIA';
	$.post(url, function(chartData) {
        if (dashboardOrcamentosOrtopedia != null) {
        	dashboardOrcamentosOrtopedia.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('dashboard-orcamentos-ortopedia').getContext('2d');
            dashboardOrcamentosOrtopedia = new Chart(ctx, chartData);
            dashboardOrcamentosOrtopedia.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
                return formatCurrencyAsMillion(value);
            }
            dashboardOrcamentosOrtopedia.options.tooltips.callbacks.label = function(label) {
                return formatCurrency(label.yLabel);
            }
            dashboardOrcamentosOrtopedia.options.scales.yAxes[0].ticks.beginAtZero = true;
            dashboardOrcamentosOrtopedia.options.onClick = function (evt, item) {
            	var mes = this.getElementAtEvent(evt)[0]._model.label;
            	var ano = new Date().getFullYear();
            	var unidade = 'ORTOPEDIA';
            	var link = '/orcamentos/sintetico?mes=' + mes + '&ano=' + ano + '&unidade=' + unidade;
            	abrirLink(evt, link);
            };
        }
        dashboardOrcamentosOrtopedia.update();
        $('#dashboard-orcamentos-ortopedia').next().addClass('d-none');
    });
	
	url = '/indicadores/orcamentos/dashboard?unidade=EQUIPAMENTOS';
	$.post(url, function(chartData) {
		if (dashboardOrcamentosEquipamentos != null) {
			dashboardOrcamentosEquipamentos.data.datasets = chartData.data.datasets;
		} else {
			var ctx = document.getElementById('dashboard-orcamentos-equipamentos').getContext('2d');
			dashboardOrcamentosEquipamentos = new Chart(ctx, chartData);
			dashboardOrcamentosEquipamentos.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
				return formatCurrencyAsMillion(value);
			}
			dashboardOrcamentosEquipamentos.options.tooltips.callbacks.label = function(label) {
				return formatCurrency(label.yLabel);
			}
			dashboardOrcamentosEquipamentos.options.scales.yAxes[0].ticks.beginAtZero = true;
			dashboardOrcamentosEquipamentos.options.onClick = function (evt, item) {
            	var mes = this.getElementAtEvent(evt)[0]._model.label;
            	var ano = new Date().getFullYear();
            	var unidade = 'EQUIPAMENTOS';
            	var link = '/orcamentos/sintetico?mes=' + mes + '&ano=' + ano + '&unidade=' + unidade;
            	abrirLink(evt, link);
			};
		}
		dashboardOrcamentosEquipamentos.update();
		$('#dashboard-orcamentos-equipamentos').next().addClass('d-none');
	});
});