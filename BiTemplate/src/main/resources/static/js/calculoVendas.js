var table;

$(document).ready(function() {
    $('#btnProcessar').click(calculaVendas);

    $('input, select').keypress(function(e) {
        if (e.keyCode == 13) {
            $(this).focus();
            $(btnProcessar).click();
            e.preventDefault();
        }
    });
    
    $(unidadeVendas).change(function() {
    	if ($(this).val() != 'unidade') {
    		$(this).removeClass('alertBorder');
    	}
    });
});

function calculaVendas() {
	if ($(unidadeVendas).val() == 'unidade') {
		$(unidadeVendas).addClass('alertBorder');
		alert('Escolha uma Unidade de Vendas!');
		return;
	}
	
    if (table != null)
        table.destroy();
    table = $('#myTable').DataTable({
        dom: 'Blpftip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
        columns: [
            { title: 'Produto', class: 'nowrap' },
            { title: 'Descricao', class: 'nowrap' },
            { title: 'Grupo', class: 'nowrap' },
            { title: 'Saldo Estoque' },
            { title: 'CMM' },
            { title: 'PREÇO' },
            { title: 'Cod Cliente', class: 'nowrap' },
            { title: 'Nome', class: 'nowrap' },
            { title: 'Email' },
            { title: 'Media Compras' },
            { title: 'Compras Mes Atu' },
        ],
        ajax: {
            url: '/utilitarios/calculo-vendas/getCalculo',
            type: 'post',
            data: {
                unidadeVendas: $(unidadeVendas).val() ,
                segmento: $(segmento).val() ,
                classe: $(classe).val(),
                sistema: $(sistema).val(),
                carac1: $(carac1).val(),
                matPrima: $(matPrima).val(),
                carac2: $(carac2).val(),
                estoqueMinimo: $(estoqueMinimo).val(),
                codigoProduto: $(codigoProduto).val(),
                descricaoProduto: $(descricaoProduto).val(),
                numClientesProduto: $(numClientesProduto).val(),
                grupo: $(grupo).val(),
            },
        },
    });
}