$(document).ready(() => {
	$('#myTable').DataTable({
        dom: 'Blpftip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
		ajax: {
			type: 'post',
		},
		columns: [
			{ title: 'Tipo' },
			{ title: 'Numero Pedido' },
			{ title: 'It' },
			{ title: 'Cliente' },
			{ title: 'Estado' },
			{ title: 'Emissao' },
			{ title: 'Entrega' },
			{ title: 'Produto' },
			{ title: 'Descricao' },
			{ title: 'Grupo' },
			{ title: 'Qtd Vend' },
			{ title: 'Qtd Ent' },
			{ title: 'Saldo' },
			{ title: 'Preço Venda' },
			{ title: 'Valor Total' },
			{ title: 'Valor Saldo' },
		],
		aaSorting: [
			//[ 7, 'desc' ],
		]
	});
});