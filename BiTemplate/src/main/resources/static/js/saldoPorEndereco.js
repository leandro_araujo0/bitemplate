$(document).ready(function() {
	$('#produtoDe').change(function() {
		$('#produtoAte').val($('#produtoDe').val());
	});
	
	$('#loteDe').change(function() {
		$('#loteAte').val($('#loteDe').val());
	});
	
	var table = $('#myTable').DataTable({
		iDisplayLength: 100,
		columns: [
			{ title: 'Produto' },
			{ title: 'Descricao' },
			{ title: 'UM' },
			{ title: 'Armazem' },
			{ title: 'Arma Padr' },
			{ title: 'Lote' },
			{ title: 'Data Entrada' },
			{ title: 'Endereco' },
			{ title: 'Quantidade' },
			{ title: 'Empenho' },
			{ title: 'Saldo' },
			{ title: 'Validade Lote' },
		],
		columnDefs: [
            {
	         },
		],
		ajax: {
			type: 'POST'
		},
		dom: 'Blpftip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
	});
});