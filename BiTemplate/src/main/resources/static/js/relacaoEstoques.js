$(document).ready(function() {
	$.post('', function(table) {
		var headers = [];
		$(table.headers).each(function(index, header) {
			headers.push( { title: header} );
		});
		
		var table = $('#myTable').DataTable({
			columns: headers,
			data: table.data,
			iDisplayLength: 1000,
	        buttons: [
	            'copyHtml5',
	            'excelHtml5',
	        ],
			dom: 'Blpftip',
			initComplete: function() {
				$('.spinner').remove();
			},
		});
	});
});