$(document).ready(function () {
    $.get('/indicadores/estoques/graficoAlmoxarifadoOrtopedia', function (chartData) {
        var ctx = document.getElementById('graficoAlmoxarifadoOrtopedia').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.legend.display = false;
        chart.options.scales.yAxes[0].ticks.callback = function (value, index, values) {
            return formatCurrency(value);
        }
        chart.options.tooltips.callbacks.label = function (label) {
            return formatCurrency(label.yLabel);
        }
        chart.options.onClick = function (evt, item) {
        	abrirLink(evt, '/indicadores/estoques/analitico?unidade=ORTOPEDIA&local=10');
        };
        chart.update();
    });


    $.get('/indicadores/estoques/graficoAlmoxarifadoEquipamentos', function (chartData) {
        var ctx = document.getElementById('graficoAlmoxarifadoEquipamentos').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.legend.display = false;
        chart.options.scales.yAxes[0].ticks.callback = function (value, index, values) {
            return formatCurrency(value);
        }
        chart.options.tooltips.callbacks.label = function (label) {
            return formatCurrency(label.yLabel);
        }
        chart.options.onClick = function (evt, item) {
        	abrirLink(evt, '/indicadores/estoques/analitico?unidade=EQUIPAMENTOS&local=11');
        };
        chart.update();
    });


    $.get('/indicadores/estoques/graficoEstoque', 'unidade=ORTOPEDIA', function (chartData) {
        var ctx = document.getElementById('graficoEstoqueOrtopedia').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.legend.display = false;
        chart.options.scales.xAxes[0].ticks.callback = function (value, index, values) {
            return formatCurrency(value);
        }
        chart.options.tooltips.callbacks.label = function (label) {
            return formatCurrency(label.xLabel);
        }
        chart.options.onClick = function (evt, item) {
        	abrirLink(evt, '/indicadores/estoques/analitico?unidade=ORTOPEDIA&local=01');
        };
        chart.options.scales.xAxes[0].ticks.beginAtZero = true;
        chart.update();
    });

    $.get('/indicadores/estoques/graficoEstoque', 'unidade=EQUIPAMENTOS', function (chartData) {
        var ctx = document.getElementById('graficoEstoqueEquipamentos').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.legend.display = false;
        chart.options.scales.xAxes[0].ticks.callback = function (value, index, values) {
            return formatCurrency(value);
        }
        chart.options.tooltips.callbacks.label = function (label) {
            return formatCurrency(label.xLabel);
        }
        chart.options.onClick = function (evt, item) {
        	abrirLink(evt, '/indicadores/estoques/analitico?unidade=EQUIPAMENTOS&local=12');
        };
        chart.options.scales.xAxes[0].ticks.beginAtZero = true;
        chart.update();
    });


    $.get('/indicadores/estoques/graficoProdutosAntigosOrtopedia', function (chartData) {
        var ctx = document.getElementById('graficoProdutosAntigosOrtopedia').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.legend.display = false;
        chart.update();
    });
});