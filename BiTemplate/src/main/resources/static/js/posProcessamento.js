var table;

$(document).ready(function () {
    addKeyListener();
    
    $('#button-form').click(function () {
    	if ($('#button-form').hasClass('disabled')) {
    		return;
    	}
    	$('#button-form').html("Processando...");
    	$('#button-form').addClass('disabled');
    	
    	
        var codigoProdutos = $('.codigoProduto').map(function () { return $(this).val() }).toArray();
        
        if (table != null) {
            table.destroy();
        }
        
        table = $('#myTable').DataTable({
            dom: 'Blpftip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
            ],
            ajax: {
                url: '/utilitarios/pos-processamento/processar',
                type: 'POST',
                data: {
                    codigoProdutos: codigoProdutos,
                },
            },
            columns: [
                { title: 'Produto' },
                { title: 'Desc' },
                { title: 'Qtd Estq' },
                { title: 'Qtd Emp' },
                { title: 'Qtd Sc' },
                { title: 'Qtd Pc' },
                { title: 'Qtd Op' },
                { title: 'Num Sc' },
                { title: 'Dt Sc' },
                { title: 'Qtd Sc' },
                { title: 'Sld Sc' },
                { title: 'Num Pc' },
                { title: 'Dt Pc' },
                { title: 'Qtd Pc' },
                { title: 'Sld Pc' },
                { title: 'Fornecedor' },
                { title: 'Num Op' },
                { title: 'Dt Op' },
                { title: 'Qtd Op' },
                { title: 'Sld Op' },
            ],
            initComplete: function() {
            	$('#button-form').removeClass('disabled');
                $('#button-form').html('Processar');
            },
        });
    });
    
    document.getElementsByClassName('codigoProduto')[0].addEventListener('paste', (e) => {
        e.preventDefault();
        const text = (e.originalEvent || e).clipboardData.getData('text/plain');
        window.document.execCommand('insertText', false, text);
        var linhas = text.split('\r\n');

        $(linhas).each(function(index, item) {
        	if (this != '') {
        		$('.codigoProduto').last().val(this.split('	')[0]);
        		if (index != linhas.length - 1) {
        			adicionarProduto();
        		}
            }
        });
        removerProduto();
    });
});

function alteraClasse(selector, classe) {
    $(selector).toggleClass(classe);
}

function removerProduto() {
    $('.form-produto').not(':first').last().remove();
}

function removerProdutos() {
    $('.form-produto').not(':last').remove();
}

function addKeyListener() {
    $('.form-row').find('input').keydown(function () {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == 13) {
            processar();
            $('#button-form').focus();
        }
    });

    $('.qtdBase').last().keydown(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (!event.shiftKey && keycode == 9) {
            adicionarProduto();
            event.preventDefault();
        }
        if (keycode == 13) {
            processar();
            $('#button-form').focus();
        }
        $('.qtdBase').not(':last').off('keydown');
    });
}

function adicionarProduto() {
    $('.form-produto').last().after($('.form-produto').last().clone());
    $('.form-produto').last().find('input').val('');
    $('.codigoProduto').last().focus();
    addKeyListener();
}