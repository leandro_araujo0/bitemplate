$(document).ready( function () {
	$('#emissao').datepicker({
		dateFormat: 'yymmdd',
		changeMonth: true,
	    changeYear: true,
	    yearRange: '2014:'+(new Date).getFullYear(),
	});
	
    var table = $('#myTable').DataTable({
    	ajax: {
    		type: "POST",
    	},
    	columns: [
    		{ title: 'Num Solicitação' },
    		{ title: 'Solicitante', class: 'nowrap' },
    		{ title: 'Produto', class: 'nowrap' },
    		{ title: 'Descricao', class: 'nowrap' },
    		{ title: 'Armazem' },
    		{ title: 'Quantidade' },
    		{ title: 'Quant em Pedido' },
    		{ title: 'Emissao' },
    		{ title: 'Necessidade' },
    		{ title: 'Comprador' },
    		{ title: 'Cod Fornecedor' },
    		{ title: 'Nome Fornecedor' }
    	],
        columnDefs: [
        ],
    	dom: 'Blpftip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
        initComplete: function() {
        },
    });
} );
