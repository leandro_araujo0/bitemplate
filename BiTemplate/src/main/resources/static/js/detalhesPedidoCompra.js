$(document).ready( function () {
    var table = $('#myTable').DataTable({
    	ajax: {
    		type: "POST",
    	},
    	columns: [
    		{ title: 'Numero Pedido' },
    		{ title: 'Numero SC' },
    		{ title: 'Solicitante' },
    		{ title: 'Fornecedor' },
    		{ title: 'Item PC' },
    		{ title: 'Produto' },
    		{ title: 'Descricao' },
    		{ title: 'OP' },
    		{ title: 'Lote' },
    		{ title: 'Emissao' },
    		{ title: 'Data Entrega' },
    		{ title: 'Qtd' },
    		{ title: 'Qtd Ent' },
    		{ title: 'Qtd Pend' },
    	],
    	dom: 'Blpftip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
        initComplete: function() {
        },
    });
} );
