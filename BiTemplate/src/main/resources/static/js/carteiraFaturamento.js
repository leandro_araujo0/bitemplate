var tabelaPedidos;
var tabelaEmpenhos;
var mes;
var botaoMes;
var pedido;
var it;
var produto;
var descricao;
var statusPedidoSelecionado;
var dataUltAtualiz;
var localiz;
var dtExp;

function geraProdutos(data) {
	$(data).each(function() {
		var codigo = this.produto.codigo;
		var descricao = this.produto.descricao;
		var checkProduto = '';
		checkProduto += '<div class="form-check">';
		checkProduto += '	<input class="form-check-input" type="checkbox" value="' + this.pedido.numero + ';' + this.item + '" onClick="habilitarBotaoAdiciona()"/>';
		checkProduto += '	<label>' + this.item + ' ' + codigo + ' - ' + descricao + 'Qtd Ven: ' + this.qtdVen + '/ Qtd Ent: ' + this.qtdEnt + '</label>';
		checkProduto += '</div>';
		$('.form-checks').append(checkProduto);
	});
	$('#salvarPedido').removeClass('d-none');
}

function atualizaStatus() {
	$('#tabelaPedidos tr').not(':first').each(function() {
		var qtdVen = $(this).find('.qtdVen').html();
		var qtdEmp = $(this).find('.qtdEmp').html();
		var qtdEnt = $(this).find('.qtdEnt').html();
		
		
		if (qtdEnt === qtdVen) {
			$(this).addClass('pedido-faturado');
		} else if(qtdEmp === qtdVen) {
			$(this).addClass('pedido-empenhado');
		} else {
			$(this).addClass('pedido-pendente');
		}
	});
}

function habilitarBotaoAdiciona() {
	$('#salvarPedido').addClass('d-none');
	$('.form-check-input').each(function() {
		if (!$('#salvarPedido').hasClass('d-none')) {
			return;
		}
		if (this.checked) {
			$('#salvarPedido').removeClass('d-none');
		} else {
			$('#salvarPedido').addClass('d-none');
		}
	});
}

function fecharMemoDiv() {
	var pedidoFaturamento = {
			numeroPedido: pedido,
			itemPedido: it,
			status: $('.div-memo .text-status').html(),
			ultAtualiz: new Date().toLocaleDateString(),
			localizacao: $('.div-memo .text-localiz').val(),
			dtExp: $('.div-memo .text-dt-exp').html(),
	};
	$.ajax({
		url: '/carteira-faturamento/salvar',
		type: 'POST',
		success: function() {
			$(statusPedidoSelecionado).html(pedidoFaturamento.status);
			$(dataUltAtualiz).html(pedidoFaturamento.ultAtualiz);
			$(localiz).html(pedidoFaturamento.localizacao);
			$(dtExp).html(pedidoFaturamento.dtExp);
			$('.div-memo').addClass('d-none');
			$('.div-memo-background').addClass('d-none');
			tabelaPedidos.ajax.reload(atualizaStatus);
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('error');
		},
		processData: false,
		contentType : 'application/json; charset=utf-8',
		data: JSON.stringify(pedidoFaturamento),
	});
}

function atualizaEmpenhos() {
	if (tabelaEmpenhos != null) {
		tabelaEmpenhos.destroy();
	}
	
	tabelaEmpenhos = $('#tabelaEmpenhos').DataTable({
		ajax: {
			url: '/carteira-faturamento/getEmpenhosPedidos',
			type: 'POST',
			data: {
				mes: mes,
				ano: '2020',
			},
		},
		columns: [
			{ title: 'NUM OP' },						//1
			{ title: 'PRODUTO OP' },					//1
			{ title: 'DESCRICAO', class: 'nowrap' },	//3
			{ title: 'EMISSAO' },						//4
			{ title: 'USUARIO', class: '' },			//5
			{ title: 'COD EMPENHO' },					//6
			{ title: 'DESCRICAO', class: 'nowrap' },	//7
			{ title: 'UM' },							//8
			{ title: 'QTD ORIGINAL' },					//9
			{ title: 'Qtd Emp' },						//10
			{ title: 'LOTE' },							//11
			{ title: 'Armz' },							//12
			{ title: 'OBS' },							//13
			{ title: 'Qtd Estoque' },					//14
            { title: 'Qtd Enderecar' },					//15
            { title: 'Qtd Inspecao' },					//16
            { title: 'Status' },						//17
            { title: 'Total Emp' },						//18
            { title: 'Qtd OP' },						//19
            { title: 'Qtd SC' },						//20
            { title: 'Qtd PC' },						//21
            { title: 'Saldo' },							//22
            { title: 'Pedido Venda' },					//23
        ],
        dom: 'Blpftp',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
        buttons: [
        	{
                extend: 'copy',
                exportOptions: {
                }
            },
            {
                extend: 'excel',
                exportOptions: {
                	orthogonal: 'export',
                },
            }
        ],
	});
}

$(document).ready(function() {
	
	$('.context-menu .context-link').click(function(e) {
		console.log(e);
	});
	
	
	$('.btn-fechar').click(function() {
		fecharMemoDiv();
	});
	
	$('#tabelaPedidos').on('contextmenu', 'tr', function(e) {
		e.preventDefault();
		var x = e.originalEvent.x;
		var y = e.originalEvent.y;
		
		var pedido = this.cells[0].innerHTML;
		var item = this.cells[1].innerHTML;
		var tr = this;
		
		var opcoes = [];
		
		opcoes.push(
			{
				nome: 'Remover Item',
				link: '/carteira-faturamento/remover?pedido=' + pedido + '&item=' + item,
			},
			{
				nome: 'Remover Pedido',
				link: '/carteira-faturamento/remover?pedido=' + pedido,
			},
			{
				nome: 'Acompanhamento/pcp',
				link: '/carteira-pcp/' + pedido + '/' + item + '/acompanhamento',
			},
			{
				nome: 'Fixar Pedido',
				link: '/carteira-pcp/adicionar-manual/' + pedido + '/' + item,
			},
			{
				nome: 'Fixar Todos',
				link: '#',
				callback: "function() {" +
					"var itens = [];" +
					"$('#tabelaPedidos tr').each(function() {" +
						"var numPedido = $(this).find('.num-pedido').html();" +
						"var itemPedido = $(this).find('.item-pedido').html();" +
						
						"if (numPedido == 'Num Ped' || itens.includes(numPedido+itemPedido)) {" +
							"return" +
						"}" +
						"itens.push(numPedido+itemPedido);" +
						"window.open('/carteira-pcp/adicionar-manual/' + numPedido + '/' + itemPedido, '_blank');" +
					"});" +
				"}",
			},
			{
//	        	{ title: 'Num Ped', class: 'num-pedido' },
//	        	{ title: 'It', class: 'item-pedido' },
				
				
				nome: 'Abrir Todos',
				link: '#',
				callback: "function() {" +
					"var itens = [];" +
					"$('#tabelaPedidos tr').each(function() {" +
						"var numPedido = $(this).find('.num-pedido').html();" +
						"var itemPedido = $(this).find('.item-pedido').html();" +
						
						"if (numPedido == 'Num Ped' || itens.includes(numPedido+itemPedido)) {" +
							"return" +
						"}" +
						"itens.push(numPedido+itemPedido);" +
						"window.open('http://192.168.0.5/carteira-pcp/' + numPedido + '/' + itemPedido + '/acompanhamento', '_blank');" +
					"});" +
				"}",
			},
		);
		
		criarContextMenu(x, y, opcoes);
	});
	
	$('#tabelaPedidos').on('dblclick', 'tr', function(e) {
		e.preventDefault();
		
		$('body').append('<div class="div-memo-background" onClick="fecharMemoDiv()"></div>');
		pedido = $(this).find('.num-pedido').html();
		it = $(this).find('.item-pedido').html();
		produto = $(this).find('.produto').html();
		descricao = $(this).find('.descricao').html();
		statusPedidoSelecionado = $(this).find('.memo');
		dataUltAtualiz = $(this).find('.dt-ult-at');
		localiz = $(this).find('.localiz');
		dtExp = $(this).find('.dt-exp');
		
		var pf;
		$.get('/carteira-faturamento/getPf/' + pedido + '/' + it, function(tempPf) {
			pf = tempPf;
			$('.div-memo .text-status').html(pf.status);
			$('.div-memo .text-localiz').val($(localiz).html());
			$('.div-memo .text-dt-exp').html($(dtExp).html()),
			$('.div-memo').removeClass('d-none');
			$('.div-memo .text-status').focus();
		});
		
		$(document).keydown(function(e) {
			if (e.keyCode == '27') {
				fecharMemoDiv();
			}
		});
	});
	
	$('#adicionarPedido').click(function() {
		$('.form-check').remove();
		$('#numPedido').val('');
		$('#salvarPedido').addClass('d-none');
		
		
		$('#div-adicionar-pedido').removeClass('d-none');
		$('#numPedido').focus();
	});
	
	
	$('#salvarPedido').click(function() {
		var pedidos = [];
		$('.form-check-input').each(function() {
			if (this.checked) {
				pedidos.push(this.value + ';' + mes);
			}
		});
		
		$.post('/carteira-faturamento/adicionar', {pedidos: pedidos}, function(success) {
			if (success) {
				$('#div-adicionar-pedido').addClass('d-none');
				$(botaoMes).click();
			}
		});
		
	});
	
	
	$('#numPedido').blur(function() {
		var numPedido = $(this).val();
		$.post('/carteira-faturamento/getItensPedido', {numPedido: numPedido}, function(data) {
			geraProdutos(data);
		});
	});
	
	
	$('.num-mes').click(function() {
		$('.num-mes').parent().removeClass('selected');
		botaoMes = $(this);
		
		mes = $(this).attr('data-value');
		$('#adicionarPedido').removeClass('disabled');
		$(this).parent().addClass('selected');
		
		if (tabelaPedidos != null) {
			tabelaPedidos.destroy();
		}
		
		tabelaPedidos = $('#tabelaPedidos').DataTable({
			ajax: {
				type: 'POST',
				data: {
					ano: '2021',
					mes: mes,
				},
			},
			dom: 'Blpftp',
			iDisplayLength: 10000,
	        buttons: [
	            'copyHtml5',
	            'excelHtml5',
	        ],
	        columns: [
	        	{ title: 'Num Ped', class: 'num-pedido' },
	        	{ title: 'It', class: 'item-pedido' },
	        	{ title: 'Nome Cli' },
	        	{ title: 'Dt Lib' },
	        	{ title: 'Produto', class: 'produto nowrap' },
	        	{ title: 'Descricao', class: 'descricao' },
	        	{ title: 'Dt Atuali.', class: 'dt-ult-at' },
	        	{ title: 'Processo', class: 'localiz' },
	        	{ title: 'Status', class: 'memo' },
	        	{ title: 'qtdVen', class: 'qtdVen' },
	        	{ title: 'qtdEmp', class: 'qtdEmp' },
	        	{ title: 'qtdEnt', class: 'qtdEnt' },
	        	{ title: 'Dt Exped.', class: 'dt-exp' },
	        ],
	        initComplete: function() {
//	        	atualizaEmpenhos();
	        	atualizaStatus();
	        },
	        columnDefs: [
	            {
	                render: function(data, type, row) {
	                	var maxLen = 80;
	                    if (type === 'display') {
	                    	if (data != null && data.length > maxLen && !data.includes('</a>')) {
	                    		return data.substring(0, maxLen) + '...';
	                    	} else {
	                    		return data;
	                    	}
	                    } else {
	                        return data;
	                    }
	                },
	                targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
	            }
	        ],
		});
	});
});