$(document).ready(function() {
	$('#todosEstoques, .popup .content').hover(function() {
		$('.popup').removeClass('d-none');
	}, function() {
		$('.popup').addClass('d-none');
	});
	
	
	$('#codigo').keypress(function(e) {
	    if (e.keyCode == 13) {
	    	window.location.href = "/historico-produto?codigoProduto=" + this.value;
	        e.preventDefault();
	    }
	});
	
	codigo.select();
	
	var target = $('#target').val();
	if (target === 'blank')
		return;
	var tableUltimasOps = $('#tableUltimasOps').DataTable({
		ajax: {
			type: 'post',
			url: '/historico-produto/ultimas-ops',
			data: {codigoProduto: codigoProduto.value},
		},
		dom: 'Bpt',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
    	aaSorting: [[0, 'desc']],
		columns: [
			{ title: 'Numero' },
			{ title: 'Qtd' },
			{ title: 'Emissao' },
			{ title: 'PC' },
			{ title: 'Fornecedor' },
		],
	});
	var tableUltimosPcs = $('#tableUltimosPcs').DataTable({
		ajax: {
			type: 'post',
			url: '/historico-produto/ultimos-pcs',
			data: {codigoProduto: codigoProduto.value},
		},
		dom: 'Bpt',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],		
    	aaSorting: [[0, 'desc']],
		columns: [
			{ title: 'Numero' },
			{ title: 'Emissao' },
			{ title: 'Fornecedor' },
			{ title: 'Preco' },
			{ title: 'Quantidade' },
			{ title: 'Lead Time' },
		],
	});
	var tableOndeEUsado = $('#tableOndeEUsado').DataTable({
		ajax: {
			type: 'post',
			url: '/historico-produto/onde-e-usado',
			data: {codigoProduto: codigoProduto.value},
		},
		dom: 'Bpt',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
    	aaSorting: [[5, 'desc']],
		columns: [
			{ title: 'Produto' },
			{ title: 'Descricao' },
			{ title: 'Tipo' },
			{ title: 'Qtd' },
			{ title: 'Opc' },
			{ title: 'Qtd Pv' },
		],
	});
});