$(document).ready(function() {
	$('form').submit(function() {
		var isValid = $('#codigoProduto').hasClass('valid');
		
		if (!isValid) {
          event.preventDefault();
          event.stopPropagation();
        }
	});
	
      
	
	$('#btn-cancelar').click(function() {
		$('.adicionar-produto').addClass('d-none');
	});
	
	$('#btn-adicionar').click(function() {
		$('#codigoProduto').val('');
		$('.adicionar-produto').removeClass('d-none');
		$('#codigoProduto').focus();
	});
	
	$('#codigoProduto').blur(function() {
		validaProduto(this.value);
	});
	
	
	var table = $('#myTable').DataTable({
		dom: 'Blpftip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
        iDisplayLength: 10000,
		ajax: {
			type: 'POST',
		},
		columns: [
			//B1_COD, B1_DESC, C1_NUM, C1_QUANT, C1_QUJE, C1_EMISSAO, C7_NUM, C7_QUANT, C7_QUJE, C7_EMISSAO, A2_NOME, C2_NUM, C2_QUANT, C2_QUJE, C2_PERDA
			{ title: 'Produto' },
			{ title: 'Descricao' },
			{ title: 'Qtd Empenho' },
			{ title: 'Qtd Estoque' },
			{ title: 'Qtd Enderecar' },
			{ title: 'Qtd Inspecao' },
			{ title: 'Num SC' },
			{ title: 'Qtd Sol' },
			{ title: 'Qtd Ent' },
			{ title: 'Emissao' },
			{ title: 'Num PC' },
			{ title: 'Qtd Ped' },
			{ title: 'Qtd Ent' },
			{ title: 'Emissao' },
			{ title: 'Forn' },
			{ title: 'Num OP' },
			{ title: 'Qtd OP' },
			{ title: 'Qtd Fin' },
			{ title: 'Qtd Perd' },
			{ title: 'Memorando' },
		],
	});
});

function validaProduto(produto) {
	$.get('/produtos/valida?codigoProduto=' + produto, function(data) {
		if (data) {
			$('#codigoProduto').addClass('valid');
			$('#codigoProduto').removeClass('invalid');
		} else {
			$('#codigoProduto').addClass('invalid');
			$('#codigoProduto').removeClass('valid');
		}
	});
}