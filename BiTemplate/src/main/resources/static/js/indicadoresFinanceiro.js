$(document).ready(function () {
    $('canvas').after('<div class="spinner"></div>');
    atualizarGraficos();
});

function atualizarGraficos() {
    $.get('/indicadores/financeiro/graficoBaixasAnualOrtopedia', function (chartData) {
        var ctx = document.getElementById('graficoBaixasAnualOrtopedia').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.scales.yAxes[0].ticks.callback = function (value) {
            return formatCurrency(value);
        }
        chart.options.tooltips.callbacks.label = function (label) {
            return formatCurrency(label.yLabel);
        }
        chart.options.scales.yAxes[0].ticks.beginAtZero = true;
        chart.options.legend.display = false;
        chart.options.scales.xAxes[0].ticks.maxRotation = 90;
        chart.options.scales.xAxes[0].ticks.minRotation = 90;
        chart.update();
        $('#graficoBaixasAnualOrtopedia').next().addClass('d-none');
    });


    $.get('/indicadores/financeiro/graficoBaixasAnualEquipamentos', function (chartData) {
        var ctx = document.getElementById('graficoBaixasAnualEquipamentos').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.scales.yAxes[0].ticks.callback = function (value) {
            return formatCurrency(value);
        }
        chart.options.tooltips.callbacks.label = function (label) {
            return formatCurrency(label.yLabel);
        }
        chart.options.scales.yAxes[0].ticks.beginAtZero = true;
        chart.options.legend.display = false;
        chart.options.scales.xAxes[0].ticks.maxRotation = 90;
        chart.options.scales.xAxes[0].ticks.minRotation = 90;
        chart.update();
        $('#graficoBaixasAnualEquipamentos').next().addClass('d-none');
    });


    $.get('/indicadores/financeiro/graficoBaixasAnualSemClas', function (chartData) {
        var ctx = document.getElementById('graficoBaixasAnualSemClas').getContext('2d');
        var chart = new Chart(ctx, chartData);
        chart.options.scales.yAxes[0].ticks.callback = function (value) {
            return formatCurrency(value);
        }
        chart.options.tooltips.callbacks.label = function (label) {
            return formatCurrency(label.yLabel);
        }
        chart.options.scales.yAxes[0].ticks.beginAtZero = true;
        chart.options.legend.display = false;
        chart.options.scales.xAxes[0].ticks.maxRotation = 90;
        chart.options.scales.xAxes[0].ticks.minRotation = 90;
        chart.update();
        $('#graficoBaixasAnualSemClas').next().addClass('d-none');
    });
	
	var tabelaBaixasOrtopedia = $('#tabelaBaixasOrtopedia').DataTable({
    	ajax: {
    		url: '/indicadores/financeiro/getTabelaBaixas',
    		type: 'post',
    		data: {
    			unidade: 'ORTOPEDIA',
    		},
    	},
    	aaSorting: [[13, 'desc']],
		columnDefs: [
			{
				render: function(data, type, row) {
			        if (type === 'display' || type === 'filter')
			                return formatCurrency(data);
			            else
			                return data;
		        },
		        targets: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
		    }
		],
		dom: 'Blpftip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
    	columns: [
    		{ title: 'Benef' },
    		{ title: 'Janeiro' },
    		{ title: 'Fevereiro' },
    		{ title: 'Marco' },
    		{ title: 'Abril' },
    		{ title: 'Maio' },
    		{ title: 'Junho' },
    		{ title: 'Julho' },
    		{ title: 'Agosto' },
    		{ title: 'Setembro' },
    		{ title: 'Outubro' },
    		{ title: 'Novembro' },
    		{ title: 'Dezembro' },
    		{ title: 'Total' },
    	],
    });
	
	var tabelaBaixasOrtopedia = $('#tabelaBaixasEquipamentos').DataTable({
    	ajax: {
    		url: '/indicadores/financeiro/getTabelaBaixas',
    		type: 'post',
    		data: {
    			unidade: 'EQUIPAMENTOS',
    		},
    	},
    	aaSorting: [[13, 'desc']],
		columnDefs: [
			{
				render: function(data, type, row) {
			        if (type === 'display' || type === 'filter')
			                return formatCurrency(data);
			            else
			                return data;
		        },
		        targets: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
		    }
		],
		dom: 'Blpftip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
    	columns: [
    		{ title: 'Benef' },
    		{ title: 'Janeiro' },
    		{ title: 'Fevereiro' },
    		{ title: 'Marco' },
    		{ title: 'Abril' },
    		{ title: 'Maio' },
    		{ title: 'Junho' },
    		{ title: 'Julho' },
    		{ title: 'Agosto' },
    		{ title: 'Setembro' },
    		{ title: 'Outubro' },
    		{ title: 'Novembro' },
    		{ title: 'Dezembro' },
    		{ title: 'Total' },
    	],
    });
	
	var tabelaBaixasOrtopedia = $('#tabelaBaixasNClass').DataTable({
    	ajax: {
    		url: '/indicadores/financeiro/getTabelaBaixas',
    		type: 'post',
    		data: {
    			unidade: 'N_CLASS',
    		},
    	},
    	aaSorting: [[13, 'desc']],
		columnDefs: [
			{
				render: function(data, type, row) {
			        if (type === 'display' || type === 'filter')
			                return formatCurrency(data);
			            else
			                return data;
		        },
		        targets: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
		    }
		],
    	dom: 'Blpftip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
    	columns: [
    		{ title: 'Benef' },
    		{ title: 'Janeiro' },
    		{ title: 'Fevereiro' },
    		{ title: 'Marco' },
    		{ title: 'Abril' },
    		{ title: 'Maio' },
    		{ title: 'Junho' },
    		{ title: 'Julho' },
    		{ title: 'Agosto' },
    		{ title: 'Setembro' },
    		{ title: 'Outubro' },
    		{ title: 'Novembro' },
    		{ title: 'Dezembro' },
    		{ title: 'Total' },
    	],
    });
}