$(document).ready(() => {
	$('#myTable').DataTable({
        dom: 'Blpftip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
		ajax: {
			type: 'post',
		},
		columns: [
			{ title: 'Tipo' },
			{ title: 'Emissao' },
			{ title: 'Pedido' },
			{ title: 'NF' },
			{ title: 'Cliente' },
			{ title: 'Nome' },
			{ title: 'Produto' },
			{ title: 'Descricao' },
			{ title: 'Lote' },
			{ title: 'Qtd' },
			{ title: 'Prc Ven' },
			{ title: 'Total' },
			{ title: 'Est' },
			{ title: 'Armazem' },
			{ title: 'CFOP' },
			{ title: 'Mens Nota', class: 'd-none'},
			{ title: 'Grupo' },
			{ title: 'Ano' },
			{ title: 'Mes' },
		],
		aaSorting: [
			[ 7, 'desc' ],
		]
	});
});