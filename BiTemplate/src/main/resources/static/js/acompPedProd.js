function allowDrop(ev) {
  ev.preventDefault();
}

function drag(ev) {
  ev.dataTransfer.setData("pedido", $(ev.target).find('.num-ped').html());
}

function drop(ev) {
  ev.preventDefault();
  var data = ev.dataTransfer.getData("pedido");
  //ev.target.appendChild(document.getElementById(data));
  
  //do something!!!
  if (!$(ev.target).hasClass('unidade')) {
	  $(ev.target).parents('.unidade').append($('#' + ev.dataTransfer.getData("pedido")).parent());
  } else {
	  $(ev.target).append($('#' + ev.dataTransfer.getData("pedido")).parent());
  }
}

$(document).ready(function(){
    $('.unidade').bind('dragover', function(){
        $(this).addClass('drag-over');
    });
    $('.unidade').bind('dragleave', function(){
        $(this).removeClass('drag-over');
    });
});