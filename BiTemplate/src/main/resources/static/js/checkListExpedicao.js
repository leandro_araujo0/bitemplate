$(document).ready(function() {
	$('#data-emissao').html(new Date().toLocaleDateString('pt-br'));
	
	var table = $('#myTable').DataTable({
		dom: '',
		iDisplayLength: 1000,
	});
	
	var timeout = null
	$('#numPedido').on('keyup', function() {
	    var text = this.value
	    clearTimeout(timeout)
	    timeout = setTimeout(function() {
			if (text.length == 6) {
				$('#documento').html('');
				$.get('/check-list-expedicao/' + text, (nfsPedido) => {
					$(nfsPedido).each(function () {
						var option = new Option(this, this);
						var select = document.getElementById('documento');
						select.add(option);
					});
				});
			} else if (text.length > 6) {
				$(this).val(text.substring(0, 6));
			}
	    }, 500)
	})
});