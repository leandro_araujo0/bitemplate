var table;

$(document).ready(function() {
	$(window).resize(function() {
		var mapa = $(mapaBrasil);
		var tabela = $(myTable);
		var mapaBrasilLeft = $(mapa).position().left;
		var tabelaLeft = $(tabela).position().left;
		var tabelaWidth = $(tabela).width();
		
		if ($(indicadores).position().left + $(indicadores).width() - $(myTable).position().left - $(myTable).width() < 656) {
			$(mapa).addClass('d-none');
		} else {
			$(mapa).removeClass('d-none');
		}
	});
	
	
	
    $(btnGerarMapa).click(function() {
        if (table != null)
            table.destroy();
        table = $(myTable).DataTable({
        	iDisplayLength: 100,
            columnDefs: [
                {
                    render: function(data, type, row) {
                        if (type === 'display' || type === 'filter')
                            return formatCurrency(data);
                        else
                            return data;
                    },
                    targets: [2, 3, 4, 5, 6],
                }
            ],
            dom: 'Blpftip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
            ],
            ajax: {
                url: '/indicadores/mapa-vendas/getMapa',
                type: 'get',
                data: {
                    unidade: unidade.value,
                },
            },
            columns: [
                { title: 'Estado' },
                { title: 'Cliente', class: 'nowrap' },
                { title: 'Vendas 2017', class: 'nowrap currency' },
                { title: 'Vendas 2018', class: 'nowrap currency' },
                { title: 'Vendas 2019', class: 'nowrap currency' },
                { title: 'Vendas 2020', class: 'nowrap currency' },
                { title: 'Vendas 2021', class: 'nowrap currency' },
            ],
			aaSorting: [ 
				[ 5, 'desc' ],
			],
            initComplete: function() {
                $('#myTable tfoot').removeClass('d-none');

                $('#myTable tfoot th').each( function () {
                    var title = $(this).text();
                    if (title != '')
                    $(this).html( '<input class="w-100" type="text" placeholder="'+title+'" />' );
                });

                table.columns().every( function () {
                    var that = this;
                    $( 'input', this.footer() ).on( 'keyup change', function () {
                        if ( that.search() !== this.value ) {
                            that.search( this.value )
                                .draw();
                        }
                    });
                });
                
                $(window).resize();
            },
        });
    });
});