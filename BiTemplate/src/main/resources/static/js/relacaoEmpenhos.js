$(document).ready(function() {
	var table = $('#myTable').DataTable({
		iDisplayLength: 10000,
		columns: [
			{ title: 'NUM OP' },						//1
			{ title: 'PRODUTO OP', class: 'context-item' },					//1
			{ title: 'DESCRICAO', class: 'nowrap' },	//3
			{ title: 'EMISSAO' },						//4
			{ title: 'USUARIO', class: '' },			//5
			{ title: 'COD EMPENHO' },					//6
			{ title: 'DESCRICAO', class: 'nowrap' },	//7
			{ title: 'UM' },							//8
			{ title: 'QTD ORIGINAL' },					//9
			{ title: 'Qtd Emp' },						//10
			{ title: 'LOTE' },							//11
			{ title: 'Armz' },							//12
			{ title: 'OBS' },							//13
			{ title: 'Qtd Estoque' },					//14
            { title: 'Qtd Enderecar' },					//15
            { title: 'Qtd Inspecao' },					//16
            { title: 'Status' },						//17
            { title: 'Total Emp' },						//18
            { title: 'Qtd OP' },						//19
            { title: 'Qtd SC' },						//20
            { title: 'Qtd PC' },						//21
            { title: 'Saldo' },							//22
		],
//		columnDefs: [
//            {
//	             "targets": [ 8, 10, 12 ],
//	             "visible": false,
//	             "searchable": false
//	         },
//		],
		ajax: {
			type: 'POST'
		},
		dom: 'Blpftip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
        initComplete: function() {
        	$('.context-item').mousedown(function (e) {
        		e.preventDefault();
        		if (e.which == 3) {
        			var codigoProduto = this.innerHTML;
        			
        			var x = e.originalEvent.x;
        			var y = e.originalEvent.y;
        			
        			var opcoes = [];
        			
        			opcoes.push(
        				{
        					nome: 'Historico Produto',
        					link: '/historico-produto?codigoProduto=' + codigoProduto,
        				},
        			);
        			
        			criarContextMenu(x, y, opcoes);
        		}
        	});
        }
	});
});