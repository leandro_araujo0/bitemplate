var graficoRecebimentoMensalOrtopedia;
var graficoRecebimentoMensalEquipamentos;
var graficoRecebimentoMensalExportacao;
var graficoInadimplenciaMensalTotal;
var graficoInadimplenciaMensalOrtopedia;
var graficoInadimplenciaMensalEquipamentos;
var graficoInadimplenciaMensalExportacao;
var aReceberTotal;
var aReceberTotalOrtopedia;
var aReceberTotalEquipamentos;
var aReceberTotalExportacao;


function getAReceberTotal(){
	return $.get('/indicadores/recebimento/valorAReceberTotal', function(data) {
		$('#valorAReceberTotal').html('R$ ' + data + " MI");
		aReceberTotal = data;
	});
}

function getAReceberTotalOrtopedia() {
	return $.get('/indicadores/recebimento/valorAReceberTotal?unidade=ORTOPEDIA', function(data) {
		$('#valorAReceberTotalOrtopedia').html('R$ ' + data + " MI");
		aReceberTotalOrtopedia = data;
	});
}

function getAReceberTotalEquipamentos() {
	return $.get('/indicadores/recebimento/valorAReceberTotal?unidade=EQUIPAMENTOS', function(data) {
		$('#valorAReceberTotalEquipamentos').html('R$ ' + data + " MI");
		aReceberTotalEquipamentos = data;
	});
}

function getAReceberTotalExportacao() {
	return $.get('/indicadores/recebimento/valorAReceberTotal?unidade=EXPORTACAO', function(data) {
		$('#valorAReceberTotalExportacao').html('R$ ' + data + " MI");
		aReceberTotalExportacao = data;
	});
}

$(document).ready(function() {
	$('canvas').after('<div class="spinner"></div>');

	$.get('/indicadores/recebimento/recebimento-mensal?unidade=ORTOPEDIA', function(chartData) {
		if (graficoRecebimentoMensalOrtopedia != null) {
			graficoRecebimentoMensalOrtopedia.data.datasets = chartData.data.datasets;
        } else {
            var ctx = document.getElementById('graficoRecebimentoMensalOrtopedia').getContext('2d');
            graficoRecebimentoMensalOrtopedia = new Chart(ctx, chartData);
            graficoRecebimentoMensalOrtopedia.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
                return formatCurrency(value);
            }
            graficoRecebimentoMensalOrtopedia.options.tooltips.callbacks.label = function(label) {
                return formatCurrency(label.yLabel);
            }
            graficoRecebimentoMensalOrtopedia.options.scales.yAxes[0].ticks.beginAtZero = true;
            graficoRecebimentoMensalOrtopedia.options.onClick = function (evt, item) {
            	var periodo = item[0]._model.label;
            	var unidade = 'ORTOPEDIA';
            	
            	abrirLink(evt, '/indicadores/recebimento/analitico/recebimentos?unidade=' + unidade + '&periodo=' + periodo);
            };
            graficoRecebimentoMensalOrtopedia.options.legend.display = false;
        }
		graficoRecebimentoMensalOrtopedia.update();
        $('#graficoRecebimentoMensalOrtopedia').next().addClass('d-none');
	});
	
	$.get('/indicadores/recebimento/recebimento-mensal?unidade=EQUIPAMENTOS', function(chartData) {
		if (graficoRecebimentoMensalEquipamentos != null) {
			graficoRecebimentoMensalEquipamentos.data.datasets = chartData.data.datasets;
		} else {
			var ctx = document.getElementById('graficoRecebimentoMensalEquipamentos').getContext('2d');
			graficoRecebimentoMensalEquipamentos = new Chart(ctx, chartData);
			graficoRecebimentoMensalEquipamentos.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
				return formatCurrency(value);
			}
			graficoRecebimentoMensalEquipamentos.options.tooltips.callbacks.label = function(label) {
				return formatCurrency(label.yLabel);
			}
			graficoRecebimentoMensalEquipamentos.options.scales.yAxes[0].ticks.beginAtZero = true;
            graficoRecebimentoMensalEquipamentos.options.onClick = function (evt, item) {
            	var periodo = item[0]._model.label;
            	var unidade = 'EQUIPAMENTOS';
            	
            	abrirLink(evt, '/indicadores/recebimento/analitico/recebimentos?unidade=' + unidade + '&periodo=' + periodo);
            };
			graficoRecebimentoMensalEquipamentos.options.legend.display = false;
		}
		graficoRecebimentoMensalEquipamentos.update();
        $('#graficoRecebimentoMensalEquipamentos').next().addClass('d-none');
	});
	
	$.get('/indicadores/recebimento/recebimento-mensal?unidade=EXPORTACAO', function(chartData) {
		if (graficoRecebimentoMensalExportacao != null) {
			graficoRecebimentoMensalExportacao.data.datasets = chartData.data.datasets;
		} else {
			var ctx = document.getElementById('graficoRecebimentoMensalExportacao').getContext('2d');
			graficoRecebimentoMensalExportacao = new Chart(ctx, chartData);
			graficoRecebimentoMensalExportacao.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
				return formatCurrency(value);
			}
			graficoRecebimentoMensalExportacao.options.tooltips.callbacks.label = function(label) {
				return formatCurrency(label.yLabel);
			}
			graficoRecebimentoMensalExportacao.options.scales.yAxes[0].ticks.beginAtZero = true;
            graficoRecebimentoMensalExportacao.options.onClick = function (evt, item) {
            	var periodo = item[0]._model.label;
            	var unidade = 'EXPORTACAO';
            	
            	abrirLink(evt, '/indicadores/recebimento/analitico/recebimentos?unidade=' + unidade + '&periodo=' + periodo);
            };
			graficoRecebimentoMensalExportacao.options.legend.display = false;
		}
		graficoRecebimentoMensalExportacao.update();
        $('#graficoRecebimentoMensalExportacao').next().addClass('d-none');
	});
	
	
//	$.get('/indicadores/recebimento/graficoInadimplenciaMensal', function(chartData) {
//		if (graficoInadimplenciaMensalTotal != null) {
//			graficoInadimplenciaMensalTotal.data.datasets = chartData.data.datasets;
//		} else {
//			var ctx = document.getElementById('graficoInadimplenciaMensalTotal').getContext('2d');
//			graficoInadimplenciaMensalTotal = new Chart(ctx, chartData);
//			graficoInadimplenciaMensalTotal.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
//				return formatCurrency(value);
//			}
//			graficoInadimplenciaMensalTotal.options.tooltips.callbacks.label = function(label) {
//				return formatCurrency(label.yLabel);
//			}
//			graficoInadimplenciaMensalTotal.options.scales.yAxes[0].ticks.beginAtZero = true;
//			graficoInadimplenciaMensalTotal.options.onClick = function (evt, item) {
//			};
//			graficoInadimplenciaMensalTotal.options.legend.display = false;
//		}
//		graficoInadimplenciaMensalTotal.update();
//        $('#graficoInadimplenciaMensalTotal').next().addClass('d-none');
//	});
//	
//	
//	$.get('/indicadores/recebimento/graficoInadimplenciaMensal?unidade=ORTOPEDIA', function(chartData) {
//		if (graficoInadimplenciaMensalOrtopedia != null) {
//			graficoInadimplenciaMensalOrtopedia.data.datasets = chartData.data.datasets;
//		} else {
//			var ctx = document.getElementById('graficoInadimplenciaMensalOrtopedia').getContext('2d');
//			graficoInadimplenciaMensalOrtopedia = new Chart(ctx, chartData);
//			graficoInadimplenciaMensalOrtopedia.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
//				return formatCurrency(value);
//			}
//			graficoInadimplenciaMensalOrtopedia.options.tooltips.callbacks.label = function(label) {
//				return formatCurrency(label.yLabel);
//			}
//			graficoInadimplenciaMensalOrtopedia.options.scales.yAxes[0].ticks.beginAtZero = true;
//			graficoInadimplenciaMensalOrtopedia.options.onClick = function (evt, item) {
//			};
//			graficoInadimplenciaMensalOrtopedia.options.legend.display = false;
//		}
//		graficoInadimplenciaMensalOrtopedia.update();
//        $('#graficoInadimplenciaMensalOrtopedia').next().addClass('d-none');
//	});	
//
//	
//	$.get('/indicadores/recebimento/graficoInadimplenciaMensal?unidade=EQUIPAMENTOS', function(chartData) {
//		if (graficoInadimplenciaMensalEquipamentos != null) {
//			graficoInadimplenciaMensalEquipamentos.data.datasets = chartData.data.datasets;
//		} else {
//			var ctx = document.getElementById('graficoInadimplenciaMensalEquipamentos').getContext('2d');
//			graficoInadimplenciaMensalEquipamentos = new Chart(ctx, chartData);
//			graficoInadimplenciaMensalEquipamentos.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
//				return formatCurrency(value);
//			}
//			graficoInadimplenciaMensalEquipamentos.options.tooltips.callbacks.label = function(label) {
//				return formatCurrency(label.yLabel);
//			}
//			graficoInadimplenciaMensalEquipamentos.options.scales.yAxes[0].ticks.beginAtZero = true;
//			graficoInadimplenciaMensalEquipamentos.options.onClick = function (evt, item) {
//			};
//			graficoInadimplenciaMensalEquipamentos.options.legend.display = false;
//		}
//		graficoInadimplenciaMensalEquipamentos.update();
//		$('#graficoInadimplenciaMensalEquipamentos').next().addClass('d-none');
//	});
//	
//	
//	$.get('/indicadores/recebimento/graficoInadimplenciaMensal?unidade=EXPORTACAO', function(chartData) {
//		if (graficoInadimplenciaMensalExportacao != null) {
//			graficoInadimplenciaMensalExportacao.data.datasets = chartData.data.datasets;
//		} else {
//			var ctx = document.getElementById('graficoInadimplenciaMensalExportacao').getContext('2d');
//			graficoInadimplenciaMensalExportacao = new Chart(ctx, chartData);
//			graficoInadimplenciaMensalExportacao.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
//				return formatCurrency(value);
//			}
//			graficoInadimplenciaMensalExportacao.options.tooltips.callbacks.label = function(label) {
//				return formatCurrency(label.yLabel);
//			}
//			graficoInadimplenciaMensalExportacao.options.scales.yAxes[0].ticks.beginAtZero = true;
//			graficoInadimplenciaMensalExportacao.options.onClick = function (evt, item) {
//			};
//			graficoInadimplenciaMensalExportacao.options.legend.display = false;
//		}
//		graficoInadimplenciaMensalExportacao.update();
//        $('#graficoInadimplenciaMensalExportacao').next().addClass('d-none');
//	});
	
	
	$.when(getAReceberTotal()).done(function() {
		$.get('/indicadores/recebimento/valorAReceberAtrasado', function(data) {
			$('#valorAReceberAtrasado').html('R$ ' + data + " MI");
			
			$('#valorAReceberAtrasado').parent().parent().find('.progress-bar').width(clamp(0, 100, (Math.round(data / aReceberTotal * 100))) + '%');
			$('#valorAReceberAtrasado').parent().parent().find('.progress-text').html(Math.round(data / aReceberTotal * 100) + '%');
		});
	});
	
	
	$.get('/indicadores/recebimento/valorRecebimentoMesAtual', function(data) {
		$('#valorRecebimentoMesAtual').html('R$ ' + data[0] + '/' + data[1] + " MI");
		$('#valorRecebimentoMesAtual').parent().parent().find('.progress-bar').width(clamp(0, 100, (data[0] / data[1] * 100)) + '%');
		
		if ((data[0] / data[1] * 100) >= 100) {
			$('#valorRecebimentoMesAtual').parent().parent().find('.progress-bar').addClass('success');
		}
		
		$('#valorRecebimentoMesAtual').parent().parent().find('.progress-text').html(Math.round(data[0] / data[1] * 100) + '%');
	});
	
	$.when(getAReceberTotalOrtopedia()).done(function() {
		$.get('/indicadores/recebimento/valorAReceberAtrasado?unidade=ORTOPEDIA', function(data) {
			$('#valorAReceberAtrasadoOrtopedia').html('R$ ' + data + " MI");
			$('#valorAReceberAtrasadoOrtopedia').parent().parent().find('.progress-bar').width(clamp(0, 100, (Math.round(data / aReceberTotalOrtopedia * 100))) + '%');
			$('#valorAReceberAtrasadoOrtopedia').parent().parent().find('.progress-text').html(Math.round(data / aReceberTotalOrtopedia * 100) + '%');
		});
	});
	
	
	$.get('/indicadores/recebimento/valorRecebimentoMesAtual?unidade=ORTOPEDIA', function(data) {
		$('#valorRecebimentoMesAtualOrtopedia').html('R$ ' + data[0] + '/' + data[1] + " MI");
		$('#valorRecebimentoMesAtualOrtopedia').parent().parent().find('.progress-bar').width(clamp(0, 100, ((data[0] / data[1] * 100))) + '%');
		if ((data[0] / data[1] * 100) >= 100) {
			$('#valorRecebimentoMesAtualOrtopedia').parent().parent().find('.progress-bar').addClass('success');
		}
		$('#valorRecebimentoMesAtualOrtopedia').parent().parent().find('.progress-text').html(Math.round(data[0] / data[1] * 100) + '%');
	});
	
	$.when(getAReceberTotalEquipamentos()).done(function() {
		$.get('/indicadores/recebimento/valorAReceberAtrasado?unidade=EQUIPAMENTOS', function(data) {
			$('#valorAReceberAtrasadoEquipamentos').html('R$ ' + data + " MI");
			$('#valorAReceberAtrasadoEquipamentos').parent().parent().find('.progress-bar').width(clamp(0, 100, (Math.round(data / aReceberTotalEquipamentos * 100))) + '%');
			$('#valorAReceberAtrasadoEquipamentos').parent().parent().find('.progress-text').html(Math.round(data / aReceberTotalEquipamentos * 100) + '%');
		});
	});

	
	$.get('/indicadores/recebimento/valorRecebimentoMesAtual?unidade=EQUIPAMENTOS', function(data) {
		$('#valorRecebimentoMesAtualEquipamentos').html('R$ ' + data[0] + '/' + data[1] + " MI");
		$('#valorRecebimentoMesAtualEquipamentos').parent().parent().find('.progress-bar').width(clamp(0, 100, ((data[0] / data[1] * 100))) + '%');
		if ((data[0] / data[1] * 100) >= 100) {
			$('#valorRecebimentoMesAtualEquipamentos').parent().parent().find('.progress-bar').addClass('success');
		}
		$('#valorRecebimentoMesAtualEquipamentos').parent().parent().find('.progress-text').html(Math.round(data[0] / data[1] * 100) + '%');
	});
	
	$.when(getAReceberTotalExportacao()).done(function() {
		$.get('/indicadores/recebimento/valorAReceberAtrasado?unidade=EXPORTACAO', function(data) {
			$('#valorAReceberAtrasadoExportacao').html('R$ ' + data + " MI");
			$('#valorAReceberAtrasadoExportacao').parent().parent().find('.progress-bar').width(clamp(0, 100, (Math.round(data / aReceberTotalExportacao * 100))) + '%');
			$('#valorAReceberAtrasadoExportacao').parent().parent().find('.progress-text').html(Math.round(data / aReceberTotalExportacao * 100) + '%');
		});
	});
	
	
	$.get('/indicadores/recebimento/valorRecebimentoMesAtual?unidade=EXPORTACAO', function(data) {
		$('#valorRecebimentoMesAtualExportacao').html('R$ ' + data[0] + '/' + data[1] + " MI");
		$('#valorRecebimentoMesAtualExportacao').parent().parent().find('.progress-bar').width(clamp(0, 100, ((data[0] / data[1] * 100))) + '%');
		if ((data[0] / data[1] * 100) >= 100) {
			$('#valorRecebimentoMesAtualExportacao').parent().parent().find('.progress-bar').addClass('success');
		}
		$('#valorRecebimentoMesAtualExportacao').parent().parent().find('.progress-text').html(Math.round(data[0] / data[1] * 100) + '%');
	});
});