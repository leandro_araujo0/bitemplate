$(document).ready( function () {
	$.post('', function(table) {
		var headers = [];
		
		table.headers.forEach(function(titulo) {
			headers.push({ title: titulo });
		});
		
		
		var table = $('#myTable').dataTable({
			data: table.data,
			columns: headers,
	    	iDisplayLength: 10000,
	    	dom: 'Blfrpti',
	        buttons: [
	            'copyHtml5',
	            'excelHtml5',
	        ],
	        initComplete: function() {
	        },
	        columnDefs: [
		        {
		            render: function(data, type, row) {
		                if (type === 'display' || type === 'filter') {
		                	var periodo = Number(data);
		                	var anos = Math.floor(periodo / 12);
		                	var meses = periodo % 12;
		                    return anos + "A " + meses + "M";
		                } else {
		                    return data;
		                }
		            },
		            targets: [4],
	        	},
	        ],
	    });
	})
});
