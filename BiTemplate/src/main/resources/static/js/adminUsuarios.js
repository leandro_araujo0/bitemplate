$(document).ready(function() {
	var table = $('#myTable').DataTable({
		columns: [
			{ title: 'Nome Usuario' },
			{ title: 'Senha' },
		],
		dom: 'Bft',
        buttons: [
            'copyHtml5',
            'excelHtml5',
        ],
		iDisplayLength: 1000,
	});
});