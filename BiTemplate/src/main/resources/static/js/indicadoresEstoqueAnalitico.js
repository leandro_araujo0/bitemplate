$(document).ready(function() {
	$.ajax({
		type: 'POST',
		success: function(dataTable) {
			var headers = [];
			dataTable.headers.forEach(function(titulo) {
				headers.push({ title: titulo, class: 'nowrap' });
			});
			
			$('#myTable').DataTable({
				iDisplayLength: 1000,
				columns: headers,
				data: dataTable.data,
		    	dom: 'Blfrpti',
		        buttons: [
		            'copyHtml5',
		            'excelHtml5',
		        ],
			});
		},
	});
});