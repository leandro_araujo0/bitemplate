$.get('/indicadores/producao/graficoProducaoAnualOrtopedia', function (chartData) {
    var ctx = document.getElementById('graficoProducaoAnualOrtopedia').getContext('2d');
    var chart = new Chart(ctx, chartData);
    chart.options.scales.yAxes[0].ticks.beginAtZero = true;
    chart.options.scales.yAxes[0].ticks.callback = function (value, index, values) {
        return formatCurrency(value) + ',00';
    }
    chart.options.tooltips.callbacks.label = function (label) {
        return formatCurrency(label.yLabel);
    }
    chart.update();
});
$.get('/indicadores/producao/graficoProducaoAnualEquipamentos', function (chartData) {
    var ctx = document.getElementById('graficoProducaoAnualEquipamentos').getContext('2d');
    var chart = new Chart(ctx, chartData);
    chart.options.scales.yAxes[0].ticks.beginAtZero = true;
    chart.options.scales.yAxes[0].ticks.callback = function (value, index, values) {
        return formatCurrency(value) + ',00';
    }
    chart.options.tooltips.callbacks.label = function (label) {
        return formatCurrency(label.yLabel);
    }
    chart.update();
});
$.get('/indicadores/producao/graficoCargaMaquina', function (chartData) {
    var ctx = document.getElementById('graficoCargaMaquina').getContext('2d');
    var chart = new Chart(ctx, chartData);
    chart.options.legend.display = false;
    chart.update();
});